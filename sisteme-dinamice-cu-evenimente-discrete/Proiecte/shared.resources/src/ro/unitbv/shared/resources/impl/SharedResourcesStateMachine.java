package ro.unitbv.shared.resources.impl;

import ro.tarnauca.mdplus.petrinetexec.core.PetriNet;
import ro.tarnauca.mdplus.petrinetexec.core.PetriNetEventGenerator;
import ro.tarnauca.mdplus.petrinetexec.core.Place;
import ro.tarnauca.mdplus.petrinetexec.core.PlaceActionCallback;
import ro.tarnauca.mdplus.petrinetexec.core.Transition;
import ro.tarnauca.mdplus.petrinetexec.exceptions.PetriNetException;
import ro.unitbv.shared.resources.SharedResourcesController.SharedResourcesActuators;
import ro.unitbv.shared.resources.SharedResourcesController.SharedResourcesEventsTypes;
import ro.unitbv.shared.resources.SharedResourcesController.SharedResourcesIndicators;
import ro.unitbv.shared.resources.SharedResourcesPN;

public class SharedResourcesStateMachine extends SharedResourcesPN {
	@Override
	public void newEventReceived(SharedResourcesEventsTypes event) {
		PetriNetEventGenerator gen = getPetriNetEventGenerator();

		switch (event) {
		case EV_S1:
			gen.pushEvent("T1", (long) 0);
			break;

		case EV_S3:
			gen.pushEvent("T2", (long) 0);
			break;

		case EV_S4:
			gen.pushEvent("T3", (long) 0);
			break;

		case EV_S5:
			gen.pushEvent("T4", (long) 0);
			break;

		case EV_S6:
			gen.pushEvent("T5", (long) 0);
			break;

		case EV_S2:
			gen.pushEvent("T6", (long) 0);
			break;

		case EV_S7:
			gen.pushEvent("T7", (long) 0);
			break;

		case EV_S8:
			gen.pushEvent("T8", (long) 0);
			break;

		case EV_S9:
			gen.pushEvent("T9", (long) 0);
			break;

		}

	}
	
	private void place_to_trans(PetriNet petriNet, String pos, String trans) {
		try {
			petriNet.retrievePlace(pos).connectToOutputTransition(
					petriNet.retrieveTransition(trans), pos + " -> " + trans, "", 1);
		} catch (PetriNetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	
	private void trans_to_place(PetriNet petriNet, String trans, String place) {
		try {
			petriNet.retrieveTransition(trans).connectToOutputPlace(
					petriNet.retrievePlace(place), trans + " -> "+ place, "", 1);
		} catch (PetriNetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public PetriNet generatePN() {
		PetriNet petriNet = new PetriNet("Shared resources");
		
		petriNet.addPlace("P1", "P1", 0);
		petriNet.addPlace("P2", "P2", 5);
		petriNet.addPlace("P3", "P3", 0);
		petriNet.addPlace("P4", "P4", 0);
		petriNet.addPlace("P5", "P5", 0);
		petriNet.addPlace("P6", "P6", 1);
		petriNet.addPlace("P7", "P7", 1);
		petriNet.addPlace("P8", "P8", 0);
		petriNet.addPlace("P9", "P9", 0);
		petriNet.addPlace("P10", "P10", 0);
		petriNet.addPlace("P11", "P11", 0);
		petriNet.addPlace("P12", "P12", 1);
		petriNet.addPlace("P13", "P13", 2);

		petriNet.addTransition("T1", "T1", Transition.TransitionType.NORMAL);
		petriNet.addTransition("T2", "T2", Transition.TransitionType.NORMAL);
		petriNet.addTransition("T3", "T3", Transition.TransitionType.NORMAL);
		petriNet.addTransition("T4", "T4", Transition.TransitionType.NORMAL);
		petriNet.addTransition("T5", "T5", Transition.TransitionType.NORMAL);
		petriNet.addTransition("T6", "T6", Transition.TransitionType.NORMAL);
		petriNet.addTransition("T7", "T7", Transition.TransitionType.NORMAL);
		petriNet.addTransition("T8", "T8", Transition.TransitionType.NORMAL);
		petriNet.addTransition("T9", "T9", Transition.TransitionType.NORMAL);
	

		place_to_trans(petriNet, "P2", "T1");
		place_to_trans(petriNet, "P1", "T2");
		place_to_trans(petriNet, "P3", "T3");
		place_to_trans(petriNet, "P4", "T4");
		place_to_trans(petriNet, "P5", "T5");
		place_to_trans(petriNet, "P8", "T6");
		place_to_trans(petriNet, "P9", "T7");
		place_to_trans(petriNet, "P10", "T8");
		place_to_trans(petriNet, "P11", "T9");
		place_to_trans(petriNet, "P12", "T6");
		place_to_trans(petriNet, "P6", "T6");
		place_to_trans(petriNet, "P6", "T1");
		place_to_trans(petriNet, "P7", "T1");
		
		trans_to_place(petriNet, "T1", "P1");
		trans_to_place(petriNet, "T2", "P3");
		trans_to_place(petriNet, "T3", "P4");
		trans_to_place(petriNet, "T4", "P5");
		trans_to_place(petriNet, "T5", "P8");
		trans_to_place(petriNet, "T6", "P9");
		trans_to_place(petriNet, "T7", "P10");
		trans_to_place(petriNet, "T8", "P11");
		trans_to_place(petriNet, "T9", "P112");
		trans_to_place(petriNet, "T6", "P13");
		trans_to_place(petriNet, "T4", "P7");
		trans_to_place(petriNet, "T2", "P6");
		trans_to_place(petriNet, "T7", "P6");
		
		
		petriNet.retrievePlace("P2").createPlaceMarkingChangeCallback(
				new PlaceActionCallback() {

					@Override
					protected void callback(PetriNet arg0, Place arg1) {
						//for positions with display indicators
						changeIndicatorState("P2",
								SharedResourcesIndicators.Y3, arg1.getMarking());
					}
				});

		petriNet.retrievePlace("P1").createPlaceMarkingChangeCallback(
				new PlaceActionCallback() {

					@Override
					protected void callback(PetriNet arg0, Place arg1) {
						//for positions with actuators(led)
						if (arg1.getMarking() == 1)
							turnONActuator("P1", SharedResourcesActuators.Y1);
						else
							turnOFFActuator("P1", SharedResourcesActuators.Y1);

					}
				});

		petriNet.retrievePlace("P3").createPlaceMarkingChangeCallback(
				new PlaceActionCallback() {

					@Override
					protected void callback(PetriNet arg0, Place arg1) {
						//for positions with actuators(led)
						if (arg1.getMarking() == 1)
							turnONActuator("P3", SharedResourcesActuators.Y4);
						else
							turnOFFActuator("P3", SharedResourcesActuators.Y4);

					}
				});

		petriNet.retrievePlace("P4").createPlaceMarkingChangeCallback(
				new PlaceActionCallback() {

					@Override
					protected void callback(PetriNet arg0, Place arg1) {
						//for positions with actuators(led)
						if (arg1.getMarking() == 1)
							turnONActuator("P4", SharedResourcesActuators.Y5);
						else
							turnOFFActuator("P4", SharedResourcesActuators.Y5);

					}
				});

		petriNet.retrievePlace("P5").createPlaceMarkingChangeCallback(
				new PlaceActionCallback() {

					@Override
					protected void callback(PetriNet arg0, Place arg1) {
						//for positions with actuators(led)
						if (arg1.getMarking() == 1)
							turnONActuator("P5", SharedResourcesActuators.Y6);
						else
							turnOFFActuator("P5", SharedResourcesActuators.Y6);

					}
				});

		
		petriNet.retrievePlace("P8").createPlaceMarkingChangeCallback(
				new PlaceActionCallback() {

					@Override
					protected void callback(PetriNet arg0, Place arg1) {
						//for positions with display indicators
						changeIndicatorState("P8",
								SharedResourcesIndicators.Y7, arg1.getMarking());
					}
				});
		
		petriNet.retrievePlace("P9").createPlaceMarkingChangeCallback(
				new PlaceActionCallback() {

					@Override
					protected void callback(PetriNet arg0, Place arg1) {
						//for positions with actuators(led)
						if (arg1.getMarking() == 1)
							turnONActuator("P9", SharedResourcesActuators.Y2);
						else
							turnOFFActuator("P9", SharedResourcesActuators.Y2);

					}
				});

		petriNet.retrievePlace("P10").createPlaceMarkingChangeCallback(
				new PlaceActionCallback() {

					@Override
					protected void callback(PetriNet arg0, Place arg1) {
						//for positions with actuators(led)
						if (arg1.getMarking() == 1)
							turnONActuator("P10", SharedResourcesActuators.Y8);
						else
							turnOFFActuator("P10", SharedResourcesActuators.Y8);

					}
				});
		petriNet.retrievePlace("P11").createPlaceMarkingChangeCallback(
				new PlaceActionCallback() {

					@Override
					protected void callback(PetriNet arg0, Place arg1) {
						//for positions with actuators(led)
						if (arg1.getMarking() == 1)
							turnONActuator("P11", SharedResourcesActuators.Y9);
						else
							turnOFFActuator("P11", SharedResourcesActuators.Y9);

					}
				});
		
		return petriNet;
	}

}
