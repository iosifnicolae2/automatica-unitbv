A = [-1 0; 1 0];
B=[1;0];
C=[0 0.1];
D = 0;

[num, den] = ss2tf(A,B,C,D)

[num0, den0] = cloop(num,den);

[h, w] = freqs(num0, den0);

mag = abs(h);
% phase = angle(h);
% phasedeg = phase*180/pi;

subplot(2,1,1), 
plot(log10(w), 20*log(mag))

% loglog(w,mag), 
% grid on
% xlabel 'Frequency (rad/s)', ylabel Magnitude
% subplot(2,1,2), semilogx(w,phasedeg), grid on
% xlabel 'Frequency (rad/s)', ylabel 'Phase (degrees)'

% figure;
subplot(2,1,2)
nyquist(num, den);