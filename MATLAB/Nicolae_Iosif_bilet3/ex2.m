numG = [9];
denG = [1 6];

numH = [1];
denH = [1 0];

[num,den] = series(numG, denG, numH, denH);
% Sistemul este stabil pentru ca (1, j.0) nu este inclus in conturul
% inchis.
nyquist(num, den);