% a) functia de transfer experimentala a sis in circuit deschis(sis cu reactie
% negativa unitara)

T = 1/100;
K = 0.1;

numG = [K];
denG = [T 1 0];

bode(numG, denG)

% b) raspunsul in frecventa
[num0, den0] = cloop(numG, denG);
figure;
freqs(num0, den0);

