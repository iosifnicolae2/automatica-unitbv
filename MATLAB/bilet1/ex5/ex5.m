w=[0:0.02:8]

num=[10];
den=[1 4 4];

[mag,phase]=bode(num,den,w);

figure;
plot(w,20*log(mag))
figure;
plot(w,phase)

% Raspunsul in frecventa
figure;
freqs(num,den,w)

% Marginea de amplitudine(Gm) si de faza(Pm)
[Gm,Pm,Wgm,Wpm] = margin(num,den);
'Marginea de amplitudine:' 
Gm
'Marginea de faza:'
Pm
figure;
margin(num,den);
% margin(num,den)