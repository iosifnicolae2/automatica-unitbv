n1=[0 1];
d1=[1 3];
n2=[0 0.5];
d2=[0 1];
n3=[0 4];
d3=[1 4];
n4=[0 1];
d4=[1 2];

% prima coloana - nr blocului -> sys(1)
% intrarile in respectivul bloc -> sys(2..n)
q = [
    1 0 0;
    2 1 -4;
    3 0 2;
    4 0 2
]
iu=1;
iy=3;

nblocks=4;

blkbuild;
[A,B,C,D]=connect(a,b,c,d,q,iu,iy)
[num,den]=ss2tf(A,B,C,D,iu)
tf(num,den)