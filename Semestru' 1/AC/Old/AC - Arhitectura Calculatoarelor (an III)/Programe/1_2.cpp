#include <conio.h>
#include <stdio.h>
#include <iostream.h>

void meniu()
{
 cout<<"1 - AND\n";
 cout<<"2 - OR\n";
 cout<<"3 - NOT\n";
 cout<<"4 - NAND\n";
 cout<<"5 - XOR\n";
 cout<<"6 - NOR\n";
 cout<<"7 - XNOR\n\n";
 cout<<"0 - Iesire Program";
}

void main()
{
 char ctrl='0';
 int a,b,c;
 do
 {
  clrscr();
  meniu();
  ctrl=getch();
  if (ctrl==0) ctrl=getch();
  switch (ctrl)
  {
   case '1':
   {
    clrscr();
    cout<<"\tSimulare poarta AND\n\n";
    cout<<"A=";
    do
    {
     ctrl=getch();
     if (ctrl==0) ctrl=getch();
    }while((ctrl!='1') && (ctrl!='0'));
    if (ctrl=='0') a=0;
    else a=1;
    cout<<a<<"\n";
    cout<<"B=";
    do
    {
     ctrl=getch();
     if (ctrl==0) ctrl=getch();
    }while((ctrl!='1') && (ctrl!='0'));
    if (ctrl=='0') b=0;
    else b=1;
    cout<<b<<"\n";
    asm{
     mov ax,a;
     mov bx,b;
     and ax,bx;
     mov c,ax;
    }
    cout<<"C="<<c;getch();
    ctrl='1';
    break;
   }
   case '2':
   {
    clrscr();
    cout<<"\tSimulare poarta OR\n\n";
    cout<<"A=";
    do
    {
     ctrl=getch();
     if (ctrl==0) ctrl=getch();
    }while((ctrl!='1') && (ctrl!='0'));
    if (ctrl=='0') a=0;
    else a=1;
    cout<<a<<"\n";
    cout<<"B=";
    do
    {
     ctrl=getch();
     if (ctrl==0) ctrl=getch();
    }while((ctrl!='1') && (ctrl!='0'));
    if (ctrl=='0') b=0;
    else b=1;
    cout<<b<<"\n";
    asm{
     mov ax,a;
     mov bx,b;
     or ax,bx;
     mov c,ax;
    }
    cout<<"\nC="<<c;getch();
    ctrl='2';
    break;
   }
   case '3':
   {
    clrscr();
    cout<<"\tSimulare poarta NOT\n\n";
    cout<<"A=";
    do
    {
     ctrl=getch();
     if (ctrl==0) ctrl=getch();
    }while((ctrl!='1') && (ctrl!='0'));
    if (ctrl=='0') a=0;
    else a=1;
    cout<<a<<"\n";
    asm{
     mov ax,a;
     xor ax,1;
     mov b,ax;
    }
    cout<<"\nB="<<b;getch();
    ctrl='3';
    break;
   }
   case '4':
   {
    clrscr();
    cout<<"\tSimulare poarta NAND\n\n";
    cout<<"A=";
    do
    {
     ctrl=getch();
     if (ctrl==0) ctrl=getch();
    }while((ctrl!='1') && (ctrl!='0'));
    if (ctrl=='0') a=0;
    else a=1;
    cout<<a<<"\n";
    cout<<"B=";
    do
    {
     ctrl=getch();
     if (ctrl==0) ctrl=getch();
    }while((ctrl!='1') && (ctrl!='0'));
    if (ctrl=='0') b=0;
    else b=1;
    cout<<b<<"\n";
    asm{
     mov ax,a;
     mov bx,b;
     and ax,bx;
     xor ax,1;
     mov c,ax;
    }
    cout<<"C="<<c;getch();
    ctrl='4';
    break;
   }
   case '5':
   {
    clrscr();
    cout<<"\tSimulare poarta XOR\n\n";
    cout<<"A=";
    do
    {
     ctrl=getch();
     if (ctrl==0) ctrl=getch();
    }while((ctrl!='1') && (ctrl!='0'));
    if (ctrl=='0') a=0;
    else a=1;
    cout<<a<<"\n";
    cout<<"B=";
    do
    {
     ctrl=getch();
     if (ctrl==0) ctrl=getch();
    }while((ctrl!='1') && (ctrl!='0'));
    if (ctrl=='0') b=0;
    else b=1;
    cout<<b<<"\n";
    asm{
     mov ax,a;
     mov bx,b;
     xor ax,bx;
     mov c,ax;
    }
    cout<<"C="<<c;getch();
    ctrl='5';
    break;
   }
   case '6':
   {
    clrscr();
    cout<<"\tSimulare poarta NOR\n\n";
    cout<<"A=";
    do
    {
     ctrl=getch();
     if (ctrl==0) ctrl=getch();
    }while((ctrl!='1') && (ctrl!='0'));
    if (ctrl=='0') a=0;
    else a=1;
    cout<<a<<"\n";
    cout<<"B=";
    do
    {
     ctrl=getch();
     if (ctrl==0) ctrl=getch();
    }while((ctrl!='1') && (ctrl!='0'));
    if (ctrl=='0') b=0;
    else b=1;
    cout<<b<<"\n";
    asm{
     mov ax,a;
     mov bx,b;
     or ax,bx;
     xor ax,1;
     mov c,ax;
    }
    cout<<"C="<<c;getch();
    ctrl='6';
    break;
   }
   case '7':
   {
    clrscr();
    cout<<"\tSimulare poarta XNOR\n\n";
    cout<<"A=";
    do
    {
     ctrl=getch();
     if (ctrl==0) ctrl=getch();
    }while((ctrl!='1') && (ctrl!='0'));
    if (ctrl=='0') a=0;
    else a=1;
    cout<<a<<"\n";
    cout<<"B=";
    do
    {
     ctrl=getch();
     if (ctrl==0) ctrl=getch();
    }while((ctrl!='1') && (ctrl!='0'));
    if (ctrl=='0') b=0;
    else b=1;
    cout<<b<<"\n";
    asm{
     mov ax,a;
     mov bx,b;
     xor ax,bx;
     xor ax,1;
     mov c,ax;
    }
    cout<<"C="<<c;getch();
    ctrl='7';
    break;
   }
  }
 }while (ctrl!='0');
}