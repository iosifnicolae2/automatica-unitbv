#include<iostream.h>
#include<conio.h>
void pauza()
{
 cout<<"\n\n\t\t\tApasati o tasta";
 getch();
}
void meniu()
{
 cout<<"\t\t\tSimulare Circuite Digitale\n\n";
 cout<<"1 -> Multiplexor\n";
 cout<<"2 -> Demultiplexor\n";
 cout<<"3 -> Decodificator\n";
 cout<<"4 -> Codificatoare de Prioritate (Arbitru)\n";
 cout<<"5 -> Sumator Complet 4 biti\n\n";
 cout<<"0 -> Iesire";
}

void MUX()
{
 int a,b,d0,d1,d2,d3,f;
 char ctrl;
 clrscr();
 cout<<"\t\t\tSimulator Multiplezor\n\n";
 cout<<"\tDati valorile intralilor de control\n";
 cout<<"A=";
 do
 {
  ctrl=getch();
  if (ctrl==0) ctrl=getch();
 }while((ctrl!='1') && (ctrl!='0'));
 if (ctrl=='0') a=0;
 else a=1;
 cout<<a<<"\n";
 cout<<"B=";
 do
 {
  ctrl=getch();
  if (ctrl==0) ctrl=getch();
 }while((ctrl!='1') && (ctrl!='0'));
 if (ctrl=='0') b=0;
 else b=1;
 cout<<b<<"\n";
 cout<<"\tDati valorile intrarilor de date\n";
 cout<<"D0=";
 do
 {
  ctrl=getch();
  if (ctrl==0) ctrl=getch();
 }while((ctrl!='1') && (ctrl!='0'));
 if (ctrl=='0') d0=0;
 else d0=1;
 cout<<d0<<"\n";
 cout<<"D1=";
 do
 {
  ctrl=getch();
  if (ctrl==0) ctrl=getch();
 }while((ctrl!='1') && (ctrl!='0'));
 if (ctrl=='0') d1=0;
 else d1=1;
 cout<<d1<<"\n";
 cout<<"D2=";
 do
 {
  ctrl=getch();
  if (ctrl==0) ctrl=getch();
 }while((ctrl!='1') && (ctrl!='0'));
 if (ctrl=='0') d2=0;
 else d2=1;
 cout<<d2<<"\n";
 cout<<"D3=";
 do
 {
  ctrl=getch();
  if (ctrl==0) ctrl=getch();
 }while((ctrl!='1') && (ctrl!='0'));
 if (ctrl=='0') d3=0;
 else d3=1;
 cout<<d3<<"\n";
 f=!a&!b&d0|!a&b&d1|a&!b&d2|a&b&d3;
 cout<<"\t\t\n\nF="<<f;
}
void DMUX()
{
 int a,b,d,f0,f1,f2,f3;
 char ctrl;
 clrscr();
 cout<<"\t\t\tSimulator Ddemultiplexor\n\n";
 cout<<"\tDati valorea intrarii de date\n";
 cout<<"D=";
 do
 {
  ctrl=getch();
  if (ctrl==0) ctrl=getch();
 }while((ctrl!='1') && (ctrl!='0'));
 if (ctrl=='0') d=0;
 else d=1;
 cout<<d<<"\n";
 cout<<"\tDati valorile intralilor de control\n";
 cout<<"A=";
 do
 {
  ctrl=getch();
  if (ctrl==0) ctrl=getch();
 }while((ctrl!='1') && (ctrl!='0'));
 if (ctrl=='0') a=0;
 else a=1;
 cout<<a<<"\n";
 cout<<"B=";
 do
 {
  ctrl=getch();
  if (ctrl==0) ctrl=getch();
 }while((ctrl!='1') && (ctrl!='0'));
 if (ctrl=='0') b=0;
 else b=1;
 cout<<b<<"\n";
 f0=d&!a&!b;
 f1=d&!a&b;
 f2=d&a&!b;
 f3=d&a&b;
 cout<<"\nF0="<<f0;
 cout<<"\nF1="<<f1;
 cout<<"\nF2="<<f2;
 cout<<"\nF3="<<f3;
}

void DCD()
{
 int a,b,enable,d0,d1,d2,d3;
 char ctrl;
 clrscr();
 cout<<"\t\t\tDecodificator\n\n";
 cout<<"\tDati valorile intralilor de date\n";
 cout<<"A=";
 do
 {
  ctrl=getch();
  if (ctrl==0) ctrl=getch();
 }while((ctrl!='1') && (ctrl!='0'));
 if (ctrl=='0') a=0;
 else a=1;
 cout<<a<<"\n";
 cout<<"B=";
 do
 {
  ctrl=getch();
  if (ctrl==0) ctrl=getch();
 }while((ctrl!='1') && (ctrl!='0'));
 if (ctrl=='0') b=0;
 else b=1;
 cout<<b<<"\n";
 cout<<"Enble=";
 do
 {
  ctrl=getch();
  if (ctrl==0) ctrl=getch();
 }while((ctrl!='1') && (ctrl!='0'));
 if (ctrl=='0') enable=0;
 else enable=1;
 cout<<enable<<"\n";
 if (enable==1)
 {
  d0=!a&!b;
  d1=!a&b;
  d2=a&!b;
  d3=a&b;
 }
 else
 {
  d0=0;
  d1=0;
  d2=0;
  d3=0;
 }
 cout<<"\nD0="<<d0;
 cout<<"\nD1="<<d1;
 cout<<"\nD2="<<d2;
 cout<<"\nD3="<<d3;
}

void ARBITRU()
{
 int a1,a2,a3,a4,f0,f1;
 char ctrl;
 clrscr();
 cout<<"\t\t\tCodificator de Prioritate\n\n";
 cout<<"\tDati valorile intralilor de date\n";
 cout<<"A1=";
 do
 {
  ctrl=getch();
  if (ctrl==0) ctrl=getch();
 }while((ctrl!='1') && (ctrl!='0'));
 if (ctrl=='0') a1=0;
 else a1=1;
 cout<<a1<<"\n";
 cout<<"A2=";
 do
 {
  ctrl=getch();
  if (ctrl==0) ctrl=getch();
 }while((ctrl!='1') && (ctrl!='0'));
 if (ctrl=='0') a2=0;
 else a2=1;
 cout<<a2<<"\n";
 cout<<"A3=";
 do
 {
  ctrl=getch();
  if (ctrl==0) ctrl=getch();
 }while((ctrl!='1') && (ctrl!='0'));
 if (ctrl=='0') a3=0;
 else a3=1;
 cout<<a3<<"\n";
 cout<<"A4=";
 do
 {
  ctrl=getch();
  if (ctrl==0) ctrl=getch();
 }while((ctrl!='1') && (ctrl!='0'));
 if (ctrl=='0') a4=0;
 else a4=1;
 cout<<a4<<"\n";
 f0=!a1&!a2&a4|!a1&!a2&a3;
 f1=!a1&!a3&a4|!a1&a2;
 cout<<"\nF0="<<f0;
 cout<<"\nF1="<<f1;
}

int valid()
{
 char v;
 do
 {
  v=getch();
  if (v==0) v=getch();
 }while((v!='1') && (v!='0'));
 if (v=='0') return 0;
 else return 1;
}
void SUMATOR()
{
 clrscr();
 int a0, b0, a1, b1, a2, b2, a3, b3, c0, c1, c2, c3, c4, s0, s1, s2, s3;
 cout<<"introduceti a0: ";
 a0=valid();cout<<a0<<"\n";
 cout<<"introduceti a1: ";
 a1=valid();cout<<a1<<"\n";
 cout<<"introduceti a2: ";
 a2=valid();cout<<a2<<"\n";
 cout<<"introduceti a3: ";
 a3=valid();cout<<a3<<"\n";
 cout<<"introduceti b0: ";
 b0=valid();cout<<b0<<"\n";
 cout<<"introduceti b1: ";
 b1=valid();cout<<b1<<"\n";
 cout<<"introduceti b2: ";
 b2=valid();cout<<b2<<"\n";
 cout<<"introduceti b3: ";
 b3=valid();cout<<b3<<"\n";
 cout<<"introduceti c0: ";
 c0=valid();cout<<c0<<"\n";
 s0 = (a0+b0+c0)%2;
 c1 = (a0+b0+c0)/2;
 s1 = (a1+b1+c1)%2;
 c2 = (a1+b1+c1)/2;
 s2 = (a2+b2+c2)%2;
 c3 = (a2+b2+c2)/2;
 s3 = (a3+b3+c3)%2;
 c4 = (a3+b3+c3)/2;
 cout<<"Rezultatul: s3s2s1s0  = "<<s3<<s2<<s1<<s0;
 cout<<"\nCarry 4 este: "<<c4;
}

main()
{
 char ctrl='0';
 do
 {
  clrscr();
  meniu();
  ctrl=getch();
  if (ctrl==0) ctrl=getch();
  switch (ctrl)
  {
   case '1':
   {
    MUX();
    pauza();
    break;
   }
   case '2':
   {
    DMUX();
    pauza();
    break;
   }
   case '3':
   {
    DCD();
    pauza();
    break;
   }
   case '4':
   {
    ARBITRU();
    pauza();
    break;
   }
   case '5':
   {
    SUMATOR();
    pauza();
    break;
   }
  }
 }while (ctrl!='0');
}