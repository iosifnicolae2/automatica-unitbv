/*
;Software License Agreement                                         
;                                                                    
;The software supplied herewith by Microchip Technology             
;Incorporated (the "Company") is intended and supplied to you, the  
;Company�s customer, for use solely and exclusively on Microchip    
;products. The software is owned by the Company and/or its supplier,
;and is protected under applicable copyright laws. All rights are   
;reserved. Any use in violation of the foregoing restrictions may   
;subject the user to criminal sanctions under applicable laws, as   
;well as to civil liability for the breach of the terms and         
;conditions of this license.                                        
;                                                                    
;THIS SOFTWARE IS PROVIDED IN AN "AS IS" CONDITION. NO WARRANTIES,  
;WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING, BUT NOT LIMITED  
;TO, IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A       
;PARTICULAR PURPOSE APPLY TO THIS SOFTWARE. THE COMPANY SHALL NOT,  
;IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL OR         
;CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.       
;****************************************************************************
;PICkit(tm) 1 Default Demonstration Program
;
;
;File:		default.c
;Author:	Mike Rylee
;Date:  	1.22.03
;Version:	1.0 (Test Version)
;Description:  This is the default demonstration program programmed into 
;              the PIC12F675
;              
*/

#include "test.h"

//***************************************************************************
//Main() - Main Routine
//***************************************************************************
void main()
{
	Init();                              //Initialize 12F629 Microcontroller
	while(1)                             //Loop Forever
	{	
		ledregister=0xFF;	             //Flash 8 LED's

	}
}
{
//***************************************************************************
//Init - Initialization Routine
//***************************************************************************
void Init()

	#asm 								 //Load Factory Calibration Value Into OSCCAL
		call 0x3FF
		bsf	_STATUS,5
		movwf _OSCCAL
	#endasm
	leddirection = FORWARD;
	TRISIO = 0xFF;                       //Set All I/O's As Inputs
	GPIO = CLEAR;                        //Clear GPIO
	VRCON = CLEAR;                       //Turn Off Voltage Reference Peripheral
	CMCON = 0x07;                        //Turn Off Comparator Peripheral
	TMR0 = CLEAR;                        //Clear Timer0
	OPTION = 0x80;
	ANSEL = 0x31;						 //RA0 Analog Input
	ADCON0 = 0x01;
	state = FIRSTSTATE;                 
	delaytime = DEBOUNCEDELAY;
	IOCB3 = SET;                         //GP3 Interrupt On Pin Changed Enabled
	GPIE = SET;                          //Interrupt On Pin Change Enabled
	T0IE = SET;                          //Timer0 Overflow Interrupt Enabled
	ADIE = SET;
	ADIF = CLEAR;
	T0IF = CLEAR;                        //Clear Timer0 Overflow Interrupt Flag
	GPIF = CLEAR;                        //Clear Interrupt On Pin Change Flag
	GIE = SET;                           //Enable All Interrupts
	return;
}

//***************************************************************************
//Functions
//***************************************************************************

//***************************************************************************
//Led1 - D0
//***************************************************************************
void Led1()                              
{
	TRISIO = LED1TRIS;
	GPIO = LED1ON;
	return;
}

//***************************************************************************
//Led2 - D1
//***************************************************************************
void Led2()				 				 
{
	TRISIO = LED2TRIS;
	GPIO = LED2ON;
	return;
}

//***************************************************************************
//Led3 - D2
//***************************************************************************
void Led3()                              
{
	TRISIO = LED3TRIS;
	GPIO = LED3ON;
	return;
}

//***************************************************************************
//Led4 - D3
//***************************************************************************
void Led4()								 
{
	TRISIO = LED4TRIS;
	GPIO = LED4ON;
	return;
}

//***************************************************************************
//Led5 - D4
//***************************************************************************
void Led5()								 
{
	TRISIO = LED5TRIS;
	GPIO = LED5ON;
	return;
}

//***************************************************************************
//Led6 - D5
//***************************************************************************
void Led6()								 
{
	TRISIO = LED6TRIS;
	GPIO = LED6ON;
	return;
}

//***************************************************************************
//Led7() - D6
//***************************************************************************
void Led7()                              
{
	TRISIO = LED7TRIS;
	GPIO = LED7ON;
	return;
}
//***************************************************************************
//Led8() - D7
//***************************************************************************
void Led8()                              
{
	TRISIO = LED8TRIS;
	GPIO = LED8ON;
	return;
}

//***************************************************************************
//Debounce() - Debounce Routine
//***************************************************************************
unsigned char Debounce()
{
	Delay(delaytime);		 		      //Delay
	

	if (PUSHBUTTON == OPEN)   		      //If Pushbutton Status Is Open, Then
	{	
		return FALSE;     
	}
	else								  //Else, Pushbutton Status Is Closed	
	{				      		          
		return TRUE;       
	}
	
}

//***************************************************************************
//Delay(value) - Delay Routine
//             - Delay=value*2.3ms (When OSC=4MHZ)
//***************************************************************************
void Delay(char value)                    
{
	for (outer=value; outer != 0; outer--)	 
	{
		for (inner=0xFF; inner != 0; inner--)
		{
	  
		}
	}
	return;
}

//***************************************************************************
//LEDStateMachine() - This is the state machine that cycles through the LED's
//     	         	- States are incremented in the interrupt routine after an 
//                 	  overflow on TMR0
//***************************************************************************
void LEDStateMachine()
{
	switch(state)
	{
		case 0:	Led1(); break;
		case 1: Led2(); break;
		case 2: Led3(); break;
		case 3: Led4(); break;
		case 4: Led5(); break;
		case 5: Led6(); break;
		case 6: Led7(); break;
		case 7: Led8(); break;
	}	
	return;
}

//***************************************************************************
//Display() - Display Routine
//          - 1 buffer bit is displayed each time this routine is called
//			- D7..D4 LED'S show most significant nibble
//          - D3..D0  LED'S show least significant nibble
//***************************************************************************
void Display()
{	
	if (ledregisterbuffer == 0)			  //If Buffer Is Empty
	{
		ledregisterbuffer = ledregister;  //Load Buffer
		state = FIRSTSTATE;				  //Reset To First State
	}
	
	savedbuffer = ledregisterbuffer;      //Save Original Buffer Contents
	ledregisterbuffer >>= state;          //Rotate Current Buffer Bit To Bit Position 0
	ledregisterbuffer &= 1;
	
	if (ledregisterbuffer == 1)		      //If Current Buffer Bit Is A 1, Then
	{
		LEDStateMachine();                //Light The LED
	}	
	      
	if ((state == LASTSTATE) & leddirection == 0)		          //If At The Last State, Then
	{	
		state = FIRSTSTATE;	    		  //Reset To First State
		ledregisterbuffer = ledregister;  //Reload Buffer With New Value
	}
	else if((state == FIRSTSTATE) & leddirection == 1)
	{
		
		state = LASTSTATE;
		ledregisterbuffer = ledregister;
	}
	else					              //Else
	{	
		if (leddirection == 0)
		{
			state++;                          
			ledregisterbuffer = savedbuffer;  //Restore Original Buffer Contents
		}
		else
		{
			state--;
			ledregisterbuffer = savedbuffer;  //Restore Original Buffer Contents
		}
	}
	
	return;

}

//***************************************************************************
//Isr() - Interrupt Service Routine
//      - Services Timer0 Overflow
//      - Services GP3 Pin Change
//***************************************************************************
void interrupt Isr()
{
	if ((T0IE & T0IF) == SET)			  //If A Timer0 Interrupt, Then
	{
		Display();						  //Update LED Array
		GODONE = 1;						  //Start an A/D Conversion
		T0IF = CLEAR;                     //Clear Timer0 Interrupt Flag
	}
	
	if ((GPIE & GPIF) == SET)					  //If A GP3 Pin-Change Interrupt
	{
		if (Debounce() == TRUE)     		 //Debounce Pushbutton
			leddirection ^= 0x1;
			      					     //Toggle Timer0 Prescaler (Changes LED Flashing Speed)
		GPIF = CLEAR;                    //Clear Interrupt On Pin Change Flag
	}
	
	if ((ADIE & ADIF) == SET)			//Binary search used to change timer0 prescaler
	{								    //which is used for led speed control
		if (ADRESH < 0x7F)
		{
			if (ADRESH > 0x3F)
			{
				if (ADRESH > 0x5E)
					OPTION = 0x84;
				else				//ADRESH < 0X5E
					OPTION = 0x85;
			}
				
			else 					//ADRESH < 0X3F
			{	if (ADRESH > 0x1F)
					OPTION = 0x86;
				else 				//ADRESH < 0X1F
					OPTION = 0x87;
			}		
					
		}
		else  						//ADRESH > 0x7F
		{
			if (ADRESH > 0xBE)
			{
				if (ADRESH > 0xDD)
					OPTION = 0x80;	
				else				//ADRESH < 0XDD
					OPTION = 0x81;				
			}
			else				    //ADRESH < 0XBE
			{
				if (ADRESH > 0x9F)
					OPTION = 0x82;
				else				//ADRESH < 0X9F
					OPTION = 0x83;
			}	
		}
	
		ADIF = CLEAR;				//Clear A/D Interrupt Flag
	}
	return;
}