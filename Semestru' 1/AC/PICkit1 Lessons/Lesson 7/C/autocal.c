/*
;Software License Agreement                                         
;                                                                    
;The software supplied herewith by Microchip Technology             
;Incorporated (the "Company") is intended and supplied to you, the  
;Company�s customer, for use solely and exclusively on Microchip    
;products. The software is owned by the Company and/or its supplier,
;and is protected under applicable copyright laws. All rights are   
;reserved. Any use in violation of the foregoing restrictions may   
;subject the user to criminal sanctions under applicable laws, as   
;well as to civil liability for the breach of the terms and         
;conditions of this license.                                        
;                                                                    
;THIS SOFTWARE IS PROVIDED IN AN "AS IS" CONDITION. NO WARRANTIES,  
;WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING, BUT NOT LIMITED  
;TO, IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A       
;PARTICULAR PURPOSE APPLY TO THIS SOFTWARE. THE COMPANY SHALL NOT,  
;IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL OR         
;CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.       
;****************************************************************************
;PICkit(tm) 1 Tutorial# 7
;PIC12F6XX Autocalibration & Test Program
;
;Filename:		autocal.c
;Version:  		1.00 (Timer1 Gate Version)
;Date:  		1/9/03
;Author:  		Mike Rylee
;Description:  	This program calibrates the internal RC oscillator on the 
;              	PIC12F6xx to the tolerance specified in the #define TOLERANCE 
;              	measurement parameter below.  The oscillator is calibrated using 
;	           	a 2.5kHz 50% square wave signal input on GP4(Timer1 Gate).
;              	The calibration is started by pushing the button connected to GP3.
;              	A test signal is output on INPUT1 which can be measured
;              	and compared to 5kHz 50% to see how well the device is calibrated.
;              	When LED D6 is on, the microcontroller is in test mode.
;			   	When LED D7 is on, the microcontroller is in calibration
;              	mode.
;
;****************************************************************************
;HI-TECH Functions & Macros Used 
;	DelayUs(delayvalue) - Function used for delay in microseconds
;                       - This file must be compiled with full optimizations
;
;   EEPROM_READ(address)- Macro used for reading data from an EEPROM address
;
;   EEPROM_WRITE(address,value) - Macro used for writing data to an EEPROM address
;
;****************************************************************************
;Revision History
;
;****************************************************************************
*/

#include "autocal.h"

//***************************************************************************
//Main() - Main Routine
//***************************************************************************
void main()
{
	Init();
	while(1)
	{
		if (BUTTON == PRESSED)//If Button Is Pressed
		{	
			Led8();
			Calibrate();
		}
		else
		{
			Led7();
			Test();
		}
	}
}

//***************************************************************************
//Init - Initialization Routine
//***************************************************************************
void Init()
{
	/* Comment out if using simulator, ICD2, or ICE2000
	#asm   
		call 0x3FF			//Load Factory Calibration Value Into OSCCAL
		bsf _STATUS,5		//Select BANK1
		movwf _OSCCAL		
	#endasm
	*/

	ANSEL = 0;				//PIC12F675 Only
	GPIO = 0;				//Clear GPIO Port
	TRISIO = 0b00111110;	//GP0-Output, GP1-Input,GP2-Input,GP3-Input,GP4-Input,GP5-Input
	VRCON = 0;				//Turn Off Vref
	CMCON = 0x07;			//Turn Off Comparator
	OPTION = 0b11001000;	//Pull Ups Disabled, Rising Edge, Prescaler 1:1 WDT
	TMR1GE = SET;			//Timer1 Gate Enabled (A Low Increments Timer1)
	TMR1ON = SET;			//Timer1 On Enabled
	return;
}

//***************************************************************************
//Functions
//***************************************************************************

//***************************************************************************
//Test() - This function is used to test the OSCCAL value in the PIC12F6xx
//       - Checks If Calibration Was Performed
//       - Updates OSCCAL Value If Calibration Was Performed
//       - When The Device Is Properly Calibrated, A 5kHz 50% Square Wave Is
//         Output Until INPUT1 Is Pressed
//***************************************************************************
void Test()
{	
	EEPROM_READ(CALFLAGADR);		//Read Calibration Flag Value
	if (CALIBRATION == TRUE)		//If Calibration Flag Value Is True
	{	
		EEPROM_READ(CALVALADR);		//Read Calibration Value
		OSCCAL = EEDATA;			//Update OSCCAL
	}		
	
/*
;When the PIC12F6xx internal RC oscillator is calibrated, it will be running at
;4MHz +- 1%.  Therefore, since the internal instruction cycle execution is 1/4 the 
;oscillator speed it will be 1us +- 1%.

;The loop below makes a 5kHz 50% Square Wave
;If The Device Is Calibrated Correctly
*/
	do   	
	{		
		OUTPUT = 1;
		DelayUs(99);
		OUTPUT = 0;
		DelayUs(94);
	}	
	while (BUTTON == RELEASED);
	
	return;
}

//***************************************************************************
//Calibrate() - Measures The PW Captured From The Input(GPIO4-Timer1Gate)
//              Reference Signal
//       	  - Updates OSCCAL Value
//            - Updates E^2
//***************************************************************************
void Calibrate()
{
	unsigned char temp, i;
	
	TMR1L = 0;
	temp = 0;
	
	for (i=0; i < CALIBRATIONTIME; i++)
	{
		while (temp == 0)		//While High Edge Is Detected
		{
			temp = TMR1L;
			temp -= TMR1L;
		}
	
		while (temp != 0)	   //While Low Edge Is Detected
		{
			temp = TMR1L;
			temp -= TMR1L;
		}
		CheckPW(TMR1L);
		TMR1L = 0;
	}
	
	UpdateEE();				   //Update EEPROM
	return;
}

//***************************************************************************
//UpdateEE()  - Updates Calibration Flag & Calibration Value
//***************************************************************************
void UpdateEE()
{
	EEPROM_WRITE(CALFLAGADR,TRUE);
	EEPROM_WRITE(CALVALADR,OSCCAL);
	return;
}

//***************************************************************************
//CheckPW(value)  - Computes Difference Between The REFERENCEPW & MEASUREDPW
//				  - The MEASUREDPW Is Passed In The Variable value When
//                  This Function Is Called.
//                - The OSCCAL Is Adjusted Up or Down If The PW Is Outside The
//                  Specified Tolerance
//***************************************************************************
void CheckPW(unsigned char measuredpw)
{
	if (measuredpw == REFERENCEPW)
		return;
		
	else if (measuredpw < REFERENCEPW) //If Oscillator Is Running Slow
	{	 
		 if ( TOLERANCE - (REFERENCEPW - measuredpw) != 0) //If Outside Of Tolerance
		 	OSCCAL += 4;	 							   //Increment OSCCAL		   
	}	
	else    //measuredpw > REFERENCEPW, Oscillator Could Be Too Fast
	{
		 if ( TOLERANCE - (measuredpw - REFERENCEPW) != 0)	//If Outside Of Tolerance
		 	OSCCAL -= 4;	 						//Decrement OSCCAL
	}
	return;
}

//***************************************************************************
//Led7() - D6
//***************************************************************************
void Led7()                              
{
	TRISIO = LED7TRIS;
	GPIO = LED7ON;
	return;
}

//***************************************************************************
//Led8() - D7
//***************************************************************************
void Led8()                              
{
	TRISIO = LED8TRIS;
	GPIO = LED8ON;
	return;
}

//***************************************************************************
//Interrupt Service Routine - Description
//***************************************************************************
void interrupt Isr()
{
	return;
}

//***************************************************************************
//EEPROM Initialization
//***************************************************************************
#asm
	psect eedata, delta=2,abs,ovrld
	org (0x2100+CALFLAGADR)
	DB 	FALSE
	DB	0x00
#endasm
