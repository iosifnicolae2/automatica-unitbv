//Filename:  autocal.h

#ifndef AUTOCAL_H
#define AUTOCAL_H

#include <pic.h>
#include "delay.h"

__CONFIG(INTIO & WDTDIS & MCLRDIS & BORDIS & UNPROTECT & PWRTEN);

//Defines

//Measurement Parameters
#define	REFERENCEPW		200
#define	TOLERANCE		1
#define	CALIBRATIONTIME	255

//Main I/O
#define	BUTTON		GPIO3
#define	OUTPUT		GPIO0

//LED's
#define	LED7TRIS	0b11111000
#define	LED8TRIS	0b11111000
#define	LED7ON		0b00000100
#define	LED8ON		0b00000010

//Bits
#define	SET				1
#define	CLEAR			0
#define	RELEASED		1
#define	PRESSED			0

//Calibration
#define	TRUE		0x5A
#define	FALSE		0xA5
#define	CALIBRATION	EEDATA

//EEPROM Adresses
#define	CALFLAGADR		0x7E 	/*Value of 0xA5 => Calibration Has Not Been Performed
								  Value of 0x5A => Calibration Has Been Performed*/
#define	CALVALADR		0x7F	//Calibration Value

//Global Variable Declarations


//Function Prototypes
void Init();
void Test();
void Calibrate();
void CheckPW(unsigned char measuredpw);
void UpdateEE();
void Led7();
void Led8();

#endif
