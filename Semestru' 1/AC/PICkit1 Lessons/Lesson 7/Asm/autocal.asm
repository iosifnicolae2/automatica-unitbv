;Software License Agreement                                         
;                                                                    
;The software supplied herewith by Microchip Technology             
;Incorporated (the "Company") is intended and supplied to you, the  
;Company�s customer, for use solely and exclusively on Microchip    
;products. The software is owned by the Company and/or its supplier,
;and is protected under applicable copyright laws. All rights are   
;reserved. Any use in violation of the foregoing restrictions may   
;subject the user to criminal sanctions under applicable laws, as   
;well as to civil liability for the breach of the terms and         
;conditions of this license.                                        
;                                                                    
;THIS SOFTWARE IS PROVIDED IN AN "AS IS" CONDITION. NO WARRANTIES,  
;WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING, BUT NOT LIMITED  
;TO, IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A       
;PARTICULAR PURPOSE APPLY TO THIS SOFTWARE. THE COMPANY SHALL NOT,  
;IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL OR         
;CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.     
;****************************************************************************
;****************************************************************************
;PICkit(tm) 1 Tutorial #7
;PIC12F6XX Autocalibration & Test Program
;
;Filename:		autocal.asm
;Version:  		1.00 (Timer1 Gate Version)
;Date:  		1/6/03
;Author: 		Mike Rylee
;Description: 	This program calibrates the internal RC oscillator on the 
;              	PIC12F6XX to the tolerance specified in the #define TOLERANCE 
;              	measurement parameter below.  The oscillator is calibrated using 
;	           	a 2.5kHz 50% square wave signal input on GP4(Timer1 Gate).
;              	The calibration is started by pushing the button connected to GP3.
;              	A test signal is output on INPUT1 which can be measured
;              	and compared to 5kHz 50% to see how well the device is calibrated.
;              	When LED D6 is on, the microcontroller is in test mode.
;			   	When LED D7 is on, the microcontroller is in calibration
;              	mode.
;****************************************************************************
;Revision History
;
;****************************************************************************

	list      p=12f675       
	#include <p12f675.inc>        

	__CONFIG   _CP_OFF & _CPD_OFF & _BODEN_OFF & _MCLRE_OFF & _WDT_OFF &  _PWRTE_ON & _INTRC_OSC_NOCLKOUT  

;****************************************************************************
;Defines
;****************************************************************************
#define	BANK0	banksel	0x00
#define BANK1	banksel	0x80

;Measurement Parameters
#define	REFERENCEPW			.200;Reference PulseWidth In Microseconds
#define	TOLERANCE			.1	;Tolerance Can Be Tweaked For Oscillator Accuracy
#define	CALIBRATIONTIME		.255;Number Of Times To Measure PW During Calibration

;MAIN I/O
#define	INPUT1			GPIO,3	;Calibrate Button
#define	OUTPUT			GPIO,0	;Outputs 5KHz Waveform

;LED's
#define	LED7TRIS	b'11111000'
#define	LED8TRIS	b'11111000'
#define	LED7ON		b'00000100'
#define	LED8ON		b'00000010'

;EEPROM Addresses
#define	CALFLAGADR			0x7E	;When Value = 0xA5 => Calibration Has Not Been Performed
	                                ;When Value = 0x5A => Calibration Has Been Performed
#define	CALVALADR			0X7F	;Calibration Value 

;****************************************************************************
;General Purpose Registers (GPR's) 
;****************************************************************************
	cblock 0x20
COUNTER	
	endc
;****************************************************************************
;Reset Vector
;****************************************************************************
	ORG     0x000            
	;call    0x3FF      ; retrieve factory calibration value
						; comment instruction if using simulator, ICD2, or ICE2000
	BANK1			   	
	movwf   OSCCAL     	; Load OSCCAL
	goto    Init            

;****************************************************************************
;Interrupt Vector
;****************************************************************************
	ORG 	0x04
	retfie

;****************************************************************************
;Initialization
;****************************************************************************
Init
	movlw	b'00111110'	;GP0-Output, GP1-Input, GP2-Input,GP3-Input,GP4-Input, GP5-Input
	movwf	TRISIO		
	clrf	ANSEL		;12f675 Only		
	clrf	VRCON		;Turn Off VREF
	BANK0				

	movlw	.7			;Turn Off comparator
	movwf	CMCON

	BANK1				
	movlw	b'11001000'	;Pull Ups Disabled, Rising Edge, Assigned to WDT , Prescaler is 1:1 WDT
	movwf	OPTION_REG	
	BANK0				
	clrf	T1CON				
	bsf		T1CON,TMR1GE;Timer1 Gate Enable (A Low Increments Timer1)
	bsf		T1CON,TMR1ON
	clrf	GPIO
	
;****************************************************************************
;Main Program - This routine watches button INPUT1
;             - Calls Calibrate() if INPUT1 button is pushed
;			    otherwise Calls Test()
;****************************************************************************
Main
	btfss	INPUT1		;Is Calibrate Button Pressed?
	goto	Two			;Yes, Enter calibration mode

One						;No, Enter testing mode
	call	Led7
	call	Test
	goto	Main	

Two
	call	Led8
	call	Calibrate
	goto	Main
	
;****************************************************************************
;Subroutines & Functions
;****************************************************************************

;****************************************************************************
;Test() - This routine is used to test the OSCCAL value in the 12F62x
;		- Checks If Calibration Was Performed
;       - Updates OSCCAL Value If Calibration Was Performed
;       - When the device is properly calibrated, A 5kHz 50% Square Wave Is Output
;         Until INPUT1 Is Pressed
;****************************************************************************
Test
	movlw	CALFLAGADR
	call	EERead
	sublw	0x5A
	btfss	STATUS,Z	;Was Calibration Flag Set?
	goto	StartTest	;No Don't Change Osccal
	
	movlw	CALVALADR
	call	EERead		;Yes Change The Osccal
	BANK1				
	movwf	OSCCAL
	BANK0				

;When the PIC12F6XX internal RC oscillator is calibrated, it will be running at
;4MHz +- 1%.  Therefore, since the internal instruction cycle execution is 1/4 the 
;oscillator speed it will be 1us +- 1%.

;The instructions below in StartTest make a 5kHZ 50% Square Wave Test Signal => 200us Period.
;When the internal RC oscillator is not calibrated the square wave frequency generated
;below will vary since the internal instruction cycle of the device will not be 1us.
	
StartTest				
	bsf		OUTPUT		;1us
	movlw	.31			;
	call	DelayUs		;99us =>100us High Edge
	bcf		OUTPUT		;1us
	movlw	.30			;96us
	call	DelayUs
	btfsc	INPUT1		;1us
	goto	StartTest	;2us  =>1+96+1+2 = 100us Low Edge
	return

;****************************************************************************
;Calibrate() - Measures The PW Captured From The Input(GP4-Timer1Gate)
;			   Reference Signal
;            - Updates OSCCAL Value
;            - Updates E^2 
;****************************************************************************
Calibrate
	movlw	CALIBRATIONTIME
	movwf	COUNTER		;Calibration Counter	
	clrf	TMR1L
			
Synch		
	movf	TMR1L,W	
	nop
	subwf	TMR1L,W
	btfsc	STATUS,Z	;Was A Low Edge Detected?
	goto	Synch		;No, High Edge Detected, Wait For Beginning of Low Edge

Sample	
	movf	TMR1L,W		;Yes, Low Edge Detected
	nop					;Waste A Clock Cycle To See If Timer1 Increments
	subwf	TMR1L,W
	btfss	STATUS,Z	;Was A Low Edge Still Detected
	goto	Sample		;Yes, Sample Again
	
	movf	TMR1L,W		;No, High Edge Was Detected
	call	CheckPW		;Check The Measured Pulse Width
	clrf	TMR1L		;Reset TMR1L
	decfsz	COUNTER,F	;Decrement The Calibration Counter
	goto	Synch
	call	UpdateEE	;Update E^2 When Finished
	return

;****************************************************************************
;UpdateEE - This routine Updates Calibration Flag & Calibration Value
;****************************************************************************
UpdateEE
	BANK1				;BANK1
	movlw	0x5A		;Update Calibration Flag
	movwf	EEDATA
	movlw	CALFLAGADR
	call	EEWrite
	
	movf	OSCCAL,W
	movwf	EEDATA
	movlw	CALVALADR
	call	EEWrite 	;Update Calibration Value
	BANK0				;BANK0
	return
	
;****************************************************************************
;CheckPW(W) - This routine computes the difference between the REFERENCEPW 
;			  and MEASUREDPW
;          	- The MEASUREDPW is contained in W when this routine is called
;           - The Osccal Is Adjusted Up or Down If The PW Is Outside The 
;             Specified Tolerance
;****************************************************************************
CheckPW
	sublw	REFERENCEPW
	btfsc	STATUS,Z	;If (ReferencePW - MeasuredPW = 0) Don't Change Osccal
	return
	
	btfsc	STATUS,C	;If (ReferencePW - MeasuredPW > 0) Oscillator Could Be Too Fast
	goto	RunningSlow	;Else Oscillator Could Be Too Slow

RunningFast
	xorlw	0xFF		;Two's Complement Value (Absolute Value)
	addlw	.1
	sublw	TOLERANCE	;If (Tolerance - (ReferencePW - MeasuredPW) = 0 ) Don't Change Osccal
	btfsc	STATUS,Z
	return
	goto	AdjustDown	;Else Adjust Osccal Down
	
RunningSlow
	sublw	TOLERANCE	;If (Tolerance - (ReferencePW - MeasuredPW) = 0) Don't Change Osccal
	btfsc	STATUS,Z
	return
	goto	AdjustUp	;Else Adjust Osccal Up
	
AdjustDown
	BANK1				;BANK1
	movlw	.4
	subwf	OSCCAL,F	;Adjust Osccal Down
	BANK0				;BANK0
	return
	
AdjustUp
	BANK1				;BANK1
	movlw	.4
	addwf	OSCCAL,F	;Adjust Osccal Up
	BANK0				;BANK0
	return	
	
;****************************************************************************	
;EEReadW) - Address To Read Is Contained In W When This Function Is Called
;****************************************************************************
EERead
	BANK1				;BANK1
	movwf	EEADR
	bsf		EECON1,RD
	movf	EEDATA,W
	BANK0				;BANK0
	return	

;****************************************************************************
;EEWrite(W) - Address To Read Is Contained In W When This Function Is Called
;           - EEDATA Is Loaded Prior To This Function Call
;           - BANK1 must be selected before this function is called
;****************************************************************************	
EEWrite
	movwf	EEADR		
	bsf		EECON1,WREN
	bcf		INTCON,GIE
	movlw	0x55
	movwf	EECON2
	movlw	0xAA
	movwf	EECON2
	bsf		EECON1,WR
EEComplete
	btfsc	EECON1,WR
	goto	EEComplete
	bcf		EECON1,WREN	
	return
	
;****************************************************************************
;DelayUs(W) - Delay Microseconds
;           - DelayTime(W) = [(1)+(2)+(1)+(W*3-1)+1+2] * (4/OSC) 
;             (This includes the movlw & the call)
;	    	- Max Time When W=0xFF, [ 771 Cycles * (4/OSC) ] 
; 	    	- Must Declare COUNTER AS GPR
;           - W > 0
;****************************************************************************
DelayUs
	movwf	COUNTER
Loop1
	decfsz	COUNTER,F
	goto	Loop1	
	nop
	return

;****************************************************************************
;LED7 - D6
;****************************************************************************
Led7
	BANK1				;BANK1
	movlw	LED7TRIS
	movwf	TRISIO		
	BANK0				;BANK0
	movlw	LED7ON
	movwf	GPIO
	return

;****************************************************************************
;LED8 - D7
;****************************************************************************
Led8
	BANK1				;BANK1
	movlw	LED8TRIS
	movwf	TRISIO	
	BANK0				;BANK0
	movlw	LED8ON
	movwf	GPIO
	return

;****************************************************************************
;EEPROM - CALFLAGADR  - Contains Calibration Flag Value  
;       - CALVALADR   - Contains Calibration Value
;****************************************************************************
	ORG	(0X2100+CALFLAGADR) 	

	DE	0A5H	;Initialize Calibration Flag

	DE	000H	;Initialize Calibration Value

	END
