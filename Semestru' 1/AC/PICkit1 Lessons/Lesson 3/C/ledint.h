//Filename:	ledint.h

#ifndef LEDINT_H
#define LEDINT_H

#include <pic.h>

__CONFIG(INTIO & WDTDIS & MCLRDIS & BORDIS & UNPROTECT & PWRTEN);

//Defines

#define	PUSHBUTTON	 	GPIO3		//GP3 - Pushbutton Input
#define	FASTFLASHING	0b10000010  //LED's Are Flashing Fast By Default
#define	SLOWFLASHING    0b10000101  //This Value Is Xored With FASTFLASHING In The Isr
#define	SET				1
#define	CLEAR			0
#define	TRUE			1
#define	FALSE			0

//Global Variable Declarations
extern unsigned char ledregister;

//Function Prototypes
void Init();
extern unsigned char Debounce();
extern void Display();
extern void InitLED();

#endif
