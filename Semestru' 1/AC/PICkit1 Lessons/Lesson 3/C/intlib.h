//Filename:	intlib.h

#ifndef INT_H
#define INT_H

#include <pic.h>

//Defines
#define	LED1TRIS	0b11001111
#define	LED2TRIS	0b11001111
#define	LED3TRIS	0b11101011
#define	LED4TRIS	0b11101011
#define	LED5TRIS	0b11011011
#define	LED6TRIS	0b11011011
#define	LED7TRIS	0b11111001
#define	LED8TRIS	0b11111001
#define LED1ON		0b00010000
#define	LED2ON		0b00100000
#define	LED3ON		0b00010000
#define	LED4ON		0b00000100
#define	LED5ON		0b00100000
#define	LED6ON		0b00000100
#define	LED7ON		0b00000100
#define	LED8ON		0b00000010
#define	PUSHBUTTON	 	GPIO3		//GP3 - Pushbutton Input
#define	DEBOUNCEDELAY	3		    //Debounce Delay Constant
#define	OPEN			1
#define	CLOSED			0
#define	SET				1
#define	CLEAR			0
#define	FIRSTSTATE		0
#define LASTSTATE		7
#define	TRUE			1
#define	FALSE			0

//Global Variable Declarations
unsigned char savedbuffer;
unsigned char inner,outer;
unsigned char state;
unsigned char temp;
unsigned char ledregisterbuffer;
unsigned char ledregister;
unsigned char delaytime;

//Function Prototypes
void Init();
void Led1();
void Led2();
void Led3();
void Led4();
void Led5();
void Led6();
void Led7();
void Led8();
unsigned char Debounce();
void Delay(char value);
void DelayUs(char value);
void LEDStateMachine();
void Display();
void InitLED();

#endif
