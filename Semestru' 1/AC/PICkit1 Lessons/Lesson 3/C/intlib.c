/*
;Software License Agreement                                         
;                                                                    
;The software supplied herewith by Microchip Technology             
;Incorporated (the "Company") is intended and supplied to you, the  
;Company�s customer, for use solely and exclusively on Microchip    
;products. The software is owned by the Company and/or its supplier,
;and is protected under applicable copyright laws. All rights are   
;reserved. Any use in violation of the foregoing restrictions may   
;subject the user to criminal sanctions under applicable laws, as   
;well as to civil liability for the breach of the terms and         
;conditions of this license.                                        
;                                                                    
;THIS SOFTWARE IS PROVIDED IN AN "AS IS" CONDITION. NO WARRANTIES,  
;WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING, BUT NOT LIMITED  
;TO, IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A       
;PARTICULAR PURPOSE APPLY TO THIS SOFTWARE. THE COMPANY SHALL NOT,  
;IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL OR         
;CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.   
;****************************************************************************
;PICkit 1 Tutorial #3
;Interrupts
;
;Filename:		intlib.c
;Author:		Mike Rylee
;Date:  		1.14.03
;Version:		1.0 (8 LED Version)
;Description:	This is a C library to be used with ledint.c for flashing LED's
;				The functions Display & Debounce are called in ledint.c 
;				interrupt service routine for flashing LED's and debouncing
;				a GP3 button push.  The global variable LEDREGISTER is used
;				for flashing LED's (i.e. LEDREGISTER = 0xFF => Flash LED's D7..D0) 
;****************************************************************************    
;Revision History
;****************************************************************************    
*/

#include <pic.h>
#include "intlib.h"

//***************************************************************************
//Initialization 
//***************************************************************************
void InitLED()
{
	ledregisterbuffer = 0;		//Initialize buffer to be empty
	state = FIRSTSTATE;         //Reset state machine variable        
	delaytime = DEBOUNCEDELAY;	//Load debouncing delay constant
	return;
}

//***************************************************************************
//Functions
//***************************************************************************

//***************************************************************************
//Led1 - D0
//***************************************************************************
void Led1()                              
{
	TRISIO = LED1TRIS;
	GPIO = LED1ON;
	return;
}

//***************************************************************************
//Led2 - D1
//***************************************************************************
void Led2()				 				 
{
	TRISIO = LED2TRIS;
	GPIO = LED2ON;
	return;
}

//***************************************************************************
//Led3 - D2
//***************************************************************************
void Led3()                              
{
	TRISIO = LED3TRIS;
	GPIO = LED3ON;
	return;
}

//***************************************************************************
//Led4 - D3
//***************************************************************************
void Led4()								 
{
	TRISIO = LED4TRIS;
	GPIO = LED4ON;
	return;
}

//***************************************************************************
//Led5 - D4
//***************************************************************************
void Led5()								 
{
	TRISIO = LED5TRIS;
	GPIO = LED5ON;
	return;
}

//***************************************************************************
//Led6 - D5
//***************************************************************************
void Led6()								 
{
	TRISIO = LED6TRIS;
	GPIO = LED6ON;
	return;
}

//***************************************************************************
//Led7() - D6
//***************************************************************************
void Led7()                              
{
	TRISIO = LED7TRIS;
	GPIO = LED7ON;
	return;
}
//***************************************************************************
//Led8() - D7
//***************************************************************************
void Led8()                              
{
	TRISIO = LED8TRIS;
	GPIO = LED8ON;
	return;
}

//***************************************************************************
//Debounce() - Debounce Routine
//***************************************************************************
//***************************************************************************
//Debounce() - Debounce Routine
//           - returns TRUE if switch is closed
//           - returns FALSE if switch is open
//***************************************************************************
unsigned char Debounce()
{
	Delay(delaytime);		 		      //Delay
	
	if (PUSHBUTTON == OPEN)   		      //If Pushbutton Status Is Open, Then
	{	
		return FALSE;     
	}
	else								  //Else, Pushbutton Status Is Closed	
	{				      		          
		return TRUE;       
	}

}

//***************************************************************************
//Delay(value) - Delay Routine
//             - Delay=value*2.3ms (When OSC=4MHZ)
//***************************************************************************
void Delay(char value)                    
{
	for (outer=value; outer != 0; outer--)	 
	{
		for (inner=0xFF; inner != 0; inner--)
		{
	  
		}
	}
	return;
}

//***************************************************************************
//LEDStateMachine() - This is the state machine that cycles through the LED's
//     	         	- States are incremented in the interrupt routine after an 
//                 	  overflow on TMR0
//***************************************************************************
void LEDStateMachine()
{
	switch(state)
	{
		case 0:	Led1(); break;
		case 1: Led2(); break;
		case 2: Led3(); break;
		case 3: Led4(); break;
		case 4: Led5(); break;
		case 5: Led6(); break;
		case 6: Led7(); break;
		case 7: Led8(); break;
	}	
	return;
}

//***************************************************************************
//Display() - Display Routine
//          - 1 buffer bit is displayed each time this routine is called
//			- D7..D4 LED'S show most significant nibble
//          - D3..D0 LED'S show least significant nibble
//***************************************************************************
void Display()
{	
	if (ledregisterbuffer == 0)			  //If Buffer Is Empty
	{
		ledregisterbuffer = ledregister;  //Load Buffer
		state = FIRSTSTATE;				  //Reset To First State
	}
	
	savedbuffer = ledregisterbuffer;      //Save Original Buffer Contents
	ledregisterbuffer >>= state;          //Rotate Current Buffer Bit To Bit Position 0
	ledregisterbuffer &= 1;
	
	if (ledregisterbuffer == 1)		      //If Current Buffer Bit Is A 1, Then
	{
		LEDStateMachine();                //Light The LED
	}	
	      
	if (state == LASTSTATE)		          //If At The Last State, Then
	{	
		state = FIRSTSTATE;	    		  //Reset To First State
		ledregisterbuffer = ledregister;  //Reload Buffer With New Value
	}
	else					              //Else
	{	
		state++;                          //Go To Next State
		ledregisterbuffer = savedbuffer;  //Restore Original Buffer Contents
	}
	
	return;

}
