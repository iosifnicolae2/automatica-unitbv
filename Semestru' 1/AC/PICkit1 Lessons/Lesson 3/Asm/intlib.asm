;Software License Agreement                                         
;                                                                    
;The software supplied herewith by Microchip Technology             
;Incorporated (the "Company") is intended and supplied to you, the  
;Company�s customer, for use solely and exclusively on Microchip    
;products. The software is owned by the Company and/or its supplier,
;and is protected under applicable copyright laws. All rights are   
;reserved. Any use in violation of the foregoing restrictions may   
;subject the user to criminal sanctions under applicable laws, as   
;well as to civil liability for the breach of the terms and         
;conditions of this license.                                        
;                                                                    
;THIS SOFTWARE IS PROVIDED IN AN "AS IS" CONDITION. NO WARRANTIES,  
;WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING, BUT NOT LIMITED  
;TO, IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A       
;PARTICULAR PURPOSE APPLY TO THIS SOFTWARE. THE COMPANY SHALL NOT,  
;IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL OR         
;CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.       
;****************************************************************************
;PICkit(tm) 1 Tutorial #3
;Interrupts
;
;Filename:		intlib.asm			
;Author:		Mike Rylee
;Date:  		1.13.03
;Version:		1.0 (8 LED Version)
;Description:  	This is an ASSEMBLY written library that is used for flashing
;              	LED's on the PICkit 1.  The Subroutines Display, Debounce, and
;              	InitLED are linked to the ledinterrupts.asm file where they are
;              	called from the interrupt service routine.  Also, the global
;              	variable LEDREGISTER is available to the user for
;              	use for flashing LED's
;****************************************************************************
;Revision History
;****************************************************************************

;****************************************************************************
#include <p12f675.inc>


;****************************************************************************
;Defines
;****************************************************************************
#define	BANK1		banksel 0x80	;Select Bank 1
#define	BANK0		banksel	0x00	;Select Bank 0
#define	LED1TRIS	b'11001111'
#define	LED2TRIS	b'11001111'
#define	LED3TRIS	b'11101011'
#define	LED4TRIS	b'11101011'
#define	LED5TRIS	b'11011011'
#define	LED6TRIS	b'11011011'
#define	LED7TRIS	b'11111001'
#define	LED8TRIS	b'11111001'
#define LED1ON		b'00010000'
#define	LED2ON		b'00100000'
#define	LED3ON		b'00010000'
#define	LED4ON		b'00000100'
#define	LED5ON		b'00100000'
#define	LED6ON		b'00000100'
#define	LED7ON		b'00000100'
#define	LED8ON		b'00000010'
#define	PUSHBUTTON		.3			;GP3 - Pushbutton Input
#define	DELAYTIME		.13			;Debounce Delay Constant ~10ms

;****************************************************************************
;General Purpose Registers (GPR's) 
;****************************************************************************
	UDATA_SHR
INNER			res	1		; register used in DELAY routine
OUTER			res	1		; register used in DELAY routine
STATE			res	1		; register used in STATEMACHINE routine
LEDREGISTER		res	1		; register used in DISPLAY routine
LEDREGISTERBUFFER	res	1	; register used in DISPLAY routine

;****************************************************************************
;Global Variables
;****************************************************************************
	global	LEDREGISTER
	
;****************************************************************************
;Subroutines & Functions
;****************************************************************************
	code
;****************************************************************************
;Initialization
;****************************************************************************
InitLED
	global	InitLED
	clrf	STATE				;Initialize To State 0
	clrf	LEDREGISTERBUFFER	;Initialize Buffer To Be Empty
	return
		
;****************************************************************************
;LEDSTATEMACHINE - This is the state machine that cycles through the LED's
;     	         - States are incremented in the interrupt routine after an 
;                  overflow on TMR0
;****************************************************************************
LEDStateMachine
	movlw	StateMachineStart	;Put Address Of Start Label In W
	addwf	STATE,w				;Add The State Offset
	btfsc	STATUS,C			;Did we cross a 256 byte boundary?
	incf	PCLATH,f			;Yes, increment PCLATH
	movwf	PCL					;Load program counter with computed goto value

StateMachineStart
	goto	S0
	goto	S1
	goto	S2
	goto	S3
	goto	S4
	goto	S5
	goto	S6
	goto	S7

S0	call	Led1
	goto	EndLEDStateMachine

S1	call	Led2
	goto	EndLEDStateMachine

S2	call	Led3
	goto	EndLEDStateMachine

S3	call	Led4
	goto	EndLEDStateMachine

S4	call	Led5
	goto	EndLEDStateMachine

S5	call	Led6
	goto	EndLEDStateMachine

S6	call	Led7
	goto	EndLEDStateMachine

S7	call	Led8
	
EndLEDStateMachine
	return
	
;****************************************************************************
;LED1 - D0
;****************************************************************************
Led1
	BANK1						
	movlw	LED1TRIS
	movwf	TRISIO
	BANK0			
	movlw	LED1ON
	movwf	GPIO
	return
	
;****************************************************************************
;LED2 - D1
;****************************************************************************
Led2	
	BANK1				
	movlw	LED2TRIS
	movwf	TRISIO
	BANK0				
	movlw	LED2ON
	movwf	GPIO
	return

;****************************************************************************
;LED3 - D2
;****************************************************************************
Led3	
	BANK1				
	movlw	LED3TRIS
	movwf	TRISIO
	BANK0				
	movlw	LED3ON
	movwf	GPIO
	return
	
;****************************************************************************
;LED4 - D3
;****************************************************************************
Led4
	BANK1				
	movlw	LED4TRIS
	movwf	TRISIO	
	BANK0				
	movlw	LED4ON
	movwf	GPIO
	return
	
;****************************************************************************
;LED5 - D4
;****************************************************************************
Led5
	BANK1				
	movlw	LED5TRIS
	movwf	TRISIO				
	BANK0				
	movlw	LED5ON
	movwf	GPIO
	return

;****************************************************************************
;LED6 - D5
;****************************************************************************
Led6
	BANK1				
	movlw	LED6TRIS
	movwf	TRISIO			
	BANK0				
	movlw	LED6ON
	movwf	GPIO
	return

;****************************************************************************
;LED7 - D6
;****************************************************************************
Led7
	BANK1				
	movlw	LED7TRIS
	movwf	TRISIO		
	BANK0				
	movlw	LED7ON
	movwf	GPIO
	return

;****************************************************************************
;LED8 - D7
;****************************************************************************
Led8
	BANK1				
	movlw	LED8TRIS
	movwf	TRISIO	
	BANK0				
	movlw	LED8ON
	movwf	GPIO
	return

;****************************************************************************
;DEBOUNCE() - Pushbutton Debouncing Routine
;			- Returns 1 If Switch Is Closed
;           - Returns 0 If Switch Is Open
;****************************************************************************
Debounce
	global	Debounce
	BANK0				  	; Ensure Bank0
	movlw	DELAYTIME	  	; Delay
	call	Delay
	btfsc	GPIO,PUSHBUTTON ; Is The Switch Open?
	goto	Open			

Closed
	retlw	1	  		
		
Open	
	retlw	0
	
;****************************************************************************
;Delay(W) - DelayTime = [ (1)+(2)+(2)+(W*768-W)+(W*3-1)+(2) ]*(OSC/4)cycles
;           (This includes the call & The movlw)
;         - Max Time When W=0xFF, [ 196356 Cycles * (OSC/4) ]
;         - Must Declare INNER & OUTER AS GPR'S
;****************************************************************************
Delay
	movwf	OUTER
	clrf	INNER
D1	decfsz	INNER,f
	goto	D1
D2	decfsz	OUTER,f
	goto	D1
	return

;****************************************************************************
;Display()-Displays Value Stored In LEDREGISTERBUFFER On LED Array
;		  -1 bit is displayed during each call
;		  -LEDREGISTERBUFFER is reloaded with LEDREGISTER after 8 calls of DISPLAY()          
;         -D7..D4 LED'S show most significant nibble
;         -D3..D0  LED'S show least significant nibble
;         -MUST DECLARE LEDREGISTERBUFFER & LEDREGISTER AS GPR'S
;****************************************************************************
Display
	global	Display
	movf	STATE,w
	btfsc	STATUS,Z		  ;Are we at state 0?	  
	goto	BUFFERRESET		  ;Yes
	btfss	STATE,3			  ;No, did we pass the last state?
	goto	BUFFERLOADED	  ;No

BUFFERRESET					  ;Yes
	movf	LEDREGISTER,w
	movwf	LEDREGISTERBUFFER ;Reload with the current value in LEDREGISTER
	clrf	STATE			  ;Ensure State Zero
	
BUFFERLOADED
	bcf		STATUS,C		;Ensure Carry Is Clear
	rrf		LEDREGISTERBUFFER,f
	btfsc	STATUS,C		;Is The Next Bit A '1'
	goto	UpdateLed		;Yes, Update The LED Array
	goto	Continue		;No

UpdateLed	
	call	LEDStateMachine ;Update LED Array
		
Continue
	incf	STATE,f			;Point To Next State
	return
	
	END
