;Software License Agreement                                         
;                                                                    
;The software supplied herewith by Microchip Technology             
;Incorporated (the "Company") is intended and supplied to you, the  
;Company�s customer, for use solely and exclusively on Microchip    
;products. The software is owned by the Company and/or its supplier,
;and is protected under applicable copyright laws. All rights are   
;reserved. Any use in violation of the foregoing restrictions may   
;subject the user to criminal sanctions under applicable laws, as   
;well as to civil liability for the breach of the terms and         
;conditions of this license.                                        
;                                                                    
;THIS SOFTWARE IS PROVIDED IN AN "AS IS" CONDITION. NO WARRANTIES,  
;WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING, BUT NOT LIMITED  
;TO, IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A       
;PARTICULAR PURPOSE APPLY TO THIS SOFTWARE. THE COMPANY SHALL NOT,  
;IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL OR         
;CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.       
;****************************************************************************
;PICkit(tm) 1 Tutorial #5
;Interrupts
;
;Filename:		pglookup.asm
;Author:		David Otten
;Date:  		1.16.03
;Version:		1.0 (8 LED Version)
;Description:	This is a MPASM written program designed to show the user
;				a look-up table example in program memory.
;       This example uses an interrupt driven LED State Machine  to
;       display the look-up value and an interrupt on pin change to
;       increment the read index.
;****************************************************************************
;Revision History
;
;****************************************************************************
;
;****************************************************************************
;How To Flash LED's	
;****************************************************************************
;To Flash LED's simply write the 8-bit value into the RAM variable called LEDREGISTER
;i.e.	movlw	0xFF		;Flash 8 LED's D7-D0  (D7 is most significant bit)
;       movwf	LEDREGISTER                       (D0 is least significant bit)
;
;To Change The rate at which the LED's flash push button SW1
;****************************************************************************

	list      p=12F675        		; list directive to define processor
	#include <p12f675.inc>          ; processor specific variable definitions
	#include "pglookup.h"
	;errorlevel  -302               ; suppress message 302 from list file

	__CONFIG   _CP_OFF & _CPD_OFF & _BODEN_OFF & _MCLRE_OFF & _WDT_OFF & _PWRTE_ON & _INTRC_OSC_NOCLKOUT  

; '__CONFIG' directive is used to embed configuration word within .asm file.
; The lables following the directive are located in the respective .inc file.
; See data sheet for additional information on configuration word settings.

;****************************************************************************
;Defines
;****************************************************************************
#define	BANK1		banksel 0x80	;Select BANK1
#define	BANK0		banksel 0x00	;Select BANK0

;****************************************************************************
;General Purpose Registers (GPR's) 
;****************************************************************************
	UDATA_SHR
	
WTEMP		res 1		; register used in Interrupt Routine
STATUSTEMP	res 1		; register used in Interrupt Routine
PCLATHTEMP	res 1		; register used in Interrupt Routine
FSRTEMP		res 1		; register used in Interrupt Routine
index		res	1		; table index
	
;****************************************************************************
;Reset Vector
;****************************************************************************
	ORG     0x000             ; processor reset vector
	nop						  ; Inserted For ICD2 Use
	goto    Init              ; go to beginning of program

;****************************************************************************
;Interrupt Vector - Interrupt Sources Used:  1.  TIMER0 Overflow
;                                      		 2.  GP3 Pin-Change
;****************************************************************************
	ORG     0x004       	;Interrupt vector location
Isr
	movwf   WTEMP         	;Save off current W register contents
	movf	STATUS,w
	clrf	STATUS			;Force to page0
	movwf	STATUSTEMP									 
	movf	PCLATH,w
	movwf	PCLATHTEMP		;Save PCLATH
	movf	FSR,w
	movwf	FSRTEMP			;Save FSR
	BANK1					  
	
;****************************************************************************
;Interrupt Source Checks
;****************************************************************************
Timer0InterruptCheck
	movf	INTCON,w
	andlw	0x20		
	btfsc	STATUS,Z		;Is T0IE Set?
	goto	Next1			;No
	movf	INTCON,w		;Yes
	andlw	0x04	
	btfss	STATUS,Z		;Is TOIF Set?
	goto	Timer0Interrupt	;Yes
	
Next1
GPIFInterruptCheck
	movf	INTCON,w
	andlw	0x08		
	btfsc	STATUS,Z		;Is GPIE Set?
	goto	Next2			;No
	movf	INTCON,w		;Yes
	andlw	0x01	
	btfss	STATUS,Z		;Is GPIF Set?
	goto	GPIFInterrupt	;Yes

Next2
GP2_INT_ExternalInterruptCheck	
	movf	INTCON,w
	andlw	0x10		
	btfsc	STATUS,Z		;Is INTE Set?
	goto	Next3			;No
	movf	INTCON,w		;Yes
	andlw	0x02	
	btfss	STATUS,Z		;Is INTF Set?
	goto	GP2_INTExternalInterrupt;Yes
	
Next3
PeripheralInterruptCheck
	movf	INTCON,w
	andlw	0x40
	btfsc	STATUS,Z		;Is PEIE Set?
	goto	EndIsr			;No
	
Next4
EEIFInterruptCheck
	movf	PIE1,w
	andlw	0x80
	btfsc	STATUS,Z		;Is EEIE Set?
	goto	Next5			;No
	BANK0					;Yes
	movf	PIR1,w
	BANK1					
	andlw	0x80
	btfss	STATUS,Z		;Is EEIF Set?
	goto	EEPROMInterrupt;Yes

Next5
ADIFInterruptCheck
	movf	PIE1,w
	andlw	0x40
	btfsc	STATUS,Z		;Is ADIE Set?
	goto	Next6			;No
	BANK0					
	movf	PIR1,w
	BANK1					
	andlw	0x40
	btfss	STATUS,Z		;Is ADIF Set?
	goto	A_DConverterInterrupt;Yes	
	
Next6
CMIFInterruptCheck
	movf	PIE1,w
	andlw	0x08
	btfsc	STATUS,Z		;Is CMIE Set?
	goto	Next7			;No
	BANK0					;Yes
	movf	PIR1,w
	BANK1
	andlw	0x08
	btfss	STATUS,Z		;Is CMIF Set?
	goto	ComparatorInterrupt;Yes

Next7
TMR1IFInterruptCheck
	movf	PIE1,w
	andlw	0x01
	btfsc	STATUS,Z		;Is TMR1IE Set?
	goto	EndIsr			;No
	BANK0					;Yes
	movf	PIR1,w
	BANK1
	andlw	0x01
	btfss	STATUS,Z		;Is TMR1IF Set?
	goto	Timer1Interrupt	;Yes
	goto	EndIsr			;No

;****************************************************************************
;Interrupt Source Code
;****************************************************************************	
Timer0Interrupt
	BANK0					  
	call	Display		  	  ;Update LED Array
	BANK1					  
	bcf		INTCON,T0IF		  ;Clear TMR0 Interrupt Flag
	BANK0					  
	goto	EndIsr

GPIFInterrupt
	call	Debounce		  ;Debounces PushButton, Ends Mismatch Condition
	sublw	.1				  ;Is Switch Closed?	
	btfss	STATUS,Z					
	goto	EndGPIFInterrupt  ;No
	BANK1					  
	incf	index,f
	btfsc	index,3	          ;Is index = 8?
	clrf	index			  ;Yes, reset index
	movf	index,w			  ;Move index value to W for lookup
	call	LookupPM		  ;Fetch next table value
	movwf	LEDREGISTER		  ;Update LED register
EndGPIFInterrupt
	BANK0					  
	movf	GPIO,w		  	  ;Clears Mismatch Condition
	BANK1					  
	bcf		INTCON,GPIF       ;Clear Interrupt On Pin Change Flag
	goto	EndIsr
	
GP2_INTExternalInterrupt
	goto	EndIsr
	
EEPROMInterrupt	
	goto	EndIsr
	
A_DConverterInterrupt
	goto	EndIsr
	
ComparatorInterrupt
	goto	EndIsr
	
Timer1Interrupt
	
EndIsr
	clrf	STATUS			  ;Select Bank0
	movf	FSRTEMP,w
	movwf	FSR				  ;Restore FSR
	movf	PCLATHTEMP,w
	movwf	PCLATH			  ;Restore PCLATH
	movf	STATUSTEMP,w
	movwf	STATUS			  ;Restore STATUS
	swapf	WTEMP,f			  
	swapf	WTEMP,w			  ;Restore W without corrupting STATUS bits
	retfie                    ;Return from interrupt	

;****************************************************************************
;Initialization
;****************************************************************************
Init	
	;call    0x3FF      ; retrieve factory calibration value
						; comment instruction if using simulator, ICD2, or ICE2000
	BANK1					  
	movwf   OSCCAL      ; update register with factory cal value 
	call	InitLED		;Initialize LED Routine Variables
	BANK0
	clrf	GPIO		;Clear Port
	BANK1
	movlw	0xFF
	movwf	TRISIO		;Tri-State All Inputs
	clrf	VRCON		;Vref Off
	clrf	ANSEL		;A/D inputs to digital
	BANK0				
	movlw	0x07		
	movwf	CMCON		;Comparator Off
	BANK1				
	movlw	b'10000000'	
	movwf	OPTION_REG	;TIMER0 Prescaler
	bsf		IOCB,3		;Interrupt on Pin Change For GP3	
	bsf		INTCON,GPIE ;Interrupt on Change Enabled
	bsf		INTCON,T0IE ;Interrupt on TIMER0 Overflow Enabled
	bcf		INTCON,T0IF ;Clear TIMER0 Overflow Interrupt Flag
	bcf		INTCON,GPIF	;Clear Interrupt On Change Flag
	bsf		INTCON,GIE	;Turn on Global Interrupts
	BANK0				
	clrf	TMR0
	clrf	index			  ;Initialize index
	
;****************************************************************************
;MAIN - Main Routine
;****************************************************************************
Main
	movf	index,w			  ;Move index value to W for lookup
	call	LookupPM		  ;Fetch next table value
	movwf	LEDREGISTER		  ;Update LED register
Loop
	goto	Loop
	
;****************************************************************************
;Subroutines & Functions
;****************************************************************************
	
;****************************************************************************
;LookupPM	- Returns value from program memory
;****************************************************************************
LookupPM
	movlw	StartTable
	addwf	index,w
	btfsc	STATUS,C
	incf	PCLATH
	movwf	PCL				;Computed goto by adding offset to PCL
StartTable

	retlw	0x5F			;W = 0
	retlw	0x06			;W = 1
	retlw	0x3B			;W = 2
	retlw	0x2F			;W = 3
	retlw	0x66			;W = 4
	retlw	0x6D			;W = 5
	retlw	0x7D			;W = 6
	retlw	0x07			;W = 7

	END

