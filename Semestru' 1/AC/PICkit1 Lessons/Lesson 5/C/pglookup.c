/*
;Software License Agreement
;
;The software supplied herewith by Microchip Technology
;Incorporated (the "Company") is intended and supplied to you, the
;Company�s customer, for use solely and exclusively on Microchip
;products. The software is owned by the Company and/or its supplier,
;and is protected under applicable copyright laws. All rights are
;reserved. Any use in violation of the foregoing restrictions may
;subject the user to criminal sanctions under applicable laws, as
;well as to civil liability for the breach of the terms and
;conditions of this license.
;
;THIS SOFTWARE IS PROVIDED IN AN "AS IS" CONDITION. NO WARRANTIES,
;WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING, BUT NOT LIMITED
;TO, IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
;PARTICULAR PURPOSE APPLY TO THIS SOFTWARE. THE COMPANY SHALL NOT,
;IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL OR
;CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
;****************************************************************************
;PICkit(tm) 1 Tutorial #6
;Interrupts
;
;Filename:		pglookup.c
;Author:		David Otten
;Date:  		1.15.03
;Version:		1.0 (8 LED Version)
;Description:	This is a C written program designed to show the user
;				a look-up table example in program memory.
;       This example uses an interrupt driven LED State Machine  to
;       display the look-up value and an interrupt on pin change to
;       increment the read index.
;****************************************************************************
;Revision History
;
;****************************************************************************
;How To Flash LED's
;****************************************************************************
;To Flash LED's simply write the 8-bit value into the variable ledregister
;in main()
;
;i.e.  ledregister = 0xFF  Flash 8 LED's D7-D0  (D7 is most significant bit)
;						                        (D0 is least significant bit)
;To Change The rate at which the LED's flash push button SW1
;****************************************************************************
*/

#include "pglookup.h"

// Look-up table in program memory...

static const unsigned char digits[8] =
{
	0x5F,
	0x06,
	0x3B,
	0x2F,
	0x66,
	0x6D,
	0x7D,
	0x07,
};

//***************************************************************************
//Main() - Main Routine
//***************************************************************************
void main()
{
	Init();                              //Initialize 12F629 Microcontroller
	while(1)                             //Loop Forever
	{
	}
}

//***************************************************************************
//Init - Initialization Routine
//***************************************************************************
void Init()
{
	/* Comment out if using simulator, ICD2, or ICE2000
	#asm   
		call 0x3FF		//Load Factory Calibration Value Into OSCCAL
		bsf	_STATUS,5      //Select Bank1
		movwf   _OSCCAL
	#endasm
	*/

	InitLED();							 //Initialize LED variables
	ledregister=digits[0];	             //Flash 8 LED's
	TRISIO = 0xFF;                       //Set All I/O's As Inputs
	GPIO = CLEAR;                        //Clear GPIO
	VRCON = CLEAR;                       //Turn Off Voltage Reference Peripheral
	CMCON = 0x07;                        //Turn Off Comparator Peripheral
	TMR0 = CLEAR;                        //Clear Timer0
	OPTION = FASTFLASHING;               //Set Timer0 Prescaler To Fast Flash LED's
	IOCB3 = SET;                         //GP3 Interrupt On Pin Changed Enabled
	GPIE = SET;                          //Interrupt On Pin Change Enabled
	T0IE = SET;                          //Timer0 Overflow Interrupt Enabled
	T0IF = CLEAR;                        //Clear Timer0 Overflow Interrupt Flag
	ANSEL = CLEAR;						 //A/D inputs to digital
	GPIF = CLEAR;                        //Clear Interrupt On Pin Change Flag
	GIE = SET;                           //Enable All Interrupts
	return;
}

//***************************************************************************
//Functions
//***************************************************************************

//***************************************************************************
//Isr() - Interrupt Service Routine
//      - Timer0 Overflow & GP3 Pin Change Are Used
//***************************************************************************
void interrupt Isr()
{
	if ( (T0IE & T0IF) == SET)  	  //If A Timer0 Interrupt, Then
	{
		Display();					  //Update LED Array
		T0IF = CLEAR;                 //Clear Timer0 Interrupt Flag
	}

	else if ( (GPIE & GPIF) == SET)	  //If A GP3 Pin-Change Interrupt
	{
		if (Debounce() == TRUE)     		 //Debounce Pushbutton
			index++;
		if (index>7)
			index=CLEAR;
		LookupPM();
		GPIF = CLEAR;                    //Clear Interrupt On Pin Change Flag
	}
	else if ( (INTE & INTF) == SET)   //If A GP2/INT External Intrrupt
	{

	}
	else if ( PEIE == SET)			  //If Peripheral Interrupts Enabled, Check Peripheral
	{                                 //Interrupts

		if ( (EEIE & EEIF) == SET)	  //If A EEPROM Interrupt
		{

		}
		else if ( (ADIE & ADIF) == SET) //If A A/D Converter Interrupt
		{

		}

		else if ( (CMIE & CMIF) == SET) //If A Comparator Interrupt
		{

		}
		else if ( (TMR1IE & TMR1IF) == SET) //If A Timer1 Interrupt
		{

		}
	}
	return;
}

//***************************************************************************
//LookupPM	- Returns value from program memory
//***************************************************************************
int LookupPM()
{
	ledregister=digits[index];

	return 0;

}
