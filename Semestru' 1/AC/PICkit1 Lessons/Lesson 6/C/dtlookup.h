#include <pic.h>

__CONFIG(INTIO & WDTDIS & MCLRDIS & BORDIS & UNPROTECT & PWRTEN);

#define	SET		1
#define	CLEAR		0
#define	PUSHBUTTON	 	GPIO3		//GP3 - Pushbutton Input
#define	FASTFLASHING	0b10000010  //LED's Are Flashing Fast By Default
#define	SLOWFLASHING    0b10000101  //This Value Is Xored With FASTFLASHING In The Isr
#define	DEBOUNCEDELAY	3		    //Debounce Delay Constant
#define	OPEN			1
#define	CLOSED			0
#define	FIRSTSTATE		0
#define LASTSTATE		7
#define	TRUE			1
#define	FALSE			0

#define	LED1TRIS	0b11001111
#define	LED2TRIS	0b11001111
#define	LED3TRIS	0b11101011
#define	LED4TRIS	0b11101011
#define	LED5TRIS	0b11011011
#define	LED6TRIS	0b11011011
#define	LED7TRIS	0b11111001
#define	LED8TRIS	0b11111001
#define LED1ON		0b00010000
#define	LED2ON		0b00100000
#define	LED3ON		0b00010000
#define	LED4ON		0b00000100
#define	LED5ON		0b00100000
#define	LED6ON		0b00000100
#define	LED7ON		0b00000100
#define	LED8ON		0b00000010
#define	TMRPRESCALE	0b10000001  		//TMR0 will roll over every 1024 uS

//Global Variable Declarations
int	index;
bit	service_flag;

int LookupDEE();
int WriteDEE();
void Led1();
void Led2();
void Led3();
void Led4();
void Led5();
void Led6();
void Led7();
void Led8();
void LEDStateMachine();
void Delay(char value);

//Global Variable Declarations
extern unsigned char ledregister;

//Function Prototypes
void Init();
extern unsigned char Debounce();
extern void Display();
extern void InitLED();
