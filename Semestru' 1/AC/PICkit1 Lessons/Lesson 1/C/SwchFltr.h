#ifndef _SwchFltr
#define _SwchFltr

#include <pic.h>

__CONFIG(UNPROTECT & BOREN & MCLRDIS & PWRTEN & WDTDIS & INTIO);   

//Defines

#define BUTTON		GPIO3
#define D0_1Tris	0b11001111
#define D0On		0b00010000
#define D1On		0b00100000
#define MaxCount	16.	// the difference between MaxCount and MinCount
#define MinCount	0.		// determines the debounce filter time where
								// each count equals 1 Timer0 overflow

//Global Variable Declarations


//Function Prototypes

void Init(void);
void TestSwitch(void);

#endif
