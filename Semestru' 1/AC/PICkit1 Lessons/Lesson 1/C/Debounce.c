/*
;Software License Agreement                                         
;                                                                    
;The software supplied herewith by Microchip Technology             
;Incorporated (the "Company") is intended and supplied to you, the  
;Company�s customer, for use solely and exclusively on Microchip    
;products. The software is owned by the Company and/or its supplier,
;and is protected under applicable copyright laws. All rights are   
;reserved. Any use in violation of the foregoing restrictions may   
;subject the user to criminal sanctions under applicable laws, as   
;well as to civil liability for the breach of the terms and         
;conditions of this license.                                        
;                                                                    
;THIS SOFTWARE IS PROVIDED IN AN "AS IS" CONDITION. NO WARRANTIES,  
;WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING, BUT NOT LIMITED  
;TO, IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A       
;PARTICULAR PURPOSE APPLY TO THIS SOFTWARE. THE COMPANY SHALL NOT,  
;IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL OR         
;CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.       
;****************************************************************************
;File Name:		Debounce.c
;Author:			w.r.brown
;Date:  			7 January 2003
;Version:		1.01
;Description: 	Single pole, single throw switch debounce
;
;Modified:		1.01 1/27/03 w.r.brown 
;					Changed processor to PIC12F675. Added initialization 
;					code to change analog inputs to digital I/O.
;****************************************************************************
;Notes:
;****************************************************************************
;    This program demonstrates a simple brute force polled switch
;    debounce routine. Action is initiated immediately on switch 
;    closure. Further action is inhibited until the switch is    
;    released and fully debounced.                               
;
;    Each BUTTON push toggles LED D0.
;****************************************************************************
*/

#include "Debounce.h"


//***************************************************************************
//Main() - Main Routine
//***************************************************************************
void main(void)
{
	Init();
	while(1)
	{
		if (!BUTTON)		// sense BUTTON closure
		{
			ToggleLED();
			Debounce();
		}
	}
	
}

//***************************************************************************
//Init - Initialization Routine
//***************************************************************************
void Init(void)
{
	/* Comment out if using simulator, ICD2, or ICE2000
	#asm			
		call 0x3FF	    //Load Factory Calibration Value Into OSCCAL
				
		bsf _STATUS,5  //BANK1
		movwf _OSCCAL
	#endasm
	*/

	ANSEL = 0b00000000;	// configure A/D inputs as digital I/O
	TRISIO = D0_1Tris;	// GPIO 3,4 outputs, GPIO 0,1,2,5,6,7 inputs
	OPTION = 0b11010000;	// Timer0 internal clock, 1:2 prescale
	GPIO = D0On;
	CMCON = 0b00000000;	// configure comparator inputs as digital I/O
}

//***************************************************************************
//Functions
//***************************************************************************

//***************************************************************************
//ToggleLED - Toggles LED D0 on and off
//***************************************************************************
void ToggleLED(void)
{
	if(LEDOn)
	{
		GPIO = D0Off;
	}
	else
	{
		GPIO = D0On;
	}
}

//***************************************************************************
//Debounce - Debounces button switch 
//***************************************************************************
void Debounce(void)
{
	unsigned char COUNT = TenMS;

	TMR0 = 0;
	for(;;)
	{
		if(!BUTTON)
		{
			// reset 10 millisecond COUNT if BUTTON pushed
			COUNT = TenMS;
		}
		else
		{
			// decrement timer COUNT for every Timer0 overflow
			if(T0IF)
			{
				// reset flag
				T0IF = 0;
				// exit on 10 consecutive milliseconds without BUTTON closure
				if(--COUNT == 0) break;
			}
		}
		
	}
}
