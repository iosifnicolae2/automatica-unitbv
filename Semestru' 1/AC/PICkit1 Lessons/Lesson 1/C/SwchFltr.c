/*
****************************************************************************
;Notes
;****************************************************************************
;
;    LED D0 indica un comutator deschis, LED D1 indica un comutator �nchis
;    Pozitia comutatorului este monitorizata continuu.  Daca avem un comutator �nchis
;    counter-ul este incrementat, iar daca avem un comutator deschis counter-ul este decrementat . 
;    Daca  avem un counter care a depasit valoare admisa comutatorul se �nchide, iar daca avem un counter ;  ;    sub o anumita valoare comutatorul de deschide. 
;
;****************************************************************************
*/

#include "SwchFltr.h"

//***************************************************************************
//Main() - Main Routine
//***************************************************************************
void main(void)
{
	Init();
	while(1)
	{
		if(T0IF)
		{
			T0IF = 0;	// reset flag
			TestSwitch();
		}
	}
	
}

//***************************************************************************
//Init - Initialization Routine
//***************************************************************************
void Init(void)
{
	
	#asm   
		call 0x3FF	//Load Factory Calibration Value Into OSCCAL
		bsf _STATUS,5  //BANK1
		movwf _OSCCAL
	#endasm
	*/

	ANSEL = 0b00000000;	// configure A/D inputs as digital I/O
	TRISIO = D0_1Tris;		// GPIO 3,4 outputs, GPIO 0,1,2,5,6,7 inputs
	OPTION = 0b11010001;	// Timer0 internal clock, 1:4 prescale
	GPIO = D0On;			// initial state is switch open and D0 on
	CMCON = 0b00000000;	// configure comparator inputs as digital I/O
}

//***************************************************************************
//Functions
//***************************************************************************

//***************************************************************************
//TestSwitch � Testam comutatorul si actionam c�nd acesta a ajuns la valoarea maxima
//***************************************************************************
void TestSwitch(void)
{
	static unsigned char FilterCount=0.;
	
	if(BUTTON)
	{
		// comutatorul este deschis
		if(FilterCount > MinCount)
		{
			// aprind LED0 c�nd counterul a ajuns la MinCount
			if(--FilterCount<=MinCount)
			{
				GPIO = D0On;
			}
		}
	}
	else
	{
		// comutatorul este inchis
		if(FilterCount<MaxCount)
		{
			// aprind LED1 c�nd counterul a atins  MaxCount
			if(++FilterCount>=MaxCount)
			{
				GPIO = D1On;
			}
		}
	}
}
