#ifndef _Debounce
#define _Debounce

#include <pic.h>

__CONFIG(UNPROTECT & BOREN & MCLRDIS & PWRTEN & WDTDIS & INTIO);   

//Defines

#define BUTTON		GPIO3
#define D0_1Tris	0b11001111
#define D0On		0b00010000
#define D0Off		0b00000000
#define LEDOn		GPIO4==1

#define TenMS		20.         // number of Timer0 overflows with Fosc = 4MHz 
                              //  and 1:2 prescale

//Global Variable Declarations


//Function Prototypes

void Init(void);
void ToggleLED(void);
void Debounce(void);

#endif
