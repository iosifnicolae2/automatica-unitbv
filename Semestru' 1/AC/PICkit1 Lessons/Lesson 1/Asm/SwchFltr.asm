;Software License Agreement                                         
;                                                                    
;The software supplied herewith by Microchip Technology             
;Incorporated (the "Company") is intended and supplied to you, the  
;Company�s customer, for use solely and exclusively on Microchip    
;products. The software is owned by the Company and/or its supplier,
;and is protected under applicable copyright laws. All rights are   
;reserved. Any use in violation of the foregoing restrictions may   
;subject the user to criminal sanctions under applicable laws, as   
;well as to civil liability for the breach of the terms and         
;conditions of this license.                                        
;                                                                    
;THIS SOFTWARE IS PROVIDED IN AN "AS IS" CONDITION. NO WARRANTIES,  
;WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING, BUT NOT LIMITED  
;TO, IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A       
;PARTICULAR PURPOSE APPLY TO THIS SOFTWARE. THE COMPANY SHALL NOT,  
;IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL OR         
;CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.       
;******************************************************************************
;File Name:	SwchFltr.asm
;Author:	w.r.brown
;Date:  	6 January 2003
;Version:	1.01
;Description:   Time averaging filter of switch contact
;
;Modified:	1.01 1/27/03 w.r.brown 
;			Changed processor to PIC12F675. Added initialization 
;			code to change analog inputs to digital I/O.
;******************************************************************************
;Notes
;******************************************************************************
;
;    LED D0 indicates an open switch. LED D1 indicates a closed switch.
;
;    This demonstrates a simple filtered switch debounce routine.
;    The switch position is monitored continuously. If the switch is closed the 
;    filter count is incremented, and if the the switch is open the filter
;    count is decremented. When, but not until, the filter count overflows the
;    switch is determined to be closed. Likewise, when the filter count 
;    underflows the switch is determined to be open. Filter hysterisis is
;    inherent in the number of counts between the filter count overflow and
;    underflow.
;
;******************************************************************************

	list      p=12F675	; list directive to define processor
	#include <p12f675.inc>	; processor specific variable definitions

	__CONFIG  _CP_OFF & _WDT_OFF & _BODEN_ON & _PWRTE_ON & _INTRC_OSC_NOCLKOUT & _MCLRE_OFF & _CPD_OFF

; '__CONFIG' directive is used to embed configuration word within .asm file.
; The labels following the directive are located in the respective .inc file.
; See data sheet for additional information on configuration word settings.

;******************************************************************************
;Defines
;******************************************************************************

#define Bank0		0x00
#define	Bank1		0x80
#define switch		GPIO,3
#define D0_1Tris	B'11001111'
#define D7_8Tris	B'11011011'
#define D9_10Tris	B'11111001'
#define D0On		B'00010000'
#define D1On		B'00100000'

;******************************************************************************
;General Purpose Registers (GPR's) 
;******************************************************************************

	cblock	0x20
	FilterCount		; debounce filter counter
	endc

;******************************************************************************
;Reset Vector 
;******************************************************************************
	ORG     0x000		; processor reset vector
	nop			; required by in circuit debugger
	goto    Init		; go to beginning of program

;******************************************************************************
;Interrupt Vector     
;******************************************************************************
	ORG     0x004
	return			; interrupt trap - return without re-enabling 

;******************************************************************************
;Initialization
;******************************************************************************
Init
	;call    0x3FF      ; retrieve factory calibration value
						; comment instruction if using simulator, ICD2, or ICE2000
	BANKSEL	Bank1		; BANK1
	movwf   OSCCAL		; update register with factory cal value 
	movlw	D0_1Tris	; set direction bits so LEDs 3 and 4 are outputs
	movwf	TRISIO		; all others are inputs (high-z)
	movlw	B'11010001'	; Timer0 internal clock, 1:4 prescale
	movwf	OPTION_REG	; set option register for Timer0 functions
	clrf	ANSEL		; configure A/D I/O as digital
	banksel	Bank0		; switch back to PORT memory bank
	movlw	CM2 | CM1 | CM0 ; configure comparator inputs as digital I/O
	movwf	CMCON		;
	clrf	FilterCount	; set initial filter count
	call	LED0		; initial LED condition is switch open

;******************************************************************************
;Main 
;******************************************************************************
Main

; Wait for Timer0 overflow
; Timer0 overflows about every 1 millisecond with the internal 4 MHz clock
; and 1:4 prescale
TimerLoop
	btfss	INTCON,T0IF	; wait for Timer0 overflow
	goto	TimerLoop	;
	
	bcf	INTCON,T0IF	; clear Timer0 overflow flag
	call	TestSwitch	; test switch once every millisecond
	goto	Main

;******************************************************************************
;Subroutines & Functions
;******************************************************************************

;******************************************************************************
;TestSwitch - Test switch state and take action when filter count saturates
;******************************************************************************
TestSwitch
	btfss	switch		; test for switch closure
	goto	SwitchClosed

SwitchOpen
; When switch is open - decrement count if not already saturated
	movf	FilterCount,f	; move affects zero flag
	btfsc	STATUS,Z	; test filter count for zero
	return			; count was already zero - no further action

; drive LED0 on when switch is open and count saturates at zero	

	decfsz	FilterCount,f	; decrement filter count
	return			; not zero - return to monitor loop
	
	call	LED0		; light LED0
	return			; return to monitor loop
	
SwitchClosed
; When switch is closed - increment count if not already saturated
	btfsc	FilterCount,4	; 16 count saturation if bit 4 is high
	return			; already saturated - return to monitor loop

; drive LED1 when switch is closed and count saturates at 16
	
	incf	FilterCount,f	; count up when switch is closed
	btfsc	FilterCount,4	; 16 count saturation when bit 4 goes high
	call	LED1		; light LED1
	
	return			; return to monitor loop

;******************************************************************************
;LED0 - Drive I/O port to turn D0 on
;******************************************************************************
LED0
	movlw	D0On		; data to forward bias LED0 and reverse bias LED1
	movwf	GPIO		; send data to GPIO port
	return			; return to calling routine
	
;******************************************************************************
;LED1 - Drive I/O port to turn D1 on
;******************************************************************************
LED1
	movlw	D1On		; data to forward bias LED1 and reverse bias LED0
	movwf	GPIO		; send data to GPIO port
	return			; return to calling routine
	
	END
