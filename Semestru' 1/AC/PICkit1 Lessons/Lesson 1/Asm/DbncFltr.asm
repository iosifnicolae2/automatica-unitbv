;Software License Agreement                                         
;                                                                    
;The software supplied herewith by Microchip Technology             
;Incorporated (the "Company") is intended and supplied to you, the  
;Company�s customer, for use solely and exclusively on Microchip    
;products. The software is owned by the Company and/or its supplier,
;and is protected under applicable copyright laws. All rights are   
;reserved. Any use in violation of the foregoing restrictions may   
;subject the user to criminal sanctions under applicable laws, as   
;well as to civil liability for the breach of the terms and         
;conditions of this license.                                        
;                                                                    
;THIS SOFTWARE IS PROVIDED IN AN "AS IS" CONDITION. NO WARRANTIES,  
;WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING, BUT NOT LIMITED  
;TO, IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A       
;PARTICULAR PURPOSE APPLY TO THIS SOFTWARE. THE COMPANY SHALL NOT,  
;IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL OR         
;CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.       
;******************************************************************************
;File Name:		 DbncFltr.asm
;Author:		 w.r.brown
;Date:  		 10 January 2003
;Version:		 1.01
;Description:   Combined brute force and time averaging filter switch debounce
;
;Modified:	1.01 1/27/03 w.r.brown 
;			Changed processor to PIC12F675. Added initialization 
;			code to change analog inputs to digital I/O.
;******************************************************************************
;Notes
;******************************************************************************
;
;    Dual demo of switch debounce routine. Demo starts in the switch filter
;    debounce mode. Holding the switch down for 1 second changes the mode
;    to the brute force debounce mode. Modes are described as follows:
;
; Switch Filter Debounce Mode
;
;    LED D0 indicates an open switch. LED D1 indicates a closed switch.
;
;    This demonstrates a simple filtered switch debounce routine.
;    The switch position is monitored continuously. If the switch is closed the 
;    filter count is incremented, and if the the switch is open the filter
;    count is decremented. When, but not until, the filter count overflows the
;    switch is determined to be closed. Likewise, when the filter count 
;    underflows the switch is determined to be open. Filter hysterisis is
;    inherent in the number of counts between the filter count overflow and
;    underflow.
;
; Brute Force Debounce Mode
;
;    Each button push toggles LED D0.
;
;    This program demonstrates a simple brute force polled switch
;    debounce routine. Toggle action is initiated immediately on switch 
;    closure. Further action is inhibited until the switch is    
;    released and fully debounced.                               
;
;******************************************************************************

	list      p=12F675		 ; list directive to define processor
	#include <p12f675.inc>		 ; processor specific variable definitions

	__CONFIG  _CP_OFF & _WDT_OFF & _BODEN_ON & _PWRTE_ON & _INTRC_OSC_NOCLKOUT & _MCLRE_OFF & _CPD_OFF

; '__CONFIG' directive is used to embed configuration word within .asm file.
; The labels following the directive are located in the respective .inc file.
; See data sheet for additional information on configuration word settings.

;******************************************************************************
;Defines
;******************************************************************************

#define Bank0		0x00
#define	Bank1		0x80
#define SWITCH		GPIO,3
#define D0_1Tris	B'11001111'
#define D0On		B'00010000'
#define D1On		B'00100000'
#define LED0On		GPIO,4
#define OneSecond	D'976'	; number of Timer0 overflows in 1 sec
		 	 	; when Fosc = 4MHz and prescale = 1:4
#define ChangeMode	Flags,0	; request to change the debounce mode		 		 		 		 		 

;******************************************************************************
;General Purpose Registers (GPR's) 
;******************************************************************************

	cblock		 0x20
	FilterCount		; debounce filter counter
	OneSecH			; one second timer high byte
	OneSecL			; one second timer low byte
	Flags		 	; general purpose single bit flags
	CountH			; brute force debounce timer ms byte
	CountL			; brute force debounce timer ls byte
	endc

;******************************************************************************
;Reset Vector 
;******************************************************************************
	ORG     0x000		; processor reset vector
	nop		 	; required by in circuit debugger
	goto    Init		; go to beginning of program

;******************************************************************************
;Interrupt Vector     
;******************************************************************************
	ORG     0x004
	return		 	; interrupt trap - return without re-enabling 

;******************************************************************************
;Initialization
;******************************************************************************
Init
	;call    0x3FF      ; retrieve factory calibration value
						; comment instruction if using simulator, ICD2, or ICE2000
	BANKSEL Bank1		; BANK1
	movwf   OSCCAL		; update register with factory cal value 
	movlw	D0_1Tris	; set direction bits so LEDs 3 and 4 are outputs
	movwf	TRISIO		; all others are inputs (high-z)
	movlw	B'11010001'	; Timer0 internal clock, 1:4 prescale
	movwf	OPTION_REG	; set option register for Timer0 functions
	clrf	ANSEL		; configure A/D I/O as digital
	banksel Bank0		; switch back to PORT memory bank
	movlw	CM2 | CM1 | CM0 ; configure comparator inputs as digital I/O
	movwf	CMCON		;
	clrf	Flags		; clear flag register
	clrf	FilterCount	; set initial filter count
	call	LED0		; initial LED condition is switch open

;******************************************************************************
;Main 
;******************************************************************************
Main

; Wait for Timer0 overflow
; Timer0 overflows about every 1 millisecond with the internal 4 MHz clock
; and 1:4 prescale
TimerLoop
	btfss	INTCON,T0IF	; wait for Timer0 overflow
	goto	TimerLoop	;
	 
	bcf	INTCON,T0IF	; clear Timer0 overflow flag
	call	TestSwitch	; test switch once every millisecond
	btfsc	ChangeMode	; request to change debounce mode?
	goto	InitMain1	; yes - initialize and run brute force mode
	goto	Main

;******************************************************************************
;Subroutines & Functions
;******************************************************************************

;******************************************************************************
;TestSwitch - Test switch state and take action when filter count saturates
;******************************************************************************
TestSwitch
	btfss	SWITCH		; test for switch closure
	goto	SwitchClosed

SwitchOpen
; When switch is open - decrement count if not already saturated
	movf	FilterCount,f	; move affects zero flag
	btfsc	STATUS,Z	; test filter count for zero
	return		 	; count saturated - return to Main

; drive LED0 on when switch is open and count saturates at zero		 

	decfsz	FilterCount,f	; decrement filter count
	return		 	; not zero - return to Main
		 
	call	LED0		; light LED0
	return		 	; return to Main
	 
SwitchClosed
; When switch is closed - increment count if not already saturated
	btfsc	FilterCount,4	; 16 count saturation if bit 4 is high
	goto	TestHold	; already saturated - test button hold

; drive LED1 when switch is closed and count saturates at 16
		 
	incf	FilterCount,f	; count up when switch is closed
	btfss	FilterCount,4	; 16 count saturation when bit 4 goes high
	return		 	; return to Main
		 
	call	Reset1Second	; reload 1 second timer count
	call	LED1		; light LED1
	return		 	; return to monitor loop

ResetAndTest
	bcf	INTCON,T0IF	; reset Timer0 overflow flag
TestHold
; Increment negative 1 second counter and switch modes if it goes to zero

	incfsz	OneSecL,f	; 16 bit increment ls byte
	return		 	; return on no overflow
	incfsz	OneSecH,f	; 16 bit increment ms byte
	return		 	; return on no overflow

; one second of button holding has elapsed when both ls and ms bytes overflow		 

	bsf	ChangeMode	; button held for one second - change mode
	return
		 
;******************************************************************************
;Reset1Second - Set 1 second counter to full count
;******************************************************************************
Reset1Second

	movlw	HIGH (-OneSecond & 0xFFFF)	; most significant bits of
				; negative count
	movwf	OneSecH		; set ms register
	movlw	LOW -OneSecond	; least significant bits of negative count
	movwf	OneSecL		; set ls register
	return		 	; return to calling routine
	 
;******************************************************************************
;LED0 - Drive I/O port to turn D0 on
;******************************************************************************
LED0
	movlw	D0On		; data to forward bias LED0 and reverse bias LED1
	movwf	GPIO		; send data to GPIO port
	return		 	; return to calling routine
	 
;******************************************************************************
;LED1 - Drive I/O port to turn D1 on
;******************************************************************************
LED1
	movlw	D1On		; data to forward bias LED1 and reverse bias LED0
	movwf	GPIO		; send data to GPIO port
	return		 	; return to calling routine

;******************************************************************************
;Main1 - Brute force debounce demo
;******************************************************************************
InitMain1
	bcf	ChangeMode	; clear mode change flag
	call	LED0		; light D0 to start
	call	SwitchDebounce	; debounce switch
	btfsc	ChangeMode	; test if button held for 1 second
	goto	InitMain0	; yes - change to Mode0
Main1
	btfsc	SWITCH		; wait in loop until SWITCH closure is sensed
	goto	Main1		; SWITCH closure grounds input pin
		 
	call	ToggleLED	; SWITCH closure sensed - toggle LED
	call	SwitchDebounce	; wait for switch to release and settle
	btfss	ChangeMode	; test if button held for 1 second
	goto	Main1		; no - stay in this loop

InitMain0		 
	bcf	ChangeMode	; clear mode change flag
	clrf	FilterCount	; set initial filter count
	call	LED1		; initial LED condition is switch closed
	goto	Main		; change to Mode0

;******************************************************************************
;ToggleLED - Toggles D0 on and off
;******************************************************************************
ToggleLED
	btfss	LED0On		; test present LED0 condition
	goto	TurnLeD0On	; LED0 is presently off - go turn it on
		
TurnLeD0Off
	bcf	LED0On		; clear LED0 drive bit
	return			; return to calling routine
		 
TurnLeD0On
	bsf	LED0On		; set LED0 drive bit
	return		 	; return to calling routine

;******************************************************************************
;SwitchDebounce - Waits for switch to release and settle
;******************************************************************************
SwitchDebounce
; The SWITCH must be a steady high for 10 milliseconds to be considered  
; released and debounced. The debounce routine sets a 10 mSec timer and looks 
; for a high SWITCH input. The timer is reset to 10 mSec for every occurance of
; a low SWITCH input.
;
; The 10 mSec reset loop includes a 1 second count down timer that is updated
; at each occurance of a Timer0 overflow. If the SWITCH is held down for 1
; second then the 10 mSec loop is exited by return with the ChangeMode request 
; flag set.
;
; Debounce timer constants (TenMSH and TenMSL) are a function of the 
; instruction execution time and number of instructions in each loop. Debounce
; time for this example is:
; Timer overhead + TenMSH * [outer loop time + (TenMSL * inner loop time)]
; or [2 + TenMSH * (5 + (TenMSL * 5))]*(4/fosc)= .010
;
; Arbitrarily choosing 99 for TenMSL we solve for TenMSH
; 5 + TenMSH * (5 + (99 * 5)) = 10,000
; TenMSH = 9995/500 ~ 20

#define TenMSH 	D'20'
#define TenMSL	D'99'

	call	Reset1Second	; reset the 1 second timer count
	bcf	INTCON,T0IF	; reset Timer0 overflow flag
SD05
; outer loop overhead consists of the 4 instructions executed (5 instruction
; cycles) between here and SD10. Instructions executed in TestHold, plus
; the 2 instructions following the call to TestHold, are not included in the
; overhead because they are not normally executed. They are part of the SWITCH 
; hold timer loop in which timing is regulated by Timer0 overflows.

	movlw	TenMSH		; set outer timer loop count
	movwf	CountH		; 
	btfss	INTCON,T0IF	; test if Timer0 overflow
	goto	SD10		; no overflow - continue with timer loop

	call	ResetAndTest 	; test for 1 second button hold
	btfsc	ChangeMode	; has button been held for 1 second?
	return		 	; yes - exit debounce with flag set by TestHold

;========== outer loop [TenMSH * 5 instruction cycles*(Inner loop time)] ======
SD10
	movlw	TenMSL		; set inner timer loop
	movwf	CountL
;----------------- inner loop (TenMSL * 5 instruction cycles) -----------------
SD20
	btfss	SWITCH		; test SWITCH input
	goto	SD05		; SWITCH was low - reset timer
		 
	decfsz	CountL,f	; inner loop countdown
	goto	SD20		; test SWITCH while counting

;-------------------------------- inner loop ----------------------------------
		 
	decfsz	CountH,f	; outer loop countdown
	goto	SD10		; reset inner loop after each outer loop count

;================================ outer loop ==================================

	return		 	; full countdown and no bounces occured - exit

	END
