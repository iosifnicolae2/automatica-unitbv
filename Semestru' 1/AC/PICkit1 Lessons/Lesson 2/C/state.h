#include <pic.h>

__CONFIG(INTIO & WDTEN & MCLRDIS & BORDIS & UNPROTECT & PWRTEN);

// input and output definitions	
#define SW1				GPIO3			// toggle switch
#define TRIS_D0_D1	0b00001111	// TRISIO setting for D0 and D1
#define TRIS_D2_D3	0b00101011	// TRISIO setting for D2 and D3
#define TRIS_D4_D5	0b00011011	// TRISIO setting for D4 and D5
#define TRIS_D6_D7	0b00111001	// TRISIO setting for D6 and D7

//define LED state (what GPIO will equal)

#define D0_ON			0b00010000	// D0 LED
#define D1_ON			0b00100000	// D1 LED
#define D2_ON			0b00010000	// D2 LED
#define D3_ON			0b00000100	// D3 LED
#define D4_ON			0b00100000	// D4 LED
#define D5_ON			0b00000100	// D5 LED
#define D6_ON			0b00000100	// D6 LED
#define D7_ON			0b00000010 	// D7 LED

//Global Variable Declarations
unsigned char STATE_DEBOUNCE;

//Function Prototypes
void Init();
char ButtonPress();
