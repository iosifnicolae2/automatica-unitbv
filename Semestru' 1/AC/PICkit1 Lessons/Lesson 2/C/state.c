/******************************************************************************
;Software License Agreement                                         
;                                                                    
;The software supplied herewith by Microchip Technology             
;Incorporated (the "Company") is intended and supplied to you, the  
;Company�s customer, for use solely and exclusively on Microchip    
;products. The software is owned by the Company and/or its supplier,
;and is protected under applicable copyright laws. All rights are   
;reserved. Any use in violation of the foregoing restrictions may   
;subject the user to criminal sanctions under applicable laws, as   
;well as to civil liability for the breach of the terms and         
;conditions of this license.                                        
;                                                                    
;THIS SOFTWARE IS PROVIDED IN AN "AS IS" CONDITION. NO WARRANTIES,  
;WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING, BUT NOT LIMITED  
;TO, IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A       
;PARTICULAR PURPOSE APPLY TO THIS SOFTWARE. THE COMPANY SHALL NOT,  
;IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL OR         
;CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.       
;******************************************************************************
;Filename:		state.c
;Author:			Reston Condit
;Date:			1/15/03  	
;Version:		1.00
;Description:  	
;	This firmware implements a simple state machine.  There are eight states:
;		STATE1 = D0 LED on
;		STATE2 = D1 LED on
;		STATE3 = D2 LED on
;		STATE4 = D3 LED on
;		STATE5 = D4 LED on
;		STATE6 = D5 LED on
;		STATE7 = D6 LED on
;		STATE8 = D7 LED on
;
;	User's Note: Under Edit -> Properties... -> Tabs set tabs to 3
;******************************************************************************
;Revision History:
;	none
;******************************************************************************

;******************************************************************************
;Instructions On How To Use This Program
;******************************************************************************
;	Press Switch 1 (SW1) on the PICkit(tm) demonstration board to cycle through
;	the eight LED states.
;******************************************************************************
*/

#include "state.h"

//*****************************************************************************
//Main() - Main Routine
//*****************************************************************************
void main()
{
	unsigned char STATE_LED = 0;
	Init();									// call initialize routine
	while(1)
	{
		CLRWDT();
		if (ButtonPress()) {				// if a button press is detected,
			STATE_LED++;					//  increment the LED state variable
		}
		STATE_LED &= 0x07;				// mask state variable
		switch (STATE_LED) {
			case 0:							// STATE0: turn only the D0 LED on
				TRISIO = TRIS_D0_D1;
				GPIO = D0_ON;
				break;
			case 1:							// STATE1: turn only the D1 LED on
				TRISIO = TRIS_D0_D1;
				GPIO = D1_ON;
				break;
			case 2:							// STATE2: turn only the D2 LED on
				TRISIO = TRIS_D2_D3;
				GPIO = D2_ON;
				break;
			case 3:							// STATE3: turn only the D3 LED on
				TRISIO = TRIS_D2_D3;
				GPIO = D3_ON;
				break;
			case 4:							// STATE4: turn only the D4 LED on
				TRISIO = TRIS_D4_D5;
				GPIO = D4_ON;
				break;
			case 5:							// STATE5: turn only the D5 LED on
				TRISIO = TRIS_D4_D5;
				GPIO = D5_ON;
				break;
			case 6:							// STATE6: turn only the D6 LED on
				TRISIO = TRIS_D6_D7;
				GPIO = D6_ON;
				break;
			case 7:							// STATE7: turn only the D7 LED on
				TRISIO = TRIS_D6_D7;
				GPIO = D7_ON;
				break;
		}	
	}
}

//*****************************************************************************
//Functions
//*****************************************************************************

//*****************************************************************************
//Init - Initialization Routine
//*****************************************************************************
void Init()
{

/* Comment out if using simulator, ICD2, or ICE2000
#asm   
	call 	0x3FF					// Load Factory Calibration Value Into OSCCAL
	bsf 	_STATUS,5  			// BANK1
	movwf _OSCCAL	
#endasm
*/
	OPTION = 0b10000100;		// disable weak pullups, 1:32 prescaler on TMR0
									//  (TMR0 will overflow in 8.2ms)
	INTCON = 0;					// disable all interrupts, clear all flags
	GPIO = 0;					// clear all I/O pins
	TRISIO = 0b00111111;		// make all I/O pins inputs
	STATE_DEBOUNCE = 0;		// initialize debounce state machine
	return;
}

//*****************************************************************************
//ButtonPress() - Returns a 1 if a button press is detected and a 0 otherwise.
// This function implements a similar debounce routine as that in Tutorial #1.  
// The only difference is the debouce rountine is implemented as a state 
// machine instead.  
//*****************************************************************************
char ButtonPress()
{
	unsigned char temp = 0;
	switch (STATE_DEBOUNCE) {		// debounce state variable (SV)
		case 0:
			if (!SW1) {					// if switch is pushed increment the SV and
				STATE_DEBOUNCE++;		//  return a one
				temp = 1;
			}
			break;
		case 1:
			if (SW1) {					// if switch is released, initialize Timer0 
				TMR0 = 0;				//  in increment the SV
				T0IF = 0;
				STATE_DEBOUNCE++;		
			}
			break;
		case 2:
			if (T0IF) 					// if Timer0 overflows, reinitialize the 
				STATE_DEBOUNCE = 0;	//  debounce routine
			else if (!SW1)				// if switch is pressed before Timer0 over-
				STATE_DEBOUNCE++;		//  flows then decrement SV
			break;
		default:
			STATE_DEBOUNCE = 1;		// if for some reason the SV is corrupted
											//  goto state 1
	}										
	return (temp);
}

//***************************************************************************
//Interrupt Service Routine - Not implemented
//***************************************************************************
void interrupt Isr()
{
	return;
}
