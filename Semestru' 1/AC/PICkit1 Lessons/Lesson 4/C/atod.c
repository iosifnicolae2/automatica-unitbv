/*
;Software License Agreement                                         
;                                                                    
;The software supplied herewith by Microchip Technology             
;Incorporated (the "Company") is intended and supplied to you, the  
;Company�s customer, for use solely and exclusively on Microchip    
;products. The software is owned by the Company and/or its supplier,
;and is protected under applicable copyright laws. All rights are   
;reserved. Any use in violation of the foregoing restrictions may   
;subject the user to criminal sanctions under applicable laws, as   
;well as to civil liability for the breach of the terms and         
;conditions of this license.                                        
;                                                                    
;THIS SOFTWARE IS PROVIDED IN AN "AS IS" CONDITION. NO WARRANTIES,  
;WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING, BUT NOT LIMITED  
;TO, IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A       
;PARTICULAR PURPOSE APPLY TO THIS SOFTWARE. THE COMPANY SHALL NOT,  
;IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL OR         
;CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.       
;****************************************************************************
;PICkit(tm) 1 Tutorial #4
;Interrupts
;
;Filename:		atod.c
;Author:		Ruan Lourens
;Date:  		1.03.03
;Version:		1.0 (A to D Conversion)
;Description:  	This is a C written program designed to show the user  
;	       		a timer driven analog-to-digital conversion.It is based on the 
;				interrupt driven conversionLED State Machine used in 
;	       		tutorial #3.	     
;****************************************************************************
;Revision History
;****************************************************************************
*/

#include "atod.h"

//***************************************************************************
//Main() - Main Routine
//***************************************************************************
void main()
{
	Init();                              //Initialize 12F675 Microcontroller
	while(1)                             //Loop Forever
	{	
//		Place Code Here
	}
}

//***************************************************************************
//Init - Initialization Routine
//***************************************************************************
void Init()
{

/* Comment out if using simulator, ICD2, or ICE2000 
	#asm 								 //Load Factory Calibration Value Into OSCCAL
		call 0x3FF
		bsf	_STATUS,5				
		movwf _OSCCAL					
	#endasm
*/	
	InitLED();							 //Initialize LED variables
	TRISIO = 0xFF;                       //Set All I/O's As Inputs
	GPIO = CLEAR;                        //Clear GPIO
	VRCON = CLEAR;                       //Turn Off Voltage Reference Peripheral
	CMCON = 0x07;                        //Turn Off Comparator Peripheral
	ANSEL = 0b00010001;					 //Select AN0 
	ADCON0 = 0b00000001;				 //Configure A/D - Select AN0, Left justified & enables A/D
	TMR0 = CLEAR;                        //Clear Timer0
	OPTION = TMRPRESCALE;                //TMR0 will roll over every 1024 uS
	IOCB3 = SET;                         //GP3 Interrupt On Pin Changed Enabled
	T0IE = SET;                          //Timer0 Overflow Interrupt Enabled
	T0IF = CLEAR;                        //Clear Timer0 Overflow Interrupt Flag
	GIE = SET;                           //Enable All Interrupts
	return;
}

//***************************************************************************
//AtoD() 	- A to D Routine
//          	- Store the most significant 8 bits in the ledregister for displaying 
//		- Also starts the next A to D cycle
//***************************************************************************
void AtoD()
{	
	ledregister = ADRESH;
	GODONE = SET;
	return;
}

//***************************************************************************
//Isr() - Interrupt Service Routine
//      - Timer0 Overflow is used
//***************************************************************************
void interrupt Isr()
{
	if ( (T0IE & T0IF) == SET)  	  //If A Timer0 Interrupt, Then
	{
		if(atod_service_flag)				//Either service AtoD or Display functions
		{
			AtoD();					// Service A to D
		}
		else
		{			
			Display();						  //Update LED Array
		}
		atod_service_flag = atod_service_flag ^ 1;			// Toggle atod_service_flag
		T0IF = CLEAR;                     //Clear Timer0 Interrupt Flag
	}

	else if ( (GPIE & GPIF) == SET)	  //If A GP3 Pin-Change Interrupt
	{

	}
	else if ( (INTE & INTF) == SET)   //If A GP2/INT External Intrrupt
	{
		
	}
	else if ( PEIE == SET)			  //If Peripheral Interrupts Enabled, Check Peripheral
	{                                 //Interrupts
	
		if ( (EEIE & EEIF) == SET)	  //If A EEPROM Interrupt
		{
			
		}
		else if ( (ADIE & ADIF) == SET) //If A A/D Converter Interrupt
		{
			
		}
		
		else if ( (CMIE & CMIF) == SET) //If A Comparator Interrupt
		{
			
		}
		else if ( (TMR1IE & TMR1IF) == SET) //If A Timer1 Interrupt
		{
			
		}
	}	
	return;
}

