/*
;Software License Agreement                                         
;                                                                    
;The software supplied herewith by Microchip Technology             
;Incorporated (the "Company") is intended and supplied to you, the  
;Company�s customer, for use solely and exclusively on Microchip    
;products. The software is owned by the Company and/or its supplier,
;and is protected under applicable copyright laws. All rights are   
;reserved. Any use in violation of the foregoing restrictions may   
;subject the user to criminal sanctions under applicable laws, as   
;well as to civil liability for the breach of the terms and         
;conditions of this license.                                        
;                                                                    
;THIS SOFTWARE IS PROVIDED IN AN "AS IS" CONDITION. NO WARRANTIES,  
;WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING, BUT NOT LIMITED  
;TO, IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A       
;PARTICULAR PURPOSE APPLY TO THIS SOFTWARE. THE COMPANY SHALL NOT,  
;IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL OR         
;CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.       
;****************************************************************************
;Filename:		comp.c
;Author:		Ruan Lourens	
;Date:  		1.03.03
;Version:		1.0 (Comp Version)
;Description:  	This is a C written program designed to show the user a    
;	       		dedicated comparator function using the internal voltage 
;				referance. The LED3 is on when the comparator result is high.
;				The potentiometer RP1 serves as input to AN0
;****************************************************************************
 Revision History
;****************************************************************************

*/

#include <pic.h>
void Init();
__CONFIG(INTIO & WDTDIS & MCLRDIS & BORDIS & UNPROTECT & PWRTEN);


//***************************************************************************
//Main() - Main Routine
//***************************************************************************
void main()
{
	Init();                              //Initialize 12F629 Microcontroller
	while(1)                             //Loop Forever
	{	
//		Place Code Here
	}
}

//***************************************************************************
//Init - Initialization Routine
//***************************************************************************
void Init()
{

	
	
	GPIO = 0;                        	//Clear GPIO
	TRISIO = 0b11101011;                //Set All I/O's As Inputs
	ANSEL = 0b00000000;					//Configure to AN0 - Remove for 16F629
	VRCON = 0b10101100;                	//Used to configure Reference to Comparator
	CMCON = 0b00011101;                     //Used to configure Comparator
	GIE = 0;                           //Disable All Interrupts
	return;
}


