//Filename:	atod.h

#ifndef ATOD_H
#define ATOD_H

#include <pic.h>

__CONFIG(INTIO & WDTDIS & MCLRDIS & BORDIS & UNPROTECT & PWRTEN);

//Defines

#define	PUSHBUTTON	 	GPIO3		//GP3 - Pushbutton Input
#define	TMRPRESCALE	0b10000001  		//TMR0 will roll over every 1024 uS
#define	SET				1
#define	CLEAR			0
#define	TRUE			1
#define	FALSE			0

//Global Variable Declarations
extern unsigned char ledregister;
bit	 atod_service_flag;

//Function Prototypes
void Init();
extern unsigned char Debounce();
extern void Display();
extern void InitLED();
void AtoD();

#endif
