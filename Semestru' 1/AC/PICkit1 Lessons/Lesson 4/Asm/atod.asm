;Software License Agreement                                         
;                                                                    
;The software supplied herewith by Microchip Technology             
;Incorporated (the "Company") is intended and supplied to you, the  
;Company�s customer, for use solely and exclusively on Microchip    
;products. The software is owned by the Company and/or its supplier,
;and is protected under applicable copyright laws. All rights are   
;reserved. Any use in violation of the foregoing restrictions may   
;subject the user to criminal sanctions under applicable laws, as   
;well as to civil liability for the breach of the terms and         
;conditions of this license.                                        
;                                                                    
;THIS SOFTWARE IS PROVIDED IN AN "AS IS" CONDITION. NO WARRANTIES,  
;WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING, BUT NOT LIMITED  
;TO, IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A       
;PARTICULAR PURPOSE APPLY TO THIS SOFTWARE. THE COMPANY SHALL NOT,  
;IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL OR         
;CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.       
;****************************************************************************
;Filename:		atod.asm
;Author:		Ruan Lourens
;Date:  		1.03.03	
;Version:		1.0 (A/D Version)
;Description:  	This is an ASSEMBLY written program designed to show the user  
;	       		a timer driven analog-to-digital conversion.It is based on the 
;				interrupt driven conversionLED State Machine used in 
;	       		tutorial #3.	 
;****************************************************************************
;Revision History
;****************************************************************************


	list      p=12F675        		; list directive to define processor
	#include <p12f675.inc>          ; processor specific variable definitions
	#include "atod.h"
	errorlevel  -302               ; suppress message 302 from list file

	__CONFIG   _CP_OFF & _CPD_OFF & _BODEN_OFF & _MCLRE_OFF & _WDT_OFF & _PWRTE_ON & _INTRC_OSC_NOCLKOUT  

; '__CONFIG' directive is used to embed configuration word within .asm file.
; The lables following the directive are located in the respective .inc file.
; See data sheet for additional information on configuration word settings.

;****************************************************************************
;Defines
;****************************************************************************
#define	BANK1		banksel 0x80	;Select Bank1
#define	BANK0		banksel 0x00	;Select Bank0
#define	LED1TRIS	b'11001111'
#define	LED2TRIS	b'11001111'
#define	LED3TRIS	b'11101011'
#define	LED4TRIS	b'11101011'
#define	LED5TRIS	b'11011011'
#define	LED6TRIS	b'11011011'
#define	LED7TRIS	b'11111001'
#define	LED8TRIS	b'11111001'
#define LED1ON		b'00010000'
#define	LED2ON		b'00100000'
#define	LED3ON		b'00010000'
#define	LED4ON		b'00000100'
#define	LED5ON		b'00100000'
#define	LED6ON		b'00000100'
#define	LED7ON		b'00000100'
#define	LED8ON		b'00000010'
#define NUMBEROFBITS	.8
#define	ANSelect	b'00010001'		;Used to configure AD
#define	ADControl	b'00000001'		;Used to configure AD

;****************************************************************************
;General Purpose Registers (GPR's) 
;****************************************************************************
	UDATA_SHR
WTEMP		res 1			; register used in Interrupt Routine
STATUSTEMP	res 1			; register used in Interrupt Routine
PCLATHTEMP	res 1			; register used in Interrupt Routine
FSRTEMP		res 1			; register used in Interrupt Routine
FLAGS		res 1			; register used to set flags

;****************************************************************************
;Reset Vector
;****************************************************************************
	ORG     0x000             ; processor reset vector
	nop						  ; Inserted For ICD2 Use
	goto    Init              ; go to beginning of program

;****************************************************************************
;Interrupt Vector - Interrupts only active during animation sequence
;                 - Interrupt Sources:  1.  TIMER0 Overflow
;
;FLAGS register - bit0:  1 = A/D will be serviced, 0 = Display will be serviced
;
;****************************************************************************
	ORG     0x004             ; interrupt vector location
Isr
	movwf   WTEMP         	;Save off current W register contents
	movf	STATUS,w
	clrf	STATUS			;Force to page0
	movwf	STATUSTEMP									 
	movf	PCLATH,w
	movwf	PCLATHTEMP		;Save PCLATH
	movf	FSR,w
	movwf	FSRTEMP			;Save FSR
	BANK1					; BANK1	  
		 
	
;****************************************************************************
;Interrupt Source Checks
;****************************************************************************
Timer0InterruptCheck
	movf	INTCON,w
	andlw	0x20		
	btfsc	STATUS,Z		;Is T0IE Set?
	goto	Next1			;No
	movf	INTCON,w		;Yes
	andlw	0x04	
	btfss	STATUS,Z		;Is TOIF Set?
	goto	Timer0Interrupt	;Yes
	
Next1
GPIFInterruptCheck
	movf	INTCON,w
	andlw	0x08		
	btfsc	STATUS,Z		;Is GPIE Set?
	goto	Next2			;No
	movf	INTCON,w		;Yes
	andlw	0x01	
	btfss	STATUS,Z		;Is GPIF Set?
	goto	GPIFInterrupt	;Yes

Next2
GP2_INT_ExternalInterruptCheck	
	movf	INTCON,w
	andlw	0x10		
	btfsc	STATUS,Z		;Is INTE Set?
	goto	Next3			;No
	movf	INTCON,w		;Yes
	andlw	0x02	
	btfss	STATUS,Z		;Is INTF Set?
	goto	GP2_INTExternalInterrupt;Yes
	
Next3
PeripheralInterruptCheck
	movf	INTCON,w
	andlw	0x40
	btfsc	STATUS,Z		;Is PEIE Set?
	goto	EndIsr			;No
	
Next4
EEIFInterruptCheck
	movf	PIE1,w
	andlw	0x80
	btfsc	STATUS,Z		;Is EEIE Set?
	goto	Next5			;No
	BANK0					;Yes
	movf	PIR1,w
	BANK1					
	andlw	0x80
	btfss	STATUS,Z		;Is EEIF Set?
	goto	EEPROMInterrupt;Yes

Next5
ADIFInterruptCheck
	movf	PIE1,w
	andlw	0x40
	btfsc	STATUS,Z		;Is ADIE Set?
	goto	Next6			;No
	BANK0					
	movf	PIR1,w
	BANK1					
	andlw	0x40
	btfss	STATUS,Z		;Is ADIF Set?
	goto	A_DConverterInterrupt;Yes	
	
Next6
CMIFInterruptCheck
	movf	PIE1,w
	andlw	0x08
	btfsc	STATUS,Z		;Is CMIE Set?
	goto	Next7			;No
	BANK0					;Yes
	movf	PIR1,w
	BANK1
	andlw	0x08
	btfss	STATUS,Z		;Is CMIF Set?
	goto	ComparatorInterrupt;Yes

Next7
TMR1IFInterruptCheck
	movf	PIE1,w
	andlw	0x01
	btfsc	STATUS,Z		;Is TMR1IE Set?
	goto	EndIsr			;No
	BANK0					;Yes
	movf	PIR1,w
	BANK1
	andlw	0x01
	btfss	STATUS,Z		;Is TMR1IF Set?
	goto	Timer1Interrupt	;Yes
	goto	EndIsr			;No
	
Timer0Interrupt				;Interrupt every 1024 uS
	BANK0				;BANK0
	btfsc	FLAGS,0			;Check if A/D functions will be serviced or the display routine					
	call 	AD_Functions		;Yes, service A/D
	BANK0				;BANK0
	btfss	FLAGS,0			;Check if Display functions will be serviced
	call 	Display			;Yes, goto Display
	BANK0				;BANK0
	movlw	b'00000001'
	xorwf	FLAGS,F			;Toggle FLAGS,1
	BANK1				;BANK1
	bcf		INTCON,T0IF	;Clear TMR0 Interrupt Flag	
	goto	EndIsr

GPIFInterrupt
	goto	EndIsr

GP2_INTExternalInterrupt
	goto	EndIsr
	
EEPROMInterrupt	
	goto	EndIsr
	
A_DConverterInterrupt
	goto	EndIsr
	
ComparatorInterrupt
	goto	EndIsr
	
Timer1Interrupt
	
EndIsr
	clrf	STATUS			  ;Select Bank0
	movf	FSRTEMP,w
	movwf	FSR				  ;Restore FSR
	movf	PCLATHTEMP,w
	movwf	PCLATH			  ;Restore PCLATH
	movf	STATUSTEMP,w
	movwf	STATUS			  ;Restore STATUS
	swapf	WTEMP,f			  
	swapf	WTEMP,w			  ;Restore W without corrupting STATUS bits
	retfie                    ;Return from interrupt	


;****************************************************************************
;AD_Functions
;****************************************************************************
AD_Functions
	BANK0				;BANK0
	movf	ADRESH,W 		;Move the most significant byte of A/D Result to W
	movwf	LEDREGISTER		;The A/D result is moved to LEDREGISTER and will be displayed
	bsf	ADCON0,GO		;Start A/D
	return

;****************************************************************************
;Initialization
;****************************************************************************
Init
	;call    0x3FF      ; retrieve factory calibration value
						; comment instruction if using simulator, ICD2, or ICE2000
	BANK1					  
	movwf   OSCCAL      ; update register with factory cal value 
	call	InitLED		;Initialize LED Routine Variables
	movlw	0xFF
	movwf	TRISIO			;Tri-State All Inputs
	BANK0				;BANK 0
	clrf	GPIO			;Clear Port
	
	BANK1				;BANK 1
	clrf	VRCON			;Vref Off
	BANK0				;BANK 0
	clrf	TMR0
	movlw	0x07		
	movwf	CMCON			;Comparator Off

	BANK1					;BANK 1	
	movlw	b'10000001'	
	movwf	OPTION_REG		;TIMER0 Prescaler = 4 and pull-ups disabled
	bsf		INTCON,T0IE 	;Interrupt on TIMER0 Overflow Enabled
	bcf		INTCON,T0IF 	;Clear TIMER0 Overflow Interrupt Flag
	bsf		INTCON,GIE		;Turn on Global Interrupts
	movlw	ANSelect		
	movwf	ANSEL			;Configure AN0 & prescale to A/D	

	BANK0					;BANK 0
	movlw	ADControl		
	movwf	ADCON0			;Select AN0, Left justified & enables A/D
	NOP
	NOP
	NOP
	NOP						; Give 4 uS delay before starting A/D
	bsf	ADCON0,GO			; Start A/D
	
;****************************************************************************
;MAIN - Main Routine
;****************************************************************************
Main
;	Place code here
	goto	Main
	

	END

