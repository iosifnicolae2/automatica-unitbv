;Software License Agreement                                         
;                                                                    
;The software supplied herewith by Microchip Technology             
;Incorporated (the "Company") is intended and supplied to you, the  
;Company�s customer, for use solely and exclusively on Microchip    
;products. The software is owned by the Company and/or its supplier,
;and is protected under applicable copyright laws. All rights are   
;reserved. Any use in violation of the foregoing restrictions may   
;subject the user to criminal sanctions under applicable laws, as   
;well as to civil liability for the breach of the terms and         
;conditions of this license.                                        
;                                                                    
;THIS SOFTWARE IS PROVIDED IN AN "AS IS" CONDITION. NO WARRANTIES,  
;WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING, BUT NOT LIMITED  
;TO, IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A       
;PARTICULAR PURPOSE APPLY TO THIS SOFTWARE. THE COMPANY SHALL NOT,  
;IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL OR         
;CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.       
;****************************************************************************
;Filename:
;Author:	Ruan Lourens	
;Date:  	1.03.03
;Version:	1.0 (Comp Version)	
;Description: This is an ASSEMBLY written program designed to show the user a    
;	       	dedicated comparator function using the internal voltage 
;			referance. The LED3 is on when the comparator result is high.
;			The potentiometer RP1 serves as input to AN0
;****************************************************************************
; Revision History
;****************************************************************************

	list      p=12F675        		; list directive to define processor
	#include <p12f675.inc>          ; processor specific variable definitions

	errorlevel  -302               ; suppress message 302 from list file

	__CONFIG   _CP_OFF & _CPD_OFF & _BODEN_OFF & _MCLRE_OFF & _WDT_OFF & _PWRTE_ON & _INTRC_OSC_NOCLKOUT  

; '__CONFIG' directive is used to embed configuration word within .asm file.
; The lables following the directive are located in the respective .inc file.
; See data sheet for additional information on configuration word settings.

;****************************************************************************
;Defines
;****************************************************************************
#define	BANK1		banksel 0x80	;Select Bank1
#define	BANK0		banksel 0x00	;Select Bank0
#define	LEDTRIS		b'11101011'
#define	COMPCONTROL	b'00011101'		;Used to configure Comparator
#define	REFCONTROL	b'10101100'		;Used to configure Reference to Comparator
#define ANALOGSEL	b'00000000'		;Configure to AN0 - Remove for 16F629



;****************************************************************************
;Reset Vector
;****************************************************************************
	ORG     0x000             ; processor reset vector
	nop			  			  ; Inserted for ICD2 use
	goto    Init              ; go to beginning of program



;****************************************************************************
;Initialization
;****************************************************************************
Init

	BANK0				;BANK 0
	clrf	GPIO		;Clear Port

	BANK1				;BANK 1	
	bcf		INTCON,GIE	;Turn off Global Interrupts
	movlw	LEDTRIS
	movwf	TRISIO		;Set TRIS with GP2 as an output
	movlw	REFCONTROL
	movwf 	VRCON		;Enables internal reference at Vdd/2
	movlw 	ANALOGSEL
	movwf	ANSEL		;GPI/O as Analog - AN0


	BANK0				;BANK 0
	movlw	COMPCONTROL	
	movwf	CMCON		;Configure Comparator 	
	
;****************************************************************************
;MAIN - Main Routine
;****************************************************************************
Main
;	Place code here
	goto	Main
	END
