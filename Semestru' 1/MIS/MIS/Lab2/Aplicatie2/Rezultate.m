function varargout = Rezultate(varargin)
% REZULTATE M-file for Rezultate.fig
%      REZULTATE, by itself, creates a new REZULTATE or raises the existing
%      singleton*.
%
%      H = REZULTATE returns the handle to a new REZULTATE or the handle to
%      the existing singleton*.
%
%      REZULTATE('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in REZULTATE.M with the given input arguments.
%
%      REZULTATE('Property','Value',...) creates a new REZULTATE or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before Rezultate_OpeningFunction gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to Rezultate_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Copyright 2002-2003 The MathWorks, Inc.

% Edit the above text to modify the response to help Rezultate

% Last Modified by GUIDE v2.5 13-Sep-2011 20:31:28

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @Rezultate_OpeningFcn, ...
                   'gui_OutputFcn',  @Rezultate_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before Rezultate is made visible.
function Rezultate_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to Rezultate (see VARARGIN)

% Choose default command line output for Rezultate
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

set(handles.txtNumarDeEsantioane,'String',evalin('base','iN'));
set(handles.txtLambda,'String',evalin('base','dLambda'));
set(handles.txtM,'String',evalin('base','dM'));
set(handles.txtErMmean,'String',evalin('base','dErMmean'));
set(handles.txtErPmean,'String',evalin('base','dErPmean'));
set(handles.txtMaxMvar,'String',evalin('base','dMaxMvar'));
set(handles.txtMaxPvar,'String',evalin('base','dMaxPvar'));

% UIWAIT makes Rezultate wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = Rezultate_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;



function txtErMmean_Callback(hObject, eventdata, handles)
% hObject    handle to txtErMmean (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of txtErMmean as text
%        str2double(get(hObject,'String')) returns contents of txtErMmean as a double


% --- Executes during object creation, after setting all properties.
function txtErMmean_CreateFcn(hObject, eventdata, handles)
% hObject    handle to txtErMmean (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc
    set(hObject,'BackgroundColor','white');
else
    set(hObject,'BackgroundColor',get(0,'defaultUicontrolBackgroundColor'));
end



function txtErPmean_Callback(hObject, eventdata, handles)
% hObject    handle to txtErPmean (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of txtErPmean as text
%        str2double(get(hObject,'String')) returns contents of txtErPmean as a double


% --- Executes during object creation, after setting all properties.
function txtErPmean_CreateFcn(hObject, eventdata, handles)
% hObject    handle to txtErPmean (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc
    set(hObject,'BackgroundColor','white');
else
    set(hObject,'BackgroundColor',get(0,'defaultUicontrolBackgroundColor'));
end



function txtMaxMvar_Callback(hObject, eventdata, handles)
% hObject    handle to txtMaxMvar (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of txtMaxMvar as text
%        str2double(get(hObject,'String')) returns contents of txtMaxMvar as a double


% --- Executes during object creation, after setting all properties.
function txtMaxMvar_CreateFcn(hObject, eventdata, handles)
% hObject    handle to txtMaxMvar (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc
    set(hObject,'BackgroundColor','white');
else
    set(hObject,'BackgroundColor',get(0,'defaultUicontrolBackgroundColor'));
end



function txtMaxPvar_Callback(hObject, eventdata, handles)
% hObject    handle to txtMaxPvar (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of txtMaxPvar as text
%        str2double(get(hObject,'String')) returns contents of txtMaxPvar as a double


% --- Executes during object creation, after setting all properties.
function txtMaxPvar_CreateFcn(hObject, eventdata, handles)
% hObject    handle to txtMaxPvar (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc
    set(hObject,'BackgroundColor','white');
else
    set(hObject,'BackgroundColor',get(0,'defaultUicontrolBackgroundColor'));
end



function txtNumarDeEsantioane_Callback(hObject, eventdata, handles)
% hObject    handle to txtNumarDeEsantioane (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of txtNumarDeEsantioane as text
%        str2double(get(hObject,'String')) returns contents of txtNumarDeEsantioane as a double


% --- Executes during object creation, after setting all properties.
function txtNumarDeEsantioane_CreateFcn(hObject, eventdata, handles)
% hObject    handle to txtNumarDeEsantioane (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc
    set(hObject,'BackgroundColor','white');
else
    set(hObject,'BackgroundColor',get(0,'defaultUicontrolBackgroundColor'));
end



function txtLambda_Callback(hObject, eventdata, handles)
% hObject    handle to txtLambda (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of txtLambda as text
%        str2double(get(hObject,'String')) returns contents of txtLambda as a double


% --- Executes during object creation, after setting all properties.
function txtLambda_CreateFcn(hObject, eventdata, handles)
% hObject    handle to txtLambda (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc
    set(hObject,'BackgroundColor','white');
else
    set(hObject,'BackgroundColor',get(0,'defaultUicontrolBackgroundColor'));
end



function txtM_Callback(hObject, eventdata, handles)
% hObject    handle to txtM (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of txtM as text
%        str2double(get(hObject,'String')) returns contents of txtM as a double


% --- Executes during object creation, after setting all properties.
function txtM_CreateFcn(hObject, eventdata, handles)
% hObject    handle to txtM (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc
    set(hObject,'BackgroundColor','white');
else
    set(hObject,'BackgroundColor',get(0,'defaultUicontrolBackgroundColor'));
end


