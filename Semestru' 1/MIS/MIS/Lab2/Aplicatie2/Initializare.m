function Initializare
%ANALIZA SPECTRALA - INITIALIZARE 
%-----------------------------------------------------------------
%Functia ruleaza in rutina OpenFCN a figurii principale a aplicatiei
%si permite declararea variabilelor de program si definirea valorilor
%initiale ale acestora

% DEFINESTE SI SETEZA VALORILE VARIABILELOR DE PROGRAM
%-----------------------------------------------------------------
assignin('base','aAsistem',[1 -0.8]);%parametrul A al functiei de transfer a modelului simulat
assignin('base','aBsistem',[0 1]);%parametrul B al functiei de transfer a modelului simulat
assignin('base','iN',100);%numarul de esantioane ale semnalelor
assignin('base','dLambda',1);%ponderea zgomotului masuratorilor fata de semnalul de intrare
assignin('base','dM',50);%parametrul M al ferestrei Hamming
assignin('base','sOptiune','DiagrameBode');
%parametrii filtrului
figure(AnalizaSpectrala);