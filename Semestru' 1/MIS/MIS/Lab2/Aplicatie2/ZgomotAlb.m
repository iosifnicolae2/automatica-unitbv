function varargout = ZgomotAlb(varargin)
% ZGOMOTALB M-file for ZgomotAlb.fig
%      ZGOMOTALB, by itself, creates a new ZGOMOTALB or raises the existing
%      singleton*.
%
%      H = ZGOMOTALB returns the handle to a new ZGOMOTALB or the handle to
%      the existing singleton*.
%
%      ZGOMOTALB('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in ZGOMOTALB.M with the given input arguments.
%
%      ZGOMOTALB('Property','Value',...) creates a new ZGOMOTALB or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before ZgomotAlb_OpeningFunction gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to ZgomotAlb_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Copyright 2002-2003 The MathWorks, Inc.

% Edit the above text to modify the response to help ZgomotAlb

% Last Modified by GUIDE v2.5 13-Sep-2011 19:09:54

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @ZgomotAlb_OpeningFcn, ...
                   'gui_OutputFcn',  @ZgomotAlb_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before ZgomotAlb is made visible.
function ZgomotAlb_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to ZgomotAlb (see VARARGIN)

% Choose default command line output for ZgomotAlb
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

set(handles.txtN,'String',evalin('base','iN'));
set(handles.txtLambda,'String',evalin('base','dLambda'));

% UIWAIT makes ZgomotAlb wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = ZgomotAlb_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;



function txtN_Callback(hObject, eventdata, handles)
% hObject    handle to txtN (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of txtN as text
%        str2double(get(hObject,'String')) returns contents of txtN as a double


% --- Executes during object creation, after setting all properties.
function txtN_CreateFcn(hObject, eventdata, handles)
% hObject    handle to txtN (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc
    set(hObject,'BackgroundColor','white');
else
    set(hObject,'BackgroundColor',get(0,'defaultUicontrolBackgroundColor'));
end



function txtLambda_Callback(hObject, eventdata, handles)
% hObject    handle to txtLambda (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of txtLambda as text
%        str2double(get(hObject,'String')) returns contents of txtLambda as a double


% --- Executes during object creation, after setting all properties.
function txtLambda_CreateFcn(hObject, eventdata, handles)
% hObject    handle to txtLambda (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc
    set(hObject,'BackgroundColor','white');
else
    set(hObject,'BackgroundColor',get(0,'defaultUicontrolBackgroundColor'));
end


% --- Executes on button press in txtAnuleaza.
function txtAnuleaza_Callback(hObject, eventdata, handles)
% hObject    handle to txtAnuleaza (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in txtSalveaza.
function txtSalveaza_Callback(hObject, eventdata, handles)
% hObject    handle to txtSalveaza (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
assignin('base','iN',str2num(get(handles.txtN,'String')));
assignin('base','dLambda',str2num(get(handles.txtLambda,'String')));
close('ZgomotAlb');
figure(AnalizaSpectrala);


