function varargout = SistemSimulat(varargin)
% SISTEMSIMULAT M-file for SistemSimulat.fig
%      SISTEMSIMULAT, by itself, creates a new SISTEMSIMULAT or raises the existing
%      singleton*.
%
%      H = SISTEMSIMULAT returns the handle to a new SISTEMSIMULAT or the handle to
%      the existing singleton*.
%
%      SISTEMSIMULAT('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in SISTEMSIMULAT.M with the given input arguments.
%
%      SISTEMSIMULAT('Property','Value',...) creates a new SISTEMSIMULAT or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before SistemSimulat_OpeningFunction gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to SistemSimulat_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Copyright 2002-2003 The MathWorks, Inc.

% Edit the above text to modify the response to help SistemSimulat

% Last Modified by GUIDE v2.5 12-Sep-2011 19:02:22

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @SistemSimulat_OpeningFcn, ...
                   'gui_OutputFcn',  @SistemSimulat_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before SistemSimulat is made visible.
function SistemSimulat_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to SistemSimulat (see VARARGIN)

% Choose default command line output for SistemSimulat
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

aA=evalin('base','aAsistem');
aB=evalin('base','aBsistem');
set(handles.txtAsistem,'String',num2str(aA));
set(handles.txtBsistem,'String',num2str(aB));
% UIWAIT makes SistemSimulat wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = SistemSimulat_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;



function txtAsistem_Callback(hObject, eventdata, handles)
% hObject    handle to txtAsistem (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of txtAsistem as text
%        str2double(get(hObject,'String')) returns contents of txtAsistem as a double


% --- Executes during object creation, after setting all properties.
function txtAsistem_CreateFcn(hObject, eventdata, handles)
% hObject    handle to txtAsistem (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc
    set(hObject,'BackgroundColor','white');
else
    set(hObject,'BackgroundColor',get(0,'defaultUicontrolBackgroundColor'));
end



function txtBsistem_Callback(hObject, eventdata, handles)
% hObject    handle to txtBsistem (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of txtBsistem as text
%        str2double(get(hObject,'String')) returns contents of txtBsistem as a double


% --- Executes during object creation, after setting all properties.
function txtBsistem_CreateFcn(hObject, eventdata, handles)
% hObject    handle to txtBsistem (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc
    set(hObject,'BackgroundColor','white');
else
    set(hObject,'BackgroundColor',get(0,'defaultUicontrolBackgroundColor'));
end


% --- Executes on button press in cmdAnuleaza.
function cmdAnuleaza_Callback(hObject, eventdata, handles)
% hObject    handle to cmdAnuleaza (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%REFACE VECHILE VALORI ALE VARIABILELOR
%--------------------------------------------------------------------------
%set(evalin(handles.txtAsistem),'String',num2str(evalin('base','aAsistem')));%preia in caseta de text vechea valoare a variabilei
%set(evalin(handles.txtBsistem),'String',num2str(evalin('base','aBsistem')));%preia in caseta de text vechea valoare a variabilei

% --- Executes on button press in cmdSalveaza.
function cmdSalveaza_Callback(hObject, eventdata, handles)
% hObject    handle to cmdSalveaza (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%ACTUALIZEAZA VALORILE VARIABILELOR 
%--------------------------------------------------------------------------
aAsistem=str2num(get(handles.txtAsistem,'String'));
aBsistem=str2num(get(handles.txtBsistem,'String'));
assignin('base','aAsistem',aAsistem);%salveaza continutul casetei de text
assignin('base','aBsistem',aBsistem);%salveaza continutul casetei de text
figure(AnalizaSpectrala);