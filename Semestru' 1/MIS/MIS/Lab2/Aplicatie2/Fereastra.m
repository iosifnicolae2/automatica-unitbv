function varargout = Fereastra(varargin)
% FEREASTRA M-file for Fereastra.fig
%      FEREASTRA, by itself, creates a new FEREASTRA or raises the existing
%      singleton*.
%
%      H = FEREASTRA returns the handle to a new FEREASTRA or the handle to
%      the existing singleton*.
%
%      FEREASTRA('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in FEREASTRA.M with the given input arguments.
%
%      FEREASTRA('Property','Value',...) creates a new FEREASTRA or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before Fereastra_OpeningFunction gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to Fereastra_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Copyright 2002-2003 The MathWorks, Inc.

% Edit the above text to modify the response to help Fereastra

% Last Modified by GUIDE v2.5 13-Sep-2011 14:26:15

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @Fereastra_OpeningFcn, ...
                   'gui_OutputFcn',  @Fereastra_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before Fereastra is made visible.
function Fereastra_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to Fereastra (see VARARGIN)

% Choose default command line output for Fereastra
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

set(handles.txtM,'String',evalin('base','dM'));

% UIWAIT makes Fereastra wait for user response (see UIRESUME)
% uiwait(handles.Fereastra);

% --- Outputs from this function are returned to the command line.
function varargout = Fereastra_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;



function txtM_Callback(hObject, eventdata, handles)
% hObject    handle to txtM (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of txtM as text
%        str2double(get(hObject,'String')) returns contents of txtM as a double


% --- Executes during object creation, after setting all properties.
function txtM_CreateFcn(hObject, eventdata, handles)
% hObject    handle to txtM (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc
    set(hObject,'BackgroundColor','white');
else
    set(hObject,'BackgroundColor',get(0,'defaultUicontrolBackgroundColor'));
end


% --- Executes on button press in cmdAnuleaza.
function cmdAnuleaza_Callback(hObject, eventdata, handles)
% hObject    handle to cmdAnuleaza (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in cmdSalveaza.
function cmdSalveaza_Callback(hObject, eventdata, handles)
% hObject    handle to cmdSalveaza (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
assignin('base','dM',str2num(get(handles.txtM,'String')));
close('Fereastra');
figure(AnalizaSpectrala);
