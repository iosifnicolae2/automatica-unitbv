function Calcule()
%-----------------------------------------------------------------
%RUTINA PRINCIPALA A APLICATIEI
%-----------------------------------------------------------------

%PREIA VALORILE VARIABILELOR DE PROGRAM
%-----------------------------------------------------------------
aA=evalin('base','aAsistem');%preia valoarea variabilei aA in variabila locala
aB=evalin('base','aBsistem');%preia valoarea variabilei aB in variabila locala
iN=evalin('base','iN');%preia valoarea variabilei iN in variabila locala
dM=evalin('base','dM');%preia valoarea variabilei dM, a ferestrei Hamming in variabila locala
dLambda=evalin('base','dLambda');%preia valoarea variabilei dLambda in variabila locala

%GENEREAZA SEMNALELE DE INTRARE/IESIRE
%-----------------------------------------------------------------
aU=sign(randn(iN,100));%semnalul de intrare: o matrice N x 100, zgomot alb, distributie normala
aE=dLambda*randn(iN,100);%zgomotul datorat masuratorilor: o matrice  N x 100, zgomot alb, distributie normala
aY=filter(aB,aA,aU)+filter(1,aA,aE);%calculeaza semnalul de iesire filtrat
aW=logspace(-2,pi);%geneeaza un vector de 50 de puncte logaritmic egal departate intre 10^-2 si Pi
for k1=1:100
    aZ=[aY(:,k1),aU(:,k1)];%genereaza matricea compusa a semnalelor
    gest=spa(aZ,dM,aW);%calculeaza ftd cu metoda analizei spectrale
    [aMag, aPhase]=bode(gest);%extrage matricele modulelor si argumentelor din diagrama Bode
    aMest(k1,:)=aMag(1,1,:);%matricea modulelor fdt pe diagrama Bode
    aPest(k1,:)=aPhase(1,1,:);%matricea argumentelor fdt pe diagrama Bode
end
aMmean=mean(aMest,1)';%media valorilor modulului fdt 
aPmean=mean(aPest,1)';%media valorilor argumentului fdt
aMvar=std(aMest,1)';%dispersia valorilor modulului fdt
aPvar=std(aPest,1)';%dispersia valorilor argumentului fdt
[aMtrue, aPtrue]=dbode(aB,aA,1,aW);%extrage matricele modulelor si argumentelor din diagrama Bode a sistemului real 
dErMmean=abs(mean(aMtrue-aMmean));%
dErPmean=abs(mean(aPtrue-aPmean));%
dMaxMvar=max(aMvar);%
dMaxPvar=max(aPvar);%
%SALVEAZA REZULTATELE CALCULELOR
%-----------------------------------------------------------------
assignin('base','aU',aU);%matricea valorilor semnalului de intrare
assignin('base','aE',aE);%matricea valorilor zgomotului masuratorilor
assignin('base','aY',aY);%matricea valorilor semnalului de iesire
assignin('base','aW',aW);%matricea valorilor spatiului logaritmic echidistant
assignin('base','aMmean',aMmean);%media valorilor modulului fdt 
assignin('base','aPmean',aPmean);%media valorilor argumentului fdt
assignin('base','aMvar',aMvar);%dispersia valorilor modulului fdt
assignin('base','aPvar',aPvar);%dispersia valorilor argumentului fdt
assignin('base','aMtrue',aMtrue);%modulul fdt a sistemului real
assignin('base','aPtrue',aPtrue);%argumentul fdt a sistemului real
assignin('base','dErMmean',dErMmean);%
assignin('base','dErPmean',dErPmean);%
assignin('base','dMaxMvar',dMaxMvar);%
assignin('base','dMaxPvar',dMaxPvar);%
% Grafice;%apeleaza modulul pentru reprezentarea grafica si actualizarea controalelor
% 

%REPREZINTA GRAFIC MARIMILE MASURATE/CALCULATE
%-----------------------------------------------------------------
%figure(maner_figAnalizaSpectrala);
% axes(maner_axesGrafic1);%seteaza axesGrafic1 ca axe curente
%     loglog(aW,[aMtrue,aMmean]),grid,hold on
%     loglog(aW,[aMmean+aMvar,aMmean-aMvar],'r--'),hold off;%reprezinta pe acelasi grafic 4 grafice
% 
% 	xlabel('Pulsatia'),ylabel ('Modulul functiei de frecventa');
%     txt=['ANALIZA SPECTRALA, M = ',num2str(dM)];
%     title(txt);
%     
% axes(maner_axesGrafic2);%seteaza axesGrafic2 ca axe curente
%     semilogx(aW,[aPtrue,aPmean]), hold on
%     semilogx(aW,[aPmean+aPvar, aPmean-aPvar],'r--'),hold off;%reprezinta pe acelasi grafic 4 grafice
%     grid
%     xlabel('Pulsatia'),ylabel('Argumentul functiei de frecventa');
% 
