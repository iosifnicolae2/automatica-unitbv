unit veronica;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls;

type
  TForm1= class(TForm)
    Image1: TImage;
    Timer1: TTimer;
procedure timer1timer(sender:tobject);

  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;
  faza:integer=0;
  a:integer=0;
  b:integer=0;
  c:integer=0;
  d:integer=0;
  e:integer=0;
  f:integer=0;
  g:integer=0;

  t,x1,y1:integer;
  directie:boolean=true;
  const
  r=100;
  tmax=10;
  x0=200;
  y0=300;


implementation
procedure tform1.timer1timer(sender:tobject);
begin
with form1.image1.Canvas do
begin
//desenarea mesei
pen.Color:=clsilver;
pen.Width:=3;
brush.color:=clolive;
rectangle(0,400,700,500);
brush.color:=clsilver;
rectangle(0,410,700,490);
brush.color:=clolive;
rectangle(0,500,700,600);
pen.Color:=clolive;
polygon([point(20+a,410),point(10+a,490)]);
polygon([point(60+a,410),point(50+a,490)]);
polygon([point(100+a,410),point(90+a,490)]);
polygon([point(140+a,410),point(130+a,490)]);
polygon([point(180+a,410),point(170+a,490)]);
polygon([point(220+a,410),point(210+a,490)]);
polygon([point(260+a,410),point(250+a,490)]);
polygon([point(300+a,410),point(290+a,490)]);
polygon([point(340+a,410),point(330+a,490)]);
polygon([point(380+a,410),point(370+a,490)]);
polygon([point(420+a,410),point(410+a,490)]);
polygon([point(460+a,410),point(450+a,490)]);
polygon([point(500+a,410),point(490+a,490)]);
polygon([point(540+a,410),point(530+a,490)]);
polygon([point(580+a,410),point(570+a,490)]);
polygon([point(620+a,410),point(610+a,490)]);
polygon([point(660+a,410),point(650+a,490)]);
polygon([point(695+a,410),point(690+a,490)]);
//miscarea liniilor mesei
 case faza of
   0:
   begin
   a:=a+1;
   if(a>670) then
   faza:=1;
   end;
   end;
    case faza of
   1:
   begin
   b:=b+1;
   if(b>670) then
   faza:=2;
   end;
   end; case faza of
   2:
   begin
   c:=c+1;
   if(c>670) then
   faza:=3;
   end;
   end;
   case faza of
   3:
   begin
   d:=d+1;
   if(d>670) then
   faza:=4;
   end;
   end;
   case faza of
   4:
   begin
   e:=e+1;
   if(e>670) then
   faza:=5;
   end;
   end;
   case faza of
   5:
   begin
   f:=f+1;
   if(f>670) then
   faza:=0;
   end;
   end;
    case faza of
   6:
   begin
   g:=g+1;
   if(g>670) then
   faza:=0;
   end;
   end;
//dresenarea rotii
pen.color:=clblack;
pen.Width:=3;
brush.color:=clnavy;
ellipse(200-100,300-100,200+100,300+100);
brush.color:=clsilver;
ellipse(200-80,300-80,200+80,300+80);
brush.color:=clnavy;
ellipse(200-3,300-3,200+3,300+3);

//miscarea rotii

x1:=round(x0+r*cos(t/50));
y1:=round(y0-r*sin(t/50));
moveto(x0,y0);
lineto(x1,y1);
if(t=0)or (t=tmax) then
directie:=not directie;
if directie then
t:=t+1
else
t:=t-1;


// desen vas mine
pen.color:=clblack;
pen.width:=7;
brush.color:=clblack;
polygon([point(115,250),point(80,120),point(280,120)]);
brush.color:=clsilver;
polygon([point(80,120),point(100,100),point(280,100),point(280,120)]);

// desen brat-robot 1
pen.color:=clnavy;
pen.Width:=2;
brush.color:=clnavy;
rectangle(450,300,470,400);
rectangle(400,250,500,300);
rectangle(420,300,430,320);
ellipse(425+25,330+10,425-25,330-10) ;

// desen brat-robot 2
pen.color:=clnavy;
pen.Width:=2;
brush.Color:=clnavy;
rectangle(650,300,670,400);
rectangle(700,250,600,300);
rectangle(620,300,630,320);
rectangle(600,320,648,360);

//desenare cuva
pen.Color:=clblack;
pen.width:=7;
brush.color:=clblack;
rectangle(810,525,960,600);
pen.color:=clblack;
pen.Width:=7;
brush.color:=clsilver ;
polygon([point(810,523),point(850,450),point(930,450),point(960,525)]);


// desen seperator creioane
pen.color:=clnavy;
pen.Width:=2;
brush.Color:=clnavy;
rectangle(700,490,800,600);
polygon([point(700,490),point(800,490),point(850,520),point(860,510),point(790,470),point(700,470)]);
polygon([point(700,470),point(700,350),point(790,350),point(880,460),point(860,510)]);
brush.color:=clsilver;
polygon([point(700,350),point(685,410),point(700,410)]);
brush.color:=clnavy;
polygon([point(700,450),point(685,490),point(700,490)]);

//desen piesa banda
pen.Color:=clgray;
pen.Width:=2;
brush.color:=clcream;
rectangle(20,415,60,475);
rectangle(20,475,60,470);

//desen canale piesa banda
pen.Width:=1;
polygon([point(23,415),point(23,470)]);
polygon([point(25,415),point(25,470)]);
polygon([point(29,415),point(29,470)]);
polygon([point(31,415),point(31,470)]);
polygon([point(35,415),point(35,470)]);
polygon([point(37,415),point(37,470)]);
polygon([point(41,415),point(41,470)]);
polygon([point(43,415),point(43,470)]);
polygon([point(47,415),point(47,470)]);
polygon([point(49,415),point(49,470)]);
polygon([point(53,415),point(53,470)]);
polygon([point(55,415),point(55,470)]);

//desen piesa stiva
pen.Color:=clgray;
pen.Width:=2;
brush.color:=clcream;
rectangle(330,333,370,393);
rectangle(330,393,370,397);

{$R *.DFM}

end;
end;
end.

