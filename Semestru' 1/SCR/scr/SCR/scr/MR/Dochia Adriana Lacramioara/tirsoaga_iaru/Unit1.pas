unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls;

type
  TForm1 = class(TForm)
    Timer1: TTimer;
    Image1: TImage;
  procedure Timer1Timer(Sender:TObject);
    procedure Edit1Change(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;
  t,x1,y1,x2,y2,xd,yd,x22,b1,y22,x33,y33,x44,y44,xb,yb,xl0,yl0,xl1,yl1,xm1,ym1,xl2,yl2,x11,y11,x12,xm,ym,y12,xx,yy,xs1,xs2,ys1,ys2,xss1,xss2,yss1,yss2:integer;
  directie: boolean=false;
  dx1:integer=3;
  dy1:integer=5;
  x0:integer=145;
  y0:integer=390;
  xs:integer=150;
  ys:integer=480;
  xss:integer=150;
  yss:integer=300;
  i:integer=30;
  k:integer=0; 
  increment,decrement:integer;
  a:integer=0;
  faza:integer=0;
  b:integer=0;
  c:integer=0;
  d:integer=0;
     e:integer=0 ;
            f:integer=0;
                 g:integer=0;
  directiel: boolean=true;


const
     tl=70;
     razal=30;
     tlmax=20;
  tmax = 60;
  raza = 20;
  razas= 10;

implementation

  procedure TForm1.Timer1Timer(Sender:TObject);
    begin
      With form1.Image1.Canvas do
      begin
           //fillrect(clientrect);
        if(i<-30) then i:=30;
        fillrect(clientrect);

        if directie then t:=t+1
                    else t:=t-1;

        if(t=tmax) or (t=0)
                   then directie:=not directie;

        i:=i-1;
        pen.width:=4;

           {if(t>0)and(t<=100) then
        inc(dx1);
        moveto(x0,y0);
        x11:=x0+dx1;
        y11:=y0;

          if(t>100) and (t<=200) then
        dec(dy1);
        moveto(x11,y11);
        x12:=x11;
        y12:=y11-dy1;
                          }

        brush.color:=rgb(100,149,237);
        pen.color:=rgb(1,1,1);
        ellipse(280,508,70,608);
        ellipse(281,500,71,600);


        x1:=round(x0+raza*cos(t/10));
        y1:=round(y0+raza*sin(t/10));

        x2:=round(x0+raza*cos(t/10));
        y2:=round(y0+raza*sin(t/10));

        pen.width:=10;

        //liniile de jos
        moveto(175,506);
        lineto(x1+120,y1+8);

        moveto(232,583);
        lineto(x1+120,y1+8);

        moveto(227,585);
        lineto(x1+30,y1+8);

        moveto(120,580);
        lineto(x2+30,y2+8);

        moveto(170,506);
        lineto(x2-55,y2+8);

        moveto(115,577);
        lineto(x2-55,y2+8);

        {if directie then t:=t+1
                    else t:=t-1;

        if(t=tmax) or (t=-20)
                   then directie:=not directie;   }

        //Cerc de mijloc
        brush.color:=rgb(100,149,237);
        pen.Width:=4;
        ellipse(x1-70,y1-i+7,x1+150,y1+i+7);
        pen.Width:=8;
        pen.color:=rgb(100,149,237);
        brush.color:=rgb(100,149,237);
        pen.Width:=4;
        ellipse(x1-70,y1-i+3,x1+150,y1+i+3);
        brush.color:=rgb(100,149,237);
        pen.color:=rgb(1,1,1);
        ellipse(x1-70,y1-i-1,x1+150,y1+i-1);



        //cutie pentru placute



        brush.Color:=clgray;
       xd:=360;
        yd:=520;
                rectangle(xd-20-100,yd+40,xd+74-100,yd+160);
polygon([point(xd+70-100,yd+160),point(xd+100-100,yd+130),point(xd+100-100,yd+20),
point(xd-100,yd+20),point(xd-20-100,yd+40),point(xd+70-100,yd+40)]);
Moveto(xd+100-100,yd+20);
Lineto(xd+70-100,yd+40);


          //banda rulanta



xb:=109;
yb:=87;
brush.Color:=clgray;
brush.Color:=clgreen;
polygon([point(xb-175,yb),point(xb+227,yb),point(xb+178,yb+75),point(xb-175,yb+75)]);
brush.color:=clgray;
polygon([point(xb+170,yb+75),point(xb+180,yb+75),point(xb+232,yb+5),
point(xb+228,yb)]);
polygon([point(xb-175,yb+59),point(xb+185,yb+59),point(xb+175,yb+75),
point(xb-175,yb+75)]);



//liniile benzii



pen.Width:=1;
pen.Color:=clgray;
pen.width:=2;
//Moveto(xb+188+b1,yb);
//Lineto(xb+138+b1,yb+59);
Moveto(xb+148+b1,yb);
Lineto(xb+98+b1,yb+59);
Moveto(xb+108+b1,yb);
Lineto(xb+58+b1,yb+59);
Moveto(xb+68+b1,yb);
Lineto(xb+18+b1,yb+59);
Moveto(xb+28+b1,yb);
Lineto(xb-22+b1,yb+59);

Moveto(xb-12+b1,yb);
Lineto(xb-62+b1,yb+59);
  Moveto(xb-52+b1,yb);
Lineto(xb-102+b1,yb+59);
 Moveto(xb-92+b1,yb);
Lineto(xb-142+b1,yb+59);



pen.Width:=1;


     //miscarea liniilor benzii




if ((a>=100) or (b<=140)) or ((b<=0) and (d>140))then

begin
    b1:=b1+1;
if (t mod 40=0) then
   begin
    b1:=0;
   end;
 end;



               //desenare cutie pt impachetat
                 {xm1:=115;
                 ym1:=565;
                     rectangle(xm1+200,ym1+60,xm1+350,ym1-100);
brush.color:=clyellow;
//ellipse(xm1+232,ym1-60,xm1+242,ym1-50);
TextOut(xm1+250,ym1-60,'Depozitare');
brush.Color:=clgreen;
polygon([point(xm1+200,ym1-100),point(xm1+220,ym1-120),point(xm1+350,ym1-120),
point(xm1+350,ym1-100),point(xm1+200,ym1-100)]);
                                                   }




        //aici sunt liniile groase de jos
        pen.width:=20;
        brush.color:=rgb(240,248,255);
        pen.color:=rgb(2,54,123);

        xs1:=round(xs+razas*cos(t/10));
        ys1:=round(ys+razas*sin(t/10));

        xs2:=round(xs+razas*cos(t/10));
        ys2:=round(ys+razas*sin(t/10));

        moveto(175,506);
        lineto(xs1+74,ys1-35);

        moveto(170,506);
        lineto(xs2-25,ys2-35);

        moveto(120,580);
        lineto(xs2,ys2);

        moveto(227,585);
        lineto(xs1+50,ys1+8);

        moveto(232,583);
        lineto(xs1+100,ys1+8);

        moveto(115,577);
        lineto(xs2-47,ys2+8);


        pen.width:=4;
        pen.color:=rgb(1,1,1);



        pen.width:=10;

        //Sus mari
        moveto(x1+120,y1-4);
        lineto(265,206);

        moveto(x1+120,y1-4);
        lineto(160,220);

        moveto(x1+40,y1-8);
        lineto(265,206);

        moveto(x2+30,y2-8);
        lineto(85,206);

        moveto(x2-55,y2-4);
        lineto(160,220);

        moveto(x2-55,y2-4);
        lineto(85,206);

        pen.width:=4;
        brush.color:=rgb(100,149,237);
        ellipse(280,168+k,70,228+k);
        ellipse(281,160+k,71,220+k);
        //ellipse(xss1-70,yss1-i+7,xss1+150,yss1+i-20);
        brush.color:=rgb(240,248,255);

        if (k=10) then
          begin
            //start decrementing k:
            increment:=0;
            decrement:=1;
            k:=k-1;
          end
          else //k poate fi 0 sau intre 0 si 10
            if (k=0) then
              begin
                //start increment
                increment:=1;
                decrement:=0;
                k:=k+1;
              end
              else //k nu e 0, k nu e 10
                begin
                  if (increment=1) then k:=k+1;
                  if (decrement=1) then k:=k-1;
                end;

        //liniiiile alea groase de sus

        //pen.width:=20;
        //brush.color:=rgb(240,248,255);
        //pen.color:=rgb(2,54,123);

        xss1:=round(xss+razas*cos(t/10));
        yss1:=round(yss+razas*sin(t/10));

        xss2:=round(xss+razas*cos(t/10));
        yss2:=round(yss+razas*sin(t/10));

        pen.width:=20;
        pen.color:=rgb(2,54,123);

        moveto(x1+120,y1-4);
        lineto(xss2+113,yss2);

        moveto(x1+40,y1-8);
        lineto(xss1+70,yss1+8);

        moveto(x1+35,y1-8);
        lineto(xss1-16,yss1+8);

        moveto(x1+120,y1-4);
        lineto(xss1+70,yss1+8);

        moveto(x1-48,y1-8);
        lineto(xss2-27,yss2+8);

        moveto(x1-53,y1-8);

        lineto(xss2-59,yss2+8);



        //placuta



                              pen.color:=clgray;
             //rectangle(xb-50+tl+a+30,yb+tl-45-e-e-27+15-g,xb+5+tl+a+30,yb-30+tl+15-e-e-27+5-g);
                       pen.color:=clgray;
             rectangle(xb-45+tl+a+15,yb+tl-40-e-e-27+2-g,xb+tl+a+15,yb-25+tl+5-e-e-27+2-g);

                brush.color:=clgray;


    case faza of
        0:
          begin
            a:=a+1;
            if (a > 100)
              then
                faza:=1;
          end;

        1:
          begin
            b:=b+1;
            if (b > 200)
              then
                faza:=2;
                end;

                     2:
          begin
            e:=e+1;
            if (e <63 )
              then
                faza:=3;
          end;
        3:
          begin
            e:=e-1;
            if (e > 100)
              then
                faza:=4;
                end;
                4:
               begin
            g:=g-1;
            if (g > 1 )
              then
                faza:=5;

          end;

        {5:
          begin
            g:=g+1;
            if (g < 263)
              then
                faza:=6;
                end;  }

                        {   4:
          begin
            d:=d-1;
            if (d < 0)
              then
                faza:=5;  }
          end;




         //placuta    2




                              pen.color:=clgray;
             //rectangle(xb-50+tl+a+30,yb+tl-45-e-e-27+15-g,xb+5+tl+a+30,yb-30+tl+15-e-e-27+5-g);
                       pen.color:=clgray;
             rectangle(xb-45+tl+a+15,yb+tl-40-e-e+285,xb+tl+a+15,yb+283+tl+5-e-e-27+2);

                brush.color:=clgray;


    case faza of
        0:
          begin
            a:=a+1;
            if (a > 100)
              then
                faza:=1;
          end;

     {   1:
          begin
            b:=b+1;
            if (b > 200)
              then
                faza:=2;
                end;

              2:  }
              1:
          begin
            e:=e+1;
            if (e <63 )
              then
                faza:=2;
          end;
        2:
          begin
            e:=e-1;
            if (e > 100)
              then
                faza:=3;
                end;
                3:
               begin
            g:=g-1;
            if (g > 1 )
              then
                faza:=4;

          end;

        {5:
          begin
            g:=g+1;
            if (g < 263)
              then
                faza:=6;
                end;  }

                        {   4:
          begin
            d:=d-1;
            if (d < 0)
              then
                faza:=5;  }
          end;






// brat robot




pen.Width:=3;
pen.color:=clgray;
brush.color:=clnavy;
polygon([point(600,600),point(630,530),point(500,530),point(470,600)]);
polygon([point(470,600),point(470,630),point(600,630),point(600,600)]);
polygon([point(600,600),point(600,630),point(630,560),point(630,530)]);

   rectangle(550,220-a,590,560);
   rectangle(560,220-a,600,570);

rectangle(450-b-a-30,220-a-c-e-e-e-27,590,250-a-c-e-e-e-27);
rectangle(450-b-a-30,230-a-c-e-e-e-27,590,250-a-c-e-e-e-27);
     rectangle(455-b-a-30,220-a-c-e-e-e-27,450-b-a-30,250-a-c-e-e-e-27);
 rectangle(450-b-a-30,250-a-c-e-e-e-27,424-b-a-30,244-a-c-e-e-e-27);
 rectangle(425-b-a-30,226-a-c-e-e-e-27,450-b-a-30,220-a-c-e-e-e-27);

        rectangle(450-b-a-30,488-a-e,590,470-a-e);
  rectangle(450-b-a-30,460-a-e,590,470-a-e);
             rectangle(450-b-a-30,485-a-e,455-b-a-30,460-a-e);
 rectangle(450-b-a-30,460-a-e,426-b-a-30,465-a-e);
 rectangle(425-b-a-30,490-a-e,455-b-a-30,485-a-e);




 //banda 2 rulanta




            xb:=59;
yb:=430;
brush.Color:=clgray;
brush.Color:=clgreen;
polygon([point(xb-175+50,yb),point(xb+227+50,yb),point(xb+178+50,yb+75),point(xb-175+50,yb+75)]);
brush.color:=clgray;
polygon([point(xb+170+50,yb+75),point(xb+180+50,yb+75),point(xb+232+50,yb+5),
point(xb+228+50,yb)]);
polygon([point(xb-175+50,yb+59),point(xb+185+50,yb+59),point(xb+175+50,yb+75),
point(xb-175+50,yb+75)]);



//liniile benzii  2


pen.Width:=1;
pen.Color:=clgray;
pen.width:=2;
Moveto(xb+188+b1,yb);
Lineto(xb+138+b1,yb+59);
Moveto(xb+148+b1,yb);
Lineto(xb+98+b1,yb+59);
Moveto(xb+108+b1,yb);
Lineto(xb+58+b1,yb+59);
Moveto(xb+68+b1,yb);
Lineto(xb+18+b1,yb+59);
Moveto(xb+28+b1,yb);
Lineto(xb-22+b1,yb+59);

Moveto(xb-12+b1,yb);
Lineto(xb-62+b1,yb+59);
  Moveto(xb-52+b1,yb);
Lineto(xb-102+b1,yb+59);
 Moveto(xb-92+b1,yb);
Lineto(xb-142+b1,yb+59);



pen.Width:=1;



     //miscarea liniilor benzii   2

if ((a>=100) or (b<=140)) or ((b<=0) and (d>140))then

begin
    b1:=b1+1;
if (t mod 40=0) then
   begin
    b1:=0;
   end;
 end;





//rectangle(558,220,598,560);
//ellipse(455,200,495,260);
//polygon([point(550,550),point(550,580),point(550,510),point(580,480)]);



 //Panoul "Impachetare placute"


                       pen.Width := 5;
                   pen.Color:=clred;
                   brush.Color:=clwhite;
            rectangle(40,10,250,60);
                 TextOut(xb+28,yb-400,'Impachetare placute');
            //rectangle(20,20,650,40);
            pen.Width := 5;
          //  pen.Width := 3;


               // bile
                        // ellipse(135,430,145,440);
                       { ellipse(40,340,50,350);
                   pen.Width := 5;}
                   brush.Color :=clyellow;

                   


                  // miscare de rotatie

           {  if directiel then
             tl:=tl+1

             else
             tl:=tl-1;

             if (tl=0) or (tl=tlmax) then
              directiel:=not directiel;

               moveto (xl0,yl0);
               lineto(xl1,yl1);

        xl1:=round(xl0+razal*cos(tl/10));
        yl1:=round(yl0+razal*sin(tl/10));

        moveto (xl1,yl1);
        lineto (xl2,yl2);

        xl2:=round(xl0+razal*cos(tl/10));
        yl2:=round(yl0+razal*sin(tl/10));        }


        

//miscare brat


    case faza of
        0:
          begin
            a:=a+1;
            if (a > 100)
              then
                faza:=1;
          end;
        1:
          begin
            b:=b+1;
            if (b > 140)
              then
                faza:=2;
          end;
        2:
          begin
            c:=c-1;
            if (c > 100)
              then
                faza:=3;
          end;
        3:
          begin
            d:=d+1;
            if (d > 140)
              then
                faza:=4;
                end;
               4:

          begin
            f:=f+1;
            if (f <10)
              then
                faza:=5;
          end;
        {4:
          begin
            b:=b-1;
            if (b < 0)
              then
                faza:=5;
          end;
       { 5:
          begin
            a:=a+1;
            if (a > 200)
              then
                faza:=6;
                //  sau faza:=6;
          end; }

        5:
          begin
            a:=a-1;
            if (a > 0)
              then
                faza:=0;
          end;
    end;

 {case faza of
   0:
   begin
   a:=a-1;
   if(a>10)
    then
   faza:=1;
   end;
   end;
          { case faza of
   1:
   begin
   b:=b-1;
   if(b<100) then

   faza:=0;
          {
  case faza of
   2:

   begin
   c:=c-1;
    if(c<100) then

   faza:=2;

     end;  }
   end;
   end;





{$R *.dfm}

procedure TForm1.Edit1Change(Sender: TObject);
begin
write(x2);
end;


end.
