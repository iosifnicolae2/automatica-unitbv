unit brat;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls, Buttons;

type
  TForm1 = class(TForm)
    Image1: TImage;
    Timer1: TTimer;
    Button1: TButton;
    Button2: TButton;
    Button3: TButton;
    procedure FormCreate(Sender:TObject);
    procedure Timer1Timer(sender:TObject);
    procedure Button3Click(sender:TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Decor;
    //deseneaza bratul robot in cilindrii
    procedure BratRobot(x:integer;   //pozitie abscisa
                        y:integer;   //pozitie ordonata
                        t:integer);  //pozitia robotului
    procedure BratRobotStanga(x:integer;y:integer;t:integer); // deseneaza pastea care
                                                              //vine desenata sub tava
    //deseneaza banda fara cauciuc
    procedure Banda(x:integer;  //pozitie abscisa
                    y:integer;  //pozitie ordonata
                    l:integer;  //lungimea benzii
                    t:integer;   //pozitia rotorului
                    sens:integer); // sensul in care se invarte
    //deseneaza banda cu caiuciuc 3D
    procedure BandaCauciuc(x:integer;  //pozitie abscisa
                    y:integer;  //pozitie ordonata
                    l:integer;  //lungimea benzii
                    t:integer;   //pozitia rotorului
                    sens:integer);//sensul in care se invarte
         //cercu cu raza rotitoare
   procedure cercr(x:integer;y:integer;r:integer;t:integer);
   procedure masinabile(x:integer;y:integer;t:integer);   //masina de facut bile
   procedure alimentareplastic(x:integer;y:integer);
   procedure bile(x:integer;y:integer;t:integer); // deseneaza bilele pe banda
   procedure suportitavi(x:integer;y:integer); //deseneaza suportul tavilor pt bile
   procedure tava(x:integer;y:integer); // deseneaza tava pentru bile
   //bratul robot dreapta + miscarea
   procedure bratdreaptafata(x:integer;y:integer); // bratul din stanha de mutare a tavilor
   procedure bratdreaptaspate(x:integer;y:integer);//este necesar artificiul ascesta pentru
                                                    //a muta clestele de sus in spatele tavii
   procedure bratdreapta(x:integer;y:integer;t:integer);//ansamblarea bratului dreapta + miscare
   procedure brataspirator(x:integer;y:integer;t:integer); // bratul aspirator
   procedure colectorbile(x:integer;y:integer); // dispozitiv de colectare a bilelor
   procedure alimentaretavi(x:integer;y:integer); // suport alimentare tavi
   procedure ansamblaremiscaretavi; //aceasta este functia care are rolul de a ansambla miscarea tavilor pe masa
                                    //aici se iau deciziile de deplasare principale
   procedure biletavi(x:integer;y:integer;t:integer); //deseneaza bilele pe tavi
                                                      //avem 2 seturi de bile
   procedure detali; // text masini
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

  //timpi
  t:integer=0; //timpul universal
  //timpi pt bratul robot hiraulic
  tbx:integer=0;
  tby:integer=0;
  tb1:integer=0;
  tb2:integer=0;
  tb3:integer=0;
  tb4:integer=0;
  tb5:integer=0;

  //var pt functia de mutare a bilelor
  tbb1:integer=0;
  tbb2:integer=0;
  tbb3:integer=0;
  tbb1y:integer=0;
  tbb2y:integer=0;
  tbb3y:integer=0;
  //culori bile
  cb11,cb12,cb13:integer;
  cb21,cb22,cb23:integer;
  cb31,cb32,cb33:integer;



  //mutarea tavilor
  //variabile pentru tava 1,2,3
  tvx1:integer = 0;
  tvx2:integer = 0;
  tvx3:integer = 0;
  tvy1:integer = 0;
  tvy2:integer = 0;
  tvy3:integer = 0;

  //variabile pentru brat dreapta
  tbd:integer =0;
  tbdx:integer = 0;
  tbdy:integer = 0;

  //timp aspirator
  tasp:integer =0;
  taspx:integer = 0;
  taspy:integer = 0;

  //timpi brat mutare
  tbr :integer =0;
  tbrx :integer =0;
  tbry :integer =0;
  tbrys :integer =0;

  //pt procedura cercr
  xcerc,ycerc,xmasina,ymasina:integer;
  miscare:integer = 0;

  //timpi bile
  //generatia 1 de bile
  tb11x:integer=0;
  tb11y:integer=0;
  tb12x:integer=0;
  tb12y:integer=0;
  tb13x:integer=0;
  tb13y:integer=0;
  tb14x:integer=0;
  tb14y:integer=0;
  //culori bile
  c11,c12,c13:integer;
  c21,c22,c23:integer;
  c31,c32,c33:integer;
  //generatia 2 de bile
  tb21x:integer=0;
  tb21y:integer=0;
  tb22x:integer=0;
  tb22y:integer=0;
  tb23x:integer=0;
  tb23y:integer=0;
  tb24x:integer=0;
  tb24y:integer=0;

implementation

        procedure TForm1.Timer1Timer(Sender:TObject);

        begin
          with Form1.Image1.Canvas do
          begin


              //permite redesenarea ecranului
              fillrect(clientrect);
              //procedurile care realizeaza desenarea elementelor fixe
              decor;
              colectorbile(0,0);
              suportitavi(0,0);
              suportitavi(90,-90);
              alimentaretavi(0,0);
              alimentaretavi(-50,-159);
               detali;
              //masina bila + miscare
              masinabile(0,0,t);

              //banda de la masina cu bile
              bandacauciuc(120,450,100,t,2);
              //bile + tub racire
              bile(0,0,t);

              //aici vor avea loc principalele miscari
              ansamblaremiscaretavi;
              alimentareplastic(0,-100);



              /////////////////////////////////////////
              /////////////////////////////////////////
              //variabilele de timp- miscare
              if miscare=1 then
                  t:=t+1 ;

              //restartul aplicatiei
              if t>45000 then
                  t:=0;


        end;

       end;
       {$R *.dfm}

       procedure TForm1.FormCreate(Sender: TObject);
       begin
         form1.Height:=700;
         form1.Width:=1000;

        end;


       procedure TForm1.Button3Click(Sender: TObject);
       begin
       form1.close
       end;



procedure TForm1.Button1Click(Sender: TObject);
begin
  miscare :=1;
end;

procedure TForm1.Button2Click(Sender: TObject);
begin
  miscare :=0;
end;
procedure TForm1.Decor;
      begin
        with Form1.Image1.Canvas do
           begin
            //pereti
            //perete stanga
            brush.Color := rgb(	205,133,63);
            polygon([point(0,0),point(200,0),point(200,300),point(0,500)]);
            //perete fata
            brush.Color := rgb(222,184,135);
            rectangle(200,0,1000,300);
            //podea
            brush.Color := rgb(139,69,19);
            polygon([point(200,300),point(1000,300),point(1000,650),point(0,650),point(0,500)]);

            //fereastra folie
            brush.Color := rgb(	190,190,190);
            polygon([point(70,300),point(70,350),point(20,400),point(20,350)]);
            brush.Color := rgb(119,136,153);
            polygon([point(65,310),point(65,350),point(25,390),point(25,350)]);
            moveto(65,350);lineto(25,350);

            //suport banda bile
            brush.Color := rgb(	119,136,153);
            rectangle(140,500,220,450);
            polygon([point(220,500),point(240,480),point(240,450),point(220,450)]);

            //banda pt brate robotice
            rectangle(100,190,1000,200);
            polygon([point(100,190),point(130,170),point(1000,170),point(1000,190)]);
            rectangle(120,199,1000,220);
            pen.Width := 3;
            moveto(121,210);lineto(1000,210);
            pen.Width := 1;


           end;
      end;

 procedure TForm1.BratRobot(x:integer;y:integer;t:integer);

       begin
            //reglare brat in functie de parametrul t,parametru
            //care da lungimea bratului hidraulic
            //trebuie sa facem astfel incat dupa ce un segment a ajuns la
            //refuz sa se miste segmentul mai gros dar si segmentul ajuns la refuz

            //miscare segment 1
            if (t>=0) and (t<50)and (miscare=1) then
                tb1 := t;

            //miscare segment 1+2
            if (t>=50) and (t<80)and (miscare=1) then
            begin
                tb1 := t;
                tb2 := t - 50;
            end;
            //miscare segment 1+2+3
            if (t>=80) and (t<110) and (miscare=1) then
            begin
                tb1 := t;
                tb2 := t - 50;
                tb3 := t - 80;
            end;
            //miscare segment 1+2+3+4
            if (t>=110) and (t<140) and (miscare=1)then
            begin
                tb1 := t;
                tb2 := t - 50;
                tb3 := t - 80;
                tb4 := t - 110;
            end;
            //miscare segment 1+2+3+4+5
            if (t>=140) and (t<170) and (miscare=1)then
            begin
                tb1 := t;
                tb2 := t - 50;
                tb3 := t - 80;
                tb4 := t - 110;
                tb5 := t - 140;
            end;



            with Form1.Image1.Canvas do
            begin


            {
             //cleste
            brush.color:= rgb(0,0,255);
            //baza cleste
            rectangle(x-35,y+75+tb1,x+35,y+80+tb1);
            //stanga
            rectangle(x-40,y+75+tb1,x-35,y+90+tb1);
            //dreapta
            rectangle(x+35,y+75+tb1,x+40,y+90+tb1);
            }

            //3D

            brush.Color := rgb(	190,190,190);
             //dreapta
            rectangle(x+10,y+90+tb1,x+15,y+120+tb1);
            polygon([point(x+10,y+90+tb1),point(x+40,y+60+tb1),point(x+45,y+60+tb1),point(x+15,y+90+tb1)]);
            polygon([point(x+15,y+90+tb1),point(x+15,y+120+tb1),point(x+45,y+90+tb1),point(x+45,y+60+tb1)]);

             brush.Color := rgb(	190,190,190);
            //baza
            polygon([point(x-60,y+90+tb1),point(x+10,y+90+tb1),point(x+40,y+60+tb1),point(x-30,y+60+tb1)]);
            rectangle(x-60,y+90+tb1,x+10,y+95+tb1);



            brush.Color:=RGB(50,0,100);

            //brat robotizat
            pen.Width:=1;
            pen.Color:=clblack;
            //elementi brat
            //rectangle (x-3,y,x+3,y+60+t2+t21+t30-t33+t57-t59) ;

            rectangle (x-3,y+50,x+3,y+75+tb1);
            rectangle (x-5,y+50,x+5,y+55+tb2);
            rectangle (x-10,y+45,x+10,y+50+tb3);
            rectangle (x-15,y+40,x+15,y+45+tb4);
            rectangle (x-20,y+35,x+20,y+40+tb5);
            rectangle (x-25,y-7,x+25,y+35);
            pen.Width:=1;
            brush.Color:=RGB(100,100,100);
            ellipse (x-30,y-8,x+30,y+8);
            MoveTo (x-30,y); LineTo(x+30,y);
            brush.Color:=RGB(100,100,100);
            ellipse(x-30,y-7,x-20,y+3);
            ellipse(x+20,y-7,x+30,y+3);



            end;
       end;
    procedure Tform1.BratRobotStanga(x:integer;y:integer;t:integer);
    begin
         with Form1.Image1.Canvas do
         begin
             brush.Color := rgb(	190,190,190);
              //stanga
            rectangle(x-60,y+90+tb1,x-65,y+120+tb1);
            polygon([point(x-65,y+90+tb1),point(x-35,y+60+tb1),point(x-30,y+60+tb1),point(x-60,y+90+tb1)]);
            polygon([point(x-60,y+90+tb1),point(x-60,y+120+tb1),point(x-30,y+90+tb1),point(x-30,y+60+tb1)]);
         end;
    end;
    procedure TForm1.Banda(x:integer;y:integer;l:integer;t:integer;sens:integer);

       begin
          with Form1.Image1.Canvas do
         begin
              //prototip banda transmisie
            //acesta se poate regla prin parametrii
            //-L- da lungimea benzii
            //-X,Y - coordonatele benzii
            pen.Color:=RGB(0,128,128);
            MoveTo (x,y-8); LineTo(x+16+l,y-8); //banda transmisie verde
            MoveTo (x,y+8); LineTo(x+16+l,y+8);
            brush.Color:=RGB(210,180,140);
            ellipse(x-8,y-8,x+8,y+8);  //disc 1
            ellipse(x+8+l,y-8,x+24+l,y+8);  //disc2

            //linie suport anda
            //aceasta linie trebuie sa se invarte odata cu deplasarea benzii
            pen.Color:=clred;
            if(sens = 1) then
            begin
            MoveTo(x,y); LineTo(x+round(sin(t/10)*6),y+round(cos(t/10)*6));
            MoveTo(x+16+l,y); LineTo(x+16+l+round(sin(t/10)*6),y+round(cos(t/10)*6));
            end
            else
            begin
            MoveTo(x,y); LineTo(x+round(cos(t/10)*6),y+round(sin(t/10)*6));
            MoveTo(x+16+l,y); LineTo(x+16+l+round(cos(t/10)*6),y+round(sin(t/10)*6));
            end;
         end;
       end;
       procedure TForm1.BandaCauciuc(x:integer;y:integer;l:integer;t:integer;sens:integer);

        begin
          with Form1.Image1.Canvas do
          begin
             pen.Width :=1;
             //deseneaza banda in 2 d
             if(sens  =1)then
             begin
             Banda(x,y,l,t,sens);
             end
             else
             begin
             Banda(x,y,l,t,sens);
             end;
             brush.Color := Rgb(112,138,144);
             pen.Color := Rgb(112,138,144);
             //verificam daca lungimea este pozitiva si mai mare de 2
             if l>2 then
                rectangle(x+8,y-7,x+8+l,y+8);
             //desenam cauciucul si tesitura
             pen.Color :=  RGB(85,85,85);
             brush.Color :=  RGB(85,85,85);
             polygon([point(x-2,y-9),point(x+9,y-20),point(x+l+29,y-20),point(x+l+18,y-9)]);
             //desen tesitura
             polygon([point(x+l+19,y-8),point(x+l+24,y-2),point(x+l+36,y-14),point(x+l+30,y-20)]);
             polygon([point(x+l+24,y),point(x+l+37,y-13),point(x+l+33,y-7),point(x+l+22,y+4)]);
             pen.Color :=  RGB(0,0,0);
             moveto(x+l+30,y-20);lineto(x+l+18,y-8);
             pen.Color :=  RGB(0,0,0);
             moveto(x+l+24,y-1);lineto(x+l+37,y-14);

          end;
        end;
        procedure Tform1.masinabile(x:integer;y:integer;t:integer);
      begin
          with Form1.Image1.Canvas do
              begin
                 //calculele pentru rotor  care se invarte in sensul   opus
                 xmasina :=65 + round(5*sin(t/10));
                 ymasina :=405 + round(5*cos(t/10));
                 brush.Color := rgb(216,191,216);



                 //desenaza prima data  rotorul dreapta, sa creeze efect de invartire

                 ellipse(90,365,100,375);
                 polygon([point(90+x,370+y),point(60+x,405+y),point(70+x,405+x),point(100+x,370+y)]);
                 pen.Color := rgb(216,191,216);
                 moveto(90,370);lineto(100,370);
                 pen.Color := rgb(0,0,0);

                 ellipse(60,400,70,410) ;
                 moveto(65,405);lineto(xmasina,ymasina);
                 //desenarein spatele foliei a masinii
                 brush.Color := rgb(	119,136,153);
                 polygon([point(60+x,405+y),point(35+x,405+y),point(50+x,390+y)]);
                 brush.Color := rgb(216,191,216);
                 cercr(55,405,5,t);

                 //banda folie
                 brush.Color := rgb(216,191,216);
                 cercr(30,380,5,t);
                 brush.Color := rgb(255,255,255);
                 ellipse(x+55,340+y,65+x,350+y);
                 polygon([point(30+x,375+y),point(60+x,340+y),point(35+x,340+y),point(25+x,350+y),point(25+x,375+y)]);
                 polygon([point(30+x,375+y),point(60+x,340+y),point(90+x,370+y),point(60+x,405+y)]);
                 brush.Color := rgb(216,191,216);


                 //desenare peste folie masina de bile
                 //placa stanga
                 brush.Color := rgb(	119,136,153);
                 polygon([point(70+x,405+x),point(100+x,370+y),point(130+x,370+y),point(100+x,405+y)]);

                 rectangle(35,405,100,450);
                 polygon([point(99,449),point(99,405),point(130,370),point(130,415)]);

                 //desenare cutie recoltare
                 rectangle(50,450,80,480);
                 polygon([point(80,480),point(95,465),point(95,450),point(80,450)]);
                 rectangle(35,480,100,500);
                 polygon([point(100,500),point(115,485),point(115,465),point(100,480)]);
                 polygon([point(115,465),point(100,480),point(80,480),point(95,465)]);
                 polygon([point(35,480),point(50,465),point(50,480)]);
                 brush.Color := rgb(216,191,216);

                 //brat bile
                 polygon([point(105,435),point(105,405),point(125,380),point(125,410)])  ;
                 polygon([point(110,405),point(110,425),point(120,410),point(120,390)]);
                 //brat efectiv
                 brush.Color := rgb(	119,136,153);
                 polygon([point(110,405),point(120,390),point(140,390),point(130,405)]);
                 rectangle(110,405,130,425);
                 polygon([point(130,405),point(145,420),point(135,430),point(130,425)]);
                 polygon([point(130,405),point(140,390),point(155,405),point(145,420)]);

              end;
      end;
        procedure TForm1.cercr(x:integer;y:integer;r:integer;t:integer);
        begin
            with Form1.Image1.Canvas do
              begin
                  ellipse(x-r,y-r,x+r,y+r);
                  xcerc := round(r*cos(t/10))+x;
                  ycerc := round(r*sin(t/10))+y;
                  moveto(x,y);lineto(xcerc,ycerc);
              end;
        end;
      procedure TForm1.bile(x:integer;y:integer;t:integer);
      begin
         with Form1.Image1.Canvas do
              begin

                 //suport tub

                 rectangle(275,480,435,500);
                 polygon([point(435,500),point(445,490),point(445,480),point(435,480)]);
                 polygon([point(257,440),point(248,450),point(265,470),point(265,450)]);
                 polygon([point(248,450),point(265,470),point(265,473),point(248,453)]);

                 //bilele pe banda
                 //daca e diferit de 0 sa deseneze bilele
                 if(t <= 0)and (miscare=1)then
                 begin
                     cb11 := random(255);
                     cb12 := random(255);
                     cb13 := random(255);
                     cb21 := random(255);
                     cb22 := random(255);
                     cb23 := random(255);
                     cb31 := random(255);
                     cb32 := random(255);
                     cb33 := random(255);
                 end;
                 if(t>0)  then
                 begin
                     //deseneaza prima bila
                    //culoare aleatoare
                    brush.Color := rgb(cb11,cb12,cb13);
                    ellipse(135+x+tbb1,430+tbb1y,145+x+tbb1,440+tbb1y);
                 end;
                 if(t>55)  then
                 begin
                       //deseneaza a doua bila
                        brush.Color := rgb(cb21,cb22,cb23);
                        ellipse(135+x+tbb2,430+tbb2y,145+x+tbb2,440+tbb2y);
                 end;
                 if(t>100)  then
                   begin
                        //deseneaza a treia bila
                        brush.Color := rgb(cb31,cb32,cb33);
                        ellipse(135+x+tbb3,430+tbb3y,145+x+tbb3,440+tbb3y);
                   end;
                 if(t > 0)and (miscare=1) then
                 begin
                    tbb1 := tbb1 + 1;
                    brush.Color := rgb(255,255,255);

                    //muta bila in jos
                    if(tbb1>110)and (miscare=1) then
                        tbb1y:=tbb1y+1;
                    //daca a ajuns bila  in tub sa treaca pe pozitia initiala
                    if(tbb1 >130)and (miscare=1)then
                    begin
                        tbb1:=0;
                        tbb1y:=0;
                        cb11 := random(255);
                        cb12 := random(255);
                        cb13 := random(255);
                    end;
                    //a 2-a bila
                    if (t>55)and (miscare=1) then
                    begin
                        tbb2 := tbb2 +1;
                        if(tbb2>110)and (miscare=1) then
                             tbb2y:=tbb2y+1;
                             if(tbb2 >130)then
                             begin
                                tbb2:=0;
                                tbb2y:=0;
                                cb21 := random(255);
                                cb22 := random(255);
                                cb23 := random(255);
                             end;
                    end;
                    if (t>100)and (miscare=1) then
                    begin
                        tbb3 := tbb3 +1;
                        if(tbb3>110) and (miscare=1)then
                             tbb3y:=tbb3y+1;
                             if(tbb3 >130)and (miscare=1)then
                             begin
                                tbb3:=0;
                                tbb3y:=0;
                                 cb31 := random(255);
                                 cb32 := random(255);
                                 cb33 := random(255);
                             end;
                    end;

                 end;
                 //desenarea tubului pt racire
                 //desenare se face prin schimbarea culorii si
                 //desenarea peste elipse pt crearea impresiei de tub
                 brush.Color := rgb(	190,190,190);
                 ellipse(260,450,270,480);
                 pen.Color := rgb(	190,190,190);
                 rectangle(265,450,300,480);

                 pen.Color := rgb( 0,0,0);
                 brush.Color := rgb(	119,136,153);
                 ellipse(290,450,300,480);
                 pen.Color := rgb(	119,136,153);
                 rectangle(295,450,310,480);

                 pen.Color := rgb( 0,0,0);
                 brush.Color := rgb(	190,190,190);
                 ellipse(300,450,310,480);
                 pen.Color := rgb(	190,190,190);
                 rectangle(305,450,330,480) ;

                 pen.Color := rgb( 0,0,0);
                 brush.Color := rgb(	119,136,153);
                 ellipse(325,450,335,480);
                 pen.Color := rgb(	119,136,153);
                 rectangle(330,450,350,480);


                 pen.Color := rgb( 0,0,0);
                 brush.Color := rgb(	190,190,190);
                 ellipse(335,450,345,480);
                 pen.Color := rgb(	190,190,190);
                 rectangle(340,450,370,480) ;

                 pen.Color := rgb( 0,0,0);
                 brush.Color := rgb(	119,136,153);
                 ellipse(365,450,375,480);
                 pen.Color := rgb(	119,136,153);
                 rectangle(370,450,390,480);

                 pen.Color := rgb( 0,0,0);
                 brush.Color := rgb(	190,190,190);
                 ellipse(375,450,385,480);
                 pen.Color := rgb(	190,190,190);
                 rectangle(380,450,410,480) ;

                 pen.Color := rgb( 0,0,0);
                 brush.Color := rgb(	119,136,153);
                 ellipse(405,450,415,480);
                 pen.Color := rgb(	119,136,153);
                 rectangle(410,450,430,480);

                 pen.Color := rgb( 0,0,0);
                 brush.Color := rgb(	190,190,190);
                 ellipse(415,450,425,480);
                 pen.Color := rgb(	190,190,190);
                 rectangle(420,450,450,480) ;

                 brush.Color := rgb(	105,105,105);
                 pen.Color := rgb(0,0,0);
                 ellipse(445,450,455,480);
                 pen.Color := rgb(0,0,0);

              end
      end;
      procedure TForm1.suportitavi(x:integer;y:integer);
      begin
         with Form1.Image1.Canvas do
              begin
                  brush.Color := rgb(119,136,153);
                  polygon([point(450+x,510+y),point(500+x,510+y),point(530+x,480+y),point(480+x,480+y)]);
                  polygon([point(460+x,505+y),point(500+x,505+y),point(520+x,485+y),point(480+x,485+y)]);
                  rectangle(460+x,505+y,500+x,490+y);
                  polygon([point(500+x,505+y),point(520+x,485+y),point(520+x,470+y),point(500+x,490+y)]);
                  polygon([point(500+x,490+y),point(520+x,470+y),point(480+x,470+y),point(460+x,490+y)]);

              end;
      end;


       procedure TForm1.tava(x:integer;y:integer);
       begin
           with Form1.Image1.Canvas do
              begin
                     brush.Color := rgb(119,136,153);
                    polygon([point(445+x,495+y),point(515+x,495+y),point(545+x,465+y),point(475+x,465+y)]);
                    rectangle(445+x,496+y,515+x,492+y);
                    polygon([point(515+x,496+y),point(545+x,466+y),point(545+x,462+y),point(515+x,492+y)]);
                    polygon([point(445+x,492+y),point(515+x,492+y),point(545+x,462+y),point(475+x,462+y)]);
              end;
       end;
      procedure TForm1.bratdreaptaspate(x:integer;y:integer);
      begin
           with Form1.Image1.Canvas do
              begin
                  brush.Color := rgb(	72,61,139);
                  polygon([point(483+x,532+y),point(438+x,532+y),point(443+x,529+y),point(488+x,529+y)]);
                  rectangle(483+x,532+y,437+x,540+y);
              end;
      end;
      procedure TForm1.bratdreaptafata(x:integer;y:integer);
      begin
          with Form1.Image1.Canvas do
              begin
                  brush.Color := rgb(	72,61,139);
                  rectangle(410+x,560+y,455+x,568+y);
                  polygon([point(410+x,560+y),point(413+x,557+y),point(458+x,557+y),point(455+x,560+y)]);

                  polygon([point(458+x,557+y),point(453+x,557+y),point(478+x,532+y),point(483+x,532+y)]);
                  polygon([point(455+x,568+y),point(455+x,560+y),point(486+x,529+y),point(486+x,537+y)]);

              end;
      end;
     procedure TForm1.bratdreapta(x:integer;y:integer;t:integer);
     begin
         with Form1.Image1.Canvas do
              begin

                brush.Color := rgb(	72,61,139);
                //suport cleste
                rectangle(465+tbdx,550+tbdy,555+tbdx,558+tbdy);
                polygon([point(465+tbdx,550+tbdy),point(465+tbdx,558+tbdy),point(550+tbdx,558+tbdy),point(560+tbdx,550+tbdy)]);
                polygon([point(465+tbdx,550+tbdy),point(475+tbdx,540+tbdy),point(568+tbdx,540+tbdy),point(557+tbdx,550+tbdy)]);

                //suport brat
                brush.Color := rgb(	119,136,153);
                polygon([point(490,608),point(530,608),point(790,348),point(750,348)]);
                rectangle(490,608,530,650);
                polygon([point(530,650),point(530,608),point(790,348),point(790,390)]);

              end;
     end;
     procedure TForm1.brataspirator(x:integer;y:integer;t:integer);
     begin
         with Form1.Image1.Canvas do
              begin
            brush.Color:=RGB(50,0,200);



            //brat robotizat
            pen.Width:=1;
            pen.Color:=clblack;
            //elementi brat

            rectangle (x-7,y+50,x+7,y+95+t);
            rectangle (x-15,y+5,x+15,y+85);

            pen.Width:=1;
            brush.Color:=RGB(100,100,100);
            ellipse (x-30,y-8,x+30,y+8);
            MoveTo (x-30,y); LineTo(x+30,y);
            brush.Color:=RGB(100,100,100);
            ellipse(x-30,y-7,x-20,y+3);
            ellipse(x+20,y-7,x+30,y+3);

              end;
     end;
     procedure tform1.colectorbile(x:integer;y:integer);
     begin
          with Form1.Image1.Canvas do
              begin
                  rectangle(480,405,533,425);
                  polygon([point(533,405),point(533,425),point(563,395),point(563,375)]);
                  brush.Color := rgb(	85,85,85);
                  polygon([point(480,405),point(533,405),point(563,375),point(510,375)]);

                  brush.Color := rgb(	0,0,0);
                  polygon([point(510,400),point(520,400),point(540,380),point(530,380)]);
                  moveto(480,405);lineto(510,400);
                  moveto(533,405);lineto(520,400);
                  moveto(563,375);lineto(540,380);
                  moveto(510,375);lineto(530,380);

              end;
     end;
    procedure tform1.alimentaretavi(x:integer;y:integer);
    begin
         with Form1.Image1.Canvas do
              begin
                      //suport alimentare tavi
            brush.Color := rgb(119,136,153);
            rectangle(365+x,570+y,450+x,600+y);
            polygon([point(365+x,570+y),point(450+x,570+y),point(490+x,530+y),point(405+x,530+y)]);
            polygon([point(450+x,600+y),point(450+x,570+y),point(490+x,530+y),point(490+x,560+y)]);
            polygon([point(445+x-70,495+y+70),point(515+x-70,495+y+70),point(545+x-70,465+y+70),point(475+x-70,465+y+70)]);
              end;
    end;
     procedure tform1.ansamblaremiscaretavi;
     begin
           with Form1.Image1.Canvas do
              begin
                //partea de desenare a elementelor



                bratdreaptaspate(tbdx,tbdy);              //brat dreapta spate
                BratRobotStanga(480+tbrx,210+tbry+tbrys,tbr);


                //tava1
                tava(-70+tvx1,70+tvy1);                   //intre bratele robotizate se deseneaza tava
                //bilele de pe tavi
                //in fucntie de generatia de bile
                //vom avea si rebuturi diferite


                biletavi(0,0,tbd);
                BratRobot(480+tbrx,210+tbry,tbr);
                brataspirator(580+taspx,210+taspy,tasp);                 //bratul aspirator
                bratdreaptafata(tbdx,tbdy);               //bratul robot dreapta fata
                bratdreapta(0,0,t);                       //ansamblu brat robot




                 ///////////////////////////////////
                 //partea de miscare decizionala
                 //////////////////////////////////

                //daca este la inceput sa se afiseze bratul
                if(t=0) then
                  tbdx :=70;


                //prima tava 
                //miscarea spre alimentarea cu tave
                if(tbd>0)and(tbd<=70)and(miscare = 1) then
                    tbdx := tbdx-1;
                if (miscare = 1) then
                    tbd := tbd +1;
               //miscare la alimentare cu bile
                if(tbd>70) and (tbd<140) and (miscare = 1) then
                begin
                   tbdx := tbdx+1;
                   tbdy := tbdy-1;
                   tvx1 := tvx1+1;
                   tvy1 := tvy1-1;
                end;
                //apropiere de teava
                if(tbd>=140)and(tbd<150) and  (miscare = 1) then
                begin
                    tbdx := tbdx -1;
                    tvx1 := tvx1 -1;
                end;
                //asteapta 100 frame-uri pt alimentarea cu bile
                //departare tava
                if(tbd>=240)and(tbd<250) and  (miscare = 1) then
                begin
                    tbdx := tbdx +1;
                    tvx1 := tvx1 +1;
                end;
                //delpaseaza catre sortare
                if(tbd>=250) and (tbd<340) and (miscare = 1) then
                begin
                   tbdx := tbdx+1;
                   tbdy := tbdy-1;
                   tvx1 := tvx1+1;
                   tvy1 := tvy1-1;
                end;
                //lasa tava pe masa
                if(tbd>=340) and (tbd<400) and (miscare = 1) then
                   tbdx:= tbdx + 1;
                //intoarcete la pozitia initiala a bratului dreapta
                if(tbd>=400) and (tbd<559) and (miscare = 1) then
                begin
                   tbdx := tbdx-1;
                   tbdy := tbdy+1;
                end;
                if (tbd=559) and (miscare = 1) then
                   tbdx := tbdx+1;
                //retragere brat dreapta
                if(tbd>=560) and (tbd<569) and (miscare = 1) then
                  tbdx := tbdx+1;
                //miscare brat aspirator catre bila neagra
                if(tbd>=400) and (tbd<495) and (miscare = 1) then
                  tasp := tasp+1;
                //retragere aspirator
                if(tbd>=500) and (tbd<595) and (miscare = 1) then
                begin
                  tasp := tasp-1;
                  taspx := taspx+1;
                end;
                if(tbd>=590) and (tbd<610) and (miscare = 1) then
                  taspx := taspx+1;
                //revenire brat
                 if(tbd>=900) and (tbd<1015) and (miscare = 1) then
                  taspx := taspx-1;

               //deplasare brat catre tava  si miscare sus a partii din stanga
                if(tbd>=590) and (tbd<615) and (miscare = 1) then
                begin
                   tbrx := tbrx+1;
                   tbr := tbr+1;
                   tbrys := tbrys -1;
                end ;
                //miscare catre tava
                if(tbd>=615) and (tbd<705) and (miscare = 1) then
                   tbrx := tbrx+1;
                if(tbd>=705) and (tbd<753) and (miscare = 1) then
                   tbr := tbr+1;
                //golire tava
               if(tbd>=753) and (tbd<823) and (miscare = 1) then
                   tbrx := tbrx-1;
               if(tbd>=823) and (tbd<893) and (miscare = 1) then
                   tbrx := tbrx+1;
               if tbd = 824 then
                    begin
                      tbrx := tbrx-1;
                      //tbr := tbr+1;
                    end;
               //setare brat pentru mutare element
               if(tbd>=893) and (tbd<898) and (miscare = 1) then
               begin
                   tbrys := tbrys+1;
                   tbr := tbr+1;
               end;
               if(tbd>=898) and (tbd<918) and (miscare = 1) then
                   tbrys := tbrys+1;
               //mutare tava catre final
                if(tbd>=918) and (tbd<1127) and (miscare = 1) then
                begin
                   tvx1 := tvx1-1;
                   tbrx := tbrx-1;
                end;
                //retragere element
                if(tbd>=1127) and (tbd<1205) and (miscare = 1) then
                   tbr := tbr-1;
                if(tbd>=1137) and (tbd<1457) and (miscare = 1) then
                   tbrx := tbrx+1;

                //restarteaza tava 1
               if(tbd>1230) then
               begin
                tvx1 := 0;
                tvy1 := 0;
                tbd := 0;
                tb11x := 0;
                tb12x := 0;
                tb13x := 0;
                tb14x := 0;
                tb11y := 0;
                tb12y := 0;
                tb13y := 0;
                tb14y := 0;

               end;
              end;
     end;
     procedure TForm1.biletavi(x:integer;y:integer;t:integer);
     begin
        with Form1.Image1.Canvas do
              begin
                  //deseneaza bilele

                  //prima generatie de bile

                  ///////////////////////////
                  //partea logica de miscare
                  ///////////////////////////
                  brush.Color := rgb(255,255,255);
                  //ellipse(450,460,460,470);
                  //la 150 se pun bilele pe tava

                  //generatia 1

                  //bila 1
                  //punere pe tava
                   if tbd =0 then
                   begin
                     c11 := random(255);
                     c12 := random(255);
                     c13 := random(255);
                     c21 := random(255);
                     c22 := random(255);
                     c23 := random(255);
                     c31 := random(255);
                     c32 := random(255);
                     c33 := random(255);
                   end;
                  if(tbd>150) and (tbd<790) then
                  begin
                    brush.Color := rgb(c11,c12,c13);
                    ellipse(450+tb11x,460+tb11y,460+tb11x,470+tb11y); //bila 1
                    if(tbd>=150) and (tbd<170) and (miscare =1 ) then
                       tb11x := tb11x + 3;
                  end;

                  //bila 2
                  //punere pe tava
                  if(tbd>150)and (tbd<790) then
                  begin
                    brush.Color := rgb(c21,c22,c23);
                    ellipse(450+tb12x,460+tb12y,460+tb12x,470+tb12y); //bila 1
                    if(tbd>=150) and (tbd<157) and (miscare =1 ) then
                    begin
                       tb12x := tb12x + 2;
                       tb12y := tb12y + 2;
                    end;
                    if(tbd>=157) and (tbd<167) and (miscare =1 ) then
                       tb12x := tb12x + 3;
                  end;

                  //bila 3
                  //punere pe tava
                  if(tbd>150)and(tbd<495) then
                  begin
                    brush.color := rgb(0,0,0);
                    ellipse(450+tb13x,460+tb13y,460+tb13x,470+tb13y); //bila 1
                    if(tbd>=150) and (tbd<160) and (miscare =1 ) then
                    begin
                       tb13x := tb13x + 2;
                       tb13y := tb13y + 2;
                    end;
                    if(tbd>=160) and (tbd<163) and (miscare =1 ) then
                       tb13x := tb13x + 2;
                  end;

                  //bila 4
                  //punere pe tava
                  if(tbd>150)and (tbd<790) then
                  begin
                    brush.Color := rgb(255,255,255);
                    brush.Color := rgb(c31,c32,c33);
                    ellipse(450+tb14x,460+tb14y,460+tb14x,470+tb14y); //bila 1
                    if(tbd>=150) and (tbd<160) and (miscare =1 ) then
                       tb14x := tb14x + 2;
                  end;

                  //miscare ansamblu bile catre suport
                  if(tbd>=240)and(tbd<250) and  (miscare = 1) then
                  begin
                    tb11x := tb11x +1;
                    tb12x := tb12x +1;
                    tb13x := tb13x +1;
                    tb14x := tb14x +1;
                  end;
                  if(tbd>=250) and (tbd<340) and (miscare = 1) then
                begin
                    tb11x := tb11x +1;
                    tb11y := tb11y -1;
                    tb12x := tb12x +1;
                    tb12y := tb12y -1;
                    tb13x := tb13x +1;
                    tb13y := tb13y -1;
                    tb14x := tb14x +1;
                    tb14y := tb14y -1;
                end;
                //deplasarea bilelor catre colector
                if(tbd>=756) and (tbd<790) and (miscare = 1) then
                    tb11x := tb11x-2;
                if(tbd>=758) and (tbd<790) and (miscare = 1) then
                    tb12x := tb12x-2;
                if(tbd>=770) and (tbd<790) and (miscare = 1) then
                    tb14x := tb14x-2;

              end;
     end;
     procedure tform1.alimentareplastic(x:integer;y:integer);
         begin
        with Form1.Image1.Canvas do
              begin

            //fereastra folie
            brush.Color := rgb(	190,190,190);
            polygon([point(70+x,300+y),point(70+x,350+y),point(20+x,400+y),point(20+x,350+y)]);
            brush.Color := rgb(119,136,153);
            polygon([point(65+x,310+y),point(65+x,350+y),point(25+x,390+y),point(25+x,350+y)]);
            moveto(65+x,350+y);lineto(25+x,350+y);

                 //banda folie
                 brush.Color := rgb(216,191,216);
                 cercr(30+x,378+y,5,t);
                 brush.Color := rgb(255,255,255);
                 ellipse(x+55,340+y,65+x,350+y);
                 polygon([point(32+x,373+y),point(60+x,340+y),point(35+x,340+y),point(25+x,350+y),point(25+x,373+y)]);
                 polygon([point(32+x,373+y),point(60+x,340+y),point(90,370),point(60,405)]);
                 brush.Color := rgb(216,191,216);

              end;
        end;
     procedure tform1.detali;
         begin
        with Form1.Image1.Canvas do
              begin
               brush.Color := rgb(222,184,135);
               textout(400,50,'Constantin Adrian');
               textout(400,70,'Procesul de fabricare');
               textout(400,90,'a paintball-urilor');
               brush.Color := rgb(110,110,110);
               textout(330,420,'Depozit tavi');
               textout(380,580,'Depozit tavi');
               textout(380,580,'Depozit tavi');
               brush.Color := rgb(139,69,19);
               textout(480,430,'Cutie bile');
               textout(300,500,'Camera uscare');

              end;
        end;
end.




