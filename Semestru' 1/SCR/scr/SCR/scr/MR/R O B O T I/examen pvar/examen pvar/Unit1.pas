unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls;

type
  TForm1 = class(TForm)
    Image1: TImage;
    Timer1: TTimer;
    procedure Timer1Timer(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;
  i,j,ax1,ay1 : integer;
  si,sj:integer;
  f : integer = 1;
  mx1 : integer = 1;
  my1 : integer = 1;
  pass : integer = 0;
  side1 : Array[0..14,0..21] of Integer;
  side2 : Array[0..14,0..21] of Integer;
  k : integer = 15;
implementation

{$R *.dfm}



procedure TForm1.Timer1Timer(Sender: TObject);
begin
  with Form1.Image1.Canvas do
    begin

      case f of
          1 : begin
            ay1 := ay1 + my1;
            k := k + 1;
            if (k = 20) then
              begin
                side1[sj,si] :=  side1[sj,si] - 100;
                side2[sj,si] :=  side1[sj,si] - 220;
                si := si + my1;
                k := 0;
              end;
            if ((ay1 = 420 + pass * 6) AND (my1 = 1)) OR
               ((ay1 = pass * 6) AND (my1 = -1))
            then
              begin
                 f := 2;
                 side1[sj,si] :=  side1[sj,si] - 100;
                 side2[sj,si] :=  side1[sj,si] - 220;
              end;
          end;
          2 : begin
            if (pass = 14) then f := 0
            else
            begin
            ax1 := ax1 + 1;
            ay1 := ay1 + 1;
            if ax1 = 10 * (pass + 1) then
              begin
                if (my1 = 1) then
                  begin
                    my1 := -1;
                  end
                else
                  begin
                    my1 := 1;
                  end;
                sj := sj + 1;
                pass := pass + 1;
                f := 1;
              end;
              end;
          end;
      end;

      pen.Width := 5;
      brush.Color := clgreen;
      pen.Color := clwhite;
      rectangle(0,0,800,600);
      pen.Color := clblack;
      polygon([Point(250,50),Point(400,140),Point(400,585),Point(250,495)]);
      polygon([Point(400,140),Point(550,50),Point(550,495),Point(400,585)]);
      moveto(250,50);
      lineto(400,-30);
      lineto(550,50);
      pen.width := 1;
    //  brush.color := clblack;
      polygon([Point(250,40),Point(400,-30),Point(550,40),Point(400,130)]);
      pen.color := rgb(150,150,250);
      for j := 0 to 14 do
      for i := 0 to 21 do
        begin
           brush.color := side1[j][i];
           polygon([Point(250 + (j * 10),50 + (20 * i) + (j * 6)),
                    Point(260 + (j * 10),57 + (20 * i) + (j * 6)),
                    Point(260 + (j * 10),80 + (20 * i) + (j * 6)),
                    Point(250 + (j * 10),73 + (20 * i) + (j * 6))]);
           brush.color := side2[j][i];
           polygon([Point(550 - (j * 10),50 + (20 * i) + (j * 6)),
                    Point(540 - (j * 10),57 + (20 * i) + (j * 6)),
                    Point(540 - (j * 10),80 + (20 * i) + (j * 6)),
                    Point(550 - (j * 10),73 + (20 * i) + (j * 6))]);
        end;

        brush.color := rgb(50,50,50);
        pen.color := clblack;
        moveto(255 + ax1,50 + pass * 6);
        lineto(255 + ax1, 50 + ay1);
        polygon([Point(250 + ax1 ,50 + ay1),
                    Point(260 + ax1,57 + ay1),
                    Point(260 + ax1,80 + ay1),
                    Point(250 + ax1,73 + ay1)]);

        moveto(545 - ax1,50 + pass * 6);
        lineto(545 - ax1, 50 + ay1);
        polygon([Point(550 - ax1 ,50 + ay1),
                    Point(540 - ax1,57 + ay1),
                    Point(540 - ax1,80 + ay1),
                    Point(550 - ax1,73 + ay1)]);
    end;
end;


procedure TForm1.FormCreate(Sender: TObject);
begin
  for j := 0 to 14 do
      for i := 0 to 21 do
        begin
          side1[j,i] := rgb(100,100,180+(i+j)*2);
          side2[j,i] := rgb(130, 130,180+(i+j)*2);
        end;
end;

end.
