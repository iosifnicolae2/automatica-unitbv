unit simona;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls,StdCtrls;

type
  TForm1 = class(TForm)
    Image1: TImage;
    Timer1: TTimer;
    Button1: TButton;
    Button2: TButton;
    procedure Timer1Timer(Sender:TObject);

    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
   // procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1,Form2: TForm1;
  xs,ys,xo,yo,xa,ya,xb,yb,xm,ym,xp,yp:integer;
  x0,y0,x1,y1,x2,y2,x3,y3,x4,y4,t:integer;
  directie:boolean=true;
  t1,t2,t3,t4,t5,t6,t7,t8,t9,t10,t11,t12,t13,t14,t15,t16,t17,t18,t19,t20,t21,
  t22,t23,t24,t25,t26,t27,t28,t29,t30,t31,t32,t33,t34,t35,t36,t37,t38,t39,
  t40,t41,t42,t43,t44,t45,t46,t47,t48,t49,t50:integer;
  b1,b2,b3,b4,b5:integer;
  i,n:integer;
  buna:boolean=true;
  clBrown,clgreen2,clfundal,clblue1,clblue2 : TColor;
const
   tmax=3051;
implementation
{$R *.dfm}

procedure TForm1.Timer1Timer(Sender:TObject);
var j:integer;
begin
for j:=1 to 3 do
  with Form1.Image1.Canvas do
   begin

 fillrect(clientrect);
//desenarea bandei
xb:=0;
yb:=275;
clgreen2 := rgb(0,64,64);
clbrown:=rgb(196,98,0);
clfundal:=rgb(155,155,255);
clblue1:=rgb(0,64,128);
clblue2:=rgb(117,186,225);
brush.Color:=clblack;
rectangle(xb+232,yb+5,xb+217,yb+125);
brush.Color:=clred;
polygon([point(xb,yb),point(xb+227,yb),point(xb+178,yb+75),point(xb,yb+75)]);
brush.color:=clblack;
polygon([point(xb+170,yb+75),point(xb+180,yb+75),point(xb+232,yb+5),
point(xb+228,yb)]);
polygon([point(xb,yb+59),point(xb+185,yb+59),point(xb+175,yb+75),
point(xb,yb+75)]);
rectangle(xb+180,yb+75,xb+165,yb+150);

//desenarea rotitelor benzii
pen.Width:=2;
pen.Color:=clgray;
ellipse(xb+165,yb+59,xb+185,yb+75);
ellipse(xb+115,yb+59,xb+135,yb+75);
ellipse(xb+65,yb+59,xb+85,yb+75);
ellipse(xb+15,yb+59,xb+35,yb+75);

//miscarea roti?elor benzii
if ((t>=0)and(t<=200)) or ((t>=577) and (t<=620)or (t>=1553)and (t<=1578)) then
begin
x1:=round(xb+175+8*cos(t/10));
y1:=trunc(yb+67+8*sin(t/10));
x2:=round(xb+125+8*cos(t/10));
y2:=trunc(yb+67+8*sin(t/10));
x3:=round(xb+75+8*cos(t/10));
y3:=trunc(yb+67+8*sin(t/10));
x4:=round(xb+25+8*cos(t/10));
y4:=trunc(yb+67+8*sin(t/10));
end;

//liniile benzii
moveto(xb+175,yb+67);
lineto(x1,y1);
moveto(xb+125,yb+67);
lineto(x2,y2);
moveto(xb+75,yb+67);
lineto(x3,y3);
moveto(xb+25,yb+67);
lineto(x4,y4);
pen.Width:=1;
pen.Color:=clblack;
pen.width:=2;
Moveto(xb+188+b1,yb);
Lineto(xb+138+b1,yb+59);
Moveto(xb+148+b1,yb);
Lineto(xb+98+b1,yb+59);
Moveto(xb+108+b1,yb);
Lineto(xb+58+b1,yb+59);
Moveto(xb+68+b1,yb);
Lineto(xb+18+b1,yb+59);
Moveto(xb+28+b1,yb);
Lineto(xb-22+b1,yb+59);
pen.Width:=1;

//miscarea liniilor benzii
if ((t>=0)and(t<=200)) or ((t>=577) and (t<=620)or (t>=1553)and (t<=1578)) then
begin
    b1:=b1+1;
if (t mod 40=0) then
   begin
    b1:=0;
   end;
 end;


//masa de ansamblare
xm:=415;
ym:=275;
brush.color:=clred;
polygon([point(xm,ym),point(xm+80,ym),point(xm+40,ym+60),point(xm-40,ym+60),
point(xm,ym)]);
brush.color:=clblack;
rectangle(xm-40,ym+60,xm-25,ym+150);
rectangle(xm-15,ym+61,xm,ym+125);
rectangle(xm-40,ym+60,xm+40,ym+70);
brush.color:=clred;
polygon([point(xm+40,ym+60),point(xm+200,ym+60),point(xm+200,ym),point(xm+80,ym),
point(xm+40,ym+60)]);

//trapa de la atelierul de reparatii
pen.Width:=3;
polygon([point(xm+40,ym+59),point(xm+155,ym+59),point(xm+195,ym),point(xm+80,ym),
point(xm+40,ym+59)]);
pen.Width:=1;
if(t>=2897) and (t<3050) and (buna=false) then   //deschiderea trapei
begin
brush.color:=clblack ;
polygon([point(xm+40,ym+60),point(xm+155,ym+60),point(xm+195,ym),point(xm+80,ym),
point(xm+40,ym+60)]);
end;

//robot pt deplasarea lampii de pe masa de ansamblare
if (t>=2697) and (t<=2897) then
begin
brush.color:=clgreen;
rectangle(xm-5,ym+21,xm+t43-t46,ym+14);
rectangle(xm-5,ym+25,xm,ym+10);
end;

//robot principal
x0:=250;
y0:=325;
brush.color:=clgreen;
//suport robot principal
polygon([point(x0+40,y0),point(x0+40,y0-145),point(x0-t2+t5-t14+t18-t26,y0-145),
point(x0-t2+t5-t14+t18-t26,y0-160),point(x0+40,y0-160),
point(x0+40,y0-185),point(x0-10-t28+t31-t37,y0-185),point(x0-10-t28+t31-t37,y0-200),
point(x0+75+t33-t35,y0-200),point(x0+75+t33-t35,y0-185),point(x0+60,y0-185),
point(x0+60,y0-160),point(x0+69+t7-t12+t20-t24,y0-160),
point(x0+69+t7-t12+t20-t24,y0-145),point(x0+60,y0-145),point(x0+60,y0)]);
rectangle(x0-10,y0-10,x0+90,y0+100);
polygon([point(x0+90,y0+100),point(x0+100,y0+90),point(x0+100,y0-20),
point(x0,y0-20),point(x0-10,y0-10),point(x0+90,y0-10)]);
Moveto(x0+100,y0-20);
Lineto(x0+90,y0-10);
//brat robot jos pentru preluarea becului si a suportului lampii
polygon([point(x0-5-t2+t5-t14+t18-t26+t6-t13+t19-t25+t7-t12+t20-t24,y0-135+t3+t15-t17+t21-t23-t11),
point(x0+3-t2+t5-t14+t18-t26+t6-t13+t19-t25+t7-t12+t20-t24,y0-135+t3+t15-t17+t21-t23-t11),
point(x0+3-t2+t5-t14+t18-t26+t6-t13+t19-t25+t7-t12+t20-t24,y0-145)
,point(x0+12-t2+t5-t14+t18-t26+t6-t13+t19-t25+t7-t12+t20-t24,y0-145),
point(x0+12-t2+t5-t14+t18-t26+t6-t13+t19-t25+t7-t12+t20-t24,y0-135+t3+t15-t17+t21-t23-t11),
point(x0+20-t2+t5-t14+t18-t26+t6-t13+t19-t25+t7-t12+t20-t24,y0-135+t3+t15-t17+t21-t23-t11),
point(x0+20-t2-t4-t16+t22+t10+t5-t14+t18-t26+t6-t13+t19-t25+t7-t12+t20-t24,y0-115+t3+t15-t17+t21-t23-t11),
point(x0+15-t2+t5-t14+t18-t26+t6-t13+t19-t25+t7-t12+t20-t24,y0-130+t3+t15-t17+t21-t23-t11),
point(x0-t2+t5-t14+t18-t26+t6-t13+t19-t25+t7-t12+t20-t24,y0-130+t3+t15-t17+t21-t23-t11),
point(x0-5-t2+t4+t16-t22-t10+t5-t14+t18-t26+t6-t13+t19-t25+t7-t12+t20-t24,y0-115+t3+t15-t17+t21-t23-t11),
point(x0-5-t2+t5-t14+t18-t26+t6-t13+t19-t25+t7-t12+t20-t24,y0-135+t3+t15-t17+t21-t23-t11)]);
//brat robot sus pentru preluarea abajurului
rectangle(x0-7-t28+t31+t32+t33-t35-t36-t37,y0-185,
x0-2-t28+t31+t32+t33-t35-t36-t37,y0-175+t29-t30-t34);
roundrect(x0-35-t28+t31+t32+t33-t35-t36-t37,y0-175+t29-t30-t34,
x0+25-t28+t31+t32+t33-t35-t36-t37,y0-165+t29-t30-t34,
x0-36-t28+t31+t32+t33-t35-t36-t37,y0-170-t30-t34);

//suport lampa
brush.color:=clbrown;
xs:=-50;
ys:=300;
rectangle(xs+t1+t5+t6+t7+t43+t44,ys+t45,
xs+45+t1+t5+t6+t7+t43+t44,ys-15+t45);
polygon([point(xs+15+t1+t5+t6+t7+t43+t44,ys-15+t45),
point(xs+15+t1+t5+t6+t7+t43+t44,ys-75+t45),
point(xs+18+t1+t5+t6+t7+t43+t44,ys-75+t45),
point(xs+18+t1+t5+t6+t7+t43+t44,ys-80+t45),
point(xs+27+t1+t5+t6+t7+t43+t44,ys-80+t45),
point(xs+27+t1+t5+t6+t7+t43+t44,ys-75+t45),
point(xs+30+t1+t5+t6+t7+t43+t44,ys-75+t45),
point(xs+30+t1+t5+t6+t7+t43+t44,ys-15+t45)]);
//stecher
brush.color:=clblack;
rectangle(xs+45+t1+t5+t6+t7+t43+t44,ys-3+t45,
xs+48+t1+t5+t6+t7+t43+t44,ys-12+t45);
Moveto(xs+48+t1+t5+t6+t7+t43+t44,ys-5+t45);
pen.width:=2;
Lineto(xs+54+t1+t5+t6+t7+t43+t44,ys-5+t45);
Moveto(xs+48+t1+t5+t6+t7+t43+t44,ys-10+t45);
Lineto(xs+54+t1+t5+t6+t7+t43+t44,ys-10+t45);
pen.Width:=1;

//bec
brush.color:=clblack;
xo:=-75;
yo:=300;
rectangle(xo+t1+t8+t18+t19+t20+t43+t44,yo-t17+t21+t45,
xo+t1+9+t8+t18+t19+t20+t43+t44,yo-5-t17+t21+t45);
brush.color:=clyellow;
ellipse(xo-3+t1+t8+t18+t19+t20+t43+t44,yo-3-t17+t21+t45,
xo+12+t1+t8+t18+t19+t20+t43+t44,yo-17-t17+t21+t45);

// abajur
if (t>=2468) and (t<=2553) and (buna=true) then
brush.color:=clblue2
else
brush.color:=clblue1;
xa:=-170;
ya:=300;
polygon([point(xa+t1+t9+t27+t31+t32+t33+t43+t44,ya-t30+t45),
point(xa+15+t1+t9+t27+t31+t32+t33+t43+t44,ya-70-t30+t45),
point(xa+60+t1+t9+t27+t31+t32+t33+t43+t44,ya-70-t30+t45),
point(xa+75+t1+t9+t27+t31+t32+t33+t43+t44,ya-t30+t45),
point(xa+t1+t9+t27+t31+t32+t33+t43+t44,ya-t30+t45)]);

//atelier reparatii,sector de Ómpachetare
brush.color:=clgreen;
rectangle(xm+40,ym+60,xm+350,ym+150);
brush.color:=clyellow;
ellipse(xm+82,ym+100,xm+92,ym+110);
TextOut(xm+100,ym+100,'REPAIR');
brush.color:=clgreen;
rectangle(xm+200,ym+60,xm+350,ym-100);
brush.color:=clyellow;
ellipse(xm+232,ym-60,xm+242,ym-50);
TextOut(xm+250,ym-60,'PACKING');
brush.Color:=clgreen2;
polygon([point(xm+200,ym-100),point(xm+220,ym-120),point(xm+350,ym-120),
point(xm+350,ym-100),point(xm+200,ym-100)]);

//sursa de tensiune
brush.color:=clgreen;
xp:=450;
yp:=100;
rectangle(xp+40,yp+30,xp+50,yp+60+t38-t42);
rectangle(xp,yp,xp+90,yp+37);
brush.color:=clyellow;
ellipse(xp+10,yp+7,xp+20,yp+17);
TextOut(xp+28,yp+7,'POWER');
TextOut(xp+35,yp+19,'220V');
brush.color:=clgreen;
rectangle(xp+50,yp+60+t38-t42,xp+30-t39+t41,yp+69+t38-t42);

if directie then
  t:=t+1
else
  t:=t-1;
if (t=0) or (t=tmax) then
begin
  directie:=not directie;
end;
//timpi ansamblare lampa
if (t<=200) then
t1:=t
else if (t<=285) then
t2:=t-200
else if (t<=307) then
t3:=t-285
else if (t<=312) then
t4:=t-307
else if (t<=437) then
t5:=t-312
else if (t<=452) then
t6:=t-437
else if (t<=577) then
t7:=t-452
else if (t<=620) then
begin
t8:=t-577;
t9:=t-577;
end
else if (t<=625)then
t10:=t-620
else if (t<=647) then
t11:=t-625
else if (t<=772) then
t12:=t-648
else if (t<=787) then
t13:=t-773
else if (t<=915) then
t14:=t-788
else if (t<=1000) then
t15:=t-915
else if (t<=1005) then
t16:=t-1000
else if (t<=1090) then
t17:=t-1005
else if (t<=1215) then
t18:=t-1091
else if (t<=1230) then
t19:=t-1216
else if (t<=1358) then
t20:=t-1231
else if (t<=1367) then
t21:=t-1358
else if (t<=1372) then
t22:=t-1367
else if (t<=1381) then
t23:=t-1372
else if (t<=1506) then
t24:=t-1382
else if (t<=1521) then
t25:=t-1507
else if (t<=1553) then
t26:= t-1522
else if (t<=1578) then
t27:=t-1553
else if (t<=1688) then
t28:=t-1578
else if (t<=1758) then
t29:=t-1688
else if (t<=1798) then
t30:=t-1758
else if (t<=1953) then
t31:=t-1793
else if (t<=1973) then
t32:=t-1955
else if (t<=2098) then
t33:=t-1973
else if (t<=2128) then
t34:=t-2099
else if (t<=2258) then
t35:=t-2127
else if (t<=2263) then
t36:=t-2260
else if (t<=2323) then
t37:=t-2264
//timpi verificare functionare lampa
else if (t<=2451) then
t38:=t-2323
else if (t<=2468) then
t39:=t-2451
else if (t<=2553) then
t40:=t-2468
else if (t<=2569) then
t41:=t-2554
else if (t<=2697) then
t42:=t-2570
//timpi deplasare lampa de pe masa de ansamblare
else if (t<=2797) then
t43:=t-2698
else if (t<=2897) then
t46:=t-2797
//timpi deplasare lampa catre sectorul de Ómpachetare sau atelierul de reparatii
else if (t<3050) then
begin
if (buna=true) then
t44:=t-2897
else
begin
t45:=t-2897;
end;
end
else
begin
n:=n+1;
if n mod 3=0 then
buna:=false
else buna:=true;
//reluarea procesului
t1:=0;t2:=0;t3:=0;t4:=0;t5:=0;t6:=0;t7:=0;t8:=0;t9:=0;t10:=0;t11:=0;t12:=0;
t13:=0;t14:=0;t15:=0;t16:=0;t17:=0;t18:=0;t19:=0;t20:=0;t21:=0;t22:=0;t23:=0;
t24:=0;t25:=0;t26:=0;t27:=0;t28:=0;t29:=0;t30:=0;t31:=0;t32:=0;t33:=0;t34:=0;
t35:=0;t36:=0;t37:=0;t38:=0;t39:=0;t40:=0;t41:=0;t42:=0;t43:=0;t44:=0;t45:=0;
t46:=0;t:=0;
end;   
brush.color:=clfundal;
 end;
end;

//buton pornire/oprire
procedure TForm1.Button1Click(Sender: TObject);
begin
timer1.Enabled:=not(timer1.Enabled);
end;

//buton Ónchidere fereastr?
procedure TForm1.Button2Click(Sender: TObject);
begin
Form1.Close;
end;

end.










