unit laborator;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls;

type
  TForm1 = class(TForm)
    Timer1: TTimer;
    Image1: TImage;

   procedure Timer1Timer(Sender:TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;
  toth,toth2,q1,q2,q3,x1s,y1s,x2s,y2s,x1d,y1d,x2d,y2d,x1ds,y1ds,x2ds,y2ds,
  x3,y3,x4,y4,a,b,c,d,g,h,i,j,l,m,n,p,ct,bl,ble,b2,b3,X:integer;
  // pt animatie
  t0,t1,t2,t3,t4,t5,t6,t7,t8,t9,t10,t11,t12,t13,t14,t15:integer;
  faza:integer=0;  //traiectorie

  directie:boolean=true;
  const
  r1=100;
  r2=60;


implementation
procedure TForm1.Timer1Timer(Sender:TObject);
begin
    with Form1.Image1.Canvas do
          begin

          fillrect(clientrect);  //pt a scoate traiectoria; se chimba fundalul
          pen.Width:=2;



          Brush.Color:=RGB(131,139,131);
          Polygon([Point(505,800), Point(550,600),Point(1630,600),Point(1630,800)]);         //banda 1

          Polygon([Point(50,500), Point(250,500),Point(250,1008),Point(50,1008)]);                   //banda 2

          Brush.Color:=RGB(220,220,220);          //banda banda 1
          Roundrect(500,800,1630,820,20,20);

          Brush.Color:=RGB(255,250,250);

          ellipse(500,800,520,820);

          x1s:=round(510-10*sin(+t4/10));
          y1s:=round(810-10*cos(+t4/10));
          x2s:=round(510+10*sin(+t4/10));
          y2s:=round(810+10*cos(+t4/10));
          moveto(510,810);lineto(x1s,y1s);
          moveto(510,810);lineto(x2s,y2s);

          ellipse(700,800,720,820);

          x1s:=round(710-10*sin(+t4/10));
          y1s:=round(810-10*cos(+t4/10));
          x2s:=round(710+10*sin(+t4/10));
          y2s:=round(810+10*cos(+t4/10));
          moveto(710,810);lineto(x1s,y1s);
          moveto(710,810);lineto(x2s,y2s);

          ellipse(900,800,920,820);

          x1s:=round(910-10*sin(+t4/10));
          y1s:=round(810-10*cos(+t4/10));
          x2s:=round(910+10*sin(+t4/10));
          y2s:=round(810+10*cos(+t4/10));
          moveto(910,810);lineto(x1s,y1s);
          moveto(910,810);lineto(x2s,y2s);

          ellipse(1100,800,1120,820);

          x1s:=round(1110-10*sin(+t4/10));
          y1s:=round(810-10*cos(+t4/10));
          x2s:=round(1110+10*sin(+t4/10));
          y2s:=round(810+10*cos(+t4/10));
          moveto(1110,810);lineto(x1s,y1s);
          moveto(1110,810);lineto(x2s,y2s);

          ellipse(1300,800,1320,820);

          x1s:=round(1310-10*sin(+t4/10));
          y1s:=round(810-10*cos(+t4/10));
          x2s:=round(1310+10*sin(+t4/10));
          y2s:=round(810+10*cos(+t4/10));
          moveto(1310,810);lineto(x1s,y1s);
          moveto(1310,810);lineto(x2s,y2s);

          ellipse(1500,800,1520,820);

          x1s:=round(1510-10*sin(+t4/10));
          y1s:=round(810-10*cos(+t4/10));
          x2s:=round(1510+10*sin(+t4/10));
          y2s:=round(810+10*cos(+t4/10));
          moveto(1510,810);lineto(x1s,y1s);
          moveto(1510,810);lineto(x2s,y2s);




          Brush.Color:=RGB(0,0,128);
          Polygon([point(1300,0),Point(1250,150),Point(1580,150), Point(1620,0)]);        //tonomat capace
          Polygon([point(1250,150),Point(1580,150),Point(1580,200),Point(1250,200)]);
          Polygon([point(1620,0),Point(1580,150),Point(1580,200),Point(1620,150)]);

          Brush.Color:=RGB(255,99,71);
          POlygon([point(1350,150),Point(1520,150),Point(1470,100),Point(1400,100)]);    //  iesire capace

          Brush.Color:=RGB(0,139,131);
          Polygon([Point(1300,150),Point(1570,150),Point(1470,500),Point(1200,500)]);    //banda 3

          Brush.Color:=RGB(255,0,0);                                                       //brat
          POlygon([point(350,700),point(400,700),point(400,300),point(350,300)]);
          Polygon([point(0,300),Point(1650,300),Point(1650,250),Point(0,250)]);

          Brush.Color:=RGB(240,128,128);                                                //   suport brat
          POlygon([Point(1400-t2,275),Point(1300-t2,275),Point(1300-t2,350),Point(1400-t2,350)]);
          Polygon([Point(1320-t2,350),point(1380-t2,350),Point(1380-t2,380),Point(1320-t2,380)]);  // suport extensibil
          Polygon([Point(1330-t2,380),Point(1370-t2,380),Point(1370-t2,430+t1+t3-t15),Point(1330-t2,430+t1+t3-t15)]);


          Polygon([Point(700-t10,275),Point(600-t10,275),Point(600-t10,350),Point(700-t10,350)]);     //suport brat 2
          Polygon([Point(680-t10,350),Point(620-t10,350),Point(620-t10,380),Point(680-t10,380)]);
          Polygon([Point(630-t10,380),Point(670-t10,380),Point(670-t10,430+t5-t9+t11-t13),Point(630-t10,430+t5-t9+t11-t13)]);

          Moveto(584-t10,430+t5-t9+t11-t13);                                                             //sistem de prindere
          lineto(720-t10,430+t5-t9+t11-t13);

          moveto(584+t8-t10-t12,430+t5-t9+t11-t13);
          lineto(584+t8-t10-t12,480+t5-t9+t11-t13);

          moveto(720-t8-t10+t12,430+t5-t9+t11-t13);
          lineto(720-t8-t10+t12,480+t5-t9+t11-t13);

          Brush.Color:=RGB(255,255,0);
          POlygon([Point(1335-t2-t4-t10,470+t3-t9+t11+t14),Point(1365-t2-t4-t10,470+t3-t9+t11+t14),Point(1365-t2-t4-t10,450+t3-t9+t11+t14),Point(1335-t2-t4-t10,450+t3-t9+t11+t14)]); //capac

          moveto(1500-t0-t4-t10,750-t9+t11+t14);        //sticla 1
          lineto(1500-t0-t4-t10,650-t9+t11+t14);

          moveto(1500-t0-t4-t10,650-t9+t11+t14);
          lineto(1480-t0-t4-t10,630-t9+t11+t14);

          moveto(1480-t0-t4-t10,630-t9+t11+t14);
          lineto(1480-t0-t4-t10,620-t9+t11+t14);

          moveto(1500-t0-t4-t10,750-t9+t11+t14);
          lineto(1430-t0-t4-t10,750-t9+t11+t14);

          moveto(1430-t0-t4-t10,750-t9+t11+t14);
          lineto(1430-t0-t4-t10,650-t9+t11+t14);

          moveto(1430-t0-t4-t10,650-t9+t11+t14);
          lineto(1450-t0-t4-t10,630-t9+t11+t14);

          moveto(1450-t0-t4-t10,630-t9+t11+t14);
          lineto(1450-t0-t4-t10,620-t9+t11+t14);
        {
          Moveto(1200,750);   //sticla 2
          lineto(1200,650);

          moveto(1200,650);
          lineto(1180,630);

          moveto(1180,630);
          lineto(1180,620);

          moveto(1200,750);
          lineto(1130,750);

          moveto(1130,750);
          lineto(1130,650);

          moveto(1130,650);
          lineto(1150,630);

          moveto(1150,630);
          lineto(1150,620);

          moveto(900,750);  //sticla 3
          lineto(900,650);

          moveto(900,650);
          lineto(880,630);

          moveto(880,630);
          lineto(880,620);

          moveto(900,750);
          lineto(830,750);

          moveto(830,750);
          lineto(830,650);

          moveto(830,650);
          lineto(850,630);

          moveto(850,630);
          lineto(850,620);
         }
          ellipse(1225-t2,250,1275-t2,300); //rotile pt bratul 1
          ellipse(1425-t2,250,1475-t2,300);

          ellipse(525-t10,250,575-t10,300);
          ellipse(725-t10,250,775-t10,300);

          Brush.Color:=RGB(0,0,255);
          pen.Width:=4;

          Moveto(1250-t2,275);      //line brat1
          lineto(1450-t2,275);

          moveto(550-t10,275);  //line brat2
          lineto(750-t10,275);










          Brush.Color:=RGB(0,255,255);







         {
          //animatie
          if directie then t:=t+1

          else t:=t-2;
          //if (t=tmax) or (t=0) then
         // directie:= not directie;
          brush.color:=clwhite     // se readuce fundalul la culoarea dorita

                }
          case faza of
          0:
          begin
          t0:=t0+3;
          if(t0>200) then faza:=1;
          end;
           1:
           begin
          t1:=t1+3;
          if (t1>20)  then faza :=2;
          end;
          2:
           begin
          t2:=t2+3;
          if (t2>85)  then faza :=3;
          end;
          3:
          begin
          t3:=t3+3;
          if (t3>157)  then faza :=4;
          end;
          4:
           begin
          t4:=t4+3;
          if (t4>611)  then faza :=5;
          end;
          5:
           begin
          t5:=t5+3;
          if (t5>175)  then faza :=6;
          end;
          6:
           begin
          t6:=t6+3;
          if (t6>50)  then faza :=7;
          end;
          7:
           begin
          t7:=t7+3;
          if (t7>175)then faza :=8;
          end;
          8:
           begin
          t8:=t8+3;
          if (t8>32)  then faza :=9;
          end;
          9:
           begin
          t9:=t9+3;
          if (t9>175)  then faza :=10;
          end;
          10:
           begin
          t10:=t10+3;
          if (t10>500)  then faza :=11;
          end;
          11:
           begin
          t11:=t11+3;
          if (t11>100)  then faza :=12;
          end;
          12:
           begin
          t12:=t12+3;
          if (t12>32)  then faza :=13;
          end;
          13:
           begin
          t13:=t13+3;
          if (t13>90)  then faza :=14;
          end;
          14:
           begin
          t14:=t14+3;
          if (t14>450)  then faza :=15;
          end;
          15:
           begin
          t15:=t15+3;
          if (t15>175)  then faza :=16;
          end;
          end;

end;

{$R *.dfm}

end;
end.
