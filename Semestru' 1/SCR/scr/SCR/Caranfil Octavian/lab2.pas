unit lab2;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls;

type
  TForm1 = class(TForm)
    Timer1: TTimer;
    Image1: TImage;
    procedure Timer1Timer(Sender:TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;
  toth,toth2,q1,q2,q3,x1s,y1s,x2s,y2s,x1d,y1d,x2d,y2d,x1ds,y1ds,x2ds,y2ds,
  x3,y3,x4,y4,a,b,c,d,g,h,i,j,l,m,n,p,ct,bl,ble,b2,b3,X:integer;
  k:integer=0;
  e1:integer=0;
  e2:integer=0;
  e3:integer=0;
  e4:integer=0;
  f1:integer=0;
  f2:integer=0;
  f3:integer=0;
  f4:integer=0;
  r:integer=0;
  s:integer=0;
  t:integer=0;
  u2:integer=0;
  u:integer=-1400;
  v:integer=230;
  w:integer=-1180;
  SEL:integer=1;
  directie:boolean=true;
  directie2:boolean=true;
  start:boolean=false;
  start2:boolean=false;
  ajunsCutie:boolean=true;
const
  tothmax=4460;
implementation
procedure TForm1.Timer1Timer(Sender:TObject);
  begin

    with form1.Image1.Canvas do
      begin

      //stergere elemente mutate
      fillrect(clientrect);

      //poligon incadrare
      pen.Width:=2;
      pen.Color:=clblack;
      brush.color:=rgb(255,255,255);
      polygon([point(0,0),point(1265,0),point(1265,737),point(0,737),point(0,0)]);
      pen.Color:=clblack;

      // banda cutii
      pen.Width:=2;
      pen.Color:=clblack;
      polygon([point(0,300),point(1400,300)]);
      polygon([point(0,360),point(1400,360)]);
      brush.color:=clgray;
      rectangle(0,300,1400,360);


      //podea
      brush.color:=rgb(0,69,19);
      rectangle(1,700,1264,730);
      brush.color:=clwhite;




          // MASA lucru *
            brush.color:=clgray;
        polygon([point(452,610),point(705,610),point(742,550),point(489,550)]);
        polygon([point(705,630),point(705,610),point(742,550),point(742,565)]);
        rectangle(452,610,705,630);

      brush.color:=clblack;
      polygon([point(452,630),point(705,630),point(742,565),point(742,630),point(705,700),point(452,700)]);
      brush.color:=clwhite;

        

      //banda rulanta stanga *
      pen.Color:=clblack;
      pen.width:=2;
      brush.color:=clgray;
      polygon([point(200,610),point(230,550),point(237,549),point(243,555),point(209,623),point(200,610)]);
      polygon([point(0,550),point(230,550),point(200,610),point(-1,610),point(0,610)]);
      brush.color:=clwhite;
      ellipse (189,609,211,631);
      x1s:=round(200-10*sin(-q1/10));
      y1s:=round(620-10*cos(-q1/10));
      x2s:=round(200+10*sin(-q1/10));
      y2s:=round(620+10*cos(-q1/10));
      moveto(200,620);lineto(x1s,y1s);
      moveto(200,620);lineto(x2s,y2s);
      moveto(0,630);lineto(200,630);
      moveto(-100+q1,610);lineto(-70+q1,550);
      moveto(-40+q1,610);lineto(-10+q1,550);
      moveto(20+q1,610);lineto(50+q1,550);
      moveto(80+q1,610);lineto(110+q1,550);
      moveto(140+q1,610);lineto(170+q1,550);

      //suport banda rulanta stanga  *
      brush.color:=clblack;
      polygon([point(0,630),point(205,630),point(242,555),point(242,635),point(205,700),point(0,700)]);
      brush.color:=clwhite;

      //banda rulanta dreapta *
      pen.Color:=clblack;
      pen.width:=2;
      brush.Color:=clgray;
      polygon([point(1000,610),point(1265,610),point(1265,550),point(1030,550),point(1000,610)]);
      polygon([point(990,615),point(1020,555),point(1030,550),point(1000,610),point(990,615)]);
      brush.Color:=clwhite;
      ellipse (989,609,1011,631);
      x1d:=round(1000-10*sin(-q2/10));
      y1d:=round(620-10*cos(-q2/10));
      x2d:=round(1000+10*sin(-q2/10));
      y2d:=round(620+10*cos(-q2/10));
      moveto(1000,620);lineto(x1d,y1d);
      moveto(1000,620);lineto(x2d,y2d);
      moveto(1000,630);lineto(1265,630);
      moveto(1000+q2,610);lineto(1030+q2,550);
      moveto(1060+q2,610);lineto(1090+q2,550);
      moveto(1120+q2,610);lineto(1150+q2,550);
      moveto(1180+q2,610);lineto(1210+q2,550);
      moveto(1240+q2,610);lineto(1270+q2,550);
      moveto(1300+q2,610);lineto(1330+q2,550);


      //suport banda rulanta dreapta  *
      brush.color:=clblack;
      polygon([point(995,630),point(1265,630),point(1265,700),point(995,700)]);
      polygon([point(0,630),point(205,630),point(242,555),point(242,635),point(205,700),point(0,700)]);
      brush.color:=rgb(210,105,30);


           //carcasa o parte
           polygon([point(1040+w,170+v),point(1020+w,205+v),point(1020+w,355+v),point(1040+w,320+v)]);

      c:=0;
      brush.Color:=clwhite;
      rectangle(30+c+u+u2,270,200+c+u+u2,320);
      brush.color:=rgb(210,105,30);
      polygon([point(40+c+u+f1,310+e1),point(20+c+u+f1,345+e1),point(120+c+u+f1,345+e1),point(140+c+u+f1,310+e1)]);
      c:=300;
      brush.Color:=clwhite;
      rectangle(30+c+u+u2,270,200+c+u+u2,320);
      brush.color:=rgb(210,105,30);
      polygon([point(40+c+u+f2,310+e2),point(20+c+u+f2,345+e2),point(120+c+u+f2,345+e2),point(140+c+u+f2,310+e2)]);
      c:=700;
      brush.Color:=clwhite;
      rectangle(30+c+u+u2,270,200+c+u+u2,320);
      brush.color:=rgb(210,105,30);
      polygon([point(40+c+u+f3,310+e3),point(20+c+u+f3,345+e3),point(120+c+u+f3,345+e3),point(140+c+u+f3,310+e3)]);
      c:=1000;
      brush.Color:=clwhite;
      rectangle(30+c+u+u2,270,200+c+u+u2,320);
      brush.color:=rgb(210,105,30);
      polygon([point(40+c+u+f4,310+e4),point(20+c+u+f4,345+e4),point(120+c+u+f4,345+e4),point(140+c+u+f4,310+e4)]);


      brush.Color:=rgb(210,105,30);
      // carcasa

             polygon([point(1040+w,320+v),point(1020+w,355+v),point(1120+w,355+v),point(1140+w,320+v)]);
            polygon([point(1120+w,355+v),point(1120+w,205+v),point(1140+w,170+v),point(1140+w,320+v)]);

      //brat robot
      rectangle(0,105,1950,155);
      pen.Width:=2;
      brush.color:=clgray;
      rectangle(800+r-X,250+t,850+r+X,255+t);
      rectangle(820+r,210,830+r,250+t);
      rectangle(810+r,160,840+r,210+s);
      rectangle(800+r,0,850+r,160);
      brush.Color:=clwhite;

                // cutie1
             c:=0;

           polygon([point(30+c+u+u2,270),point(10+c+u+u2,305),point(10+c+u+u2,355),point(30+c+u+u2,320)]);
            //placuta
           rectangle(10+c+u+u2,305,180+c+u+u2,355);
           polygon([point(180+c+u+u2,355),point(180+c+u+u2,305),point(200+c+u+u2,270),point(200+c+u+u2,320)]);

          // citie2
             c:=300;

           polygon([point(30+c+u+u2,270),point(10+c+u+u2,305),point(10+c+u+u2,355),point(30+c+u+u2,320)]);
            //placuta
           rectangle(10+c+u+u2,305,180+c+u+u2,355);
           polygon([point(180+c+u+u2,355),point(180+c+u+u2,305),point(200+c+u+u2,270),point(200+c+u+u2,320)]);

          // citie3
             c:=700;

           polygon([point(30+c+u+u2,270),point(10+c+u+u2,305),point(10+c+u+u2,355),point(30+c+u+u2,320)]);
            //placuta
           rectangle(10+c+u+u2,305,180+c+u+u2,355);
           polygon([point(180+c+u+u2,355),point(180+c+u+u2,305),point(200+c+u+u2,270),point(200+c+u+u2,320)]);

           // citie4
             c:=1000;

           polygon([point(30+c+u+u2,270),point(10+c+u+u2,305),point(10+c+u+u2,355),point(30+c+u+u2,320)]);
            //placuta
           rectangle(10+c+u+u2,305,180+c+u+u2,355);
           polygon([point(180+c+u+u2,355),point(180+c+u+u2,305),point(200+c+u+u2,270),point(200+c+u+u2,320)]);

          //start cutii
       if start then
       begin
           u:=u+2
       end;

       if start2 then
       begin
           u2:=u2+2
       end;

       if u = 0 then
       begin
              start:=false
       end ;



    //temporizare miscari

    case SEL of

    1 :
    begin

    if w = -900 then
      begin
             SEL:=2;
      end
    else
      begin
         w:=w+1
       end ;

    if ajunsCutie then  //banda rulata 1
    begin
      if (toth>=0) and (toth<=60) then
      begin
          q1:=q1+1 ;
      end;
      if directie then
      begin
          toth:=toth+1;
      end;
      if toth=60 then
      begin
            toth:=0 ;
            q1:=0;
      end ;
      if (toth2>=0) and (toth2<=60) then    //banda rulata 2
      begin
          q2:=q2+1;
      end;
      if directie2 then
      begin
          toth2:=toth2+1 ;
      end;
      if toth2=60 then
      begin
            toth2:=0 ;
            q2:=0 ;
      end;
    end;

    end;//1

    2 :
    begin

    if r > -650then
      begin
          r:=r-2;
      end
    else
      begin
       SEL:=3;
      end;

    end; //2

    3 :
    begin

    if t < 320 then
      begin
        t:=t+4;
        s:=s+2 ;
      end
      else
      begin
        SEL:=4 ;
      end;

    end;

    4:
    begin

    if t > 100 then
      begin
        t:=t-4;
        s:=s-2;
        v:=v-4;
      end
      else
      begin
        SEL:=5 ;
      end;

    end;

    5:
    begin

    if r < -240 then
      begin
          r:=r+2;
          w:=w+2
      end
    else
      begin
       SEL:=6;
      end;

    end;

    6:
    begin

    if t < 320 then
      begin
        t:=t+4;
        s:=s+2;
        v:=v+4;
      end
      else
      begin
        SEL:=7 ;
      end;

    end;

    7:
    begin

      if t > 0 then
      begin
        t:=t-4;
        s:=s-2;
      end
      else
      begin
        SEL:=8 ;
        start:=true;
      end;

    end;

    8:
    begin
        if start = false then
        begin
          SEL:=9;
        end;
    end;

    9:  // miscare aprox la fel pt fiecare cutie
    begin

    if r > -440 then
      begin
          r:=r-2;
      end
    else
      begin
       SEL:=10;
      end;

    end;

    10:
    begin

    if t < 80 then
      begin
        t:=t+4;
        s:=s+2;
      end
      else
      begin
        SEL:=11 ;
      end;

    end;

    11:
    begin

      if t > 0 then
      begin
        t:=t-4;
        s:=s-2;
        e2:=e2-4;
      end
      else
      begin
        SEL:=12 ;
      end;

    end;

     12:
     begin

     if r < -232 then
      begin
          r:=r+2;
          f2:=f2+2;
      end
    else
      begin
       SEL:=13;
      end;

     end;

     13:
     begin

     if t < 280 then
      begin
        t:=t+4;
        s:=s+2;
        e2:=e2+4;
      end
      else
      begin
        SEL:=14 ;
      end;

     end;

     14:
     begin

     if t > 0 then
      begin
        t:=t-4;
        s:=s-2;
      end
      else
      begin
        SEL:=15 ;
      end;

     end;

    15:  // miscare aprox la fel pt fiecare cutie
    begin

    if r > -740 then  //unde merge prima data dupa cutie (stanga/dreapta si cat)
      begin
          r:=r-2;
      end
    else
      begin
       SEL:=16;
      end;

    end;

    16:
    begin

    if t < 80 then
      begin
        t:=t+4;
        s:=s+2;
      end
      else
      begin
        SEL:=17 ;
      end;

    end;

    17:
    begin

      if t > 0 then
      begin
        t:=t-4;
        s:=s-2;
        e1:=e1-4;
      end
      else
      begin
        SEL:=18 ;
      end;

    end;

     18:
     begin

     if r < -232 then    //pozitia initiala in care se intoarce
      begin
          r:=r+2;
          f1:=f1+2;
      end
    else
      begin
       SEL:=19;
      end;

     end;

     19:
     begin

     if t < 240 then     //cat coboara ; scade cu -40
      begin
        t:=t+4;
        s:=s+2;
        e1:=e1+4;
      end
      else
      begin
        SEL:=20 ;
      end;

     end;

     20:
     begin

     if t > 0 then
      begin
        t:=t-4;
        s:=s-2;
      end
      else
      begin
        SEL:=21 ;
      end;

     end;

     21:  // miscare aprox la fel pt fiecare cutie
    begin

    if r < -40 then  //unde merge prima data dupa cutie (stanga/dreapta si cat)
      begin
          r:=r+2;
      end
    else
      begin
       SEL:=22;
      end;

    end;

    22:
    begin

    if t < 80 then
      begin
        t:=t+4;
        s:=s+2;
      end
      else
      begin
        SEL:=23 ;
      end;

    end;

    23:
    begin

      if t > 0 then
      begin
        t:=t-4;
        s:=s-2;
        e3:=e3-4;
      end
      else
      begin
        SEL:=24 ;
      end;

    end;

     24:
     begin

     if r > -232 then    //pozitia initiala in care se intoarce
      begin
          r:=r-2;
          f3:=f3-2;
      end
    else
      begin
       SEL:=25;
      end;

     end;

     25:
     begin

     if t < 200 then     //cat coboara ; scade cu -40
      begin
        t:=t+4;
        s:=s+2;
        e3:=e3+4;
      end
      else
      begin
        SEL:=26 ;
      end;

     end;

     26:
     begin

     if t > 0 then
      begin
        t:=t-4;
        s:=s-2;
      end
      else
      begin
        SEL:=27 ;
      end;

     end;

     27:  // miscare aprox la fel pt fiecare cutie
    begin

    if r < 260 then  //unde merge prima data dupa cutie (stanga/dreapta si cat)
      begin
          r:=r+2;
      end
    else
      begin
       SEL:=28;
      end;

    end;

    28:
    begin

    if t < 80 then
      begin
        t:=t+4;
        s:=s+2;
      end
      else
      begin
        SEL:=29 ;
      end;

    end;

    29:
    begin

      if t > 0 then
      begin
        t:=t-4;
        s:=s-2;
        e4:=e4-4;
      end
      else
      begin
        SEL:=30 ;
      end;

    end;

     30:
     begin

     if r > -232 then    //pozitia initiala in care se intoarce
      begin
          r:=r-2;
          f4:=f4-2;
      end
    else
      begin
       SEL:=31;
      end;

     end;

     31:
     begin

     if t < 172 then     //cat coboara ; scade cu -40
      begin
        t:=t+4;
        s:=s+2;
        e4:=e4+4;

      end
      else
      begin
        SEL:=32 ;
      end;

     end;

     32:
     begin

     if t > 0 then
      begin
        t:=t-4;
        s:=s-2;

      end
      else
      begin
        SEL:=33 ;
      end;

     end;

     33:
     begin
         start2:=true;
         if u2 = 1400 then
         begin
          SEL:=34;
         end;
     end;

     34:
     begin

     if t < 184 then     //cat coboara ; scade cu -40
      begin
        t:=t+4;
        s:=s+2;
        //e4:=e4+4;
      end
      else
      begin
        SEL:=35 ;
      end;

     end;

     35:
     begin

     if t > 0 then
      begin
        t:=t-4;
        s:=s-2;
        v:=v-4;
        e1:=e1-4;
        e2:=e2-4;
        e3:=e3-4;
        e4:=e4-4;
      end
      else
      begin
        SEL:=36 ;
      end;

     end;

    36:           //MUTA CUTIA  FINAL
     begin

     if r < 300 then
      begin
          r:=r+2;
          w:=w+2;
        f1:=f1+2;
        f2:=f2+2;
        f3:=f3+2;
        f4:=f4+2;
      end
    else
      begin
       SEL:=37;
      end;

     end;

     37:
     begin

     if t < 172 then     //cat coboara ; scade cu -40
      begin
        t:=t+4;
        s:=s+2;
        v:=v+4;
        e1:=e1+4;
        e2:=e2+4;
        e3:=e3+4;
        e4:=e4+4;
      end
      else
      begin
        SEL:=38 ;
      end;

     end;

     38:
     begin

     if t > 0 then
      begin
        t:=t-4;
        s:=s-2;
      end
      else
      begin
        SEL:=39 ;
        ajunsCutie:=true;
      end;

     end;

     39 :
    begin
        k:=k+1;
        w:=w+1 ;
        f1:=f1+1;
        f2:=f2+1;
        f3:=f3+1;
        f4:=f4+1;

        if k = 300 then
        begin
           SEL:=40;
        end;

    if ajunsCutie then  //banda rulata 1
    begin
      if (toth>=0) and (toth<=60) then
      begin
          q1:=q1+1 ;
      end;
      if directie then
      begin
          toth:=toth+1;
      end;
      if toth=60 then
      begin
            toth:=0 ;
            q1:=0;
      end ;
      if (toth2>=0) and (toth2<=60) then    //banda rulata 2
      begin
          q2:=q2+1;
      end;
      if directie2 then
      begin
          toth2:=toth2+1 ;
      end;
      if toth2=60 then
      begin
            toth2:=0 ;
            q2:=0 ;
      end;
    end;

    end;





    end;
    end;
    end;
   {$R *.dfm}
   end.
