object Form4: TForm4
  Left = 567
  Top = 164
  Width = 578
  Height = 479
  Caption = 'Form4'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBox1: TGroupBox
    Left = 0
    Top = 0
    Width = 569
    Height = 401
    Caption = 'GroupBox1'
    TabOrder = 0
    object Label1: TLabel
      Left = 64
      Top = 96
      Width = 1
      Height = 25
      Caption = 'Label1'
    end
    object Label2: TLabel
      Left = 24
      Top = 32
      Width = 28
      Height = 13
      Caption = 'Nume'
    end
    object Label3: TLabel
      Left = 24
      Top = 72
      Width = 42
      Height = 13
      Caption = 'Prenume'
    end
    object Label4: TLabel
      Left = 24
      Top = 120
      Width = 33
      Height = 13
      Caption = 'Adresa'
    end
    object Label5: TLabel
      Left = 24
      Top = 168
      Width = 49
      Height = 13
      Caption = 'Telefon fix'
    end
    object Label6: TLabel
      Left = 32
      Top = 216
      Width = 63
      Height = 13
      Caption = 'Telefon mobil'
    end
    object Label7: TLabel
      Left = 40
      Top = 264
      Width = 25
      Height = 13
      Caption = 'Email'
    end
    object Label8: TLabel
      Left = 40
      Top = 320
      Width = 22
      Height = 13
      Caption = 'Tara'
    end
    object TDBEdit
      Left = 120
      Top = 24
      Width = 121
      Height = 21
      DataField = 'NUME'
      DataSource = Form1.DataSource1
      TabOrder = 0
    end
    object TDBEdit
      Left = 120
      Top = 64
      Width = 121
      Height = 21
      DataField = 'PRENUME'
      DataSource = Form1.DataSource1
      TabOrder = 1
    end
    object DBEdit3: TDBEdit
      Left = 120
      Top = 112
      Width = 121
      Height = 21
      DataField = 'ADRESA'
      DataSource = Form1.DataSource1
      TabOrder = 2
    end
    object DBEdit4: TDBEdit
      Left = 120
      Top = 168
      Width = 121
      Height = 21
      DataField = 'TEL_FIX'
      DataSource = Form1.DataSource1
      TabOrder = 3
    end
    object DBEdit5: TDBEdit
      Left = 120
      Top = 208
      Width = 121
      Height = 21
      DataField = 'TEL_MOBIL'
      DataSource = Form1.DataSource1
      TabOrder = 4
    end
    object DBEdit6: TDBEdit
      Left = 120
      Top = 264
      Width = 121
      Height = 21
      DataField = 'EMAIL'
      DataSource = Form1.DataSource1
      TabOrder = 5
    end
    object DBEdit7: TDBEdit
      Left = 120
      Top = 320
      Width = 121
      Height = 21
      DataField = 'TARA'
      DataSource = Form1.DataSource1
      TabOrder = 6
    end
  end
  object Button1: TButton
    Left = 0
    Top = 408
    Width = 569
    Height = 33
    Caption = 'Editeaza inregistrarea'
    TabOrder = 1
    OnClick = Button1Click
  end
end
