unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, ComCtrls;

type

  TForm1 = class(TForm)
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Edit1: TEdit;
    Edit2: TEdit;
    Button1: TButton;
    Button2: TButton;
    CheckBox1: TCheckBox;
    Label4: TLabel;
    RadioGroup1: TRadioGroup;
    rb1: TRadioButton;
    rb2: TRadioButton;
    rb3: TRadioButton;
    Label5: TLabel;
    Button3: TButton;
    DateTimePicker1: TDateTimePicker;
    Button4: TButton;
    Button5: TButton;
    Label6: TLabel;
    Button6: TButton;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure CheckBox1Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure Button5Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Button6Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }

  end;

var
  Form1: TForm1;


implementation

uses Unit2;

{$R *.dfm}

procedure TForm1.Button1Click(Sender: TObject);
var s:integer;
begin
  s:=StrToInt(Edit1.Text)+StrToInt(Edit2.Text);
  Label3.Caption:=IntToStr(s);
end;

procedure TForm1.Button2Click(Sender: TObject);
begin
Close;
end;

procedure TForm1.CheckBox1Click(Sender: TObject);
var t:real;
begin
If CheckBox1.Checked then
Label4.Caption:=FloatToStr(StrToFloat(Label3.Caption)*1.24)
end;

procedure TForm1.Button3Click(Sender: TObject);
begin
 if rb1.Checked then
label5.Caption:=IntToStr(StrToInt(Label3.Caption)+1)
else if rb2.Checked then
label5.Caption:=IntToStr(StrToInt(Label3.Caption)+2)
else if rb3.Checked then
label5.Caption:=IntToStr(StrToInt(Label3.Caption)+3)
end;

procedure TForm1.Button4Click(Sender: TObject);
var d: TSystemTime;
begin
  GetLocalTime(d);
  DateTimePicker1.DateTime:= SystemTimeToDateTime(d);
end;

procedure TForm1.Button5Click(Sender: TObject);
var p:integer;
begin
  p:=StrToInt(Edit1.Text)*StrToInt(Edit2.Text);
  Label6.Caption:=IntToStr(p);

end;

procedure TForm1.FormCreate(Sender: TObject);
begin
Edit1.Text:='';
Edit2.Text:='';
DateTimePicker1.Date:= StrToDate('01.03.2012');
end;

procedure TForm1.Button6Click(Sender: TObject);
begin
form2.show;
end;

end.
