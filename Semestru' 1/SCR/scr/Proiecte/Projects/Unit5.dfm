object Form5: TForm5
  Left = 192
  Top = 114
  Width = 722
  Height = 169
  Caption = 'Form5'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 112
    Top = 40
    Width = 328
    Height = 24
    Caption = 'Esti sigur ca vrei sa stergi inregistrarea?'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -20
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Button1: TButton
    Left = 128
    Top = 80
    Width = 75
    Height = 25
    Caption = 'DA'
    TabOrder = 0
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 296
    Top = 80
    Width = 75
    Height = 25
    Caption = 'NU'
    TabOrder = 1
    OnClick = Button2Click
  end
end
