unit Unit5;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls;

type
  TForm5 = class(TForm)
    Label1: TLabel;
    Button1: TButton;
    Button2: TButton;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form5: TForm5;

implementation

uses Unit45;

{$R *.dfm}

procedure TForm5.Button1Click(Sender: TObject);
begin
Form1.Table1.Delete;
Form1.Table1.Refresh;
Close;
end;

procedure TForm5.Button2Click(Sender: TObject);
begin
Close;
end;

end.
