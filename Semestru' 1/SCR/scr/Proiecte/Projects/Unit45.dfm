object Form1: TForm1
  Left = 114
  Top = 133
  Width = 1061
  Height = 625
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Menu = MainMenu1
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 88
    Top = 112
    Width = 90
    Height = 13
    Caption = 'Cauta(dupa nume):'
  end
  object Label2: TLabel
    Left = 88
    Top = 152
    Width = 108
    Height = 13
    Caption = 'Cauta (dupa prenume):'
  end
  object DBGrid1: TDBGrid
    Left = 48
    Top = 184
    Width = 577
    Height = 345
    DataSource = DataSource1
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
  end
  object DBNavigator1: TDBNavigator
    Left = 40
    Top = 536
    Width = 400
    Height = 25
    DataSource = DataSource1
    TabOrder = 1
  end
  object Button1: TButton
    Left = 88
    Top = 32
    Width = 89
    Height = 49
    Caption = 'Adauga'
    TabOrder = 2
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 216
    Top = 32
    Width = 161
    Height = 49
    Caption = 'Editeaza inregistrarea curenta'
    TabOrder = 3
    OnClick = Button2Click
  end
  object Button3: TButton
    Left = 408
    Top = 32
    Width = 185
    Height = 49
    Caption = 'Sterge inregistrarea curenta'
    TabOrder = 4
    OnClick = Button3Click
  end
  object Button4: TButton
    Left = 648
    Top = 32
    Width = 97
    Height = 57
    Caption = 'Iesire'
    TabOrder = 5
    OnClick = Button4Click
  end
  object Edit2: TEdit
    Left = 200
    Top = 112
    Width = 121
    Height = 21
    TabOrder = 6
    OnChange = Edit1Change
  end
  object Edit3: TEdit
    Left = 200
    Top = 144
    Width = 121
    Height = 21
    TabOrder = 7
    OnChange = Edit2Change
  end
  object GroupBox1: TGroupBox
    Left = 640
    Top = 176
    Width = 385
    Height = 345
    Caption = 'GroupBox1'
    TabOrder = 8
    object Label3: TLabel
      Left = 16
      Top = 32
      Width = 28
      Height = 13
      Caption = 'Nume'
    end
    object Label4: TLabel
      Left = 16
      Top = 72
      Width = 42
      Height = 13
      Caption = 'Prenume'
    end
    object Label5: TLabel
      Left = 16
      Top = 112
      Width = 33
      Height = 13
      Caption = 'Adresa'
    end
    object Label6: TLabel
      Left = 16
      Top = 152
      Width = 49
      Height = 13
      Caption = 'Telefon fix'
    end
    object Label7: TLabel
      Left = 16
      Top = 200
      Width = 63
      Height = 13
      Caption = 'Telefon mobil'
    end
    object Label8: TLabel
      Left = 16
      Top = 240
      Width = 25
      Height = 13
      Caption = 'Email'
    end
    object Label9: TLabel
      Left = 16
      Top = 288
      Width = 22
      Height = 13
      Caption = 'Tara'
    end
    object DBText1: TDBText
      Left = 104
      Top = 32
      Width = 65
      Height = 17
      DataField = 'NUME'
      DataSource = DataSource1
    end
    object DBText2: TDBText
      Left = 104
      Top = 72
      Width = 65
      Height = 17
      DataField = 'PRENUME'
      DataSource = DataSource1
    end
    object DBText3: TDBText
      Left = 104
      Top = 112
      Width = 65
      Height = 17
      DataField = 'ADRESA'
      DataSource = DataSource1
    end
    object DBText4: TDBText
      Left = 104
      Top = 152
      Width = 65
      Height = 17
      DataField = 'TEL_FIX'
      DataSource = DataSource1
    end
    object DBText5: TDBText
      Left = 104
      Top = 200
      Width = 65
      Height = 17
      DataField = 'TEL_MOBIL'
      DataSource = DataSource1
    end
    object DBText6: TDBText
      Left = 104
      Top = 240
      Width = 65
      Height = 17
      DataField = 'EMAIL'
      DataSource = DataSource1
    end
    object DBText7: TDBText
      Left = 104
      Top = 288
      Width = 65
      Height = 17
      DataField = 'PRENUME'
      DataSource = DataSource1
    end
  end
  object Table1: TTable
    Active = True
    DatabaseName = 'd:\student\4483\'
    TableName = 'CORO&CRY.DBF'
    Left = 16
    Top = 224
  end
  object DataSource1: TDataSource
    DataSet = Table1
    Left = 16
    Top = 192
  end
  object MainMenu1: TMainMenu
    Left = 704
    Top = 128
    object Fileff1: TMenuItem
      Caption = 'File'
      object New2: TMenuItem
        Caption = 'New'
        OnClick = New2Click
      end
      object Delete1: TMenuItem
        Caption = 'Delete'
      end
      object Exit1: TMenuItem
        Caption = 'Exit'
      end
    end
    object New1: TMenuItem
      Caption = 'Edit'
    end
    object Edit1: TMenuItem
      Caption = 'Help'
    end
  end
end
