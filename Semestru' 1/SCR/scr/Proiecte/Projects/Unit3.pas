unit Unit3;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls;

type
  TForm2 = class(TForm)
    GroupBox1: TGroupBox;
    Button1: TButton;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Edit1: TEdit;
    Edit2: TEdit;
    Edit3: TEdit;
    Edit4: TEdit;
    Edit5: TEdit;
    Edit6: TEdit;
    Edit7: TEdit;
 
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form2: TForm2;

implementation

uses Unit45;

{$R *.dfm}

procedure TForm2.Button1Click(Sender: TObject);
begin
Form1.Table1.Edit;
Form1.Table1.FieldByName('NUME').AsString:=Edit1.Text;
Form1.Table1.FieldByName('PRENUME').AsString:=Edit2.Text;
Form1.Table1.FieldByName('TEL_FIX').AsString:=Edit4.Text;
Form1.Table1.FieldByName('TEL_MOBIL').AsString:=Edit5.Text;
Form1.Table1.FieldByName('ADRESA').AsString:=Edit3.Text;
Form1.Table1.FieldByName('TARA').AsString:=Edit7.Text;
Form1.Table1.FieldByName('EMAIL').AsString:=Edit6.Text;
Form1.Table1.Post;
Form2.Close;
end;

end.
