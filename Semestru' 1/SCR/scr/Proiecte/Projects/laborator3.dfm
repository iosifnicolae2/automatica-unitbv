object Form1: TForm1
  Left = 140
  Top = 192
  Width = 961
  Height = 478
  Caption = 'Form1'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label5: TLabel
    Left = 296
    Top = 104
    Width = 75
    Height = 13
    Caption = 'Cautare student'
  end
  object DBGrid1: TDBGrid
    Left = 160
    Top = 192
    Width = 489
    Height = 120
    DataSource = DataSource1
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
  end
  object DBNavigator1: TDBNavigator
    Left = 160
    Top = 320
    Width = 240
    Height = 25
    DataSource = DataSource1
    TabOrder = 1
  end
  object GroupBox1: TGroupBox
    Left = 656
    Top = 192
    Width = 233
    Height = 217
    Caption = 'GroupBox1'
    TabOrder = 2
    object Label1: TLabel
      Left = 16
      Top = 32
      Width = 28
      Height = 13
      Caption = 'Nume'
    end
    object Label2: TLabel
      Left = 16
      Top = 57
      Width = 29
      Height = 13
      Caption = 'Nota1'
    end
    object Label3: TLabel
      Left = 16
      Top = 80
      Width = 29
      Height = 13
      Caption = 'Nota2'
    end
    object Label4: TLabel
      Left = 16
      Top = 104
      Width = 29
      Height = 13
      Caption = 'Media'
    end
    object DBText1: TDBText
      Left = 64
      Top = 32
      Width = 65
      Height = 17
      DataField = 'NUME'
      DataSource = DataSource1
    end
    object DBText2: TDBText
      Left = 64
      Top = 56
      Width = 65
      Height = 17
      DataField = 'NOTA1'
      DataSource = DataSource1
    end
    object DBText3: TDBText
      Left = 64
      Top = 80
      Width = 65
      Height = 17
      DataField = 'NOTA2'
      DataSource = DataSource1
    end
    object DBText4: TDBText
      Left = 64
      Top = 104
      Width = 65
      Height = 17
      DataField = 'MEDIA'
      DataSource = DataSource1
    end
  end
  object Button1: TButton
    Left = 192
    Top = 376
    Width = 97
    Height = 25
    Caption = 'Adugare student'
    TabOrder = 3
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 400
    Top = 408
    Width = 75
    Height = 25
    Caption = 'Exit'
    TabOrder = 4
    OnClick = Button2Click
  end
  object Button3: TButton
    Left = 328
    Top = 376
    Width = 169
    Height = 25
    Caption = 'Sterge inregistrarea curenta'
    TabOrder = 5
    OnClick = Button3Click
  end
  object Edit1: TEdit
    Left = 384
    Top = 104
    Width = 121
    Height = 21
    TabOrder = 6
    Text = 'Edit1'
    OnChange = Edit1Change
  end
  object Table1: TTable
    Active = True
    DatabaseName = 'd:\Student\4481'
    TableName = 'Student.dbf'
    Left = 160
    Top = 88
  end
  object DataSource1: TDataSource
    DataSet = Table1
    Left = 200
    Top = 88
  end
end
