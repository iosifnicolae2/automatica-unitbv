unit Lab1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls;

type
  TForm1 = class(TForm)
    Image1: TImage;
    Timer1: TTimer;
    Procedure timer1timer (sender:tobject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;
  mv,yg,t:integer;
  faza:integer=0;
  a:integer=0;
  b:integer=0;
  c:integer=0;

  x0:integer=100;
  y0:integer=400;
  x1:integer=200;
  y1:integer=200;
  x2:integer=300;
  y2:integer=300;
  x3:integer=100;
  y3:integer=500;


  directie:boolean=true;
  const
  tmax=300;
  r1=20;
  r2=40;
  r3=30;
implementation

Procedure tform1.timer1timer (sender:tobject);

begin
with form1.image1.canvas do
begin


yg:=150;
mv:=50;


//fundal
brush.Color:=rgb(200,200,200);
polygon([point(0,0),point(300,0),point(300,300),point(0,600)]);
brush.Color:=rgb(230,230,230);
polygon([point(300,0),point(1000,0),point(1000,300),point(300,300)]);
brush.Color:=rgb(150,150,150);
polygon([point(300,300),point(1300,300),point(1300,600),point(0,600)]);

//intrare
brush.Color:=rgb(80,80,80);
polygon([point(100,200+yg),point(200,100+yg),point(200,200+yg),point(100,300+yg)]);

//iesire
brush.Color:=rgb(80,80,80);
polygon([point(100,310+yg),point(200,210+yg),point(200,220+yg),point(100,320+yg)]);


//banda2
brush.Color:=rgb(90,175,100);
polygon([point(550-t,220+yg),point(600-t,220+yg),point(500-t,320+yg),point(450-t,320+yg)]);
polygon([point(500-t,220+yg),point(550-t,220+yg),point(450-t,320+yg),point(400-t,320+yg)]);

//banda
brush.Color:=rgb(180,250,200);
polygon([point(200+a,200+yg),point(250+a,200+yg),point(150+a,300+yg),point(100+a,300+yg)]);
polygon([point(250+a,200+yg),point(300+a,200+yg),point(200+a,300+yg),point(150+a,300+yg)]);
polygon([point(300+a,200+yg),point(350+a,200+yg),point(250+a,300+yg),point(200+a,300+yg)]);
polygon([point(350+a,200+yg),point(400+a,200+yg),point(300+a,300+yg),point(250+a,300+yg)]);
polygon([point(400+a,200+yg),point(450+a,200+yg),point(350+a,300+yg),point(300+a,300+yg)]);
polygon([point(450+a,200+yg),point(500+a,200+yg),point(400+a,300+yg),point(350+a,300+yg)]);
polygon([point(500+a,200+yg),point(550+a,200+yg),point(450+a,300+yg),point(400+a,300+yg)]);
polygon([point(550+a,200+yg),point(600+a,200+yg),point(500+a,300+yg),point(450+a,300+yg)]);

//roti
brush.Color:=rgb(80,50,20);
ellipse(125,450,150,475);


//EXAMPLE
//rectangle(200,100,300+a,140);
//rectangle(280+a,140,300+a,260+b);
//rectangle(280+a,260+b,340+a+c,280+b);







case faza of
0:
begin

a:=a+1;
if (a>500) then begin faza:=1; end;
end;

1:
begin
b:=b+1;
if (b>=140) then faza:=2;
end;

2:
begin
c:=c+1;
if (c=155) then faza:=3;
end;

3:
begin
c:=c-2;
if (c<=0) then faza:=4;
end;

4:
begin
b:=b-1;
if (b<=0) then faza:=5;
end;

5:
begin
a:=a-1;
if (a<=0) then faza:=0;
end;

6:
begin
a:=a;
if (a>=0) then faza:=0;
end;

end;



//if directie then t:=t+1

//else t:=0;

//if (t>10) and (t<200) then a:=a+1;

//if (t=tmax) or (t=0) then directie:= not directie;

{
//EXAMPLE
x1:=round(x0+r1*cos(t/10));
y1:=trunc(y0+r1*sin(t/10));

moveto(x0,y0);
lineto(x1,y1);

x2:=round(x1+r2*cos(t/100));
y2:=round(y1+r2*sin(t/100));

moveto(x1,y1);
lineto(x2,y2);

x3:=round(x2+r3*cos(t/100));
y3:=round(y2+r3*sin(t/100));

moveto(x2,y2);
lineto(x3,y3);
}
end;
end;

{$R *.dfm}

end.
