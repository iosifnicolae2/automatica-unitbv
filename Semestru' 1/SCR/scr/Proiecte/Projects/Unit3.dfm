object Form2: TForm2
  Left = 262
  Top = 190
  Width = 471
  Height = 471
  Caption = 'Form2'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBox1: TGroupBox
    Left = 0
    Top = 0
    Width = 457
    Height = 393
    Caption = 'Introdu datele'
    TabOrder = 0
    object Label1: TLabel
      Left = 32
      Top = 32
      Width = 28
      Height = 13
      Caption = 'Nume'
    end
    object Label2: TLabel
      Left = 32
      Top = 72
      Width = 42
      Height = 13
      Caption = 'Prenume'
    end
    object Label3: TLabel
      Left = 32
      Top = 120
      Width = 33
      Height = 13
      Caption = 'Adresa'
    end
    object Label4: TLabel
      Left = 32
      Top = 168
      Width = 49
      Height = 13
      Caption = 'Telefon fix'
    end
    object Label5: TLabel
      Left = 32
      Top = 224
      Width = 63
      Height = 13
      Caption = 'Telefon mobil'
    end
    object Label6: TLabel
      Left = 32
      Top = 280
      Width = 25
      Height = 13
      Caption = 'Email'
    end
    object Label7: TLabel
      Left = 32
      Top = 336
      Width = 22
      Height = 13
      Caption = 'Tara'
    end
    object Edit1: TEdit
      Left = 128
      Top = 32
      Width = 121
      Height = 21
      TabOrder = 0
    end
    object Edit2: TEdit
      Left = 128
      Top = 72
      Width = 121
      Height = 21
      TabOrder = 1
    end
    object Edit3: TEdit
      Left = 128
      Top = 112
      Width = 121
      Height = 21
      TabOrder = 2
    end
    object Edit4: TEdit
      Left = 128
      Top = 168
      Width = 121
      Height = 21
      TabOrder = 3
    end
    object Edit5: TEdit
      Left = 128
      Top = 224
      Width = 121
      Height = 21
      TabOrder = 4
    end
    object Edit6: TEdit
      Left = 128
      Top = 272
      Width = 121
      Height = 21
      TabOrder = 5
    end
    object Edit7: TEdit
      Left = 128
      Top = 328
      Width = 121
      Height = 21
      TabOrder = 6
    end
  end
  object Button1: TButton
    Left = 0
    Top = 400
    Width = 457
    Height = 33
    Caption = 'Adauga inregistrarea'
    TabOrder = 1
    OnClick = Button1Click
  end
end
