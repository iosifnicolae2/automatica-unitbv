unit Teodorescu.Dragos;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls;

type
  TForm1 = class(TForm)
    Timer1: TTimer;
    Image1: TImage;
    procedure Timer1Timer(sender:TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

procedure TForm1.Timer1Timer(sender:TObject);
begin
with Form1.Image1.canvas do
begin
brush.color:=clblue;
polygon ([point(110,210),point(290,210),point(290,390),point(110,390)]) ;
brush.color:=clgreen;
polygon ([point(110,210),point(290,210),point(340,160),point(160,160)]);
brush.color:=clyellow;
polygon ([point(290,210),point(340,160),point(340,340),point(290,390)]);
moveto(50,50);
lineto(500,50);
pen.width:=2;
pen.color:=clred;
ellipse (100,200,300,400);
polyline ([point(290,210),point(340,160),point(340,340),point(290,390)]);
end ;
end ;
{$R *.dfm}

end.
