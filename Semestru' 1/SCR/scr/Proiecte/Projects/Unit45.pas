unit Unit45;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, DBGrids, DB, DBTables, ExtCtrls, DBCtrls, Menus, StdCtrls;

type
  TForm1 = class(TForm)
    Table1: TTable;
    DataSource1: TDataSource;
    DBGrid1: TDBGrid;
    DBNavigator1: TDBNavigator;
    MainMenu1: TMainMenu;
    Fileff1: TMenuItem;
    New1: TMenuItem;
    Edit1: TMenuItem;
    New2: TMenuItem;
    Delete1: TMenuItem;
    Exit1: TMenuItem;
    Button1: TButton;
    Button2: TButton;
    Button3: TButton;
    Button4: TButton;
    Edit2: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    Edit3: TEdit;
    GroupBox1: TGroupBox;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    DBText1: TDBText;
    DBText2: TDBText;
    DBText3: TDBText;
    DBText4: TDBText;
    DBText5: TDBText;
    DBText6: TDBText;
    DBText7: TDBText;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure Edit1Change(Sender: TObject);
    procedure Edit2Change(Sender: TObject);
    procedure New2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

uses Unit3, Unit4, Unit5;

{$R *.dfm}

procedure TForm1.Button1Click(Sender: TObject);
begin
Table1.Insert;
Form2.Show;
end;

procedure TForm1.Button2Click(Sender: TObject);
begin

Form4.Show;
end;

procedure TForm1.Button3Click(Sender: TObject);
begin
Form5.Show;
end;

procedure TForm1.Button4Click(Sender: TObject);
begin
Close;
end;

procedure TForm1.Edit1Change(Sender: TObject);
var b:boolean;
begin

b:=(Table1.Locate('NUME', Edit2.Text,[LoPartialKey]));
if not b then
ShowMessage(Edit2.Text+' nu l-am putut gasi!!');

end;

procedure TForm1.Edit2Change(Sender: TObject);
var b:boolean;
begin

b:=(Table1.Locate('PRENUME', Edit3.Text,[LoPartialKey]));
if not b then
ShowMessage(Edit3.Text+' nu l-am putut gasi!!');

end;

procedure TForm1.New2Click(Sender: TObject);
begin
Table1.Insert;
Form2.Show;
end;

end.
