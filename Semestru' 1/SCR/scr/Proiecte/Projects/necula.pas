unit necula;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls;

type
  TForm1 = class(TForm)
    Timer1: TTimer;
    Image1: TImage;
    procedure timer1timer(sender:tobject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation
   procedure tform1.timer1timer(sender:tobject);
   begin
   with form1.Image1.Canvas do
   begin
   //rectangle(100,200,200,100);
   //rectangle(150,250,250,150);
   //polygon([point(100,200),point(150,250)]);
   //brush.Color:=clred;
   //polygon([point(100,100),point(150,150)]);
   //brush.Color:=clblue;
   //polygon([point(200,100),point(250,150)]);
   brush.Color:=clyellow;
   polygon([point(100,200),point(200,200),point(250,300),point(150,300)]);
   brush.Color:=clred;
   polygon([point(250,300),point(250,400),point(150,400),point(150,300)]);
   brush.Color:=clgreen;
   polygon([point(100,200),point(100,300),point(150,400),point(150,300)]);
   moveto(100,450);
   lineto(250,250);
   pen.Width:=10;
   pen.Color:=clblue;

   polyline([point(250,500),point(250,700),point(150,600),point(150,500)]);
    rectangle(300,200,500,500);
   brush.Color:=clred;
   ellipse(300,200,500,500);
    brush.Color:=clpurple;
   roundrect(200,300,400,500,50,60);
   chord(400,500,800,700,50,60,30,40);
   pie(400,500,800,700,50,60,2000,300);
  
   end;
  end;
{$R *.dfm}

end.
