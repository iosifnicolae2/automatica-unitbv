unit Unit1_L2;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls;

type
  TForm1 = class(TForm)
    Image1: TImage;
    Timer1: TTimer;
    procedure Timer1Timer(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;
  t:integer;
  directie:boolean=true;
  x0:integer=20  ;
  y0:integer=30  ;
  x1,y1:integer;
  const tmax=20;
        raza1=150  ;

implementation

{$R *.dfm}

procedure TForm1.Timer1Timer(Sender: TObject);
begin
 with form1.Image1.Canvas do
  begin
    fillrect(clientrect);
   {moveTo(170,170);
   lineTo(170,300);
   lineTo(300,300);
   lineTo(300,170);
   lineTo(170,170);

   moveTo(100,100);
   lineTo(100,230);
   lineTo(230,230);
   lineTo(230,100);
   lineTo(100,100);

   moveTo(100,100);
   lineTo(170,170);

   moveTo(170,300);
   lineTo(100,230);

   moveTo(300,300);
   lineTo(230,230);

   moveTo(300,170);
   lineTo(230,100); }

   brush.color:=clblack;
   rectangle(120,120,200,140);
   pen.color:=clblack;
   brush.color:=clyellow;
   pen.color:=clblack;
   brush.color:=clyellow;
   rectangle(200,80,220,180);
   polygon([point(220,90+t),point(260,100+t),point(260,110+t),point(220,110+t)]);
   polygon([point(220,150-t),point(260,150-t),point(260,160-t),point(220,170-t)]);
   rectangle(110,130,130,200);
   ellipse(110,120,130,140);
   brush.color:=clwhite;

   x1:=round(x0+raza1*cos(t/10));
   y1:=round(y0+raza1*sin(t/10));
     pen.width:=2;
     moveto(10,10);
     lineto(x0,y0);
     moveto(x0,y0);
     lineto(x1,y1);

   if directie then t:=t+1
            else t:=t-1;
   if (t=tmax) or (t=0) then directie:=not directie;

   end;
end;

end.
