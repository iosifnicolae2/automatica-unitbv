unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls;

type
  TForm1 = class(TForm)
    Image1: TImage;
    Timer1: TTimer;
    procedure timer1timer(sender:tobject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;
  aux2,aux,retract,x,t,tDown,tDown2,tDown3,tLeft,tLeft2,mLeft,mLeft2 : Integer;
  directie : boolean = true;
  dirx : boolean = true;



 continuu,x0,x1,x2,x3,x4,x5,x6,x7 : Integer;
          ddown, upp, y0,y1,y2,y3,y4,y5,y6,y7 : Integer;
          k0,z0,k1,z1,k2,z2,k3,z3,k4,k5,k6,k7,z4,z5,z6,z7: Integer;
          tline : boolean = true;
          down : Integer = -200;
          faza : Integer = -1;
          xleft : Integer = -300;
          yleft : Integer = -300;
          retractx : Integer;
          retracty : Integer;
const

  r1 = 5;

implementation

procedure tform1.timer1timer(sender:tobject);
begin
with form1.Image1.Canvas do
  begin

if tline then continuu:=continuu+1;
  //BG
Brush.Color:=RGB(123,123,123);
  rectangle(0,100,800,0);

Brush.Color:=RGB(105,105,105);
  rectangle(0,99,800,322);

  //Banda
Brush.Color:=RGB(116,144,158);
  rectangle(0,174,800,253);

Brush.Color:=RGB(66,94,108);
  rectangle(0,252,800,263);

Brush.Color:=RGB(180,180,180);


  Polygon([Point(38,252),Point(48,252),Point(38,262)]);
  ellipse(33,252,43,262);

x0:= 38;
y0:= 257;

  k0:=round(x0+r1*cos(continuu/100));
  z0:=trunc(y0+r1*sin(continuu/100));
      moveto(x0,y0);
      lineto(k0,z0);

  Polygon([Point(38+100,252),Point(48+100,252),Point(38+100,262)]);
  ellipse(33+100,252,43+100,262);

x1 := 138;
y1 := 257;

  k1:=round(x1+r1*cos(continuu/100));
  z1:=trunc(y1+r1*sin(continuu/100));
      moveto(x1,y1);
      lineto(k1,z1);

  Polygon([Point(38+200,252),Point(48+200,252),Point(38+200,262)]);
  ellipse(33+200,252,43+200,262);

x2 := 238;
y2 := 257;

  k2:=round(x2+r1*cos(continuu/100));
  z2:=trunc(y2+r1*sin(continuu/100));
      moveto(x2,y2);
      lineto(k2,z2);


  Polygon([Point(38+300,252),Point(48+300,252),Point(38+300,262)]);
  ellipse(33+300,252,43+300,262);

x3 := 338;
y3 := 257;

  k3:=round(x3+r1*cos(continuu/100));
  z3:=trunc(y3+r1*sin(continuu/100));
      moveto(x3,y3);
      lineto(k3,z3);

  Polygon([Point(38+400,252),Point(48+400,252),Point(38+400,262)]);
  ellipse(33+400,252,43+400,262);

x4 := 438;
y4 := 257;

  k4:=round(x4+r1*cos(continuu/100));
  z4:=trunc(y4+r1*sin(continuu/100));
      moveto(x4,y4);
      lineto(k4,z4);


  Polygon([Point(38+500,252),Point(48+500,252),Point(38+500,262)]);
  ellipse(33+500,252,43+500,262);

x5 := 538;
y5 := 257;

  k5:=round(x5+r1*cos(continuu/100));
  z5:=trunc(y5+r1*sin(continuu/100));
      moveto(x5,y5);
      lineto(k5,z5);

  Polygon([Point(38+600,252),Point(48+600,252),Point(38+600,262)]);
  ellipse(33+600,252,43+600,262);

x6 := 638;
y6 := 257;

  k6:=round(x6+r1*cos(continuu/100));
  z6:=trunc(y6+r1*sin(continuu/100));
      moveto(x6,y6);
      lineto(k6,z6);

  Polygon([Point(38+700,252),Point(48+700,252),Point(38+700,262)]);
  ellipse(33+700,252,43+700,262);

x7 := 738;
y7 := 257;

  k7:=round(x7+r1*cos(continuu/100));
  z7:=trunc(y7+r1*sin(continuu/100));
      moveto(x7,y7);
      lineto(k7,z7);

  //Birou------------------------------------


//2spate

Brush.Color:=RGB(172,139,0);
  Polygon([Point(447+continuu-600,180+down),Point(527+continuu-600,180+down),Point(527+continuu-600,60+down),Point(225+continuu-600,60+down),Point(225+continuu-600,117+down),Point(447+continuu-600,117+down)]);

//brate1   -----

Brush.Color:=RGB(222,179,0);
  Polygon([Point(386+continuu-600+xleft,245-yleft),Point(386+continuu-600+xleft,125-yleft),Point(447+continuu-600+xleft,64-yleft),Point(447+continuu-600+xleft,184-yleft)]);

//hand1

 Brush.Color:=RGB(150,150,150);
  Polygon([Point(336+continuu-600+xleft-retractx,245-yleft+retracty),Point(336+continuu-600+xleft-retractx,225-yleft+retracty),Point(397+continuu-600+xleft-retractx,164-yleft+retracty),Point(397+continuu-600+xleft-retractx,184-yleft+retracty)]);

//1baza

Brush.Color:=RGB(222,179,0);
  Polygon([Point(386+continuu-600,241),Point(466+continuu-600,241),Point(527+continuu-600,180),Point(447+continuu-600,180)]);
Brush.Color:=RGB(192,149,0);
  Polygon([Point(386+continuu-600,241),Point(466+continuu-600,241),Point(466+continuu-600,245),Point(386+continuu-600,245)]);
Brush.Color:=RGB(216,175,150);
  Polygon([Point(466+continuu-600,245),Point(466+continuu-600,241),Point(527+continuu-600,180),Point(527+continuu-600,184)]);

//brate2   -----

Brush.Color:=RGB(222,179,0);
  Polygon([Point(466+continuu-600+xleft,245-yleft),Point(466+continuu-600+xleft,125-yleft),Point(527+continuu-600+xleft,64-yleft),Point(527+continuu-600+xleft,184-yleft)]);
Brush.Color:=RGB(222,179,0);
  Polygon([Point(164+continuu-600+xleft,245-yleft),Point(164+continuu-600+xleft,125-yleft),Point(225+continuu-600+xleft,64-yleft),Point(225+continuu-600+xleft,184-yleft)]);

//hand2

Brush.Color:=RGB(150,150,150);
  Polygon([Point(116+continuu-600+xleft-retractx,245-yleft+retracty),Point(116+continuu-600+xleft-retractx,225-yleft+retracty),Point(177+continuu-600+xleft-retractx,164-yleft+retracty),Point(177+continuu-600+xleft-retractx,184-yleft+retracty)]);
Brush.Color:=RGB(150,150,150);
  Polygon([Point(416+continuu-600+xleft-retractx,245-yleft+retracty),Point(416+continuu-600+xleft-retractx,225-yleft+retracty),Point(477+continuu-600+xleft-retractx,164-yleft+retracty),Point(477+continuu-600+xleft-retractx,184-yleft+retracty)]);

Brush.Color:=RGB(140,140,140);
  Polygon([Point(116+continuu-600+xleft-retractx,245-yleft+retracty),Point(116+continuu-600+xleft-retractx,225-yleft+retracty),Point(416+continuu-600+xleft-retractx,225-yleft+retracty),Point(416+continuu-600+xleft-retractx,245-yleft+retracty)]);
Brush.Color:=RGB(160,160,160);
  Polygon([Point(126+continuu-600+xleft-retractx,215-yleft+retracty),Point(116+continuu-600+xleft-retractx,225-yleft+retracty),Point(416+continuu-600+xleft-retractx,225-yleft+retracty),Point(426+continuu-600+xleft-retractx,215-yleft+retracty)]);

Brush.Color:=RGB(140,140,140);
  Polygon([Point(216+continuu-600+xleft-retractx,345-yleft+retracty),Point(216+continuu-600+xleft-retractx,225-yleft+retracty),Point(316+continuu-600+xleft-retractx,225-yleft+retracty),Point(316+continuu-600+xleft-retractx,345-yleft+retracty)]);

//top

Brush.Color:=RGB(222,179,0);
  Polygon([Point(124+continuu-600,-132+ddown),Point(484+continuu-600,-132+ddown),Point(560+continuu-600,-208+ddown),Point(200+continuu-600,-208+ddown)]);

//hand3

Brush.Color:=RGB(150,150,150);
  Polygon([Point(144+continuu-600,-132+ddown-retracty),Point(464+continuu-600,-132+ddown-retracty),Point(540+continuu-600,-208+ddown-retracty),Point(220+continuu-600,-208+ddown-retracty)]);
Brush.Color:=RGB(140,140,140);
  Polygon([Point(204+continuu-600,-142+ddown-retracty),Point(404+continuu-600,-142+ddown-retracty),Point(460+continuu-600,-198+ddown-retracty),Point(260+continuu-600,-198+ddown-retracty)]);
Brush.Color:=RGB(130,130,130);
  Polygon([Point(254+continuu-600,-170+ddown-retracty),Point(404+continuu-600,-170+ddown-retracty),Point(404+continuu-600,-398+ddown-retracty),Point(254+continuu-600,-398+ddown-retracty)]);



//hands1

Brush.Color:=RGB(150,150,150);
  Polygon([Point(525+continuu-600,35+down-upp),Point(223+continuu-600,35+down-upp),Point(223+continuu-600,47+down-upp),Point(525+continuu-600,47+down-upp)]);
Brush.Color:=RGB(170,170,170);
  Polygon([Point(525+continuu-600,35+down-upp),Point(223+continuu-600,35+down-upp),Point(229+continuu-600,29+down-upp),Point(531+continuu-600,29+down-upp)]);
Brush.Color:=RGB(140,140,140);
  Polygon([Point(525+continuu-600,47+down-upp),Point(525+continuu-600,35+down-upp),Point(531+continuu-600,29+down-upp),Point(531+continuu-600,41+down-upp)]);

Brush.Color:=RGB(145,145,145);
  Polygon([Point(300+continuu-600,35+down-upp),Point(310+continuu-600,35+down-upp),Point(310+continuu-600,77+down-upp),Point(300+continuu-600,77+down-upp)]);
  Polygon([Point(450+continuu-600,35+down-upp),Point(460+continuu-600,35+down-upp),Point(460+continuu-600,77+down-upp),Point(450+continuu-600,77+down-upp)]);

  Polygon([Point(250+continuu-600,32+down-upp),Point(260+continuu-600,32+down-upp),Point(260+continuu-600,-100+down-upp),Point(250+continuu-600,-100+down-upp)]);
  Polygon([Point(500+continuu-600,32+down-upp),Point(510+continuu-600,32+down-upp),Point(510+continuu-600,-100+down-upp),Point(500+continuu-600,-100+down-upp)]);




if continuu=450 then begin tline:=false; if tline=false then down:=down+1; end;
if down=4 then tline:=true; end;
if continuu=600 then begin tline:=false; if tline=false then xleft:=xleft+1; yleft:=yleft+1; end;
if xleft=0 then begin tline:=true; upp:=upp+1; end;
if continuu=750 then begin tline:=false; if tline=false then ddown:=ddown+1; end;
if ddown=265 then begin tline:=true; retractx:=retractx+2; retracty:=retracty+1 end;







if continuu=1500 then begin continuu:=0;
                            down:= -200;
                            xleft:= -300;
                            yleft:= -300;
                            ddown:=0;
                            upp:=0;
                            retractx:=0;
                            retracty:=0; end;

{$R *.dfm}
     end;
end.
