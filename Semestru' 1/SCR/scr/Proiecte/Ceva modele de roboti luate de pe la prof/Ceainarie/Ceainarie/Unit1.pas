unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, ExtCtrls, StdCtrls, Gauges;

type
  TForm1 = class(TForm)
    Timer1: TTimer;
    Image1: TImage;
    MainMenu1: TMainMenu;
    Control1: TMenuItem;
    Start1: TMenuItem;
    Pauz1: TMenuItem;
    Exit1: TMenuItem;
    Verde1: TMenuItem;
    Negru1: TMenuItem;
    N0cuburi1: TMenuItem;
    N11: TMenuItem;
    N2bucti1: TMenuItem;
    N3bucti1: TMenuItem;
    N4bucti1: TMenuItem;
    procedure Timer1Timer(Sender: TObject);
    procedure Exit1Click(Sender: TObject);
    procedure Negru1Click(Sender: TObject);
    procedure Verde1Click(Sender: TObject);
    procedure N0cuburi1Click(Sender: TObject);
    procedure N11Click(Sender: TObject);
    procedure N2bucti1Click(Sender: TObject);
    procedure N3bucti1Click(Sender: TObject);
    procedure N4bucti1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;
  xb:integer=200;yb:integer=250;
  xb1:integer=380+20;yb1:integer=225;
  xb2:integer=220;yb2:integer=195;
  xb3:integer=120;yb3:integer=185;
  xa,ya,xc,yc,xd,yd,xe,ye,xf,yf,xg,yg:integer;
  xa1,ya1,xc1,yc1,xd1,yd1,xe1,ye1,xf1,yf1,xg1,yg1:integer;
  xa2,ya2,xc2,yc2,xd2,yd2,xe2,ye2,xf2,yf2,xg2,yg2:integer;
  xa3,ya3,xc3,yc3,xd3,yd3,xe3,ye3,xf3,yf3,xg3,yg3:integer;
  profunz:integer=40;sujos:integer=25;
  h1:integer;//max 50      //oala7
  h2:integer=0;
  h22:real;
  prinde:integer=0;//max9    //oala1
  culis,culiss:integer; //max70  //oala55
  culis1:real;
  ho:integer; // max45 - - - 10
  hoo:real=45;
  xo,yo,xt,ht,pr:integer;
  xas,yas,xad,yad,xbs,ybs,xbd,ybd,c2,csc:integer;
  negru:integer=100;
  x:integer=0;
  y:integer=0;
  xce1,xce2,xce3,yce1,yce2,yce3:integer;
  an1,an2,an3,timp:integer;
  container:integer;
  zahar:integer=0;
  apa:boolean=false;
  oala:boolean=false;
  af:boolean=false;
  ceai:boolean=false;
  zah:boolean=false;
  ceaigata:boolean=false;
  laloc:boolean=false;
  tur1:boolean=false;
  tur2:boolean=false;
  ciclu:boolean=false;
  gol:boolean=false;

implementation

{$R *.dfm}

procedure TForm1.Timer1Timer(Sender: TObject);

begin

  with form1.image1.canvas do
  begin
    brush.Color:=rgb(255,255,255);
    fillrect(clientrect);
  h22:=h2/3;
  h1:=round(h22);
  culis1:=culiss/3;
  culis:=round(culis1);
       an1:=rgb(240,240,240);
       an2:=rgb(240,240,240);
       an3:=rgb(240,240,240);
  ho:=round(hoo);
  //----------------------------------------------------------------------------
 //--miscari--

     if (container<>0)and(apa=false)and(gol=false) then
       begin
         if (h2<90) then h2:=h2+1;
         if (h2=90)and(culiss<18) then culiss:=culiss+1;
         if (culiss=18)and(prinde<2)and(h2=90) then prinde:=prinde+1;
         if (prinde=2)and(culiss=18)and(h2=90) then
           begin
             if (hoo>10) then
               begin
                 hoo:=hoo-1/2;       //-++++++++++++++++++++++++++++++++++
                 xas:=12;yas:=42;xad:=-16;yad:=43;
               end
             else apa:=true;
           end;
       end;

     if (apa)and(oala=false)and(gol=false) then
       begin
         if (prinde>0)and(h2=90)and(culiss=18) then prinde:=prinde-1;
         xas:=0;yas:=0;xad:=0;yad:=0;
         if (prinde=0)and(h2=90)and(culiss>0) then culiss:=culiss-1;
         if (prinde=0)and(h2>12)and(culiss=0) then h2:=h2-1;
         if (culiss<6)and(prinde=0)and(h2=12) then culiss:=culiss+1;
         if (culiss=6)and(prinde<4)and(h2=12) then prinde:=prinde+1;
         if (culiss=6)and(prinde=4)and(h2=12) then oala:=true;
       end;

     if (apa)and(oala)and(af=false)and(ceaigata=false)and(gol=false) then
       begin
         if (negru>0) then negru:=negru-10;
         if (h2<30) and (culiss=6) then begin h2:=h2+1;yo:=yo+1; end;
         if (h2=30)and(culiss<60) then
           begin
             xo:=xo+1;
             culiss:=culiss+1;
           end;
         if (h2>12)and(culiss=60) then begin h2:=h2-1;yo:=yo-1; end;
         if (h2=12)and(culiss=60)and(prinde>0) then prinde:=prinde-1;
         if (h2=12)and(culiss>30)and(prinde=0)then culiss:=culiss-1;
         if (h2=12)and(culiss=30)and(prinde=0)then af:=true;
       end;

//-------container1
     if (container=1)and(apa)and(oala)and(af)and(ceai=false) then
       begin
         if (xt<1) then xt:=xt+1;
         if (xt=1)and(ht<60)and(pr=0) then ht:=ht+1;
         if (xt=1)and(ht=60)and(pr<8) then pr:=pr+1;
         if (xt=1)and(ht>10)and(pr=8) then begin ht:=ht-1; yce1:=yce1-1; end;
         if (xt<250)and(ht=10)and(pr=8)and(yce1<-5) then
           begin
             xt:=xt+1;
             xce1:=xce1+1;
           end;
         if (xt=250)and(ht<30)and(pr=8) then
           begin
             ht:=ht+1;
             yce1:=yce1+1;
           end;
         if (xt=250)and(ht=30)and(pr>0) then
           begin
             pr:=pr-1;
             xce1:=0;
             yce1:=0;
           end;
         if (xt=250)and(ht=30)and(pr=0) then ceai:=true;
       end;
//-------container2
     if (container=2)and(apa)and(oala)and(af)and(ceai=false) then
       begin
         if (xt<33) then xt:=xt+1;
         if (xt=33)and(ht<60)and(pr=0) then ht:=ht+1;
         if (xt=33)and(ht=60)and(pr<8) then pr:=pr+1;
         if (xt=33)and(ht>10)and(pr=8) then begin ht:=ht-1; yce2:=yce2-1; end;
         if (xt<250)and(ht=10)and(pr=8)and(yce2<-5) then
           begin
             xt:=xt+1;
             xce2:=xce2+1;
           end;
         if (xt=250)and(ht<30)and(pr=8) then
           begin
             ht:=ht+1;
             yce2:=yce2+1;
           end;
         if (xt=250)and(ht=30)and(pr>0) then
           begin
             pr:=pr-1;
             xce2:=0;
             yce2:=0;
           end;
         if (xt=250)and(ht=30)and(pr=0) then ceai:=true;
       end;
//----zahar---
     if (apa)and(oala)and(af)and(ceai)and(zah)and(zahar>0)and(ceaigata=false) then
       begin
         if (xt=250)and(ht>0)and(pr=0) then ht:=ht-1;
         if (xt>65)and(ht=0)and(pr=0) then xt:=xt-1;
         container:=3;
       end;

     if (container=3)and(apa)and(oala)and(af)and(ceai)and(zah)and(zahar>0)and(ceaigata=false) then
       begin

         if (xt<65) then xt:=xt+1;
         if (xt=65)and(ht<60)and(pr=0) then ht:=ht+1;
         if (xt=65)and(ht=60)and(pr<8) then pr:=pr+1;
         if (xt=65)and(ht>10)and(pr=8) then begin ht:=ht-1; yce3:=yce3-1; end;
         if (xt<250)and(ht=10)and(pr=8)and(yce3<-5) then
           begin
             xt:=xt+1;
             xce3:=xce3+1;
           end;
         if (xt=250)and(ht<30)and(pr=8) then
           begin
             ht:=ht+1;
             yce3:=yce3+1;
           end;
         if (xt=250)and(ht=30)and(pr>0) then
           begin
             pr:=pr-1;
             xce3:=0;
             yce3:=0;
           end;
         if (xt=250)and(ht=30)and(pr=0) then zahar:=zahar-1;
        end;

//----revenire---
     if (apa)and(oala)and(af)and(ceai)and(zahar=0) then
       begin
         if (xt=250)and(ht>0)and(pr=0) then ht:=ht-1;
         if (xt>0)and(ht=0)and(pr=0) then xt:=xt-1;
         if (xt=0)and(ht=0)and(pr=0) then ceaigata:=true;
       end;
//----ceaigata ia oala de pe foc--
  if (ceaigata)and(gol=false) then begin
     if (h2=12)and(culiss<60)and(prinde=0) then culiss:=culiss+1;
     if (h2=12)and(culiss=60)and(prinde<4) then prinde:=prinde+1;
     if (h2<40)and(culiss=60)and(prinde=4) then
       begin
         h2:=h2+1;
         yo:=yo+1;
       end;
     if (negru<100) then negru:=negru+1;
     if (h2=40)and(culiss<165)and(prinde=4) then
       begin
         culiss:=culiss+1;
         xo:=xo+1;
       end;
     if (h2>22)and(culiss=165)and(prinde=4) then
       begin
         h2:=h2-1;
         yo:=yo-1;
       end;
     if (h2=22)and(culiss=165)and(prinde>0) then
       begin
         prinde:=prinde-1;
         laloc:=true;
       end;
     if (h2=22)and(culiss>0)and(prinde=0) then culiss:=culiss-1;
//--toarna in cesti--
     if (csc<10)and(tur2=false) then csc:=csc+1;
     if (csc=10)and(laloc)and(timp<20)and(hoo<45) then
       begin
         xbs:=2;ybs:=22;xbd:=-2;ybd:=22;timp:=timp+1;tur2:=true;hoo:=hoo+1/10;
       end                                                   //+++++++++++++++++++++
     else
       begin
         xbs:=0;ybs:=0;xbd:=0;ybd:=0;
       end;
     if (csc<100)and(tur2)and(timp=20) then csc:=csc+1;
     if (csc=100)and(tur2)and(timp=20) then
       begin
         csc:=0; timp:=0; tur1:=true; tur2:=false;
       end;
     if (tur2) then an2:=rgb(160,160,75);
     if (tur1) then an3:=rgb(160,160,75);
     if (hoo>45) then
       begin
         xbs:=0;ybs:=0;xbd:=0;ybd:=0;gol:=true;
       end;
  end;
  if (ceaigata)and(gol) then
    begin
      if (tur2) then an2:=rgb(160,160,75);
      if (tur1) then an3:=rgb(160,160,75);
      if (h2=22)and(culiss<165)and(prinde=0) then culiss:=culiss+1;
      if (h2=22)and(culiss=165)and(prinde<4) then prinde:=prinde+1;
      if (h2<40)and(culiss=165)and(prinde=4) then
        begin
          h2:=h2+1;yo:=yo+1;
        end;
      if (h2=40)and(culiss>6)and(prinde=4) then
        begin
          culiss:=culiss-1;xo:=xo-1;
        end;
      if (h2>12)and(culiss=6)and(prinde=4) then
        begin
          h2:=h2-1;yo:=yo-1;
        end;
      if (h2=12)and(culiss=6)and(prinde>0) then prinde:=prinde-1;
      if (h2=12)and(culiss>0)and(prinde=0) then culiss:=culiss-1;
      if (h2>0)and(culiss=0)and(prinde=0) then h2:=h2-1;
      if (h2=0)and(culiss=0)and(prinde=0) then
        begin
          apa:=false;
          oala:=false;
          af:=false;
          ceai:=false;
          zah:=false;
          ceaigata:=false;
          laloc:=false;
          gol:=false;
          container:=0;
          ciclu:=true;
          zahar:=0;
        end;
    end;
    if (ciclu) then
      begin
       //   tur1:=true;
       //   tur2:=true;
     if (tur2) then an2:=rgb(160,160,75);
     if (tur1) then an3:=rgb(160,160,75);
      end;

  //----------------------------------------------------------------------------
 //--masa suport--fix--

  xa:=xb+profunz; ya:=yb-sujos;
  xc:=xb;         yc:=yd;
  xd:=xb+250;     yd:=yb+50;
  xg:=xd;         yg:=yb;
  xf:=xa+250;     yf:=ya;
  xe:=xf;         ye:=yf+50;

        pen.width:=1;
        pen.color:=rgb(180,180,180);

        brush.Color:=rgb(218,218,218);
    rectangle (xb,yb,xd,yd);
        brush.Color:=rgb(238,238,238);
    Polygon([Point(xb,yb), Point(xa,ya),
             Point(xf,yf), Point(xg,yg)]);
        brush.Color:=rgb(198,198,198);
    Polygon([Point(xg,yg), Point(xf,yf),
             Point(xe,ye), Point(xd,yd)]);
  //----------------------------------------------------------------------------
 //--banda rulanta--c2--

                                                                //   csc:=csc+1;
 c2:=5*csc;                                                    
pen.color:=clred;
pen.width:=3;
brush.Color:=rgb(100,100,100);
roundrect(-20,200,250,225,28,28);
pen.color:=rgb(100,100,100);
Polygon([Point(-20,223), Point(240,223),
         Point(202,255), Point(-20,255)]);
pen.color:=clred;
brush.Color:=rgb(140,140,140);
roundrect(-20,230,212,255,28,28);

pen.color:=clblack;
pen.width:=1;

ellipse(-7,230,20,255);                                                         //rotation  de la roue 1
x:=round(-7+13+13*sin(C2/90));
y:=round(243+13*cos(C2/90));
MoveTo(-7+13,243);LineTo(x,y);

x:=round(-7+13+13*-sin(C2/90));
y:=round(243+13*-cos(C2/90));
MoveTo(x,y);LineTo(6,243);
MoveTo(14,252);LineTo(40,231);

ellipse(57,230,84,255);                                                         //rotation  de la roue 2
x:=round(70+13*sin(C2/90));
y:=round(243+13*cos(C2/90));
MoveTo(70,243);LineTo(x,y);

x:=round(70+13*-sin(C2/90));
y:=round(243+13*-cos(C2/90));
MoveTo(x,y);LineTo(70,243);
MoveTo(78,252);LineTo(104,231);

ellipse(127,230,154,255);                                                       //rotation  de la roue 3
x:=round(140+13*sin(C2/90));
y:=round(243+13*cos(C2/90));
MoveTo(140,243);LineTo(x,y);

x:=round(140+13*-sin(C2/90));
y:=round(243+13*-cos(C2/90));
MoveTo(x,y);LineTo(140,243);
MoveTo(148,252);LineTo(174,231);

ellipse(185,230,212,255);                                                       //rotation  de la roue 4
x:=round(198+13*sin(C2/90));
y:=round(243+13*cos(C2/90));
MoveTo(198,243);LineTo(x,y);

x:=round(198+13*-sin(C2/90));
y:=round(243+13*-cos(C2/90));
MoveTo(x,y);LineTo(198,243);


pen.Width:=6;
moveto(5,243);lineto(5,300);                                                    // supports des tapis (tiges)
moveto(70,243);lineto(70,300);
moveto(140,243);lineto(140,300);
moveto(198,243);lineto(198,300);

pen.Width:=2;
brush.Color:=clgray;                                                            // support de b�ton
rectangle(0,300,560,350);

  //----------------------------------------------------------------------------
 //--masa robot--fix--
 
    pen.width:=1;
    pen.color:=rgb(180,180,180);
    brush.Style:=bsSolid;

  xa1:=xb1+profunz-15; ya1:=yb1-sujos+10;       //15 a fost 20
  xc1:=xb1;            yc1:=yd1;
  xd1:=xb1+45;         yd1:=yb1+20;
  xg1:=xd1;            yg1:=yb1;
  xf1:=xa1+45;         yf1:=ya1;
  xe1:=xf1;            ye1:=yf1+20;

        brush.Color:=rgb(218,218,218);
    rectangle (xb1,yb1,xd1,yd1);
        brush.Color:=rgb(238,238,238);
    Polygon([Point(xb1,yb1), Point(xa1,ya1),
             Point(xf1,yf1), Point(xg1,yg1)]);
        brush.Color:=rgb(198,198,198);
    Polygon([Point(xg1,yg1), Point(xf1,yf1),
             Point(xe1,ye1), Point(xd1,yd1)]);

  //----------------------------------------------------------------------------
 //--suport oala--fix--

        pen.width:=1;
        pen.color:=rgb(128,0,0);
  xa3:=xb3+profunz-10;  ya3:=yb3-sujos+10;
  xc3:=xb3;             yc3:=yd3;
  xd3:=xb3+60;          yd3:=yb3+10;
  xg3:=xd3;             yg3:=yb3;
  xf3:=xa3+60;          yf3:=ya3;
  xe3:=xf3;             ye3:=yf3+10;

        brush.Color:=rgb(135,1,4);
    rectangle (xb3,yb3,xd3,yd3);
        brush.Color:=rgb(145,1,4);
    Polygon([Point(xb3,yb3), Point(xa3,ya3),
             Point(xf3,yf3), Point(xg3,yg3)]);
        brush.Color:=rgb(120,0,3);
    Polygon([Point(xg3,yg3), Point(xf3,yf3),
             Point(xe3,ye3), Point(xd3,yd3)]);

  //----------------------------------------------------------------------------
 //--robinet--fix--

       pen.width:=1;
       pen.color:=rgb(0,200,0);
       brush.Color:=rgb(134,214,122);
    ellipse(338,65,368,95);
       brush.Color:=rgb(154,234,142);
    ellipse(340,70,360,90);
    Polygon([Point(352,73), Point(352,85),    //tuspic
             Point(330,94), Point(322,95)]);

  //----------------------------------------------------------------------------
 //--cesti--csc--an1--an2--an3-

                                                   //    an1:=rgb(160,160,75);
                                                 //      an2:=rgb(160,160,75);
                                                  //     an3:=rgb(160,160,75);
       pen.width:=1;
       pen.color:=rgb(205,205,205);
       brush.Color:=rgb(240,240,240);

    ellipse(55+170-csc,210,95+170-csc,220);
    ellipse(58+170-csc,203,65+170-csc,213);
    Polygon([Point(60+170-csc,200), Point(90+170-csc,200),
             Point(85+170-csc,215), Point(65+170-csc,215)]);
       brush.Color:=an1;
    ellipse(60+170-csc,198,90+170-csc,205);

       brush.Color:=rgb(240,240,240);
    ellipse(55+70-csc,210,95+70-csc,220);
    ellipse(58+70-csc,203,65+70-csc,213);
    Polygon([Point(60+70-csc,200), Point(90+70-csc,200),
             Point(85+70-csc,215), Point(65+70-csc,215)]);
       brush.Color:=an2;
    ellipse(60+70-csc,198,90+70-csc,205);

       brush.Color:=rgb(240,240,240);
    ellipse(55-30-csc,210,95-30-csc,220);
    ellipse(58-30-csc,203,65-30-csc,213);
    Polygon([Point(60-30-csc,200), Point(90-30-csc,200),
             Point(85-30-csc,215), Point(65-30-csc,215)]);
       brush.Color:=an3;
    ellipse(60-30-csc,198,90-30-csc,205);

  //----------------------------------------------------------------------------
 //--masa flacara--fix--

        pen.width:=1;
        pen.color:=rgb(180,180,180);

  xa2:=xb2+profunz-10; ya2:=yb2-sujos+10;
  xc2:=xb2;            yc2:=yd2;
  xd2:=xb2+145;        yd2:=yb2+50;
  xg2:=xd2;            yg2:=yb2;
  xf2:=xa2+145;        yf2:=ya2;
  xe2:=xf2;            ye2:=yf2+50;

        brush.Color:=rgb(218,218,218);
    rectangle (xb2,yb2,xd2,yd2);
        brush.Color:=rgb(238,238,238);
    Polygon([Point(xb2,yb2), Point(xa2,ya2),
             Point(xf2,yf2), Point(xg2,yg2)]);
        brush.Color:=rgb(198,198,198);
    Polygon([Point(xg2,yg2), Point(xf2,yf2),
             Point(xe2,ye2), Point(xd2,yd2)]);

  //----------------------------------------------------------------------------
 //--flacara--negru--

  pen.width:=2;
    pen.color:=rgb(255,180,0);
  brush.Color:=rgb(255-negru,0,0);        //rosu
  ellipse(245,182,285,193);
  brush.Color:=rgb(0,0,0);
  brush.Style:=bsbdiagonal;
  ellipse(245,182,285,193);

    pen.width:=1;
    pen.color:=rgb(180,180,180);
    brush.Style:=bsSolid;

  //----------------------------------------------------------------------------
 //--apa tuspic oala--2 valori--

       pen.color:=rgb(170,170,100);
       brush.Color:=rgb(160,160,75);
    Polygon([Point(295-xo,190-yo),         Point(291-xo,190-yo),               //jet apa
             Point(291+xbd-xo,190+ybd-yo), Point(295+xbs-xo,190+ybs-yo)]);
                                           //  xbs:=2;ybs:=22;xbd:=-2;ybd:=22;

  //----------------------------------------------------------------------------
 //--oala--xo--yo--ho--

       pen.width:=1;
       pen.color:=rgb(176,180,54);
       brush.Color:=rgb(223,225,175);
    Roundrect(290+55-xo,150-yo,310+55-xo,168-yo,5,5);             //maner
    RoundRect(240+55-xo,120-yo,300+55-xo,194-yo,50,30);           //oala
    Polygon([Point(250+55-xo,180-yo), Point(250+55-xo,186-yo),    //tuspic
             Point(240+55-xo,190-yo), Point(236+55-xo,190-yo)]);
       brush.Color:=rgb(210,210,165);
    RoundRect(240+55-xo,120-yo,300+55-xo,140-yo,50,30);           //gura
       brush.Color:=rgb(200,200,155);
    RoundRect(270+55-xo,145-yo,275+55-xo,190-yo,5,5);             //nivelmetru
       brush.Color:=rgb(0,128,255);
    RoundRect(270+55-xo,145+ho-yo,275+55-xo,190-yo,5,5);          //apa
                                                         //   xo:=160; yo:=10;

  //----------------------------------------------------------------------------
 //--robot sus--xt--ht--pr--
 
 brush.Color:=clwhite;
 pen.Width:=2;
 pen.Color:=clred;
 roundrect(-10,10,350,30,20,20);

 brush.Color:=clwhite;
 pen.Width:=1;
 pen.Color:=clblack;
 rectangle (375-370+xt+pr,222-140+ht,376-370+xt+pr,242-140+ht) ;
 rectangle (404-370+xt-pr,222-140+ht,405-370+xt-pr,242-140+ht) ;
 rectangle (375-370+xt,217-140+ht,405-370+xt,222-140+ht) ;
 rectangle (385-370+xt,137-120,395-370+xt,217-140+ht) ;
 rectangle (380-370+xt,137-120,400-370+xt,187-120) ;

 pen.Width:=2;
 ellipse (410-370+xt,130-120,430-370+xt,150-120);  //roue droite
 ellipse (350-370+xt,130-120,370-370+xt,150-120);  //roue gauche

 pen.Width:=1;
 ellipse (355-370+xt,130-120,425-370+xt,150-120);
 MoveTo (360-370+xt,140-120); LineTo(420-370+xt,140-120);

  //----------------------------------------------------------------------------
 //--cuburi ceai si zahar--xc1--xc2--xc3--yc1--yc2--yc3--

      pen.Width:=1;
      pen.Color:=clblack;
    brush.Color:=rgb(70,70,70);
 rectangle (15+xce1,155+yce1,27+xce1,165+yce1) ;
    brush.Color:=rgb(50,213,50);
 rectangle (47+xce2,155+yce2,59+xce2,165+yce2) ;
    brush.Color:=clwhite;
 rectangle (79+xce3,155+yce3,91+xce3,165+yce3) ;

  //----------------------------------------------------------------------------
 //--containere ceai si zahar--fix--

      pen.Width:=1;
      pen.Color:=clblack;
    brush.Color:=rgb(70,70,70);
 rectangle (5,150,37,180) ;
    brush.Color:=rgb(50,213,50);
 rectangle (37,150,69,180) ;
    brush.Color:=clwhite;
 rectangle (69,150,99,180) ;
 Polygon([Point(98,150),         Point(105,150),
          Point(105,173), Point(98,179)]);



  //----------------------------------------------------------------------------
 //--apa robinet--2 valori--

       pen.color:=rgb(184,216,254);
       brush.Color:=rgb(207,228,254);
    Polygon([Point(322,95),         Point(330,94),               //jet apa
             Point(322+xas,95+yas), Point(330+xad,94+yad)]);
                                            // xas:=12;yas:=42;xad:=-16;yad:=43;

  //----------------------------------------------------------------------------
 //--robot--h2--prinde--culis--

       pen.width:=1;
       pen.color:=rgb(180,180,180);
       brush.Color:=rgb(185,207,208);
    RoundRect(392+20,185-h1,432+20,222,30,20);           //cil jos
 //   RoundRect(392,180-h1,432,190-h1,30,20);            //cil capac
    RoundRect(400+20,170-h2,422+20,191-h1,20,10);        //cil sus


//              x1             y1            x2             y2
    rectangle (370+20-culiss,165-h2,       515+20-culiss,175-h2);            //brat st-dr
    rectangle (375+20-culis,  162-h2,       520+20-culis,  178-h2);            //brat st-dr gros
    rectangle (380+20,        160-h2,       440+20,        180-h2);            //suport brat st-dr
    rectangle (360+20-culiss,155-h2,       370+20-culiss,185-h2);            //suport cleste
    rectangle (340+20-culiss,155-h2+prinde,360+20-culiss,160-h2+prinde);     //cleste sus
    rectangle (340+20-culiss,180-h2-prinde,360+20-culiss,185-h2-prinde);     // cleste jos

  //----------------------------------------------------------------------------
       pen.color:=rgb(180,180,180);
       brush.Color:=rgb(185,207,208);
    roundrect(150,188,170,193,2,2);
       brush.Color:=clred;
    roundrect(150,188,150+timp,193,2,2);

  end;
end;

procedure TForm1.Exit1Click(Sender: TObject);
begin
   close
end;

procedure TForm1.Negru1Click(Sender: TObject);
begin
container:=1;
end;

procedure TForm1.Verde1Click(Sender: TObject);
begin
container:=2;
end;

procedure TForm1.N0cuburi1Click(Sender: TObject);
begin
//container:=3;
zahar:=0;
zah:=false;
end;

procedure TForm1.N11Click(Sender: TObject );
begin
zahar:=1;
zah:=true;
end;

procedure TForm1.N2bucti1Click(Sender: TObject );
begin
zahar:=2;
zah:=true;
end;

procedure TForm1.N3bucti1Click(Sender: TObject);
begin
zahar:=3;
zah:=true;
end;

procedure TForm1.N4bucti1Click(Sender: TObject);
begin
zahar:=4;
zah:=true;
end;

end.
