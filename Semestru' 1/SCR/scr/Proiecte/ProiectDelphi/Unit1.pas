unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls;

type

  TForm1 = class(TForm)
    Image1: TImage;
    Timer1: TTimer;

    Procedure Timer1timer(Sender : TObject);

  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;
  t:integer;
  directie:boolean=true;

  const
  tmax:integer=60;
  
implementation
    Procedure TForm1.Timer1timer(Sender : TObject);

    begin
      with Form1.Image1.Canvas do
      begin

        fillrect(clientrect);          

        brush.Color:=rgb(140,40,200);
        //fata
        //polygon([point(100+t,200+t),point(300+t,200+t),point(300+t,400+t),point(100+t,400+t)]);
        polygon([point(100+t,200),point(300+t,200),point(300+t,400),point(100+t,400)]);
        brush.Color:=rgb(30,220,50);
        //sus
        //polygon([point(100+t,200+t),point(200+t,100+t),point(400+t,100+t),point(300+t,200+t)]);
        polygon([point(100+t,200),point(200+t,100),point(400+t,100),point(300+t,200)]);
        brush.Color:=clyellow;
        //dreapta
        //polygon([point(300+t,200+t),point(400+t,100+t),point(400+t,300+t),point(300+t,400+t)]);
        polygon([point(300+t,200),point(400+t,100),point(400+t,300),point(300+t,400)]);

         brush.Color:=rgb(158,30,60);
        ellipse(150+t,250+t,350+t,450+t);

         brush.Color:=clwhite;

        //animatie
        if (directie)
            then t:=t+1
        else t:=t-1;

        if (t=0) or (t=tmax)
            then directie := not directie;



          //ShowMessage(IntToStr(t));
      end;
    end;
{$R *.dfm}
end.
