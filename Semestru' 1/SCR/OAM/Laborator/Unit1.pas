unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls;

type
  TForm1 = class(TForm)
    tmr1: TTimer;
    img1: TImage;
    procedure tmr1Timer(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;
  t:Integer;
  rotation, x_rot,y_rot,a,b,c,deplasare,i,conveior,x1,x2,x3,y1,y2,y3 :Integer;
  directie:boolean = True;
  faza :Integer = 0;
  const
  t_max = 300;
  x01 = 600;
  y01 = 500;
  x0 = 800;
  y0 = 200;
  r1 = 80;
  r2 = 60;
  r3 = 40;


implementation

uses Math;

{$R *.dfm}

procedure TForm1.tmr1Timer(Sender:TObject);
begin
  with Form1.img1.Canvas do
  begin
    FillRect(ClientRect);

    Pen.Width := 2;
    Pen.color :=clRed;
    Brush.Color := clBlack;
    {Rectangle(200,250,300,350);}
    Polygon([Point(350 - a ,230),Point(250 - a,300),Point(400 - a,300),Point(500 - a,230)]);
    Polygon([Point(250,300 + b),Point(250,450 + b),Point(400,450 + b),Point(400,300 + b)]);
    Polygon([Point(400 + c,450),Point(500 + c,380),Point(500 + c,230),Point(400 + c,300)]);

//    Polygon([Point(350,230), Point(250,300),Point(250,450),Point(400,450),Point(500,380),Point(500,230)]);

    Brush.Color := clBlue;
    Pen.Color := clGreen;

    Ellipse(325,275,425,375);
    Ellipse(325,312,425,337);


    Pen.color :=clRed;
//    Polygon([Point(250,300),Point(400,300)]);
//    Polygon([Point(400,300),Point(400,450)]);
//    Polygon([Point(400,300),Point(500,230)]);

    Rectangle(200, 100, 300 + a, 140);
    Rectangle(280 + a, 140, 300 + a, 260 + b);
    Rectangle(280 + a, 260 + b, 340 + a + c, 280 + b);

    //animatie cub
    if directie then t:=t+10 else
    t:=t-10;
    if(t = 0) or (t = t_max) then
    directie := not directie;

    case faza of
    0 : begin
    a := a + 10;
    if(a > 100) then
    faza := 1;
    end;

    1 : begin
    b := b + 10;
    if(b > 150) then
    faza := 2;
    end;

    2 : begin
    c := c + 10;
    if(c > 200) then
    faza := 3;
    end;

    3 : begin
    c := c -10;
    if(c = 0) then
    faza := 4;
    end;

    4 : begin
    b := b - 10;
    if(b = 0) then
    faza := 5;
    end;

    5 : begin
    a := a - 10;
    if(a = 0) then
    faza := 0;
    end;
  end;

  //modelare antrenare conveior
  RoundRect(200, 400, 800, 420, 20, 20);

  for i := 0 to 10 do
  Begin if deplasare < 100 then
    Begin
     // Rectangle(200 + i * 52 + deplasare, 550, 230 + i * 52 + deplasare, 400);
      MoveTo(600 + i *30 + deplasare, 300);
      LineTo(630 + i * 30 + deplasare, 400);
      if deplasare = 30 then
        deplasare := 0;
    end;
  end;
      case conveior of
      0 : begin
      deplasare := deplasare + 3;
          end;
      end;

  RoundRect(200, 550, 800, 570, 20, 20);


  //modelare simulare brat robotic antropomorf

  pen.Color := clGray;
  pen.Width := 6;
  x1 := trunc(x0 + r1 * sin(a/10));
  y1 := trunc(y0 + r1 * sin(a/10));
  MoveTo(x0,y0);
  LineTo(x0 ,y0 - 100);





  polygon([point(trunc(cos(x_rot)) ,100 - 100 *trunc(sin(y_rot))),point(500,100),point(500,200),point(400,200)]);



   case rotation of

      0 : begin
        x_rot := x_rot + 2;
        y_rot := y_rot + 2;
        if(x_rot > 50) and (y_rot > 50 ) then
        begin
          rotation := 1;
        end;
      end;

            1 : begin
        x_rot := x_rot - 2;
        y_rot := y_rot - 2;
        if(x_rot = 0) and (y_rot = 0 ) then
        begin
          rotation := 0;
        end;
      end;


   end;


  brush.color := clwhite;
end;

end;

end.







