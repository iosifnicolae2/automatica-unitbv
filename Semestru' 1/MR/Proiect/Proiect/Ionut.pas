unit Ionut;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls;

type
  TForm1 = class(TForm)
    Timer1: TTimer;
    Image1: TImage;
    procedure Timer1Ttimer (sender:TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;
  k:integer=0;
  k1:integer=0;
  faza1:integer=0;
  a1:integer=0;
  b1:integer=0;
  c1:integer=0;
  d1:integer=0;
  e1:integer=0;
  faza2:integer=0;
  a2:integer=0;
  b2:integer=0;
  faza3:integer=0;
  a3:integer=0;
  b3:integer=0;
  c3:integer=0;
  d3:integer=0;
  faza4:integer=0;
  a4:integer=0;
  faza5:integer=0;
  a5:integer=0;
  b5:integer=0;
  c5:integer=0;
  d5:integer=0;
  faza6:integer=0;
  a6:integer=0;
  b6:integer=0;
  c6:integer=0;
  d6:integer=0;
  faza7:integer=0;
  a7:integer=0;
  b7:integer=0;
  faza8:integer=0;
  a8:integer=0;
  b8:integer=0;
  c8:integer=0;
  d8:integer=0;
  x1_cerc,y1_cerc:integer;

implementation

    procedure TForm1.Timer1Ttimer (sender:TObject);
    begin
    with Form1.Image1.Canvas do
    begin
    //perete
    brush.Color:=clyellow;
    rectangle(0,0,1605,1105);
    PenPos := Point(0,150);
    lineto(1605,150);
    //cutie_1  (tabla)
    brush.Color:=clgray;
    rectangle(50,250,150,350);
    Polygon([point(100,200),point(200,200), point(150,250),point(50,250)]);
    brush.Color:=clblack;
    Polygon([point(105,212),point(200,212), point(150,250),point(67,250)]);
    brush.Color:=clgray;
    Polygon([point(150,250),point(200,200), point(200,300),point(150,350)]);

    brush.Color:=clblack;


    //BANDA TRANSPORTOARE 3
    //banda spate
    brush.Color:=clgray;
    rectangle(0,1000,1605,1100);
    brush.Color:=clwhite;
    //roti banda transport
    brush.Color:=clwhite;
    RoundRect(25,1060,65,1100,25,1060);
        PenPos := Point(45,1070);
    lineto(45,1080);
    RoundRect(100,1060,140,1100,100,1060);
        PenPos := Point(120,1070);
    lineto(120,1080);
    RoundRect(200,1060,240,1100,200,1060);
        PenPos := Point(220,1070);
    lineto(220,1080);
    RoundRect(300,1060,340,1100,300,1060);
        PenPos := Point(320,1070);
    lineto(320,1080);
    RoundRect(400,1060,440,1100,400,1060);
        PenPos := Point(420,1070);
    lineto(420,1080);
    RoundRect(500,1060,540,1100,500,1060);
        PenPos := Point(520,1070);
    lineto(520,1080);
    RoundRect(600,1060,640,1100,600,1060);
        PenPos := Point(620,1070);
    lineto(620,1080);
    RoundRect(700,1060,740,1100,700,1060);
        PenPos := Point(720,1070);
    lineto(720,1080);
    RoundRect(800,1060,840,1100,800,1060);
        PenPos := Point(820,1070);
    lineto(820,1080);
    RoundRect(900,1060,940,1100,900,1060);
        PenPos := Point(920,1070);
    lineto(920,1080);
    RoundRect(1000,1060,1040,1100,1000,1060);
        PenPos := Point(1020,1070);
    lineto(1020,1080);
    RoundRect(1100,1060,1140,1100,1100,1060);
        PenPos := Point(1120,1070);
    lineto(1120,1080);
    RoundRect(1200,1060,1240,1100,1200,1060);
        PenPos := Point(1220,1070);
    lineto(1220,1080);
    RoundRect(1300,1060,1340,1100,1300,1060);
        PenPos := Point(1320,1070);
    lineto(1320,1080);
    RoundRect(1400,1060,1440,1100,1400,1060);
        PenPos := Point(1420,1070);
    lineto(1420,1080);
    RoundRect(1500,1060,1540,1100,1500,1060);
        PenPos := Point(1520,1070);
    lineto(1520,1080);

    brush.Color:=clgray;
    //banda fata
    rectangle(0,870,1605,1070);



    //cutie_2  (impachetare)
    brush.Color:=clgray;
    rectangle(50,950,150,1050);
    Polygon([point(100,900),point(200,900), point(150,950),point(50,950)]);
    brush.Color:=clblack;
    Polygon([point(105,912),point(200,912), point(150,950),point(67,950)]);
    brush.Color:=clgray;
    Polygon([point(150,950),point(200,900), point(200,1000),point(150,1050)]);
    brush.Color:=clblack;


    //cutie_3 (impachetare)
    //brush.Color:=clgray;
    //rectangle(350,950,450,1050);
    //Polygon([point(400,900),point(500,900), point(450,950),point(350,950)]);
    //brush.Color:=clblack;
    //Polygon([point(405,912),point(500,912), point(450,950),point(367,950)]);
    //brush.Color:=clgray;
    //Polygon([point(450,950),point(500,900), point(500,1000),point(450,1050)]);
    //brush.Color:=clblack;

    //cutie ambalare
    brush.Color:=clred;
    rectangle(585,820,1285,910);
    rectangle(550,940,1250,1060);
    Polygon([point(585,820),point(1285,820), point(1250,940),point(550,940)]);


    brush.Color:=clblack;

    //BANDA TRANSPORTOARE 1
    //banda spate
    brush.Color:=clgray;
    rectangle(250,300,700,350);
    brush.Color:=clwhite;
    //roti banda transport
    brush.Color:=clgray;
    RoundRect(225,270,265,310,225,270);
    brush.Color:=clwhite;
    RoundRect(225,310,265,350,225,310);
        PenPos := Point(245,320);
    lineto(245,330);
    RoundRect(300,310,340,350,300,310);
        PenPos := Point(320,320);
    lineto(320,330);
    RoundRect(400,310,440,350,400,310);
        PenPos := Point(420,320);
    lineto(420,330);
    RoundRect(500,310,540,350,500,310);
        PenPos := Point(520,320);
    lineto(520,330);
    RoundRect(600,310,640,350,600,310);
        PenPos := Point(620,320);
    lineto(620,330);
    RoundRect(685,310,725,350,685,310);
        PenPos := Point(705,320);
    lineto(705,330);
    brush.Color:=clgray;
    RoundRect(685,270,725,310,685,270);

    brush.Color:=clgray;
    //banda fata
    rectangle(250,270,700,320);
    //capat banda
    Polygon([point(250,270), point(225,290),point(225,340),point(250,320)]);
    Polygon([point(700,270), point(725,290),point(725,340),point(700,320)]);


    //Masa suport cuptor
    brush.Color:=clgray;
    rectangle(150,600,750,670);
    Polygon([point(80,500),point(630,500), point(750,600),point(150,600)]);
    Polygon([point(80,570),point(80,500), point(150,600),point(150,670)]);


    //cutie_6  (cuptor)
    brush.Color:=clred;
    rectangle(175,480,600,520);
    //suport piese cuptor
    brush.Color:=clblack;
    rectangle(170,502,670,507);
    brush.Color:=clred;
    rectangle(225,520,650,595);
    Polygon([point(175,480),point(600,480), point(650,520),point(225,520)]);


    //suport robot_1
    brush.Color:=clgray;
    rectangle(750,200,900,250);
    Polygon([point(780,180),point(930,180), point(900,200),point(750,200)]);
    Polygon([point(900,200),point(930,180), point(930,230),point(900,250)]);

    brush.Color:=clblack;


    //suport robot_4
    brush.Color:=clgray;
    rectangle(1350,200,1500,250);
    Polygon([point(1380,180),point(1530,180), point(1500,200),point(1350,200)]);
    Polygon([point(1500,200),point(1530,180), point(1530,230),point(1500,250)]);

    brush.Color:=clblack;


    //presa
    //partea din spate
    brush.Color:=clgray;
    rectangle(1000,70,1250,350);
    Polygon([point(980,60),point(1240,60), point(1250,70),point(1000,70)]);
    Polygon([point(980,60),point(1000,70), point(1000,350),point(980,330)]);

    //presa partea de jos, bucata de sus
    Polygon([point(1000,250),point(1250,250), point(1300,280),point(1050,280)]);


    //miscare brat presa
    brush.Color:=clblack;
    rectangle(1125,100,1190,150+a4);
    case faza4 of
    0:
    begin
    if((d1+a2>610) and (c1>40) and (b3>-120)) then
    a4:=a4+1;
    if(a4>120) then faza4:=1;
    end;
    1:
    begin
    a4:=a4-1;
    if(a4<1) then faza4:=2;
    end;
    2:
    begin
    k:=1;
    end;
    end;
    brush.Color:=clgray;

    //partea de sus
    rectangle(1050,100,1300,130);
    Polygon([point(1000,70),point(1250,70), point(1300,100),point(1050,100)]);
    Polygon([point(1000,110),point(1000,70), point(1050,100),point(1050,130)]);
    //partea de jos
    Polygon([point(1000,350),point(1000,250), point(1050,280),point(1050,400)]);


    //robot_1
    brush.Color:=clgreen;
    rectangle(135+b1,0+a1,155+b1,20);


    case faza1 of
    0:
    begin
    a1:=a1+1;
    if(a1>225)then faza1:=1;
    end;
    1:
    begin
    c1:=0;d1:=0;
    a1:=a1-1;
    c1:=a1-225;
    if(a1<160)then faza1:=2;
    end;
    2:
    begin
    b1:=b1+1;
    d1:=b1;
    if(b1>160)then faza1:=3;
    end;
    3:
    begin
    a1:=a1+1;
    c1:=c1+1;
    if(a1>280) then faza1:=5;
    end;
    4:
    begin
    a1:=a1-1;
    b1:=b1-1;
    if ((a1<160) and (b1<0)) then faza1:=6;
    end;
    5:
    begin
    if(k=1) then faza1:=5;
    if(a2>350)then faza1:=4;
    end;
    6:
    begin
    end;
    end;


           //brat robot_1
    brush.Color:=clgreen;
    rectangle(840,188-a3,850,192);
    rectangle(820-b3,180-a3,850,188-a3);
    rectangle(820-b3,188-a3,830-b3,208-a3+c3);
    brush.Color:=clred;
    RoundRect(838,180-a3,853,195-a3,838,180-a3);
    RoundRect(818-b3,180-a3,833-b3,195-a3,818-b3,180-a3);

    case faza3 of
    0:
    begin
    if(a2>350)then
    a3:=a3+1;
    if(a3>50)then faza3:=1;
    end;
    1:
    begin
    b3:=b3+1;
    if((b3>170) and (k1=0))then faza3:=2;
    if((b3>170)and (k1>0))then faza3:=7;
    end;
    2:
    begin
    c3:=c3+1;
    if(c3>120)then faza3:=3;
    end;
    3:
    begin
    d3:=d3+1;
    c3:=c3-1;
    c1:=c1-1;
    if((d3>80) and (c3<80)) then faza3:=4;
    end;
    4:
    begin
    b3:=b3-1;
    d1:=d1+1;
    if(b3<-340)then faza3:=5;
    end;
    5:
    begin
    c3:=c3+1;
    c1:=c1+1;
    if(c3>110)then faza3:=6;
    end;
    6:
    begin
    c3:=c3-1;
    k1:=k1+1;
    if(c3<0)then faza3:=1;
    end;
    7:
    begin
    end;
    end;

 //banda transportoare 1
    case faza2 of
    0:
    begin
    if ((d1>160) and (a1>280)) then
    a2:=a2+1;
    if(a2>350) then faza2:=1;
    end;
    1:
    begin
    end;
    end;

     //brat robot_4
    brush.Color:=clgreen;
    rectangle(1440,188-a5,1450,192);
    rectangle(1420-b5,180-a5,1450,188-a5);
    rectangle(1420-b5,188-a5,1430-b5,208-a5+c5);
    brush.Color:=clred;
    RoundRect(1438,180-a5,1453,195-a5,1438,180-a5);
    RoundRect(1418-b5,180-a5,1433-b5,195-a5,1418-b5,180-a5);

    case faza5 of
    0:
    begin
    if(k=1) then
    a5:=a5+1;
    if(a5>50)then faza5:=1;
    end;
    1:
    begin
    b5:=b5+1;
    if(b5>250)then faza5:=2;
    end;
    2:
    begin
    c5:=c5+1;
    if(c5>110)then faza5:=3;
    end;
    3:
    begin
    d5:=d5+1;
    c5:=c5-1;
    c1:=c1-1;
    if((d5>80) and (c5<80)) then faza5:=4;
    end;
    4:
    begin
    b5:=b5-1;
    d1:=d1+1;
    if(b5<=-40)then faza5:=5;
    end;
    5:
    begin
    c5:=c5+1;
    c1:=c1+1;
    if(c5>=260)then faza5:=6;
    end;
    6:
    begin
    c5:=c5-1;
    if(c5<0)then faza5:=7;
    end;
    7:
    begin
    end;
    end;

    //cutie_5 (degresare)
    brush.Color:=clgray;
    Polygon([point(1400,550),point(1600,550), point(1500,600),point(1300,600)]);

    //cutie_4 (vopsea)
    Polygon([point(1000,550),point(1200,550), point(1100,600),point(900,600)]);

    //cuptor partea din spate
    brush.Color:=clred;
    rectangle(175,480,600,520);
    //tabla
    Brush.Color:=clblack;
    Polygon([point(105+d1+a2+d6+b7,212+c1+c6),point(188+d1+a2+d6+b7,212+c1+c6), point(150+d1+a2+d6+b7,250+c1+c6),point(67+d1+a2+d6+b7,250+c1+c6)]);

    //presa partea de jos, panou fata
    brush.Color:=clgray;
    rectangle(1050,280,1300,400);

    //brat_1 capat magnetic
    brush.Color:=clgray;
    RoundRect(115+b1,10+a1,175+b1,-20+a1,115+b1,10+a1);

    //brat2
    brush.Color:=clgreen;
    rectangle(1450+b6,0,1470+b6,120+a6+c6);
    rectangle(1450+b6,120+a6+c6,1470+b6,200+a6+c6);
    brush.Color:=clblack;
    rectangle(1458+b6,200+a6+c6,1462+b6,240+a6+c6);
    rectangle(1445+b6,240+a6+c6,1462+b6,245+a6+c6);
    brush.Color:=clred;
    RoundRect(1448+b6,110+a6+c6,1473+b6,140+a6+c6,1448+b6,110+a6+c6);
    case faza6 of
    0:
    begin
    a6:=a6+1;
    if(a6>180) then faza6:=1;
    end;
    1:
    begin
    if((b5=-40) and (c5=260))then faza6:=2;
    end;
    2:
    begin
    c6:=c6+1;
    if(c6>160)then faza6:=3;
    end;
    3:
    begin
    c6:=c6-1;
    if(c6<100)then faza6:=4;
    end;
    4:
    begin
    b6:=b6-1;
    d6:=d6-1;
    if(b6<-380) then faza6:=5;
    end;
    5:
    begin
    c6:=c6+1;
    if(c6>160)then faza6:=6;
    end;
    6:
    begin
    c6:=c6-1;
    if(c6<100)then faza6:=7;
    end;
    7:
    begin
    b6:=b6-1;
    d6:=d6-1;
    if(b6<-800) then faza6:=8;
    end;
    8:
    begin
    b6:=b6+1;
    if(a6>0) then a6:=a6-1;
    if(b6=0) then faza6:=9;
    end;
    9:begin
    end;
    end;


    case faza7 of
    0:
    begin
    if(b6=-800) then faza7:=1;
    end;
    1:
    begin
    a7:=a7-1;
    b7:=b7-1;
    if(a7<-452) then faza7:=2;
    end;
    2:
    begin
    end;
    end;

    
        //suport robot_2
    brush.Color:=clgray;
    rectangle(0,700,200,750);
    Polygon([point(30,680),point(230,680), point(200,700),point(0,700)]);
    Polygon([point(200,700),point(230,680), point(230,730),point(200,750)]);

    brush.Color:=clblack;


                    //brat robot_2
    brush.Color:=clgreen;
    rectangle(60,690,70,642+a8);
    rectangle(60,630+a8,90+b8,638+a8);
    rectangle(80+b8,638+a8,90+b8,658+a8);
    brush.Color:=clred;
    RoundRect(78+b8,630+a8,93+b8,645+a8,78+b8,630+a8);
    RoundRect(58,630+a8,73,645+a8,58,630+a8);


    case faza8 of
    0:
    begin
    if(a7=-350) then faza8:=1;
    end;
    1:
    begin
    a8:=a8-1;
    if(a8<-150)then faza8:=2;
    end;
    2:
    begin
    b8:=b8+1;
    if(b8>85) then faza8:=3;
    end;
    3:
    begin
    b8:=b8-1;
    d1:=d1-1;
    if(b8=30) then faza8:=4;
    end;
    4:
    begin
    a8:=a8+1;
    c1:=c1+1;
    if(a8>250) then faza8:=5;
    end;
    5:
    begin

    end;
    end;


    //cutie_4 (vopsea)
    brush.Color:=clgray;
    rectangle(900,600,1100,800);
    brush.Color:=clblack;
    Polygon([point(1005,562),point(1200,562), point(1100,600),point(917,600)]);
    brush.Color:=clgray;
    Polygon([point(1100,600),point(1200,550), point(1200,750),point(1100,800)]);

    brush.Color:=clblack;

    //cutie_5 (degresare)
    brush.Color:=clgray;
    rectangle(1300,600,1500,800);
    brush.Color:=clblack;
    Polygon([point(1405,562),point(1600,562), point(1500,600),point(1317,600)]);
    brush.Color:=clgray;
    Polygon([point(1500,600),point(1600,550), point(1600,750),point(1500,800)]);

        //cutie_6  (cuptor)
    brush.Color:=clred;


    //suport piese cuptor
    brush.Color:=clblack;
    rectangle(170,502,670,507);
    brush.Color:=clred;
    rectangle(225,520,650,595);
    Polygon([point(175,480),point(600,480), point(650,520),point(225,520)]);


    end;


  //miscare banda transportoare 1
  //x1_cerc := trunc(245 + 20*cos(b3/42));
  //y1_cerc := trunc(320 + 20*sin(b3/42));
  //moveto(225,270);
  //lineto(x1_cerc,y1_cerc);
    //moveto(100,100);
    //lineto(150,150);
    //pen.Width:=5;
    //pen.Color:=clblue;
    //brush.Color:=clyellow;
    //ellipse(400,100,600,200);
    //Polyline([point(500,500),point(600,300), point(500,300),point(400,500)]);
    end;

{$R *.dfm}

end.

