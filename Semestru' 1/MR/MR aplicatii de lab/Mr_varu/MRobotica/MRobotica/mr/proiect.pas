unit proiect;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls;

type
  TForm1 = class(TForm)
    Image1: TImage;
    Timer1: TTimer;


  Procedure Timer1Timer(Sender:TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;


var
  Form1: TForm1;
  t:integer;
  directie:boolean=true;
  x1,y1,x10,y10,x0,y0:integer;
  const
  tmax:integer=14;
  raza1:integer=40;

implementation
{$R *.dfm}


Procedure TForm1.Timer1Timer(Sender:TObject);
begin
  with Form1.Image1.Canvas do
  begin
  fillrect(clientrect);
  pen.Color:=clblack;
  pen.width:=4;
  //prehensor
  moveTo(10,50);
  lineTo(50,50);
  moveTo(10,60);
  lineTo(50,60);
  moveTo(40,30);
  lineTo(40,50);
  moveTo(40,60);
  lineTo(40,80);
  moveTo(40,30);
  lineTo(50,30);
  moveTo(50,30);
  lineTo(50,80);
  moveTo(50,80);
  lineTo(40,80);
  pen.width:=4;
  pen.Color:=clblue;
  brush.color:=clred;
  //degete prehensor
  polygon([point(50,30+t),point(80,30+t),point(80,40+t),point(50,40+t)]);
  polygon([point(50,70-t),point(80,70-t),point(80,80-t),point(50,80-t)]);
  //brat robot
  x1:=round(x0+raza1*cos(t/10));
  y1:=round(y0+raza1*sin(t/10));
  pen.Width:=4;
  MoveTo(x10,y10);
  LineTo(x1,y1);
  if directie then t:=t+1
              else t:=t-1;
  if (t=tmax) or (t=0)
     then directie:=not directie;
   brush.color:=clwhite;


    end;
  end;

end.
