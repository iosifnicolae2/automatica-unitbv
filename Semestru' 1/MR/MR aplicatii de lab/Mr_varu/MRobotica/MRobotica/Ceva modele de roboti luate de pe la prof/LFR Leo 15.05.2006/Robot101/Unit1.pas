unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls;

type
  TForm1 = class(TForm)
    Image1: TImage;
    Button1: TButton;
    Timer1: TTimer;
    Button2: TButton;
    procedure Timer1Timer(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;
  t:integer=0;
  tp1,tp2,t1,t2,t3,t4,t5,t6,t7,t8,t9,t10,t11,t12,t13,t14,t15,t16,t17,t18,t19,t20,t21,t22,t23,t24,t25,t26,t27,t28,t29,x1,y1,x2,y2:integer;
  //tp1,tp2 timpi piston 3
  dir,d:boolean;
   x0:integer=390;
   y0:integer=10;
   q:real=10;
   q1:real=1;
   directie:boolean=true;
   directie_linie:boolean=true;

const
  r1=20;
  r2=20;
  r3=30;
  r4=32;
  qmax=380;
  q1max=400;

implementation

{$R *.DFM}

procedure TForm1.Timer1Timer(Sender: TObject);
var
x1,y1:integer;
begin


  x1:=round(x0-r1*cos(t2/15+t23));
  y1:=round(y0-r1*sin(t2/15+t23));
  x2:=round(x1-r2*cos(t3/70+t23));
  y2:=round(y1-r2*sin(t3/70+t23));

with form1.image1.canvas do
begin


  fillrect(clientrect); {functie care ne curata imaginea dupa fiecare poza}
   rectangle(380,0,400,20);
   MoveTo(x0,y0);
        pen.Color:=clred;

   pen.Width:=15;
    LineTo(x1,y1);
  pen.color:=clblue;
     LineTo(x2,y2);
  //pen.color:=clblue;
   //pen.Width:=10;
  pen.width:=1;


//pen.color:=rgb(20,40,255);
//pen.style:=psdash;        //incadreaza cu albastru punctat functiile
//brush.Color:=rgb(200,100,20); {coloarea raftului de depozitare}
//brush.Style:=bsbdiagonal;   {imaginea apare hasurata oblic cu negru}

brush.Color:=rgb(15,100,20); {coloarea primului piston de presare}
rectangle(380,100+t12-t13,400,200+t12-t13);   //piston1
 brush.Color:=rgb(150,100,20); {coloarea pistonului de presare 2}
 rectangle(420,100+t15+t16,440,200+t15+t16);   //piston 2

        brush.Color:=rgb(150,0,255); {coloarea pistonului de presare 3}
        rectangle(460,100+tp1+tp2,480,200+tp1+tp2);   //piston 3

 brush.Color:=rgb(0,0,0); {coloarea raftului de depozitare}
 rectangle(380,260,530,380); //nou

 brush.Color:=rgb(0,255,0);
 rectangle(495,100,535,380);   // rect dreapata (ultimul)
brush.Color:=rgb(255,0,0);
rectangle(380+t14-t17,255,420+t14-t17,260);//suport mobil presa


brush.Color:=rgb(100,100,20);
rectangle(375,100,405,150);       //cilindru presa 1
rectangle(415,100,445,150);       //cilindru presa 2

         // introduc cilindru presa 3
          rectangle(455,100,485,150);

brush.Color:=rgb(10,100,255);
rectangle(375,70,550,100);     // tavan

 rectangle(0,380,1100,390);  //podea
 brush.Color:=rgb(0,100,20);
 //rectangle(650,330,750,380);   //suport brat robot
  brush.Color:=rgb(50,50,20);

  brush.Color:=rgb(20,200,2); //suport pt piese

    ellipse (550,100,590,140) ;


   if(t<=30)then t1:=t;

     if ((t<=60)and(t>30)) then
             begin
        t1:=30;
        t2:=t-30
        end;
      if ((t<=65)and(t>60)) then
             begin
        t1:=30;
        t2:=30;
        t5:=t-60
        end;

     if ((t<=110)and(t>65)) then
             begin
        t1:=30;
        t2:=-t+95;
        t4:=-t+65
        end ;

     if ((t<=285)and(t>100)) then
             begin
        t8:=t-100
        end      ;

     if ((t<=325)and(t>285)) then
             begin
        t9:=t-285
        end ;

      if ((t<=330)and(t>325)) then
             begin
        t10:=t-325
        end       ;

      if ((t<=360)and(t>330)) then
             begin
        t11:=t-330
        end ;

      if ((t<=390)and(t>360)) then
             begin
        t12:=t-360
        end  ;

      if ((t<=420)and(t>390)) then
             begin
        t13:=t-390
        end;

       if ((t<=460)and(t>420)) then      //miscare dreapta piesa la presa
             begin
        t14:=t-420
        end;

        if ((t<=490)and(t>460)) then      //miscare ptrsa 2
             begin
        t15:=t-460
        end ;

        if ((t<=520)and(t>490)) then       //ridicare presa2
             begin
        t16:=-t+490
        end ;


        // incerc o miscare a pisei pt pistonul 3 si a pistonului 3


         if ((t<=550)and(t>520)) then      //miscare dreapta piesa la presa
             begin
        t14:=t-520
        end;

        if ((t<=580)and(t>550)) then      //miscare ptrsa 2
             begin
        tp1:=t-550
        end ;

        if ((t<=610)and(t>580)) then       //ridicare presa2
             begin
        tp2:=-t+580
        end ;








       if ((t<=560)and(t>520)) then      //miscare stanga piesa la presa
             begin
        t17:=t-520
        end;

       if ((t<=590)and(t>560)) then      //miscare dreapta robot cartezian
             begin
        t18:=t-560
        end  ;

       if ((t<=595)and(t>590)) then      //prindere obiect
             begin
        t19:=t-590
        end ;

       if ((t<=635)and(t>595)) then      //miscare dreapta robot cartezian
             begin
        t20:=t-595
        end   ;

        if ((t<=820)and(t>635)) then      //miscare verticala
             begin
        t21:=t-635
        end ;

       if ((t<=860)and(t>820)) then      //miscare dreapta robot cartezian
             begin
        t22:=t-820
        end;


       if ((t<=1300)and(t>860)) then      //miscare dreapta robot cartezian
             begin
        t23:=t-860
        end



else dir:= dir;
 brush.Color:=rgb(20,200,2);
rectangle(100+t2+t9-t11+t18-t20+t22,190-t1*5+t8-t21,300+t2+t9-t11+t18-t20+t22,225-t1*5+t8-t21); //brat miscare orizontala
brush.Color:=rgb(20,100,2);
rectangle(300+t2+t9-t11+t18-t20+t22,180-t1*5+t8-t21,320+t2+t9-t11+t18-t20+t22,235-t1*5+t8-t21); //brat miscare orizontala

 brush.Color:=rgb(200,200,20);

rectangle(130,180-t1*5+t8-t21,200,235-t1*5+t8-t21); //piston brat


brush.Color:=rgb(200,0,0);
rectangle(320+t2+t9-t11+t18-t20+t22,190-t1*5+t8+t5-t10+t19-t21,350+t2+t9-t11+t18-t20+t22,200-t1*5+t8+t5-t10+t19-t21); {pensa 1 sus}
rectangle(320+t2+t9-t11+t18-t20+t22,215-t1*5+t8-t5+t10-t19-t21,350+t2+t9-t11+t18-t20+t22,225-t1*5+t8-t5+t10-t19-t21); {pensa 2 jos}

 brush.Color:=rgb(0,0,255);//piesa

 rectangle(350+t4+t9+t14-t17-t20+t22,55-t3*5+t8-t21,380+t4+t9+t14-t17-t20+t22,60-t3*5+t8-t21);{piesa}
 rectangle(380+t4+t9+t14-t17-t20+t22,43-t3*5+t8-t21,410+t4+t9+t14-t17-t20+t22,70-t3*5+t8-t21);{piesa}

brush.Color:=rgb(250,120,20);   //suport brat ridicator, coborator

rectangle(120,20,180,350);
brush.Color:=rgb(10,100,10);
rectangle(100,350,200,380); {suport brat ridicator}


Form1.Image1.Canvas.Brush.Color:=clwhite;
if dir then
begin
t:=t+1;
 end

else
 begin
 t:=t-1;
    end;

 if (t>50000) or (t<0)then  dir:= not dir;
  if directie then q:=q-1
             else q:=q+1;
 if (q=qmax)or(q=0) then  directie :=not directie ;
 if directie_linie then q1:=q1-10
                    else q1:=q1+1;
 if (q1=q1max)or(q1=0)then directie_linie :=not directie_linie;

 end;
 end;



procedure TForm1.Button1Click(Sender: TObject);
begin
form1.close
end;

procedure TForm1.Button2Click(Sender: TObject);
begin
timer1.enabled:=not (timer1.enabled);
end;

end.
