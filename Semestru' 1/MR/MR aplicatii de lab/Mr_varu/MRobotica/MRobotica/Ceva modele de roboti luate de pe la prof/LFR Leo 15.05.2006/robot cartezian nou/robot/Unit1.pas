unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls;

type
  TForm1 = class(TForm)
    Image1: TImage;
    Button1: TButton;
    Timer1: TTimer;
    Button2: TButton;
    procedure Timer1Timer(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;
  t:integer=0;
  t1,t2,t3,t4,t5,t6:integer;
  dir,d:boolean;

implementation

{$R *.DFM}

procedure TForm1.Timer1Timer(Sender: TObject);
begin

with form1.image1.canvas do
begin
  fillrect(clientrect);
pen.width:=2; {grosimea linilor}
//pen.color:=rgb(00,00,250); {culoarea  robotului , nu se pot da valori mai mari de 250}
pen.style:=psdash;
brush.Color:=rgb(170,10,00);
//brush.Style:=bsbdiagonal;



if(t<=30)then t1:=t
else if(t<=60)then
begin t1:=30; t2:=t-30;  end
else if (t<=65) then
begin t1:=30; t2:=30;t5:=t-60;  end

else if (t<=95) then begin t1:=30; t2:=-t+95; t4:=-t+65;t3:=0; end

else if (t<=125) then begin   t1:=125-t; t3:=t1-30;t4:=t2-30;   end
else if (t<=155) then begin t1:=0; t2:=t-125; t3:=t1-30; t4:=t2-30; end
else if    (t<=160)then begin t1:=0; t2:=30; t5:=-t+160;end
else if (t<=190)then begin t1:=0; t2:=-t+190; t3:=-30; t4:=0;end
         else if (t<=250)then begin t1:=0; t2:=0; t3:=0; t4:=0;t5:=0;end

else dir:=not dir;
{t4-timpul in care robotul urca}
{t2-timpul in care bratul drept se deplaseaza la dreapta}
{t3-timpul in care  robotul coboara}
{t1-timpul in care bratul drept  se retrage }
{t5-timpul in care se stringe clestele}
 ellipse(30-t1,280,80-t1,180);
 ellipse(450-t1,280,500-t1,180);
rectangle(100+t2,105-t1,320+t2,130-t1);brush.color:=clblue;{bratul drept}

rectangle(130,80-t1,200,185-t1);{dreptunghiul pe care sta bratul robotului}
rectangle(320+t2,105-t1+t5,330+t2,110-t1+t5); {clestele sus din dreapta}
rectangle(320+t2,125-t1-t5,330+t2,130-t1-t5); brush.color:=clsilver; {clestele jos din dreapta}
rectangle(350+t4,85-t3,380+t4,90-t3);brush.color:=clyellow;{piesa}

rectangle(120,150,210,200);brush.color:=clred; {baza de pe baza}
rectangle(100,200,230,280);brush.color:=clmoneygreen;{baza}
rectangle(5,280,525,300);brush.color:=clblack;{baza}
rectangle(380,60,400,280);brush.color:=clblue;{bara dreapta}
rectangle(375,240-t1,380,280-t1);
rectangle(360,120,380,130);brush.color:=clblue;
rectangle(360,90,380,100); brush.Color:=clyellow;
//rectangle (360+t4,100-t3,320+t4,280);brush.color:=clfuchsia;
rectangle(300+t4,230,380+t4,280);brush.Color:=clmedgray;

if dir then
begin
t:=t+2;
 end

else
 begin
 t:=t-2;
    end;

 if (t>191) or (t<0)then dir:= not dir;

 end;
 end;


procedure TForm1.Button1Click(Sender: TObject);
begin
form1.close
end;

procedure TForm1.Button2Click(Sender: TObject);
begin
timer1.enabled:=not (timer1.enabled);
end;


end.
