unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Menus;

type
  TForm1 = class(TForm)
    Image1: TImage;
    Button1: TButton;
    Timer1: TTimer;
    Button2: TButton;
    PopupMenu1: TPopupMenu;
    procedure Timer1Timer(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;
  t:integer=0;
  t1,t2,t3,t4,t5,t6,t7,t8,t9,t10,t11,t12,t13:integer;
  dir,d:boolean;
  x,y:integer;
  a,b,c:integer;
  a1,b1:integer;
  xc,yc:integer;
  dx,dy,dz:integer;
  qx:integer;
  qy:integer;
  unghi:real;
  unghi1:real;
  unghi2:real;

implementation

{$R *.DFM}

procedure TForm1.Timer1Timer(Sender: TObject);
begin

with form1.image1.canvas do
begin
  fillrect(clientrect);
pen.width:=1;
//pen.color:=rgb(20,40,255);
//pen.style:=psdash;
brush.Color:=rgb(200,100,20);
//brush.Style:=bsbdiagonal;



if(t<=20)then t1:=t+170
else if (t<=70)then begin t1:=-35; t2:=70-t;dz:=t-23;unghi:=unghi+0.1;end
else if (t<=80)then begin t1:=-35; dz:=t-23;unghi:=unghi+0.1;end

else if (t<=100)then begin t1:=-35; dy:=t-80;end
else if (t<=120)then begin t1:=-35; dy:=120-t;end
else if (t<=190)then begin t1:=-35; dz:=t-63;
unghi:=unghi+0.1;end

else if (t<=212)then begin t13:=t-190;end

else if (t<=332)then begin t1:=-35; a:=212-t;unghi1:=unghi1-0.1;end
else if (t<=472)then begin t1:=-35; b:=t-332;end

else if (t<=477)then begin t1:=-35; c:=t-472;end
else if (t<=557)then begin t1:=-35;t3:=qx+1; b:=617-t;qx:=t-477;end
else if (t<=677)then begin t1:=-35;a:=t-677;unghi1:=unghi1+0.1;
dz:=t-430;end
else if (t<=812)then begin t1:=-35;qx:=757-t;t3:=qx;b:=t-617;end
else if (t<=817)then begin t1:=-35;c:=817-t;end
else if (t<=932)then begin t1:=-35;b:=1012-t;end

else if (t<=1032)then begin t1:=-35;dz:=t-685;qy:=t-932;
unghi2:=unghi2+0.1;end

else if (t<=2200)then begin t1:=-35;end   


else dir:=not dir;

 brush.Color:=rgb(0,0,255);
rectangle(29+t4+dz+t13,310-t3,65+t4+dz+t13,315-t3);            {piesa}
// brush.Color:=rgb(0,0,255);
//rectangle(350+t7,115-t6,385+t7,120-t6);
// brush.Color:=rgb(0,0,255);
//rectangle(350+t9,145-t8,385+t9,150-t8);

brush.Color:=rgb(100,170,0);                  {freza pt taiat}
rectangle(90,235,120,250);
brush.Color:=rgb(40,50,100);
rectangle(100,250,110,275+dy);
brush.Color:=rgb(40,170,0);
rectangle(86,275+dy,122,280+dy);


// brush.Color:=rgb(0,0,255);
//rectangle(350+t4,90-t3,385+t4,85-t3);

{//pen.Color:=clblue;
    brush.Color:=clnavy;
    ellipse(100+dx,445,130+dx,475);  // prima roata
    ellipse(200+dx,445,230+dx,475);  // a 2-a roata
      x:=trunc(115+15*sin(unghi));
      y:=trunc(460-15*cos(unghi));
      pen.Width:=2;
      pen.Color:=clred;
      moveto(115+dx,460); lineto(x+dx,y); // raza primei roti
      x:=trunc(215+15*sin(unghi+1.5));
      y:=trunc(460-15*cos(unghi+1.5));
      moveto(215+dx,460); lineto(x+dx,y);//raza roata 2
pen.Color:=clblack;
rectangle(0,475,1000,478);  }

pen.Color:=clred;
pen.Width:=2;
    brush.Color:=clnavy;
    ellipse(15,325,45,355);  // prima roata
    ellipse(195,325,225,355);  // a 2-a roata
      pen.Width:=2;
      pen.Color:=clred;
      
      x:=trunc(30+15*sin(unghi));
      y:=trunc(340-15*cos(unghi));
      moveto(30+dx,340); lineto(x+dx,y); // raza1 a primei roti

      x:=trunc(210+15*sin(unghi+2.0943));
      y:=trunc(340-15*cos(unghi+2.0943));
      moveto(210+dx,340); lineto(x+dx,y);//raza2 a rotii 2

      rectangle(27,325,213,327);        {banda transportoare}
      rectangle(27,354,213,356);
pen.Color:=clblack;

brush.Color:=rgb(100,120,120);
rectangle(30+dz+t13,300-qx,65+dz+t13,310-qx);       {rafturile separate}
rectangle(30+dz+t13,315-qx,65+dz+t13,325-qx);


 pen.Width:=4;
 brush.Color:=clblack;
moveto(0,460);lineto(1000,460);
 //pen.Width:=1;

   pen.color:=clBlue;
   pen.Width :=4;
   moveto(120,97);
   lineto(420,97); // linia de rulare

brush.color:=clgreen;pen.color:=clbtnface;pen.Width:=2;

   rectangle(260+a,45,370+a,85); // carut

   ellipse(280+a,75,300+a,95);  // roata 1
  a1:=round(290+10*sin(unghi1));
  b1:=round(85-10*cos(unghi1));
  pen.color:=clblack;
  moveto(290+a,85); lineto(a1+a,b1); // raza roata 1

   pen.color:=clbtnface;
   ellipse(330+a,75,350+a,95); //roata 2
  a1:=round(340+10*sin(unghi1+1));
  b1:=round(85-10*cos(unghi1+1));
  pen.color:=clblack;
  moveto(340+a,85); lineto(a1+a,b1); // raza roata 2

   rectangle(306+a,85,326+a,135);// cilindru piston
   rectangle(308+a,135,324+a,140+b); // piston
   rectangle(285+a,140+b,350+a,155+b); // suport prehensor
   rectangle(285+a+c,155+b,295+a+c,185+b); // brat 1 prehensor
   rectangle(340+a-c,155+b,350+a-c,185+b); // brat 2 prehensor

pen.Color:=clblack;


      ellipse(296+qy,400,356+qy,460);
      x:=trunc(326+30*sin(unghi2));
      y:=trunc(430-30*cos(unghi2));
      moveto(326+qy,430); lineto(x+qy,y); // raza rotii camion

      pen.Width:=4;
      moveto(276+qy,430);lineto(276+qy,390);   //camionul
//      moveto(276+qy,410);lineto(291+qy,390);   //
      moveto(276+qy,390);lineto(376+qy,390);   //
//      moveto(361+qy,390);lineto(376+qy,410);   //
      moveto(376+qy,390);lineto(376+qy,430);   //
      moveto(376+qy,410);lineto(426+qy,410);   //
      moveto(376+qy,430);lineto(426+qy,430);   //


      pen.Width:=6;pen.Color:=rgb(122,255,226);        // recipientul
      moveto(276+qy,344);lineto(276+qy,384);           // pentru
      moveto(276+qy,384);lineto(356+qy,384);           // piesa
      moveto(356+qy,384);lineto(356+qy,344);           // ambalata
      pen.Color:=clblack;


Form1.Image1.Canvas.Brush.Color:=clwhite;
if dir then
begin
t:=t+1;
 end

else
 begin
 t:=t-1;
    end;

 if (t>3501) or (t<0)then dir:= not dir;

 end;
 end;


procedure TForm1.Button1Click(Sender: TObject);
begin
form1.close
end;

procedure TForm1.Button2Click(Sender: TObject);
begin
timer1.enabled:=not (timer1.enabled);
end;

end.
