unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls;

type
  TForm1 = class(TForm)
    Image1: TImage;
    Button1: TButton;
    Timer1: TTimer;
    Button2: TButton;
    Button3: TButton;
    procedure Timer1Timer(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;
  t:integer=0;
  t1,t2,t3,t4,t5,t6,t7,t8,t9,t10,t11,t12,t13,t14,t15,t16,t17,t18,t19,t20,t21,t22,t23,t24,t25,t26,t27,t28,t29,t30,t31,t40,x1,y1,x2,y2:integer;
  dir,d:boolean;
   x0:integer=390;
   y0:integer=10;
   q:real=10;
   q1:real=1;
   directie:boolean=true;
   directie_linie:boolean=true;

const
  r1=20;
  r2=20;
  r3=30;
  r4=32;
  qmax=380;
  q1max=400;

implementation

{$R *.DFM}

procedure TForm1.Timer1Timer(Sender: TObject);
var
x1,y1:integer;
begin


  x1:=round(x0-r1*cos(t2/15+t23));
  y1:=round(y0-r1*sin(t2/15+t23));
  x2:=round(x1-r2*cos(t3/70+t23));
  y2:=round(y1-r2*sin(t3/70+t23));

with form1.image1.canvas do
begin


  fillrect(clientrect); {functie care ne curata imaginea dupa fiecare poza}
   rectangle(380,0,400,20);
   MoveTo(x0,y0);
        pen.Color:=clred;

   pen.Width:=15;
    LineTo(x1,y1);
  pen.color:=clblue;
     LineTo(x2,y2);
  //pen.color:=clblue;
   //pen.Width:=10;
  pen.width:=1;


//pen.color:=rgb(20,40,255);
//pen.style:=psdash;        //incadreaza cu albastru punctat functiile
//brush.Color:=rgb(200,100,20); {coloarea raftului de depozitare}
//brush.Style:=bsbdiagonal;   {imaginea apare hasurata oblic cu negru}

brush.Color:=rgb(50,100,20); {coloarea raftului de depozitare}
rectangle(380,100+t12-t13,400,200+t12-t13);   //brat1
 brush.Color:=rgb(150,100,20); {coloarea raftului de depozitare}
 rectangle(420,100+t15+t16,440,200+t15+t16);   //brat2
 rectangle(460,100+t30+t31,480,200+t30+t31);   //brat3
 brush.Color:=rgb(200,100,20); {coloarea raftului de depozitare}
 rectangle(380,260,490,380); //nou

 rectangle(490,100,530,380);   // presa vertical stanga{suport raft}
brush.Color:=rgb(100,100,20);
rectangle(380+t40+t14+t17,255,420+t40+t14+t17,260);//piesa mobila presa



rectangle(375,100,405,150);       //cilindru presa 1
rectangle(415,100,445,150);       //cilindru presa 2
rectangle(455,100,485,150);       //cilindru presa 3
rectangle(375,70,530,100);

 rectangle(0,380,1100,390);  //podea
 brush.Color:=rgb(0,100,20);
 //rectangle(650,330,750,380);   //suport brat robot
  brush.Color:=rgb(50,50,20);

  brush.Color:=rgb(20,200,2); //suport pt piese

    ellipse (550,100,590,140) ;


   if(t<=30)then t1:=t;

     if ((t<=60)and(t>30)) then
             begin
        t1:=30;
        t2:=t-30
        end;
      if ((t<=65)and(t>60)) then
             begin
        t1:=30;
        t2:=30;
        t5:=t-60
        end;

     if ((t<=110)and(t>65)) then
             begin
        t1:=30;
        t2:=-t+95;
        t4:=-t+65
        end ;

     if ((t<=285)and(t>100)) then
             begin
        t8:=t-100
        end      ;

     if ((t<=325)and(t>285)) then
             begin
        t9:=t-285
        end ;

      if ((t<=330)and(t>325)) then
             begin
        t10:=t-325
        end       ;

      if ((t<=360)and(t>330)) then    // brat
             begin
        t11:=t-330
        end ;

      if ((t<=390)and(t>360)) then     // brat 
             begin
        t12:=t-360
        end  ;

      if ((t<=420)and(t>390)) then    // brat 
             begin
        t13:=t-390
        end;

       if ((t<=460)and(t>420)) then      //miscare dreapta piesa la presa s1
             begin
        t14:=t-420
        end;


        if ((t<=490)and(t>460)) then      //miscare ptrsa 2
             begin
        t15:=t-460
        end ;

        if ((t<=520)and(t>480)) then       //ridicare presa2
             begin
        t16:=-t+490
        end ;

         if ((t<=540)and(t>510)) then      //miscare presa 3
             begin
        t30:=t-510
        end ;

        if ((t<=580)and(t>540)) then       //ridicare presa 3
             begin
        t31:=-t+550
        end ;

       if ((t<=560)and(t>520)) then      //miscare stanga piesa la presa
             begin
        t17:=t-520
        end;

         if ((t<=530)and(t>560)) then      //miscare stanga piesa la presa
             begin
        t40:=t-480
        end;

       if ((t<=590)and(t>560)) then      //miscare dreapta robot cartezian
             begin
        t18:=t-560
        end  ;

       if ((t<=595)and(t>590)) then      //prindere obiect
             begin
        t19:=t-590
        end ;

       if ((t<=635)and(t>595)) then      //miscare dreapta robot cartezian
             begin
        t20:=t-595
        end   ;

        if ((t<=820)and(t>635)) then      //miscare verticala
             begin
        t21:=t-635
        end ;

       if ((t<=860)and(t>820)) then      //miscare dreapta robot cartezian
             begin
        t22:=t-820
        end;



       if ((t<=1300)and(t>860)) then      //miscare dreapta robot cartezian
             begin
        t23:=t-860
        end





else dir:= dir;
 brush.Color:=rgb(20,200,2);
rectangle(100+t2+t9-t11+t18-t20+t22,190-t1*5+t8-t21,300+t2+t9-t11+t18-t20+t22,225-t1*5+t8-t21); //brat miscare orizontala
brush.Color:=rgb(20,100,2);
rectangle(300+t2+t9-t11+t18-t20+t22,180-t1*5+t8-t21,320+t2+t9-t11+t18-t20+t22,235-t1*5+t8-t21); //brat miscare orizontala

 brush.Color:=rgb(200,200,20);

rectangle(130,180-t1*5+t8-t21,200,235-t1*5+t8-t21); //piston
 rectangle(10,360,70,382);
 brush.Color:=rgb(0,100,20);
 rectangle(25,200,50,360);
  brush.Color:=rgb(20,200,2);
 rectangle(17,300+t9+t2,100,320+t9+t2);
  brush.Color:=rgb(0,0,255);
  rectangle(50,290+t9+t2,60,330+t9+t2);
  brush.Color:=rgb(100,100,20);
  rectangle(100,290+t9+t2,110,330+t9+t2);
  brush.Color:=rgb(0,100,20);
 rectangle(25,200,50,360);
 brush.Color:=rgb(0,100,20);






brush.Color:=rgb(200,0,0);
rectangle(320+t2+t9-t11+t18-t20+t22,190-t1*5+t8+t5-t10+t19-t21,350+t2+t9-t11+t18-t20+t22,200-t1*5+t8+t5-t10+t19-t21); {pensa 1 sus}
rectangle(320+t2+t9-t11+t18-t20+t22,215-t1*5+t8-t5+t10-t19-t21,350+t2+t9-t11+t18-t20+t22,225-t1*5+t8-t5+t10-t19-t21); {pensa 2 jos}

 brush.Color:=rgb(0,0,255);

 rectangle(350+t4+t9+t14-t17-t20+t22,55-t3*5+t8-t21,380+t4+t9+t14-t17-t20+t22,60-t3*5+t8-t21);{piesa}
 rectangle(380+t4+t9+t14-t17-t20+t22,43-t3*5+t8-t21,410+t4+t9+t14-t17-t20+t22,70-t3*5+t8-t21);{piesa}

brush.Color:=rgb(250,120,20);

rectangle(120,20,180,350); {cilindru}
brush.Color:=clblue;
rectangle(100,350,200,380); {suport}


 brush.Color:=rgb(20,255,20);
rectangle(300,300,250,380);  {suport}
 brush.Color:=rgb(20,20,255);
rectangle(400,300,300,600);  {suport}

Form1.Image1.Canvas.Brush.Color:=clwhite;
if dir then
begin
t:=t+1;
 end

else
 begin
 t:=t-1;
    end;

 if (t>50000) or (t<0)then  dir:= not dir;
  if directie then q:=q-1
             else q:=q+1;
 if (q=qmax)or(q=0) then  directie :=not directie ;
 if directie_linie then q1:=q1-10
                    else q1:=q1+1;
 if (q1=q1max)or(q1=0)then directie_linie :=not directie_linie;

 end;
 end;



procedure TForm1.Button1Click(Sender: TObject);
begin
form1.close
end;

procedure TForm1.Button2Click(Sender: TObject);
begin
timer1.enabled:=not (timer1.enabled);
end;

procedure TForm1.Button3Click(Sender: TObject);
begin
  timer1.enabled:=not (timer1.enabled);
end;
end;

end.
