unit Unit2;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Menus;

type
  TForm1 = class(TForm)
    Image1: TImage;
    Button1: TButton;
    Timer1: TTimer;
    Button2: TButton;
    PopupMenu1: TPopupMenu;
    procedure Timer1Timer(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;
  t:integer=0;
  t1,t2,t3,t4,t5:integer;
  dir,d:boolean;

implementation

{$R *.DFM}

procedure TForm1.Timer1Timer(Sender: TObject);
begin

with form1.image1.canvas do
begin
  fillrect(clientrect);
pen.width:=1;
//pen.color:=rgb(20,40,255);
//pen.style:=psdash;
brush.Color:=rgb(200,100,20);
//brush.Style:=bsbdiagonal;


rectangle(385,70,420,280);        {suport rafturi}
                                  {380,400 - pozitia pe orizontala}
rectangle(360,120,385,130);       {120,130 - pozitia pe verticala}
rectangle(360,90,385,100);        {rafturile de sus}
rectangle(360,150,385,160);

 brush.Color:=rgb(100,120,120);
rectangle(360,220,385,230);       {rafturi jos}
rectangle(360,190,385,200);
rectangle(360,250,385,260);

rectangle(390,300,405,310);       {rafturile separate}
rectangle(390,315,405,325);

if(t<=5)then t1:=30
else if (t<=60) then begin t1:=90;t2:=t-30;end
else if (t<=65) then begin t1:=90; t2:=30;t5:=t-60;  end
else if (t<=95) then begin t1:=90; t2:=-t+96; t4:=-t+66;t3:=0; end
else if (t<=125) then begin   t1:=185-t; t3:=t1-60;t4:=-30;   end
else if (t<=155) then begin t1:=60; t2:=t-125; t3:=t1-30; t4:=t2-30; end
else if (t<=160)then begin t1:=60; t2:=30; t5:=-t+161;end
else if (t<=190)then begin t1:=60; t2:=-t+190; t3:=t1-30; t4:=0;end
else if (t<=220)then begin t1:=60; t2:=t-190;end
else if (t<=225)then begin t1:=60; t2:=30;t5:=t-220;end
else if (t<=255)then begin t1:=60; t2:=-t+256;t3:=t1-30;t4:=t2-30;end
else if (t<=285)then begin t1:=315-t; t3:=t1-30;t4:=-30;end
else if (t<=315)then begin t1:=30; t2:=t-285;t3:=0;t4:=t2-30;end
else if (t<=320)then begin t1:=30; t2:=30;t5:=-t+321;end
else if (t<=350)then begin t1:=30;t2:=350-t;t3:=t1-30;t4:=0;end

//else if (t<=380)then begin t1:=((380-t)-60);t3:=-60;end

else if (t<=380)then begin t1:=30;t2:=t-350;end
else if (t<=385)then begin t1:=30;t2:=30;t5:=t-380;end
else if (t<=415)then begin t1:=30;t2:=416-t;t3:=t1-30;t4:=t2-30;end
else if (t<=480)then begin t1:=445-t;t3:=t1-30;end
else if (t<=541)then begin t1:=-35;t2:=t-480;t4:=t2-30; end
else if (t<=585)then begin t1:=-35; end


else dir:=not dir;

 brush.Color:=rgb(20,200,2);              {bratul telescopic verde}
rectangle(100+t2,265-t1,320+t2,290-t1);

 brush.Color:=rgb(200,200,20);            {bratul de sustinere - bej}
rectangle(130,240-t1,200,445-t1);

 brush.Color:=rgb(200,0,0);
rectangle(320+t2,265-t1+t5,330+t2,270-t1+t5); {pensa 1 sus}
rectangle(320+t2,285-t1-t5,330+t2,290-t1-t5); {pensa 2 jos}

// brush.Color:=rgb(0,0,255);
//rectangle(350+t4,245-t3,385+t4,250-t3);            {piesa}

 brush.Color:=rgb(0,0,255);
rectangle(350+t4,90-t3,385+t4,85-t3);

 brush.Color:=rgb(250,120,20);
rectangle(120,350,210,400);

 brush.Color:=rgb(220,100,20);
rectangle(100,400,230,460);

Form1.Image1.Canvas.Brush.Color:=clwhite;
if dir then
begin
t:=t+1;
 end

else
 begin
 t:=t-1;
    end;

 if (t>586) or (t<0)then dir:= not dir;

 end;
 end;


procedure TForm1.Button1Click(Sender: TObject);
begin
form1.close
end;

procedure TForm1.Button2Click(Sender: TObject);
begin
timer1.enabled:=not (timer1.enabled);
end;

end.
