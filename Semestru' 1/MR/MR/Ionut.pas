unit Ionut;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls;

type
  TForm1 = class(TForm)
    Timer1: TTimer;
    Image1: TImage;
    procedure Timer1Ttimer (sender:TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

    procedure TForm1.Timer1Ttimer (sender:TObject);
    begin
    with Form1.Image1.Canvas do
    begin
    //rectangle(100,200,300,400);
    //polygon([point(100,200),point(200,300), point(300,200),point(200,400)]);
    //RoundRect(100,100,40,50,100,100);
    rectangle(200,200,400,400);
    brush.Color:=clred;
    rectangle(100,300,300,500);
    brush.Color:=clgreen;
    Polygon([point(200,200),point(400,200), point(300,300),point(100,300)]);
    brush.Color:=clgray;
    Polygon([point(300,300),point(400,200), point(400,400),point(300,500)]);
    moveto(100,100);
    lineto(150,150);
    pen.Width:=5;
    pen.Color:=clblue;
    brush.Color:=clyellow;
    ellipse(400,100,600,200);
    Polyline([point(500,500),point(600,300), point(500,300),point(400,500)]);
    end;
    end;
{$R *.dfm}

end.
