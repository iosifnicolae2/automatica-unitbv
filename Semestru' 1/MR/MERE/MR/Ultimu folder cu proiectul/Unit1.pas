unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls;

type
  TForm1 = class(TForm)
    Image1: TImage;
    Timer1: TTimer;
    Button1: TButton;
    Button2: TButton;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    procedure Timer1Timer(Sender: TObject);
    procedure Button1Click(Sender: TObject);
   procedure Button2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;
  var t:integer;  // variabila de timp
  var x1_cerc,y1_cerc:integer;
  var x1_cerc1,y1_cerc1:integer;  //folosite la desenarea razelor cercurilor care se misca
  t1:integer; //folosita  la prehensor
implementation

{$R *.dfm}

procedure TForm1.Timer1Timer(Sender: TObject);
begin
with Image1.Canvas do
 begin
  brush.color:=clwhite;
  fillrect(clientrect);
  //liniile de sus si jos de la banda rulanta
pen.Width:=2;
pen.Color:=clblue;
moveto(100,300);
lineto(300,300);
pen.Width:=2;
pen.Color:=clblue;
moveto(100,350);
lineto(300,350);
//chestia aia din care iese piesa
pen.color:=clred;
brush.Color:=clyellow;
rectangle(0,200,81,400);
//aia pe care asez piesa
pen.Width:=3;
pen.color:=clred; brush.Color:=clyellow;
rectangle(370,380,440,400);
//cele 2 cercuri de la banda rulanta
pen.Width:=2;           brush.Color:=clwhite;
pen.Color:=clblue;
Ellipse(275,300,325,350);
Ellipse(80,300,130,350);

//cele 2 linii (cabluri) de la macaraua suspendata
pen.Width:=4;
pen.Color:=clred;
moveto(0,40);lineto(650,40);
moveto(0,80);lineto(650,80);

//SE REALIZEAZA MISCAREA PIESEI PE BANDA TRANSPORTOARE
if (t<200) then
begin
// piesa
  pen.Width:=2;
  pen.Color:=clgreen;
  brush.Color:=clgreen;
  rectangle(80+t,260,100+t,300);
//razele celor 2 cercuri care se misca  pe banda transportoare
  pen.Width:=2;
  pen.Color:=clblue;
  brush.Color:=clblue;
  x1_cerc := trunc(300 + 25*cos(t/42));
  y1_cerc := trunc(325 + 25*sin(t/42));
  moveto(300,325);
  lineto(x1_cerc,y1_cerc);
  x1_cerc1 := trunc(105 + 25*cos(t/42));
  y1_cerc1 := trunc(325 + 25*sin(t/42));
  moveto(105,325);
  lineto(x1_cerc1,y1_cerc1);
  //clestele  macaralei
  pen.Color:=clblack;
  pen.Width:=6;
  moveto(270,176);
  lineto(310,176);
  pen.Width:=4;
  moveto(270,176); lineto(274,183);
  pen.Width:=4;
  moveto(310,176); lineto(306,183);
  //macaraua propriu-zisa
pen.Width:=4;
pen.Color:=clblue;
rectangle(250,44,325,77);
//cablu macara
pen.Width:=9;
pen.Color:=clblack;
moveto(289,80);
lineto(289,174);
end;


//PIESA RAMANE MENISCATA IN CAPATUL BANDEI SI MACARAUA COBOARA
if((t>=200) and (t<283)) then
begin
//piesa
  pen.Width:=2;
  pen.Color:=clgreen;
  brush.Color:=clgreen;
  rectangle(279, 260, 299,300);
 //razele cerurilor
  pen.Width:=2;
  pen.Color:=clblue;
  brush.Color:=clblue;
  moveto(105,325);
  lineto(105,300);
  moveto(300,325);
  lineto(300,300);

  //cablu de la macara care coboara
  pen.Width:=9;
  pen.Color:=clblack;
  moveto(289,80);
  lineto(289,174+t-200);
//clestele macaralei
  pen.Width:=6;
  moveto(270,176+t-200);
  lineto(310,176+t-200);
//cele  prehensoare ale macaralei
  pen.Width:=4;
  moveto(270,176+t-200); lineto(274,183+t-200);
  pen.Width:=4;
  moveto(310,176+t-200); lineto(306,183+t-200);

// macaraua propriu-zisa
pen.Width:=4;
pen.Color:=clblue;   brush.color:=clblue;
rectangle(250,44,325,77);

end;


 //MACARAUA NU SE MAI MISCA SI PREHENSORUL APUCA PIESA
   if((t>=283) and (t<293)) then
begin
// piesa
  pen.Width:=2;
  pen.Color:=clgreen;
  brush.Color:=clgreen;
  rectangle(279, 260, 299,300);

//cablu de la macara
  pen.Width:=9;
  pen.Color:=clblack;
  moveto(289,80);
  lineto(289,254);
//clestele  macaralei
  pen.Width:=6;
  moveto(270,256);
  lineto(310,256);
  pen.Width:=4;
  pen.Color:=clblack;
  brush.Color:=clblack;
  moveto(300,325);
  lineto(300,300);
  //realizare apucarii piesei e catre prehensor
  {if (t1<10)then  begin
  pen.Width:=4;     t1:=0;
  moveto(270+t1,256); lineto(280+t1,256);
  pen.Width:=4;
  moveto(310+t1,256); lineto(300+t1,256);
   t1:=t1+1;
   end;          }
   moveto(270+283+t,256); lineto(280+283+t,256);
  pen.Width:=4;
  moveto(310+283+t,256); lineto(300+283+t,256);

   //razele cerurilor
  pen.Width:=2;
  pen.Color:=clblue;
  brush.Color:=clblue;
  moveto(105,325);
  lineto(105,300);
  moveto(300,325);
  lineto(300,300);

//macaraua propriu-zisa
pen.Width:=4;
pen.Color:=clblue;  brush.color:=clblue;
rectangle(250,44,325,77);

end;


//PIESA SI CU MACARAUA INCEP SA URCE
if((t>=293) and (t<371)) then
begin
// piesa
  pen.Width:=2;
  pen.Color:=clgreen;
  brush.Color:=clgreen;
  rectangle(279, 256-t+293, 299,296-t+293);
// cablul macaralei
  pen.Width:=9;
  pen.Color:=clblack;
  moveto(289,80);
  lineto(289,174-t+371);
// clestele macaralei
  pen.Width:=6;
  moveto(270,176-t+371);
  lineto(310,176-t+371);
  pen.Width:=4;
  pen.Color:=clblack;
  brush.Color:=clblack;
  moveto(300,325-t+371);
//prehensor
  pen.Width:=4;
  moveto(270,176-t+371); lineto(280,183-t+371);
  pen.Width:=4;
  moveto(310,176-t+371); lineto(300,183-t+371);
 //razele cerurilor
  pen.Width:=2;
  pen.Color:=clblue;
  brush.Color:=clblue;
  moveto(105,325);
  lineto(105,300);
  moveto(300,325);
  lineto(300,300);
//macaraua propriu-zisa
  pen.Width:=4;     brush.Color:=clblue;
  pen.Color:=clblue;
rectangle(250,44,325,77);
end;

//MACARA INCEPE SA SE MISTE CU PIESA SPRE DREAPTA
if((t>=371) and (t<491)) then
begin
//PIESA
  pen.Width:=2;
  pen.Color:=clgreen;
  brush.Color:=clgreen;
  rectangle(279-371+t, 176, 299-371+t,216);
//cablu macara
pen.Width:=9;
pen.Color:=clblack;
moveto(289+t-371,80);
lineto(289+t-371,174);
  //clestele  macaralei
  pen.Color:=clblack;
  pen.Width:=6;
  moveto(270-371+t,176);
  lineto(310-371+t,176);
  pen.Width:=4;
  moveto(270-371+t,176); lineto(274-371+5+t,183);
  pen.Width:=4;
  moveto(310-371+t,176); lineto(306-371-5+t,183);
////razele cerurilor
  pen.Width:=2;
  pen.Color:=clblue;
  brush.Color:=clblue;
  moveto(105,325);
  lineto(105,300);
  moveto(300,325);
  lineto(300,300);
//macaraua propriu-zisa (se misca in dreapta)
  pen.Width:=4;
  pen.Color:=clblue;
  rectangle(250-371+t,44,325-371+t,77);
end;

//MACARA SI PIESA AJUNSE INCEP SA COBOARE

if((t>=491) and (t<655)) then
begin
//piesa
  pen.Width:=2;
  pen.Color:=clgreen;
  brush.Color:=clgreen;
  rectangle(395, 176+t-491, 415,216+t-491);
// cablul macaralei
  pen.Width:=9;
  pen.Color:=clblack;
  moveto(405,80);
  lineto(405,171+t-491);
//clestele macaralei
  pen.Color:=clblack;
  pen.Width:=6;
  moveto(385,176+t-491);
  lineto(425,176+t-491);
  pen.Width:=4;
  moveto(385,176+t-491); lineto(394,183+t-491);
  pen.Width:=4;
  moveto(425,176+t-491); lineto(416,183+t-491);
//razele cerurilor
  pen.Width:=2;
  pen.Color:=clblue;
  brush.Color:=clblue;
  moveto(105,325);
  lineto(105,300);
  moveto(300,325);
  lineto(300,300);
//macaraua propriu-zisa
  pen.Width:=4;
  pen.Color:=clblue;
  rectangle(367,44,442,77);
end;

  //MACARA LASA PIESA JOS SI INCEPE SA URCE

if((t>=655) and (t<751)) then
begin
//piesa
  pen.Width:=2;
  pen.Color:=clgreen;
  brush.Color:=clgreen;
  rectangle(397, 340, 417,380);
// cablul macaralei
  pen.Width:=9;
  pen.Color:=clblack;
  moveto(405,80);
  lineto(405,338-t+655);
//clestele macaralei
  pen.Color:=clblack;
  pen.Width:=6;
  moveto(385,338-t+655);
  lineto(425,338-t+655);
  pen.Width:=4;
  moveto(385,338-t+655); lineto(389,346-t+655);
  pen.Width:=4;
  moveto(425,338-t+655); lineto(421,346-t+655);
//razele cerurilor
  pen.Width:=2;   pen.Color:=clblue;      
  brush.Color:=clblue;
  moveto(105,325);
  lineto(105,300);
  moveto(300,325);
  lineto(300,300);
//macaraua propriu-zisa
  pen.Width:=4;
  pen.Color:=clblue;
  rectangle(367,44,442,77);
end;
t:=t+1;
//opresc piesa
if (t=751) then
begin
  timer1.enabled:=false;
end;


//podeaua
pen.Width:=1;
pen.Color:=clgray;
brush.color:=clgray;
rectangle(0,400,700,600);

  end

  end;
 // procedura pentru apasarea butonului Pauza
procedure TForm1.Button1Click(Sender: TObject);
begin
if(timer1.enabled) then
  begin
  timer1.Enabled:= false;
  button1.caption:='Resume';
  end
    else
  begin
  timer1.Enabled:= true;
  button1.caption:='Pauza';
  end

    end;
     //procedura pentru apasarea butonului Reset
    procedure TForm1.Button2Click(Sender: TObject);
begin
t:=0;
end;

end.

