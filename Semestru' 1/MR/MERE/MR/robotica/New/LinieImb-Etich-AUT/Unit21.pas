unit Unit21;
interface
uses
   Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls;
type
  TForm1 = class(TForm)
    Image1: TImage;
    Timer1: TTimer;
    Button1: TButton;
    Button2: TButton;
    Button3: TButton;
    procedure Timer1Timer(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;
   t:integer=0;
   A:integer=0;
   B:integer=0;
   C:integer=0;
   E:integer=0;
   J:integer=0;
   U:integer=0;
   R:integer=0;
   V:integer=0;
   X:integer=0;
   Y:integer=0;
   j2:integer=150;
   choix:integer;
   A1,B1,B2,B3,B4,B5,C1,C2,E1,J1,k,R1,R2,R3,R4,R5,V1,V2,V3,V4,V5,V6,V7,V8,V9,V10,V11,V12,V13,X1,Y1,X2,Y2:integer;
  directie:boolean=false;
implementation
{$R *.DFM}
procedure TForm1.Timer1Timer(Sender: TObject);
  begin
  with form1.Image1.canvas     do
    begin
   fillrect(clientrect);

//tapis principal

pen.color:=clred;
pen.width:=2;
roundrect(-20,350,630,375,28,28);
pen.color:=clblack;
pen.width:=1;
ellipse(50,350,77,375);                                                         //rotation  de la roue 1
x:=round(63+13*sin(-C2/90));
y:=round(362+13*cos(-C2/90));
MoveTo(63,362);LineTo(x,y);

x:=round(63+13*-sin(-C2/90));
y:=round(362+13*-cos(-C2/90));
MoveTo(x,y);LineTo(63,362);

ellipse(182,350,209,375);                                                       //rotation  de la roue 2
x:=round(195+13*sin(-C2/90));
y:=round(362+13*cos(-C2/90));
MoveTo(195,362);LineTo(x,y);

x:=round(195+13*-sin(-C2/90));
y:=round(362+13*-cos(-C2/90));
MoveTo(x,y);LineTo(195,362);

ellipse(315,350,342,375);                                                       //rotation  de la roue 3
x:=round(328+13*sin(-C2/90));
y:=round(362+13*cos(-C2/90));
MoveTo(328,362);LineTo(x,y);

x:=round(328+13*-sin(-C2/90));
y:=round(362+13*-cos(-C2/90));
MoveTo(x,y);LineTo(328,362);

ellipse(459,350,486,375);                                                       //rotation  de la roue 4
x:=round(472+13*sin(-C2/90));
y:=round(362+13*cos(-C2/90));
MoveTo(472,362);LineTo(x,y);

x:=round(472+13*-sin(-C2/90));
y:=round(362+13*-cos(-C2/90));
MoveTo(x,y);LineTo(472,362);


ellipse(602,350,629,375);                                                       //rotation  de la roue 5
x:=round(615+13*sin(-C2/90));
y:=round(362+13*cos(-C2/90));
MoveTo(615,362);LineTo(x,y);

x:=round(615+13*-sin(-C2/90));
y:=round(362+13*-cos(-C2/90));
MoveTo(x,y);LineTo(615,362);

pen.Width:=6;
moveto(63,362);lineto(63,395);                                                  // supports des tapis (tiges)
moveto(195,362);lineto(195,395);
moveto(328,362);lineto(328,395);
moveto(472,362);lineto(472,395);
moveto(615,362);lineto(615,395);

pen.Width:=2;
brush.Color:=clgray;                                                            // support de b�ton
rectangle(-10,395,625,450);
brush.Color:=clwhite;

//ensemble


pen.Width:=2;
pen.color:=clblack;
brush.Color:=clgray;
rectangle(-5,-5,625,55);
rectangle(825,-5,845,610);


//rail du bras


pen.Width:=1;
brush.Color:=clgray;
rectangle(480,270,700,287);
rectangle(480,273,700,275);
rectangle(480,282,700,284);
brush.Color:=clwhite;


//ascenseur porteur de bouteille


pen.Width:=1;
pen.color:=clblack;
brush.Color:=clgray;
rectangle(712,0,738,800);
brush.Color:=clwhite;
brush.Color:=clblack;
//
rectangle(635,-109+A1,815,-97+A1);                                              // 7 palier (grand)
rectangle(635,6+A1,815,18+A1);                                                  // 6eme palier (grand)
rectangle(635,121+A1,815,133+A1);                                               // 5eme palier (grand)
rectangle(635,236+A1,815,248+A1);                                               // 4eme palier (grand)
rectangle(635,351+A1,815,363+A1);                                               // 3eme palier (grand)
rectangle(635,466+A1,815,478+A1);                                               // 2eme palier (grand)
rectangle(635,581+A1,815,593+A1);                                               // 1er palier  (grand)
brush.Color:=clwhite;
brush.Color:=clgray;
rectangle(707,-97+A1,743,-90+A1);                                               // 7 palier (petit)
rectangle(707,18+A1,743,25+A1);                                                 // 6eme palier (petit)
rectangle(707,133+A1,743,140+A1);                                               // 5eme palier (petit)
rectangle(707,248+A1,743,255+A1);                                               // 4eme palier (petit)
rectangle(707,363+A1,743,370+A1);                                               // 3eme palier (petit)
rectangle(707,478+A1,743,485+A1);                                               // 2eme palier (petit)
rectangle(707,593+A1,743,600+A1);                                               // 1er palier  (petit)
brush.Color:=clwhite;

//porteur de bouteilles

pen.Width:=4;
moveto(497+B1+B3-B5,282-B2-B4);lineto(534+B1+B3-B5,282-B2-B4);                  //grande tige du bras
moveto(534+B1+B3-B5,319-B2-B4);lineto(534+B1+B3-B5,282-B2-B4);                  //petite tige du bras
pen.Width:=1;
rectangle(480+B1+B3-B5,265,500+B1+B3-B5,292);
pen.Width:=2;
moveto(471+B1+B3-B5,328-B2-B4);lineto(471+B1+B3-B5,343-B2-B4);                  //1er bras
moveto(494-B1+B3-B5,328-B2-B4);lineto(494-B1+B3-B5,343-B2-B4);
moveto(471+B3-B5+B1,336-B2-B4);lineto(494+B3-B5-B1,336-B2-B4);
moveto(482+B3-B5,320-B2-B4);lineto(482+B3-B5,336-B2-B4);
moveto(523+B1+B3-B5,328-B2-B4);lineto(523+B1+B3-B5,343-B2-B4);                  //2eme bras
moveto(546-B1+B3-B5,328-B2-B4);lineto(546-B1+B3-B5,343-B2-B4);
moveto(523+B3-B5+B1,336-B2-B4);lineto(546+B3-B5-B1,336-B2-B4);
moveto(534+B3-B5,320-B2-B4);lineto(534+B3-B5,336-B2-B4);
moveto(575+B1+B3-B5,328-B2-B4);lineto(575+B1+B3-B5,343-B2-B4);                  //3eme bras
moveto(598-B1+B3-B5,328-B2-B4);lineto(598-B1+B3-B5,343-B2-B4);
moveto(575+B3-B5+B1,336-B2-B4);lineto(598+B3-B5-B1,336-B2-B4);
moveto(586+B3-B5,320-B2-B4);lineto(586+B3-B5,336-B2-B4);
moveto(481+B3-B5,320-B2-B4);lineto(586+B3-B5,320-B2-B4);                        // bras principal
pen.Width:=7;



// TOUTES LES BOUTEILLES


pen.Color:=clgreen;
pen.Width:=2;

moveto (-144+C1,315);lineto(-144+C1,320);                                       //bouteille -3
moveto (-139+C1,315);lineto(-139+C1,320);
moveto (-144+C1,320);lineto(-149+C1,325);
moveto (-139+C1,320);lineto(-134+C1,325);
moveto (-149+C1,325);lineto(-149+C1,350);
moveto (-134+C1,325);lineto(-134+C1,350);
moveto (-149+C1,350);lineto(-134+C1,350);

moveto (-92+C1,315);lineto(-92+C1,320);                                         //bouteille -2
moveto (-87+C1,315);lineto(-87+C1,320);
moveto (-92+C1,320);lineto(-97+C1,325);
moveto (-87+C1,320);lineto(-82+C1,325);
moveto (-97+C1,325);lineto(-97+C1,350);
moveto (-82+C1,325);lineto(-82+C1,350);
moveto (-97+C1,350);lineto(-82+C1,350);

moveto (-40+C1,315);lineto(-40+C1,320);                                         //bouteille -1
moveto (-35+C1,315);lineto(-35+C1,320);
moveto (-40+C1,320);lineto(-45+C1,325);
moveto (-35+C1,320);lineto(-30+C1,325);
moveto (-45+C1,325);lineto(-45+C1,350);
moveto (-30+C1,325);lineto(-30+C1,350);
moveto (-45+C1,350);lineto(-30+C1,350);

moveto (12+C1,315);lineto(12+C1,320);                                           //bouteille 0
moveto (17+C1,315);lineto(17+C1,320);
moveto (12+C1,320);lineto(07+C1,325);
moveto (17+C1,320);lineto(22+C1,325);
moveto (07+C1,325);lineto(07+C1,350);
moveto (22+C1,325);lineto(22+C1,350);
moveto (07+C1,350);lineto(22+C1,350);

moveto (64+C1,315);lineto(64+C1,320);                                           //bouteille 1
moveto (69+C1,315);lineto(69+C1,320);
moveto (64+C1,320);lineto(59+C1,325);
moveto (69+C1,320);lineto(74+C1,325);
moveto (59+C1,325);lineto(59+C1,350);
moveto (74+C1,325);lineto(74+C1,350);
moveto (59+C1,350);lineto(74+C1,350);

moveto (116+C1,315);lineto(116+C1,320);                                         //bouteille 2
moveto (121+C1,315);lineto(121+C1,320);
moveto (116+C1,320);lineto(111+C1,325);
moveto (121+C1,320);lineto(126+C1,325);
moveto (111+C1,325);lineto(111+C1,350);
moveto (126+C1,325);lineto(126+C1,350);
moveto (111+C1,350);lineto(126+C1,350);

moveto (168+C1,315);lineto(168+C1,320);                                         //bouteille 3
moveto (173+C1,315);lineto(173+C1,320);
moveto (168+C1,320);lineto(163+C1,325);
moveto (173+C1,320);lineto(178+C1,325);
moveto (163+C1,325);lineto(163+C1,350);
moveto (178+C1,325);lineto(178+C1,350);
moveto (163+C1,350);lineto(178+C1,350);

moveto (220+C1,315);lineto(220+C1,320);                                         //bouteille 4
moveto (225+C1,315);lineto(225+C1,320);
moveto (220+C1,320);lineto(215+C1,325);
moveto (225+C1,320);lineto(230+C1,325);
moveto (215+C1,325);lineto(215+C1,350);
moveto (230+C1,325);lineto(230+C1,350);
moveto (215+C1,350);lineto(230+C1,350);

moveto (272+C1,315);lineto(272+C1,320);                                         //bouteille 5
moveto (277+C1,315);lineto(277+C1,320);
moveto (272+C1,320);lineto(267+C1,325);
moveto (277+C1,320);lineto(282+C1,325);
moveto (267+C1,325);lineto(267+C1,350);
moveto (282+C1,325);lineto(282+C1,350);
moveto (267+C1,350);lineto(282+C1,350);


moveto (324+C1,315);lineto(324+C1,320);                                         //bouteille 6
moveto (329+C1,315);lineto(329+C1,320);
moveto (324+C1,320);lineto(319+C1,325);
moveto (329+C1,320);lineto(334+C1,325);
moveto (319+C1,325);lineto(319+C1,350);
moveto (334+C1,325);lineto(334+C1,350);
moveto (319+C1,350);lineto(334+C1,350);


moveto (376+C1,315);lineto(376+C1,320);                                         //bouteille 7
moveto (381+C1,315);lineto(381+C1,320);
moveto (376+C1,320);lineto(371+C1,325);
moveto (381+C1,320);lineto(386+C1,325);
moveto (371+C1,325);lineto(371+C1,350);
moveto (386+C1,325);lineto(386+C1,350);
moveto (371+C1,350);lineto(386+C1,350);

moveto (428+C1,315);lineto(428+C1,320);                                         //bouteille 8
moveto (433+C1,315);lineto(433+C1,320);
moveto (428+C1,320);lineto(423+C1,325);
moveto (433+C1,320);lineto(438+C1,325);
moveto (423+C1,325);lineto(423+C1,350);
moveto (438+C1,325);lineto(438+C1,350);
moveto (423+C1,350);lineto(438+C1,350);

moveto (480+B3,315-B2+A1);lineto(480+B3,320-B2+A1);                             // bouteilles dans le bras
moveto (485+B3,315-B2+A1);lineto(485+B3,320-B2+A1);
moveto (480+B3,320-B2+A1);lineto(475+B3,325-B2+A1);
moveto (485+B3,320-B2+A1);lineto(490+B3,325-B2+A1);
moveto (475+B3,325-B2+A1);lineto(475+B3,350-B2+A1);
moveto (490+B3,325-B2+A1);lineto(490+B3,350-B2+A1);
moveto (475+B3,350-B2+A1);lineto(490+B3,350-B2+A1);

moveto (532+B3,315-B2+A1);lineto(532+B3,320-B2+A1);
moveto (537+B3,315-B2+A1);lineto(537+B3,320-B2+A1);
moveto (532+B3,320-B2+A1);lineto(527+B3,325-B2+A1);
moveto (537+B3,320-B2+A1);lineto(542+B3,325-B2+A1);
moveto (527+B3,325-B2+A1);lineto(527+B3,350-B2+A1);
moveto (542+B3,325-B2+A1);lineto(542+B3,350-B2+A1);
moveto (527+B3,350-B2+A1);lineto(542+B3,350-B2+A1);

moveto (584+B3,315-B2+A1);lineto(584+B3,320-B2+A1);
moveto (589+B3,315-B2+A1);lineto(589+B3,320-B2+A1);
moveto (584+B3,320-B2+A1);lineto(579+B3,325-B2+A1);
moveto (589+B3,320-B2+A1);lineto(594+B3,325-B2+A1);
moveto (579+B3,325-B2+A1);lineto(579+B3,350-B2+A1);
moveto (594+B3,325-B2+A1);lineto(594+B3,350-B2+A1);
moveto (579+B3,350-B2+A1);lineto(594+B3,350-B2+A1);

                                                                                // trio de bouteilles (2eme palier)
moveto (670,430+A1);lineto(670,435+A1);
moveto (675,430+A1);lineto(675,435+A1);
moveto (670,435+A1);lineto(665,440+A1);
moveto (675,435+A1);lineto(680,440+A1);
moveto (665,440+A1);lineto(665,465+A1);
moveto (680,440+A1);lineto(680,465+A1);
moveto (665,465+A1);lineto(680,465+A1);


moveto (722,430+A1);lineto(722,435+A1);
moveto (727,430+A1);lineto(727,435+A1);
moveto (722,435+A1);lineto(717,440+A1);
moveto (727,435+A1);lineto(732,440+A1);
moveto (717,440+A1);lineto(717,465+A1);
moveto (732,440+A1);lineto(732,465+A1);
moveto (717,465+A1);lineto(732,465+A1);


moveto (774,430+A1);lineto(774,435+A1);
moveto (779,430+A1);lineto(779,435+A1);
moveto (774,435+A1);lineto(769,440+A1);
moveto (779,435+A1);lineto(784,440+A1);
moveto (769,440+A1);lineto(769,465+A1);
moveto (784,440+A1);lineto(784,465+A1);
moveto (769,465+A1);lineto(784,465+A1);

                                                                                // trio de bouteilles (1er palier)
moveto (670,545+A1);lineto(670,550+A1);
moveto (675,545+A1);lineto(675,550+A1);
moveto (670,550+A1);lineto(665,555+A1);
moveto (675,550+A1);lineto(680,555+A1);
moveto (665,555+A1);lineto(665,580+A1);
moveto (680,555+A1);lineto(680,580+A1);
moveto (665,580+A1);lineto(680,580+A1);

moveto (722,545+A1);lineto(722,550+A1);
moveto (727,545+A1);lineto(727,550+A1);
moveto (722,550+A1);lineto(717,555+A1);
moveto (727,550+A1);lineto(732,555+A1);
moveto (717,555+A1);lineto(717,580+A1);
moveto (732,555+A1);lineto(732,580+A1);
moveto (717,580+A1);lineto(732,580+A1);


moveto (774,545+A1);lineto(774,550+A1);
moveto (779,545+A1);lineto(779,550+A1);
moveto (774,550+A1);lineto(769,555+A1);
moveto (779,550+A1);lineto(784,555+A1);
moveto (769,555+A1);lineto(769,580+A1);
moveto (784,555+A1);lineto(784,580+A1);
moveto (769,580+A1);lineto(784,580+A1);


// BOUCHONS


pen.Color:=clred;
moveto(322+C1,315);lineto(330+C1,315);                                          //les bouchons des bouteilles avant bras
moveto(374+C1,315);lineto(382+C1,315);
moveto(426+C1,315);lineto(434+C1,315);
moveto(478+B3,315-B2+A1);lineto(486+B3,315-B2+A1);                              //les bouchons des bouteilles  dans bras
moveto(530+B3,315-B2+A1);lineto(538+B3,315-B2+A1);
moveto(582+B3,315-B2+A1);lineto(590+B3,315-B2+A1);


moveto(668,430+A1);lineto(676,430+A1);                                          // bouchons du 2eme palier
moveto(720,430+A1);lineto(728,430+A1);
moveto(772,430+A1);lineto(780,430+A1);


moveto(668,545+A1);lineto(676,545+A1);                                          // bouchons du 1er palier
moveto(720,545+A1);lineto(728,545+A1);
moveto(772,545+A1);lineto(780,545+A1);


//PALINCKA


pen.Color:=clblue;
pen.width:=2;
brush.Color:=clblue;

rectangle (7+C1,350-R4,22+C1,350);                                              //  palincka de  la 1ere serie bouteilles
rectangle (59+C1,350-R3,74+C1,350);
rectangle (111+C1,350-R2,126+C1,350);


rectangle (163+C1,326,178+C1,350);                                              //  palincka de  la 2eme serie bouteilles
rectangle (215+C1,326,230+C1,350);
rectangle (267+C1,326,282+C1,350);

rectangle (319+C1,326,334+C1,350);                                              //  palincka des bouteilles avant bras
rectangle (371+C1,326,386+C1,350);
rectangle (423+C1,326,438+C1,350);

rectangle (475+B3,326-B2+A1,490+B3,350-B2+A1);                                  //  palincka des bouteilles dans bras
rectangle (527+B3,326-B2+A1,542+B3,350-B2+A1);
rectangle (579+B3,326-B2+A1,594+B3,350-B2+A1);

rectangle (769,441+A1,784,465+A1);                                              //  palincka 2eme palier
rectangle (717,441+A1,732,465+A1);
rectangle (665,441+A1,680,465+A1);

rectangle (769,556+A1,784,580+A1);                                              //  palincka 1er palier
rectangle (717,556+A1,732,580+A1);
rectangle (665,556+A1,680,580+A1);
brush.Color:=clwhite;


//ETIQUETTES


pen.Color:=clgray;
pen.width:=1;
brush.Color:=clyellow;
rectangle (370+C1,329,387+C1,345);                                              // etiquettes des bouteilles dans etiquettage
moveto(370+C1,329);lineto(387+C1,345);
moveto(370+C1,345);lineto(387+C1,329);
rectangle (422+C1,329,439+C1,345);
moveto(422+C1,329);lineto(439+C1,345);
moveto(422+C1,345);lineto(439+C1,329);

rectangle (474+B3,329-B2+A1,491+B3,345-B2+A1);                                  //etiquettes des bouteilles dans bras
moveto(474+B3,329-B2+A1);lineto(491+B3,345-B2+A1);
moveto(474+B3,345-B2+A1);lineto(491+B3,329-B2+A1);
rectangle (526+B3,329-B2+A1,543+B3,345-B2+A1);
moveto(526+B3,329-B2+A1);lineto(543+B3,345-B2+A1);
moveto(526+B3,345-B2+A1);lineto(543+B3,329-B2+A1);
rectangle (578+B3,329-B2+A1,595+B3,345-B2+A1);
moveto(578+B3,329-B2+A1);lineto(595+B3,345-B2+A1);
moveto(578+B3,345-B2+A1);lineto(595+B3,329-B2+A1);

rectangle (664,444+A1,681,460+A1);                                              //etiquettes du 2eme palier
moveto(664,444+A1);lineto(681,460+A1);
moveto(664,460+A1);lineto(681,444+A1);
rectangle (716,444+A1,733,460+A1);
moveto(716,444+A1);lineto(733,460+A1);
moveto(716,460+A1);lineto(733,444+A1);
rectangle (768,444+A1,785,460+A1);
moveto(768,444+A1);lineto(785,460+A1);
moveto(768,460+A1);lineto(785,444+A1);

rectangle (664,559+A1,681,575+A1);                                              //etiquettes du 1er palier
moveto(664,559+A1);lineto(681,575+A1);
moveto(664,575+A1);lineto(681,559+A1);
rectangle (716,559+A1,733,575+A1);
moveto(716,559+A1);lineto(733,575+A1);
moveto(716,575+A1);lineto(733,559+A1);
rectangle (768,559+A1,785,575+A1);
moveto(768,559+A1);lineto(785,575+A1);
moveto(768,575+A1);lineto(785,559+A1);
brush.Color:=clwhite;



//cuve a palincka et remplissage


pen.Color:=clblack;
pen.Width:=2;
rectangle(2,2,100,130);                                                         //cuve
pen.Color:=clblue;
brush.Color:=clblue;
rectangle(4,20+R5,98,128);
brush.Color:=clwhite;
pen.color:=clblack;
rectangle(15,129,35,300);                                                       //rail
pen.Color:=clblue;
pen.width:=5;
rectangle(24,97+R1,26,245+R1);                                                  //tuyau de palincka vertical
pen.Width:=2;
pen.Color:=clred;
moveto(4,74);lineto(98,74);
pen.color:=clblack;
pen.Width:=2;
moveto(36,228+R1);lineto(130,228+R1);                                           //bras horizontal
moveto(36,244+R1);lineto(130,244+R1);
pen.width:=5;
pen.Color:=clblue;
rectangle(37,235+R1,130,237+R1);                                                //tuyau de palincka horizontal
pen.Width:=1;
pen.color:=clred;
brush.Color:=clred;
roundrect(10,224+R1,40,249+R1,15,35);                                           //permet la translation
brush.Color:=clwhite;
pen.Width:=2;
brush.Color:=clred;
rectangle(106,219+R1,132,259+R1);                                               //remplisseur
brush.Color:=clwhite;
moveto(106,259+R1);lineto(114,269+R1);
moveto(130,259+R1);lineto(122,269+R1);
moveto(114,269+R1);lineto(122,269+R1);
pen.Width:=3;
pen.color:=clblue;
moveto(118,286+R1);lineto(118,286+R1+J1);                                       //coul�e de palincka dans bouteille
pen.color:=clred;
moveto(118,269+R1);lineto(118,287+R1);                                          //le tuyau


//capsule


pen.Width:=2;
pen.color:=clred;
moveto(270+V4,277+V1);lineto(278+V4,277+V1);
moveto(270+V8,272+V1+V3+V5+V6);lineto(278+V8,272+V1+V3+V5+V6);
moveto(270+V12,272+V1+V3+V6+V7+V9+V10);lineto(278+V12,272+V1+V3+V6+V7+V9+V10);
moveto(270,272+V1+V3+V6+V7+V10+V11+V13);lineto(278,272+V1+V3+V6+V7+V10+V11+V13);

//bras encapsuleur

pen.color:=clblack;
pen.Width:=13;
x2:=round(217+60*sin(J2/80));
y2:=round(265+60*cos(J2/80));
moveto(217,265);lineto(X2,Y2);
pen.Width:=20;
moveto(X2,Y2);lineto(X2,Y2+30);







//Support du bras

pen.color:=clblack;
pen.Width:=2;
brush.Color:=clgray;
rectangle (190,54,217,265);                                                     //support principal
brush.Color:=clgray;
rectangle (160,54,247,175);                                                     //support secondaire
brush.Color:=clwhite;
pen.color:=clred;
ellipse(186,252,221,287);


// Machine a etiquetage

pen.Width:=1;
pen.color:=clgray;
brush.Color:=clyellow;
rectangle (370+E1,329,387+E1,345);                                              //�tiquette de la 7eme bouteille
moveto(370+E1,329);lineto(387+E1,345);
moveto(370+E1,345);lineto(387+E1,329);
brush.Color:=clwhite;
pen.color:=clblack;
pen.Width:=2;
brush.Color:=clred;
rectangle (350,295,460,348);
brush.Color:=clwhite;

If choix=1 then begin                                                           // bouton MARCHE
If choix=2 then t:=t;                                                           // bouton PAUSE


//temporisation du tapis de bouteilles


If (t>0)   and (t<=2)   then B1:=B1+1;                                          //pince se serre
If (t>2)   and (t<=12)  then B2:=B2+1;                                          //pince se soul�ve
If (t>12)  and (t<=202) then B3:=B3+1;                                          //pince part � droite
If (t>202) and (t<=212) then B2:=B2-1;                                          //pince descend
If (t>212) and (t<=214) then B1:=B1-1;                                          //pince se dessererre
If (t>254) and (t<=264) then B4:=B4+1;                                          //pince remonte
If (t>264) and (t<=454) then B5:=B5+1;                                          //pince part � gauche
If (t>454) and (t<=464) then B4:=B4-1;                                          //pince descend



If (t>414) and (t<=529) then A1:=A1+1;                                          //descente de l'ascenseur


If (t>229) and (t<=261) then R1:=R1+1;                                          //descente du tuyau � palincka
If (t>261) and (t<=291) then J1:=J1+1;                                          //coul�e de palincka
If (t>291) and (t<=315) then R2:=R2+1;                                          //remplissage de la 3eme bouteille
If (t>316) and (t<=346) then J1:=J1-1;                                          //stop de la coul�e de palincka
If (t>229) and (t<=267) then V1:=V1+1;                                          //descente du bouchon a venir
If (t>229) and (t<=276) then J2:=J2-1;                                          //rotation du bras
If (t>290) and (t<=328) then V3:=V3-1;                                          //remont�e des bouchons a venir
If (t>290) and (t<=337) then J2:=J2+1;                                          //rotation du bras
If (t>345) and (t<=350) then V5:=V5+1;                                          //pr�paration de la capsule
If (t>300) and (t<=306) then R5:=R5+1;                                          //le reservoir se vide
If (t>400) and (t<=432) then R1:=R1-1;                                          //mont�e du tuyau
If (t>529) and (t<=581) then begin C1:=C1+1;C2:=C2+1;V4:=V4+1;end;              //avance de une bouteille et de la capsule


If (t>581) and (t<=613) then R1:=R1+1;                                          //descente du tuyau
If (t>613) and (t<=643) then J1:=J1+1;                                          //coul�e de palincka
If (t>643) and (t<=667) then R3:=R3+1;                                          //remplissage de la 2eme bouteille
If (t>668) and (t<=698) then J1:=J1-1;                                          //stop de la coul�e de palincka
If (t>581) and (t<=619) then V6:=V6+1;                                          //descente du bouchon a venir
If (t>581) and (t<=628) then J2:=J2-1;                                          //rotation du bras
If (t>642) and (t<=680) then V7:=V7-1;                                          //remont�e des bouchons a venir
If (t>642) and (t<=689) then J2:=J2+1;                                          //rotation du bras
If (t>697) and (t<=702) then V9:=V9+1;                                          //pr�paration de la capsule
If (t>652) and (t<=658) then R5:=R5+1;                                          //le reservoir se vide
If (t>752) and (t<=784) then R1:=R1-1;                                          //mont�e du tuyau
If (t>881) and (t<=933) then begin C1:=C1+1;C2:=C2+1;V4:=V4+1;V8:=V8+1;end;     //avance de une bouteille


If (t>933) and (t<=965) then R1:=R1+1;                                          //descente du tuyau
If (t>965) and (t<=995) then J1:=J1+1;                                          //coul�e de palincka
If (t>995) and (t<=1019)then R4:=R4+1;                                          //remplissage de la 1ere bouteille
If (t>1020)and (t<=1050)then J1:=J1-1;                                          //stop de la coul�e de palincka
If (t>933) and (t<=971) then V10:=V10+1;                                        //descente du bouchon a venir
If (t>933) and (t<=980) then J2:=J2-1;                                          //rotation du bras
If (t>994) and (t<=1032)then V11:=V11-1;                                        //remont�e du bras
If (t>994) and (t<=1041) then J2:=J2+1;                                         //rotation du bras
If (t>1057)and (t<=1062)then V13:=V13+1;                                        //pr�paration de la capsule
If (t>1004)and (t<=1010)then R5:=R5+1;                                          //le reservoir se vide
If (t>1104)and (t<=1136)then R1:=R1-1;                                          //mont�e du tuyau
If (t>1233)and (t<=1285)then begin C1:=C1+1;C2:=C2+1;V4:=V4+1;V8:=V8+1;V12:=V12+1;end;//avance de une bouteille

If (t>881)  and (t<=933)  then E1:=E1+1;                                        //1ere avance de l'�tiquette de la 7eme bouteille
If (t>1233) and (t<=1285) then E1:=E1+1;                                        //2eme avance de l'�tiquette de la 7eme bouteille

If (t>1325) then begin B1:=0;B2:=0;B3:=0;B4:=0;B5:=0;A1:=0;C1:=0;J2:=150;R1:=0;R2:=0;R3:=0;R4:=0;E1:=0;V1:=0;V2:=0;V3:=0;V4:=0;V5:=0;V6:=0;V7:=0;V8:=0;V9:=0;V10:=0;V11:=0;V12:=0;V13:=0;end;

If (t>1285) and (t<=1339)and (k=2) then begin R5:=R5-1;end;                     //Rempli la la cuve

If directie
then t:=t+1 else t:=0;

If (t=0) or (t=1357)                                                            // r�initialise la temporisation
then directie:= not directie;
If (t=1357) and (k=3) then k:=0;
If (t=1356) then k:=k+1;

end;
end;
end;
  procedure TForm1.FormCreate(Sender: TObject);
begin
         form1.Height:=700;
         form1.Width:=1000;
end;
procedure TForm1.Button1Click(Sender: TObject);
begin
choix:=1;
end;

procedure TForm1.Button2Click(Sender: TObject);
begin
choix:=2;
end;

procedure TForm1.Button3Click(Sender: TObject);
begin
form1.close

end;

end.
