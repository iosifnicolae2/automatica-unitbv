unit Unit2;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls, Buttons;

type
  TForm1 = class(TForm)
    Image1: TImage;
    Timer1: TTimer;
    ScrollBar1: TScrollBar;
    Button1: TButton;
    BitBtn1: TBitBtn;
    procedure button1click(sender:tobject);
    procedure BitBtn1click(sender:tobject);
    procedure Timer1Timer(Sender:Tobject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
   Form1: TForm1;
  t,x0,y0,x1,y1,x2,y2,x3,y3:integer;
  a:integer=0;
  b:integer=0;
  c:integer=0;
  faza:integer=0;
  d:integer=0;
  directie:boolean=true;
  const
  tmax:integer=300;
  raza:integer=250;



implementation
    procedure TForm1.Timer1Timer(Sender:Tobject);
    begin
    with Form1.Image1.Canvas do
    begin
    fillrect(clientrect);
    brush.Color:=clWhite;
   {Completarea cilindrului=Partea invizibila a cilindrului}
   brush.color:=clBlue;
   ellipse(115,240,185,310);
   brush.color:=clGray;
   polygon([point(105,270),point(130,245),point(150,240),point(125,265)]);
   polygon([point(150,240),point(125,265),point(150,275),point(175,250)]);
   polygon([point(160,300),point(185,275),point(175,250),point(150,275)]);
   polygon([point(160,300),point(185,275),point(174,299),point(149,324)]);

    //moveto(105,270);
   //lineto(130,245);
    //moveto(125,265);
   //lineto(150,240);
   //moveto(160,300);
   //lineto(185,275);
   //moveto(150,275);
   //lineto(175,250);
   // moveto(149,324);
   //lineto(174,299);

  //Bratul de sus
  brush.Color:=clRed;
  polygon([point(100,100),point(100,275),point(150,275),point(150,100)]) ;
  polygon([point(150,100),point(175,75),point(175,250),point(150,275)]);
   polygon([point(100,100),point(150,100),point(175,75),point(125,75)]);
    //Bratul de jos
    brush.Color:=clGreen;

   {
    if (t>0) and (t<60)
  then
  begin
         //polygon([point(150,325),point(400,375),point(408,325),point(150,275)]);
        polygon([point(150,325),point(round(150+raza*cos(t/20)),round(325+raza*sin(t/20))),point(round(150+raza*cos(t/20)),round(275+raza*sin(t/20))),point(150,275)]);

  polygon([point(175,250),point(425,300),point(408,325),point(150,275)]);
   polygon([point(400,375),point(408,325),point(425,300),point(417,350)]);



  end;}           brush.Color:=clWhite;
            scrollbar1.position:=t;

     brush.Color:=clBlue;
     {Fata vizibila a cilindrului}
  ellipse(90,265,160,335);




  {
  x0:=200;y0:=200;
  x1:=round(x0+raza*cos(t/20));
  y1:=round(y0+raza*sin(t/20));
  moveto (x0,y0);
  lineto (x1,y1);  }

               //linie transport
     Rectangle(0,350,1500,600); //suport podea
     brush.Color:=clwhite;

     //banda
     pen.Color := clblack;
     pen.Width:=1;
     RoundRect(-50,400,700,450,50,50);
     RoundRect(-50,340,760,390,50,50);

     brush.color:=clblack;
     polygon([point(-50,400),point(-50,340),point(735,340),point(675,400)]) ;
        brush.Color:=clwhite;
      // motorul  1
     pen.color:=clblack;
    // brush.color:=clgray;
     pen.Width:=1;
     Ellipse(100,400,50,450);
     pen.Width:=4;
     MoveTo(75,425);
     Lineto(75+round(23*cos(t/10)),425+round(23*sin(t/10)));
     MoveTo(75,425);
     Lineto(75-round(23*cos(t/10)),425-round(23*sin(t/10)));
      pen.Width:=0;
      brush.color:=clwhite;
     //completarea cilindrului
     brush.color:=clblack;
     polygon([point(735,340),point(675,400),point(686,404),point(746,344)]);
     polygon([point(686,404),point(746,344),point(756,352),point(696,412)]);
     polygon([point(696,412),point(756,352),point(760,365),point(700,425)]);
     polygon([point(760,365),point(700,425),point(696,437),point(756,376)]);
     brush.color:=clwhite;

     // motorul  2
     pen.Width:=1;
     Ellipse(400,400,350,450);
     pen.Width:=4;
     MoveTo(375,425);
     Lineto(375+round(23*cos(t/10)),425+round(23*sin(t/10)));
     MoveTo(375,425);
     Lineto(375-round(23*cos(t/10)),425-round(23*sin(t/10)));
      // completarea cilindrului
     brush.color:=clblack;
     pen.color:=clblack;
     polygon([point(435,340),point(375,400),point(386,404),point(446,344)]);
     polygon([point(386,404),point(446,344),point(456,352),point(396,412)]);
     polygon([point(396,412),point(456,352),point(460,365),point(400,425)]);
     polygon([point(460,365),point(400,425),point(396,437),point(456,376)]);
     brush.color:=clwhite;


     // motorul3
     pen.Width:=1;
     Ellipse(700,400,650,450);
     pen.Width:=4;
     MoveTo(675,425);
     Lineto(675+round(23*cos(t/10)),425+round(23*sin(t/10)));
     MoveTo(675,425);
     Lineto(675-round(23*cos(t/10)),425-round(23*sin(t/10)));

      // completarea cilindrului
     brush.color:=clblack;
     pen.color:=clblack;
     polygon([point(135,340),point(75,400),point(86,404),point(146,344)]);
     polygon([point(86,404),point(146,344),point(156,352),point(96,412)]);
     polygon([point(96,412),point(156,352),point(160,365),point(100,425)]);
     polygon([point(160,365),point(100,425),point(96,437),point(156,376)]);
     brush.color:=clwhite;
    brush.color:=clblack;
     //piciorul 1
     MoveTo(75,425);
     pen.Width:=10;
     LineTo(75,500);

     //piciorul 2
     MoveTo(375,425);
     pen.Width:=10;
     LineTo(375,500);

     //piciorul 3
     MoveTo(675,425);
     pen.Width:=10;
     LineTo(675,500);

     //piciorul 4

     moveto(735,365);
     pen.width:=10;
     lineto(735,440);

    brush.color:=clpurple;
      pen.Width:=0;
     polygon([point(750+a,500),point(850+a,500),point(850+a,400),point(750+a,400)]) ;
     brush.color:=clgray;
     polygon([point(750+b,400),point(850+b,400),point(900+b,370),point(800+b,370)]) ;
     moveto(800+b,370);
     lineto(800+b,400);
     brush.color:=clpurple;
     polygon([point(900+c,370),point(850+c,400),point(850+c,500),point(900+c,465)])  ;
     pen.Width:=0;
     brush.Color:=clwhite;
    inc(t);
  { if directie then t:= t+1
    else t:=t-1;
    if (t=0) or (t=tmax) then directie:=not directie;
   }
case d of
0:
begin
if (t<90) then
begin
inc(t);
brush.color:=clwhite;
polygon([point(50+t,380),point(50+t,360),point(100+t,360),point(100+t,380)]);
end
else
begin
d:=0; t:=0;
end;
end;
end;


   case faza of
   0:
   begin
   inc(a);
   if (a=60) then faza :=1;
   end;
   1:
   begin
   inc(b);
   if (b=60) then faza :=2;
   end;
   2:
   begin
   inc(c);
   if (c=60) then faza :=3;
   end;
   3:
   begin
   dec(c);
   if (c=0) then faza :=4;
   end;
   4:
   begin
   dec(b);
   if (b=0) then faza :=5;
   end;
   5:
   begin
   dec(a);
   if (a=0) then faza :=6;
   end;
   6:
   begin
   a:=a;
   end;
   end;
    end;
    end;
{$R *.dfm}


procedure tform1.button1click(sender:tobject);
begin
timer1.enabled:=not(timer1.enabled);
end;

 procedure tform1.BitBtn1click(sender:tobject);
begin
timer1.enabled:=not(timer1.enabled);
end;


end.


