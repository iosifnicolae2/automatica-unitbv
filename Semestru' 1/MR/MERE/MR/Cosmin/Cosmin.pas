unit Cosmin;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls;

type
  TForm1 = class(TForm)
    Image1: TImage;
    Timer1: TTimer;
    procedure Timer1Timer(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.Timer1Timer(Sender: TObject);
begin
      with form1.Image1.Canvas do
      begin
      fillrect(clientrect);
      pen.Width:=1;
       // Pamant
      brush.color:=clgray;
      rectangle(1,500,500,600);
      brush.color:=clwhite;
      // Mare
      brush.color:=clblue;
      rectangle(500,515,1000,600);
      brush.color:=clwhite;
      // Vapor
      brush.color:=rgb(144,22,1);
      polygon([point(500,470),point(545,470),point(570,480),point(725,480),point(750,470),point(800,470),point(775,505),point(775,515),point(540,515),point(540,505),point(500,470)]);
      brush.color:=clwhite;
      brush.color:=rgb(213,238,225);
      rectangle(765,470,785,440);
      rectangle(760,435,790,440);
      ellipse (772,429,778,435);
      brush.color:=clwhite;
      // Macara
      rectangle(200,395,700,405);
      rectangle(470,500,480,380);
      rectangle(420,500,430,380);
      rectangle(405,380,495,390);
      rectangle(445,380,455,335);
      pen.width:=2;
      moveto (445,340);
      lineto (205,395);
      moveto (455,340);
      lineto (695,395);

      end;
end;

end.
