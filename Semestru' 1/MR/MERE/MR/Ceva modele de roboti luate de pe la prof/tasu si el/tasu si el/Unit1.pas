unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls, Buttons;

type
  TForm1 = class(TForm)
    Image1: TImage;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    Timer1: TTimer;
    procedure BitBtn1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;
  posx,posy:integer;

implementation

{$R *.dfm}

procedure TForm1.BitBtn1Click(Sender: TObject);
begin
   timer1.Enabled:=not timer1.Enabled;
   if bitbtn1.Caption='Start' then bitbtn1.Caption:='Stop'
    else bitbtn1.Caption:='Start';
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
   timer1.Enabled:=false;
   posx:=20;
   posy:=20;
   randomize();
end;

procedure TForm1.Timer1Timer(Sender: TObject);
begin
  with image1.Canvas do
  begin
 // brush.Color:=rgb(255,255,255);
 // fillrect(clientrect);
  brush.Color:=rgb(random(255),random(255),random(255));
  rectangle(posx,posy,posx+20,posy+20);
  posx:=posx+20;
  if posx+20>image1.Width-20 then
  begin
  posy:=posy+20;
  posx:=20;
  end;
  end;
end;

procedure TForm1.BitBtn2Click(Sender: TObject);
begin
  image1.Canvas.Brush.Color:=clwhite;
  image1.Canvas.FillRect(image1.ClientRect);
  posx:=20;posy:=20;
end;

end.



