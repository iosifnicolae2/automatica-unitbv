unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Menus;

type
  TForm1 = class(TForm)
    Image1: TImage;
    Timer1: TTimer;
    MainMenu1: TMainMenu;
    File1: TMenuItem;
    Stop1: TMenuItem;
    Stop2: TMenuItem;
    Exit1: TMenuItem;
    Image2: TImage;
    procedure Timer1Timer(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Stop1Click(Sender: TObject);
    procedure Stop2Click(Sender: TObject);
    procedure Exit1Click(Sender: TObject);
    procedure Image1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;
  t:integer=0;
  t1,t2,t3,t4,t5,t6,t7,t8,t9,t10,t11,t12,t13,t14,t15,t16,t17,t18,t19,t20,t21,t22,t23,t24,t30,t31,t32,t25,t26,t27,t28,t29,tgros,x1,y1,x2,y2:integer;
  dir,d:boolean;
   x0:integer=390;
   y0:integer=10;
   q:real=10;
   q1:real=1;
   directie:boolean=true;
   directie_linie:boolean=true;
   directie1:boolean=true;
   directie2:boolean=true;
   inaltime:integer=2;
   inaltime1:integer=2;
const
  r1=20;
  r2=20;
  r3=30;
  r4=32;
  qmax=380;
  q1max=400;

implementation

{$R *.DFM}

procedure TForm1.Timer1Timer(Sender: TObject);
var
x1,y1:integer;
begin


  x1:=round(x0-r1*cos(t2/15+t23));
  y1:=round(y0-r1*sin(t2/15+t23));
  x2:=round(x1-r2*cos(t3/70+t23));
  y2:=round(y1-r2*sin(t3/70+t23));

with form1.image1.canvas do
begin


  fillrect(clientrect); {functie care ne curata imaginea dupa fiecare poza}
   rectangle(380,0,400,20);
   MoveTo(x0,y0);
        pen.Color:=clred;

   pen.Width:=15;
    LineTo(x1,y1);
  pen.color:=clblue;
     LineTo(x2,y2);
  //pen.color:=clblue;
   //pen.Width:=10;
  pen.width:=1;


//pen.color:=rgb(20,40,255);
//pen.style:=psdash;        //incadreaza cu albastru punctat functiile
//brush.Color:=rgb(200,100,20); {coloarea raftului de depozitare}
//brush.Style:=bsbdiagonal;   {imaginea apare hasurata oblic cu negru}

brush.Color:=rgb(50,100,20); {coloarea raftului de depozitare}
rectangle(380,100+t12-t13,400,200+t12-t13+tgros*2);   //brat1

 brush.Color:=rgb(150,100,20); {coloarea raftului de depozitare}
 rectangle(420,100+t15-t16,440,200+t15-t16+tgros*2);   //brat2
        //------------------------------------------------------------------------------------
 brush.Color:=rgb(90,100,60); {coloarea raftului de depozitare}
rectangle(460,100+t30-t31,480,200+t30-t31+tgros*2);   //brat1

 brush.Color:=rgb(200,100,20); {coloarea raftului de depozitare}
 rectangle(380,260,520,380); //nou

 rectangle(490,100,520,380);   // presa vertical stanga{suport raft}
brush.Color:=rgb(100,100,20);
rectangle(380+t14+t17-2*t32,255,420+t14+t17-2*t32,260);//piesa mobila presa

rectangle(375,100,405,150);       //cilindru presa 1
rectangle(415,100,445,150);       //cilindru presa 2
rectangle(455,100,485,150);       //cilindru presa 3
rectangle(375,70,520,100);

 rectangle(0,380,1100,390);  //podea
 brush.Color:=rgb(0,100,20);
 //rectangle(650,330,750,380);   //suport brat robot
  brush.Color:=rgb(50,50,20);

  brush.Color:=rgb(20,200,2); //suport pt piese

    ellipse (550,100,590,140) ;

 if(t<=30)then begin
         t1:=t;
         tgros:=0;
         end;

     if ((t<=60)and(t>30)) then
             begin
        t1:=30;
        t2:=t-30
        end;
      if ((t<=65)and(t>60)) then
             begin
        t1:=30;
        t2:=30;
        t5:=t-60
        end;

     if ((t<=110)and(t>65)) then
             begin
        t1:=30;
        t2:=-t+95;
        t4:=-t+65
        end ;

     if ((t<=285)and(t>100)) then
             begin
        t8:=t-100
        end      ;

     if ((t<=325)and(t>285)) then
             begin
        t9:=t-285
        end ;

      if ((t<=330)and(t>325)) then
             begin
        t10:=t-325
        end       ;

      if ((t<=360)and(t>330)) then
             begin
        t11:=t-330
        end ;

      // -----------------------------------------------------------------------

      if ((t<=390)and(t>360)) then        //miscare ptrsa 1
             begin
        t12:=t-360   ;
        end  ;

      if ((t<=420)and(t>390)) then          //ridicare presa1
             begin
        t13:=t-390;
        tgros:=2;
        end;

       if ((t<=460)and(t>420)) then      //miscare dreapta piesa la presa
             begin
        t14:=t-420
        end;

       // -----------------------------------------------------------------------


        if ((t<=490)and(t>460)) then      //miscare ptrsa 2
             begin
        t15:=t-460
        end ;

        if ((t<=520)and(t>490)) then       //ridicare presa2
             begin
        t16:=t-490;
        tgros:=3;
        end ;

        if ((t<=560)and(t>520)) then      //miscare dreapta piesa la presa
             begin
        t17:=t-520
        end;

       // -----------------------------------------------------------------------


        if ((t<=590)and(t>560)) then      //miscare ptrsa 3
             begin
        t30:=t-560
        end ;

        if ((t<=620)and(t>590)) then       //ridicare presa3
             begin
        t31:=t-590   ;
        tgros:=4;
        end ;

       if ((t<=660)and(t>620)) then      //miscare stanga piesa la presa
             begin
        t32:=t-620
        end;

      // -----------------------------------------------------------------------


       if ((t<=690)and(t>660)) then      //miscare dreapta robot cartezian
             begin
        t18:=t-660
        end  ;

       if ((t<=695)and(t>690)) then      //prindere obiect
             begin
        t19:=t-690
        end ;

       if ((t<=735)and(t>695)) then      //miscare dreapta robot cartezian
             begin
        t20:=t-695
        end   ;

        if ((t<=920)and(t>735)) then      //miscare verticala
             begin
        t21:=t-735
        end ;

       if ((t<=960)and(t>920)) then      //miscare dreapta robot cartezian
             begin
        t22:=t-920
        end;


       if ((t<=1400)and(t>960)) then      //miscare dreapta robot cartezian
             begin
        t23:=t-960
        end



else dir:= dir;
      brush.Color:=rgb(20,200,2);
rectangle(100+t2+t9-t11+t18-t20+t22,tgros+190-t1*5+t8-t21,300+t2+t9-t11+t18-t20+t22,tgros+225-t1*5+t8-t21); //brat miscare orizontala
brush.Color:=rgb(20,100,2);
rectangle(300+t2+t9-t11+t18-t20+t22,tgros+180-t1*5+t8-t21,320+t2+t9-t11+t18-t20+t22,tgros+235-t1*5+t8-t21); //brat miscare orizontala

 brush.Color:=rgb(200,200,20);

rectangle(130,tgros+180-t1*5+t8-t21,200,tgros+235-t1*5+t8-t21); //piston


brush.Color:=rgb(200,0,0);
rectangle(320+t2+t9-t11+t18-t20+t22,tgros+190-t1*5+t8+t5-t10+t19-t21,350+t2+t9-t11+t18-t20+t22,tgros+200-t1*5+t8+t5-t10+t19-t21); {pensa 1 sus}
rectangle(320+t2+t9-t11+t18-t20+t22,tgros+215-t1*5+t8-t5+t10-t19-t21,350+t2+t9-t11+t18-t20+t22,tgros+225-t1*5+t8-t5+t10-t19-t21); {pensa 2 jos}

 brush.Color:=rgb(0,0,255);

 // piesa

 rectangle(350+t4+t9+t14+t17-2*t32-t20+t22,tgros+55-t3*5+t8-t21,390+t4+t9+t14+t17-2*t32-t20+t22,tgros+60-t3*5+t8-t21);{piesa}
 rectangle(390-tgros+t4+t9+t14+t17-2*t32-t20+t22,2*tgros+43-t3*5+t8-t21,400+tgros+t4+t9+t14+t17-2*t32-t20+t22,70-t3*5+t8-t21);{piesa}

brush.Color:=rgb(250,120,20);

rectangle(120,20,180,350); {cilindru}
brush.Color:=rgb(220,100,20);
rectangle(100,350,200,380); {suport}

brush.Color:=rgb(0,255,255);
rectangle(10,10,20,20);
image1.Canvas.MoveTo(30,30);


Form1.Image1.Canvas.Brush.Color:=clwhite;
if dir then
begin
t:=t+1;
 end

else
 begin
 t:=t-1;
    end;

 if (t>50000) or (t<0)then  dir:= not dir;
  if directie then q:=q-1
             else q:=q+1;
 if (q=qmax)or(q=0) then  directie :=not directie ;
 if directie_linie then q1:=q1-10
                    else q1:=q1+1;
 if (q1=q1max)or(q1=0)then directie_linie :=not directie_linie;

 end;
 with image2.Canvas do

 begin
 brush.Color:=clwhite;
 fillrect(clientrect);
 brush.color:=clred;
 rectangle(150,image2.Height-10,190,image2.Height);
   if inaltime > 120 then directie1:=false;
 if inaltime < 10 then directie1:=true;
 if directie1 then inaltime:=inaltime+1 else inaltime:=inaltime-1;
 brush.color:=clblue;
 rectangle(180, image2.height-10-inaltime-10,260,image2.height-10-inaltime);
 brush.color:=cllime;
 rectangle(180,image2.height-15-inaltime-20,195,image2.height-0-inaltime);
 brush.color:=clgreen;
 rectangle(160,image2.height-260,180,image2.height-10);
 brush.color:=cllime;
 rectangle(260,image2.height-15-inaltime-20,245,image2.height-0-inaltime);
 brush.color:=clblue;
 rectangle(280,image2.height-3-inaltime-3,260,image2.height-0-inaltime);
 brush.color:=clblue;
 rectangle(280,image2.height-3-inaltime-32,260,image2.height-29-inaltime);
   if inaltime1 < 10 then directie2:=true;
   if inaltime1 > 30 then directie2:=false;
   if directie2 then inaltime1:=inaltime1+1 else inaltime1:=inaltime1-0;
 brush.color:=clgreen;
 rectangle(280,100,300,200);
 rectangle(320,100,340,200);
 rectangle(360,100,380,200);
 brush.color:=clblue;
 rectangle(395,80,420,380);
 rectangle(275,100,305,150);       //cilindru presa 1
 rectangle(315,100,345,150);       //cilindru presa 2
 rectangle(355,100,385,150);       //cilindru presa 3
 rectangle(275,70,420,100);
 rectangle(275,260,395,380);
 brush.color:=clred;
 rectangle(275+t14+t17-2*t32,255,310+t14+t17-2*t32,260);//piesa mobila presa
     procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Stop1Click(Sender: TObject);
    procedure Stop2Click(Sender: TObject);
    procedure Exit1Click(Sender: TObject);

 if(t<=30)then begin
         t1:=t;
         tgros:=0;
         end;

     if ((t<=60)and(t>30)) then
             begin
        t1:=30;
        t2:=t-30
        end;
      if ((t<=65)and(t>60)) then
             begin
        t1:=30;
        t2:=30;
        t5:=t-60
        end;

     if ((t<=110)and(t>65)) then
             begin
        t1:=30;
        t2:=-t+95;
        t4:=-t+65
        end ;

     if ((t<=285)and(t>100)) then
             begin
        t8:=t-100
        end      ;

     if ((t<=325)and(t>285)) then
             begin
        t9:=t-285
        end ;

      if ((t<=330)and(t>325)) then
             begin
        t10:=t-325
        end       ;

      if ((t<=360)and(t>330)) then
             begin
        t11:=t-330
        end ;

      // -----------------------------------------------------------------------

      if ((t<=390)and(t>360)) then        //miscare ptrsa 1
             begin
        t12:=t-360   ;
        end  ;

      if ((t<=420)and(t>390)) then          //ridicare presa1
             begin
        t13:=t-390;
        tgros:=2;
        end;

       if ((t<=460)and(t>420)) then      //miscare dreapta piesa la presa
             begin
        t14:=t-420
        end;

       // -----------------------------------------------------------------------


        if ((t<=490)and(t>460)) then      //miscare ptrsa 2
             begin
        t15:=t-460
        end ;

        if ((t<=520)and(t>490)) then       //ridicare presa2
             begin
        t16:=t-490;
        tgros:=3;
        end ;

        if ((t<=560)and(t>520)) then      //miscare dreapta piesa la presa
             begin
        t17:=t-520
        end;

       // -----------------------------------------------------------------------


        if ((t<=590)and(t>560)) then      //miscare ptrsa 3
             begin
        t30:=t-560
        end ;

        if ((t<=620)and(t>590)) then       //ridicare presa3
             begin
        t31:=t-590   ;
        tgros:=4;
        end ;

       if ((t<=660)and(t>620)) then      //miscare stanga piesa la presa
             begin
        t32:=t-620
        end;

      // -----------------------------------------------------------------------


       if ((t<=690)and(t>660)) then      //miscare dreapta robot cartezian
             begin
        t18:=t-660
        end  ;

       if ((t<=695)and(t>690)) then      //prindere obiect
             begin
        t19:=t-690
        end ;

       if ((t<=735)and(t>695)) then      //miscare dreapta robot cartezian
             begin
        t20:=t-695
        end   ;

        if ((t<=920)and(t>735)) then      //miscare verticala
             begin
        t21:=t-735
        end ;

       if ((t<=960)and(t>920)) then      //miscare dreapta robot cartezian
             begin
        t22:=t-920
        end;


       if ((t<=1400)and(t>960)) then      //miscare dreapta robot cartezian
             begin
        t23:=t-960
        end



else dir:= dir;
      brush.Color:=rgb(20,200,2);
rectangle(100+t2+t9-t11+t18-t20+t22,tgros+190-t1*5+t8-t21,300+t2+t9-t11+t18-t20+t22,tgros+225-t1*5+t8-t21); //brat miscare orizontala
brush.Color:=rgb(20,100,2);
rectangle(300+t2+t9-t11+t18-t20+t22,tgros+180-t1*5+t8-t21,320+t2+t9-t11+t18-t20+t22,tgros+235-t1*5+t8-t21); //brat miscare orizontala

 brush.Color:=rgb(200,200,20);

rectangle(130,tgros+180-t1*5+t8-t21,200,tgros+235-t1*5+t8-t21); //piston


brush.Color:=rgb(200,0,0);
rectangle(320+t2+t9-t11+t18-t20+t22,tgros+190-t1*5+t8+t5-t10+t19-t21,350+t2+t9-t11+t18-t20+t22,tgros+200-t1*5+t8+t5-t10+t19-t21); {pensa 1 sus}
rectangle(320+t2+t9-t11+t18-t20+t22,tgros+215-t1*5+t8-t5+t10-t19-t21,350+t2+t9-t11+t18-t20+t22,tgros+225-t1*5+t8-t5+t10-t19-t21); {pensa 2 jos}

 brush.Color:=rgb(0,0,255);

 // piesa

 rectangle(350+t4+t9+t14+t17-2*t32-t20+t22,tgros+55-t3*5+t8-t21,390+t4+t9+t14+t17-2*t32-t20+t22,tgros+60-t3*5+t8-t21);{piesa}
 rectangle(390-tgros+t4+t9+t14+t17-2*t32-t20+t22,2*tgros+43-t3*5+t8-t21,400+tgros+t4+t9+t14+t17-2*t32-t20+t22,70-t3*5+t8-t21);{piesa}

brush.Color:=rgb(250,120,20);

rectangle(120,20,180,350); {cilindru}
brush.Color:=rgb(220,100,20);
rectangle(100,350,200,380); {suport}

brush.Color:=rgb(0,255,255);
rectangle(10,10,20,20);
image1.Canvas.MoveTo(30,30);


Form1.Image1.Canvas.Brush.Color:=clwhite;
if dir then
begin
t:=t+1;
 end

else
 begin
 t:=t-1;
    end;

 if (t>50000) or (t<0)then  dir:= not dir;
  if directie then q:=q-1
             else q:=q+1;
 if (q=qmax)or(q=0) then  directie :=not directie ;
 if directie_linie then q1:=q1-10
                    else q1:=q1+1;
 if (q1=q1max)or(q1=0)then directie_linie :=not directie_linie;

 end;

        end ;

   end.
end;




procedure TForm1.Button1Click(Sender: TObject);
begin
form1.close
end;

procedure TForm1.Button2Click(Sender: TObject);
begin
timer1.enabled:=not (timer1.enabled);
end;

procedure TForm1.Stop1Click(Sender: TObject);
begin
timer1.Enabled := true;
end;

procedure TForm1.Stop2Click(Sender: TObject);
begin
timer1.Enabled := false;
end;

procedure TForm1.Exit1Click(Sender: TObject);
begin
application.Terminate;
end;

procedure TForm1.Image1Click(Sender: TObject);
begin
if (timer1.Enabled) then timer1.enabled := false
      else timer1.enabled := true;
end;

end.
