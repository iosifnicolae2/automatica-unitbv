unit r0b0t1c_4rms2;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls;

type
  TForm1 = class(TForm)
    Image1: TImage;
    Button1: TButton;
    Timer1: TTimer;
    Button2: TButton;
    Button3: TButton;
    Button4: TButton;
    Button5: TButton;
    Button6: TButton;
    procedure Timer1Timer(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Box1Click(Sender: TObject);
    procedure Box2Click(Sender: TObject);
    procedure Box3Click(Sender: TObject);
    procedure Box4Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;
  movx:integer=0;  // horizontal movement
  movy:integer=0;  //vert movment
  movpx:integer=0; movpy:integer=0; movpx2:integer=0;// prens0r vars
  lim1:integer=0;lim2:integer=0; limy0:integer=0;  // extension limits
  limy1:integer=0;  limy2:integer=0;  limy3:integer=0;  limy4:integer=0;
  box:integer=0;      //what box was selected?
  locx:integer=0;  locy:integer=0; baka:integer=0;// box coords on the truck
  movy2:integer=0;  // vertical movement
  locationx:integer=240; locationy:integer=495; // arm movement
  posx1:integer=0; posx2:integer=0; posx3:integer=0; posx4:integer=0;
  posy1:integer=0; posy2:integer=0; posy3:integer=0; posy4:integer=0;
  stage:integer=0;  // current stage of animation

implementation

{$R *.DFM}

procedure TForm1.Timer1Timer(Sender: TObject);
begin

with form1.image1.canvas do
begin
  fillrect(clientrect);
pen.width:=1;
//pen.color:=rgb(20,40,255);
//pen.style:=psdash;
brush.Color:=rgb(200,100,20);
//brush.Style:=bsbdiagonal;
//rectangle(980,740,1180,840); //bottom right
//rectangle(680,740,880,840);  // bottom left
//rectangle(980,640,1180,740); //upper right
//rectangle(680,640,880,740);  // upper left
case baka of
0: begin
      locx:=940;
      locy:=753;
end;

1: begin
      locx:=650;
      locy:=753;
end;
2: begin
      locx:=940;
      locy:=653;
end;
3: begin
      locx:=650;
      locy:=653;
end;
end;

   pen.width:=4;
    brush.Color:=rgb(0,0,0);
    moveto(650,840);lineto(1200,840); // truck platform

    moveto(1200,840);lineto(1200,640);
    rectangle(1220,690,1280,840); //truck head
    brush.Color:=clmaroon;
    rectangle(660,850,1280,880);
    brush.Color:=clwhite;
    rectangle(1230,710,1270,750); //truck window ;)
    brush.Color:=clnavy;
    ellipse(670,850,720,900); // truck wheel
    ellipse(730,850,780,900); // truck wheel
    ellipse(790,850,840,900); // truck wheel
    ellipse(1030,850,1080,900); // truck wheel
    ellipse(1090,850,1140,900); // truck wheel
    ellipse(1225,850,1275,900); // truck wheel
    moveto(0,382);lineto(230,382);
    moveto(0,532);lineto(230,532); // platform
    moveto(0,682);lineto(230,682); // platform        //for t3h boxes
    moveto(0,832);lineto(230,832); // platform


case box of
0: begin sleep(1);

 end;
1: begin
  Button3.Enabled:=False;
  case stage of
    0: begin
        if movx>=-240 then
          begin
            movx:=movx-1;   // horiz ext of platform 1
            if movx>-60 then
             begin
              lim1:=movx;
             end;
            if movx>-145 then
             begin
              lim2:=movx;
             end
          end
        else
          stage:=1;
       end;
    1: begin
        if movy>=-5 then
          begin
           movy:=movy-1;  // move box up a bit
           posy1:=posy1-1;
          end
        else
          stage:=2;
       end;
    2: begin
        if movx<0 then
          begin
            movx:=movx+1;   // horiz ext of platform 1
            if movx>-60 then
             begin
              lim1:=movx;
             end;
            if movx>-145 then
             begin
              lim2:=movx;
             end;
          posx1:=posx1+1;
          end
        else
          stage:=3;
       end;
     3: begin
        if movy<0 then
          begin
           movy:=movy+1;  // move box up a bit
           posy1:=posy1+1;
          end
        else
          stage:=4;
       end;
     4: begin                   //low3r pr3ns0rz
        if movpy<130 then
          begin
             movpy:=movpy+1;
        end
        else
           stage:=5;
       end;
     5: begin                  //catch t3h b0x
        if movpx<5 then
        begin
             movpx:=movpx+1;
        end
        else
           stage:=6;
        end;
     6: begin                   //movez0r pr3nz0rz up
        if movpy>-100 then
          begin
             movpy:=movpy-1;
             posy1:=posy1-1;
          end
         else
           stage:=7;
        end;
     7: begin                   //movez0r the sh1t to position
       // if posx1<locx then
          if locationx<locx then
          begin
             locationx:=locationx+1;
             movpx2:=movpx2+1;
             posx1:=posx1+1;
          end
         else
           stage:=8;
        end;

     8: begin                           //put the sh1t on t3h truck
         //if posy1<locy then
           if locationy<locy then
           begin
              locationy:=locationy+1;
              movpy:=movpy+1;
              posy1:=posy1+1;
           end
         else
            stage:=9;
        end;
     9: begin                  //release the box
        if movpx>0 then
        begin
             movpx:=movpx-1;
        end
        else
           stage:=10;
        end;
     10: begin                                  //retract crane
         if movpy>-100 then
           begin
              movpy:=movpy-1;
           end
         else
            stage:=11;
        end;
     11: begin                   //movez0r the sh1t to position
        if movpx2>0 then
          begin
             movpx2:=movpx2-1;
          end
         else
           stage:=12;
        end;
     12: begin                   //movez0r to init pos
        if movpy<0 then
          begin
             movpy:=movpy+1;
          end
         else
           begin
           baka:=baka+1;
           box:=0;
           stage:=0;
           locationx:=240; locationy:=495;
           end;
        end;//end 12:

end;  //end stage case

end; //end 1:
2: begin
Button4.Enabled:=False;
    case stage of
    0: begin
        if movy>=-150 then
          begin
           movy:=movy-1;  // move box up a bit
           if movy>=-70 then
           begin
              limy0:=movy;
           end;
           if movy>=-140 then
           begin
              limy1:=movy;
           end;
           if movy>=-210 then
           begin
              limy2:=movy;
           end;
           if movy>=-280 then
           begin
              limy3:=movy;
           end;
           if movy>=-350 then
           begin
              limy4:=movy;
           end;
          end
        else
          stage:=1;
       end;
    1: begin
        if movx>=-240 then
          begin
            movx:=movx-1;   // horiz ext of platform 1
            if movx>-60 then
             begin
              lim1:=movx;
             end;
            if movx>-145 then
             begin
              lim2:=movx;
             end
          end
        else
          stage:=2;
       end;
    2: begin
        if movy>=-155 then
          begin
           movy:=movy-1;  // move box up a bit
           posy2:=posy2-1;
          end
        else
          stage:=3;
       end;
    3: begin
        if movx<0 then
          begin
            movx:=movx+1;   // horiz ext of platform 1
            if movx>-60 then
             begin
              lim1:=movx;
             end;
            if movx>-145 then
             begin
              lim2:=movx;
             end;
          posx2:=posx2+1;
          end
        else
          stage:=4;
       end;
     4: begin
        if movy<0 then
         begin
           movy:=movy+1;
           if movy>-70 then
           begin
              limy0:=movy;
           end;
           if movy>-140 then
           begin
              limy1:=movy;
           end;
           if movy>-210 then
           begin
              limy2:=movy;
           end;
           if movy>-280 then
           begin
              limy3:=movy;
           end;
           if movy>-350 then
           begin
              limy4:=movy;
           end;
          posy2:=posy2+1;
         end
        else
          stage:=5;
       end; //end 4:
     5: begin                   //low3r pr3ns0rz
        if movpy<130 then
          begin
             movpy:=movpy+1;
        end
        else
           stage:=6;
       end;
     6: begin                  //catch t3h b0x
        if movpx<5 then
        begin
             movpx:=movpx+1;
        end
        else
           stage:=7;
        end;
     7: begin                   //movez0r pr3nz0rz up
        if movpy>-100 then
          begin
             movpy:=movpy-1;
             posy2:=posy2-1;
          end
         else
           stage:=8;
        end;
     8: begin                   //movez0r the sh1t to position
        //if posx2<locx then
        if locationx<locx then
          begin
             locationx:=locationx+1;
             movpx2:=movpx2+1;
             posx2:=posx2+1;
          end
         else
           stage:=9;
        end;

     9: begin                           //put the sh1t on t3h truck
         //if posy2<locy then
         if locationy<locy then
           begin
              locationy:=locationy+1;
              movpy:=movpy+1;
              posy2:=posy2+1;
           end
         else
            stage:=10;
        end;
     10: begin                  //release the box
        if movpx>0 then
        begin
             movpx:=movpx-1;
        end
        else
           stage:=11;
        end;
     11: begin
         if movpy>-100 then       //retract crane
           begin
              movpy:=movpy-1;
           end
         else
            stage:=12;
        end;
     12: begin                   //movez0r the sh1t to position
        if movpx2>0 then
          begin
             movpx2:=movpx2-1;
          end
         else
           stage:=13;
        end;
     13: begin                   //movez0r to init pos
        if movpy<0 then
          begin
             movpy:=movpy+1;
          end
         else
           begin
           baka:=baka+1;
           box:=0;
           stage:=0;
           locationx:=240; locationy:=495;
           end;
        end;//end 13:

     end;  //end stage case
end;    //end case 2:
3: begin
Button5.Enabled:=False;
    case stage of
    0: begin
        if movy>=-300 then
          begin
           movy:=movy-1;  // move box up a bit
           if movy>=-70 then
           begin
              limy0:=movy;
           end;
           if movy>=-140 then
           begin
              limy1:=movy;
           end;
           if movy>=-210 then
           begin
              limy2:=movy;
           end;
           if movy>=-280 then
           begin
              limy3:=movy;
           end;
           if movy>=-350 then
           begin
              limy4:=movy;
           end;
          end
        else
          stage:=1;
       end;
    1: begin
        if movx>=-240 then
          begin
            movx:=movx-1;   // horiz ext of platform 1
            if movx>-60 then
             begin
              lim1:=movx;
             end;
            if movx>-145 then
             begin
              lim2:=movx;
             end
          end
        else
          stage:=2;
       end;
    2: begin
        if movy>=-305 then
          begin
           movy:=movy-1;  // move box up a bit
           posy3:=posy3-1;
          end
        else
          stage:=3;
       end;
    3: begin
        if movx<0 then
          begin
            movx:=movx+1;   // horiz ext of platform 1
            if movx>-60 then
             begin
              lim1:=movx;
             end;
            if movx>-145 then
             begin
              lim2:=movx;
             end;
          posx3:=posx3+1;
          end
        else
          stage:=4;
       end;
     4: begin
        if movy<0 then
         begin
           movy:=movy+1;
           if movy>-70 then
           begin
              limy0:=movy;
           end;
           if movy>-140 then
           begin
              limy1:=movy;
           end;
           if movy>-210 then
           begin
              limy2:=movy;
           end;
           if movy>-280 then
           begin
              limy3:=movy;
           end;
           if movy>-350 then
           begin
              limy4:=movy;
           end;
          posy3:=posy3+1;
         end
        else
          stage:=5;
       end; //end 4:
       5: begin                   //low3r pr3ns0rz
        if movpy<130 then
          begin
             movpy:=movpy+1;
        end
        else
           stage:=6;
       end;
     6: begin                  //catch t3h b0x
        if movpx<5 then
        begin
             movpx:=movpx+1;
        end
        else
           stage:=7;
        end;
     7: begin                   //movez0r pr3nz0rz up
        if movpy>-100 then
          begin
             movpy:=movpy-1;
             posy3:=posy3-1;
          end
         else
           stage:=8;
        end;
     8: begin                   //movez0r the sh1t to position
        //if posx2<locx then
        if locationx<locx then
          begin
             locationx:=locationx+1;
             movpx2:=movpx2+1;
             posx3:=posx3+1;
          end
         else
           stage:=9;
        end;

     9: begin                           //put the sh1t on t3h truck
         //if posy2<locy then
         if locationy<locy then
           begin
              locationy:=locationy+1;
              movpy:=movpy+1;
              posy3:=posy3+1;
           end
         else
            stage:=10;
        end;
     10: begin                  //release the box
        if movpx>0 then
        begin
             movpx:=movpx-1;
        end
        else
           stage:=11;
        end;
     11: begin
         if movpy>-100 then                 // move crane up
           begin
              movpy:=movpy-1;
           end
         else
            stage:=12;
        end;
     12: begin                   //movez0r the sh1t to position
        if movpx2>0 then
          begin
             movpx2:=movpx2-1;
          end
         else
           stage:=13;
        end;
     13: begin                   //movez0r to init pos
        if movpy<0 then
          begin
             movpy:=movpy+1;
          end
         else
           begin

           baka:=baka+1;
           box:=0;
           stage:=0;
           locationx:=240; locationy:=495;
           end;
        end;//end 13:
     end;  //end stage case
end;    //end case 3:
4: begin
Button6.Enabled:=False;
    case stage of
    0: begin
        if movy>=-450 then
          begin
           movy:=movy-1;  // move box up a bit
           if movy>=-70 then
           begin
              limy0:=movy;
           end;
           if movy>=-150 then
           begin
              limy1:=movy;
           end;
           if movy>=-230 then
           begin
              limy2:=movy;
           end;
           if movy>=-310 then
           begin
              limy3:=movy;
           end;
           if movy>=-390 then
           begin
              limy4:=movy;
           end;
          end
        else
          stage:=1;
       end;
    1: begin
        if movx>=-240 then
          begin
            movx:=movx-1;   // horiz ext of platform 1
            if movx>-60 then
             begin
              lim1:=movx;
             end;
            if movx>-145 then
             begin
              lim2:=movx;
             end
          end
        else
          stage:=2;
       end;
    2: begin
        if movy>=-455 then
          begin
           movy:=movy-1;  // move box up a bit
           posy4:=posy4-1;
          end
        else
          stage:=3;
       end;
    3: begin
        if movx<0 then
          begin
            movx:=movx+1;   // horiz ext of platform 1
            if movx>-60 then
             begin
              lim1:=movx;
             end;
            if movx>-145 then
             begin
              lim2:=movx;
             end;
          posx4:=posx4+1;
          end
        else
          stage:=4;
       end;
     4: begin
        if movy<0 then
         begin
           movy:=movy+1;
           if movy>-70 then
           begin
              limy0:=movy;
           end;
           if movy>-150 then
           begin
              limy1:=movy;
           end;
           if movy>-230 then
           begin
              limy2:=movy;
           end;
           if movy>-310 then
           begin
              limy3:=movy;
           end;
           if movy>-390 then
           begin
              limy4:=movy;
           end;
          posy4:=posy4+1;
         end
        else
          stage:=5;
       end; //end 4:
       5: begin                   //low3r pr3ns0rz
        if movpy<130 then
          begin
             movpy:=movpy+1;
        end
        else
           stage:=6;
       end;
     6: begin                  //catch t3h b0x
        if movpx<5 then
        begin
             movpx:=movpx+1;
        end
        else
           stage:=7;
        end;
     7: begin                   //movez0r pr3nz0rz up
        if movpy>-100 then
          begin
             movpy:=movpy-1;
             posy4:=posy4-1;
          end
         else
           stage:=8;
        end;
     8: begin                   //movez0r the sh1t to position
        //if posx2<locx then
        if locationx<locx then
          begin
             locationx:=locationx+1;
             movpx2:=movpx2+1;
             posx4:=posx4+1;
          end
         else
           stage:=9;
        end;

     9: begin                           //put the sh1t on t3h truck
         //if posy2<locy then
         if locationy<locy then
           begin
              locationy:=locationy+1;
              movpy:=movpy+1;
              posy4:=posy4+1;
           end
         else
            stage:=10;
        end;
     10: begin                  //release the box
        if movpx>0 then
        begin
             movpx:=movpx-1;
        end
        else
           stage:=11;
        end;
     11: begin
         if movpy>-100 then           //back off crane
           begin
              movpy:=movpy-1;
           end
         else
            stage:=12;
        end;
     12: begin                   //movez0r the sh1t to position
        if movpx2>0 then
          begin
             movpx2:=movpx2-1;
          end
         else
           stage:=13;
        end;
     13: begin                   //movez0r to init pos
        if movpy<0 then
          begin
             movpy:=movpy+1;
          end
         else
           begin
           baka:=baka+1;
           box:=0;
           stage:=0;
           locationx:=240; locationy:=495;
           end;
        end;//end 13:
     end;  //end stage case
end;    //end case 4:
end; //end box case

// movx:integer=250;  // horizontal movement
//movy:integer=820;
brush.Color:=clyellow;
rectangle(250+movx,812+movy,500+movx,822+movy); //crane platform 1 base
rectangle(500+movx,722+movy,510+movx,822+movy); //crane platform 1 vert
rectangle(510+movx,762+movy,615+movx,782+movy); //cp1 last horiz extension
rectangle(515+lim2,758+movy,615+lim2,788+movy); //cp1 2 horiz extension
rectangle(520+lim1,752+movy,615+lim1,792+movy); //cp1 3 horiz extension
rectangle(525,750+movy,575,900);           // crane last vert ext
rectangle(520,820+limy4,580,900);
rectangle(515,825+limy3,585,900);
rectangle(510,830+limy2,590,900);
rectangle(505,835+limy1,595,900);
rectangle(500,840+limy0,600,900);           //chibi base pole
rectangle(250,850,600,900);           // base of the crane
//rectangle(250,650,1200,700);
brush.Color:=clred;
pen.width:=2;
rectangle(525,400+movy,575,750+movy);           // horiz crane vert base
ellipse(237+100+movpx2,370+movy,470-100+movpx2,400+movy);
ellipse(277+100+movpx2,370+movy,510-100+movpx2,400+movy);
brush.Color:=clblack;
rectangle(237+100+movpx2,330+movy,510-100+movpx2,370+movy); //motorbox
brush.Color:=clred;
pen.width:=8;
MoveTo(257+116+movpx2,540+movy+movpy); LineTo(257+116+movpx2,350+movy); //cablu
pen.width:=2;
rectangle(257,400+movy,1190,420+movy);  // horiz transfer main

rectangle(257+movpx+movpx2,680+movy+movpy,267+movpx+movpx2,560+movy+movpy);  //left pr3ns0r
rectangle(480-movpx+movpx2,680+movy+movpy,490-movpx+movpx2,560+movy+movpy);  //right pr3ns0r
rectangle(257+movpx2,560+movy+movpy,490+movpx2,530+movy+movpy);  //prens0r transv
//MoveTo(257+116+movpx2,560+movy+movpy); LineTo(257+116+movpx2,400+movy);

    pen.width:=2;
    brush.Color:=cllime;
    rectangle(30+posx4,260+posy4,235+posx4,360+posy4);  // box 4 [top]
    brush.Color:=clskyblue;
    rectangle(30+posx3,410+posy3,235+posx3,510+posy3);  // box 3 [middle]
    brush.Color:=clnavy;
    rectangle(30+posx2,560+posy2,235+posx2,660+posy2); // box 2 [middle down]
    brush.Color:=clgreen;
    rectangle(30+posx1,710+posy1,235+posx1,810+posy1); // box 1 [bottom]


Form1.Image1.Canvas.Brush.Color:=clwhite;

 end;
 end;


procedure TForm1.Button1Click(Sender: TObject);
begin
form1.close
end;

procedure TForm1.Button2Click(Sender: TObject);
begin
timer1.enabled:=not (timer1.enabled);
end;

procedure TForm1.Box1Click(Sender: TObject);
begin
box:=1;
end;

procedure TForm1.Box2Click(Sender: TObject);
begin
box:=2;
end;

procedure TForm1.Box3Click(Sender: TObject);
begin
box:=3;
end;

procedure TForm1.Box4Click(Sender: TObject);
begin
box:=4;
end;

end.
