unit Ut1L1t0r;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls;

type
  TForm1 = class(TForm)
    Image1: TImage;
    Button1: TButton;
    Timer1: TTimer;
    Button2: TButton;
    procedure Timer1Timer(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;
  t:integer=0;
  box,t1,t2,t3,t4,t5:integer;
  varx:integer=0;
  vary:integer=0;
  prex:integer=0;
  boxx,boxy:integer;

  dir,d:boolean;


implementation

{$R *.DFM}

procedure TForm1.Timer1Timer(Sender: TObject);
begin


with form1.image1.canvas do
begin
  fillrect(clientrect);
pen.width:=1;
//pen.color:=rgb(20,40,255);
//pen.style:=psdash;

case box of
0: begin
  if vary<74 then
           begin
              vary:=vary+1;
           end
         else
            box:=1;
  end;
1:
  begin
  if prex<2 then
        begin
             prex:=prex+1;
        end
        else
            box:=2;
  end;
2: begin
 if vary>0 then
           begin
              vary:=vary-1; boxy:=boxy-1;
           end
         else
            box:=3;
  end;
3: begin
  if varx<215 then
            begin
              varx:=varx+1; boxx:=boxx+1;
              end
            else
              box:=4;
  end;
4: begin
  if vary<74 then
           begin
              vary:=vary+1; boxy:=boxy+1;
           end
         else
            box:=5;
  end;
5:
  begin
  if prex>0 then
        begin
             prex:=prex-1;
        end
        else
            box:=6;
  end;
6: begin
 if vary>0 then
           begin
              vary:=vary-1;
           end
         else
            box:=7;
  end;
7: begin
    if varx>0 then
            begin
              varx:=varx-1; boxx:=boxx+1;
            end
            else
              box:=8;
  end;
8: begin
      varx:=0;
      vary:=0;
      prex:=0;
      boxx:=0;
      boxy:=0;
      box:=0;
    end;
end;

//first base cube
brush.Color:=rgb(200,200,200);
rectangle(30,220,160,250);

//second base cube
brush.Color:=rgb(200,200,200);
rectangle(260,220,490,250);

//movement area for lift sustain0r
brush.Color:=rgb(200,200,200);
rectangle(20,20,470,30); {1st cube}

//lift sustain0r
brush.Color:=rgb(200,200,20);
rectangle(65+varx,10,125+varx,20);
ellipse(55+varx,10,65+varx,20);
ellipse(125+varx,10,135+varx,20);

//lift c4blz0r
brush.Color:=rgb(0,0,0);
rectangle(94+varx,20,96+varx,100+vary);

//lift bar
brush.Color:=rgb(0,0,0);
rectangle(65+varx,100+vary,125+varx,104+vary);

//prensor 1
brush.Color:=rgb(200,200,20);
rectangle(65+prex+varx,104+vary,68+prex+varx,112+vary);

//prensor 2
brush.Color:=rgb(200,200,20);
rectangle(122-prex+varx,104+vary,125-prex+varx,112+vary);

//load
pen.width:=0;
brush.Color:=rgb(162,231,126);
rectangle(70+boxx,180+boxy,120+boxx,220+boxy);

Form1.Image1.Canvas.Brush.Color:=clwhite;
if dir then
begin
t:=t+1;
 end

else
 begin
 t:=t-1;
    end;

 if (t>191) or (t<0)then dir:= not dir;

 end;
 end;


procedure TForm1.Button1Click(Sender: TObject);
begin
form1.close
end;

procedure TForm1.Button2Click(Sender: TObject);
begin
timer1.enabled:=not (timer1.enabled);
end;

end.
