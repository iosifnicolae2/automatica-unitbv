object Form1: TForm1
  Left = 220
  Top = 147
  Width = 585
  Height = 375
  Caption = 'Form1'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Menu = MainMenu1
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Image1: TImage
    Left = 8
    Top = 8
    Width = 561
    Height = 313
  end
  object Timer1: TTimer
    Interval = 30
    OnTimer = Timer1Timer
    Left = 256
    Top = 176
  end
  object MainMenu1: TMainMenu
    Left = 8
    Top = 8
    object Control1: TMenuItem
      Caption = '&Control'
      object Start1: TMenuItem
        Caption = '&Tip ceai'
        object Verde1: TMenuItem
          Caption = 'Verde'
          ShortCut = 16470
          OnClick = Verde1Click
        end
        object Negru1: TMenuItem
          Caption = 'Negru'
          ShortCut = 16462
          OnClick = Negru1Click
        end
      end
      object Pauz1: TMenuItem
        Caption = '&Zah'#259'r'
        object N0cuburi1: TMenuItem
          Caption = '0 buc'#259'ti'
          OnClick = N0cuburi1Click
        end
        object N11: TMenuItem
          Caption = '1 bucat'#259
          ShortCut = 112
          OnClick = N11Click
        end
        object N2bucti1: TMenuItem
          Caption = '2 buc'#259'ti'
          ShortCut = 113
          OnClick = N2bucti1Click
        end
        object N3bucti1: TMenuItem
          Caption = '3 buc'#259'ti'
          ShortCut = 114
          OnClick = N3bucti1Click
        end
        object N4bucti1: TMenuItem
          Caption = '4 buc'#259'ti'
          ShortCut = 115
          OnClick = N4bucti1Click
        end
      end
    end
    object Exit1: TMenuItem
      Caption = '&Inchide'
      OnClick = Exit1Click
    end
  end
end
