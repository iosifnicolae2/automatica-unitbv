unit Unit2;

interface
uses
  Math, Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls;
type
  TForm1 = class(TForm)
  Image1: TImage;
  Timer1: TTimer;
  procedure timer1timer(Sender:TObject);
  private
  public
  end;
type punct=record
      x,y:integer;
      end;
     Incheietura=record
      poz:punct;
      alfa,l:integer;
      end;
     clest=record
      x,y:integer;
      poz:array[1..16] of punct;
      end;
     robot_pr=record
      inchei:array[1..4] of incheietura;
      cleste:clest;
      end;
     robot_sc=record
      inchei:array[1..3] of incheietura;
      cleste:clest;
      end;
    Robot_Principal=robot_pr;
    Robot_Secundar=robot_sc;
var Form1: TForm1;
    rp,aux:Robot_Principal;
    rs:Robot_Secundar;
    i,j,k,deplasare:integer;
    directie:array[1..4] of -1..1;
    a:array[1..16]of punct;
implementation
procedure costr_brat_rp(brat:integer;var aux:Robot_Principal);
 var micro_aux,hlp:Extended;
 begin
    micro_aux:=trunc(15/rp.inchei[brat].l);
    hlp:=Cos(Pi);
    aux.inchei[1].poz.x:=trunc(rp.inchei[brat].poz.x+sin((rp.inchei[brat].alfa-0)*3.14/180)*15);
    aux.inchei[1].poz.y:=trunc(rp.inchei[brat].poz.y+cos((rp.inchei[brat].alfa-0)*3.14/180)*15);

    aux.inchei[2].poz.x:=trunc(rp.inchei[brat].poz.x+sin((rp.inchei[brat].alfa+180)*3.14/180)*15);
    aux.inchei[2].poz.y:=trunc(rp.inchei[brat].poz.y+cos((rp.inchei[brat].alfa+180)*3.14/180)*15);

    aux.inchei[3].poz.x:=trunc(rp.inchei[brat].poz.x+sin((rp.inchei[brat].alfa+180-86)*3.14/180)*(rp.inchei[brat].l));
    aux.inchei[3].poz.y:=trunc(rp.inchei[brat].poz.y+cos((rp.inchei[brat].alfa+180-86)*3.14/180)*(rp.inchei[brat].l));

    aux.inchei[4].poz.x:=trunc(rp.inchei[brat].poz.x+sin((rp.inchei[brat].alfa+86)*3.14/180)*(rp.inchei[brat].l));
    aux.inchei[4].poz.y:=trunc(rp.inchei[brat].poz.y+cos((rp.inchei[brat].alfa+86)*3.14/180)*(rp.inchei[brat].l));

 end;
procedure constr_cleste_rp(deplasare:byte;var aux:robot_Principal);
 begin
  //1
  aux.cleste.poz[1].x:=aux.cleste.x+15;
  aux.cleste.poz[1].y:=aux.cleste.y;
  //2
  aux.cleste.poz[2].x:=aux.cleste.x+15;
  aux.cleste.poz[2].y:=aux.cleste.y+30;
  //3
  aux.cleste.poz[3].x:=aux.cleste.x+35;
  aux.cleste.poz[3].y:=aux.cleste.y+30;
  //4
  aux.cleste.poz[4].x:=aux.cleste.x+35;
  aux.cleste.poz[4].y:=aux.cleste.y+40;
  //5
  aux.cleste.poz[5].x:=aux.cleste.x+35-deplasare;
  aux.cleste.poz[5].y:=aux.cleste.y+40;
  //6
  aux.cleste.poz[6].x:=aux.cleste.x+35-deplasare;
  aux.cleste.poz[6].y:=aux.cleste.y+80;
  //7
  aux.cleste.poz[7].x:=aux.cleste.x+25-deplasare;
  aux.cleste.poz[7].y:=aux.cleste.y+85;
  //8
  aux.cleste.poz[8].x:=aux.cleste.x+25-deplasare;
  aux.cleste.poz[8].y:=aux.cleste.y+40;
  //16
  aux.cleste.poz[16].x:=aux.cleste.x-15;
  aux.cleste.poz[16].y:=aux.cleste.y;
  //15
  aux.cleste.poz[15].x:=aux.cleste.x-15;
  aux.cleste.poz[15].y:=aux.cleste.y+30;
  //14
  aux.cleste.poz[14].x:=aux.cleste.x-35;
  aux.cleste.poz[14].y:=aux.cleste.y+30;
  //13
  aux.cleste.poz[13].x:=aux.cleste.x-35;
  aux.cleste.poz[13].y:=aux.cleste.y+40;
  //12
  aux.cleste.poz[12].x:=aux.cleste.x-35+deplasare;
  aux.cleste.poz[12].y:=aux.cleste.y+40;
  //11
  aux.cleste.poz[11].x:=aux.cleste.x-35+deplasare;
  aux.cleste.poz[11].y:=aux.cleste.y+80;
  //10
  aux.cleste.poz[10].x:=aux.cleste.x-25+deplasare;
  aux.cleste.poz[10].y:=aux.cleste.y+85;
  //9
  aux.cleste.poz[9].x:=aux.cleste.x-25+deplasare;
  aux.cleste.poz[9].y:=aux.cleste.y+40;


 end;
procedure TForm1.timer1timer(Sender:TObject);
  begin
   with form1.Image1.Canvas do
    begin
    fillrect(clientrect);
    brush.color:=clred;

    brush.color:=clwhite;
    //*************************************************************************
    // Banda jos - Principala
    //*************************************************************************
    rectangle(135,600,735,630);
    ellipse(150,600,120,630);
    ellipse(450,600,420,630);
    ellipse(750,600,720,630);
    inc(i);
    for j:=1 to 8 do
       begin

         a[j].x:=trunc(435+sin((i+j*45)*3.14/180)*15);
         a[j].y:=trunc(615-cos((i+(j)*45)*3.14/180)*15);

         a[j+8].x:=trunc(435+sin(((i+j*45)+22.5)*3.14/180)*8);
         a[j+8].y:=trunc(615-cos(((i+j*45)+22.5)*3.14/180)*8);

       end;
{   polygon([point(a[1].x,a[1].y),point(a[9].x,a[9].y),point(a[2].x,a[2].y),point(a[10].x,a[10].y),
             point(a[3].x,a[3].y),point(a[11].x,a[11].y),point(a[4].x,a[4].y),point(a[12].x,a[12].y),
             point(a[5].x,a[5].y),point(a[13].x,a[13].y),point(a[6].x,a[6].y),point(a[14].x,a[14].y),
             point(a[7].x,a[7].y),point(a[15].x,a[15].y),point(a[8].x,a[8].y),point(a[16].x,a[16].y),
             point(a[1].x,a[1].y),point(a[1].x,a[1].y)]);}

  polygon([point(a[1].x+300,a[1].y),point(a[9].x+300,a[9].y),point(a[2].x+300,a[2].y),point(a[10].x+300,a[10].y),
             point(a[3].x+300,a[3].y),point(a[11].x+300,a[11].y),point(a[4].x+300,a[4].y),point(a[12].x+300,a[12].y),
             point(a[5].x+300,a[5].y),point(a[13].x+300,a[13].y),point(a[6].x+300,a[6].y),point(a[14].x+300,a[14].y),
             point(a[7].x+300,a[7].y),point(a[15].x+300,a[15].y),point(a[8].x+300,a[8].y),point(a[16].x+300,a[16].y),
             point(a[1].x+300,a[1].y),point(a[1].x+300,a[1].y)]);

  polygon([point(a[1].x-300,a[1].y),point(a[9].x-300,a[9].y),point(a[2].x-300,a[2].y),point(a[10].x-300,a[10].y),
             point(a[3].x-300,a[3].y),point(a[11].x-300,a[11].y),point(a[4].x-300,a[4].y),point(a[12].x-300,a[12].y),
             point(a[5].x-300,a[5].y),point(a[13].x-300,a[13].y),point(a[6].x-300,a[6].y),point(a[14].x-300,a[14].y),
             point(a[7].x-300,a[7].y),point(a[15].x-300,a[15].y),point(a[8].x-300,a[8].y),point(a[16].x-300,a[16].y),
             point(a[1].x-300,a[1].y),point(a[1].x-300,a[1].y)]);

    //*************************************************************************
    // Banda 2
    //*************************************************************************
    rectangle(15,350,615,380);
    ellipse(30,350,0,380);
    ellipse(330,350,300,380);
    ellipse(630,350,600,380);
    //*************************************************************************
    // Banda 3
    //*************************************************************************
    rectangle(15,500,615,530);
    ellipse(30,500,0,530);
    ellipse(330,500,300,530);
    ellipse(630,500,600,530);

    //**********************
    // ROBOT 1 -PRINCIPAL
    //**********************
    brush.color:=clred;
    rectangle(805,580,835,700);
    rectangle(740,680,900,700);
    rp.inchei[1].poz.x:=820;
    rp.inchei[1].poz.y:=580;
     if rp.inchei[1].alfa=0 then
       begin
        rp.inchei[1].alfa:=90;
        rp.inchei[2].alfa:=90;
        rp.inchei[3].alfa:=180;
       end;
     if rp.inchei[1].alfa=90 then directie[1]:=-1;
     if rp.inchei[1].alfa=20 then directie[1]:=1;

     if rp.inchei[2].alfa=90 then directie[2]:=+1;
     if rp.inchei[2].alfa=200 then directie[2]:=-1;

     if rp.inchei[3].alfa=180 then directie[3]:=+1;
     if rp.inchei[3].alfa=230 then directie[3]:=-1;

    inc(rp.inchei[1].alfa,directie[1]);
    inc(rp.inchei[2].alfa,directie[2]);
    inc(rp.inchei[3].alfa,directie[3]);

    rp.inchei[1].l:=200;
    costr_brat_rp(1,aux);
    polygon([point(aux.inchei[1].poz.x,aux.inchei[1].poz.y),point(aux.inchei[2].poz.x,aux.inchei[2].poz.y)
            ,point(aux.inchei[3].poz.x,aux.inchei[3].poz.y),point(aux.inchei[4].poz.x,aux.inchei[4].poz.y)
            ,point(aux.inchei[1].poz.x,aux.inchei[1].poz.y)]);


    //***********brat 2
    rp.inchei[2].poz.x:=trunc(rp.inchei[1].poz.x+sin((rp.inchei[1].alfa+90)*3.14/180)*(rp.inchei[1].l));
    rp.inchei[2].poz.y:=trunc(rp.inchei[1].poz.y+cos((rp.inchei[1].alfa+90)*3.14/180)*(rp.inchei[1].l));

    rp.inchei[2].l:=200;

    costr_brat_rp(2,aux);
    polygon([point(aux.inchei[1].poz.x,aux.inchei[1].poz.y),point(aux.inchei[2].poz.x,aux.inchei[2].poz.y)
            ,point(aux.inchei[3].poz.x,aux.inchei[3].poz.y),point(aux.inchei[4].poz.x,aux.inchei[4].poz.y)
            ,point(aux.inchei[1].poz.x,aux.inchei[1].poz.y)]);

    //***********brat 3
    rp.inchei[3].poz.x:=trunc(rp.inchei[2].poz.x+sin((rp.inchei[2].alfa+90)*3.14/180)*(rp.inchei[2].l));
    rp.inchei[3].poz.y:=trunc(rp.inchei[2].poz.y+cos((rp.inchei[2].alfa+90)*3.14/180)*(rp.inchei[2].l));

    rp.inchei[3].l:=150;

    costr_brat_rp(3,aux);
    polygon([point(aux.inchei[1].poz.x,aux.inchei[1].poz.y),point(aux.inchei[2].poz.x,aux.inchei[2].poz.y)
            ,point(aux.inchei[3].poz.x,aux.inchei[3].poz.y),point(aux.inchei[4].poz.x,aux.inchei[4].poz.y)
            ,point(aux.inchei[1].poz.x,aux.inchei[1].poz.y)]);
    //cleste
    rp.cleste.x:=trunc(rp.inchei[3].poz.x+sin((rp.inchei[3].alfa+90)*3.14/180)*(rp.inchei[3].l));
    rp.cleste.y:=trunc(rp.inchei[3].poz.y+cos((rp.inchei[3].alfa+90)*3.14/180)*(rp.inchei[3].l));
    inc(deplasare,k);
     if deplasare=20 then  k:=-1;
     if deplasare=0 then k:=1;
    constr_cleste_rp(deplasare,rp);
    polygon([point(rp.cleste.poz[1].x,rp.cleste.poz[1].y),point(rp.cleste.poz[2].x,rp.cleste.poz[2].y),
             point(rp.cleste.poz[3].x,rp.cleste.poz[3].y),point(rp.cleste.poz[4].x,rp.cleste.poz[4].y),
             point(rp.cleste.poz[5].x,rp.cleste.poz[5].y),point(rp.cleste.poz[6].x,rp.cleste.poz[6].y),
             point(rp.cleste.poz[7].x,rp.cleste.poz[7].y),point(rp.cleste.poz[8].x,rp.cleste.poz[8].y),
             point(rp.cleste.poz[9].x,rp.cleste.poz[9].y),point(rp.cleste.poz[10].x,rp.cleste.poz[10].y),
             point(rp.cleste.poz[11].x,rp.cleste.poz[11].y),point(rp.cleste.poz[12].x,rp.cleste.poz[12].y),
             point(rp.cleste.poz[13].x,rp.cleste.poz[13].y),point(rp.cleste.poz[14].x,rp.cleste.poz[14].y),
             point(rp.cleste.poz[15].x,rp.cleste.poz[15].y),point(rp.cleste.poz[16].x,rp.cleste.poz[16].y),
             point(rp.cleste.poz[1].x,rp.cleste.poz[1].y),point(rp.cleste.poz[1].x,rp.cleste.poz[1].y)]);

    //***********incheieturi
    brush.color:=clblue;
    ellipse(805,565,835,595);
    ellipse(rp.inchei[3].poz.x-15,rp.inchei[3].poz.y-15,rp.inchei[3].poz.x+15,rp.inchei[3].poz.y+15);
    ellipse(rp.inchei[2].poz.x-15,rp.inchei[2].poz.y-15,rp.inchei[2].poz.x+15,rp.inchei[2].poz.y+15);
    ellipse(rp.cleste.x-15,rp.cleste.y-15,rp.cleste.x+15,rp.cleste.y+15);    

    brush.color:=clred;


    brush.color:=clwhite;
    pen.Color:=clblack;
    end;
  end;
end.
