unit lab1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls;

type
  TForm1 = class(TForm)
    Image1: TImage;
    Timer1: TTimer;
    procedure Timer1Timer(Sender:TObject);
    private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;
  tx,ty: integer;
  tx1,ty1: integer;
  y1,y2: integer;
  ox: boolean=true;
  oy: boolean=true;
  traza: integer=10;
  raza: boolean=true;
  fixy1,fixy2: integer;
 const
 x=200;
 y=200;
 txmax=20;
 tymax=50;
 trazamax=25;
 trazamin=5;
 razax=75;
 x1=60;
 x2=20;
  implementation
  procedure TForm1.Timer1Timer(Sender:Tobject);
  begin
  with form1.Image1.Canvas do
  begin

  fillrect(clientrect);
  pen.width:=1;

  y1:=round(Sqrt((1-(x1*x1)/razax/razax)*traza*traza));
  y2:=round(Sqrt((1-(x2*x2)/razax/razax)*traza*traza));

  brush.Color:=rgb(50,150,100);

  rectangle(x-razax+tx,40+2*ty,x+razax+tx, 50+2*ty);
  ellipse(x-razax+tx,40-trazamin+trazamax+2*ty,x+razax+tx,40+trazamin-trazamax+2*ty);
  ellipse(x-razax+tx,50-trazamin+trazamax+2*ty,x+razax+tx,50+trazamin-trazamax+2*ty);
  rectangle(x-razax,350,x+razax, 360);
  ellipse(x-razax,360-trazamin+trazamax,x+razax,360+trazamin-trazamax);
  ellipse(x-razax,350-trazamin+trazamax,x+razax,350+trazamin-trazamax);
  brush.Color:=rgb(15,13,20);

  if (tx=ty)
  then
  begin
  rectangle(x+tx-razax,y+ty+10,x+tx+razax,y+ty);
  ellipse(x+tx-razax,y+ty-traza,x+tx+razax,y+ty+traza);
  ellipse(x+tx-razax,y+12+ty-traza,x+tx+razax,y+12+ty+traza);
  end;
  brush.Color:=rgb(255,255,255);

//Aici am declarat bratele hexapodului

  fixy1:=round(Sqrt((1-(x1*x1)/razax/razax)*(trazamax-trazamin)*(trazamax-trazamin)));
  fixy2:=round(Sqrt((1-(x2*x2)/razax/razax)*(trazamax-trazamin)*(trazamax-trazamin)));
  pen.width:=5;
  brush.Color:=rgb(0,0,0);
  ellipse(x-x1-6,350-fixy1+15,x-x1+6,350-fixy1+8);
  moveto(x-x1,350-fixy1+10);
  lineto(x+tx-x1,y+ty+y1);
  ellipse(x-x2-6,350+fixy2-5,x-x2+6,350+fixy2-12);
  moveto(x-x2,350+fixy2-10);
  lineto(x+tx-x2,y+ty-y2+15);
  ellipse(x+x1-6,350+fixy1-5,x+x1+6,350+fixy1-12);
  moveto(x+x1,350+fixy1-10);
  lineto(x+tx+x1,y+ty-y1+10);
  ellipse(x+x2-6,350-fixy2+13,x+x2+6,350-fixy2+6);
  moveto(x+x2,350-fixy2+5);
  lineto(x+tx+x2,y+ty-y2);

  rectangle(x-x1+tx-4,50+fixy1+2*ty-6,x-x1+tx+6,50+fixy1+2*ty-11);
  ellipse(x-x1+tx-5,50+fixy1+2*ty-3,x-x1+tx+6,50+fixy1+2*ty-13);
  moveto(x-x1+tx,50+fixy1+2*ty-10);
  lineto(x+tx-x1,y+ty-y1);
  rectangle(x-x2+tx-4,50-fixy2+2*ty+14,x-x2+tx+6,50-fixy2+2*ty+9);
  ellipse(x-x2+tx-5,50-fixy2+2*ty+17,x-x2+tx+6,50-fixy2+2*ty+7);
  moveto(x-x2+tx,50-fixy2+2*ty+10);
  lineto(x+tx-x2,y+ty+y2);
  rectangle(x+x1+tx-4,50-fixy1+2*ty+14,x+x1+tx+6,50-fixy1+2*ty+9);
  ellipse(x+x1+tx-5,50-fixy1+2*ty+17,x+x1+tx+6,50-fixy1+2*ty+7);
  moveto(x+x1+tx,50-fixy1+2*ty+10);
  lineto(x+tx+x1,y+ty+y1);
  Brush.Color:=rgb(0,0,0);
  rectangle(x+x2+tx-4,50+fixy2+2*ty-6,x+x2+tx+6,50+fixy2+2*ty-11);
  ellipse(x+x2+tx-5,50+fixy2+2*ty-3,x+x2+tx+6,50+fixy2+2*ty-13);
  moveto(x+x2+tx,50+fixy2+2*ty-5);
  lineto(x+tx+x2,y+ty-y2);
  Brush.Color:=rgb(205,228,220);
  

// aici am declarat pistoanele
  pen.width:=9;
  moveto(x-x1+tx,50+fixy1+2*ty-10);
  lineto(x+tx-x1,50+fixy1+2*ty+50);
  moveto(x-x2+tx,50-fixy2+2*ty+10);
  lineto(x+tx-x2,50-fixy2+2*ty+80);
  moveto(x+x1+tx,50-fixy1+2*ty+10);
  lineto(x+tx+x1,50-fixy1+2*ty+80);
  moveto(x+x2+tx,50+fixy2+2*ty-5);
  lineto(x+tx+x2,50+fixy2+2*ty+50);

  moveto(x-x1,350-fixy1+10);
  lineto(x+round(tx/2)-x1,350-fixy1-50);
  moveto(x-x2,350+fixy2-10);
  lineto(x+round(tx/2)-x2,350+fixy2-70);
  moveto(x+x1,350+fixy1-10);
  lineto(x+round(tx/2)+x1,350+fixy1-70);
  moveto(x+x2,350-fixy2+5);
  lineto(x+round(tx/2)+x2,350-fixy2-50);

  if ox then tx:=tx+1
    else tx:=tx-1;
  if(tx=-txmax)
  then
    begin
      ox:=not ox;
    end;
  if(tx=txmax)
  then
    begin
      ox:=not ox;
    end;

  if oy then ty:=ty+1
    else ty:=ty-1;
  if(ty=0)
  then
    begin
      oy:=not oy;
    end;
  if(ty=tymax)
  then
    begin
      oy:=not oy;
    end;

      if raza then traza:=traza+1
    else traza:=traza-1;
  if(traza=trazamin)
  then
    begin
      raza:=not raza;
    end;
  if(traza=trazamax)
  then
    begin
      raza:=not raza;
    end;
{$R *.dfm}
  end;
  end;
end. 
