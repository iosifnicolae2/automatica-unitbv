unit unitrobot;
//Marius
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls,RpBase;

type
  TForm1 = class(TForm)
    Image1: TImage;
    Button1: TButton;
    Timer1: TTimer;
    Button2: TButton;
    Edit1: TEdit;
    procedure Timer1Timer(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;
  t:integer=0;
  t1,t2,t3,t4,t5,t6,t7,t8,t9,t10:integer;
  dir,d:boolean;
  dist:integer=200;
implementation

{$R *.DFM}

procedure TForm1.Timer1Timer(Sender: TObject);
var ok:string;
begin
t9:=-10;
t8:=0;
 if (dir) then
  t10:=t
  else
  t10:=-t;
 if (t>=0)and(t<53)and dir then
  begin
   t1:=t;
   t2:=-200;
   t3:=0;
   t4:=-10
  end;
 if (t>53)and (t<158)and dir then
 begin
  t1:=53;
  t8:=t+2-55;
 end;
 if (t>=158)and(t<=178)and dir then
   begin
    t1:=53;
    t8:=105;
    t9:=-(178-t)div 2;
   end;
 if (t>178)and(t<=180) and dir then
    begin
     t1:=53;
     t8:=105;
     t9:=0;
    end;
 if (t>232)and(t<=285)and not dir then
  begin
     t1:=t-232;
     t8:=0;
     t9:=-10
    end;
 if (t>180)and(t<=285) and dir then
  begin
    t1:=53;
    t8:=-t+285;
    t6:=t8+245;
    t9:=0;
 end;
 if (t>285)and(t<950) then
 begin
  t1:=53+t-285;
  t8:=0;
  if dir then
   begin
   t6:=t8+245;
   t9:=0;
   t5:=t1+347;
  end
  else
  begin
   t6:=245;
   t9:=-10;
   t5:=1400;
  end

 end;
 if (t>=950)and(t<=970) then
 begin
  t1:=53+t-285;
  t8:=0;
  if dir then
   begin
    t6:=245;
    t9:=-(t-950)div 2;
    t9:=-10;
    t5:=53+t-285+347;
   end
  else
   begin
    t6:=245;
    t9:=-10;
    t5:=1400; //e out
   end
 end;
 if (t>970)and(t<=1000) then
 begin
  t1:=53+t-285;
  t8:=970-t;
  t9:=-10;
  if (dir) then
   begin
    t9:=-10;
    t6:=245;
    t5:=53+t-285+347;
   end
  else
  begin
    t6:=245;
    t5:=53+1300-285+347;
   end;
 end;
 if (t>1000) then
 begin
  t1:=53+1000-285;
  if (dir) then
   begin
    t8:=-30;
    t6:=245;
    t9:=-10;
    t5:=53+t-285+347;
   end
  else
   begin
    t8:=-30;
    t6:=245;
    t9:=-10;
    t5:=53+1300-285+347;
   end;
 end;



 if (t<0) then
  begin
   t1:=0;
   t2:=-t-100;
   t3:=0;
   if (dir) then
    t4:=-10
   else
    begin
     t4:=0;
     t5:=t2;
     t6:=t3;
    end;
  end; //miscare macara sup
 if (t<-500)and (t>-850) then
  begin
   t1:=0;
   t2:=400;
   t3:=-t-500;
   if (dir) then
    t4:=-10
   else
    begin
     t4:=0;
     t5:=t2;
     t6:=t3;
    end;
   end;   //miscare obiect jos
 if (t<=-850)and(t>=-870) then
  begin
   t1:=0;
   t2:=400;
   t3:=350;
   t5:=t2;
   t6:=t3;
   if (not dir) then
    t4:=-(-t-850)div 2
   else
    t4:=-10;
  end;    //deschidere pense
 if (t<-852) then
  begin
   t1:=0;
   t2:=400;
   t3:=350;
   t5:=400;
   t6:=350;
   t4:=-10;
  end;    //raman deschise pensele


with form1.Image1.Canvas do
     begin
      //pen.Color := clwhite;
      FillRect(ClientRect);
      brush.Color:=clblack;
      Rectangle(0,500,1500,600); //suport podea
      Rectangle(0,10,510,20);
      Rectangle(500,0,510,20); //sina macara
      Rectangle(25+t1,475,25+dist+t1,470); //punte fata-spate
      Rectangle(75+t1,475,-25+dist+t1,400); //suport punte
      RoundRect(t1,430,50+dist+t1,400,10,10); //placa suport
       brush.Color:=clgreen;
      RoundRect(t1,400,100+t1,360,20,20);     //motor
      //suports for weels
      brush.Color:=clred;
      Rectangle(0+18+t1,475,0+32+t1,430);
      Rectangle(0+18+t1+dist,475,0+32+t1+dist,430);
      brush.Color:=clgray;
      //wheels
      pen.Width:=5;
      pen.Color := clblack;
      Ellipse(0+t1,450,50+t1,500);
      Ellipse(0+dist+t1,450,50+dist+t1,500);
      brush.Color:=clblack;
      MoveTo(t1+25,475);        //for the radius
      Lineto(t1+25+round(20*cos(t1/10)),475+round(20*sin(t1/10)));
      MoveTo(t1+25+dist,475);        //for the radius
      Lineto(t1+25+dist+round(20*cos(t1/10)),475+round(20*sin(t1/10)));
      pen.Width:=0;
      //cable
      pen.Width:=3;
      pen.Color := clMaroon;
      MoveTo(156+t1,390);
      Lineto(dist+t1+172,185);
      MoveTo(50+dist+t1+147,200);
      LineTo(50+dist+t1+147,300+t8);
      brush.Color:=clblue;
      pen.Color := clblack;
      pen.Width:=1;
      RoundRect(30+dist+t1+147,300+t8,70+dist+t1+147,310+t8,5,5);  //pense masina
      RoundRect(42+dist+t1+147+t9,310+t8,45+dist+t1+147+t9,325+t8,5,5);
      RoundRect(58+dist+t1+147-t9,310+t8,55+dist+t1+147-t9,325+t8,5,5);
      //arm + distrib weels
      pen.Width:=10;
      pen.Color := clblack;
      MoveTo(40+dist+t1,420);
      Lineto(50+dist+t1+130,200);
      LineTo(dist+t1-50,420);
      pen.Width:=0;
      Rectangle(150+t1,400,130+t1,380);
      brush.Color:=clgray;
      Ellipse(120+t1,400,160+t1,360); //lower wheel
      Ellipse(50+dist+t1+110,180,50+dist+t1+150,220);//uper wheel
      pen.Width:=3;
      pen.Color := clblack;
      MoveTo(140+t1,380);
      LineTo(140+t1+round(18*cos(t8/10)),380-round(18*sin(t8/10)));
      MoveTo(50+dist+t1+130,200);
      LineTo(50+dist+t1+130+round(18*cos(t8/10)),200+round(18*sin(t8/10)));
      brush.Color:=clblack;
      RoundRect(100+t1,385,120+t1,375,3,3);
      pen.Width:=0;
      //lift 1
      brush.Color:=clgray;
      RoundRect(0+t2,0,100+t2,10,10,10);
      Rectangle(10+t2,10,20+t2,30);
      Rectangle(80+t2,10,90+t2,30);
      brush.Color:=clgreen;
      RoundRect(0+t2,30,100+t2,50,10,10);
      brush.Color:=clblack;
      Rectangle(48+t2,50,52+t2,55+t3);
      brush.Color:=clblue;
      RoundRect(30+t2,55+t3,70+t2,65+t3,5,5);
      RoundRect(42+t2+t4,65+t3,45+t2+t4,80+t3,5,5);
      RoundRect(58+t2-t4,65+t3,55+t2-t4,80+t3,5,5);
      //obiectul de carat(t5,t6)
      brush.Color:=clred;
      Rectangle(30+t5,80+t6,70+t5,150+t6);
      Rectangle(45+t5,70+t6,55+t5,80+t6);

      brush.color:=clmaroon;
      rectangle(500,400,510,90);
      brush.color:=clmaroon;
      rectangle(800,400,790,90);
      brush.color:=clred;
      rectangle(500,200,700,100);
      brush.color:=clyellow;
      rectangle(600,200,700,100);
      brush.color:=clwhite;
      brush.color:=clwhite;
      ellipse(620,180,680,120);
      brush.color:=clblue;
      rectangle(700,200,800,100);



      //linie transport
     // Rectangle(0,500,1500,600); //suport podea
     brush.Color :=clblue;
     pen.Color := clred;
     pen.Width:=10;
     RoundRect(1100,400,1700,450,50,50);
     pen.Width:=0;
     Ellipse(1100,400,1150,450);
     MoveTo(1125,425);
     pen.Width:=1;
     LineTo(1125,500);
     pen.Width:=4;
     MoveTo(1125,42);
     Lineto(1125+round(23*cos(t10/10)),425+round(23*sin(t10/10)));
     MoveTo(1125,425);
     Lineto(1125-round(23*cos(t10/10)),425-round(23*sin(t10/10)));
     pen.Width:=0;
     brush.Color:=clwhite;
     moveto(1200,900);
     lineto(1800,900);
     MoveTo(1125,425);
     pen.Width:=1;
     end;
 if (dir) then
  inc(t)
  else
  dec(t);
 if (t>1200) or (t<-852)then dir:= not dir;
 str(t,ok);
 Edit1.Text:=ok;
end;

procedure TForm1.Button1Click(Sender: TObject);
begin
form1.close
end;

procedure TForm1.Button2Click(Sender: TObject);
begin
timer1.enabled:=not (timer1.enabled);
end;

end.
