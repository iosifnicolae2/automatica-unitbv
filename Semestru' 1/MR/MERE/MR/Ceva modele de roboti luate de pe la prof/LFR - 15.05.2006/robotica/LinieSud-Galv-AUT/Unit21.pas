unit Unit21;
interface
uses
   Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Menus, ColorGrd, jpeg, ComCtrls;
type
  TForm1 = class(TForm)
    Image1: TImage;
    Timer1: TTimer;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    MainMenu1: TMainMenu;
    Choixdescouleurs1: TMenuItem;
    Jaune1: TMenuItem;
    Bleu1: TMenuItem;
    Violet1: TMenuItem;
    Vert1: TMenuItem;
    Rouge1: TMenuItem;
    Label1: TLabel;
    BitBtn3: TBitBtn;
    procedure Timer1Timer(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure Jaune1Click(Sender: TObject);
    procedure Bleu1Click(Sender: TObject);
    procedure Violet1Click(Sender: TObject);
    procedure Vert1Click(Sender: TObject);
    procedure Rouge1Click(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);




  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;
   t,t1,t2,t3,t4,t5,t6,t7,t10,t11,t12,t13,t14,
   t15,t16,t20,t21,x,y,x1,y1,couleur,couleur1,choix:integer;
   dir:boolean;
   pause:boolean=true;
  implementation
{$R *.DFM}


procedure TForm1.Timer1Timer(Sender: TObject);       // prog principal

  begin

  with form1.Image1.canvas     do

  if (choix=1) and (pause) then

    begin


  //pen.color:=clblue;

   fillrect(clientrect) ;     // effacer les trace de deplacement a chaque iteration

  // sol

  pen.width:=20;       // epaisseur
  pen.Color:=clnavy;   // couleur

  MoveTo (0,400); LineTo(750,400); // ligne

  //bras de soudure gauche

  pen.width:=7;
  pen.Color:=clnavy;
  MoveTo (230,400); LineTo(230,320);

  pen.Color:=clpurple;
  pen.width:=5;
  x:=round(230+30*sin((t20+2700)/20));  // mvt
  y:=round(320+30*cos((t20+2700)/20));
  MoveTo (230,320); LineTo(x,y);

  pen.Color:=clred;
  pen.width:=3;
  MoveTo (x,y); LineTo(x+17,y+13);

  pen.Color:=clblack			;   // Teal
  brush.Color:=clwhite		;
  ellipse (225,315,235,325);
  ellipse (x-4,y-4,x+4,y+4);

 //bras de soudure droite

  pen.width:=7;
  brush.Color:=clwhite;
  pen.Color:=clnavy;
  MoveTo (332,400); LineTo(332,320);
  pen.Color:=clpurple;
  pen.width:=5;
  x:=round(332+30*sin((-t21+2704)/20));  // mvt
  y:=round(320+30*cos((-t21+2704)/20));
  MoveTo (332,320); LineTo(x,y);
  pen.Color:=clred;
  pen.width:=3;
  MoveTo (x,y); LineTo(x-20,y+12);
  pen.Color:=clblack			;   // Teal
  brush.Color:=clwhite		;
  ellipse (328,315,338,325);
  ellipse (x-4,y-4,x+4,y+4);


 //tapis haut

 pen.Color:=clblue;      // couleur
 pen.width:=3;          //epaisseur des traits

 ellipse (100,150,120,170);   //ellipse gauche
 x:=round(110+10*sin(-t1/20));  // mvt
 y:=round(160+10*cos(-t1/20));  // mvt

 pen.width:=2;

 MoveTo (110,160); LineTo(x,y);   //trait

 MoveTo (0,150); LineTo(110,150);   // ligne superieur
 MoveTo (0,170); LineTo(110,170);   // ligne inferieur

 //tapis bas gauche

 pen.Color:=clpurple;      // couleur
 pen.width:=3;             // epaisseur des traits

 ellipse (390,350,410,370);   // roue
 x1:=round(400+10*sin(-t1/20));  // variable
 y1:=round(360+10*cos(-t1/20));  // variable

 pen.width:=2;                   // epaisseur

 MoveTo (400,360); LineTo(x1,y1); // trait simulant la rotation du tapis
 MoveTo (0,350); LineTo(400,350); // ligne superieur
 MoveTo (0,370); LineTo(400,370); // ligne inferieur

 pen.Color:=clnavy;              // couleur
 pen.width:=3;                    // epaisseur
 MoveTo (400,360); LineTo(410,390);   // trait du suport droit
 MoveTo (400,360); LineTo(390,390);   // trait du suport gauche

 pen.Width:=1;
 pen.Color:=clwhite;
 brush.Color:=clwhite;
 rectangle (0,351,380,369) ;  // remplissage tapis

  // rails

 pen.Color:=cllime;          // couleur
 pen.width:=3;               // epaisseur
 MoveTo (0,0); LineTo(320,0);        //  rail haut HG
 MoveTo (0,20); LineTo(300,20);      //  rail haut bG
 MoveTo (320,0); LineTo(320,130);     // rail droit v
 MoveTo (300,20); LineTo(300,150);    // rail gauche v
 MoveTo (320,130); LineTo(750,130);   // rail bas hd
 MoveTo (300,150); LineTo(750,150);   // rail bas bd

  // tapis bas droite

 pen.Color:=clred;
 pen.width:=3;

 ellipse (640,350,660,370);    // roue
 x1:=round(650+10*sin(-t1/20));   // mvt
 y1:=round(360+10*cos(-t1/20));   // mvt

 pen.width:=2;

 MoveTo (650,360); LineTo(x1,y1);   // trait
 MoveTo (650,350); LineTo(750,350);
 MoveTo (650,370); LineTo(750,370);

 pen.Color:=clnavy;
 pen.width:=3;
 MoveTo (650,360); LineTo(660,390);   // suport
 MoveTo (650,360); LineTo(640,390);   // suport

 //grande piece       peinte grace a l epaisseur

 pen.Width:=1;
 pen.Color:=clgray;
 brush.Color:=clgray;

 rectangle (-60+t1,320,-40+t1,350) ;  // la + a gauche
 rectangle (50+t1,320,70+t1,350) ;    // 2eme
 rectangle (160+t1,320,180+t1,350) ;  // 3 eme
 rectangle (270+t1,320,290+t1,350) ;  // 4eme
 rectangle (380+t5+t1,320-t4,400+t5+t1,350-t4) ;   // piece en mvt


 //petite piece   peinte grace a brush.color

 pen.Width:=1;
 pen.Color:=clTeal;
 brush.Color:=clTeal;
 rectangle (-10+t1,140,0+t1,150) ;
 rectangle (100+t13+t1,140-t12+3*t14,110+t13+t1,150-t12+3*t14) ;
 rectangle (385+t5+t1,320-t4,395+t5+t1,310-t4) ;
 rectangle (275+t1,320,285+t1,310) ;

 // choix de la couleur du cycle

 if t=0 then couleur:=couleur1;

 // piece peinte

 if (t>couleur+200)and (t<1110) then begin

    if couleur=215 then begin   // rectangle couleur jaune

  pen.Width:=1;
  pen.Color:=clyellow;
  brush.Color:=clyellow;
  rectangle (380+t5+t1,320-t4,400+t5+t1,350-t4) ;
  rectangle (385+t5+t1,320-t4,395+t5+t1,310-t4) ;end;  // rectangle couleur

    if couleur=245 then begin   // rectangle couleur bleu

  pen.Width:=1;
  pen.Color:=clblue;
  brush.Color:=clblue;
  rectangle (380+t5+t1,320-t4,400+t5+t1,350-t4) ;
  rectangle (385+t5+t1,320-t4,395+t5+t1,310-t4) ;end;

    if couleur=275 then begin  // rectangle couleur violet

  pen.Width:=1;
  pen.Color:=clpurple;
  brush.Color:=clpurple;
  rectangle (380+t5+t1,320-t4,400+t5+t1,350-t4) ;
  rectangle (385+t5+t1,320-t4,395+t5+t1,310-t4) ;end;

    if couleur=305 then begin  // rectangle couleur vert

  pen.Width:=1;
  pen.Color:=clgreen;
  brush.Color:=clgreen;
  rectangle (380+t5+t1,320-t4,400+t5+t1,350-t4) ;
  rectangle (385+t5+t1,320-t4,395+t5+t1,310-t4) ;end;

    if couleur=335 then begin  // rectangle couleur rouge

  pen.Width:=1;
  pen.Color:=clred;
  brush.Color:=clred;
  rectangle (380+t5+t1,320-t4,400+t5+t1,350-t4) ;
  rectangle (385+t5+t1,320-t4,395+t5+t1,310-t4) ;end;
    end;



 //bras droit
 brush.Color:=clwhite;
 pen.Width:=1;
 pen.Color:=clblack;
 rectangle (370+t3+t5-t6,242+t2-t4,371+t3+t5-t6,262+t2-t4) ;
 rectangle (409-t3+t5-t6,242+t2-t4,410-t3+t5-t6,262+t2-t4) ;
 rectangle (370+t5-t6,237+t2-t4,410+t5-t6,242+t2-t4) ;
 rectangle (385+t5-t6,137+t2-t4,395+t5-t6,237+t2-t4) ;
 rectangle (380+t5-t6,137,400+t5-t6,227) ;

 pen.Width:=2;
 ellipse (410+t5-t6,130,430+t5-t6,150);  //roue droite
 ellipse (350+t5-t6,130,370+t5-t6,150);  //roue gauche

 pen.Width:=1;
 x:=round(420+t5-t6+10*sin((-t5+t6)/20));  // mvt
 y:=round(140+10*cos((-t5+t6)/20));  // mvt
 MoveTo (420+t5-t6,140); LineTo(x,y);

 x:=round(360+t5-t6+10*sin((-t5+t6)/20));  // mvt
 y:=round(140+10*cos((-t5+t6)/20));  // mvt
 MoveTo (360+t5-t6,140); LineTo(x,y);

 ellipse (355+t5-t6,130,425+t5-t6,150);
 MoveTo (360+t5-t6,140); LineTo(420+t5-t6,140);

 //bras haut gauche

 pen.Width:=1;
 pen.Color:=clblack;
 rectangle (95+t11+t13-t16,112+t10-t12+3*t14-3*t15,96+t11+t13-t16,120+t10-t12+3*t14-3*t15) ; //pince
 rectangle (114-t11+t13-t16,112+t10-t12+3*t14-3*t15,115-t11+t13-t16,120+t10-t12+3*t14-3*t15) ; //pince
 rectangle (90+t13-t16,107+t10-t12+3*t14-3*t15,120+t13-t16,112+t10-t12+3*t14-3*t15) ;
 rectangle (100+t13-t16,17,110+t13-t16,107+t10-t12+3*t14-3*t15) ;
 rectangle (95+t13-t16,10,115+t13-t16,105+2*t14-2*t15) ;
 rectangle (90+t13-t16,10,120+t13-t16,100+t14-t15) ;
 rectangle (85+t13-t16,10,125+t13-t16,95) ;


 pen.Width:=2;
 ellipse (65+t13-t16,0,85+t13-t16,20);  //roue droite
 ellipse (125+t13-t16,0,145+t13-t16,20);  //roue gauche

 pen.Width:=1;
 x:=round(135+t13-t16+10*sin((-t13+t16)/20));  // mvt
 y:=round(10+10*cos((-t13+t16)/20));  // mvt
 MoveTo (135+t13-t16,10); LineTo(x,y);

 x:=round(75+t13-t16+10*sin((-t13+t16)/20));  // mvt
 y:=round(10+10*cos((-t13+t16)/20));  // mvt
 MoveTo (75+t13-t16,10); LineTo(x,y);

 ellipse (70+t13-t16,0,140+t13-t16,20);
 MoveTo (75+t13-t16,10); LineTo(135+t13-t16,10);


 if (t=0) then begin t1:=0;t2:=0;t3:=0;t4:=0;t5:=0;t6:=0;t10:=0;t11:=0;t12:=0;t13:=0;t14:=0;t15:=0;t16:=0;end ;
 if (t>0)and(t<=65) then t2:=t2+1;  // descente
 if (t>65)and(t<=74) then t3:=t3+1;  //fermeture pince
 if (t>75)and(t<=140) then t4:=t4+1;  //monter avec piece
 if (t>140)and(t<=couleur) then t5:=t5+1;    //droite
 if (t>couleur)and(t<=couleur+80) then t4:=t4-1;   //descente avec piece
 if (t>couleur+200)and(t<=couleur+280) then t4:=t4+1;  //monter avec piece
 if (t>couleur+280)and(t<=715) then t5:=t5+1;   //droite
 if (t>715)and(t<=780) then t4:=t4-1;     //descente avec piece
 if (t>795)and(t<=804) then t3:=t3-1;   //ouverture pince
 if (t>805)and(t<=870) then t2:=t2-1;   //monter
 if (t>870)and(t<=1165) then t6:=t6+1;   //gauche
 if (t>990)and(t<=1100) then t1:=t1+1;   //avance tapis
 if (t>1166) then begin t5:=0;t1:=0;t2:=0;t3:=0;t4:=0;t6:=0;end;

 if (t>0)and(t<=25) then t10:=t10+1;  // descente pv
 if (t>35)and(t<=39) then t11:=t11+1;  //fermeture pince
 if (t>40)and(t<=50) then t12:=t12+1;  //monter avec piece
 if (t>50)and(t<=115) then t13:=t13+1;  //droite
 if (t>120)and(t<=180) then t14:=t14+1;  //descente gv + piece
 if (t>180)and(t<=184) then t11:=t11-1;  //ouverture pince
 if (t>185)and(t<=245) then t15:=t15+1;  // monte gv
 if (t>245)and(t<=260) then t10:=t10-1;  // monte pv
 if (t>260)and(t<=325) then t16:=t16+1;  // gauche
 if (t>1166) then begin t10:=0;t11:=0;t12:=0;t13:=0;t14:=0;t15:=0;t16:=0;end;

 if (t>0)and(t<=20) then t20:=t20-1;  //premiere soudure
 if (t>30)and(t<=50) then t20:=t20+1;
 if (t>70)and(t<=90) then t21:=t21-1;  //deuxieme soudure
 if (t>100)and(t<=120) then t21:=t21+1;
 if (t>140)and(t<=160) then t20:=t20-1;  //3eme soudure
 if (t>170)and(t<=190) then t20:=t20+1;
 if (t>200)and(t<=220) then t21:=t21-1;  //4eme soudure
 if (t>230)and(t<=250) then t21:=t21+1;


 // box de peinture

 pen.width:=29;
  pen.Color:=clyellow;
 rectangle (464,339,467,356) ;

 pen.width:=29;
  pen.Color:=clblue;
 rectangle (494,339,496,356) ;

 pen.width:=29;
  pen.Color:=clpurple;
 rectangle (524,339,526,356) ;

 pen.width:=29;
  pen.Color:=clgreen;
 rectangle (554,339,556,356) ;

 pen.width:=29;
  pen.Color:=clred;
 rectangle (584,339,586,356) ;

  pen.Width:=7  ;
  pen.Color:=clblack;
  MoveTo (450,370); LineTo(600,370); // fond bac
  pen.Width:=3  ;
  MoveTo (450,323); LineTo(450,370); //  exterieur gauche
  MoveTo (600,323); LineTo(600,370); // exterieur droite

 pen.Width:=1  ;
 pen.Color:=clblack;

 MoveTo (450,324); LineTo(450,370); // trait n0
 MoveTo (480,324); LineTo(480,370); // trait n1
 MoveTo (510,324); LineTo(510,370); // trait n2
 MoveTo (540,324); LineTo(540,370); // trait n3
 MoveTo (570,324); LineTo(570,370); // trait n4
 MoveTo (600,324); LineTo(600,370); // trait n5

 // support bac peinture

 brush.Color:=clblack;

 rectangle (450,370,470,390) ;  //450
 rectangle (490,370,510,390) ;  //500
 rectangle (540,370,560,390) ;   //550
 rectangle (580,370,600,390) ;   //600

 brush.Color:=clwhite;

 // etincelle de soudure

 if ((t>220) and (t<=230)) or ((t>90) and (t<=100))  //droite
 then

  begin
 pen.Width:=1  ;
 pen.Color:=clblue;
 MoveTo (292,305); LineTo(287,315); //1er etincelle
 MoveTo (290,300); LineTo(287,315);  // 2eme etincelle
 MoveTo (294,310); LineTo(287,315); // 3eme etincelle
  end;

 if ((t>20) and (t<=30)) or ((t>160) and (t<=170))   //gauche
 then

  begin
 pen.Width:=1  ;
 pen.Color:=clblue;
 MoveTo (272,305); LineTo(273,315);  //1er etincelle
 MoveTo (266,300); LineTo(273,315);  // 2eme etincelle
 MoveTo (262,310); LineTo(273,315);  // 3eme etincelle
  end;

 // choix de la couleur du cycle

 if t=0 then couleur:=couleur1;

     // timer

 if pause
 then
  if dir
           then t:=t+1
           else t:=0 ;
  end;
    if (t<=0)or(t>1166)
    then dir:= not dir;




end;

 // boutons

procedure TForm1.BitBtn1Click(Sender: TObject); // arret
begin
Form1.Close;
end;

procedure TForm1.BitBtn2Click(Sender: TObject);  // marche
begin
bitbtn3.Visible:=true;
bitbtn2.Visible:=false;
Label1.Visible:=false;
choix:=1;
couleur1:=215;
end;

procedure TForm1.Jaune1Click(Sender: TObject); //jaune menu deroulant
begin
couleur1:=215 ;
end;

procedure TForm1.Bleu1Click(Sender: TObject);  //bleu menu deroulant
begin
couleur1:=245 ;
end;

procedure TForm1.Violet1Click(Sender: TObject); //violet menu deroulant
begin
couleur1:=275 ;
end;

procedure TForm1.Vert1Click(Sender: TObject);   //vert menu deroulant
begin
couleur1:=305 ;
end;

procedure TForm1.Rouge1Click(Sender: TObject);  //rouge menu deroulant
begin
couleur1:=335 ;
end;


procedure TForm1.BitBtn3Click(Sender: TObject);
begin
pause:=not pause;
end;

end.




