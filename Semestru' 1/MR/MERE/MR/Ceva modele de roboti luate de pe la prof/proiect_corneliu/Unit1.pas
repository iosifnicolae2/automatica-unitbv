unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls, Buttons;

type
  TForm1 = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    Image1: TImage;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    BitBtn3: TBitBtn;
    BitBtn4: TBitBtn;
    BitBtn5: TBitBtn;
    tsd: TTimer;
    tjs: TTimer;
    procedure FormCreate(Sender: TObject);
    procedure deseneaza_robot();
    procedure BitBtn1Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
    procedure BitBtn4Click(Sender: TObject);
    procedure BitBtn5Click(Sender: TObject);
    procedure tjsTimer(Sender: TObject);
    procedure tsdTimer(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

type
   componenta=record
     posx:integer;
     posy:integer;
     height:integer;
     weight:integer;
   end;


var
  Form1: TForm1;
  posx:integer=0;
  posy:integer=0;
  dx:boolean=true;
  dy:boolean=true;
  piesa,baza,corp,brat1,brat2,brat3,preh:componenta;

implementation

{$R *.dfm}
procedure Tform1.deseneaza_robot();
begin
   image1.Canvas.Brush.Color:=clwhite;
   image1.Canvas.FillRect(clientrect);
   image1.Canvas.Brush.Color:=clred;
   baza.posx:=40;baza.posy:=image1.Height;
   image1.Canvas.rectangle(baza.posx,baza.posy,baza.posx+40,baza.posy-10);
   image1.Canvas.Brush.color:=clyellow;
   image1.Canvas.Rectangle(posx,image1.Height-posy,posx+100,image1.Height-posy-10);
   image1.Canvas.Brush.Color:=cllime;
   image1.Canvas.Rectangle(50,image1.Height-10,70,image1.Height-120);
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
   image1.Canvas.Brush.color:=clwhite;
   image1.Canvas.FillRect(clientrect);
   posx:=20;
   posy:=12;
   deseneaza_robot();
end;

procedure TForm1.BitBtn1Click(Sender: TObject);
begin
   tsd.Enabled:=true;
   dx:=false;
end;

procedure TForm1.BitBtn2Click(Sender: TObject);
begin
   tsd.Enabled:=true;
   dx:=true;
end;

procedure TForm1.BitBtn3Click(Sender: TObject);
begin
   tjs.Enabled:=true;
   dy:=false;
end;

procedure TForm1.BitBtn4Click(Sender: TObject);
begin
   tjs.Enabled:=true;
   dy:=true;
end;

procedure TForm1.BitBtn5Click(Sender: TObject);
begin
   tsd.Enabled:=false;
   tjs.Enabled:=false;
end;

procedure TForm1.tjsTimer(Sender: TObject);
begin
   if (((posy<14)and (not dy))or((posy>100)and dy))then tjs.Enabled:=false
   else
      if dy then
         begin
            posy:=posy+2;
            deseneaza_robot();
         end
      else
         begin
            posy:=posy-2;
            deseneaza_robot();
         end;
end;

procedure TForm1.tsdTimer(Sender: TObject);
begin
    if (((posx<22)and(not dx))or((posx>50)and dx)) then tsd.Enabled:=false
       else
          if dx then
             begin
                posx:=posx+1;
                deseneaza_robot();
             end
          else
             begin
                posx:=posx-1;
                deseneaza_robot();
             end;
end;

end.
