object Form1: TForm1
  Left = 192
  Top = 120
  Width = 553
  Height = 324
  Caption = 'Form1'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 336
    Top = 144
    Width = 201
    Height = 113
    BevelInner = bvSpace
    TabOrder = 0
    object BitBtn1: TBitBtn
      Left = 16
      Top = 8
      Width = 75
      Height = 25
      Caption = 'Stanga'
      TabOrder = 0
      OnClick = BitBtn1Click
    end
    object BitBtn2: TBitBtn
      Left = 112
      Top = 8
      Width = 75
      Height = 25
      Caption = 'Dreapta'
      TabOrder = 1
      OnClick = BitBtn2Click
    end
    object BitBtn3: TBitBtn
      Left = 16
      Top = 40
      Width = 75
      Height = 25
      Caption = 'Jos'
      TabOrder = 2
      OnClick = BitBtn3Click
    end
    object BitBtn4: TBitBtn
      Left = 112
      Top = 40
      Width = 75
      Height = 25
      Caption = 'Sus'
      TabOrder = 3
      OnClick = BitBtn4Click
    end
    object BitBtn5: TBitBtn
      Left = 64
      Top = 72
      Width = 75
      Height = 25
      Caption = 'Stop'
      TabOrder = 4
      OnClick = BitBtn5Click
    end
  end
  object Panel2: TPanel
    Left = 8
    Top = 8
    Width = 321
    Height = 273
    BevelInner = bvLowered
    TabOrder = 1
    object Image1: TImage
      Left = 16
      Top = 8
      Width = 305
      Height = 257
    end
  end
  object tsd: TTimer
    Enabled = False
    Interval = 20
    OnTimer = tsdTimer
    Left = 352
    Top = 256
  end
  object tjs: TTimer
    Enabled = False
    Interval = 20
    OnTimer = tjsTimer
    Left = 496
    Top = 256
  end
end
