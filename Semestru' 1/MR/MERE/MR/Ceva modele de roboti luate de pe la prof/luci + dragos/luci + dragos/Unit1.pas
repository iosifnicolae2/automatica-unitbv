unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls;

type
  TForm1 = class(TForm)
    Image1: TImage;
    Timer1: TTimer;
    Button1: TButton;
    Button2: TButton;
    procedure Timer1Timer(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;
  var t:integer;  // variabila folosita in prima faza ( miscare piesa pe banda, miscare macara SUS JOS)
  var x1_cerc,y1_cerc:integer; // folosite la raza cercului de pe BANDA
  var x1_cerc1,y1_cerc1:integer;
  implementation

{$R *.dfm}

procedure TForm1.Timer1Timer(Sender: TObject);
begin
with image1.Canvas do
  begin
  brush.color:=clwhite; // tot timpu sa umple fundalul cu alb
  fillrect(clientrect); //functie care face refresh la fiecare pas, aka curata ecranu

//desenez linia de sus de la conveyer belt
pen.Width:=2;
pen.Color:=clblue;
moveto(100,300);
lineto(300,300);

//desenez linia de jos de la conveyer belt
pen.Width:=2;
pen.Color:=clblue;
moveto(100,350);
lineto(300,350);

//desenez cercuri
pen.Width:=2;
pen.Color:=clblue;
Ellipse(275,300,325,350);
Ellipse(80,300,130,350);

//desenez macara 1 pentru piesa -LINIA
pen.Width:=4;
pen.Color:=clred;
moveto(0,40);lineto(650,40);
moveto(0,80);lineto(650,80);

//MISCARE PIESA PE BANDA >> DREAPTA
if (t<200) then
begin
//desenez piesa
  pen.Width:=2;
  pen.Color:=clgreen;
  brush.Color:=clgreen;
  rectangle(80+t,260,100+t,300);
//raza cerc care se misca de pe banda
  pen.Width:=2;
  pen.Color:=clblue;
  brush.Color:=clblue;
  x1_cerc := trunc(300 + 25*cos(t/42));
  y1_cerc := trunc(325 + 25*sin(t/42));
  moveto(300,325);
  lineto(x1_cerc,y1_cerc);
  x1_cerc1 := trunc(105 + 25*cos(t/42));
  y1_cerc1 := trunc(325 + 25*sin(t/42));
  moveto(105,325);
  lineto(x1_cerc1,y1_cerc1);
  //cleste macara
  pen.Color:=clblack;
  pen.Width:=6;
  moveto(280,176);
  lineto(299,176);
//desenez macara 1***
pen.Width:=4;
pen.Color:=clblue;
rectangle(250,44,325,77);
//cablu macara
pen.Color:=clblack;
moveto(289,80);
lineto(289,176);
end;

//PIESA PE BANDA >> STA
//MACARA COBOARA
if((t>=200) and (t<283)) then
begin
//redesenez piesa
  pen.Width:=2;
  pen.Color:=clgreen;
  brush.Color:=clgreen;
  rectangle(279, 260, 299,300);
//cablu de la macara
  pen.Width:=4;
  pen.Color:=clblack;
  moveto(289,80);
  lineto(289,176+t-202);
//cleste macara
  pen.Width:=6;
  moveto(280,176+t-200);
  lineto(299,176+t-200);
//raza cerc
  pen.Width:=2;
  pen.Color:=clblue;
  brush.Color:=clblue;
  moveto(300,325);
  lineto(300,300);
//desenez macara 1***
pen.Width:=4;
pen.Color:=clblue;
rectangle(250,44,325,77);

end;

//MACARA URCA
//PIESA URCA
if((t>=283) and (t<355)) then
begin
//redesenez piesa
  pen.Width:=2;
  pen.Color:=clgreen;
  brush.Color:=clgreen;
  rectangle(279, 260-t+280, 299,300-t+280);      //!!!!!
//redesenez cablu de la macara
  pen.Width:=2;
  pen.Color:=clblack;
  moveto(289,80);
  lineto(289,176-t+356);
//redesenez cleste macara
  pen.Width:=6;
  moveto(280,80-t+360);
  lineto(299,176-t+360);
//raza cerc
  pen.Width:=2;
  pen.Color:=clblue;
  brush.Color:=clblue;
  moveto(300,325);
  lineto(300,300);
//desenez macara 1***
  pen.Width:=4;
  pen.Color:=clblue;
rectangle(250,44,325,77);
end;

//MACARA SUS
//PIESA SUS
if((t>=355) and (t<400)) then
begin
//redesenez piesa
  pen.Width:=2;
  pen.Color:=clgreen;
  brush.Color:=clgreen;
  rectangle(279, 260-354+280, 299,300-354+280);
//redesenez cablu de la macara
  pen.Width:=2;
  pen.Color:=clblack;
  moveto(289,174);
  lineto(289,178-354+356);
//redesenez cleste macara
  pen.Width:=6;
  moveto(280,176-354+360);
  lineto(299,176-354+360);
//raza cerc
  pen.Width:=2;
  pen.Color:=clblue;
  brush.Color:=clblue;
  moveto(300,325);
  lineto(300,300);
//desenez macara 1***
pen.Width:=4;
pen.Color:=clblue;
rectangle(250,44,325,77);
end;

//MACARA SUS
//PESA SUS
//MACARA + PIESA SE MISCA >>> DREAPTA
if((t>=400) and (t<517)) then
begin
//redesenez piesa
  pen.Width:=2;
  pen.Color:=clgreen;
  brush.Color:=clgreen;
  rectangle(279-400+t, 186, 299-400+t,226);
//redesenez cablu de la macara
  pen.Width:=2;
  pen.Color:=clblack;
  moveto(289-400+t,174);
  lineto(289-400+t,178);
//redesenez cleste macara
  pen.Width:=6;
  moveto(280-400+t,182);
  lineto(299-400+t,182);
//raza cerc
  pen.Width:=2;
  pen.Color:=clblue;
  brush.Color:=clblue;
  moveto(300,325);
  lineto(300,300);
//desenez macara 1*** si misc spre DREAPTA
  pen.Width:=4;
  pen.Color:=clblue;
  rectangle(250-400+t,150,325-400+t,175);
end;

//MACARA SUS
//PESA SUS
//MACARA + PIESA AJUNSE IN DREAPTA si STAU
// fata de if-ul anterior, unde era variabila t=516
if((t>=517) and (t<550)) then
begin
//redesenez piesa
  pen.Width:=2;
  pen.Color:=clgreen;
  brush.Color:=clgreen;
  rectangle(395, 186, 415,226);
//redesenez cablu de la macara
  pen.Width:=2;
  pen.Color:=clblack;
  moveto(405,174);
  lineto(405,178);
//redesenez cleste macara
  pen.Width:=6;
  moveto(396,182);
  lineto(415,182);
//raza cerc
  pen.Width:=2;
  pen.Color:=clblue;
  brush.Color:=clblue;
  moveto(300,325);
  lineto(300,300);
//desenez macara 1*** si misc spre DREAPTA
  pen.Width:=4;
  pen.Color:=clblue;
  rectangle(366,150,441,175);
end;

//MACARA SUS
//PESA SUS
//MACARA + PIESA AJUNSE IN DREAPTA si COBOARA
// fata de if-ul anterior, unde era variabila t=549
if((t>=550) and (t<600)) then
begin
//redesenez piesa
  pen.Width:=2;
  pen.Color:=clgreen;
  brush.Color:=clgreen;
  rectangle(395, 186, 415,226);
//redesenez cablu de la macara
  pen.Width:=2;
  pen.Color:=clblack;
  moveto(405,174);
  lineto(405,178);
//redesenez cleste macara
  pen.Width:=6;
  moveto(396,182);
  lineto(415,182);
//raza cerc
  pen.Width:=2;
  pen.Color:=clblue;
  brush.Color:=clblue;
  moveto(300,325);
  lineto(300,300);
//desenez macara 1*** si misc spre DREAPTA
  pen.Width:=4;
  pen.Color:=clblue;
  rectangle(366,150,441,175);
end;


t:=t+1;

//opresc piesa
if (t=699) then
begin
  timer1.enabled:=false;
end;


//dispozitiv care produce piese
{pen.Width:=1;
pen.Color:=clyellow;
brush.color:=clyellow;
rectangle(0,200,100,400);   }

//dispozitiv STATIV pe care am sa asez piesa !
pen.Width:=1;
pen.Color:=clred;
brush.color:=clyellow;
rectangle(370,380,440,400);

//podea aka un adevarat sistem informatic complex
pen.Width:=1;
pen.Color:=clgray;
brush.color:=clgray;
rectangle(0,400,700,600);

  end

  end;

procedure TForm1.Button1Click(Sender: TObject);
begin
if(timer1.enabled) then
  begin
  timer1.Enabled:= false;
  button1.caption:='resume';
  end
    else
  begin
  timer1.Enabled:= true;
  button1.caption:='pauza';
  end

    end;

procedure TForm1.Button2Click(Sender: TObject);
begin
t:=0;
end;

end.
