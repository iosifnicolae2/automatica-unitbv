unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls;

type
  TForm1 = class(TForm)
    Timer1: TTimer;
    Image1: TImage;
  procedure Timer1Timer(Sender:TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;
  t,x1,y1,x2,y2,x11,y11,x12,y12,xp,yp,xp1,yp1,xp2,yp2,xp3,yp3,xp4,yp4:integer;
  directie: boolean=false;
  dx1:integer=3;
  dy1:integer=5;
  x0:integer=145;
  y0:integer=90;
  xm:integer=150;
  ym:integer=270;
  xn:integer=125;
  yn:integer=200;
  xo:integer=150;
  yo:integer=280;
  xt:integer=120;
  yt:integer=380;
  xr:integer=140;
  yr:integer=200;
  i:integer=-50;


const

  tmax = 60;
  raza = 20;
  raza1 = 1;
  raza2 = 15;
  raza3 = 10;
  raza4 = 6;
  raza5 = 4;

implementation

  procedure TForm1.Timer1Timer(Sender:TObject);
    begin
      With form1.Image1.Canvas do
      begin


      if(i>50) then i:=-50;

        fillrect(clientrect);


        pen.width:=8;
        brush.color:=clgreen;
        ellipse(280,200,70,300);
        pen.width:=6;


        moveto(175,206);
        lineto(x1+120,y1);
        x1:=round(x0+raza*cos(t/10));
        y1:=round(y0+raza*sin(t/10));
        pen.width:=16;
        xp1:=round(xn+raza1*cos(t/10));
        yp1:=round(yn+raza1*sin(t/10));
        moveto(175,206);
        lineto(xp1+57,yp1);
        pen.width:=10;
        pen.Color:=clgray;
        xp4:=round(xr+raza5*cos(t/10));
        yp4:=round(yr+raza5*sin(t/10));
        moveto(175,206);
        lineto(xp4+48,yp4-10);
        pen.width:=8;


           if(t>0)and(t<=100) then
        inc(dx1);
        moveto(x0,y0);
        x11:=x0+dx1;
        y11:=y0;

          if(t>100) and (t<=200) then
        dec(dy1);
        moveto(x11,y11);
        x12:=x11;
        y12:=y11-dy1;

        moveto(170,206);
        lineto(x2-55,y2);
        x2:=round(x0+raza*cos(t/10));
        y2:=round(y0+raza*sin(t/10));
        pen.width:=16;
        xp1:=round(xn+raza1*cos(t/10));
        yp1:=round(yn+raza1*sin(t/10));
        moveto(170,206);
        lineto(xp1+40,yp1);
        pen.width:=10;
        xp4:=round(xr+raza5*cos(t/10));
        yp4:=round(yr+raza5*sin(t/10));
        moveto(170,206);
        lineto(xp4+17,yp4-12);
        pen.width:=6;


        moveto(227,285);
        lineto(x1+30,y1);
        pen.width:=16;
        xp:=round(xm+raza1*cos(t/10));
        yp:=round(ym+raza1*sin(t/10));
        moveto(227,285);
        lineto(xp+73,yp);
        pen.width:=10;
        xp2:=round(xt+raza5*cos(t/10));
        yp2:=round(yt+raza5*sin(t/10));
        moveto(227,285);
        lineto(xp2+99,yp2-125);
        pen.width:=6;



        moveto(120,280);
        lineto(x2+30,y2);
        pen.width:=16;
        xp:=round(xm+raza1*cos(t/10));
        yp:=round(ym+raza1*sin(t/10));
        moveto(120,280);
        lineto(xp-27,yp);
        pen.width:=10;
        xp3:=round(xo+raza4*cos(t/10));
        yp3:=round(yo+raza4*sin(t/10));
        moveto(120,280);
        lineto(xp2+8,yp2-125);
        pen.width:=6;


        moveto(115,277);
        lineto(x2-55,y2);
        pen.width:=16;
        xp:=round(xm+raza1*cos(t/10));
        yp:=round(ym+raza1*sin(t/10));
        moveto(115,277);
        lineto(xp-37,yp);
        pen.width:=10;
        xp3:=round(xo+raza4*cos(t/10));
        yp3:=round(yo+raza4*sin(t/10));
        moveto(115,277);
        lineto(xp2-8,yp2-125);
        pen.width:=6;


        moveto(232,283);
        lineto(x1+120,y1);
        pen.width:=16;
        xp:=round(xm+raza1*cos(t/10));
        yp:=round(ym+raza1*sin(t/10));
        moveto(232,283);
        lineto(xp+85,yp);
        pen.width:=10;
        xp2:=round(xt+raza5*cos(t/10));
        yp2:=round(yt+raza5*sin(t/10));
        moveto(232,283);
        lineto(xp2+118,yp2-128);


        pen.width:=7;
        brush.color:=clyellow;
        ellipse(x1-70,y1-i,x1+150,y1+i);
        brush.color:=clwhite;

        if directie then t:=t-2
                    else t:=t+2;

        if(t=tmax) or (t=0)
                   then directie:=not directie;
        i:=i+1;


        end;
    end;


{$R *.dfm}

end.

