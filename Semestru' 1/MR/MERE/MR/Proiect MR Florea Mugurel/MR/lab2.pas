unit lab2;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls;

type
  TForm1 = class(TForm)
    Timer1: TTimer;
    Image1: TImage;
    procedure Timer1Timer(Sender:TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;
  toth,q1,q2,q3,x1s,y1s,x2s,y2s,x1d,y1d,x2d,y2d,x1ds,y1ds,x2ds,y2ds,
  x3,y3,x4,y4,a,b,c,d,e,f,g,h,i,j,k,l,m,n,p,r,s,t,u,v,w:integer;
  directie:boolean=true;
const
  tothmax=4460;
implementation
procedure TForm1.Timer1Timer(Sender:TObject);
  begin

    with form1.Image1.Canvas do
      begin

      //stergere elemente mutate
      fillrect(clientrect);

      //poligon incadrare
      pen.Width:=2;
      pen.Color:=clblack;
      brush.color:=rgb(255,255,100);
      polygon([point(0,0),point(1265,0),point(1265,737),point(0,737),point(0,0)]);

      //podea
      brush.color:=rgb(139,69,19);
      rectangle(1,707,1264,737);
      brush.color:=clwhite;

      //masa incarcare baterie cu acid
      pen.Width:=2;
      brush.color:=clblack;
      polygon([point(540,615),point(560,615),point(560,680),point(580,690),point(580,707),point(520,707),point(520,690),point(540,680)]);
      polygon([point(650,615),point(670,615),point(670,680),point(690,690),point(690,707),point(630,707),point(630,690),point(650,680)]);
      brush.color:=clblue;
      polygon([point(500,550),point(750,550),point(720,610),point(470,610),point(500,550)]);
      rectangle(470,610,720,630);
      polygon([point(720,610),point(750,550),point(750,570),point(720,629),point(720,610)]);
      brush.color:=clwhite;

      //suport robot
      pen.Color:=clblack;
      pen.Width:=2;
      brush.color:=rgb(255,100,10);
      rectangle(330,430,350,706);
      rectangle(320,580,360,706);
      rectangle(420+f,427,530+f,443);
      rectangle(130,425,533,445);
      brush.color:=clwhite;

      //banda rulanta stanga
      pen.Color:=clblack;
      pen.width:=2;
      brush.color:=clgray;
      polygon([point(200,610),point(230,550),point(237,549),point(243,555),point(209,623),point(200,610)]);
      polygon([point(0,550),point(230,550),point(200,610),point(-1,610),point(0,610)]);
      brush.color:=clwhite;
      ellipse (189,609,211,631);
      x1s:=round(200-10*sin(-q1/10));
      y1s:=round(620-10*cos(-q1/10));
      x2s:=round(200+10*sin(-q1/10));
      y2s:=round(620+10*cos(-q1/10));
      moveto(200,620);lineto(x1s,y1s);
      moveto(200,620);lineto(x2s,y2s);
      moveto(0,630);lineto(200,630);
      moveto(-100+q1,610);lineto(-70+q1,550);
      moveto(-40+q1,610);lineto(-10+q1,550);
      moveto(20+q1,610);lineto(50+q1,550);
      moveto(80+q1,610);lineto(110+q1,550);
      moveto(140+q1,610);lineto(170+q1,550);

      //suport banda rulanta stanga
      brush.color:=clblack;
      polygon([point(100,615),point(120,615),point(120,680),point(140,690),point(140,707),point(80,707),point(80,690),point(100,680)]);
      brush.color:=clwhite;

      //banda rulanta dreapta
      pen.Color:=clblack;
      pen.width:=2;
      brush.Color:=clgray;
      polygon([point(1000,610),point(1265,610),point(1265,550),point(1030,550),point(1000,610)]);
      polygon([point(990,615),point(1020,555),point(1030,550),point(1000,610),point(990,615)]);
      brush.Color:=clwhite;
      ellipse (989,609,1011,631);
      x1d:=round(1000-10*sin(-q2/10));
      y1d:=round(620-10*cos(-q2/10));
      x2d:=round(1000+10*sin(-q2/10));
      y2d:=round(620+10*cos(-q2/10));
      moveto(1000,620);lineto(x1d,y1d);
      moveto(1000,620);lineto(x2d,y2d);
      moveto(1000,630);lineto(1265,630);

      //suport banda rulanta dreapta
      brush.color:=clblack;
      polygon([point(1100,615),point(1120,615),point(1120,680),point(1140,690),point(1140,707),point(1080,707),point(1080,690),point(1100,680)]);
      brush.color:=clwhite;

      //banda rulanta dreapta  sus
      pen.Color:=clblack;
      pen.width:=2;
      brush.Color:=clgray;
      polygon([point(1000,310),point(1265,310),point(1265,250),point(1030,250),point(1000,310)]);
      polygon([point(990,315),point(1020,255),point(1030,250),point(1000,310),point(990,315)]);
      brush.Color:=clwhite;
      ellipse (989,309,1011,331);
      x1ds:=round(1000-10*sin(q3/10));
      y1ds:=round(320-10*cos(q3/10));
      x2ds:=round(1000+10*sin(q3/10));
      y2ds:=round(320+10*cos(q3/10));
      moveto(1000,320);lineto(x1ds,y1ds);
      moveto(1000,320);lineto(x2ds,y2ds);
      moveto(1000,330);lineto(1265,330);
      moveto(1060-q3,310);lineto(1090-q3,250);
      moveto(1120-q3,310);lineto(1150-q3,250);
      moveto(1180-q3,310);lineto(1210-q3,250);
      moveto(1240-q3,310);lineto(1270-q3,250);
      moveto(1300-q3,310);lineto(1330-q3,250);

      //brat antropomorf
      pen.Width:=20;
      pen.Color:=rgb(255,100,10);;
      moveto(890,700);lineto(890,450);

      pen.color:=rgb(255,120,0);
      x3:=round(890-100*cos(g/60));
      y3:=round(450-100*sin(g/60));
      moveto(890,450);lineto(x3,y3);

      pen.color:=rgb(255,100,10);
      x4:=round(x3-(80-k)*cos(h/60));
      y4:=round(y3-(80-k)*sin(h/60));
      moveto(x3,y3);lineto(x4,y4);

      //prehensor incheietura  brat robot
      pen.Width:=2;
      pen.Color:=clblack;
      brush.color:=rgb(255,100,10);
      rectangle(620+x4-625+k div 10,470+y4-470,630+x4-625+k div 10,490+y4-470);
      rectangle(585+x4-625+k div 10,490+y4-470,665+x4-625+k div 10,500+y4-470);

      //deget stanga brat robot
      pen.Width:=6;
      moveto(595+n+x4-625+k div 10,500+y4-470);lineto(585+n+x4-625+k div 10,520+y4-470);
      moveto(585+n+x4-625+k div 10,520+y4-470);lineto(595+n+x4-625+k div 10,540+y4-470);

      //deget dreapta  brat robot
      moveto(655-n+x4-625+k div 10,500+y4-470);lineto(665-n+x4-625+k div 10,520+y4-470);
      moveto(665-n+x4-625+k div 10,520+y4-470);lineto(655-n+x4-625+k div 10,540+y4-470);

      //capace de pe banda
      pen.Width:=2;
      brush.color:=rgb(0,255,0);

      rectangle(1080-q3-w+l+q2,275-v+m,1085-q3-w+l+q2,290-v+m);
      rectangle(1140-q3,275,1145-q3,290);
      rectangle(1200-q3,275,1205-q3,290);
      rectangle(1260-q3,275,1265-q3,290);

      rectangle(1092-q3-w+l+q2,275-v+m,1097-q3-w+l+q2,290-v+m);
      rectangle(1152-q3,275,1157-q3,290);
      rectangle(1212-q3,275,1217-q3,290);
      rectangle(1272-q3,275,1277-q3,290);

      rectangle(1104-q3-w+l+q2,275-v+m,1109-q3-w+l+q2,290-v+m);
      rectangle(1164-q3,275,1169-q3,290);
      rectangle(1224-q3,275,1229-q3,290);
      rectangle(1284-q3,275,1289-q3,290);

      rectangle(1116-q3-w+l+q2,275-v+m,1121-q3-w+l+q2,290-v+m);
      rectangle(1176-q3,275,1181-q3,290);
      rectangle(1236-q3,275,1241-q3,290);
      rectangle(1296-q3,275,1301-q3,290);

      brush.color:=clwhite;

      //bateria de pe banda
      pen.width:=2;
      brush.color:=clyellow;
      rectangle(-90+q1,560,-40+q1,600);
      rectangle(-30+q1,560,20+q1,600);
      rectangle(30+q1,560,80+q1,600);
      rectangle(90+q1+d+l+q2,560-c+m,140+q1+d+l+q2,600-c+m);
      brush.color:=clwhite;
      rectangle(-80+q1,570,-75+q1,600);
      rectangle(-20+q1,570,-15+q1,600);
      rectangle(40+q1,570,45+q1,600);
      rectangle(100+q1+d+l+q2,570-c+m,105+q1+d+l+q2,600-c+m);
      brush.Color:=clred;
      rectangle(100+q1+d+l+q2,600-c+m-r div 2,105+q1+d+l+q2,600-c+m);

      //prehensor incheietura
      brush.color:=rgb(255,100,10);
      rectangle(170+d-e,444,180+d-e,480+a);
      rectangle(135+d-e,480+a,215+d-e,490+a);
      brush.color:=clwhite;

      //deget stanga
      pen.Width:=6;
      moveto(145+b+d-e,490+a);lineto(135+b+d-e,510+a);
      moveto(135+b+d-e,510+a);lineto(145+b+d-e,530+a);

      //deget dreapta
      moveto(205-b+d-e,490+a);lineto(215-b+d-e,510+a);
      moveto(215-b+d-e,510+a);lineto(205-b+d-e,530+a);

      //robot umplere
      pen.Width:=2;
      rectangle(0,0,200,100);
      brush.color:=clred;
      rectangle(199,90,630,100);
      rectangle(0,20+r div 4,200,100);
      brush.color:=clgray;
      rectangle(0,100,1265,140);
      rectangle(305+p,250+i+j,345+p,260+i+j);
      rectangle(320+p,210,330+p,250+i+j);
      rectangle(310+p,160,340+p,210+i);
      rectangle(300+p,100,350+p,160);
      polygon([point(305+p,260+i+j),point(310+p,268+i+j),point(315+p,260+i+j),
               point(315+p,260+i+j),point(320+p,268+i+j),point(325+p,260+i+j),
               point(325+p,260+i+j),point(330+p,268+i+j),point(335+p,260+i+j),
               point(335+p,260+i+j),point(340+p,268+i+j),point(344+p,260+i+j)]);
      brush.Color:=clwhite;

      //brat montare capac
      pen.Width:=2;
      brush.color:=clgray;
      rectangle(800+s,250+t+u,850+s,255+t+u);
      rectangle(820+s,210+t,830+s,250+t+u);
      rectangle(810+s,160,840+s,210+t);
      rectangle(800+s,100,850+s,160);
      brush.Color:=clwhite;

      //temporizare miscari
      if (toth>0) and (toth<=60) then
          q1:=q1+1;
      if (toth>60) and (toth<=110) then
          a:=a+1;
      if (toth>110) and (toth<=115) then
          b:=b+1;
      if (toth>115) and (toth<=165) then
      begin
          a:=a-1;
          c:=c+1;
      end;
      if (toth>165) and (toth<=615) then
          begin
          d:=d+1;
          if d>345 then
             f:=f+1;
          end;
      if (toth>615) and (toth<=665) then
      begin
           a:=a+1;
           c:=c-1;
      end;
      if (toth>665) and (toth<=670) then
          b:=b-1;
      if (toth>670) and (toth<=720) then
          a:=a-1;
      if (toth>720) and (toth<=1170) then
          begin
          e:=e+1;
          if (e>1) and (e<110)then
            f:=f-1;
          if (e>1) and (e<300) then
            p:=p+1;
          if (e>300) and (e<440) then
            i:=i+1;
          end;
      if (toth>1170)and(toth<=1320) then
          j:=j+1;
      if (toth>1320)and(toth<=1380) then
          r:=r+1;
      if (toth>1390)and(toth<=1540)then
          j:=j-1;
      if(toth>1540)and(toth<=1680)then
          i:=i-1;
      if(toth>1680)and(toth<=1980)then
          p:=p-1;
      if(toth>1980)and(toth<=2040)then
          q3:=q3+1;
      if(toth>2040)and(toth<=2255)then
          s:=s+1;
      if(toth>2255)and(toth<=2285)then
          t:=t+1;
      if(toth>2285)and(toth<=2315)then
          begin
          t:=t-1;
          v:=v+1;
          end;
      if(toth>2315)and(toth<=2730)then
          begin
          s:=s-1;
          w:=w+1
          end;
      if(toth>2730)and(toth<=2870)then
          begin
          t:=t+1;
          v:=v-1
          end;
      if(toth>2870)and(toth<=3040)then
          begin
          u:=u+1;
          v:=v-1;
          end;
      if(toth>3050)and(toth<=3220)then
          u:=u-1;
      if(toth>3220)and(toth<=3360)then
          t:=t-1;
      if(toth>3360)and(toth<=3560)then
          s:=s+1;
      if (toth>3560) and (toth<=3640) then
          k:=k-1;
      if (toth>3640) and (toth<=3660) then
          h:=h-1;
       if (toth>3660) and (toth<=3665) then
          n:=n+1;
      if (toth>3665) and (toth<=3685) then
          begin
          h:=h+1;
          l:=x4-625;
          m:=y4-510;
          end;
      if (toth>3685) and (toth<=3760) then
          begin
          k:=k+1;
          l:=x4-625;
          m:=y4-510;
          end;
      if (toth>3760) and (toth<=3855) then
          begin
          g:=g+1;
          l:=x4-625;
          m:=y4-510;
          end;
      if (toth>3855) and (toth<=4055) then
          begin
          h:=h+1;
          l:=x4-625;
          m:=y4-510;
          end;
      if (toth>4055) and (toth<=4155) then
          begin
          g:=g+1;
          l:=x4-625;
          m:=y4-510;
          end;
      if (toth>4155) and (toth<=4185) then
          begin
          h:=h+1;
          l:=x4-625;
          m:=y4-510;
          end;
      if (toth>4185) and (toth<=4190) then
         begin
         n:=n-1;
         q2:=q2+1;
         end;
      if (toth>4190) and (toth<=4385) then
         begin
         h:=h-1;
         g:=g-1;
         q2:=q2+1;
         end;
      if (toth>4390) and (toth<=4425) then
         begin
         h:=h-1;
         q2:=q2+1;
         if q2>230 then
            k:=k+1;
         end;
      if (toth>4450) then
        begin
            q1:=0;q2:=0;q3:=0;a:=0;b:=0;c:=0;d:=0;e:=0;f:=0;g:=0;h:=0;i:=0;j:=0;k:=0;l:=0;m:=0;n:=0;p:=0;r:=0;s:=0;t:=0;u:=0;v:=0;w:=0;
        end;
      if directie then
          toth:=toth+1
      else
          toth:=0;
      if (toth=tothmax)or(toth=0) then
         directie:=not directie;
      end;
      end;
   {$R *.dfm}
end.

