object Form1: TForm1
  Left = 71
  Top = 186
  Width = 1078
  Height = 606
  Caption = 'Form1'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Image1: TImage
    Left = -8
    Top = 8
    Width = 1041
    Height = 553
    AutoSize = True
    Center = True
    OnClick = Timer1Timer
  end
  object BitBtn1: TBitBtn
    Left = 32
    Top = 48
    Width = 75
    Height = 25
    Caption = 'Start'
    TabOrder = 0
    OnClick = BitBtn2click
  end
  object BitBtn2: TBitBtn
    Left = 112
    Top = 48
    Width = 75
    Height = 25
    Caption = 'Stop'
    TabOrder = 1
    OnClick = BitBtn2click
  end
  object ScrollBar1: TScrollBar
    Left = 8
    Top = 8
    Width = 249
    Height = 17
    PageSize = 0
    TabOrder = 2
    OnChange = Timer1Timer
  end
  object Timer1: TTimer
    Interval = 10
    OnTimer = Timer1Timer
    Left = 192
    Top = 48
  end
end
