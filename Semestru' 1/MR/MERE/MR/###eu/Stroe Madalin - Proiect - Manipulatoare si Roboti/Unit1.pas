unit Unit1;
interface
uses
   Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls;
type
  TForm1 = class(TForm)
    Image1: TImage;
    Timer1: TTimer;
    Button1: TButton;
    Button2: TButton;
    Button3: TButton;
    procedure Timer1Timer(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

   t:integer=0;

    A11:integer=0;
    A12:integer=0;
    A13:integer=0;
    A14:integer=0;
    A15:integer=0;
    A16:integer=0;
    A17:integer=0;
    A18:integer=0;
    A19:integer=0;
    A20:integer=0;
    A21:integer=0;
    A23:integer=0;
    A22:integer=0;
    A24:integer=0;
    A25:integer=0;
    A26:integer=0;
    A27:integer=0;
    A28:integer=0;
    A29:integer=0;
    A30:integer=0;
    A31:integer=0;
    B11:integer=0;
    B12:integer=0;
    B13:integer=0;
    B14:integer=0;
    B15:integer=0;
    B16:integer=0;
    B17:integer=0;
    B18:integer=0;
    B19:integer=0;
    B20:integer=0;
    B21:integer=0;
    B23:integer=0;
    B22:integer=0;
    B24:integer=0;
    B25:integer=0;
    B26:integer=0;
    B27:integer=0;
    B28:integer=0;
    B29:integer=0;
    B30:integer=0;
    B31:integer=0;
    B32:integer=0;
    C11:integer=0;
    C12:integer=0;
    C13:integer=0;
    C14:integer=0;
    C15:integer=0;
    C16:integer=0;
    C17:integer=0;
    C18:integer=0;
    C19:integer=0;
    C20:integer=0;
    C21:integer=0;
    C23:integer=0;
    C22:integer=0;
    C24:integer=0;
    C25:integer=0;
    C26:integer=0;
    C27:integer=0;
    C28:integer=0;
    C29:integer=0;
    C30:integer=0;
    C31:integer=0;
    C32:integer=0;

    D11:integer=0;
    D12:integer=0;
    D13:integer=0;
    D14:integer=0;
    D15:integer=0;
    D16:integer=0;
    D17:integer=0;
    D18:integer=0;
    D19:integer=0;
    D20:integer=0;
    D21:integer=0;
    D23:integer=0;
    D22:integer=0;
    D24:integer=0;
    D25:integer=0;
    D26:integer=0;
    D27:integer=0;
    D28:integer=0;
    D29:integer=0;
    D30:integer=0;
    D31:integer=0;
    D32:integer=0;
    x:integer=0;
    z:integer=0;
    h:integer=0;
    z1:integer=0;
    h1:integer=0;
   h2:integer=0;
   z2:integer=0;
   h3:integer=0;
   z3:integer=0;
   h4:integer=0;
   z4:integer=0;
   z5:integer=0;
   h5:integer=0;
   z6:integer=0;
   h6:integer=0;
   z7:integer=0;
   h7:integer=0;
   y:integer=0;
   ales:integer;
   x1,y1,x2,y2,x3,y3,x4,y4,x5,y5,x6,y6,x7,y7:integer;
   directie:boolean=false;
implementation
{$R *.DFM}
procedure TForm1.Timer1Timer(Sender: TObject);
  begin
  with form1.Image1.canvas     do
    begin
   fillrect(clientrect);

  pen.Width:=4;
  brush.Color:=clgray;
   polygon([point(615,395),point(625,385),point(0,385),point(0,395)]);
   brush.color:=clwhite;

//banda transportoare

pen.color:=clred;
pen.width:=4;
brush.color:=clred;
roundrect(-20,340,640,365,28,28);
brush.Color:=clwhite;

roundrect(-20,350,630,375,28,28);
brush.color:=clred;
polygon([point(0,340),point(633,340),point(620,350),point(0,350)]);

brush.Color:=clwhite;
pen.color:=clblack;
pen.width:=3;
ellipse(50,350,77,375);
x:=round(63+13*sin((-A12-A19-B11-B13-B20-C11-C13-C20-D11-D13-D20)/15));
y:=round(362+13*cos((-A12-A19-B11-B13-B20-C11-C13-C20-D11-D13-D20)/15));
MoveTo(63,362);LineTo(x,y);

MoveTo(x,y);LineTo(63,362);
pen.color:=clblack;
MoveTo(77,365);LineTo(90,352);

ellipse(182,350,209,375);
x:=round(195+13*sin((-A12-A19-B11-B13-B20-C11-C13-C20-D11-D13-D20)/15));
y:=round(362+13*cos((-A12-A19-B11-B13-B20-C11-C13-C20-D11-D13-D20)/15));
MoveTo(195,362);LineTo(x,y);

MoveTo(x,y);LineTo(195,362);
pen.color:=clblack;
MoveTo(209,365);LineTo(222,352);

ellipse(315,350,342,375);
x:=round(328+13*sin((-A12-A19-B11-B13-B20-C11-C13-C20-D11-D13-D20)/15));
y:=round(362+13*cos((-A12-A19-B11-B13-B20-C11-C13-C20-D11-D13-D20)/15));
MoveTo(328,362);LineTo(x,y);

MoveTo(x,y);LineTo(328,362);
pen.color:=clblack;
MoveTo(342,365);LineTo(355,352);

ellipse(459,350,486,375);
x:=round(472+13*sin((-A12-A19-B11-B13-B20-C11-C13-C20-D11-D13-D20)/15));
y:=round(362+13*cos((-A12-A19-B11-B13-B20-C11-C13-C20-D11-D13-D20)/15));
MoveTo(472,362);LineTo(x,y);

MoveTo(x,y);LineTo(472,362);
pen.color:=clblack;
MoveTo(486,365);LineTo(499,352);


ellipse(602,350,629,375);
x:=round(615+13*sin((-A12-A19-B11-B13-B20-C11-C13-C20-D11-D13-D20)/15));
y:=round(362+13*cos((-A12-A19-B11-B13-B20-C11-C13-C20-D11-D13-D20)/15));
MoveTo(615,362);LineTo(x,y);

MoveTo(x,y);LineTo(615,362);

pen.Width:=8;
moveto(63,362);lineto(63,395);
moveto(195,362);lineto(195,395);
moveto(328,362);lineto(328,395);
moveto(472,362);lineto(472,395);
moveto(615,362);lineto(615,395);

pen.Width:=7;
brush.Color:=clblue;
rectangle(-10,395,615,600);
pen.Width:=4;
brush.Color:=clgray;
polygon([point(615,395),point(625,385),point(625,600),point(615,600)]);

brush.Color:=clwhite;


//mediu

pen.Width:=7;
pen.color:=clblack;
brush.Color:=clblue;
rectangle(-5,-5,615,205);
pen.Width:=4;
brush.Color:=clgray;
polygon([point(615,205),point(625,195),point(625,0),point(615,0)]);
brush.Color:=clwhite;


// sina brat 3

pen.Width:=3;
brush.Color:=clblue;
rectangle(535,260,700,277);
brush.Color:=clwhite;
   pen.Width:=7;
moveto(614,258); lineto(614,205);


//brat 3
 pen.Width:=3;
 brush.Color:=clyellow;
rectangle(555+(A25-A30)+B26-B31+C26-C31+D26-D31,295+(A21+A24+A26-A29)+B22+B25+B27-B30+C22+C25+C27-C30+D22+D25+D27-D30,570+(A25-A30)+B26-B31+C26-C31+D26-D31,257+(A21+A24+A26-A29)+B22+B25+B27-B30+C22+C25+C27-C30+D22+D25+D27-D30);   //coboara
rectangle(535+(A25-A30)+B26-B31+C26-C31+D26-D31,300+(A21+A24+A26-A29)+B22+B25+B27-B30+C22+C25+C27-C30+D22+D25+D27-D30,590+(A25-A30)+B26-B31+C26-C31+D26-D31,295+(A21+A24+A26-A29)+B22+B25+B27-B30+C22+C25+C27-C30+D22+D25+D27-D30);
rectangle(545+(A25-A30)+B26-B31+C26-C31+D26-D31,253,580+(A25-A30)+B26-B31+C26-C31+D26-D31,285);
rectangle(535+(A22+A25+A28-A30)+B23+B26+B29-B31+C23+C26+C29-C31+D23+D26+D29-D31,310+(A21+A24+A26-A29)+B22+B25+B27-B30+C22+C25+C27-C30+D22+D25+D27-D30,540+(A22+A25+A28-A30)+B23+B26+B29-B31+C23+C26+C29-C31+D23+D26+D29-D31,300+(A21+A24+A26-A29)+B22+B25+B27-B30+C22+C25+C27-C30+D22+D25+D27-D30);
rectangle(585+(A23+A25+A27-A30)+B24+B26+B28-B31+C24+C26+C28-C31+D24+D26+D28-D31,310+(A21+A24+A26-A29)+B22+B25+B27-B30+C22+C25+C27-C30+D22+D25+D27-D30,590+(A23+A25+A27-A30)+B24+B26+B28-B31+C24+C26+C28-C31+D24+D26+D28-D31,300+(A21+A24+A26-A29)+B22+B25+B27-B30+C22+C25+C27-C30+D22+D25+D27-D30);

brush.Color:=clwhite;

// ascensor

pen.Width:=8;
pen.color:=clblack;
brush.Color:=clyellow;
rectangle(712,0,738,600);
brush.Color:=clgray;
brush.Color:=clblack;
pen.Width:=3;
polygon([point(645,-581+A31+B32+C32+D32),point(655,-591+A31+B32+C32+D32),point(712,-591+A31+B32+C32+D32),point(712,-581+A31+B32+C32+D32)]);
polygon([point(645,-466+A31+B32+C32+D32),point(655,-476+A31+B32+C32+D32),point(712,-476+A31+B32+C32+D32),point(712,-466+A31+B32+C32+D32)]);
polygon([point(645,-351+A31+B32+C32+D32),point(655,-361+A31+B32+C32+D32),point(712,-361+A31+B32+C32+D32),point(712,-351+A31+B32+C32+D32)]);
polygon([point(645,-236+A31+B32+C32+D32),point(655,-246+A31+B32+C32+D32),point(712,-246+A31+B32+C32+D32),point(712,-236+A31+B32+C32+D32)]);
polygon([point(645,-109+A31+B32+C32+D32),point(655,-119+A31+B32+C32+D32),point(712,-119+A31+B32+C32+D32),point(712,-109+A31+B32+C32+D32)]);
polygon([point(645, 6+A31+B32+C32+D32),point(655,-4+A31+B32+C32+D32),point(712,-4+A31+B32+C32+D32),point(712,6+A31+B32+C32+D32)]);
polygon([point(645,121+A31+B32+C32+D32),point(655,111+A31+B32+C32+D32),point(712,111+A31+B32+C32+D32),point(712,121+A31+B32+C32+D32)]);
polygon([point(645,236+A31+B32+C32+D32),point(655,226+A31+B32+C32+D32),point(712,226+A31+B32+C32+D32),point(712,236+A31+B32+C32+D32)]);
polygon([point(645,351+A31+B32+C32+D32),point(655,341+A31+B32+C32+D32),point(712,341+A31+B32+C32+D32),point(712,351+A31+B32+C32+D32)]);
polygon([point(645,466+A31+B32+C32+D32),point(655,456+A31+B32+C32+D32),point(712,456+A31+B32+C32+D32),point(712,466+A31+B32+C32+D32)]);
polygon([point(645,581+A31+B32+C32+D32),point(655,571+A31+B32+C32+D32),point(712,571+A31+B32+C32+D32),point(712,581+A31+B32+C32+D32)]);
brush.Color:=clgray;


//brat generator
pen.Width:=1;
pen.color:=clred;
brush.Color:=clyellow;
rectangle(60+D13+D20+D26,280+D12+D25+D27+D32,80+D13+D20+D26,260+D12+D25+D27+D32);
polygon([point(60+D13+D20+D26,260+D12+D25+D27+D32),point(70+D13+D20+D26,250+D12+D25+D27+D32),point(90+D13+D20+D26,250+D12+D25+D27+D32),point(80+D13+D20+D26,260+D12+D25+D27+D32)]);
polygon([point(80+D13+D20+D26,260+D12+D25+D27+D32),point(90+D13+D20+D26,250+D12+D25+D27+D32),point(90+D13+D20+D26,270+D12+D25+D27+D32),point(80+D13+D20+D26,280+D12+D25+D27+D32)]);

rectangle(60+C13+C20+C26,280+C12+C25+C27+C32+D32,80+C13+C20+C26,260+C12+C25+C27+C32+D32);
polygon([point(60+C13+C20+C26,260+C12+C25+C27+C32+D32),point(70+C13+C20+C26,250+C12+C25+C27+C32+D32),point(90+C13+C20+C26,250+C12+C25+C27+C32+D32),point(80+C13+C20+C26,260+C12+C25+C27+C32+D32)]);
polygon([point(80+C13+C20+C26,260+C12+C25+C27+C32+D32),point(90+C13+C20+C26,250+C12+C25+C27+C32+D32),point(90+C13+C20+C26,270+C12+C25+C27+C32+D32),point(80+C13+C20+C26,280+C12+C25+C27+C32+D32)]);

rectangle(60+B13+B20+B26,280+B12+B25+B27+B32+C32+D32,80+B13+B20+B26,260+B12+B25+B27+B32+C32+D32);
polygon([point(60+B13+B20+B26,260+B12+B25+B27+B32+C32+D32),point(70+B13+B20+B26,250+B12+B25+B27+B32+C32+D32),point(90+B13+B20+B26,250+B12+B25+B27+B32+C32+D32),point(80+B13+B20+B26,260+B12+B25+B27+B32+C32+D32)]);
polygon([point(80+B13+B20+B26,260+B12+B25+B27+B32+C32+D32),point(90+B13+B20+B26,250+B12+B25+B27+B32+C32+D32),point(90+B13+B20+B26,270+B12+B25+B27+B32+C32+D32),point(80+B13+B20+B26,280+B12+B25+B27+B32+C32+D32)]);

rectangle(60+A12+A19+A25,280+A11+A24+A26+A31+B32+C32+D32,80+A12+A19+A25,260+A11+A24+A26+A31+B32+C32+D32);
polygon([point(60+A12+A19+A25,260+A11+A24+A26+A31+B32+C32+D32),point(70+A12+A19+A25,250+A11+A24+A26+A31+B32+C32+D32),point(90+A12+A19+A25,250+A11+A24+A26+A31+B32+C32+D32),point(80+A12+A19+A25,260+A11+A24+A26+A31+B32+C32+D32)]);
polygon([point(80+A12+A19+A25,260+A11+A24+A26+A31+B32+C32+D32),point(90+A12+A19+A25,250+A11+A24+A26+A31+B32+C32+D32),point(90+A12+A19+A25,270+A11+A24+A26+A31+B32+C32+D32),point(80+A12+A19+A25,280+A11+A24+A26+A31+B32+C32+D32)]);

pen.Width:=2;
pen.color:=clblack;
brush.Color:=clblue;
polygon([point(40,140),point(40,270),point(55,280),point(95,280),point(110,270),point(110,140) ]);
moveto(55,280); lineto(55,140);
moveto(95,280); lineto(95,140);


//brat 2
pen.Width:=2;
pen.color:=clblack;
brush.Color:=clblack;
rectangle(225,275+A13-A16+B14-B17+C14-C17+D14-D17,245,150+A13-A16+B14-B17+C14-C17+D14-D17);
rectangle(205,280+A13-A16+B14-B17+C14-C17+D14-D17,265,275+A13-A16+B14-B17+C14-C17+D14-D17);
rectangle(205+A14-A17+B15-B18+C15-C18+D15-D18,295+A13-A16+B14-B17+C14-C17+D14-D17,210+A14-A17+B15-B18+C15-C18+D15-D18,280+A13-A16+B14-B17+C14-C17+D14-D17);
rectangle(260+A15-A18+B16-B19+C16-C19+D16-D19,295+A13-A16+B14-B17+C14-C17+D14-D17,265+A15-A18+B16-B19+C16-C19+D16-D19,280+A13-A16+B14-B17+C14-C17+D14-D17);
pen.Width:=2;
pen.color:=clblack;
brush.Color:=clblue;
pen.Width:=2;
rectangle(205,270,265,140);


// toate cutiile

//a patra
pen.Color:=clblack;
pen.Width:=2;
brush.color:=clgreen;
rectangle(-55+D11+D13+D20+D26,350+D25+D27+D32,-85+D11+D13+D20+D26,320+D25+D27+D32);
polygon([point(-85+D11+D13+D20+D26,320+D25+D27+D32),point(-75+D11+D13+D20+D26,310+D25+D27+D32),point(-45+D11+D13+D20+D26,310+D25+D27+D32), point(-55+D11+D13+D20+D26,320+D25+D27+D32)]);
polygon([point(-55+D11+D13+D20+D26,320+D25+D27+D32),point(-45+D11+D13+D20+D26,310+D25+D27+D32),point(-45+D11+D13+D20+D26,340+D25+D27+D32), point(-55+D11+D13+D20+D26,350+D25+D27+D32)]);


x6:=round(-85+D11+D13+D20+D26+15*-sin((-D15+320)/17));
y6:=round(320+D25+D27+D32+15*-cos((-D15+322)/17));
z6:=round(-75+D11+D13+D20+D26+15*-sin((-D15+320)/17));
h6:=round(310+D25+D27+D32+15*-cos((-D15+320)/17));
polygon([point(-85+D11+D13+D20+D26,320+D25+D27+D32),point(x6,y6),point(z6,h6),point(-75+D11+D13+D20+D26,310+D25+D27+D32)]);


x7:=round(-55+D11+D13+D20+D26+15*-sin((D15+320)/17));
y7:=round(320+D25+D27+D32+15*-cos((D15+320)/17));
z7:=round(-45+D11+D13+D20+D26+15*-sin((D15+320)/17));
h7:=round(310+D25+D27+D32+15*-cos((-D15+320)/17));
polygon([point(-55+D11+D13+D20+D26,320+D25+D27+D32),point(x7,y7),point(z7,h7),point(-45+D11+D13+D20+D26,310+D25+D27+D32)]);


{a treia cutie}
pen.Color:=clblack;
pen.Width:=2;
brush.color:=clgreen;
rectangle(-55+C11+C13+C20+C26,350+C25+C27+C32+D32,-85+C11+C13+C20+C26,320+C25+C27+C32+D32);
polygon([point(-85+C11+C13+C20+C26,320+C25+C27+C32+D32),point(-75+C11+C13+C20+C26,310+C25+C27+C32+D32),point(-45+C11+C13+C20+C26,310+C25+C27+C32+D32), point(-55+C11+C13+C20+C26,320+C25+C27+C32+D32)]);
polygon([point(-55+C11+C13+C20+C26,320+C25+C27+C32+D32),point(-45+C11+C13+C20+C26,310+C25+C27+C32+D32),point(-45+C11+C13+C20+C26,340+C25+C27+C32+D32),point(-55+C11+C13+C20+C26,350+C25+C27+C32+D32) ]);


x4:=round(-85+C11+C13+C20+C26+15*-sin((-C15+320)/17));
y4:=round(320+C25+C27+C32+D32+15*-cos((-C15+322)/17));
z4:=round(-75+C11+C13+C20+C26+15*-sin((-C15+320)/17));
h4:=round(310+C25+C27+C32+D32+15*-cos((-C15+320)/17));
polygon([point(-85+C11+C13+C20+C26,320+C25+C27+C32+D32),point(x4,y4),point(z4,h4),point(-75+C11+C13+C20+C26,310+C25+C27+C32+D32)]);


x5:=round(-55+C11+C13+C20+C26+15*-sin((C15+320)/17));
y5:=round(320+C25+C27+C32+D32+15*-cos((C15+320)/17));
z5:=round(-45+C11+C13+C20+C26+15*-sin((C15+320)/17));
h5:=round(310+C25+C27+C32+D32+15*-cos((-C15+320)/17));
polygon([point(-55+C11+C13+C20+C26,320+C25+C27+C32+D32),point(x5,y5),point(z5,h5),point(-45+C11+C13+C20+C26,310+C25+C27+C32+D32)]);



{a doua cutie}
pen.Color:=clblack;
pen.Width:=2;
brush.color:=clgreen;
rectangle(-55+B11+B13+B20+B26,350+B25+B27+B32+C32+D32,-85+B11+B13+B20+B26,320+B25+B27+B32+C32+D32);
polygon([point(-85+B11+B13+B20+B26,320+B25+B27+B32+C32+D32),point(-75+B11+B13+B20+B26,310+B25+B27+B32+C32+D32),point(-45+B11+B13+B20+B26,310+B25+B27+B32+C32+D32), point(-55+B11+B13+B20+B26,320+B25+B27+B32+C32+D32)]);
polygon([point(-55+B11+B13+B20+B26,320+B25+B27+B32+C32+D32),point(-45+B11+B13+B20+B26,310+B25+B27+B32+C32+D32),point(-45+B11+B13+B20+B26,340+B25+B27+B32+C32+D32),point(-55+B11+B13+B20+B26,350+B25+B27+B32+C32+D32) ]);


x2:=round(-85+B11+B13+B20+B26+15*-sin((-B15+320)/17));
y2:=round(320+B25+B27+B32+C32+D32+15*-cos((-B15+322)/17));
z2:=round(-75+B11+B13+B20+B26+15*-sin((-B15+320)/17));
h2:=round(310+B25+B27+B32+C32+D32+15*-cos((-B15+320)/17));
polygon([point(-85+B11+B13+B20+B26,320+B25+B27+B32+C32+D32),point(x2,y2),point(z2,h2),point(-75+B11+B13+B20+B26,310+B25+B27+B32+C32+D32)]);


x3:=round(-55+B11+B13+B20+B26+15*-sin((B15+320)/17));
y3:=round(320+B25+B27+B32+C32+D32+15*-cos((B15+320)/17));
z3:=round(-45+B11+B13+B20+B26+15*-sin((B15+320)/17));
h3:=round(310+B25+B27+B32+C32+D32+15*-cos((-B15+320)/17));
polygon([point(-55+B11+B13+B20+B26,320+B25+B27+B32+C32+D32),point(x3,y3),point(z3,h3),point(-45+B11+B13+B20+B26,310+B25+B27+B32+C32+D32)]);


{prima cutie}
pen.Color:=clblack;
pen.Width:=2;
brush.color:=clgreen;
rectangle(55+A12+A19+A25,350+A24+A26+A31+B32+C32+D32,85+A12+A19+A25,320+A24+A26+A31+B32+C32+D32);
polygon([point(55+A12+A19+A25,320+A24+A26+A31+B32+C32+D32),point(65+A12+A19+A25,310+A24+A26+A31+B32+C32+D32),point(95+A12+A19+A25,310+A24+A26+A31+B32+C32+D32), point(85+A12+A19+A25,320+A24+A26+A31+B32+C32+D32,)]);
polygon([point(85+A12+A19+A25,320+A24+A26+A31+B32+C32+D32),point(85+A12+A19+A25,350+A24+A26+A31+B32+C32+D32),point(95+A12+A19+A25,340+A24+A26+A31+B32+C32+D32),point(95+A12+A19+A25,310+A24+A26+A31+B32+C32+D32) ]);

x:=round(55+A12+A19+A25+15*-sin((-A14+320)/17));
y:=round(320+A24+A26+A31+B32+C32+D32+15*-cos((-A14+320)/17));
z:=round(65+A12+A19+A25+15*-sin((-A14+320)/17));
h:=round(310+A24+A26+A31+B32+C32+D32+15*-cos((-A14+320)/17));
polygon([point(55+A12+A19+A25,320+A24+A26+A31+B32+C32+D32),point(x,y),point(z,h),point(65+A12+A19+A25,310+A24+A26+A31+B32+C32+D32)]);

x1:=round(85+A12+A19+A25+15*-sin((A14+320)/17));
y1:=round(320+A24+A26+A31+B32+C32+D32+15*-cos((A14+320)/17));
z1:=round(95+A12+A19+A25+15*-sin((A14+320)/17));
h1:=round(310+A24+A26+A31+B32+C32+D32+15*-cos((-A14+320)/17));
polygon([point(85+A12+A19+A25,320+A24+A26+A31+B32+C32+D32),point(x1,y1),point(z1,h1),point(95+A12+A19+A25,310+A24+A26+A31+B32+C32+D32)]);


pen.Color:=clblack;
pen.width:=3;
brush.Color:=clgray;
polygon([point(430,350),point(440,340),point(440,285),point(430,295)]);


// pt pachete

pen.Width:=2;
pen.color:=clblack;
brush.Color:=clmaroon;

rectangle (370+D21+D26,350+D25+D27+D32,400+D21+D26,320+D25+D27+D32);
moveto(370+D21+D26,350+D25+D27+D32);lineto(400+D21+D26,320+D25+D27+D32);
moveto(370+D21+D26,320+D25+D27+D32);lineto(400+D21+D26,350+D25+D27+D32);
polygon([point(370+D21+D26,320+D25+D27+D32),point(380+D21+D26,310+D25+D27+D32),point(410+D21+D26,310+D25+D27+D32),point(400+D21+D26,320+D25+D27+D32)]);
polygon([point(400+D21+D26,350+D25+D27+D32),point(400+D21+D26,320+D25+D27+D32),point(410+D21+D26,310+D25+D27+D32),point(410+D21+D26,340+D25+D27+D32) ]);


rectangle (370+C21+C26,350+C25+C27+C32+D32,400+C21+C26,320+C25+C27+C32+D32);
moveto(370+C21+C26,350+C25+C27+C32+D32);lineto(400+C21+C26,320+C25+C27+C32+D32);
moveto(370+C21+C26,320+C25+C27+C32+D32);lineto(400+C21+C26,350+C25+C27+C32+D32);
polygon([point(370+C21+C26,320+C25+C27+C32+D32),point(380+C21+C26,310+C25+C27+C32+D32),point(410+C21+C26,310+C25+C27+C32+D32), point(400+C21+C26,320+C25+C27+C32+D32)]);
polygon([point(400+C21+C26,350+C25+C27+C32+D32),point(400+C21+C26,320+C25+C27+C32+D32),point(410+C21+C26,310+C25+C27+C32+D32),point(410+C21+C26,340+C25+C27+C32+D32) ]);


rectangle (370+B21+B26,350+B25+B27+B32+C32+D32,400+B21+B26,320+B25+B27+B32+C32+D32);
moveto(370+B21+B26,350+B25+B27+B32+C32+D32);lineto(400+B21+B26,320+B25+B27+B32+C32+D32);
moveto(370+B21+B26,320+B25+B27+B32+C32+D32);lineto(400+B21+B26,350+B25+B27+B32+C32+D32);
polygon([point(370+B21+B26,320+B25+B27+B32+C32+D32),point(380+B21+B26,310+B25+B27+B32+C32+D32),point(410+B21+B26,310+B25+B27+B32+C32+D32), point(400+B21+B26,320+B25+B27+B32+C32+D32)]);
polygon([point(400+B21+B26,350+B25+B27+B32+C32+D32),point(400+B21+B26,320+B25+B27+B32+C32+D32),point(410+B21+B26,310+B25+B27+B32+C32+D32),point(410+B21+B26,340+B25+B27+B32+C32+D32) ]);



rectangle (370+A20+A25,350+A24+A26+A31+B32+C32+D32,400+A20+A25,320+A24+A26+A31+B32+C32+D32);
moveto(370+A20+A25,350+A24+A26+A31+B32+C32+D32);lineto(400+A20+A25,320+A24+A26+A31+B32+C32+D32);
moveto(370+A20+A25,320+A24+A26+A31+B32+C32+D32);lineto(400+A20+A25,350+A24+A26+A31+B32+C32+D32);
polygon([point(370+A20+A25,320+A24+A26+A31+B32+C32+D32),point(380+A20+A25,310+A24+A26+A31+B32+C32+D32),point(410+A20+A25,310+A24+A26+A31+B32+C32+D32), point(400+A20+A25,320+A24+A26+A31+B32+C32+D32,)]);
polygon([point(400+A20+A25,350+A24+A26+A31+B32+C32+D32),point(400+A20+A25,320+A24+A26+A31+B32+C32+D32),point(410+A20+A25,310+A24+A26+A31+B32+C32+D32),point(410+A20+A25,340+A24+A26+A31+B32+C32+D32) ]);

brush.Color:=clwhite;
pen.color:=clblack;
pen.Width:=3;
brush.Color:=clyellow;
rectangle (320,295,430,350);
polygon([point(320,295),point(330,285),point(440,285),point(430,295)]);
brush.Color:=clgray;
polygon([point(430,350),point(440,340),point(440,285),point(430,295)]);

brush.Color:=clwhite;



If ales=1 then begin
If ales=2 then t:=t;


//variabilele de timp:
if (t>0) and (t<=70)  then A11:=A11+1;  //coborare pachet
if (t>70) and (t<=230)  then A12:=A12+1;  //miscare cutie intre generator si brat

if (t>230) and (t<=250) then A13:=A13+1;  //coborare  brat 2
if (t>250) and (t<=275) then A14:=A14+1;
if (t>250) and (t<=275) then A15:=A15-1;  //strangere clesti brat 1

if (t>285) and (t<=310) then A17:=A17+1;
if (t>285) and (t<=310) then A18:=A18-1;
if (t>295) and (t<=315) then A16:=A16+1; //revenire brat 2


if (t>320) and (t<=645) then A19:=A19+1; //miscare spre etichetare
if (t>475) and (t<=645) then A20:=A20+1; //miscare pana la brat 3

if (t>645) and (t<=655) then A21:=A21+1; // coborare brat 3
if (t>655) and (t<=660) then A22:=A22+1;
if (t>655) and (t<=660) then A23:=A23-1;

if (t>660) and (t<=670) then A24:=A24-1;  //ridicare brat 3
if (t>670) and (t<=790) then A25:=A25+1; //transport brat 3
if (t>790) and (t<=800) then A26:=A26+1; //coborare brat spre treapta
if (t>800) and (t<=805) then A27:=A27+1;
if (t>800) and (t<=805) then A28:=A28-1;
if (t>805) and (t<=815) then A29:=A29+1; //revenire brat 3
if (t>815) and (t<=935) then A30:=A30+1;

if (t>850) and (t<=965) then A31:=A31+1; //coborare ascensor+cutie 1

//pentru a doua cutie
if (t>660) and (t<=800) then B11:=B11+1; //miscare cutia 2 spre brat gener.
if (t>800) and (t<=870) then B12:=B12+1;  //coborare pachet 2
if (t>870) and (t<=1030) then B13:=B13+1;
if (t>1030) and (t<=1050) then B14:=B14+1;  //coborare brat2
if (t>1050) and (t<=1075) then B15:=B15+1;
if (t>1050) and (t<=1075) then B16:=B16-1;

if (t>1085) and (t<=1110) then B18:=B18+1;
if (t>1085) and (t<=1110) then B19:=B19-1;
if (t>1095) and (t<=1115) then B17:=B17+1; //revenire brat
if (t>1120) and (t<=1445) then B20:=B20+1; //miscare spre etichetare
if (t>1275) and (t<=1445) then B21:=B21+1;

if (t>1445) and (t<=1455) then B22:=B22+1;   //coborare brat 3
if (t>1455) and (t<=1460) then B23:=B23+1;
if (t>1455) and (t<=1460) then B24:=B24-1;

if (t>1460) and (t<=1470) then B25:=B25-1;
if (t>1470) and (t<=1590) then B26:=B26+1;

if (t>1590) and (t<=1600) then B27:=B27+1;
if (t>1600) and (t<=1605) then B28:=B28+1;
if (t>1600) and (t<=1605) then B29:=B29-1;
if (t>1605) and (t<=1615) then B30:=B30+1;
if (t>1615) and (t<=1735) then B31:=B31+1;
if (t>1650) and (t<=1765) then B32:=B32+1;

 //pt a treia cutie
if (t>1590) and (t<=1730) then C11:=C11+1; //miscare cutia 3 spre brat gener.
if (t>1730) and (t<=1800) then C12:=C12+1;  //coborare pachet 3
if (t>1800) and (t<=1960) then C13:=C13+1;
if (t>1960) and (t<=1980) then C14:=C14+1;  //coborare brat2
if (t>1980) and (t<=2005) then C15:=C15+1;
if (t>1980) and (t<=2005) then C16:=C16-1;

if (t>2015) and (t<=2040) then C18:=C18+1;
if (t>2015) and (t<=2040) then C19:=C19-1;
if (t>2025) and (t<=2045) then C17:=C17+1; //revenire brat
if (t>2050) and (t<=2375) then C20:=C20+1; //miscare spre etichetare
if (t>2205) and (t<=2375) then C21:=C21+1;

if (t>2375) and (t<=2385) then C22:=C22+1;   //coborare brat 3
if (t>2385) and (t<=2390) then C23:=C23+1;
if (t>2385) and (t<=2390) then C24:=C24-1;

if (t>2390) and (t<=2400) then C25:=C25-1;
if (t>2400) and (t<=2520) then C26:=C26+1;

if (t>2520) and (t<=2530) then C27:=C27+1;
if (t>2530) and (t<=2535) then C28:=C28+1;
if (t>2530) and (t<=2535) then C29:=C29-1;
if (t>2535) and (t<=2545) then C30:=C30+1;
if (t>2545) and (t<=2665) then C31:=C31+1;
if (t>2580) and (t<=2695) then C32:=C32+1;

     //pentru a patra cutie
if (t>2520) and (t<=2660) then D11:=D11+1; //miscare cutia 2 spre brat gener.
if (t>2660) and (t<=2730) then D12:=D12+1;  //coborare pachet 2
if (t>2730) and (t<=2890) then D13:=D13+1;
if (t>2890) and (t<=2910) then D14:=D14+1;  //coborare brat2
if (t>2910) and (t<=2935) then D15:=D15+1;
if (t>2910) and (t<=2935) then D16:=D16-1;

if (t>2945) and (t<=2970) then D18:=D18+1;
if (t>2945) and (t<=2970) then D19:=D19-1;
if (t>2955) and (t<=2975) then D17:=D17+1; //revenire brat
if (t>2980) and (t<=3305) then D20:=D20+1; //miscare spre etichetare
if (t>3135) and (t<=3305) then D21:=D21+1;

if (t>3305) and (t<=3315) then D22:=D22+1;   //coborare brat 3
if (t>3315) and (t<=3320) then D23:=D23+1;
if (t>3315) and (t<=3320) then D24:=D24-1;

if (t>3320) and (t<=3330) then D25:=D25-1;
if (t>3330) and (t<=3450) then D26:=D26+1;

if (t>3450) and (t<=3460) then D27:=D27+1;
if (t>3460) and (t<=3465) then D28:=D28+1;
if (t>3460) and (t<=3465) then D29:=D29-1;
if (t>3465) and (t<=3475) then D30:=D30+1;
if (t>3475) and (t<=3595) then D31:=D31+1;
if (t>3510) and (t<=3625) then D32:=D32+1;


If (t>3610) then
 begin
A11:=0;
A12:=0;
A13:=0;
A14:=0;
A15:=0;
A16:=0;
A17:=0;
A18:=0;
A19:=0;
A20:=0;
A21:=0;
A22:=0;
A23:=0;
A24:=0;
A25:=0;
A26:=0;
A27:=0;
A28:=0;
A29:=0;
A30:=0;
A31:=0;
B11:=0;
B12:=0;
B13:=0;
B14:=0;
B15:=0;
B16:=0;
B17:=0;
B18:=0;
B19:=0;
B20:=0;
B21:=0;
B22:=0;
B23:=0;
B24:=0;
B25:=0;
B26:=0;
B27:=0;
B28:=0;
B29:=0;
B30:=0;
B31:=0;
B32:=0;
C11:=0;
C12:=0;
C13:=0;
C14:=0;
C15:=0;
C16:=0;
C17:=0;
C18:=0;
C19:=0;
C20:=0;
C21:=0;
C22:=0;
C23:=0;
C24:=0;
C25:=0;
C26:=0;
C27:=0;
C28:=0;
C29:=0;
C30:=0;
C31:=0;
C32:=0;

D11:=0;
D12:=0;
D13:=0;
D14:=0;
D15:=0;
D16:=0;
D17:=0;
D18:=0;
D19:=0;
D20:=0;
D21:=0;
D22:=0;
D23:=0;
D24:=0;
D25:=0;
D26:=0;
D27:=0;
D28:=0;
D29:=0;
D30:=0; D31:=0; D32:=0;
end;

If directie
then t:=t+1 else t:=0;

If (t=0) or (t=3620)             // reinitializare
then directie:= not directie;


end;
end;
end;
  procedure TForm1.FormCreate(Sender: TObject);
begin
         form1.Height:=600;
         form1.Width:=800;
end;
procedure TForm1.Button1Click(Sender: TObject);
begin
ales:=1;
end;

procedure TForm1.Button2Click(Sender: TObject);
begin
ales:=2;
end;

procedure TForm1.Button3Click(Sender: TObject);
begin
form1.close

end;

end.
