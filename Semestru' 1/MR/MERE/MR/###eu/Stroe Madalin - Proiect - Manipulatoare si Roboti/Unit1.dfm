object Form1: TForm1
  Left = 21
  Top = 103
  Width = 795
  Height = 557
  Caption = 'Form1'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  DesignSize = (
    787
    523)
  PixelsPerInch = 96
  TextHeight = 13
  object Image1: TImage
    Left = -1
    Top = 0
    Width = 802
    Height = 646
    Anchors = [akTop, akBottom]
    Center = True
    DragCursor = crHandPoint
    ParentShowHint = False
    ShowHint = True
    Stretch = True
    OnClick = Timer1Timer
    OnDblClick = Timer1Timer
  end
  object Button1: TButton
    Left = 176
    Top = 40
    Width = 75
    Height = 25
    Caption = 'Start'
    TabOrder = 0
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 264
    Top = 40
    Width = 75
    Height = 25
    Caption = 'Stop'
    TabOrder = 1
    OnClick = Button2Click
  end
  object Button3: TButton
    Left = 352
    Top = 40
    Width = 75
    Height = 25
    Caption = 'Inchide'
    TabOrder = 2
    OnClick = Button3Click
  end
  object Timer1: TTimer
    Interval = 1
    OnTimer = Timer1Timer
    Left = 128
    Top = 40
  end
end
