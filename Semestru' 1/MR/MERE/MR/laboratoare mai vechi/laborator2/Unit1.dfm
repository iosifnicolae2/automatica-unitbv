object Form1: TForm1
  Left = 223
  Top = 190
  Width = 546
  Height = 244
  Caption = 'Operatii simple'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 257
    Height = 209
    TabOrder = 0
    object Label1: TLabel
      Left = 72
      Top = 32
      Width = 73
      Height = 13
      Caption = 'Data De Intrare'
    end
    object Label2: TLabel
      Left = 32
      Top = 88
      Width = 6
      Height = 13
      Caption = 'a'
    end
    object Label3: TLabel
      Left = 32
      Top = 128
      Width = 6
      Height = 13
      Caption = 'b'
    end
    object Label4: TLabel
      Left = 32
      Top = 160
      Width = 6
      Height = 13
      Caption = 'c'
    end
    object Eda: TEdit
      Left = 88
      Top = 88
      Width = 121
      Height = 21
      TabOrder = 0
      Text = 'Eda'
    end
    object Edb: TEdit
      Left = 88
      Top = 120
      Width = 121
      Height = 21
      TabOrder = 1
      Text = 'Edb'
    end
    object Edc: TEdit
      Left = 88
      Top = 152
      Width = 121
      Height = 21
      TabOrder = 2
      Text = 'Edc'
    end
  end
  object GroupBox1: TGroupBox
    Left = 256
    Top = 0
    Width = 281
    Height = 185
    Caption = 'Rezultate'
    TabOrder = 1
    object Label5: TLabel
      Left = 32
      Top = 80
      Width = 60
      Height = 13
      Caption = 'Suma a+b+c'
    end
    object Label6: TLabel
      Left = 32
      Top = 120
      Width = 70
      Height = 13
      Caption = 'Diferenta a-b-c'
    end
    object Label7: TLabel
      Left = 32
      Top = 160
      Width = 70
      Height = 13
      Caption = 'Produsul a*b*c'
    end
    object EdS: TEdit
      Left = 120
      Top = 80
      Width = 121
      Height = 21
      TabOrder = 0
    end
    object EdD: TEdit
      Left = 120
      Top = 112
      Width = 121
      Height = 21
      TabOrder = 1
    end
    object EdP: TEdit
      Left = 120
      Top = 152
      Width = 121
      Height = 21
      TabOrder = 2
    end
  end
  object BitBtn1: TBitBtn
    Left = 256
    Top = 184
    Width = 137
    Height = 25
    Caption = 'Calcul'
    TabOrder = 2
    OnClick = BitBtn1Click
    Glyph.Data = {
      76010000424D7601000000000000760000002800000020000000100000000100
      04000000000000010000120B0000120B00001000000000000000000000000000
      800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00300000000000
      0000377777777777777703030303030303037F7F7F7F7F7F7F7F000000000000
      00007777777777777777933393303933337073F37F37F73F3377393393303393
      379037FF7F37F37FF777379793303379793037777337F3777737339933303339
      93303377F3F7F3F77F3733993930393993303377F737F7377FF7399993303399
      999037777337F377777793993330333393307377FF37F3337FF7333993303333
      993033377F37F33377F7333993303333993033377337F3337737333333303333
      33303FFFFFF7FFFFFFF700000000000000007777777777777777030303030303
      03037F7F7F7F7F7F7F7F00000000000000007777777777777777}
    NumGlyphs = 2
  end
  object BitBtn2: TBitBtn
    Left = 392
    Top = 184
    Width = 145
    Height = 25
    TabOrder = 3
    Kind = bkClose
  end
end
