unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons;

type
  TForm1 = class(TForm)
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    GroupBox3: TGroupBox;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    edx: TEdit;
    edeps: TEdit;
    eds: TEdit;
    edc: TEdit;
    edks: TEdit;
    edkc: TEdit;
    edsin: TEdit;
    edcos: TEdit;
    procedure BitBtn1Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;
  x,e,s,t,k,c:real;

implementation

{$R *.dfm}

procedure TForm1.BitBtn1Click(Sender: TObject);
begin
x:=strtofloat(edx.Text);
e:=strtofloat(edeps.Text);
S:=0;
T:=x;
k:=0;
repeat
S:=S+T;
k:=k+1;
T:=T*(-x*x/(2*k*(2*k+1)));
until abs(T)<e;
eds.Text:=floattostr(s);
edKs.Text:=floattostr(k);
c:=0;
t:=1;
k:=0;
repeat
c:=c+t;
k:=k+1;
T:=T*(-x*x/(2*k*(2*k-1)));
until abs(T)<e;
edc.Text:=floattostr(c);
edKc.Text:=floattostr(k);
edsin.text:=floattostr(sin(x));
edcos.text:=floattostr(cos(x));
end;

procedure TForm1.BitBtn2Click(Sender: TObject);
begin
close;
end;

end.
