object Form1: TForm1
  Left = 192
  Top = 107
  Width = 266
  Height = 409
  Caption = 'Form1'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBox1: TGroupBox
    Left = 0
    Top = 0
    Width = 257
    Height = 105
    Caption = 'Date de intrare'
    TabOrder = 0
    object Label1: TLabel
      Left = 48
      Top = 24
      Width = 7
      Height = 13
      Caption = 'X'
    end
    object Label2: TLabel
      Left = 176
      Top = 24
      Width = 6
      Height = 16
      Caption = 'e'
      Font.Charset = SYMBOL_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Symbol'
      Font.Style = []
      ParentFont = False
    end
    object edx: TEdit
      Left = 24
      Top = 48
      Width = 65
      Height = 21
      TabOrder = 0
      Text = '5'
    end
    object edeps: TEdit
      Left = 144
      Top = 48
      Width = 73
      Height = 21
      TabOrder = 1
      Text = '0.01'
    end
  end
  object GroupBox2: TGroupBox
    Left = 0
    Top = 104
    Width = 257
    Height = 105
    TabOrder = 1
    object Label3: TLabel
      Left = 16
      Top = 24
      Width = 34
      Height = 13
      Caption = 'Serie S'
    end
    object Label4: TLabel
      Left = 16
      Top = 72
      Width = 37
      Height = 13
      Caption = 'Suma C'
    end
    object Label5: TLabel
      Left = 152
      Top = 32
      Width = 12
      Height = 13
      Caption = 'Ks'
    end
    object Label6: TLabel
      Left = 152
      Top = 72
      Width = 13
      Height = 13
      Caption = 'Kc'
    end
    object eds: TEdit
      Left = 64
      Top = 24
      Width = 65
      Height = 21
      Enabled = False
      TabOrder = 0
    end
    object edc: TEdit
      Left = 64
      Top = 72
      Width = 65
      Height = 21
      Enabled = False
      TabOrder = 1
    end
    object edks: TEdit
      Left = 176
      Top = 24
      Width = 65
      Height = 21
      Enabled = False
      TabOrder = 2
    end
    object edkc: TEdit
      Left = 176
      Top = 72
      Width = 65
      Height = 21
      Enabled = False
      TabOrder = 3
    end
  end
  object GroupBox3: TGroupBox
    Left = 0
    Top = 216
    Width = 257
    Height = 105
    TabOrder = 2
    object Label7: TLabel
      Left = 24
      Top = 32
      Width = 26
      Height = 13
      Caption = 'Sin(x)'
    end
    object Label8: TLabel
      Left = 24
      Top = 72
      Width = 29
      Height = 13
      Caption = 'Cos(x)'
    end
    object edsin: TEdit
      Left = 72
      Top = 24
      Width = 121
      Height = 21
      Enabled = False
      TabOrder = 0
    end
    object edcos: TEdit
      Left = 72
      Top = 64
      Width = 121
      Height = 21
      Enabled = False
      TabOrder = 1
    end
  end
  object BitBtn1: TBitBtn
    Left = 0
    Top = 328
    Width = 129
    Height = 49
    Caption = 'Calcul'
    TabOrder = 3
    OnClick = BitBtn1Click
  end
  object BitBtn2: TBitBtn
    Left = 136
    Top = 328
    Width = 121
    Height = 49
    Caption = 'Close'
    TabOrder = 4
    OnClick = BitBtn2Click
  end
end
