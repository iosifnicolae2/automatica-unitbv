object Form1: TForm1
  Left = 192
  Top = 107
  Width = 454
  Height = 287
  Caption = 'Corpuri 3D'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBox1: TGroupBox
    Left = 0
    Top = 0
    Width = 225
    Height = 153
    Caption = 'Date'
    TabOrder = 0
    object Label1: TLabel
      Left = 16
      Top = 48
      Width = 36
      Height = 13
      Caption = 'Raza R'
    end
    object Label2: TLabel
      Left = 16
      Top = 96
      Width = 53
      Height = 13
      Caption = 'Inaltimea H'
    end
    object edr: TSpinEdit
      Left = 80
      Top = 40
      Width = 121
      Height = 22
      MaxValue = 0
      MinValue = 0
      TabOrder = 0
      Value = 0
    end
    object edh: TSpinEdit
      Left = 80
      Top = 88
      Width = 121
      Height = 22
      MaxValue = 0
      MinValue = 0
      TabOrder = 1
      Value = 0
    end
  end
  object GroupBox2: TGroupBox
    Left = 232
    Top = 0
    Width = 209
    Height = 153
    Caption = 'Selectie'
    TabOrder = 1
    object rbcil: TRadioButton
      Left = 32
      Top = 48
      Width = 113
      Height = 17
      Caption = 'Cilinhru'
      Checked = True
      TabOrder = 0
      TabStop = True
      OnClick = BitBtn1Click
    end
    object rbcon: TRadioButton
      Left = 32
      Top = 80
      Width = 113
      Height = 17
      Caption = 'Con'
      TabOrder = 1
      OnClick = BitBtn1Click
    end
    object rbsf: TRadioButton
      Left = 32
      Top = 112
      Width = 113
      Height = 17
      Caption = 'Sfera'
      TabOrder = 2
      OnClick = BitBtn1Click
    end
  end
  object GroupBox3: TGroupBox
    Left = 0
    Top = 160
    Width = 225
    Height = 97
    Caption = 'Reultate'
    TabOrder = 2
    object lbV: TLabel
      Left = 24
      Top = 24
      Width = 7
      Height = 13
      Caption = 'V'
    end
    object lbAt: TLabel
      Left = 24
      Top = 56
      Width = 10
      Height = 13
      Caption = 'At'
    end
    object edat: TEdit
      Left = 80
      Top = 48
      Width = 121
      Height = 21
      TabOrder = 0
      Text = 'edat'
    end
    object edv: TEdit
      Left = 80
      Top = 16
      Width = 121
      Height = 21
      TabOrder = 1
      Text = 'edv'
    end
  end
  object BitBtn1: TBitBtn
    Left = 232
    Top = 160
    Width = 209
    Height = 41
    Caption = 'Calcul'
    TabOrder = 3
    OnClick = BitBtn1Click
  end
  object BitBtn2: TBitBtn
    Left = 232
    Top = 208
    Width = 209
    Height = 49
    TabOrder = 4
    Kind = bkClose
  end
end
