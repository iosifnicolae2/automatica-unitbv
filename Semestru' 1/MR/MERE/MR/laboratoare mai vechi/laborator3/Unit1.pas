unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Spin, StdCtrls, Buttons;

type
  TForm1 = class(TForm)
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    GroupBox3: TGroupBox;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    rbcil: TRadioButton;
    rbcon: TRadioButton;
    rbsf: TRadioButton;
    edat: TEdit;
    edv: TEdit;
    lbV: TLabel;
    lbAt: TLabel;
    edr: TSpinEdit;
    edh: TSpinEdit;
    Label1: TLabel;
    Label2: TLabel;
    procedure BitBtn1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;
  r,h: real;
  v,at: real;

implementation

{$R *.dfm}

procedure TForm1.BitBtn1Click(Sender: TObject);
begin
r:=strtofloat(edr.text);
h:=strtofloat(edh.text);
 if rbcil.Checked then
 begin
   v:=pi*r*r*h;
   at:=2*pi*r*(r+h);
 end;
 if rbcon.Checked then
 begin
   v:=pi*r*r*h/3;
   at:=pi*r*(r+sqrt(r*r+h*h));
 end;
 if rbsf.Checked then
 begin
   v:=4*pi*r*r*r/3;
   at:=4*pi*r*r;
 end;
 edv.Text:=floattostr(v);
 edat.Text:=floattostr(at);
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
r:=2;
h:=5;
v:=0;
at:=0;
edr.Text:=floattostr(r);
edh.Text:=floattostr(h);
edv.Text:=floattostr(v);
edat.Text:=floattostr(at);
end;

end.
