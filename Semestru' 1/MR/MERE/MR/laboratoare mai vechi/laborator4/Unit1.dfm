object Form1: TForm1
  Left = 192
  Top = 107
  Width = 696
  Height = 480
  Caption = 'Tip Triunghi '
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label4: TLabel
    Left = 488
    Top = 128
    Width = 18
    Height = 13
    Caption = 'Aria'
  end
  object Label5: TLabel
    Left = 488
    Top = 160
    Width = 46
    Height = 13
    Caption = 'Perimetrul'
  end
  object GroupBox1: TGroupBox
    Left = 40
    Top = 32
    Width = 321
    Height = 210
    Caption = 'Date de intrare'
    Color = clBlue
    ParentColor = False
    TabOrder = 0
    object Label1: TLabel
      Left = 16
      Top = 48
      Width = 6
      Height = 13
      Caption = 'a'
    end
    object Label2: TLabel
      Left = 16
      Top = 96
      Width = 6
      Height = 13
      Caption = 'b'
    end
    object Label3: TLabel
      Left = 16
      Top = 144
      Width = 6
      Height = 13
      Caption = 'c'
    end
    object eda: TSpinEdit
      Left = 144
      Top = 40
      Width = 121
      Height = 22
      MaxValue = 0
      MinValue = 0
      TabOrder = 0
      Value = 0
    end
    object edb: TSpinEdit
      Left = 144
      Top = 88
      Width = 121
      Height = 22
      MaxValue = 0
      MinValue = 0
      TabOrder = 1
      Value = 0
    end
    object edc: TSpinEdit
      Left = 144
      Top = 136
      Width = 121
      Height = 22
      MaxValue = 0
      MinValue = 0
      TabOrder = 2
      Value = 0
    end
  end
  object BitBtn1: TBitBtn
    Left = 48
    Top = 288
    Width = 75
    Height = 25
    Caption = 'Calcul'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clRed
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    OnClick = BitBtn1Click
  end
  object BitBtn2: TBitBtn
    Left = 192
    Top = 288
    Width = 75
    Height = 25
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
    Kind = bkClose
  end
  object cbnutri: TCheckBox
    Left = 464
    Top = 56
    Width = 97
    Height = 17
    Caption = 'Nu este triunghi'
    TabOrder = 3
  end
  object cbtri: TCheckBox
    Left = 464
    Top = 96
    Width = 97
    Height = 17
    Caption = 'Triunghi'
    TabOrder = 4
  end
  object edaria: TEdit
    Left = 552
    Top = 120
    Width = 121
    Height = 21
    TabOrder = 5
  end
  object edperim: TEdit
    Left = 552
    Top = 152
    Width = 121
    Height = 21
    TabOrder = 6
  end
  object cbiso: TCheckBox
    Left = 400
    Top = 256
    Width = 97
    Height = 17
    Caption = 'Isoscel'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clRed
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 7
  end
  object cbech: TCheckBox
    Left = 400
    Top = 296
    Width = 97
    Height = 17
    Caption = 'Echilateral'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clYellow
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 8
  end
  object cbdre: TCheckBox
    Left = 400
    Top = 320
    Width = 97
    Height = 17
    Caption = 'Dreptunghic'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clHotLight
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 9
  end
  object cboare: TCheckBox
    Left = 400
    Top = 352
    Width = 97
    Height = 17
    Caption = 'Oarecare'
    TabOrder = 10
  end
  object edceas: TEdit
    Left = 536
    Top = 72
    Width = 121
    Height = 21
    TabOrder = 11
  end
  object Timer1: TTimer
    Enabled = False
    OnTimer = Timer1Timer
    Left = 528
    Top = 280
  end
end
