unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, Spin, ExtCtrls;

type
  TForm1 = class(TForm)
    GroupBox1: TGroupBox;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    eda: TSpinEdit;
    edb: TSpinEdit;
    edc: TSpinEdit;
    cbnutri: TCheckBox;
    cbtri: TCheckBox;
    Label4: TLabel;
    Label5: TLabel;
    edaria: TEdit;
    edperim: TEdit;
    cbiso: TCheckBox;
    cbech: TCheckBox;
    cbdre: TCheckBox;
    cboare: TCheckBox;
    Timer1: TTimer;
    edceas: TEdit;
    procedure BitBtn1Click(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;
  a,b,c: integer;
  aria, perim, sp: real;
  tri, iso, ech, dre, oare: boolean;

implementation

{$R *.dfm}

procedure TForm1.BitBtn1Click(Sender: TObject);
begin
a:=strtoint(eda.Text);
b:=strtoint(edb.text);
c:=strtoint(edc.text);
  tri:=(a+b>c) and (b+c>a) and (a+c>b);
  ech:=(a=b) and (b=c);
  iso:=((a=b) or (b=c) or (a=c)) and not(ech);
  dre:=(a*a+b*b=c*c) or (b*b+c*c=a*a);
  oare:=tri and not(ech) and not(iso) and not(dre);
  cbtri.Checked:=tri;
  cbnutri.checked:=not(tri);
  cbiso.Checked:=iso;
  cbech.checked:=ech;
  cbdre.Checked:=dre;
  cboare.Checked:=oare;
  if tri then
    begin
      perim:=a+b+c; sp:=perim/2;
      aria:=sqrt(sp*(sp-a)*(sp-b)*(sp-c));
      edaria.Text:=floattostr(aria);
      edperim.Text:=floattostr(perim);
    end;
  if cbnutri.checked then
    begin
      edaria.Text:=' '; edperim.Text:=' ';
      timer1.enabled:=true;
    end

end;

procedure TForm1.Timer1Timer(Sender: TObject);
begin
edceas.text:=timetostr(time);
end;

end.
