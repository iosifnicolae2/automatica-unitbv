unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons;

type
  TForm1 = class(TForm)
    sbrosu: TScrollBar;
    sbverde: TScrollBar;
    sbalbastru: TScrollBar;
    edculoare: TEdit;
    edrosu: TEdit;
    edverde: TEdit;
    edalbastru: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    BitBtn1: TBitBtn;
    procedure sbrosuChange(Sender: TObject);
    procedure edrosuChange(Sender: TObject);
    procedure sbverdeChange(Sender: TObject);
    procedure sbalbastruChange(Sender: TObject);
    procedure edverdeChange(Sender: TObject);
    procedure edalbastruChange(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;
  r,v,a:integer;

implementation

{$R *.dfm}

procedure TForm1.sbrosuChange(Sender: TObject);
begin
r:=sbrosu.Position ;
edculoare.Color:=RGB(r,v,a);
edrosu.Text:=inttostr(r);
end;

procedure TForm1.edrosuChange(Sender: TObject);
begin
r:=strtoint(edrosu.Text);
edculoare.Color:=RGB(r,v,a);
sbrosu.Position:=r;
end;

procedure TForm1.sbverdeChange(Sender: TObject);
begin
v:=sbverde.Position ;
edculoare.Color:=RGB(r,v,a);
edverde.Text:=inttostr(v);
end;

procedure TForm1.sbalbastruChange(Sender: TObject);
begin
a:=sbalbastru.Position ;
edculoare.Color:=RGB(r,v,a);
edalbastru.Text:=inttostr(a);
end;

procedure TForm1.edverdeChange(Sender: TObject);
begin
v:=strtoint(edverde.Text);
edculoare.Color:=RGB(r,v,a);
sbverde.Position:=v;
end;

procedure TForm1.edalbastruChange(Sender: TObject);
begin
a:=strtoint(edalbastru.Text);
edculoare.Color:=RGB(r,v,a);
sbalbastru.Position:=a;
end;

end.
