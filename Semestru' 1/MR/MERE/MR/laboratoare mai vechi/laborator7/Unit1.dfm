object Form1: TForm1
  Left = 202
  Top = 109
  Width = 325
  Height = 152
  Caption = 'Culori'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 8
    Width = 25
    Height = 13
    Caption = 'Rosu'
  end
  object Label2: TLabel
    Left = 8
    Top = 32
    Width = 28
    Height = 13
    Caption = 'Verde'
  end
  object Label3: TLabel
    Left = 0
    Top = 56
    Width = 38
    Height = 13
    Caption = 'Albastru'
  end
  object Label4: TLabel
    Left = 0
    Top = 96
    Width = 36
    Height = 13
    Caption = 'Culoare'
  end
  object sbrosu: TScrollBar
    Left = 48
    Top = 8
    Width = 201
    Height = 16
    Max = 255
    PageSize = 0
    TabOrder = 0
    OnChange = sbrosuChange
  end
  object sbverde: TScrollBar
    Left = 48
    Top = 32
    Width = 201
    Height = 16
    Max = 255
    PageSize = 0
    TabOrder = 1
    OnChange = sbverdeChange
  end
  object sbalbastru: TScrollBar
    Left = 48
    Top = 56
    Width = 201
    Height = 16
    Max = 255
    PageSize = 0
    TabOrder = 2
    OnChange = sbalbastruChange
  end
  object edculoare: TEdit
    Left = 40
    Top = 80
    Width = 145
    Height = 21
    TabOrder = 3
  end
  object edrosu: TEdit
    Left = 256
    Top = 8
    Width = 57
    Height = 21
    TabOrder = 4
    OnChange = edrosuChange
  end
  object edverde: TEdit
    Left = 256
    Top = 32
    Width = 57
    Height = 21
    TabOrder = 5
    OnChange = edverdeChange
  end
  object edalbastru: TEdit
    Left = 256
    Top = 56
    Width = 57
    Height = 21
    TabOrder = 6
    OnChange = edalbastruChange
  end
  object BitBtn1: TBitBtn
    Left = 192
    Top = 80
    Width = 121
    Height = 41
    TabOrder = 7
    Kind = bkClose
  end
end
