object Form1: TForm1
  Left = 192
  Top = 107
  Width = 696
  Height = 480
  Caption = 'Ziua de nastere'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label4: TLabel
    Left = 120
    Top = 200
    Width = 121
    Height = 13
    Caption = 'Ziua din saptamana'
  end
  object GroupBox1: TGroupBox
    Left = 8
    Top = 8
    Width = 441
    Height = 137
    Caption = 'GroupBox1'
    TabOrder = 0
    object Label1: TLabel
      Left = 56
      Top = 40
      Width = 21
      Height = 13
      Caption = 'Ziua'
    end
    object Label2: TLabel
      Left = 176
      Top = 40
      Width = 24
      Height = 13
      Caption = 'Luna'
    end
    object Label3: TLabel
      Left = 328
      Top = 40
      Width = 21
      Height = 13
      Caption = 'Anul'
    end
    object cdan: TSpinEdit
      Left = 288
      Top = 64
      Width = 121
      Height = 22
      MaxValue = 0
      MinValue = 0
      TabOrder = 0
      Value = 0
    end
    object edzi: TSpinEdit
      Left = 48
      Top = 64
      Width = 49
      Height = 22
      MaxValue = 0
      MinValue = 0
      TabOrder = 1
      Value = 0
    end
    object cbluna: TComboBox
      Left = 152
      Top = 64
      Width = 113
      Height = 21
      ItemHeight = 13
      TabOrder = 2
      Items.Strings = (
        'januarie'
        'februarie'
        'martie'
        'aprilie'
        'mai'
        'junie'
        'julie'
        'august'
        'septembrie'
        'octombrie'
        'noiembrie'
        'decembrie')
    end
  end
  object edziua: TEdit
    Left = 272
    Top = 192
    Width = 121
    Height = 21
    TabOrder = 1
  end
  object BitBtn1: TBitBtn
    Left = 136
    Top = 264
    Width = 75
    Height = 25
    Caption = 'Ziua'
    TabOrder = 2
    OnClick = BitBtn1Click
    Kind = bkHelp
  end
  object BitBtn2: TBitBtn
    Left = 304
    Top = 264
    Width = 75
    Height = 25
    TabOrder = 3
    Kind = bkClose
  end
end
