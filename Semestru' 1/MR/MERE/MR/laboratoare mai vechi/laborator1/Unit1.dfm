object Form1: TForm1
  Left = 192
  Top = 111
  Width = 696
  Height = 480
  Caption = 'primul proiect delphi'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Menu = MainMenu1
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object ToolBar1: TToolBar
    Left = 0
    Top = 0
    Width = 688
    Height = 29
    Caption = 'ToolBar1'
    TabOrder = 0
    object BitBtn1: TBitBtn
      Left = 0
      Top = 2
      Width = 75
      Height = 22
      Caption = 'unu'
      TabOrder = 0
      OnClick = BitBtn1Click
      OnMouseMove = BitBtn1MouseMove
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000120B0000120B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00300000000000
        0000377777777777777703030303030303037F7F7F7F7F7F7F7F000000000000
        00007777777777777777933393303933337073F37F37F73F3377393393303393
        379037FF7F37F37FF777379793303379793037777337F3777737339933303339
        93303377F3F7F3F77F3733993930393993303377F737F7377FF7399993303399
        999037777337F377777793993330333393307377FF37F3337FF7333993303333
        993033377F37F33377F7333993303333993033377337F3337737333333303333
        33303FFFFFF7FFFFFFF700000000000000007777777777777777030303030303
        03037F7F7F7F7F7F7F7F00000000000000007777777777777777}
      NumGlyphs = 2
    end
    object BitBtn2: TBitBtn
      Left = 75
      Top = 2
      Width = 75
      Height = 22
      Caption = 'doi'
      TabOrder = 1
      OnClick = BitBtn2Click
      OnMouseMove = BitBtn2MouseMove
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000120B0000120B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333303333
        333333333337FF3333333333330003333333333333777F333333333333080333
        3333333F33777FF33F3333B33B000B33B3333373F777773F7333333BBB0B0BBB
        33333337737F7F77F333333BBB0F0BBB33333337337373F73F3333BBB0F7F0BB
        B333337F3737F73F7F3333BB0FB7BF0BB3333F737F37F37F73FFBBBB0BF7FB0B
        BBB3773F7F37337F377333BB0FBFBF0BB333337F73F333737F3333BBB0FBF0BB
        B3333373F73FF7337333333BBB000BBB33333337FF777337F333333BBBBBBBBB
        3333333773FF3F773F3333B33BBBBB33B33333733773773373333333333B3333
        333333333337F33333333333333B333333333333333733333333}
      NumGlyphs = 2
    end
    object BitBtn3: TBitBtn
      Left = 150
      Top = 2
      Width = 75
      Height = 22
      TabOrder = 2
      Kind = bkClose
    end
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 415
    Width = 688
    Height = 19
    Panels = <>
    SimplePanel = True
    SimpleText = 'autor:abc'
  end
  object MainMenu1: TMainMenu
    Left = 80
    Top = 40
    object meniu11: TMenuItem
      Caption = 'meniu1'
      object meniu111: TMenuItem
        Caption = '&meniu11'
        ShortCut = 16451
      end
      object meniu121: TMenuItem
        Caption = '&meniu12'
        ShortCut = 16470
      end
      object N1: TMenuItem
        Caption = '-'
      end
      object meniu131: TMenuItem
        Caption = 'meniu13'
        OnClick = meniu131Click
      end
    end
    object meniu21: TMenuItem
      Caption = 'meniu2'
      object meniu211: TMenuItem
        Caption = 'meniu21'
      end
      object meniu221: TMenuItem
        Caption = 'meniu22'
      end
      object N2: TMenuItem
        Caption = '-'
      end
      object meniu231: TMenuItem
        Caption = 'meniu23'
      end
    end
    object quit1: TMenuItem
      Caption = 'quit'
      OnClick = quit1Click
    end
  end
end
