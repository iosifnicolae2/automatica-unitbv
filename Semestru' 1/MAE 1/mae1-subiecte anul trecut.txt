===MAE 1===


BILETUL 4:  Studiul matematic al stabilitatii S.A.E. prin liniarizarea ec de miscare;
            Determinarea Puterii motoarelor folosind serviciul S1;
            Ce este Cuplul de Sarcina Reactiv.

BILETUL 5:  Definiti Momentul de Inertie + Unitatile de Masura; 
            Prezentati Serviciile S1-S4 & Marimile caracteristice; 
            Ce este intrefierul? (Este acel spatiu intre stator si rotor :P)


!!! ATENTIE !!!

Numarul biletului de examen s-a schimbat de la un an la altul, ca atare, biletele 4 si 5
ar putea avea alte numere (1,2), (5,8). Dar totalul numarului de bilete propuse de prof 
este de 8. Deci sansele sunt minime!