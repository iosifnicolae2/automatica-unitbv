
_main:

;Masina de Gaurit.c,9 :: 		void main() {
;Masina de Gaurit.c,12 :: 		A=1;
	MOVLW      1
	MOVWF      _A+0
;Masina de Gaurit.c,15 :: 		INTCON.GIE = 1;
	BSF        INTCON+0, 7
;Masina de Gaurit.c,16 :: 		INTCON.INTE = 1;
	BSF        INTCON+0, 4
;Masina de Gaurit.c,19 :: 		TRISD.RD0=1;
	BSF        TRISD+0, 0
;Masina de Gaurit.c,20 :: 		TRISD.RD1=1;
	BSF        TRISD+0, 1
;Masina de Gaurit.c,21 :: 		TRISD.RD2=1;
	BSF        TRISD+0, 2
;Masina de Gaurit.c,23 :: 		TRISB.RB0=1;
	BSF        TRISB+0, 0
;Masina de Gaurit.c,25 :: 		TRISD.RD3=0;
	BCF        TRISD+0, 3
;Masina de Gaurit.c,26 :: 		TRISD.RD4=0;
	BCF        TRISD+0, 4
;Masina de Gaurit.c,27 :: 		TRISD.RD5=0;
	BCF        TRISD+0, 5
;Masina de Gaurit.c,28 :: 		TRISD.RD6=0;
	BCF        TRISD+0, 6
;Masina de Gaurit.c,31 :: 		while(1)
L_main0:
;Masina de Gaurit.c,36 :: 		if (PORTD.RD0==1) oldstate = 1;
	BTFSS      PORTD+0, 0
	GOTO       L_main2
	MOVLW      1
	MOVWF      _oldstate+0
L_main2:
;Masina de Gaurit.c,37 :: 		if (oldstate && PORTD.RD0==0 && count<10) {
	MOVF       _oldstate+0, 0
	BTFSC      STATUS+0, 2
	GOTO       L_main5
	BTFSC      PORTD+0, 0
	GOTO       L_main5
	MOVLW      128
	XORWF      _count+1, 0
	MOVWF      R0+0
	MOVLW      128
	SUBWF      R0+0, 0
	BTFSS      STATUS+0, 2
	GOTO       L__main22
	MOVLW      10
	SUBWF      _count+0, 0
L__main22:
	BTFSC      STATUS+0, 0
	GOTO       L_main5
L__main21:
;Masina de Gaurit.c,38 :: 		count++;
	INCF       _count+0, 1
	BTFSC      STATUS+0, 2
	INCF       _count+1, 1
;Masina de Gaurit.c,39 :: 		oldstate = 0; }
	CLRF       _oldstate+0
L_main5:
;Masina de Gaurit.c,41 :: 		if (PORTD.RD1==1) oldstate2 = 1;
	BTFSS      PORTD+0, 1
	GOTO       L_main6
	MOVLW      1
	MOVWF      _oldstate2+0
L_main6:
;Masina de Gaurit.c,42 :: 		if (oldstate2 && PORTD.RD1==0 && count>0) {
	MOVF       _oldstate2+0, 0
	BTFSC      STATUS+0, 2
	GOTO       L_main9
	BTFSC      PORTD+0, 1
	GOTO       L_main9
	MOVLW      128
	MOVWF      R0+0
	MOVLW      128
	XORWF      _count+1, 0
	SUBWF      R0+0, 0
	BTFSS      STATUS+0, 2
	GOTO       L__main23
	MOVF       _count+0, 0
	SUBLW      0
L__main23:
	BTFSC      STATUS+0, 0
	GOTO       L_main9
L__main20:
;Masina de Gaurit.c,43 :: 		count--;
	MOVLW      1
	SUBWF      _count+0, 1
	BTFSS      STATUS+0, 0
	DECF       _count+1, 1
;Masina de Gaurit.c,44 :: 		oldstate2 = 0; }
	CLRF       _oldstate2+0
L_main9:
;Masina de Gaurit.c,48 :: 		if (A==1) {A=0; PORTD.RD6=0; PORTD.RD5=0; PORTD.RD4=0; PORTD.RD3=1; B=1;}
	MOVF       _A+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main10
	CLRF       _A+0
	BCF        PORTD+0, 6
	BCF        PORTD+0, 5
	BCF        PORTD+0, 4
	BSF        PORTD+0, 3
	MOVLW      1
	MOVWF      _B+0
L_main10:
;Masina de Gaurit.c,49 :: 		if ((B==1) &&  (PORTD.RD2=1)) {B=0; PORTD.RD3=0; PORTD.RD4=1; Vdelay_ms(count*1000); g=1;}
	MOVF       _B+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main13
	BSF        PORTD+0, 2
	BTFSS      PORTD+0, 2
	GOTO       L_main13
L__main19:
	CLRF       _B+0
	BCF        PORTD+0, 3
	BSF        PORTD+0, 4
	MOVF       _count+0, 0
	MOVWF      R0+0
	MOVF       _count+1, 0
	MOVWF      R0+1
	MOVLW      232
	MOVWF      R4+0
	MOVLW      3
	MOVWF      R4+1
	CALL       _Mul_16x16_U+0
	MOVF       R0+0, 0
	MOVWF      FARG_VDelay_ms_Time_ms+0
	MOVF       R0+1, 0
	MOVWF      FARG_VDelay_ms_Time_ms+1
	CALL       _VDelay_ms+0
	MOVLW      1
	MOVWF      _g+0
L_main13:
;Masina de Gaurit.c,50 :: 		if (g==1) {g=0; PORTD.RD4=0; PORTD.RD5=1; Delay_ms(2000); h=1;}
	MOVF       _g+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main14
	CLRF       _g+0
	BCF        PORTD+0, 4
	BSF        PORTD+0, 5
	MOVLW      3
	MOVWF      R11+0
	MOVLW      138
	MOVWF      R12+0
	MOVLW      85
	MOVWF      R13+0
L_main15:
	DECFSZ     R13+0, 1
	GOTO       L_main15
	DECFSZ     R12+0, 1
	GOTO       L_main15
	DECFSZ     R11+0, 1
	GOTO       L_main15
	NOP
	NOP
	MOVLW      1
	MOVWF      _h+0
L_main14:
;Masina de Gaurit.c,51 :: 		if (h==1) {h=0; PORTD.RD5=0; PORTD.RD6=1; Delay_ms(1000); A=1;}
	MOVF       _h+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main16
	CLRF       _h+0
	BCF        PORTD+0, 5
	BSF        PORTD+0, 6
	MOVLW      2
	MOVWF      R11+0
	MOVLW      69
	MOVWF      R12+0
	MOVLW      169
	MOVWF      R13+0
L_main17:
	DECFSZ     R13+0, 1
	GOTO       L_main17
	DECFSZ     R12+0, 1
	GOTO       L_main17
	DECFSZ     R11+0, 1
	GOTO       L_main17
	NOP
	NOP
	MOVLW      1
	MOVWF      _A+0
L_main16:
;Masina de Gaurit.c,53 :: 		}
	GOTO       L_main0
;Masina de Gaurit.c,55 :: 		}
	GOTO       $+0
; end of _main

_interrupt:
	MOVWF      R15+0
	SWAPF      STATUS+0, 0
	CLRF       STATUS+0
	MOVWF      ___saveSTATUS+0
	MOVF       PCLATH+0, 0
	MOVWF      ___savePCLATH+0
	CLRF       PCLATH+0

;Masina de Gaurit.c,57 :: 		void interrupt()
;Masina de Gaurit.c,59 :: 		if (INTCON.INTF == 1)
	BTFSS      INTCON+0, 1
	GOTO       L_interrupt18
;Masina de Gaurit.c,61 :: 		PORTD.RD3=0; PORTD.RD4=0; PORTD.RD5=0; PORTD.RD6=1;
	BCF        PORTD+0, 3
	BCF        PORTD+0, 4
	BCF        PORTD+0, 5
	BSF        PORTD+0, 6
;Masina de Gaurit.c,62 :: 		A=1;
	MOVLW      1
	MOVWF      _A+0
;Masina de Gaurit.c,63 :: 		B=0;
	CLRF       _B+0
;Masina de Gaurit.c,64 :: 		g=0;
	CLRF       _g+0
;Masina de Gaurit.c,65 :: 		h=0;
	CLRF       _h+0
;Masina de Gaurit.c,66 :: 		INTCON.INTF = 0;
	BCF        INTCON+0, 1
;Masina de Gaurit.c,67 :: 		}
L_interrupt18:
;Masina de Gaurit.c,68 :: 		}
L__interrupt24:
	MOVF       ___savePCLATH+0, 0
	MOVWF      PCLATH+0
	SWAPF      ___saveSTATUS+0, 0
	MOVWF      STATUS+0
	SWAPF      R15+0, 1
	SWAPF      R15+0, 0
	RETFIE
; end of _interrupt
