
_main:
	MOVLW      63
	MOVWF      main_sir1_L0+0
	MOVLW      6
	MOVWF      main_sir1_L0+1
	MOVLW      91
	MOVWF      main_sir1_L0+2
	MOVLW      79
	MOVWF      main_sir1_L0+3
	MOVLW      102
	MOVWF      main_sir1_L0+4
	MOVLW      109
	MOVWF      main_sir1_L0+5
	MOVLW      125
	MOVWF      main_sir1_L0+6
	MOVLW      7
	MOVWF      main_sir1_L0+7
	MOVLW      127
	MOVWF      main_sir1_L0+8
	MOVLW      111
	MOVWF      main_sir1_L0+9
	MOVLW      63
	MOVWF      main_sir1_L0+10
	MOVLW      6
	MOVWF      main_sir1_L0+11
	MOVLW      91
	MOVWF      main_sir1_L0+12
	MOVLW      79
	MOVWF      main_sir1_L0+13
	MOVLW      102
	MOVWF      main_sir1_L0+14
	MOVLW      63
	MOVWF      main_sir_L0+0
	MOVLW      63
	MOVWF      main_sir_L0+1
	MOVLW      63
	MOVWF      main_sir_L0+2
	MOVLW      63
	MOVWF      main_sir_L0+3
	MOVLW      63
	MOVWF      main_sir_L0+4
	MOVLW      63
	MOVWF      main_sir_L0+5
	MOVLW      63
	MOVWF      main_sir_L0+6
	MOVLW      63
	MOVWF      main_sir_L0+7
	MOVLW      63
	MOVWF      main_sir_L0+8
	MOVLW      63
	MOVWF      main_sir_L0+9
	MOVLW      6
	MOVWF      main_sir_L0+10
	MOVLW      6
	MOVWF      main_sir_L0+11
	MOVLW      6
	MOVWF      main_sir_L0+12
	MOVLW      6
	MOVWF      main_sir_L0+13
	MOVLW      6
	MOVWF      main_sir_L0+14
	CLRF       main_i_L0+0
;numarare.c,2 :: 		
;numarare.c,6 :: 		
	MOVLW      128
	MOVWF      TRISD+0
;numarare.c,7 :: 		
	MOVLW      128
	MOVWF      TRISC+0
;numarare.c,8 :: 		
	CLRF       TRISB+0
;numarare.c,9 :: 		
	BCF        PORTB+0, 0
;numarare.c,10 :: 		
	MOVF       main_sir_L0+0, 0
	MOVWF      PORTD+0
;numarare.c,11 :: 		
	MOVF       main_sir1_L0+0, 0
	MOVWF      PORTC+0
;numarare.c,13 :: 		
L_main0:
;numarare.c,15 :: 		
	BTFSS      PORTD+0, 7
	GOTO       L_main2
;numarare.c,17 :: 		
	MOVLW      128
	XORWF      main_i_L0+0, 0
	MOVWF      R0+0
	MOVLW      128
	XORLW      14
	SUBWF      R0+0, 0
	BTFSC      STATUS+0, 0
	GOTO       L_main3
;numarare.c,19 :: 		
	INCF       main_i_L0+0, 1
;numarare.c,20 :: 		
	MOVF       main_i_L0+0, 0
	ADDLW      main_sir_L0+0
	MOVWF      FSR
	MOVF       INDF+0, 0
	MOVWF      PORTD+0
;numarare.c,21 :: 		
	MOVF       main_i_L0+0, 0
	ADDLW      main_sir1_L0+0
	MOVWF      FSR
	MOVF       INDF+0, 0
	MOVWF      PORTC+0
;numarare.c,22 :: 		
	MOVLW      130
	MOVWF      R12+0
	MOVLW      221
	MOVWF      R13+0
L_main4:
	DECFSZ     R13+0, 1
	GOTO       L_main4
	DECFSZ     R12+0, 1
	GOTO       L_main4
	NOP
	NOP
;numarare.c,23 :: 		
L_main3:
;numarare.c,24 :: 		
	GOTO       L_main5
L_main2:
;numarare.c,26 :: 		
	BTFSS      PORTC+0, 7
	GOTO       L_main6
;numarare.c,29 :: 		
	MOVLW      128
	XORLW      0
	MOVWF      R0+0
	MOVLW      128
	XORWF      main_i_L0+0, 0
	SUBWF      R0+0, 0
	BTFSC      STATUS+0, 0
	GOTO       L_main7
;numarare.c,31 :: 		
	DECF       main_i_L0+0, 1
;numarare.c,32 :: 		
	MOVF       main_i_L0+0, 0
	ADDLW      main_sir_L0+0
	MOVWF      FSR
	MOVF       INDF+0, 0
	MOVWF      PORTD+0
;numarare.c,33 :: 		
	MOVF       main_i_L0+0, 0
	ADDLW      main_sir1_L0+0
	MOVWF      FSR
	MOVF       INDF+0, 0
	MOVWF      PORTC+0
;numarare.c,34 :: 		
	MOVLW      130
	MOVWF      R12+0
	MOVLW      221
	MOVWF      R13+0
L_main8:
	DECFSZ     R13+0, 1
	GOTO       L_main8
	DECFSZ     R12+0, 1
	GOTO       L_main8
	NOP
	NOP
;numarare.c,35 :: 		
L_main7:
;numarare.c,36 :: 		
L_main6:
L_main5:
;numarare.c,37 :: 		
	MOVF       main_i_L0+0, 0
	XORLW      14
	BTFSS      STATUS+0, 2
	GOTO       L_main9
;numarare.c,38 :: 		
	BSF        PORTB+0, 0
	GOTO       L_main10
L_main9:
;numarare.c,40 :: 		
	BCF        PORTB+0, 0
L_main10:
;numarare.c,43 :: 		
	GOTO       L_main0
;numarare.c,44 :: 		
	GOTO       $+0
; end of _main
