
_main:

;MyProject3.c,1 :: 		void main() {
;MyProject3.c,3 :: 		int zar = 1;
	MOVLW      1
	MOVWF      main_zar_L0+0
	MOVLW      0
	MOVWF      main_zar_L0+1
;MyProject3.c,5 :: 		TRISB= 0B00000000;
	CLRF       TRISB+0
;MyProject3.c,7 :: 		PORTB = 0;
	CLRF       PORTB+0
;MyProject3.c,10 :: 		while(1)
L_main0:
;MyProject3.c,12 :: 		zar = zar +1;
	INCF       main_zar_L0+0, 1
	BTFSC      STATUS+0, 2
	INCF       main_zar_L0+1, 1
;MyProject3.c,14 :: 		if (zar == 7)
	MOVLW      0
	XORWF      main_zar_L0+1, 0
	BTFSS      STATUS+0, 2
	GOTO       L__main17
	MOVLW      7
	XORWF      main_zar_L0+0, 0
L__main17:
	BTFSS      STATUS+0, 2
	GOTO       L_main2
;MyProject3.c,16 :: 		zar = 1;
	MOVLW      1
	MOVWF      main_zar_L0+0
	MOVLW      0
	MOVWF      main_zar_L0+1
;MyProject3.c,18 :: 		}
L_main2:
;MyProject3.c,21 :: 		if (PORTB.RB0 == 1)
	BTFSS      PORTB+0, 0
	GOTO       L_main3
;MyProject3.c,24 :: 		if (zar == 1)
	MOVLW      0
	XORWF      main_zar_L0+1, 0
	BTFSS      STATUS+0, 2
	GOTO       L__main18
	MOVLW      1
	XORWF      main_zar_L0+0, 0
L__main18:
	BTFSS      STATUS+0, 2
	GOTO       L_main4
;MyProject3.c,26 :: 		PORTB = 0b00001100;
	MOVLW      12
	MOVWF      PORTB+0
;MyProject3.c,27 :: 		delay_ms(1);
	MOVLW      3
	MOVWF      R12+0
	MOVLW      151
	MOVWF      R13+0
L_main5:
	DECFSZ     R13+0, 1
	GOTO       L_main5
	DECFSZ     R12+0, 1
	GOTO       L_main5
	NOP
	NOP
;MyProject3.c,29 :: 		}
L_main4:
;MyProject3.c,31 :: 		if (zar == 2)
	MOVLW      0
	XORWF      main_zar_L0+1, 0
	BTFSS      STATUS+0, 2
	GOTO       L__main19
	MOVLW      2
	XORWF      main_zar_L0+0, 0
L__main19:
	BTFSS      STATUS+0, 2
	GOTO       L_main6
;MyProject3.c,33 :: 		PORTB = 0b10110110;
	MOVLW      182
	MOVWF      PORTB+0
;MyProject3.c,34 :: 		delay_ms(1);
	MOVLW      3
	MOVWF      R12+0
	MOVLW      151
	MOVWF      R13+0
L_main7:
	DECFSZ     R13+0, 1
	GOTO       L_main7
	DECFSZ     R12+0, 1
	GOTO       L_main7
	NOP
	NOP
;MyProject3.c,36 :: 		}
L_main6:
;MyProject3.c,38 :: 		if (zar == 3)
	MOVLW      0
	XORWF      main_zar_L0+1, 0
	BTFSS      STATUS+0, 2
	GOTO       L__main20
	MOVLW      3
	XORWF      main_zar_L0+0, 0
L__main20:
	BTFSS      STATUS+0, 2
	GOTO       L_main8
;MyProject3.c,40 :: 		PORTB = 0b10011110;
	MOVLW      158
	MOVWF      PORTB+0
;MyProject3.c,41 :: 		delay_ms(1);
	MOVLW      3
	MOVWF      R12+0
	MOVLW      151
	MOVWF      R13+0
L_main9:
	DECFSZ     R13+0, 1
	GOTO       L_main9
	DECFSZ     R12+0, 1
	GOTO       L_main9
	NOP
	NOP
;MyProject3.c,43 :: 		}
L_main8:
;MyProject3.c,45 :: 		if (zar == 4)
	MOVLW      0
	XORWF      main_zar_L0+1, 0
	BTFSS      STATUS+0, 2
	GOTO       L__main21
	MOVLW      4
	XORWF      main_zar_L0+0, 0
L__main21:
	BTFSS      STATUS+0, 2
	GOTO       L_main10
;MyProject3.c,47 :: 		PORTB =0b11001100;
	MOVLW      204
	MOVWF      PORTB+0
;MyProject3.c,48 :: 		delay_ms(1);
	MOVLW      3
	MOVWF      R12+0
	MOVLW      151
	MOVWF      R13+0
L_main11:
	DECFSZ     R13+0, 1
	GOTO       L_main11
	DECFSZ     R12+0, 1
	GOTO       L_main11
	NOP
	NOP
;MyProject3.c,50 :: 		}
L_main10:
;MyProject3.c,52 :: 		if (zar == 5)
	MOVLW      0
	XORWF      main_zar_L0+1, 0
	BTFSS      STATUS+0, 2
	GOTO       L__main22
	MOVLW      5
	XORWF      main_zar_L0+0, 0
L__main22:
	BTFSS      STATUS+0, 2
	GOTO       L_main12
;MyProject3.c,54 :: 		PORTB =0b11011010;
	MOVLW      218
	MOVWF      PORTB+0
;MyProject3.c,55 :: 		delay_ms(1);
	MOVLW      3
	MOVWF      R12+0
	MOVLW      151
	MOVWF      R13+0
L_main13:
	DECFSZ     R13+0, 1
	GOTO       L_main13
	DECFSZ     R12+0, 1
	GOTO       L_main13
	NOP
	NOP
;MyProject3.c,57 :: 		}
L_main12:
;MyProject3.c,59 :: 		if (zar == 6)
	MOVLW      0
	XORWF      main_zar_L0+1, 0
	BTFSS      STATUS+0, 2
	GOTO       L__main23
	MOVLW      6
	XORWF      main_zar_L0+0, 0
L__main23:
	BTFSS      STATUS+0, 2
	GOTO       L_main14
;MyProject3.c,61 :: 		PORTB = 0b11111010;
	MOVLW      250
	MOVWF      PORTB+0
;MyProject3.c,62 :: 		delay_ms(1);
	MOVLW      3
	MOVWF      R12+0
	MOVLW      151
	MOVWF      R13+0
L_main15:
	DECFSZ     R13+0, 1
	GOTO       L_main15
	DECFSZ     R12+0, 1
	GOTO       L_main15
	NOP
	NOP
;MyProject3.c,64 :: 		}
L_main14:
;MyProject3.c,66 :: 		}
L_main3:
;MyProject3.c,67 :: 		}
	GOTO       L_main0
;MyProject3.c,68 :: 		}
L_end_main:
	GOTO       $+0
; end of _main
