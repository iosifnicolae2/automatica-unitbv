
_ADCInit:
;ADC.c,17 :: 		void ADCInit()
;ADC.c,19 :: 		ANSEL.AN0 =255; //Analog input
	BSF        ANSEL+0, 0
;ADC.c,21 :: 		ADCON0.ADON = 1; //Left Justified
	BSF        ADCON0+0, 0
;ADC.c,23 :: 		ADCON0.VCFG1 = 0; //Vss reference
	BCF        ADCON0+0, 6
;ADC.c,24 :: 		ADCON0.VCFG0 = 0; //Vdd reference
	BCF        ADCON0+0, 5
;ADC.c,26 :: 		ADCON0.CHS2 = 0;
	BCF        ADCON0+0, 4
;ADC.c,27 :: 		ADCON0.CHS1 = 0;  //Channel 00
	BCF        ADCON0+0, 3
;ADC.c,28 :: 		ADCON0.CHS0 = 0;
	BCF        ADCON0+0, 2
;ADC.c,30 :: 		ADCON1.ADCS2 = 0;
	BCF        ADCON1+0, 6
;ADC.c,31 :: 		ADCON1.ADCS2 = 0; //conversin clock 4us, 001
	BCF        ADCON1+0, 6
;ADC.c,32 :: 		ADCON1.ADCS2 = 1;
	BSF        ADCON1+0, 6
;ADC.c,33 :: 		}
	RETURN
; end of _ADCInit

_ADCRead:
	CLRF       ADCRead_conv_L0+0
	CLRF       ADCRead_conv_L0+1
;ADC.c,35 :: 		unsigned int ADCRead(unsigned char ch)
;ADC.c,38 :: 		if(ch>7) return 0;  //Invalid Channel
	MOVF       FARG_ADCRead_ch+0, 0
	SUBLW      7
	BTFSC      STATUS+0, 0
	GOTO       L_ADCRead0
	CLRF       R0+0
	CLRF       R0+1
	RETURN
L_ADCRead0:
;ADC.c,40 :: 		ADCON0=(ch<<2);   //Select ADC Channel
	MOVF       FARG_ADCRead_ch+0, 0
	MOVWF      R0+0
	RLF        R0+0, 1
	BCF        R0+0, 0
	RLF        R0+0, 1
	BCF        R0+0, 0
	MOVF       R0+0, 0
	MOVWF      ADCON0+0
;ADC.c,42 :: 		ADCON0.ADON=1;  //switch on the adc module
	BSF        ADCON0+0, 0
;ADC.c,44 :: 		ADCON0.GO_DONE=1;//Start conversion
	BSF        ADCON0+0, 1
;ADC.c,46 :: 		while(ADCON0.GO_DONE){} //wait for the conversion to finish
L_ADCRead1:
	BTFSS      ADCON0+0, 1
	GOTO       L_ADCRead2
	GOTO       L_ADCRead1
L_ADCRead2:
;ADC.c,48 :: 		ADCON0.ADON=0;  //switch off adc
	BCF        ADCON0+0, 0
;ADC.c,49 :: 		conv |= ADRESH << 2 | ADRESL >> 6   ;
	MOVF       ADRESH+0, 0
	MOVWF      R2+0
	CLRF       R2+1
	RLF        R2+0, 1
	RLF        R2+1, 1
	BCF        R2+0, 0
	RLF        R2+0, 1
	RLF        R2+1, 1
	BCF        R2+0, 0
	MOVLW      6
	MOVWF      R1+0
	MOVF       ADRESL+0, 0
	MOVWF      R0+0
	MOVF       R1+0, 0
L__ADCRead6:
	BTFSC      STATUS+0, 2
	GOTO       L__ADCRead7
	RRF        R0+0, 1
	BCF        R0+0, 7
	ADDLW      255
	GOTO       L__ADCRead6
L__ADCRead7:
	MOVLW      0
	MOVWF      R0+1
	MOVF       R2+0, 0
	IORWF      R0+0, 1
	MOVF       R2+1, 0
	IORWF      R0+1, 1
	MOVF       ADCRead_conv_L0+0, 0
	IORWF      R0+0, 1
	MOVF       ADCRead_conv_L0+1, 0
	IORWF      R0+1, 1
	MOVF       R0+0, 0
	MOVWF      ADCRead_conv_L0+0
	MOVF       R0+1, 0
	MOVWF      ADCRead_conv_L0+1
;ADC.c,50 :: 		return conv;
;ADC.c,51 :: 		}
	RETURN
; end of _ADCRead

_main:
	CLRF       main_tensiune_L0+0
	CLRF       main_tensiune_L0+1
	MOVLW      0
	MOVWF      main_step_L0+0
	MOVLW      0
	MOVWF      main_step_L0+1
	MOVLW      32
	MOVWF      main_step_L0+2
	MOVLW      119
	MOVWF      main_step_L0+3
;ADC.c,53 :: 		void main() {
;ADC.c,59 :: 		ADCInit();
	CALL       _ADCInit+0
;ADC.c,60 :: 		Lcd_Init();
	CALL       _Lcd_Init+0
;ADC.c,61 :: 		while(1)
L_main3:
;ADC.c,63 :: 		val =ADCRead(0);
	CLRF       FARG_ADCRead_ch+0
	CALL       _ADCRead+0
;ADC.c,64 :: 		nr=val*step*1000;
	CALL       _Word2Double+0
	MOVF       main_step_L0+0, 0
	MOVWF      R4+0
	MOVF       main_step_L0+1, 0
	MOVWF      R4+1
	MOVF       main_step_L0+2, 0
	MOVWF      R4+2
	MOVF       main_step_L0+3, 0
	MOVWF      R4+3
	CALL       _Mul_32x32_FP+0
	MOVLW      0
	MOVWF      R4+0
	MOVLW      0
	MOVWF      R4+1
	MOVLW      122
	MOVWF      R4+2
	MOVLW      136
	MOVWF      R4+3
	CALL       _Mul_32x32_FP+0
;ADC.c,65 :: 		tensiune=nr;
	CALL       _Double2Int+0
	MOVF       R0+0, 0
	MOVWF      main_tensiune_L0+0
	MOVF       R0+1, 0
	MOVWF      main_tensiune_L0+1
;ADC.c,66 :: 		sir[0]='0'+tensiune/1000;
	MOVLW      232
	MOVWF      R4+0
	MOVLW      3
	MOVWF      R4+1
	CALL       _Div_16x16_S+0
	MOVF       R0+0, 0
	ADDLW      48
	MOVWF      main_sir_L0+0
;ADC.c,67 :: 		tensiune = tensiune%1000;
	MOVLW      232
	MOVWF      R4+0
	MOVLW      3
	MOVWF      R4+1
	MOVF       main_tensiune_L0+0, 0
	MOVWF      R0+0
	MOVF       main_tensiune_L0+1, 0
	MOVWF      R0+1
	CALL       _Div_16x16_S+0
	MOVF       R8+0, 0
	MOVWF      R0+0
	MOVF       R8+1, 0
	MOVWF      R0+1
	MOVF       R0+0, 0
	MOVWF      main_tensiune_L0+0
	MOVF       R0+1, 0
	MOVWF      main_tensiune_L0+1
;ADC.c,68 :: 		sir[1]=',';
	MOVLW      44
	MOVWF      main_sir_L0+1
;ADC.c,69 :: 		sir[2]='0'+tensiune/100;
	MOVLW      100
	MOVWF      R4+0
	MOVLW      0
	MOVWF      R4+1
	CALL       _Div_16x16_S+0
	MOVF       R0+0, 0
	ADDLW      48
	MOVWF      main_sir_L0+2
;ADC.c,70 :: 		tensiune=tensiune%100;
	MOVLW      100
	MOVWF      R4+0
	MOVLW      0
	MOVWF      R4+1
	MOVF       main_tensiune_L0+0, 0
	MOVWF      R0+0
	MOVF       main_tensiune_L0+1, 0
	MOVWF      R0+1
	CALL       _Div_16x16_S+0
	MOVF       R8+0, 0
	MOVWF      R0+0
	MOVF       R8+1, 0
	MOVWF      R0+1
	MOVF       R0+0, 0
	MOVWF      main_tensiune_L0+0
	MOVF       R0+1, 0
	MOVWF      main_tensiune_L0+1
;ADC.c,71 :: 		sir[3]='0'+tensiune/10;
	MOVLW      10
	MOVWF      R4+0
	MOVLW      0
	MOVWF      R4+1
	CALL       _Div_16x16_S+0
	MOVF       R0+0, 0
	ADDLW      48
	MOVWF      main_sir_L0+3
;ADC.c,72 :: 		tensiune=tensiune%10;
	MOVLW      10
	MOVWF      R4+0
	MOVLW      0
	MOVWF      R4+1
	MOVF       main_tensiune_L0+0, 0
	MOVWF      R0+0
	MOVF       main_tensiune_L0+1, 0
	MOVWF      R0+1
	CALL       _Div_16x16_S+0
	MOVF       R8+0, 0
	MOVWF      R0+0
	MOVF       R8+1, 0
	MOVWF      R0+1
	MOVF       R0+0, 0
	MOVWF      main_tensiune_L0+0
	MOVF       R0+1, 0
	MOVWF      main_tensiune_L0+1
;ADC.c,73 :: 		sir[4]='0'+tensiune;
	MOVF       R0+0, 0
	ADDLW      48
	MOVWF      main_sir_L0+4
;ADC.c,74 :: 		sir[5]='\0';
	CLRF       main_sir_L0+5
;ADC.c,77 :: 		Lcd_Out(2,1,sir);
	MOVLW      2
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      1
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      main_sir_L0+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
;ADC.c,79 :: 		Delay_ms(50);
	MOVLW      17
	MOVWF      R12+0
	MOVLW      58
	MOVWF      R13+0
L_main5:
	DECFSZ     R13+0, 1
	GOTO       L_main5
	DECFSZ     R12+0, 1
	GOTO       L_main5
	NOP
;ADC.c,80 :: 		}
	GOTO       L_main3
;ADC.c,81 :: 		}
	GOTO       $+0
; end of _main
