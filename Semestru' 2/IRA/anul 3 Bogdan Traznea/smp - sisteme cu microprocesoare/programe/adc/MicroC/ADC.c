// Lcd pinout settings
sbit LCD_RS at RB4_bit;
sbit LCD_EN at RB5_bit;
sbit LCD_D7 at RB3_bit;
sbit LCD_D6 at RB2_bit;
sbit LCD_D5 at RB1_bit;
sbit LCD_D4 at RB0_bit;

// Pin direction
sbit LCD_RS_Direction at TRISB4_bit;
sbit LCD_EN_Direction at TRISB5_bit;
sbit LCD_D7_Direction at TRISB3_bit;
sbit LCD_D6_Direction at TRISB2_bit;
sbit LCD_D5_Direction at TRISB1_bit;
sbit LCD_D4_Direction at TRISB0_bit;

void ADCInit()
{
ANSEL.AN0 =255; //Analog input

ADCON0.ADON = 1; //Left Justified

ADCON0.VCFG1 = 0; //Vss reference
ADCON0.VCFG0 = 0; //Vdd reference

ADCON0.CHS2 = 0;
ADCON0.CHS1 = 0;  //Channel 00
ADCON0.CHS0 = 0;

ADCON1.ADCS2 = 0;
ADCON1.ADCS2 = 0; //conversin clock 4us, 001
ADCON1.ADCS2 = 1;
}

unsigned int ADCRead(unsigned char ch)
{
   int conv=0;
   if(ch>7) return 0;  //Invalid Channel

   ADCON0=(ch<<2);   //Select ADC Channel

   ADCON0.ADON=1;  //switch on the adc module

   ADCON0.GO_DONE=1;//Start conversion

   while(ADCON0.GO_DONE){} //wait for the conversion to finish

   ADCON0.ADON=0;  //switch off adc
     conv |= ADRESH << 2 | ADRESL >> 6   ;
   return conv;
}

void main() {
unsigned int val; //ADC Value
char sir[15];
float step=0.0048828125;
float nr=0;
int tensiune=0;
ADCInit();
Lcd_Init();
while(1)
   {
   val =ADCRead(0);
   nr=val*step*1000;
    tensiune=nr;
   sir[0]='0'+tensiune/1000;
   tensiune = tensiune%1000;
   sir[1]=',';
   sir[2]='0'+tensiune/100;
   tensiune=tensiune%100;
   sir[3]='0'+tensiune/10;
   tensiune=tensiune%10;
   sir[4]='0'+tensiune;
   sir[5]='\0';

   //sprinti(sir,"Nr = %d    ",val);
   Lcd_Out(2,1,sir);

      Delay_ms(50);
   }
}
