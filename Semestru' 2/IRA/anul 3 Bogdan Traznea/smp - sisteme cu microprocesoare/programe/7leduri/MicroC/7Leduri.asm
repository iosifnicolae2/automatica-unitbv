
_main:

;7Leduri.c,1 :: 		void main() {
;7Leduri.c,3 :: 		TRISB = 0;
	CLRF       TRISB+0
;7Leduri.c,4 :: 		i=1;
	MOVLW      1
	MOVWF      R1+0
	MOVLW      0
	MOVWF      R1+1
;7Leduri.c,5 :: 		while (1)
L_main0:
;7Leduri.c,7 :: 		if (i<=128)
	MOVLW      128
	MOVWF      R0+0
	MOVLW      128
	XORWF      R1+1, 0
	SUBWF      R0+0, 0
	BTFSS      STATUS+0, 2
	GOTO       L__main6
	MOVF       R1+0, 0
	SUBLW      128
L__main6:
	BTFSS      STATUS+0, 0
	GOTO       L_main2
;7Leduri.c,9 :: 		PORTB = i;
	MOVF       R1+0, 0
	MOVWF      PORTB+0
;7Leduri.c,10 :: 		Delay_ms(100);
	MOVLW      2
	MOVWF      R11+0
	MOVLW      4
	MOVWF      R12+0
	MOVLW      186
	MOVWF      R13+0
L_main3:
	DECFSZ     R13+0, 1
	GOTO       L_main3
	DECFSZ     R12+0, 1
	GOTO       L_main3
	DECFSZ     R11+0, 1
	GOTO       L_main3
	NOP
;7Leduri.c,11 :: 		i*=2;
	RLF        R1+0, 1
	RLF        R1+1, 1
	BCF        R1+0, 0
;7Leduri.c,12 :: 		}
	GOTO       L_main4
L_main2:
;7Leduri.c,14 :: 		i=1;
	MOVLW      1
	MOVWF      R1+0
	MOVLW      0
	MOVWF      R1+1
L_main4:
;7Leduri.c,15 :: 		}
	GOTO       L_main0
;7Leduri.c,16 :: 		}
L_end_main:
	GOTO       $+0
; end of _main
