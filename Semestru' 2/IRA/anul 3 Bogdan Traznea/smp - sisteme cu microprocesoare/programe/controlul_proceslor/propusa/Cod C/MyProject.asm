
_main:

;MyProject.c,1 :: 		void main()
;MyProject.c,12 :: 		TRISD.RD0=1;
	BSF        TRISD+0, 0
;MyProject.c,13 :: 		TRISD.RD1=1;
	BSF        TRISD+0, 1
;MyProject.c,14 :: 		TRISD.RD2=1;
	BSF        TRISD+0, 2
;MyProject.c,15 :: 		TRISD.RD3 = 1;
	BSF        TRISD+0, 3
;MyProject.c,16 :: 		TRISD.RD4 = 1;
	BSF        TRISD+0, 4
;MyProject.c,18 :: 		TRISB.RB0 = 0;
	BCF        TRISB+0, 0
;MyProject.c,19 :: 		TRISB.RB1 = 0;
	BCF        TRISB+0, 1
;MyProject.c,20 :: 		TRISB.RB2 = 0;
	BCF        TRISB+0, 2
;MyProject.c,21 :: 		TRISB.RB3 = 0;
	BCF        TRISB+0, 3
;MyProject.c,22 :: 		TRISB.RB4 = 0;
	BCF        TRISB+0, 4
;MyProject.c,25 :: 		A = 1;
	MOVLW      1
	MOVWF      R1+0
;MyProject.c,26 :: 		B = 0;
	CLRF       R2+0
;MyProject.c,27 :: 		C = 0;
	CLRF       R3+0
;MyProject.c,28 :: 		D = 0;
	CLRF       R4+0
;MyProject.c,29 :: 		E = 0;
	CLRF       R5+0
;MyProject.c,32 :: 		while(1)
L_main0:
;MyProject.c,35 :: 		if ((A == 1) && (PORTD.RD0 == 1)) { A = 0; B = 1; E = 1;}
	MOVF       R1+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main4
	BTFSS      PORTD+0, 0
	GOTO       L_main4
L__main26:
	CLRF       R1+0
	MOVLW      1
	MOVWF      R2+0
	MOVLW      1
	MOVWF      R5+0
L_main4:
;MyProject.c,36 :: 		if ((B == 1) && (PORTD.RD1 == 1)) { B = 0; C = 1; }
	MOVF       R2+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main7
	BTFSS      PORTD+0, 1
	GOTO       L_main7
L__main25:
	CLRF       R2+0
	MOVLW      1
	MOVWF      R3+0
L_main7:
;MyProject.c,37 :: 		if ((C == 1) && (PORTD.RD2 == 1)) { C = 0; A = 1; }
	MOVF       R3+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main10
	BTFSS      PORTD+0, 2
	GOTO       L_main10
L__main24:
	CLRF       R3+0
	MOVLW      1
	MOVWF      R1+0
L_main10:
;MyProject.c,38 :: 		if ((D == 1) && (PORTD.RD3 == 1)) { D = 0; E = 1; }
	MOVF       R4+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main13
	BTFSS      PORTD+0, 3
	GOTO       L_main13
L__main23:
	CLRF       R4+0
	MOVLW      1
	MOVWF      R5+0
L_main13:
;MyProject.c,39 :: 		if ((E == 1) && (PORTD.RD4 == 1)) { E = 0; D = 1; }
	MOVF       R5+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main16
	BTFSS      PORTD+0, 4
	GOTO       L_main16
L__main22:
	CLRF       R5+0
	MOVLW      1
	MOVWF      R4+0
L_main16:
;MyProject.c,44 :: 		if (A == 1) {PORTB.RB0 = 0; PORTB.RB1 = 0; PORTB.RB2 = 0; PORTB.RB3 = 0;                }
	MOVF       R1+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main17
	BCF        PORTB+0, 0
	BCF        PORTB+0, 1
	BCF        PORTB+0, 2
	BCF        PORTB+0, 3
L_main17:
;MyProject.c,45 :: 		if (B == 1) {PORTB.RB0 = 1; PORTB.RB1 = 0; PORTB.RB2 = 1; PORTB.RB3 = 1;                }
	MOVF       R2+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main18
	BSF        PORTB+0, 0
	BCF        PORTB+0, 1
	BSF        PORTB+0, 2
	BSF        PORTB+0, 3
L_main18:
;MyProject.c,46 :: 		if (C == 1) {PORTB.RB0 = 0; PORTB.RB1 = 1; PORTB.RB2 = 0; PORTB.RB3 = 0;                }
	MOVF       R3+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main19
	BCF        PORTB+0, 0
	BSF        PORTB+0, 1
	BCF        PORTB+0, 2
	BCF        PORTB+0, 3
L_main19:
;MyProject.c,47 :: 		if (D == 1) {                                                             PORTB.RB4 = 0;}
	MOVF       R4+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main20
	BCF        PORTB+0, 4
L_main20:
;MyProject.c,48 :: 		if (E == 1) {                                                             PORTB.RB4 = 1;}
	MOVF       R5+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main21
	BSF        PORTB+0, 4
L_main21:
;MyProject.c,49 :: 		}
	GOTO       L_main0
;MyProject.c,50 :: 		}
L_end_main:
	GOTO       $+0
; end of _main
