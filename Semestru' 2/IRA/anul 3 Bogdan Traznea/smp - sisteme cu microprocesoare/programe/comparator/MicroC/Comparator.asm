
_main:

;Comparator.c,1 :: 		void main() {
;Comparator.c,3 :: 		CMCON0=0b00000010;
	MOVLW      2
	MOVWF      CMCON0+0
;Comparator.c,4 :: 		VRCON.VREN = 1;
	BSF        VRCON+0, 7
;Comparator.c,5 :: 		VRCON.VRR = 0;
	BCF        VRCON+0, 5
;Comparator.c,6 :: 		VRCON.VR3 = 1;
	BSF        VRCON+0, 3
;Comparator.c,7 :: 		VRCON.VR2 = 1;
	BSF        VRCON+0, 2
;Comparator.c,8 :: 		VRCON.VR1 = 1;
	BSF        VRCON+0, 1
;Comparator.c,9 :: 		VRCON.VR0 = 1;
	BSF        VRCON+0, 0
;Comparator.c,10 :: 		TRISA.RA0=1;
	BSF        TRISA+0, 0
;Comparator.c,11 :: 		TRISB=0;
	CLRF       TRISB+0
;Comparator.c,12 :: 		PORTB=0;
	CLRF       PORTB+0
;Comparator.c,14 :: 		while(1)
L_main0:
;Comparator.c,16 :: 		PORTB.RB0=CMCON0.C1OUT;
	BTFSC      CMCON0+0, 6
	GOTO       L__main3
	BCF        PORTB+0, 0
	GOTO       L__main4
L__main3:
	BSF        PORTB+0, 0
L__main4:
;Comparator.c,17 :: 		}
	GOTO       L_main0
;Comparator.c,19 :: 		}
L_end_main:
	GOTO       $+0
; end of _main
