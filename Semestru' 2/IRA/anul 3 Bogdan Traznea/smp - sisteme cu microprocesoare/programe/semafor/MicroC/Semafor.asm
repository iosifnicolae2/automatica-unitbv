
_main:

;Semafor.c,1 :: 		void main() {
;Semafor.c,2 :: 		int cifra[10] = { 63, 6, 91, 79, 102, 109, 125, 39, 127, 111};
	MOVLW      63
	MOVWF      main_cifra_L0+0
	MOVLW      0
	MOVWF      main_cifra_L0+1
	MOVLW      6
	MOVWF      main_cifra_L0+2
	MOVLW      0
	MOVWF      main_cifra_L0+3
	MOVLW      91
	MOVWF      main_cifra_L0+4
	MOVLW      0
	MOVWF      main_cifra_L0+5
	MOVLW      79
	MOVWF      main_cifra_L0+6
	MOVLW      0
	MOVWF      main_cifra_L0+7
	MOVLW      102
	MOVWF      main_cifra_L0+8
	MOVLW      0
	MOVWF      main_cifra_L0+9
	MOVLW      109
	MOVWF      main_cifra_L0+10
	MOVLW      0
	MOVWF      main_cifra_L0+11
	MOVLW      125
	MOVWF      main_cifra_L0+12
	MOVLW      0
	MOVWF      main_cifra_L0+13
	MOVLW      39
	MOVWF      main_cifra_L0+14
	MOVLW      0
	MOVWF      main_cifra_L0+15
	MOVLW      127
	MOVWF      main_cifra_L0+16
	MOVLW      0
	MOVWF      main_cifra_L0+17
	MOVLW      111
	MOVWF      main_cifra_L0+18
	MOVLW      0
	MOVWF      main_cifra_L0+19
;Semafor.c,4 :: 		TRISB = 0;
	CLRF       TRISB+0
;Semafor.c,5 :: 		TRISC = 0;
	CLRF       TRISC+0
;Semafor.c,6 :: 		TRISD = 0;
	CLRF       TRISD+0
;Semafor.c,7 :: 		PORTC = 0;
	CLRF       PORTC+0
;Semafor.c,8 :: 		while (1)
L_main0:
;Semafor.c,10 :: 		for (i=9; i>=0; i--)
	MOVLW      9
	MOVWF      main_i_L0+0
	MOVLW      0
	MOVWF      main_i_L0+1
L_main2:
	MOVLW      128
	XORWF      main_i_L0+1, 0
	MOVWF      R0+0
	MOVLW      128
	SUBWF      R0+0, 0
	BTFSS      STATUS+0, 2
	GOTO       L__main13
	MOVLW      0
	SUBWF      main_i_L0+0, 0
L__main13:
	BTFSS      STATUS+0, 0
	GOTO       L_main3
;Semafor.c,12 :: 		PORTB = cifra[i];
	MOVF       main_i_L0+0, 0
	MOVWF      R0+0
	MOVF       main_i_L0+1, 0
	MOVWF      R0+1
	RLF        R0+0, 1
	RLF        R0+1, 1
	BCF        R0+0, 0
	MOVF       R0+0, 0
	ADDLW      main_cifra_L0+0
	MOVWF      FSR
	MOVF       INDF+0, 0
	MOVWF      PORTB+0
;Semafor.c,13 :: 		if (i == 0)
	MOVLW      0
	XORWF      main_i_L0+1, 0
	BTFSS      STATUS+0, 2
	GOTO       L__main14
	MOVLW      0
	XORWF      main_i_L0+0, 0
L__main14:
	BTFSS      STATUS+0, 2
	GOTO       L_main5
;Semafor.c,14 :: 		PORTD = 17;
	MOVLW      17
	MOVWF      PORTD+0
	GOTO       L_main6
L_main5:
;Semafor.c,16 :: 		PORTD = 9;
	MOVLW      9
	MOVWF      PORTD+0
L_main6:
;Semafor.c,17 :: 		Delay_1sec();
	CALL       _Delay_1sec+0
;Semafor.c,10 :: 		for (i=9; i>=0; i--)
	MOVLW      1
	SUBWF      main_i_L0+0, 1
	BTFSS      STATUS+0, 0
	DECF       main_i_L0+1, 1
;Semafor.c,18 :: 		}
	GOTO       L_main2
L_main3:
;Semafor.c,19 :: 		PORTB = 0;
	CLRF       PORTB+0
;Semafor.c,20 :: 		for (i=5; i>=0; i--)
	MOVLW      5
	MOVWF      main_i_L0+0
	MOVLW      0
	MOVWF      main_i_L0+1
L_main7:
	MOVLW      128
	XORWF      main_i_L0+1, 0
	MOVWF      R0+0
	MOVLW      128
	SUBWF      R0+0, 0
	BTFSS      STATUS+0, 2
	GOTO       L__main15
	MOVLW      0
	SUBWF      main_i_L0+0, 0
L__main15:
	BTFSS      STATUS+0, 0
	GOTO       L_main8
;Semafor.c,22 :: 		PORTC = cifra[i];
	MOVF       main_i_L0+0, 0
	MOVWF      R0+0
	MOVF       main_i_L0+1, 0
	MOVWF      R0+1
	RLF        R0+0, 1
	RLF        R0+1, 1
	BCF        R0+0, 0
	MOVF       R0+0, 0
	ADDLW      main_cifra_L0+0
	MOVWF      FSR
	MOVF       INDF+0, 0
	MOVWF      PORTC+0
;Semafor.c,23 :: 		if (i == 0)
	MOVLW      0
	XORWF      main_i_L0+1, 0
	BTFSS      STATUS+0, 2
	GOTO       L__main16
	MOVLW      0
	XORWF      main_i_L0+0, 0
L__main16:
	BTFSS      STATUS+0, 2
	GOTO       L_main10
;Semafor.c,24 :: 		PORTD = 36 ;
	MOVLW      36
	MOVWF      PORTD+0
	GOTO       L_main11
L_main10:
;Semafor.c,26 :: 		PORTD = 6;
	MOVLW      6
	MOVWF      PORTD+0
L_main11:
;Semafor.c,27 :: 		Delay_1sec();
	CALL       _Delay_1sec+0
;Semafor.c,20 :: 		for (i=5; i>=0; i--)
	MOVLW      1
	SUBWF      main_i_L0+0, 1
	BTFSS      STATUS+0, 0
	DECF       main_i_L0+1, 1
;Semafor.c,28 :: 		}
	GOTO       L_main7
L_main8:
;Semafor.c,29 :: 		PORTC = 0;
	CLRF       PORTC+0
;Semafor.c,30 :: 		}
	GOTO       L_main0
;Semafor.c,31 :: 		}
L_end_main:
	GOTO       $+0
; end of _main
