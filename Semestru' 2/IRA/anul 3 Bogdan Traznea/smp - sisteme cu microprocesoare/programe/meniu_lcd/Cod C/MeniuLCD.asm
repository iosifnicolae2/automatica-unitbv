
_main:

;MeniuLCD.c,17 :: 		void main() {
;MeniuLCD.c,28 :: 		TRISD.RD0 = 1;  // OK
	BSF        TRISD+0, 0
;MeniuLCD.c,29 :: 		TRISD.RD1 = 1;  // Cancel
	BSF        TRISD+0, 1
;MeniuLCD.c,30 :: 		TRISD.RD2 = 1;  // <
	BSF        TRISD+0, 2
;MeniuLCD.c,31 :: 		TRISD.RD3 = 1;  // >
	BSF        TRISD+0, 3
;MeniuLCD.c,34 :: 		TRISD.RD4 = 0;
	BCF        TRISD+0, 4
;MeniuLCD.c,35 :: 		PORTD.RD4 = 0;
	BCF        PORTD+0, 4
;MeniuLCD.c,38 :: 		A = 1;
	MOVLW      1
	MOVWF      main_A_L0+0
;MeniuLCD.c,39 :: 		B = 0;
	CLRF       main_B_L0+0
;MeniuLCD.c,40 :: 		C = 0;
	CLRF       main_C_L0+0
;MeniuLCD.c,41 :: 		D = 0;
	CLRF       main_D_L0+0
;MeniuLCD.c,42 :: 		E = 0;
	CLRF       main_E_L0+0
;MeniuLCD.c,45 :: 		Lcd_Init();
	CALL       _Lcd_Init+0
;MeniuLCD.c,46 :: 		Lcd_Cmd(_LCD_CURSOR_OFF);
	MOVLW      12
	MOVWF      FARG_Lcd_Cmd_out_char+0
	CALL       _Lcd_Cmd+0
;MeniuLCD.c,48 :: 		while(1)
L_main0:
;MeniuLCD.c,52 :: 		if ((A==1)&&(PORTD.RD0==1)) {A=0; B=1; C=0; D=0; E=0; while( PORTD.RD0 == 1 ){}}
	MOVF       main_A_L0+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main4
	BTFSS      PORTD+0, 0
	GOTO       L_main4
L__main60:
	CLRF       main_A_L0+0
	MOVLW      1
	MOVWF      main_B_L0+0
	CLRF       main_C_L0+0
	CLRF       main_D_L0+0
	CLRF       main_E_L0+0
L_main5:
	BTFSS      PORTD+0, 0
	GOTO       L_main6
	GOTO       L_main5
L_main6:
L_main4:
;MeniuLCD.c,53 :: 		if ((B==1)&&(PORTD.RD0==1)) {A=0; B=0; C=0; D=1; E=0; while( PORTD.RD0 == 1 ){}}  // B -> OK -> D
	MOVF       main_B_L0+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main9
	BTFSS      PORTD+0, 0
	GOTO       L_main9
L__main59:
	CLRF       main_A_L0+0
	CLRF       main_B_L0+0
	CLRF       main_C_L0+0
	MOVLW      1
	MOVWF      main_D_L0+0
	CLRF       main_E_L0+0
L_main10:
	BTFSS      PORTD+0, 0
	GOTO       L_main11
	GOTO       L_main10
L_main11:
L_main9:
;MeniuLCD.c,54 :: 		if ((B==1)&&(PORTD.RD1==1)) {A=1; B=0; C=0; D=0; E=0; while( PORTD.RD1 == 1 ){}}  // B -> Cancel -> A
	MOVF       main_B_L0+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main14
	BTFSS      PORTD+0, 1
	GOTO       L_main14
L__main58:
	MOVLW      1
	MOVWF      main_A_L0+0
	CLRF       main_B_L0+0
	CLRF       main_C_L0+0
	CLRF       main_D_L0+0
	CLRF       main_E_L0+0
L_main15:
	BTFSS      PORTD+0, 1
	GOTO       L_main16
	GOTO       L_main15
L_main16:
L_main14:
;MeniuLCD.c,55 :: 		if ((B==1)&&(PORTD.RD3==1)) {A=0; B=0; C=1; D=0; E=0; while( PORTD.RD3 == 1 ){}}  // B -> > -> C
	MOVF       main_B_L0+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main19
	BTFSS      PORTD+0, 3
	GOTO       L_main19
L__main57:
	CLRF       main_A_L0+0
	CLRF       main_B_L0+0
	MOVLW      1
	MOVWF      main_C_L0+0
	CLRF       main_D_L0+0
	CLRF       main_E_L0+0
L_main20:
	BTFSS      PORTD+0, 3
	GOTO       L_main21
	GOTO       L_main20
L_main21:
L_main19:
;MeniuLCD.c,56 :: 		if ((C==1)&&(PORTD.RD0==1)) {A=0; B=0; C=0; D=0; E=1; while( PORTD.RD0 == 1 ){}}   // C -> OK -> E
	MOVF       main_C_L0+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main24
	BTFSS      PORTD+0, 0
	GOTO       L_main24
L__main56:
	CLRF       main_A_L0+0
	CLRF       main_B_L0+0
	CLRF       main_C_L0+0
	CLRF       main_D_L0+0
	MOVLW      1
	MOVWF      main_E_L0+0
L_main25:
	BTFSS      PORTD+0, 0
	GOTO       L_main26
	GOTO       L_main25
L_main26:
L_main24:
;MeniuLCD.c,57 :: 		if ((C==1)&&(PORTD.RD1==1)) {A=1; B=0; C=0; D=0; E=0; while( PORTD.RD1 == 1 ){}}   // C -> Cancel -> A
	MOVF       main_C_L0+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main29
	BTFSS      PORTD+0, 1
	GOTO       L_main29
L__main55:
	MOVLW      1
	MOVWF      main_A_L0+0
	CLRF       main_B_L0+0
	CLRF       main_C_L0+0
	CLRF       main_D_L0+0
	CLRF       main_E_L0+0
L_main30:
	BTFSS      PORTD+0, 1
	GOTO       L_main31
	GOTO       L_main30
L_main31:
L_main29:
;MeniuLCD.c,58 :: 		if ((C==1)&&(PORTD.RD2==1)) {A=0; B=1; C=0; D=0; E=0; while( PORTD.RD2 == 1 ){}}   // C -> < -> B
	MOVF       main_C_L0+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main34
	BTFSS      PORTD+0, 2
	GOTO       L_main34
L__main54:
	CLRF       main_A_L0+0
	MOVLW      1
	MOVWF      main_B_L0+0
	CLRF       main_C_L0+0
	CLRF       main_D_L0+0
	CLRF       main_E_L0+0
L_main35:
	BTFSS      PORTD+0, 2
	GOTO       L_main36
	GOTO       L_main35
L_main36:
L_main34:
;MeniuLCD.c,59 :: 		if ((D==1)&&(PORTD.RD2==1)) {A=0; B=1; C=0; D=0; E=0; while( PORTD.RD2 == 1 ){}}   // D -> < -> B
	MOVF       main_D_L0+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main39
	BTFSS      PORTD+0, 2
	GOTO       L_main39
L__main53:
	CLRF       main_A_L0+0
	MOVLW      1
	MOVWF      main_B_L0+0
	CLRF       main_C_L0+0
	CLRF       main_D_L0+0
	CLRF       main_E_L0+0
L_main40:
	BTFSS      PORTD+0, 2
	GOTO       L_main41
	GOTO       L_main40
L_main41:
L_main39:
;MeniuLCD.c,60 :: 		if ((E==1)&&(PORTD.RD2==1)) {A=0; B=0; C=1; D=0; E=0; while( PORTD.RD2 == 1 ){}}   // E -> < -> C
	MOVF       main_E_L0+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main44
	BTFSS      PORTD+0, 2
	GOTO       L_main44
L__main52:
	CLRF       main_A_L0+0
	CLRF       main_B_L0+0
	MOVLW      1
	MOVWF      main_C_L0+0
	CLRF       main_D_L0+0
	CLRF       main_E_L0+0
L_main45:
	BTFSS      PORTD+0, 2
	GOTO       L_main46
	GOTO       L_main45
L_main46:
L_main44:
;MeniuLCD.c,64 :: 		if (A==1) { Lcd_Out(1,1,"  Seteaza RB4 "); Lcd_Out(2,1,"       OK      ");}
	MOVF       main_A_L0+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main47
	MOVLW      1
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      1
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      ?lstr1_MeniuLCD+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
	MOVLW      2
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      1
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      ?lstr2_MeniuLCD+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
L_main47:
;MeniuLCD.c,65 :: 		if (B==1) { Lcd_Out(1,1,"  Aprinde LED "); Lcd_Out(2,1,"Cancel | OK |  >");}
	MOVF       main_B_L0+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main48
	MOVLW      1
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      1
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      ?lstr3_MeniuLCD+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
	MOVLW      2
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      1
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      ?lstr4_MeniuLCD+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
L_main48:
;MeniuLCD.c,66 :: 		if (C==1) { Lcd_Out(1,1,"  Stinge LED  "); Lcd_Out(2,1,"<  | OK | Cancel");}
	MOVF       main_C_L0+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main49
	MOVLW      1
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      1
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      ?lstr5_MeniuLCD+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
	MOVLW      2
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      1
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      ?lstr6_MeniuLCD+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
L_main49:
;MeniuLCD.c,67 :: 		if (D==1) { PORTD.RD4 = 1; Lcd_Out(1,1,"  LED Aprins  "); Lcd_Out(2,1,"   Back <       ");}
	MOVF       main_D_L0+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main50
	BSF        PORTD+0, 4
	MOVLW      1
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      1
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      ?lstr7_MeniuLCD+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
	MOVLW      2
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      1
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      ?lstr8_MeniuLCD+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
L_main50:
;MeniuLCD.c,68 :: 		if (E==1) { PORTD.RD4 = 0; Lcd_Out(1,1,"  LED Stins  ");  Lcd_Out(2,1,"   Back <       ");}
	MOVF       main_E_L0+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main51
	BCF        PORTD+0, 4
	MOVLW      1
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      1
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      ?lstr9_MeniuLCD+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
	MOVLW      2
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      1
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      ?lstr10_MeniuLCD+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
L_main51:
;MeniuLCD.c,70 :: 		}
	GOTO       L_main0
;MeniuLCD.c,78 :: 		}
L_end_main:
	GOTO       $+0
; end of _main
