module test_bench (in);
   output  in;
   reg  in;
   initial  in = 1'b0;           
   always #5 in  <=  ~ in;  
endmodule

