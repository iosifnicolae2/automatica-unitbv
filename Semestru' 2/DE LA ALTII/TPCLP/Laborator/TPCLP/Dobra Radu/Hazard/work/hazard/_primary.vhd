library verilog;
use verilog.vl_types.all;
entity hazard is
    port(
        \in\            : in     vl_logic;
        \out\           : out    vl_logic
    );
end hazard;
