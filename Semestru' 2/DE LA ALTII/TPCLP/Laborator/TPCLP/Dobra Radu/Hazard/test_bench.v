module test();
  wire  in, out;
      test_bench TB (.in(in));
      hazard  DUT_hazard (.in(in),  .out(out));
endmodule

