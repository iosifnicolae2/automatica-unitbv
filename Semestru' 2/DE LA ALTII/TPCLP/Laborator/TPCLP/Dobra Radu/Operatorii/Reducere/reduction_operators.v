module reduction_operators();
 initial
   begin
       // Bit Wise AND reduction
     $display ();
     $display (" Bit Wise AND reduction");
     $display ("   & 4'b1001 = %b", (&  4'b1001));
     $display ("   & 4'bx111 = %b", (&  4'bx111));
     $display ("   & 4'bz111 = %b", (&  4'bz111));
     $display ("   & 4'bx011 = %b", (&  4'bx011));
     $display ("   & 4'bz011 = %b", (&  4'bz011));
     
       // Bit Wise NAND reduction
     $display ();
     $display (" Bit Wise NAND reduction");
     $display ("   ~& 4'b1001 = %b", (~& 4'b1001));
     $display ("   ~& 4'bx001 = %b", (~& 4'bx001));
     $display ("   ~& 4'bz001 = %b", (~& 4'bz001));
     $display ("   ~& 4'bx111 = %b", (~& 4'bx111));
     $display ("   ~& 4'bz111 = %b", (~& 4'bz111));
     $display ("   ~& 4'bxz11 = %b", (~& 4'bxz11));
     
      // Bit Wise OR reduction
     $display ();
     $display (" Bit Wise OR reduction");
     $display ("   | 4'b1001 = %b", (| 4'b1001));
     $display ("   | 4'bx000 = %b", (| 4'bx000));
     $display ("   | 4'bz000 = %b", (| 4'bz000));
     $display ("   | 4'bx100 = %b", (| 4'bx100));
     $display ("   | 4'bz100 = %b", (| 4'bz100));
     $display ("   | 4'bxz00 = %b", (| 4'bxz00));
     
      // Bit Wise NOR reduction
     $display ();
     $display (" Bit Wise NOR reduction");
     $display ("   ~| 4'b1001 = %b", (~| 4'b1001));
     $display ("   ~| 4'bx001 = %b", (~| 4'bx001));
     $display ("   ~| 4'bz001 = %b", (~| 4'bz001));
     $display ("   ~| 4'bx000 = %b", (~| 4'bx000));
     $display ("   ~| 4'bz000 = %b", (~| 4'bz000));
     
      // Bit Wise XOR reduction
     $display ();
     $display (" Bit Wise XOR reduction");
     $display ("   ^  4'b1001 = %b", (^ 4'b1001));
     $display ("   ^  4'bx001 = %b", (^ 4'bx001));
     $display ("   ^  4'bz001 = %b", (^ 4'bz001));
     $display ("   ^  4'bxz00 = %b", (^ 4'bxz00));
     
      // Bit Wise XNOR
     $display ();
     $display (" Bit Wise XNOR reduction");
     $display ("   ~^ 4'b1001 = %b", (~^ 4'b1001));
     $display ("   ~^ 4'bx001 = %b", (~^ 4'bx001));
     $display ("   ~^ 4'bz001 = %b", (~^ 4'bz001));
     $display ();
     #1  $stop;
   end
endmodule

