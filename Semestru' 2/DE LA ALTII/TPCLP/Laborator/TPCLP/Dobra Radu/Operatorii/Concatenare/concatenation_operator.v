module concatenation_operator();
  reg [3:0] a, b, c;
  reg [7:0] d;
 initial
   begin
     #1;
     a = 4'b1111;
     b = 4'bzzzz;
     c = {a,b};
     d = {a,b};
   // concatenation operator
    $display (" \n Concatenation two binary numbers");
    $display (" {4'b1000, 4'bx00z}  = %b ", {4'b1000, 4'bx00z});
    $display (" \n a = %b,  b = %b,  c ={a,b}= %b,  d ={a,b}= %b  \n", a, b, c, d);
    #1  $stop;
  end
endmodule

