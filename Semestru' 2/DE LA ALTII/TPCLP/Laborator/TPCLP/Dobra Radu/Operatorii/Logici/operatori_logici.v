module operatori_logici;
//autor: svh, Mai 2010
initial
   begin
     // Operatorul logic AND
     $display("Logical AND");
     $display("  0 && 1 =  ", 1'b0 && 1'b1); // rezultat: 0
     $display("  0 && X =  ", 1'b0 && 1'bX); // rezultat: 0
     $display("  0 && Z =  ", 1'b0 && 1'bZ); // rezultat: 0
     $display("  1 && 1 =  ", 1'b1 && 1'b1); // rezultat: 1
     $display("  1 && X =  ", 1'b1 && 1'bX); // rezultat: x
     $display("  1 && Z =  ", 1'b1 && 1'bZ); // rezultat: x
     $display("  X && X =  ", 1'bX && 1'bX); // rezultat: x
     $display("  X && Z =  ", 1'bX && 1'bZ); // rezultat: x
     $display();
     
     // Operatorul logic OR
     $display("Logical OR");
     $display("  0 || 1 =  ", 1'b0 || 1'b1); // rezultat: 1
     $display("  0 || X =  ", 1'b0 || 1'bX); // rezultat: x
     $display("  0 || Z =  ", 1'b0 || 1'bZ); // rezultat: x
     $display("  1 || 1 =  ", 1'b1 || 1'b1); // rezultat: 1
     $display("  1 || X =  ", 1'b1 || 1'bX); // rezultat: 1
     $display("  1 || Z =  ", 1'b1 || 1'bZ); // rezultat: 1
     $display("  X || Z =  ", 1'bX || 1'bZ); // rezultat: x
     $display("  Z || Z =  ", 1'bZ || 1'bZ); // rezultat: x
     $display();
     
     // Operatorul logic NOT (!)
     $display("Logical NOT (!)");
     $display("  ! 0 =  ", ! 1'b0); // rezultat: 1
     $display("  ! 1 =  ", ! 1'b1); // rezultat: 0
     $display("  ! X =  ", ! 1'bX); // rezultat: x
     $display("  ! Z =  ", ! 1'bZ); // rezultat: x
     $display();
     
     // Operatorul logic NOT (~)
     $display("Logical NOT (~)");
     $display("  ~ 0 =  ", ! 1'b0); // rezultat: 1
     $display("  ~ 1 =  ", ! 1'b1); // rezultat: 0
     $display("  ~ X =  ", ! 1'bX); // rezultat: x
     $display("  ~ Z =  ", ! 1'bZ); // rezultat: x
     
   end
endmodule
