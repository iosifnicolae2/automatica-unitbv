module conditional_operator();
  wire out;
  reg enable,data;
     // Tri state buffer
  assign out = (enable) ? data : 1'bz;  // daca <enable> este true, atunci out=data, altfel out=z
  initial
   begin
     $display ("");  // afiseaza un rand liber
     $display ("time\t enable\t data\t out");  // caracterele \t comanda o tabulare
     $monitor ("%0d\t %b\t %b\t %b",$time,enable,data,out);  //$time este o functie sistem de timp
     enable = 0;
     data = 0;
     #1  data = 1;
     #1  data = 0;
     #1  enable = 1;
     #1  data = 1;
     #1  data = 0;
     #1  enable = 0;
     
     #1 $display ("");
          $stop;  // opreste procedura de simulare
   end	
endmodule

