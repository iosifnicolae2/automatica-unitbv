module shift_operators();
  reg [3:0] a, b, c;
  initial
   begin
     $monitor ("time=%g  a= %b  b= %b  c= %b", $time, a, b, c);
     a <= 4'b0000;
     b <= 4'b0000;
     #10 a <= 4'b1x1z;
     #20 b <= a << 1;
     #5 c <= a << 1;
          // Left Shift
     $display (" 4'b1001 << 1 = %b", (4'b1001 << 1));
     $display (" 4'b10x1 << 1 = %b", (4'b10x1 << 1));
     $display (" 4'b10z1 << 1 = %b", (4'b10z1 << 1));
          // Right Shift
     $display (" 4'b1001 >> 1 = %b", (4'b1001 >> 1));
     $display (" 4'b10x1 >> 1 = %b", (4'b10x1 >> 1));
     $display (" 4'b10z1 >> 1 = %b", (4'b10z1 >> 1));
     #10  $finish;
   end
endmodule

