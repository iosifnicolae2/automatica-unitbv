library verilog;
use verilog.vl_types.all;
entity test_bench is
    port(
        enable          : out    vl_logic;
        clear           : out    vl_logic;
        clk             : out    vl_logic;
        load            : out    vl_logic;
        read            : out    vl_logic;
        D3              : out    vl_logic;
        D2              : out    vl_logic;
        D1              : out    vl_logic;
        D0              : out    vl_logic
    );
end test_bench;
