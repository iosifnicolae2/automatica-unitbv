module dff (cl, d, ck, q); // device flip-flop, de tip d
 input cl, d, ck; // porturi de intrare, de 1 bit
 output q; // port de iesire, de 1 bit
 reg q; // iesirea q declarata de tip registru
 
 always @(posedge ck, negedge cl)
   begin
    if (cl == 0)   
      q = 0;  
    else
      q = d;
   end
endmodule

