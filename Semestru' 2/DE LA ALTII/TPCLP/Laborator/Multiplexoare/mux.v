module mux(i0, i1, sel, out);
input i0, i1, sel;
output out;

assign out = (i0 & (~sel)) | (i1 & sel);
endmodule

