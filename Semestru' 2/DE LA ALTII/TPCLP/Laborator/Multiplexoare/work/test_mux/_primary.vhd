library verilog;
use verilog.vl_types.all;
entity test_mux is
    generic(
        per             : integer := 5
    );
    attribute mti_svvh_generic_type : integer;
    attribute mti_svvh_generic_type of per : constant is 1;
end test_mux;
