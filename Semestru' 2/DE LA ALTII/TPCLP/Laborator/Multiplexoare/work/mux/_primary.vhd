library verilog;
use verilog.vl_types.all;
entity mux is
    port(
        i0              : in     vl_logic;
        i1              : in     vl_logic;
        sel             : in     vl_logic;
        \out\           : out    vl_logic
    );
end mux;
