library verilog;
use verilog.vl_types.all;
entity test_bench_mux is
    generic(
        per             : integer := 5
    );
    port(
        i0              : out    vl_logic;
        i1              : out    vl_logic;
        sel             : out    vl_logic
    );
    attribute mti_svvh_generic_type : integer;
    attribute mti_svvh_generic_type of per : constant is 1;
end test_bench_mux;
