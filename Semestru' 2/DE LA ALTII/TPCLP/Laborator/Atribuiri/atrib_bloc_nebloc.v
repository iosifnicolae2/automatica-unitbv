module atrib_bloc_nebloc ();
  reg a, b, c, d;
  reg e, f, g, h;
  
  initial  // atribuiri de valori initiale
   begin
     a = 1'b0;
     b = 1'b0;
     c = 1'b0;
     d = 1'b0;
          
     e <= 1'b0;
     f <= 1'b0;
     g <= 1'b0;
     h <= 1'b0;
     
   // proceduri de atribuiri blocante
     $display(" atribuiri blocante");
     $display("Time\t a\t b\t c\t d");
     $monitor("%g\t %b\t %b\t %b\t %b", $time, a, b, c, d);
     a = #10 1'b1;
     b = #2 1'b1;
     c = #4 1'b1;
     d = #6 1'b1;
     $display();
     
     // proceduri de atribuiri neblocante
     $display(" atribuiri neblocante");
     $display("Time\t e\t f\t g\t h");
     $monitor("%g\t %b\t %b\t %b\t %b", $time, e, f, g, h);
     e <= #10 1'b1;
     f <= #2 1'b1;
     g <= #4 1'b1;
     h <= #6 1'b1;          
   end
endmodule

