module test_bench_reg_deriv4b (enable, clear, clk, wordin,);
output enable, clear, clk, wordin;
reg enable, clear, clk;
reg [3:0] wordin;

initial clk = 1'b0;  
always #5 clk <= ~clk;

initial 
begin
wordin[3] = 1; 
wordin[2] = 0; 
wordin[1] = 1; 
wordin[0] = 1; 
enable = 0; 
#5 enable = 1; 
#5 clear = 0;  
#5 clear = 1; 
 
repeat (2) @(posedge clk);
wordin <= 4'b1101; 

repeat (2) @(posedge clk);
wordin <= 4'b1101; 

repeat (2) @(posedge clk);
wordin <= 4'b1001; 
$stop;
end
endmodule

