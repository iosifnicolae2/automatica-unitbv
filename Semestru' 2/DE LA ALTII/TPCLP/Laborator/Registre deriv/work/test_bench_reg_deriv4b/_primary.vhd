library verilog;
use verilog.vl_types.all;
entity test_bench_reg_deriv4b is
    port(
        enable          : out    vl_logic;
        clear           : out    vl_logic;
        clk             : out    vl_logic;
        wordin          : out    vl_logic_vector(3 downto 0)
    );
end test_bench_reg_deriv4b;
