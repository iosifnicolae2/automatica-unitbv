library verilog;
use verilog.vl_types.all;
entity reg_deriv4b is
    port(
        enable          : in     vl_logic;
        clear           : in     vl_logic;
        clk             : in     vl_logic;
        wordin          : in     vl_logic_vector(3 downto 0);
        wordout         : out    vl_logic_vector(3 downto 0)
    );
end reg_deriv4b;
