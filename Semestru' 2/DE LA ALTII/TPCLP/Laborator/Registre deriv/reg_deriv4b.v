module reg_deriv4b (enable, clear, clk, wordin, wordout);
	input  wordin, clk, enable, clear;
	output wordout;
	wire [3:0] wordin;
	reg    [3:0] wordout;
	
	always @(posedge clk or clear or enable)
        begin
	   if (enable == 1)
		if (clear == 0)	      
		   wordout <= 4'b0000;
	     	else wordout <= wordin;
	   else wordout <= 4'bzzzz;
        end
        endmodule

