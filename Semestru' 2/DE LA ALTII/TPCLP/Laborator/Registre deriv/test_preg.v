module test_preg ();
  wire enable, clear, clk;
  wire [3:0] wordin, wordout;
  	
  reg_deriv4b DUT (
.enable(enable),
.clear(clear),
.clk(clk), 
.wordin(wordin), 
.wordout(wordout)
);

  test_bench_reg_deriv4b TB (
.enable(enable),
.clear(clear),
.clk(clk),
.wordin(wordin)
);
endmodule

