module latch (d, enable, q_latch);
input d, enable;
output q_latch;				
reg q_latch; 

always @ (d or enable) 
   if (enable) 
      begin
           q_latch = d;
      end
endmodule	

