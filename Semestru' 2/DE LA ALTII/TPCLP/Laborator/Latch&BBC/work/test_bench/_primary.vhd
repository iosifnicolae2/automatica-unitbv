library verilog;
use verilog.vl_types.all;
entity test_bench is
    port(
        d               : out    vl_logic;
        clk             : out    vl_logic
    );
end test_bench;
