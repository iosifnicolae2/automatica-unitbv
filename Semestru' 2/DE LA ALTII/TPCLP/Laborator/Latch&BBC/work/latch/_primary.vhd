library verilog;
use verilog.vl_types.all;
entity latch is
    port(
        d               : in     vl_logic;
        enable          : in     vl_logic;
        q_latch         : out    vl_logic
    );
end latch;
