module test ();
wire d, clk, qb, qbn, q_latch;

latch  DUT_latch (.d(d), . enable(clk), .q_latch(q_latch) );
cbb_d  DUT_cbb (.d(d), .clk(clk), .q(qb), .qn(qbn) );
test_bench TB (.d(d), .clk(clk) );

endmodule

