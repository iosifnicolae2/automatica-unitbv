module test_bench (d, clk);
output d, clk;
reg d, clk;

initial 
	begin 
	    clk = 0;    // initializarea semnalului clk de clock
	      d = 0;    // initializarea semnalului d destinat intr?rii de date
	   #8 d = 1;    // asignerea d=1 cu o intarziere de 8 ut (unitati de timp)
	  #16 d = 0;    // asignerea d=0 cu o intarziere de 8+16=24 ut
	  #27 d = 1;    // asignerea d=1 cu o intarziere de 24+27=51 ut
	   #3 d = 0;    // asignerea d=0 cu o intarziere de 51+3=54 ut
	   #2 d = 1;    // asignerea d=1 cu o intarziere de 54+2=56 ut
	  #32 d = 0;    // asignerea d=0 cu o intarziere de 56+32=88 ut
#1 $stop;  //se opreste simularea la momentul 88+1=89 ut. Altfel ceasul ar comuta mereu.
end
always #5 clk <= ~clk;   // comutarea clock-ului la intervale de 5 ut
endmodule
