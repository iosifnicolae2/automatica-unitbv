module test_bench_sumator (a, b, cin); //numele modulului si (lista porturi)

parameter per = 5; // parametrului per i se atribuie 5 unitati de timp

output a, b, cin; // declarare porturi de iesire din generatorul de stimuli

reg [2:0] counter; // declararea tipului si dimensiunii variabilei counter
reg clk; // declararea tipului variabilei clk a ceasului (size=1 bit)

initial //instructiune procedurala;blocul begin-end se executa o singura data
 begin     // inceputul blocului de atribuiri ale instructiunii initial
  clk = 0;      // se initializeaza ceasul
  counter = 0;  // se initializeaza contorul
 end       // sfarsitul blocului de atribuiri ale instructiunii initial

always @( posedge clk ) /* instructiune procedurala, sensibila
   la fronturile pozitive ale ceasului clk */
 begin
  if (counter == {3{1'b1}}) /* daca counter devine egala cu secventa 111 
     (obtinuta prin concatenare),se opreste generarea de stimuli(cu $stop)*/
   begin
     $stop;	
   end
  else            // altfel, se trece la incrementarea contorului counter
   begin
     counter = counter + 1;    // se incrementeaza cu 1 variabila counter 
   end      
 end 

assign a   = counter[2]; //asignere a cu valoare bit rang 2 al lui counter
assign b   = counter[1]; //asignere b cu valoare bit rang 1 al lui counter
assign cin = counter[0]; //asignere cin cu valoare bit rang 0 al lui counter

always
  #per clk = ~clk; // se comuta semnalul clk, din 5 in 5 unitati de timp

endmodule   // se incheie corpul modulului

