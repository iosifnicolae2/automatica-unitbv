module test_sumator();   // numele modulului. Nu exista lista  de porturi.

parameter per = 5;    // asignerea cu 5 unitati de timp a parametrului per 

wire a, b, cin, s, cout;   //fire de legatura intre TB si DUT, DUT si TEST

test_bench_sumator #(per) TB (   // este citata instanta modulului TB
		.a(a),	// semnifica ca: portul a al TB se leaga la firul (a)
		.b(b),
		.cin(cin)
		);

sumator DUT (               // este citata instanta modulului DUT
		.a(a),		//portul a al DUT se leaga la firul (a)
		.b(b),		//portul b al DUT se leaga la firul (b)
		.cin(cin), 		//portul cin al DUT se leaga la firul (cin)
		.s(s),            //portul s al DUT se leaga la firul (s)
		.cout(cout)       //portul cout al DUT se elaga la firul (cout)
		);
endmodule

