library verilog;
use verilog.vl_types.all;
entity test_bench_sumator is
    generic(
        per             : integer := 5
    );
    port(
        a               : out    vl_logic;
        b               : out    vl_logic;
        cin             : out    vl_logic
    );
    attribute mti_svvh_generic_type : integer;
    attribute mti_svvh_generic_type of per : constant is 1;
end test_bench_sumator;
