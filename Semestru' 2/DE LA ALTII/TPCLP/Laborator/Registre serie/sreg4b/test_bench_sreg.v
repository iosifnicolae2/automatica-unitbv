module test_bench_sreg (d, clk);
output d, clk;
reg d, clk;
reg wordin[3:0];

initial
 begin
  wordin[3]=1; // asignere biti cuvant destinat incarcarii in registru
  wordin[2]=0; // asignere biti cuvant destinat incarcarii in registru
  wordin[1]=1; //asignere biti cuvant destinat incarcarii in registru
  wordin[0]=1; // asignere biti cuvant destinat incarcarii in registru
  clk = 1'b0;  // initializarea semnalului de ceas
 end
  
always #5 clk <= ~clk;
initial
  begin
  @(posedge clk);  //se executa frontul 1 de clock
    d = wordin[0]; //asignere bit word[0] (LSB) aplicat primului bistabil al registrului
  @(posedge clk);  //se executa frontul 2 de clock
d = wordin[1]; //asignere bit word[1] aplicat primului bistabil al registrului
  @(posedge clk);  //se executa frontul 3 de clock
	  d = wordin[2]; //asignere bit word[2] aplicat primului bistabil al registrului
  @(posedge clk);  //se executa frontul 4 de clock. Acum intregul cuvant se afla memorat in registru.
    d = wordin[3]; //asignere bit word[3] aplicat primului bistabil al registrului
    	
  @(posedge clk); // se executa frontul 5 de clock.
    d = 0;   //asignere cu 0 a primului bistabil al registrului. Pasul 1 de descarcare.	
  @(posedge clk); // se executa frontul 6 de clock.
    d = 0;   //asignere cu 0 a primului bistabil al registrului. Pasul 2 de descarcare.
  @(posedge clk); // se executa frontul 7 de clock.
    d = 0;   //asignere cu 0 a primului bistabil al registrului. Pasul 3 de descarcare.
	@(posedge clk); // se executa frontul 8 de clock.
    d = 0;   //asignere cu 0 a primului bistabil al registrului. Pasul 4 de descarcare (registrul e descarcat complet).
	  end
endmodule

