module sreg4b (shiftout, shiftin, clk);
  input shiftin, clk;
  output shiftout;
  wire q3, q2, q1, q0;
  reg[3:0] word_out;
  reg[3:0] counter;

  dff G3 (.q(q3), .d(shiftin), .ck(clk));
  dff G2 (.q(q2), .d(q3), .ck(clk));
  dff G1 (.q(q1), .d(q2), .ck(clk));  
  dff G0 (.q(shiftout), .d(q1), .ck(clk));
  
  assign q0 = shiftout;
  
  initial counter = 0;  
  always @(posedge clk)
  begin
    if (counter == 4'b1000)  $stop;
    else 
     begin
       if (counter == 4'b0100)  word_out[0] = shiftout;
       if (counter == 4'b0101)  word_out[1] = shiftout;
       if (counter == 4'b0110)  word_out[2] = shiftout;
       if (counter == 4'b0111)  word_out[3] = shiftout;
       counter = counter + 1;
     end
  end
endmodule

