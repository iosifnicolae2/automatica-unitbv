module test_sreg ();
  wire shiftout, shiftin, clk;
  sreg4b DUT (.shiftout(shiftout), .shiftin(shiftin), .clk(clk));
  test_bench_sreg TB (.d(shiftin), .clk(clk));
endmodule

