%====================================================================
%simularea functionarii motorului asincron la flux statoric constant
%====================================================================

clear%sterge workspace
close all%inchide toate ferestrele active
A=input('Comanda U/f=const. = 1; Orientare dupa camp = 2.  ');
%----------------------------------------------------------------------
%parametrii schemei in T a motorului
%----------------------------------------------------------------------
m1=3;%numarul de faze ale infasurarii statorului
R1=0.30;             %rezistenta pe faza a statorului
R2=0.15;             %rezistenta pe faza a rotorului
Lh=31.83e-3;           %inductanta utila
sigma1=0.04;         %factor de cuplaj magnetic
sigma2=0.02;         %idem
sigma=1-1/((1+sigma1)*(1+sigma2));%idem
p=2;%nr de perechi de poli
L1sigma=sigma1*Lh;%inductanta de scapari a statorului
L2sigma=sigma2*Lh;%inductanta de scapari a rotorului raportata la stator
L1=Lh+L1sigma;%inductanta proprie a statorului
L2=Lh+L2sigma;%inductanta proprie a rotorului raportata la stator
T2=L2/R2;%constanta de timp a rotorului
%----------------------------------------------------------------------
%parametrii schemei in G a motorului
%----------------------------------------------------------------------
c1=1+sigma1;%constanta c1
R1stea=c1*R1;%rezistenta echivalenta a statorului
R2stea=c1^2*R2;%rezistenta echivalenta a rotorului raportata la stator
L1sigmastea=c1*L1sigma;%inductanta de scapari a statorului
L2sigmastea=c1^2*L2sigma;%inductanta de scapari s rotorului raportata la stator
Lsigmastea=L1sigmastea+L2sigmastea;%inductanta de scapari totala
%----------------------------------------------------------------------
%parametrii functionali
%----------------------------------------------------------------------

U1N=220;%tensiunea de alimentare nominala
f1N=50;%frecventa nominala a tensiunii de alimentare
U_f=U1N/f1N;%raportul tensiune/frecventa
omega1N=2*pi*f1N;          %pulsatia nominala a tensiunii de la retea
Det=0.1e-3;%perioada de esantionare
Pi=4*atan(1);        %numarul pi
J=0.3;               %momentul de inertie axial

%----------------------------------------------------------------------
%calculul caracteristicii mecanice
%----------------------------------------------------------------------
nmax=1000;%numarul maxim de iteratii
turatia_max=4000;%turatia maxima in rot/min
delta=turatia_max/nmax;%incrementul de turatie

turatia(1)=0;%valoarea initiala a turatiei
omega(1)=0;%valoarea initiala a pulsatiei echivalente
omega2(1)=0;%valoarea initiala a pulsatiei marimilor electrice din rotor
M(1)=0;%valoarea initiala a cuplului electromagnetic
f1(1)=0;%valoarea initiala a frecventei de alimentarea a statorului
U1(1)=0;%valoarea initiala a tensiunii de alimentare a statorului
deltaf1=10;%incrementul de crestere a frecventei

for k=1:7
    f1(k+1)=f1(k)+deltaf1;%frecventa tensiunii de alimentare a statorului
    omega1(k+1)=2*pi*f1(k+1);%pulsatia tensiunii de alimentare a statorului
    if f1(k+1)<=f1N %daca frecventa este mai mica sau egala cu frecventa nominala
        U1(k+1)=U_f*f1(k+1);%tensiunea de alimentare a statorului
    else f1(k+1)>f1N%daca frecventa este mai mare dacat frecventa nominala
        U1(k+1)=U1N;%tensiunea ramane egala cu tensiunea nominala
    end
        omegak=1/sigma/T2;%pulsatia critica
%     Mk(k+1)=m1*p*U1(k+1)^2/(2*omega1(k+1)*(R1stea+...
%     sqrt(R1stea^2+omega1(k+1)^2*Lsigmastea^2)));%cuplul critic
        Mk(k+1)=m1*p*U1(k+1)^2/(2*omega1(k+1)^2*Lsigmastea);%cuplul critic
    for n=1:nmax-1
        turatia(n+1)=turatia(n)+delta;%valorile de calcul ale turatiei
        omega(n+1)=p*pi*turatia(n+1)/30;%pulsatia echivalenta a rotorului
        omega2(n+1)=omega1(k+1)-omega(n+1);%pulsatia marimilor electrice din rotor
        switch A
            case 1%comanda U/f
            M(n+1)=2*omegak*Mk(k+1)*omega2(n+1)/(omega2(n+1)^2+omegak^2);%relatia lui Kloos
            I0(n+1)=U1(k+1)/(omega1(k+1)*L1sigmastea);%curentul de magnetizare echivalent
                Aux1(n+1)=j*(1-sigma)/sigma*omega2(n+1);
                Aux2(n+1)=omegak+j*omega2(n+1);
            I1(n+1)=I0(n+1)*(1+Aux1(n+1)/Aux2(n+1));%locul geometric al curentului statoric
            ReI1(n+1)=real(I1(n+1));
            ImI1(n+1)=imag(I1(n+1));
            case 2%orientarea dupa camp
            M(n+1)=2*Mk(k+1)*omega2(n+1)/omegak;%orientarea dupa camp
%             I0(n+1)=U1(k+1)/(omega1(k+1)*L1sigmastea);%curentul de magnetizare echivalent
            Aux3(n+1)=j*omega2(n+1)*T2;
            I1(n+1)=U1(k+1)/(omega1(k+1)*j*sigma*L1)*(1+Aux3(n+1));
            ReI1(n+1)=real(I1(n+1));
            ImI1(n+1)=imag(I1(n+1));
        end
    end
%caracteristicile mecanice
    switch k
    case 1
    case 2
        n=1;
        while (n+1)<=nmax
            M2(n)=M(n);
            tur(n)=turatia(n);
            ReI12(n)=ReI1(n+1);
            ImI12(n)=ImI1(n+1);
            n=n+1;
        end
    case 3
        n=1;
        while (n+1)<=nmax
            M3(n)=M(n);
            %tur(n)=turatia(n);
            ReI13(n)=ReI1(n+1);
            ImI13(n)=ImI1(n+1);
            n=n+1;
        end
    case 4
        n=1;
        while (n+1)<=nmax
            M4(n)=M(n);
            %tur(n)=turatia(n);
            ReI14(n)=ReI1(n+1);
            ImI14(n)=ImI1(n+1);
            n=n+1;
        end
    case 5
        n=1;
        while (n+1)<=nmax
            M5(n)=M(n);
            %tur(n)=turatia(n);
            ReI15(n)=ReI1(n+1);
            ImI15(n)=ImI1(n+1);
            n=n+1;
        end
    case 6
        n=1
        while (n+1)<=nmax
            M6(n)=M(n);
            %tur(n)=turatia(n);
            ReI16(n)=ReI1(n+1);
            ImI16(n)=ImI1(n+1);
            n=n+1;
        end
     case 7
         n=1
        while (n+1)<=nmax
            M7(n)=M(n);
            %tur(n)=turatia(n);
            ReI17(n)=ReI1(n+1);
            ImI17(n)=ImI1(n+1);
            n=n+1;
        end    
    end
end
subplot 121
plot(M2,tur,M3,tur,M4,tur,M5,tur,M6,tur,M7,tur)
title('CARACTERISTICILE MECANICE')
xlabel('Cuplul electromagnetic; [N.m]')
 ylabel('Turatia; [rot/min]')
grid

 subplot 122
 plot(ReI12, ImI12)
 xlabel('Fazorul I1 partea reala; [A]')
 ylabel('Fazorul I1 partea imaginara; [A]')
 title('LOCUL GEOMETRIC AL FAZORULUI CURENTULUI STATORIC')
 grid
% 
% subplot 223
% plot(timp,iU1)
% title('curentul de faza iU1')
% grid
% 
% subplot 224
% plot(timp,Mw)
% title('cuplul')
% grid

%Comentarii:
%pornirea depinde foarte mult de cum alegi perioada de esantionare!