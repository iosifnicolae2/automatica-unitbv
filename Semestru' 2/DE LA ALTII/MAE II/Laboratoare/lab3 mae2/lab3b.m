function lab3b
U1=220; %tensiunea nominala de faza
R1=0.3; %rezistenta statorica
R2prim=0.15; %rezistenta rotorica
Lh=31.8e-3; %inductanta utila
sigma=0.0572; %factor de cuplaj magnetic
sigma1=0.0398; 
sigma2=0.0201;
p=2; %nr de perechi de poli
f1=50; % frecventa de alimentare
omega1=2*pi*f1; %pulsatia tensiunii de alimentare
L1sigma=Lh*sigma1;
L2sigma=Lh*sigma2;

L1=Lh+L1sigma;
L2prim=Lh+L2sigma;
x1sigma=omega1*L1sigma;
x2sigma=omega1*L2sigma;
xh=omega1*Lh;
x1=xh+x1sigma;
x2prim=xh+x2sigma;

Z2=R2prim/0.07+j*x2sigma;
Zprim=Z2*j*xh/(Z2+j*xh);
Zechiv=R1+j*x1sigma+Zprim;
I1=U1/Zechiv;
aux=0;