clear%sterge workspace
close all%inchide toate ferestrele active
R1=0.30; %rezistenta pe faza a statorului
R2=0.15; %rezistenta pe faza a rotorului
Lh=31.85e-3; %inductanta utila
sigma=0.0572; %factor de cuplaj magnetic
sigma1=0.0398; %idem
sigma2=0.0201; %idem
p=2; %nr de perechi de poli
omega1=314;%pulsatia tensiunii de la retea
%Det=0.0001;%perioada de esantionare
Det=0.1e-3;
Pi=4*atan(1); %numarul pi
Mw=0; %cuplul de sarcina
J=0.3; %momentul de inertie axial
N=4000;
for k=1:N
timp(k)=k*Det;%vectorul esantioanelor de timp
uU1(k)=220*sqrt(2)*sin(omega1*timp(k));%tensiunea de faza U1
uV1(k)=220*sqrt(2)*sin(omega1*timp(k)-2*Pi/3);%tensiunea de faza V1
uA1(k)=sqrt(3/2)*uU1(k);%transformarea T32
uB1(k)=1/sqrt(2)*uU1(k)+sqrt(2)*uV1(k);%transformarea T32
alfa(k)=omega1*timp(k)-pi/2;%unghiul electric al cm statoric
ud1(k)=uA1(k)*cos(alfa(k))+uB1(k)*sin(alfa(k));%transformarea Talfa
uq1(k)=-uA1(k)*sin(alfa(k))+uB1(k)*cos(alfa(k));%transformarea Talfa
end
for u=1:N
Psid1(u)=0;
Psiq1(u)=0;
Psid2(u)=0;
Psiq2(u)=0;
id1(u)=0;
iq1(u)=0;
id2(u)=0;
iq2(u)=0;
omega(u)=0;
turatia(u)=0;%turatia rotorului
if u<3000
Mw(u)=0;%pornire in gol
else
Mw(u)=100;%salt de cuplu dupa 3 secunde
end
end
for t=2:N-1
Psid1(t+1)=Psid1(t)+Det*(ud1(t)-R1*id1(t)+omega1*Psiq1(t));
Psiq1(t+1)=Psiq1(t)+Det*(uq1(t)-R1*iq1(t)-omega1*Psid1(t));
Psid2(t+1)=Psid2(t)+Det*(-id2(t)*R2-(omega(t)-omega1)*Psiq2(t));
Psiq2(t+1)=Psiq2(t)+Det*(-iq2(t)*R2+(omega(t)-omega1)*Psid2(t));
omega(t+1)=omega(t)+Det*p/J*(p*(Psid1(t)*iq1(t)-Psiq1(t)*id1(t))-Mw(t));
turatia(t+1)=30/pi*omega(t+1)/p;%turatia rotorului in rpm
id1(t+1)=(1-sigma)/sigma/Lh*((1+sigma2)*Psid1(t+1)-Psid2(t+1));
iq1(t+1)=(1-sigma)/sigma/Lh*((1+sigma2)*Psiq1(t+1)-Psiq2(t+1));
id2(t+1)=(1-sigma)/sigma/Lh*(-Psid1(t+1)+(1+sigma1)*Psid2(t+1));
iq2(t+1)=(1-sigma)/sigma/Lh*(-Psiq1(t+1)+(1+sigma1)*Psiq2(t+1));
gama(t+1)=omega(t+1)*timp(t+1);%unghiul electric al rotorului
alfa(t+1)=omega1*timp(t+1);%unghiul electric al cm statoric
Tainv=[cos(alfa(t+1)) -sin(alfa(t+1));...
sin(alfa(t+1)) cos(alfa(t+1))];%transformarea Clark inversa curentiidin stator
iA1(t+1)=Tainv(1,1)*id1(t+1)+Tainv(1,2)*iq1(t+1);%curentul iA1
iB1(t+1)=Tainv(2,1)*id1(t+1)+Tainv(2,2)*iq1(t+1);%curentul iA2
T32inv=[sqrt(2/3) 0;-1/sqrt(6) 1/sqrt(2)];%transformarea Park inversa
iU1(t+1)=T32inv(1,1)*iA1(t+1)+T32inv(1,2)*iB1(t+1);%curentul pe faza U1
iV1(t+1)=T32inv(2,1)*iA1(t+1)+T32inv(2,2)*iB1(t+1);%curentul pe faza V1
end
%y=uU1';
%t=timp';
subplot 221
plot(timp,id1, timp,iq1)
title('id1 iq1')
grid
subplot 222
plot(timp, turatia)
title('turatia')
grid
subplot 223
plot(timp,iU1)
title('curentul de faza iU1')
grid
subplot 224
plot(timp,Mw)
title('cuplul')
grid