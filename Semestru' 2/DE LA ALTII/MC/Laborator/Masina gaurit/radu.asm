
_main:
;radu.c,1 :: 		void main()
;radu.c,4 :: 		A=1;
	MOVLW      1
	MOVWF      R1+0
;radu.c,5 :: 		B=0;
	CLRF       R2+0
;radu.c,6 :: 		C=0;
	CLRF       R3+0
;radu.c,7 :: 		D=1;
	MOVLW      1
	MOVWF      R4+0
;radu.c,8 :: 		E=0;
	CLRF       R5+0
;radu.c,9 :: 		TRISD.RD0=1;
	BSF        TRISD+0, 0
;radu.c,10 :: 		TRISD.RD1=1;
	BSF        TRISD+0, 1
;radu.c,11 :: 		TRISD.RD2=1;
	BSF        TRISD+0, 2
;radu.c,12 :: 		TRISD.RD3=1;
	BSF        TRISD+0, 3
;radu.c,13 :: 		TRISD.RD4=1;
	BSF        TRISD+0, 4
;radu.c,14 :: 		TRISA.RA0=0;
	BCF        TRISA+0, 0
;radu.c,15 :: 		TRISA.RA1=0;
	BCF        TRISA+0, 1
;radu.c,16 :: 		TRISA.RA2=0;
	BCF        TRISA+0, 2
;radu.c,17 :: 		TRISA.RA3=0;
	BCF        TRISA+0, 3
;radu.c,18 :: 		TRISA.RA4=0;
	BCF        TRISA+0, 4
;radu.c,19 :: 		while(1)
L_main0:
;radu.c,21 :: 		if((A==1)&&(PORTD.RD0==1)){A=0;B=1;C=0;D=0;E=0;}
	MOVF       R1+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main4
	BTFSS      PORTD+0, 0
	GOTO       L_main4
L__main26:
	CLRF       R1+0
	MOVLW      1
	MOVWF      R2+0
	CLRF       R3+0
	CLRF       R4+0
	CLRF       R5+0
L_main4:
;radu.c,22 :: 		if((B==1)&&(PORTD.RD2==1)){B=0;C=1;A=0;D=1;E=0;}
	MOVF       R2+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main7
	BTFSS      PORTD+0, 2
	GOTO       L_main7
L__main25:
	CLRF       R2+0
	MOVLW      1
	MOVWF      R3+0
	CLRF       R1+0
	MOVLW      1
	MOVWF      R4+0
	CLRF       R5+0
L_main7:
;radu.c,23 :: 		if((C==1)&&(PORTD.RD1==1)){C=0;A=1;B=0;E=0;D=1;}
	MOVF       R3+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main10
	BTFSS      PORTD+0, 1
	GOTO       L_main10
L__main24:
	CLRF       R3+0
	MOVLW      1
	MOVWF      R1+0
	CLRF       R2+0
	CLRF       R5+0
	MOVLW      1
	MOVWF      R4+0
L_main10:
;radu.c,24 :: 		if((D==1)&&(PORTD.RD4==1)){E=1;A=0;B=0;C=0;D=1;}
	MOVF       R4+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main13
	BTFSS      PORTD+0, 4
	GOTO       L_main13
L__main23:
	MOVLW      1
	MOVWF      R5+0
	CLRF       R1+0
	CLRF       R2+0
	CLRF       R3+0
	MOVLW      1
	MOVWF      R4+0
L_main13:
;radu.c,25 :: 		if((E==1)&&(PORTD.RD3==1)){E=0;D=1;A=0;B=0;C=0;}
	MOVF       R5+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main16
	BTFSS      PORTD+0, 3
	GOTO       L_main16
L__main22:
	CLRF       R5+0
	MOVLW      1
	MOVWF      R4+0
	CLRF       R1+0
	CLRF       R2+0
	CLRF       R3+0
L_main16:
;radu.c,27 :: 		if(A==1){PORTA.RA0=0;PORTA.RA1=0;PORTA.RA2=1;PORTA.RA3=0;PORTA.RA4=0;}
	MOVF       R1+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main17
	BCF        PORTA+0, 0
	BCF        PORTA+0, 1
	BSF        PORTA+0, 2
	BCF        PORTA+0, 3
	BCF        PORTA+0, 4
L_main17:
;radu.c,28 :: 		if(B==1){PORTA.RA0=1;PORTA.RA2=1;PORTA.RA4=1;PORTA.RA1=0;PORTA.RA3=0;}
	MOVF       R2+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main18
	BSF        PORTA+0, 0
	BSF        PORTA+0, 2
	BSF        PORTA+0, 4
	BCF        PORTA+0, 1
	BCF        PORTA+0, 3
L_main18:
;radu.c,29 :: 		if(C==1){PORTA.RA1=1;PORTA.RA2=1;PORTA.RA0=0;PORTA.RA3=0;PORTA.RA4=0;}
	MOVF       R3+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main19
	BSF        PORTA+0, 1
	BSF        PORTA+0, 2
	BCF        PORTA+0, 0
	BCF        PORTA+0, 3
	BCF        PORTA+0, 4
L_main19:
;radu.c,30 :: 		if(D==1){PORTA.RA2=1;PORTA.RA0=0;PORTA.RA1=0;PORTA.RA3=0;PORTA.RA4=0;}
	MOVF       R4+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main20
	BSF        PORTA+0, 2
	BCF        PORTA+0, 0
	BCF        PORTA+0, 1
	BCF        PORTA+0, 3
	BCF        PORTA+0, 4
L_main20:
;radu.c,31 :: 		if(E==1){PORTA.RA2=1;PORTA.RA0=0;PORTA.RA1=0;PORTA.RA3=0;PORTA.RA4=0;}
	MOVF       R5+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main21
	BSF        PORTA+0, 2
	BCF        PORTA+0, 0
	BCF        PORTA+0, 1
	BCF        PORTA+0, 3
	BCF        PORTA+0, 4
L_main21:
;radu.c,32 :: 		}
	GOTO       L_main0
;radu.c,33 :: 		}
	GOTO       $+0
; end of _main
