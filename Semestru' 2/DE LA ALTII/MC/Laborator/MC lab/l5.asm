
_main:
	MOVLW      63
	MOVWF      main_sir_L0+0
	MOVLW      6
	MOVWF      main_sir_L0+1
	MOVLW      91
	MOVWF      main_sir_L0+2
	MOVLW      79
	MOVWF      main_sir_L0+3
	MOVLW      102
	MOVWF      main_sir_L0+4
	MOVLW      109
	MOVWF      main_sir_L0+5
	MOVLW      125
	MOVWF      main_sir_L0+6
	MOVLW      39
	MOVWF      main_sir_L0+7
	MOVLW      127
	MOVWF      main_sir_L0+8
	MOVLW      111
	MOVWF      main_sir_L0+9
;l5.c,1 :: 		void main()
;l5.c,5 :: 		TRISA=0;
	CLRF       TRISA+0
;l5.c,6 :: 		TRISC=0;
	CLRF       TRISC+0
;l5.c,7 :: 		TRISB=128;
	MOVLW      128
	MOVWF      TRISB+0
;l5.c,8 :: 		TRISD=0;
	CLRF       TRISD+0
;l5.c,11 :: 		while(1)
L_main0:
;l5.c,13 :: 		PORTC=0;
	CLRF       PORTC+0
;l5.c,14 :: 		PORTD=33;
	MOVLW      33
	MOVWF      PORTD+0
;l5.c,15 :: 		PORTA=33;
	MOVLW      33
	MOVWF      PORTA+0
;l5.c,16 :: 		if(PORTB.RB7)
	BTFSS      PORTB+0, 7
	GOTO       L_main2
;l5.c,19 :: 		PORTD=34;
	MOVLW      34
	MOVWF      PORTD+0
;l5.c,20 :: 		PORTA=34;
	MOVLW      34
	MOVWF      PORTA+0
;l5.c,21 :: 		delay_ms(1000);
	MOVLW      11
	MOVWF      R11+0
	MOVLW      38
	MOVWF      R12+0
	MOVLW      93
	MOVWF      R13+0
L_main3:
	DECFSZ     R13+0, 1
	GOTO       L_main3
	DECFSZ     R12+0, 1
	GOTO       L_main3
	DECFSZ     R11+0, 1
	GOTO       L_main3
	NOP
	NOP
;l5.c,22 :: 		PORTD=12;
	MOVLW      12
	MOVWF      PORTD+0
;l5.c,23 :: 		PORTA=12;
	MOVLW      12
	MOVWF      PORTA+0
;l5.c,25 :: 		for(a=3;a>-1;a--)
	MOVLW      3
	MOVWF      R1+0
	MOVLW      0
	MOVWF      R1+1
L_main4:
	MOVLW      127
	MOVWF      R0+0
	MOVLW      128
	XORWF      R1+1, 0
	SUBWF      R0+0, 0
	BTFSS      STATUS+0, 2
	GOTO       L__main8
	MOVF       R1+0, 0
	SUBLW      255
L__main8:
	BTFSC      STATUS+0, 0
	GOTO       L_main5
;l5.c,27 :: 		PORTC=sir[a];
	MOVF       R1+0, 0
	ADDLW      main_sir_L0+0
	MOVWF      R0+0
	MOVF       R0+0, 0
	MOVWF      FSR
	MOVF       INDF+0, 0
	MOVWF      PORTC+0
;l5.c,29 :: 		PORTB=sir[a];
	MOVF       R0+0, 0
	MOVWF      FSR
	MOVF       INDF+0, 0
	MOVWF      PORTB+0
;l5.c,30 :: 		delay_ms(1000);
	MOVLW      11
	MOVWF      R11+0
	MOVLW      38
	MOVWF      R12+0
	MOVLW      93
	MOVWF      R13+0
L_main7:
	DECFSZ     R13+0, 1
	GOTO       L_main7
	DECFSZ     R12+0, 1
	GOTO       L_main7
	DECFSZ     R11+0, 1
	GOTO       L_main7
	NOP
	NOP
;l5.c,25 :: 		for(a=3;a>-1;a--)
	MOVLW      1
	SUBWF      R1+0, 1
	BTFSS      STATUS+0, 0
	DECF       R1+1, 1
;l5.c,32 :: 		}
	GOTO       L_main4
L_main5:
;l5.c,33 :: 		PORTD=20;
	MOVLW      20
	MOVWF      PORTD+0
;l5.c,34 :: 		PORTA=20;
	MOVLW      20
	MOVWF      PORTA+0
;l5.c,35 :: 		PORTB.RB7=0;
	BCF        PORTB+0, 7
;l5.c,37 :: 		}
L_main2:
;l5.c,41 :: 		}
	GOTO       L_main0
;l5.c,43 :: 		}
	GOTO       $+0
; end of _main
