
_interrupt:
	MOVWF      R15+0
	SWAPF      STATUS+0, 0
	CLRF       STATUS+0
	MOVWF      ___saveSTATUS+0
	MOVF       PCLATH+0, 0
	MOVWF      ___savePCLATH+0
	CLRF       PCLATH+0
;timer.c,20 :: 		void interrupt()
;timer.c,22 :: 		INTCON.GIE=0;
	BCF        INTCON+0, 7
;timer.c,23 :: 		if (INTCON.T0IF)
	BTFSS      INTCON+0, 2
	GOTO       L_interrupt0
;timer.c,25 :: 		i++;
	INCF       _i+0, 1
	BTFSC      STATUS+0, 2
	INCF       _i+1, 1
;timer.c,26 :: 		INTCON.T0IF=0;
	BCF        INTCON+0, 2
;timer.c,27 :: 		if(i==15)
	MOVLW      0
	XORWF      _i+1, 0
	BTFSS      STATUS+0, 2
	GOTO       L__interrupt5
	MOVLW      15
	XORWF      _i+0, 0
L__interrupt5:
	BTFSS      STATUS+0, 2
	GOTO       L_interrupt1
;timer.c,29 :: 		secunde+=1;
	INCF       _secunde+0, 1
	BTFSC      STATUS+0, 2
	INCF       _secunde+1, 1
;timer.c,30 :: 		i=0;
	CLRF       _i+0
	CLRF       _i+1
;timer.c,31 :: 		}
L_interrupt1:
;timer.c,34 :: 		TMR0=0;
	CLRF       TMR0+0
;timer.c,35 :: 		}
L_interrupt0:
;timer.c,36 :: 		INTCON.GIE=1;
	BSF        INTCON+0, 7
;timer.c,37 :: 		}
L__interrupt4:
	MOVF       ___savePCLATH+0, 0
	MOVWF      PCLATH+0
	SWAPF      ___saveSTATUS+0, 0
	MOVWF      STATUS+0
	SWAPF      R15+0, 1
	SWAPF      R15+0, 0
	RETFIE
; end of _interrupt

_main:
;timer.c,38 :: 		void main()
;timer.c,40 :: 		OPTION_REG=0b00000111;
	MOVLW      7
	MOVWF      OPTION_REG+0
;timer.c,41 :: 		INTCON.T0IE=1;
	BSF        INTCON+0, 5
;timer.c,42 :: 		INTCON.PEIE=1;
	BSF        INTCON+0, 6
;timer.c,43 :: 		INTCON.GIE=1;
	BSF        INTCON+0, 7
;timer.c,44 :: 		LCD_Init();
	CALL       _Lcd_Init+0
;timer.c,45 :: 		while(1)
L_main2:
;timer.c,47 :: 		sprinti(sir,"x=%d",secunde);
	MOVLW      _sir+0
	MOVWF      FARG_sprinti_wh+0
	MOVLW      ?lstr_1_timer+0
	MOVWF      FARG_sprinti_f+0
	MOVLW      hi_addr(?lstr_1_timer+0
	MOVWF      FARG_sprinti_f+1
	MOVF       _secunde+0, 0
	MOVWF      FARG_sprinti_wh+3
	MOVF       _secunde+1, 0
	MOVWF      FARG_sprinti_wh+4
	CALL       _sprinti+0
;timer.c,48 :: 		LCD_Out(1,1,sir);
	MOVLW      1
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      1
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      _sir+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
;timer.c,49 :: 		}
	GOTO       L_main2
;timer.c,53 :: 		}
	GOTO       $+0
; end of _main
