
_main:
;intreruperi.c,1 :: 		void main()
;intreruperi.c,2 :: 		{OPTION_REG=0;
	CLRF       OPTION_REG+0
;intreruperi.c,3 :: 		WPUB7_bit=1;
	BSF        WPUB7_bit+0, 7
;intreruperi.c,4 :: 		INTCON.GIE=1;
	BSF        INTCON+0, 7
;intreruperi.c,5 :: 		INTCON.INTE=1;
	BSF        INTCON+0, 4
;intreruperi.c,6 :: 		TRISB=1;;
	MOVLW      1
	MOVWF      TRISB+0
;intreruperi.c,7 :: 		PORTB=0;
	CLRF       PORTB+0
;intreruperi.c,8 :: 		TRISA=0;
	CLRF       TRISA+0
;intreruperi.c,9 :: 		PORTA=~PORTA;
	COMF       PORTA+0, 1
;intreruperi.c,10 :: 		}
	GOTO       $+0
; end of _main

_interrupt:
	MOVWF      R15+0
	SWAPF      STATUS+0, 0
	CLRF       STATUS+0
	MOVWF      ___saveSTATUS+0
	MOVF       PCLATH+0, 0
	MOVWF      ___savePCLATH+0
	CLRF       PCLATH+0
;intreruperi.c,11 :: 		void interrupt()
;intreruperi.c,13 :: 		INTCON.GIE=0;
	BCF        INTCON+0, 7
;intreruperi.c,14 :: 		if (INTCON.INTF)
	BTFSS      INTCON+0, 1
	GOTO       L_interrupt0
;intreruperi.c,16 :: 		INTCON.INTF=0;
	BCF        INTCON+0, 1
;intreruperi.c,17 :: 		}
L_interrupt0:
;intreruperi.c,18 :: 		INTCON.GIE=1;
	BSF        INTCON+0, 7
;intreruperi.c,19 :: 		}
L__interrupt1:
	MOVF       ___savePCLATH+0, 0
	MOVWF      PCLATH+0
	SWAPF      ___saveSTATUS+0, 0
	MOVWF      STATUS+0
	SWAPF      R15+0, 1
	SWAPF      R15+0, 0
	RETFIE
; end of _interrupt
