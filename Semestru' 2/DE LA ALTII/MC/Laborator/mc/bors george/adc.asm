
_initADC:
;adc.c,16 :: 		void initADC()
;adc.c,17 :: 		{ANSEL=0b00000001;
	MOVLW      1
	MOVWF      ANSEL+0
;adc.c,18 :: 		ADCON0=0b10000001;
	MOVLW      129
	MOVWF      ADCON0+0
;adc.c,19 :: 		TRISA=0b00100000;
	MOVLW      32
	MOVWF      TRISA+0
;adc.c,20 :: 		LCD_Init();
	CALL       _Lcd_Init+0
;adc.c,21 :: 		}
	RETURN
; end of _initADC

_main:
;adc.c,22 :: 		void main()
;adc.c,23 :: 		{   initADC();
	CALL       _initADC+0
;adc.c,24 :: 		while(1)
L_main0:
;adc.c,25 :: 		{ ADCON0.GO_DONE=1;
	BSF        ADCON0+0, 1
;adc.c,26 :: 		while(ADCON0.GO_DONE)
L_main2:
	BTFSS      ADCON0+0, 1
	GOTO       L_main3
;adc.c,28 :: 		}
	GOTO       L_main2
L_main3:
;adc.c,29 :: 		val=256*ADRESH+ADRESL ;
	MOVF       ADRESH+0, 0
	MOVWF      R0+1
	CLRF       R0+0
	MOVF       ADRESL+0, 0
	ADDWF      R0+0, 1
	BTFSC      STATUS+0, 0
	INCF       R0+1, 1
	MOVF       R0+0, 0
	MOVWF      _val+0
	MOVF       R0+1, 0
	MOVWF      _val+1
;adc.c,30 :: 		sprinti(buff,"%d ",val) ;
	MOVLW      _buff+0
	MOVWF      FARG_sprinti_wh+0
	MOVLW      ?lstr_1_adc+0
	MOVWF      FARG_sprinti_f+0
	MOVLW      hi_addr(?lstr_1_adc+0
	MOVWF      FARG_sprinti_f+1
	MOVF       R0+0, 0
	MOVWF      FARG_sprinti_wh+3
	MOVF       R0+1, 0
	MOVWF      FARG_sprinti_wh+4
	CALL       _sprinti+0
;adc.c,31 :: 		Lcd_Out(1,1,buff) ;
	MOVLW      1
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      1
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      _buff+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
;adc.c,33 :: 		}
	GOTO       L_main0
;adc.c,36 :: 		}
	GOTO       $+0
; end of _main
