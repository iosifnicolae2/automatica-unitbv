
_main:

;adc.c,15 :: 		void main()
;adc.c,19 :: 		TRISA=0xFF;
	MOVLW      255
	MOVWF      TRISA+0
;adc.c,20 :: 		LCD_init();
	CALL       _Lcd_Init+0
;adc.c,21 :: 		while(1)
L_main0:
;adc.c,23 :: 		var=ADC_Read(0)/ 205;
	CLRF       FARG_ADC_Read_channel+0
	CALL       _ADC_Read+0
	MOVLW      205
	MOVWF      R4+0
	CLRF       R4+1
	CALL       _Div_16x16_U+0
	CALL       _Word2Double+0
;adc.c,24 :: 		sprinti(buffer,"var=%e ",var);
	MOVLW      main_buffer_L0+0
	MOVWF      FARG_sprinti_wh+0
	MOVLW      ?lstr_1_adc+0
	MOVWF      FARG_sprinti_f+0
	MOVLW      hi_addr(?lstr_1_adc+0)
	MOVWF      FARG_sprinti_f+1
	MOVF       R0+0, 0
	MOVWF      FARG_sprinti_wh+3
	MOVF       R0+1, 0
	MOVWF      FARG_sprinti_wh+4
	MOVF       R0+2, 0
	MOVWF      FARG_sprinti_wh+5
	MOVF       R0+3, 0
	MOVWF      FARG_sprinti_wh+6
	CALL       _sprinti+0
;adc.c,25 :: 		Lcd_Out(1,1,buffer);
	MOVLW      1
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      1
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      main_buffer_L0+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
;adc.c,26 :: 		}
	GOTO       L_main0
;adc.c,27 :: 		}
	GOTO       $+0
; end of _main
