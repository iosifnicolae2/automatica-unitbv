
_main:

;masina_gaurit.c,1 :: 		void main()
;masina_gaurit.c,4 :: 		A=1;
	MOVLW      1
	MOVWF      R1+0
	MOVLW      0
	MOVWF      R1+1
;masina_gaurit.c,5 :: 		B=0;
	CLRF       R3+0
	CLRF       R3+1
;masina_gaurit.c,6 :: 		C=0;
	CLRF       R5+0
	CLRF       R5+1
;masina_gaurit.c,7 :: 		TRISC=0x00;
	CLRF       TRISC+0
;masina_gaurit.c,8 :: 		TRISB=0xFF;
	MOVLW      255
	MOVWF      TRISB+0
;masina_gaurit.c,9 :: 		while(1)
L_main0:
;masina_gaurit.c,11 :: 		if((A==1)&&(PORTB.RB5==1))
	MOVLW      0
	XORWF      R1+1, 0
	BTFSS      STATUS+0, 2
	GOTO       L__main14
	MOVLW      1
	XORWF      R1+0, 0
L__main14:
	BTFSS      STATUS+0, 2
	GOTO       L_main4
	BTFSS      PORTB+0, 5
	GOTO       L_main4
L__main13:
;masina_gaurit.c,12 :: 		{PORTC.RC3=0;
	BCF        PORTC+0, 3
;masina_gaurit.c,13 :: 		PORTC.RC4=0;
	BCF        PORTC+0, 4
;masina_gaurit.c,14 :: 		A=0;B=1;C=0;}
	CLRF       R1+0
	CLRF       R1+1
	MOVLW      1
	MOVWF      R3+0
	MOVLW      0
	MOVWF      R3+1
	CLRF       R5+0
	CLRF       R5+1
L_main4:
;masina_gaurit.c,15 :: 		if((B==1)&&(PORTB.RB6==1))
	MOVLW      0
	XORWF      R3+1, 0
	BTFSS      STATUS+0, 2
	GOTO       L__main15
	MOVLW      1
	XORWF      R3+0, 0
L__main15:
	BTFSS      STATUS+0, 2
	GOTO       L_main7
	BTFSS      PORTB+0, 6
	GOTO       L_main7
L__main12:
;masina_gaurit.c,16 :: 		{PORTC.RC3=1;
	BSF        PORTC+0, 3
;masina_gaurit.c,17 :: 		PORTC.RC4=0;
	BCF        PORTC+0, 4
;masina_gaurit.c,18 :: 		B=0;C=1;A=0;
	CLRF       R3+0
	CLRF       R3+1
	MOVLW      1
	MOVWF      R5+0
	MOVLW      0
	MOVWF      R5+1
	CLRF       R1+0
	CLRF       R1+1
;masina_gaurit.c,19 :: 		}
L_main7:
;masina_gaurit.c,20 :: 		if((C==1)&&(PORTB.RB7==1))
	MOVLW      0
	XORWF      R5+1, 0
	BTFSS      STATUS+0, 2
	GOTO       L__main16
	MOVLW      1
	XORWF      R5+0, 0
L__main16:
	BTFSS      STATUS+0, 2
	GOTO       L_main10
	BTFSS      PORTB+0, 7
	GOTO       L_main10
L__main11:
;masina_gaurit.c,21 :: 		{PORTC.RC3=0;
	BCF        PORTC+0, 3
;masina_gaurit.c,22 :: 		PORTC.RC4=1;
	BSF        PORTC+0, 4
;masina_gaurit.c,23 :: 		C=0;A=1;B=0;}
	CLRF       R5+0
	CLRF       R5+1
	MOVLW      1
	MOVWF      R1+0
	MOVLW      0
	MOVWF      R1+1
	CLRF       R3+0
	CLRF       R3+1
L_main10:
;masina_gaurit.c,24 :: 		}
	GOTO       L_main0
;masina_gaurit.c,25 :: 		}
	GOTO       $+0
; end of _main
