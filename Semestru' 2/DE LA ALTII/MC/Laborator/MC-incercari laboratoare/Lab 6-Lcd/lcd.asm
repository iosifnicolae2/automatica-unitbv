
_main:

;lcd.c,20 :: 		void main()
;lcd.c,22 :: 		Lcd_Init();
	CALL       _Lcd_Init+0
;lcd.c,24 :: 		TRISD=0;
	CLRF       TRISD+0
;lcd.c,25 :: 		while(1)
L_main0:
;lcd.c,27 :: 		if (RD0_bit==0)
	BTFSC      RD0_bit+0, 0
	GOTO       L_main2
;lcd.c,29 :: 		intrare=intrare+1;
	INCF       _intrare+0, 1
	BTFSC      STATUS+0, 2
	INCF       _intrare+1, 1
;lcd.c,30 :: 		while(RD0_bit==0)
L_main3:
	BTFSC      RD0_bit+0, 0
	GOTO       L_main4
;lcd.c,32 :: 		}
	GOTO       L_main3
L_main4:
;lcd.c,33 :: 		}
	GOTO       L_main5
L_main2:
;lcd.c,34 :: 		else if (RD3_bit==0)
	BTFSC      RD3_bit+0, 3
	GOTO       L_main6
;lcd.c,36 :: 		iesire=iesire+1;
	INCF       _iesire+0, 1
	BTFSC      STATUS+0, 2
	INCF       _iesire+1, 1
;lcd.c,37 :: 		while(RD3_bit==0)
L_main7:
	BTFSC      RD3_bit+0, 3
	GOTO       L_main8
;lcd.c,39 :: 		}
	GOTO       L_main7
L_main8:
;lcd.c,41 :: 		}
L_main6:
L_main5:
;lcd.c,42 :: 		sprinti(buffer,"Intrare %d",intrare);
	MOVLW      _buffer+0
	MOVWF      FARG_sprinti_wh+0
	MOVLW      ?lstr_1_lcd+0
	MOVWF      FARG_sprinti_f+0
	MOVLW      hi_addr(?lstr_1_lcd+0)
	MOVWF      FARG_sprinti_f+1
	MOVF       _intrare+0, 0
	MOVWF      FARG_sprinti_wh+3
	MOVF       _intrare+1, 0
	MOVWF      FARG_sprinti_wh+4
	CALL       _sprinti+0
;lcd.c,43 :: 		sprinti(buffer2,"Iesire %d",iesire);
	MOVLW      _buffer2+0
	MOVWF      FARG_sprinti_wh+0
	MOVLW      ?lstr_2_lcd+0
	MOVWF      FARG_sprinti_f+0
	MOVLW      hi_addr(?lstr_2_lcd+0)
	MOVWF      FARG_sprinti_f+1
	MOVF       _iesire+0, 0
	MOVWF      FARG_sprinti_wh+3
	MOVF       _iesire+1, 0
	MOVWF      FARG_sprinti_wh+4
	CALL       _sprinti+0
;lcd.c,44 :: 		Lcd_Out(1,1,buffer);
	MOVLW      1
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      1
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      _buffer+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
;lcd.c,45 :: 		Lcd_Out(2,1,buffer2);
	MOVLW      2
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      1
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      _buffer2+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
;lcd.c,46 :: 		}
	GOTO       L_main0
;lcd.c,47 :: 		}
	GOTO       $+0
; end of _main
