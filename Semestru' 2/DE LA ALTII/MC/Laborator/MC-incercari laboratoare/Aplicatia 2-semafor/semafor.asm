
_interrupt:
	MOVWF      R15+0
	SWAPF      STATUS+0, 0
	CLRF       STATUS+0
	MOVWF      ___saveSTATUS+0
	MOVF       PCLATH+0, 0
	MOVWF      ___savePCLATH+0
	CLRF       PCLATH+0

;semafor.c,20 :: 		void interrupt()
;semafor.c,22 :: 		if(INTCON.INTF==1)
	BTFSS      INTCON+0, 1
	GOTO       L_interrupt0
;semafor.c,24 :: 		Lcd_Init();
	CALL       _Lcd_Init+0
;semafor.c,25 :: 		j=j+1;
	INCF       _j+0, 1
	BTFSC      STATUS+0, 2
	INCF       _j+1, 1
;semafor.c,26 :: 		sprinti(buffer,"apasare=%d",j);
	MOVLW      _buffer+0
	MOVWF      FARG_sprinti_wh+0
	MOVLW      ?lstr_1_semafor+0
	MOVWF      FARG_sprinti_f+0
	MOVLW      hi_addr(?lstr_1_semafor+0)
	MOVWF      FARG_sprinti_f+1
	MOVF       _j+0, 0
	MOVWF      FARG_sprinti_wh+3
	MOVF       _j+1, 0
	MOVWF      FARG_sprinti_wh+4
	CALL       _sprinti+0
;semafor.c,27 :: 		Lcd_Out(1,1,buffer);
	MOVLW      1
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      1
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      _buffer+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
;semafor.c,30 :: 		PORTD=0b00010001;
	MOVLW      17
	MOVWF      PORTD+0
;semafor.c,31 :: 		delay_ms(500);
	MOVLW      6
	MOVWF      R11+0
	MOVLW      19
	MOVWF      R12+0
	MOVLW      173
	MOVWF      R13+0
L_interrupt1:
	DECFSZ     R13+0, 1
	GOTO       L_interrupt1
	DECFSZ     R12+0, 1
	GOTO       L_interrupt1
	DECFSZ     R11+0, 1
	GOTO       L_interrupt1
	NOP
	NOP
;semafor.c,32 :: 		PORTD=0b00001100;
	MOVLW      12
	MOVWF      PORTD+0
;semafor.c,33 :: 		delay_ms(1000);
	MOVLW      11
	MOVWF      R11+0
	MOVLW      38
	MOVWF      R12+0
	MOVLW      93
	MOVWF      R13+0
L_interrupt2:
	DECFSZ     R13+0, 1
	GOTO       L_interrupt2
	DECFSZ     R12+0, 1
	GOTO       L_interrupt2
	DECFSZ     R11+0, 1
	GOTO       L_interrupt2
	NOP
	NOP
;semafor.c,39 :: 		INTCON.INTF=0;
	BCF        INTCON+0, 1
;semafor.c,40 :: 		}
L_interrupt0:
;semafor.c,41 :: 		}
L__interrupt5:
	MOVF       ___savePCLATH+0, 0
	MOVWF      PCLATH+0
	SWAPF      ___saveSTATUS+0, 0
	MOVWF      STATUS+0
	SWAPF      R15+0, 1
	SWAPF      R15+0, 0
	RETFIE
; end of _interrupt

_main:

;semafor.c,43 :: 		void main()
;semafor.c,45 :: 		while(1)
L_main3:
;semafor.c,48 :: 		TRISD=0x00;
	CLRF       TRISD+0
;semafor.c,49 :: 		TRISC=0x00;
	CLRF       TRISC+0
;semafor.c,50 :: 		PORTD=0b00100001;
	MOVLW      33
	MOVWF      PORTD+0
;semafor.c,52 :: 		INTCON.GIE=1;
	BSF        INTCON+0, 7
;semafor.c,53 :: 		INTCON.INTE=1;
	BSF        INTCON+0, 4
;semafor.c,54 :: 		}
	GOTO       L_main3
;semafor.c,55 :: 		}
	GOTO       $+0
; end of _main
