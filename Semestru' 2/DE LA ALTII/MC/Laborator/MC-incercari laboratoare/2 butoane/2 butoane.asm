
_interrupt:
	MOVWF      R15+0
	SWAPF      STATUS+0, 0
	CLRF       STATUS+0
	MOVWF      ___saveSTATUS+0
	MOVF       PCLATH+0, 0
	MOVWF      ___savePCLATH+0
	CLRF       PCLATH+0

;2 butoane.c,1 :: 		void interrupt()
;2 butoane.c,3 :: 		TRISD=0x00;
	CLRF       TRISD+0
;2 butoane.c,4 :: 		if (INTCON.RBIF)
	BTFSS      INTCON+0, 0
	GOTO       L_interrupt0
;2 butoane.c,7 :: 		PORTD=0x00;
	CLRF       PORTD+0
;2 butoane.c,8 :: 		PORTD=0b00000011;
	MOVLW      3
	MOVWF      PORTD+0
;2 butoane.c,9 :: 		delay_ms(1000);
	MOVLW      11
	MOVWF      R11+0
	MOVLW      38
	MOVWF      R12+0
	MOVLW      93
	MOVWF      R13+0
L_interrupt1:
	DECFSZ     R13+0, 1
	GOTO       L_interrupt1
	DECFSZ     R12+0, 1
	GOTO       L_interrupt1
	DECFSZ     R11+0, 1
	GOTO       L_interrupt1
	NOP
	NOP
;2 butoane.c,10 :: 		INTCON.RBIF=0;
	BCF        INTCON+0, 0
;2 butoane.c,11 :: 		}
L_interrupt0:
;2 butoane.c,12 :: 		}
L__interrupt2:
	MOVF       ___savePCLATH+0, 0
	MOVWF      PCLATH+0
	SWAPF      ___saveSTATUS+0, 0
	MOVWF      STATUS+0
	SWAPF      R15+0, 1
	SWAPF      R15+0, 0
	RETFIE
; end of _interrupt

_main:

;2 butoane.c,13 :: 		void main()
;2 butoane.c,15 :: 		TRISB=0xff;
	MOVLW      255
	MOVWF      TRISB+0
;2 butoane.c,16 :: 		TRISD=0x00;
	CLRF       TRISD+0
;2 butoane.c,17 :: 		PORTD=0x00;
	CLRF       PORTD+0
;2 butoane.c,18 :: 		PORTD=0b00001100;
	MOVLW      12
	MOVWF      PORTD+0
;2 butoane.c,19 :: 		INTCON.GIE=1;
	BSF        INTCON+0, 7
;2 butoane.c,20 :: 		INTCON.RBIE=1;
	BSF        INTCON+0, 3
;2 butoane.c,26 :: 		}
	GOTO       $+0
; end of _main
