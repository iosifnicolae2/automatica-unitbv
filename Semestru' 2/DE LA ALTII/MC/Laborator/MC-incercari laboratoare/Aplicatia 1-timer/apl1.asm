
_interrupt:
	MOVWF      R15+0
	SWAPF      STATUS+0, 0
	CLRF       STATUS+0
	MOVWF      ___saveSTATUS+0
	MOVF       PCLATH+0, 0
	MOVWF      ___savePCLATH+0
	CLRF       PCLATH+0

;apl1.c,3 :: 		void interrupt()
;apl1.c,5 :: 		INTCON.GIE=0;
	BCF        INTCON+0, 7
;apl1.c,6 :: 		if (INTCON.T0IF)
	BTFSS      INTCON+0, 2
	GOTO       L_interrupt0
;apl1.c,8 :: 		i++;
	INCF       _i+0, 1
	BTFSC      STATUS+0, 2
	INCF       _i+1, 1
;apl1.c,9 :: 		INTCON.T0IF=0;
	BCF        INTCON+0, 2
;apl1.c,10 :: 		PORTD=sir[i%6];
	MOVLW      6
	MOVWF      R4+0
	MOVLW      0
	MOVWF      R4+1
	MOVF       _i+0, 0
	MOVWF      R0+0
	MOVF       _i+1, 0
	MOVWF      R0+1
	CALL       _Div_16x16_S+0
	MOVF       R8+0, 0
	MOVWF      R0+0
	MOVF       R8+1, 0
	MOVWF      R0+1
	MOVF       R0+0, 0
	ADDLW      _sir+0
	MOVWF      FSR
	MOVF       INDF+0, 0
	MOVWF      PORTD+0
;apl1.c,11 :: 		TMR0=110;
	MOVLW      110
	MOVWF      TMR0+0
;apl1.c,12 :: 		}
L_interrupt0:
;apl1.c,13 :: 		INTCON.GIE=1;
	BSF        INTCON+0, 7
;apl1.c,14 :: 		}
L__interrupt3:
	MOVF       ___savePCLATH+0, 0
	MOVWF      PCLATH+0
	SWAPF      ___saveSTATUS+0, 0
	MOVWF      STATUS+0
	SWAPF      R15+0, 1
	SWAPF      R15+0, 0
	RETFIE
; end of _interrupt

_main:

;apl1.c,16 :: 		void main()
;apl1.c,18 :: 		TRISD=0x00;
	CLRF       TRISD+0
;apl1.c,19 :: 		OPTION_REG=0b00000111;
	MOVLW      7
	MOVWF      OPTION_REG+0
;apl1.c,20 :: 		INTCON.T0IE=1;
	BSF        INTCON+0, 5
;apl1.c,21 :: 		INTCON.PEIE=1;
	BSF        INTCON+0, 6
;apl1.c,22 :: 		INTCON.GIE=1;
	BSF        INTCON+0, 7
;apl1.c,23 :: 		TMR0=110;
	MOVLW      110
	MOVWF      TMR0+0
;apl1.c,24 :: 		PORTD=sir[0];
	MOVF       _sir+0, 0
	MOVWF      PORTD+0
;apl1.c,25 :: 		while(1)
L_main1:
;apl1.c,28 :: 		}
	GOTO       L_main1
;apl1.c,29 :: 		}
	GOTO       $+0
; end of _main
