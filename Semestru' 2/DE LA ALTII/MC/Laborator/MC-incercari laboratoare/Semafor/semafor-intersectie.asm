
_main:

;semafor-intersectie.c,1 :: 		void main()
;semafor-intersectie.c,3 :: 		char sir[10]={63,134,219,207,230,237,253,135,255,239};
	MOVLW      63
	MOVWF      main_sir_L0+0
	MOVLW      134
	MOVWF      main_sir_L0+1
	MOVLW      219
	MOVWF      main_sir_L0+2
	MOVLW      207
	MOVWF      main_sir_L0+3
	MOVLW      230
	MOVWF      main_sir_L0+4
	MOVLW      237
	MOVWF      main_sir_L0+5
	MOVLW      253
	MOVWF      main_sir_L0+6
	MOVLW      135
	MOVWF      main_sir_L0+7
	MOVLW      255
	MOVWF      main_sir_L0+8
	MOVLW      239
	MOVWF      main_sir_L0+9
;semafor-intersectie.c,5 :: 		TRISA=0x00;
	CLRF       TRISA+0
;semafor-intersectie.c,6 :: 		TRISB=0x00;
	CLRF       TRISB+0
;semafor-intersectie.c,7 :: 		TRISC=0x00;
	CLRF       TRISC+0
;semafor-intersectie.c,8 :: 		TRISD=0x00;
	CLRF       TRISD+0
;semafor-intersectie.c,9 :: 		PORTA=0x00;
	CLRF       PORTA+0
;semafor-intersectie.c,10 :: 		PORTB=0x00;
	CLRF       PORTB+0
;semafor-intersectie.c,11 :: 		PORTC=0x00;
	CLRF       PORTC+0
;semafor-intersectie.c,12 :: 		PORTD=0x00;
	CLRF       PORTD+0
;semafor-intersectie.c,13 :: 		while(1)
L_main0:
;semafor-intersectie.c,15 :: 		a=9;
	MOVLW      9
	MOVWF      R1+0
	MOVLW      0
	MOVWF      R1+1
;semafor-intersectie.c,16 :: 		PORTD=1;
	MOVLW      1
	MOVWF      PORTD+0
;semafor-intersectie.c,17 :: 		PORTB=132;
	MOVLW      132
	MOVWF      PORTB+0
;semafor-intersectie.c,18 :: 		for(a;a>=0;a--)
L_main2:
	MOVLW      128
	XORWF      R1+1, 0
	MOVWF      R0+0
	MOVLW      128
	SUBWF      R0+0, 0
	BTFSS      STATUS+0, 2
	GOTO       L__main12
	MOVLW      0
	SUBWF      R1+0, 0
L__main12:
	BTFSS      STATUS+0, 0
	GOTO       L_main3
;semafor-intersectie.c,20 :: 		PORTC=sir[a];
	MOVF       R1+0, 0
	ADDLW      main_sir_L0+0
	MOVWF      FSR
	MOVF       INDF+0, 0
	MOVWF      PORTC+0
;semafor-intersectie.c,21 :: 		delay_ms(1000);
	MOVLW      11
	MOVWF      R11+0
	MOVLW      38
	MOVWF      R12+0
	MOVLW      93
	MOVWF      R13+0
L_main5:
	DECFSZ     R13+0, 1
	GOTO       L_main5
	DECFSZ     R12+0, 1
	GOTO       L_main5
	DECFSZ     R11+0, 1
	GOTO       L_main5
	NOP
	NOP
;semafor-intersectie.c,18 :: 		for(a;a>=0;a--)
	MOVLW      1
	SUBWF      R1+0, 1
	BTFSS      STATUS+0, 0
	DECF       R1+1, 1
;semafor-intersectie.c,22 :: 		}
	GOTO       L_main2
L_main3:
;semafor-intersectie.c,23 :: 		PORTD=10;
	MOVLW      10
	MOVWF      PORTD+0
;semafor-intersectie.c,24 :: 		PORTB=68;
	MOVLW      68
	MOVWF      PORTB+0
;semafor-intersectie.c,25 :: 		Delay_ms(3000);
	MOVLW      31
	MOVWF      R11+0
	MOVLW      113
	MOVWF      R12+0
	MOVLW      30
	MOVWF      R13+0
L_main6:
	DECFSZ     R13+0, 1
	GOTO       L_main6
	DECFSZ     R12+0, 1
	GOTO       L_main6
	DECFSZ     R11+0, 1
	GOTO       L_main6
	NOP
;semafor-intersectie.c,26 :: 		PORTD=33;
	MOVLW      33
	MOVWF      PORTD+0
;semafor-intersectie.c,27 :: 		PORTB=48;
	MOVLW      48
	MOVWF      PORTB+0
;semafor-intersectie.c,28 :: 		a=9;
	MOVLW      9
	MOVWF      R1+0
	MOVLW      0
	MOVWF      R1+1
;semafor-intersectie.c,29 :: 		for(a;a>=0;a--)
L_main7:
	MOVLW      128
	XORWF      R1+1, 0
	MOVWF      R0+0
	MOVLW      128
	SUBWF      R0+0, 0
	BTFSS      STATUS+0, 2
	GOTO       L__main13
	MOVLW      0
	SUBWF      R1+0, 0
L__main13:
	BTFSS      STATUS+0, 0
	GOTO       L_main8
;semafor-intersectie.c,31 :: 		PORTC=sir[a];
	MOVF       R1+0, 0
	ADDLW      main_sir_L0+0
	MOVWF      FSR
	MOVF       INDF+0, 0
	MOVWF      PORTC+0
;semafor-intersectie.c,32 :: 		delay_ms(1000);
	MOVLW      11
	MOVWF      R11+0
	MOVLW      38
	MOVWF      R12+0
	MOVLW      93
	MOVWF      R13+0
L_main10:
	DECFSZ     R13+0, 1
	GOTO       L_main10
	DECFSZ     R12+0, 1
	GOTO       L_main10
	DECFSZ     R11+0, 1
	GOTO       L_main10
	NOP
	NOP
;semafor-intersectie.c,29 :: 		for(a;a>=0;a--)
	MOVLW      1
	SUBWF      R1+0, 1
	BTFSS      STATUS+0, 0
	DECF       R1+1, 1
;semafor-intersectie.c,33 :: 		}
	GOTO       L_main7
L_main8:
;semafor-intersectie.c,34 :: 		PORTD=17;
	MOVLW      17
	MOVWF      PORTD+0
;semafor-intersectie.c,35 :: 		PORTB=40;
	MOVLW      40
	MOVWF      PORTB+0
;semafor-intersectie.c,36 :: 		Delay_ms(3000);
	MOVLW      31
	MOVWF      R11+0
	MOVLW      113
	MOVWF      R12+0
	MOVLW      30
	MOVWF      R13+0
L_main11:
	DECFSZ     R13+0, 1
	GOTO       L_main11
	DECFSZ     R12+0, 1
	GOTO       L_main11
	DECFSZ     R11+0, 1
	GOTO       L_main11
	NOP
;semafor-intersectie.c,38 :: 		}
	GOTO       L_main0
;semafor-intersectie.c,39 :: 		}
	GOTO       $+0
; end of _main
