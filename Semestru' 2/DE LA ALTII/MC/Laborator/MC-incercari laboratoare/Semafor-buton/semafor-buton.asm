
_main:

;semafor-buton.c,1 :: 		void main()
;semafor-buton.c,3 :: 		char sir[10]={63,134,219,207,230,237,253,135,255,239};
	MOVLW      63
	MOVWF      main_sir_L0+0
	MOVLW      134
	MOVWF      main_sir_L0+1
	MOVLW      219
	MOVWF      main_sir_L0+2
	MOVLW      207
	MOVWF      main_sir_L0+3
	MOVLW      230
	MOVWF      main_sir_L0+4
	MOVLW      237
	MOVWF      main_sir_L0+5
	MOVLW      253
	MOVWF      main_sir_L0+6
	MOVLW      135
	MOVWF      main_sir_L0+7
	MOVLW      255
	MOVWF      main_sir_L0+8
	MOVLW      239
	MOVWF      main_sir_L0+9
;semafor-buton.c,5 :: 		TRISA=0x00;
	CLRF       TRISA+0
;semafor-buton.c,6 :: 		TRISB=128;
	MOVLW      128
	MOVWF      TRISB+0
;semafor-buton.c,7 :: 		TRISC=0x00;
	CLRF       TRISC+0
;semafor-buton.c,8 :: 		TRISD=0x00;
	CLRF       TRISD+0
;semafor-buton.c,9 :: 		PORTC=0x00;
	CLRF       PORTC+0
;semafor-buton.c,10 :: 		PORTD=0x00;
	CLRF       PORTD+0
;semafor-buton.c,11 :: 		while(1)
L_main0:
;semafor-buton.c,13 :: 		PORTC=0;
	CLRF       PORTC+0
;semafor-buton.c,14 :: 		PORTD=12;
	MOVLW      12
	MOVWF      PORTD+0
;semafor-buton.c,15 :: 		PORTA=132;
	MOVLW      132
	MOVWF      PORTA+0
;semafor-buton.c,16 :: 		if(PORTB.RB7==1)
	BTFSS      PORTB+0, 7
	GOTO       L_main2
;semafor-buton.c,18 :: 		PORTD=10;
	MOVLW      10
	MOVWF      PORTD+0
;semafor-buton.c,19 :: 		PORTA=68;
	MOVLW      68
	MOVWF      PORTA+0
;semafor-buton.c,20 :: 		delay_ms(3000);
	MOVLW      31
	MOVWF      R11+0
	MOVLW      113
	MOVWF      R12+0
	MOVLW      30
	MOVWF      R13+0
L_main3:
	DECFSZ     R13+0, 1
	GOTO       L_main3
	DECFSZ     R12+0, 1
	GOTO       L_main3
	DECFSZ     R11+0, 1
	GOTO       L_main3
	NOP
;semafor-buton.c,22 :: 		PORTD=33;
	MOVLW      33
	MOVWF      PORTD+0
;semafor-buton.c,23 :: 		PORTA=48;
	MOVLW      48
	MOVWF      PORTA+0
;semafor-buton.c,24 :: 		for(a=9;a>=0;a--)
	MOVLW      9
	MOVWF      R1+0
	MOVLW      0
	MOVWF      R1+1
L_main4:
	MOVLW      128
	XORWF      R1+1, 0
	MOVWF      R0+0
	MOVLW      128
	SUBWF      R0+0, 0
	BTFSS      STATUS+0, 2
	GOTO       L__main8
	MOVLW      0
	SUBWF      R1+0, 0
L__main8:
	BTFSS      STATUS+0, 0
	GOTO       L_main5
;semafor-buton.c,26 :: 		PORTC=sir[a];
	MOVF       R1+0, 0
	ADDLW      main_sir_L0+0
	MOVWF      FSR
	MOVF       INDF+0, 0
	MOVWF      PORTC+0
;semafor-buton.c,27 :: 		delay_ms(1000);
	MOVLW      11
	MOVWF      R11+0
	MOVLW      38
	MOVWF      R12+0
	MOVLW      93
	MOVWF      R13+0
L_main7:
	DECFSZ     R13+0, 1
	GOTO       L_main7
	DECFSZ     R12+0, 1
	GOTO       L_main7
	DECFSZ     R11+0, 1
	GOTO       L_main7
	NOP
	NOP
;semafor-buton.c,24 :: 		for(a=9;a>=0;a--)
	MOVLW      1
	SUBWF      R1+0, 1
	BTFSS      STATUS+0, 0
	DECF       R1+1, 1
;semafor-buton.c,28 :: 		}
	GOTO       L_main4
L_main5:
;semafor-buton.c,29 :: 		PORTD=17;
	MOVLW      17
	MOVWF      PORTD+0
;semafor-buton.c,30 :: 		PORTA=40;
	MOVLW      40
	MOVWF      PORTA+0
;semafor-buton.c,31 :: 		PORTB.RB7=0;
	BCF        PORTB+0, 7
;semafor-buton.c,32 :: 		}
L_main2:
;semafor-buton.c,33 :: 		}
	GOTO       L_main0
;semafor-buton.c,34 :: 		}
	GOTO       $+0
; end of _main
