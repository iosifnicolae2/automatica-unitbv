
_main:
	CLRF       main_iesire_L0+0
	CLRF       main_iesire_L0+1
	CLRF       main_intrare_L0+0
	CLRF       main_intrare_L0+1
	MOVLW      15
	MOVWF      main_temp_L0+0
	MOVLW      0
	MOVWF      main_temp_L0+1
;lab7.c,20 :: 		void main()
;lab7.c,26 :: 		Lcd_Init();
	CALL       _Lcd_Init+0
;lab7.c,28 :: 		TRISD=0;
	CLRF       TRISD+0
;lab7.c,29 :: 		while(1)
L_main0:
;lab7.c,31 :: 		if (RD0_bit==0)
	BTFSC      RD0_bit+0, 0
	GOTO       L_main2
;lab7.c,33 :: 		intrare +=1  ;  // intrare = intrare +1 ;
	INCF       main_intrare_L0+0, 1
	BTFSC      STATUS+0, 2
	INCF       main_intrare_L0+1, 1
;lab7.c,34 :: 		while (RD0_bit == 0)
L_main3:
	BTFSC      RD0_bit+0, 0
	GOTO       L_main4
;lab7.c,35 :: 		{}
	GOTO       L_main3
L_main4:
;lab7.c,36 :: 		}
	GOTO       L_main5
L_main2:
;lab7.c,37 :: 		else if (RD5_bit==0)
	BTFSC      RD5_bit+0, 5
	GOTO       L_main6
;lab7.c,39 :: 		iesire ++ ;
	INCF       main_iesire_L0+0, 1
	BTFSC      STATUS+0, 2
	INCF       main_iesire_L0+1, 1
;lab7.c,40 :: 		while (RD5_bit == 0);
L_main7:
	BTFSC      RD5_bit+0, 5
	GOTO       L_main8
	GOTO       L_main7
L_main8:
;lab7.c,41 :: 		}
L_main6:
L_main5:
;lab7.c,42 :: 		sprinti(buffer , "Au intrat %d pers" , intrare );
	MOVLW      main_buffer_L0+0
	MOVWF      FARG_sprinti_wh+0
	MOVLW      ?lstr_1_lab7+0
	MOVWF      FARG_sprinti_f+0
	MOVLW      hi_addr(?lstr_1_lab7+0
	MOVWF      FARG_sprinti_f+1
	MOVF       main_intrare_L0+0, 0
	MOVWF      FARG_sprinti_wh+3
	MOVF       main_intrare_L0+1, 0
	MOVWF      FARG_sprinti_wh+4
	CALL       _sprinti+0
;lab7.c,43 :: 		sprinti(buffer2, "Au iesit %d pers" , iesire );
	MOVLW      main_buffer2_L0+0
	MOVWF      FARG_sprinti_wh+0
	MOVLW      ?lstr_2_lab7+0
	MOVWF      FARG_sprinti_f+0
	MOVLW      hi_addr(?lstr_2_lab7+0
	MOVWF      FARG_sprinti_f+1
	MOVF       main_iesire_L0+0, 0
	MOVWF      FARG_sprinti_wh+3
	MOVF       main_iesire_L0+1, 0
	MOVWF      FARG_sprinti_wh+4
	CALL       _sprinti+0
;lab7.c,44 :: 		Lcd_Out(1,1,buffer);
	MOVLW      1
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      1
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      main_buffer_L0+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
;lab7.c,45 :: 		Lcd_Out(2,1,buffer2);
	MOVLW      2
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      1
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      main_buffer2_L0+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
;lab7.c,46 :: 		}
	GOTO       L_main0
;lab7.c,47 :: 		}
	GOTO       $+0
; end of _main
