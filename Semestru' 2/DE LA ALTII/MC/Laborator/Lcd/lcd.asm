
_main:
;lcd.c,25 :: 		void main()
;lcd.c,27 :: 		TRISD=0;
	CLRF       TRISD+0
;lcd.c,29 :: 		Lcd_Init();
	CALL       _Lcd_Init+0
;lcd.c,31 :: 		while (1)
L_main0:
;lcd.c,33 :: 		if (RD0_bit==0)
	BTFSC      RD0_bit+0, 0
	GOTO       L_main2
;lcd.c,35 :: 		intrare=intrare+1;
	INCF       _intrare+0, 1
	BTFSC      STATUS+0, 2
	INCF       _intrare+1, 1
;lcd.c,36 :: 		while (RD0_bit==0)
L_main3:
	BTFSC      RD0_bit+0, 0
	GOTO       L_main4
;lcd.c,38 :: 		}
	GOTO       L_main3
L_main4:
;lcd.c,39 :: 		}
	GOTO       L_main5
L_main2:
;lcd.c,40 :: 		else if (RD3_bit==0)
	BTFSC      RD3_bit+0, 3
	GOTO       L_main6
;lcd.c,42 :: 		iesire=iesire+1;
	INCF       _iesire+0, 1
	BTFSC      STATUS+0, 2
	INCF       _iesire+1, 1
;lcd.c,43 :: 		while (RD3_bit==0)
L_main7:
	BTFSC      RD3_bit+0, 3
	GOTO       L_main8
;lcd.c,45 :: 		}
	GOTO       L_main7
L_main8:
;lcd.c,46 :: 		}
L_main6:
L_main5:
;lcd.c,47 :: 		sprinti(buffer,"Intrare=%d",intrare);
	MOVLW      _buffer+0
	MOVWF      FARG_sprinti_wh+0
	MOVLW      ?lstr_1_lcd+0
	MOVWF      FARG_sprinti_f+0
	MOVLW      hi_addr(?lstr_1_lcd+0
	MOVWF      FARG_sprinti_f+1
	MOVF       _intrare+0, 0
	MOVWF      FARG_sprinti_wh+3
	MOVF       _intrare+1, 0
	MOVWF      FARG_sprinti_wh+4
	CALL       _sprinti+0
;lcd.c,48 :: 		sprinti(buffer2,"Iesire=%d",iesire);
	MOVLW      _buffer2+0
	MOVWF      FARG_sprinti_wh+0
	MOVLW      ?lstr_2_lcd+0
	MOVWF      FARG_sprinti_f+0
	MOVLW      hi_addr(?lstr_2_lcd+0
	MOVWF      FARG_sprinti_f+1
	MOVF       _iesire+0, 0
	MOVWF      FARG_sprinti_wh+3
	MOVF       _iesire+1, 0
	MOVWF      FARG_sprinti_wh+4
	CALL       _sprinti+0
;lcd.c,49 :: 		Lcd_Out(1,1,buffer);
	MOVLW      1
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      1
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      _buffer+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
;lcd.c,50 :: 		Lcd_Out(2,1,buffer2);
	MOVLW      2
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      1
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      _buffer2+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
;lcd.c,51 :: 		}
	GOTO       L_main0
;lcd.c,52 :: 		}
	GOTO       $+0
; end of _main
