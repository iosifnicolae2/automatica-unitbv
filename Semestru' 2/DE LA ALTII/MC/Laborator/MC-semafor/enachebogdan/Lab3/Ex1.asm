.model small
.stack 10h
.data

         sir db 10,20,-30,100,-100,200
         lung dw $-sir
         maxim db ?
         mes_sir_vid db 'sir vid de valori$'
.code
start:
         mov  ax,@data     ;initializare registru segment
         mov  ds,ax
         mov  cx,lung      ;contor numar de valori din sir
         jcxz sir_vid      ;dc sirul este vid se tipareste mesaj
         lea  si,sir       ;index elemente din sir
         cld               ;directia de parcurgere
         mov  bl,[si]      ;se initializeaza valoarea maxima din sir
         inc  si           ;actualizare index elemente
         dec  cx           ;actualizare contor
         jcxz gata         ;daca a fost un singur element, s-a terminat

iar:     lodsb             ;citeste element curent din sir
         cmp  bl,al        ;se compara cu maximul curent
         jge  urm          ;daca maximul>val.curenta, trece la elementul urmator
                           ;pentru numere fara semn se inlocuieste "jge" cu "jnc"
         mov  bl,al        ;altfel,se actualizeaza valoarea maxima
urm:     loop iar          ;se reia ciclul daca mai sunt elemente din sir
gata:    mov  maxim,bl     ;se depune valoarea maxima
iesire:  mov  ax,4c00h     ;revenire DOS
         int  21h
sir_vid: lea  dx,mes_sir_vid    ;se afiseaza mesajul
         mov  ah,9         ;'sir vid de valori'
         int  21h
         jmp  iesire       ;revenire DOS
end start
