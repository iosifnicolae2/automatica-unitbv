.model small
.stack 20h
.data

         executie db 0Dh,0Ah,'executie secventa(1,2,3 sau 4=stop):$'
         mes_secv1 db 'Se executa secventa 1!',0Dh,0Ah,'$'
         mes_secv2 db 'Se executa secventa 2!',0Dh,0Ah,'$'
         mes_secv3 db 'Se executa secventa 3!',0Dh,0Ah,'$'
         tab_secv label word
                        dw secv1
                        dw secv2
                        dw secv3
                        dw gata
.code
start:   mov  ax,@data           ;initializare adresa de segment
         mov  ds,ax

iar:     lea  dx,executie        ;solicita secventa dorita utilizatorului
         mov  ah,9               ;se tipareste mesajul de selectie secventa
         int  21h                ;apel functia 9,tiparire mesaj
         mov  ah,1               ;apel functia 1,citire carater
         int  21h                ;caracter returnat in AL
         sub  al,31h             ;se transforma intervalul '1'->'4' in '0'->'3'
         jc   iar                ;verifica daca este in intervalul '0'->'3': daca nu se
         cmp  al,4               ;cere introducerea unei valori,in acest interval,
         jnc  iar                ;fara a se executa vreuna dintre secvente
         cbw                     ;extensie pe 16 biti
         mov  bx,ax              
         shl  bx,1               ;inmultirea cu doi,pentru a obtine adresa relativa
         jmp  word ptr tab_secv[bx] ;in tabela cu adresele secventelor
secv1:   lea  dx,mes_secv1       ;se executa prima secventa
         mov  ah,9
         int  21h
         jmp  short iar
secv2:   lea  dx,mes_secv2       ;se executa a doua secventa
         mov  ah,9
       	 int  21h
         jmp  short iar
secv3:   lea  dx,mes_secv3       ;se executa a treia secventa
         mov  ah,9
         int  21h
         jmp  short iar
gata:    mov  ax,4c00h           ;revenire DOS
         int  21h
end start