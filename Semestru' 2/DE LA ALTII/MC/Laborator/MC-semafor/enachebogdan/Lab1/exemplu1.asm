;Programul citeste un caracter folosind 01h si apoi il afiseaza folosind 02h.
.MODEL SMALL
.STACK 100h
.data
Mesaj DB   13,10,'Apasati o tasta$'
NewLine    DB     13,10,'$'
aux   db   ?

.CODE
start:     mov   ax,@data
           mov	 ds,ax
 	   
	   mov   dx,OFFSET Mesaj
	   mov   ah,9
           int   21h
	   mov   dx,OFFSET NewLine
	   mov   ah,9
	   int   21h
	   mov	 ah,1
	   int   21h
	   mov   aux,al
           mov   dx,OFFSET NewLine
           mov   ah,9
           int   21h
           mov   dl,aux
           mov   ah,2
           int   21h
           mov   ah,7
           int   21h
 
           mov   ah,4ch
      	   mov   al,0
	   int   21h

end start