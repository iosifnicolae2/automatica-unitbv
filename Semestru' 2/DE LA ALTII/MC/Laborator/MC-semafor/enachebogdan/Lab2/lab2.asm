
_main:
	CLRF       main_a_L0+0
	CLRF       main_a_L0+1
	MOVLW      63
	MOVWF      main_sir_L0+0
	MOVLW      134
	MOVWF      main_sir_L0+1
	MOVLW      219
	MOVWF      main_sir_L0+2
	MOVLW      207
	MOVWF      main_sir_L0+3
	MOVLW      230
	MOVWF      main_sir_L0+4
	MOVLW      237
	MOVWF      main_sir_L0+5
	MOVLW      253
	MOVWF      main_sir_L0+6
	MOVLW      135
	MOVWF      main_sir_L0+7
	MOVLW      255
	MOVWF      main_sir_L0+8
	MOVLW      239
	MOVWF      main_sir_L0+9
;lab2.c,1 :: 		void main() {
;lab2.c,4 :: 		TRISA=0;
	CLRF       TRISA+0
;lab2.c,5 :: 		TRISB=0;
	CLRF       TRISB+0
;lab2.c,6 :: 		TRISC=0;
	CLRF       TRISC+0
;lab2.c,7 :: 		TRISD=0;
	CLRF       TRISD+0
;lab2.c,8 :: 		TRISE=0;
	CLRF       TRISE+0
;lab2.c,10 :: 		while(1)
L_main0:
;lab2.c,12 :: 		if(PORTA.RA3=0)
	BCF        PORTA+0, 3
	BTFSS      PORTA+0, 3
	GOTO       L_main2
;lab2.c,13 :: 		{PORTB.RB7=1;
	BSF        PORTB+0, 7
;lab2.c,14 :: 		PORTA.RA0=1;
	BSF        PORTA+0, 0
;lab2.c,15 :: 		PORTE=0b00000100;
	MOVLW      4
	MOVWF      PORTE+0
;lab2.c,16 :: 		PORTB.RB2=1;
	BSF        PORTB+0, 2
;lab2.c,17 :: 		for(a;a<10;a++)
L_main3:
	MOVLW      128
	XORWF      main_a_L0+1, 0
	MOVWF      R0+0
	MOVLW      128
	SUBWF      R0+0, 0
	BTFSS      STATUS+0, 2
	GOTO       L__main12
	MOVLW      10
	SUBWF      main_a_L0+0, 0
L__main12:
	BTFSC      STATUS+0, 0
	GOTO       L_main4
;lab2.c,19 :: 		PORTD=sir[a];
	MOVF       main_a_L0+0, 0
	ADDLW      main_sir_L0+0
	MOVWF      R0+0
	MOVF       R0+0, 0
	MOVWF      FSR
	MOVF       INDF+0, 0
	MOVWF      PORTD+0
;lab2.c,20 :: 		PORTC=sir[a];
	MOVF       R0+0, 0
	MOVWF      FSR
	MOVF       INDF+0, 0
	MOVWF      PORTC+0
;lab2.c,21 :: 		delay_ms(1000);
	MOVLW      11
	MOVWF      R11+0
	MOVLW      38
	MOVWF      R12+0
	MOVLW      93
	MOVWF      R13+0
L_main6:
	DECFSZ     R13+0, 1
	GOTO       L_main6
	DECFSZ     R12+0, 1
	GOTO       L_main6
	DECFSZ     R11+0, 1
	GOTO       L_main6
	NOP
	NOP
;lab2.c,17 :: 		for(a;a<10;a++)
	INCF       main_a_L0+0, 1
	BTFSC      STATUS+0, 2
	INCF       main_a_L0+1, 1
;lab2.c,22 :: 		}
	GOTO       L_main3
L_main4:
;lab2.c,23 :: 		PORTB.RB7=0;
	BCF        PORTB+0, 7
;lab2.c,24 :: 		PORTA.RA0=0;
	BCF        PORTA+0, 0
;lab2.c,25 :: 		PORTE=0;
	CLRF       PORTE+0
;lab2.c,26 :: 		PORTB.RB2=0;
	BCF        PORTB+0, 2
;lab2.c,27 :: 		}
	GOTO       L_main7
L_main2:
;lab2.c,29 :: 		{ a=0;
	CLRF       main_a_L0+0
	CLRF       main_a_L0+1
;lab2.c,31 :: 		PORTA.RA2=1;
	BSF        PORTA+0, 2
;lab2.c,32 :: 		PORTB.RB5=1;
	BSF        PORTB+0, 5
;lab2.c,33 :: 		PORTB.RB4=1;
	BSF        PORTB+0, 4
;lab2.c,34 :: 		PORTE=0b0000001;
	MOVLW      1
	MOVWF      PORTE+0
;lab2.c,35 :: 		for(a;a<10;a++)
L_main8:
	MOVLW      128
	XORWF      main_a_L0+1, 0
	MOVWF      R0+0
	MOVLW      128
	SUBWF      R0+0, 0
	BTFSS      STATUS+0, 2
	GOTO       L__main13
	MOVLW      10
	SUBWF      main_a_L0+0, 0
L__main13:
	BTFSC      STATUS+0, 0
	GOTO       L_main9
;lab2.c,37 :: 		PORTD=sir[a];
	MOVF       main_a_L0+0, 0
	ADDLW      main_sir_L0+0
	MOVWF      R0+0
	MOVF       R0+0, 0
	MOVWF      FSR
	MOVF       INDF+0, 0
	MOVWF      PORTD+0
;lab2.c,38 :: 		PORTC=sir[a];
	MOVF       R0+0, 0
	MOVWF      FSR
	MOVF       INDF+0, 0
	MOVWF      PORTC+0
;lab2.c,39 :: 		delay_ms(1000);
	MOVLW      11
	MOVWF      R11+0
	MOVLW      38
	MOVWF      R12+0
	MOVLW      93
	MOVWF      R13+0
L_main11:
	DECFSZ     R13+0, 1
	GOTO       L_main11
	DECFSZ     R12+0, 1
	GOTO       L_main11
	DECFSZ     R11+0, 1
	GOTO       L_main11
	NOP
	NOP
;lab2.c,35 :: 		for(a;a<10;a++)
	INCF       main_a_L0+0, 1
	BTFSC      STATUS+0, 2
	INCF       main_a_L0+1, 1
;lab2.c,40 :: 		}
	GOTO       L_main8
L_main9:
;lab2.c,41 :: 		PORTA.RA2=0;
	BCF        PORTA+0, 2
;lab2.c,42 :: 		PORTB.RB5=0;
	BCF        PORTB+0, 5
;lab2.c,43 :: 		PORTB.RB4=0;
	BCF        PORTB+0, 4
;lab2.c,44 :: 		PORTE=0;
	CLRF       PORTE+0
;lab2.c,45 :: 		}
L_main7:
;lab2.c,46 :: 		}
	GOTO       L_main0
;lab2.c,51 :: 		}
	GOTO       $+0
; end of _main
