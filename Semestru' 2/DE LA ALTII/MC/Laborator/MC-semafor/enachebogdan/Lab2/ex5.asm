.model     small
.stack     100h


.data
        string1 DB    "oralaborator"
        lstr1   EQU   $-string1
        mes1    DB    "Sirurile coincid",0Dh,0Ah,"$"
        mes2    DB    "Sirurile nu coincid",0Dh,0Ah,"$"
        string2 DB    "oradelaborator"   

.code
start:
        mov  ax,@data
        mov  ds,ax
        mov  ax,@data
        mov  es,ax
        cld
        mov  cx,lstr1
        mov  si,OFFSET string1
        mov  di,OFFSET string2
        repe cmpsb
        jz   ALL
        mov  dx,OFFSET mes2
        mov  ah,09h
        int  21h
        jmp  sf
ALL:
        mov  dx,OFFSET mes1
        mov  ah,09h
        int  21h
sf:
        mov  ax,4c00h
        int  21h
END start 