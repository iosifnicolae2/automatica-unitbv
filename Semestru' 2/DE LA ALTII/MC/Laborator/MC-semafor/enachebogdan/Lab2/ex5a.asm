.model      small
.stack      100h

.data
         string  DB  "ora laborator"
         lstring EQU $-string
         pstring DD  string
         mes1    DB  "Caracterul nu exista",0Dh,0Ah,"$"
         mes2    DB  "Caracterul exista",0Dh,0Ah,"$"

.code
start: 
         mov   ax,@data
         mov   ds,ax
         cld
         mov   cx,lstring
         les   di,pstring
         mov   al,"z"
         repne scasb
         jnz   notfound
         mov   dx,OFFSET mes2
         mov   ah,09h
         int   21h
         jmp   sf
notfound:
         mov   dx,OFFSET mes1
         mov   ah,09h
         int   21h
sf:
         mov   ax,4c00h
               int   21h
         end   start