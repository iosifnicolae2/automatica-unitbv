.model small
.stack
.data
	Ent db 13,10,"$"
	Sir1 db "Sir de caractere$"
	Sir2 db 17 dup(0)
.code
start:
	mov ax,@data
	mov ds,ax
	mov es,ax
	mov ah,9
	mov dx,OFFSET Sir1
	int 21h
	cld
	mov si,OFFSET Sir1
	mov di,OFFSET Sir2
	mov cx,17
	rep movsb
	mov ah,9
	mov dx,OFFSET Ent
	int 21h
	mov dx,OFFSET Sir2
	int 21h
	mov ax,4C00h
	int 21h