
_main:
	MOVLW      1
	MOVWF      main_sin_L0+0
	MOVLW      2
	MOVWF      main_sin_L0+1
	MOVLW      4
	MOVWF      main_sin_L0+2
	MOVLW      8
	MOVWF      main_sin_L0+3
	MOVLW      16
	MOVWF      main_sin_L0+4
	MOVLW      32
	MOVWF      main_sin_L0+5
	MOVLW      64
	MOVWF      main_sin_L0+6
	MOVLW      128
	MOVWF      main_sin_L0+7
	CLRF       main_a_L0+0
	CLRF       main_a_L0+1
;Apl4.c,1 :: 		void main() {
;Apl4.c,4 :: 		TRISB=0;
	CLRF       TRISB+0
;Apl4.c,10 :: 		for(a;a<8;a++)
L_main0:
	MOVLW      128
	XORWF      main_a_L0+1, 0
	MOVWF      R0+0
	MOVLW      128
	SUBWF      R0+0, 0
	BTFSS      STATUS+0, 2
	GOTO       L__main4
	MOVLW      8
	SUBWF      main_a_L0+0, 0
L__main4:
	BTFSC      STATUS+0, 0
	GOTO       L_main1
;Apl4.c,12 :: 		PORTB=1<<a;
	MOVF       main_a_L0+0, 0
	MOVWF      R1+0
	MOVLW      1
	MOVWF      R0+0
	MOVF       R1+0, 0
L__main5:
	BTFSC      STATUS+0, 2
	GOTO       L__main6
	RLF        R0+0, 1
	BCF        R0+0, 0
	ADDLW      255
	GOTO       L__main5
L__main6:
	MOVF       R0+0, 0
	MOVWF      PORTB+0
;Apl4.c,13 :: 		delay_ms(1000);
	MOVLW      11
	MOVWF      R11+0
	MOVLW      38
	MOVWF      R12+0
	MOVLW      93
	MOVWF      R13+0
L_main3:
	DECFSZ     R13+0, 1
	GOTO       L_main3
	DECFSZ     R12+0, 1
	GOTO       L_main3
	DECFSZ     R11+0, 1
	GOTO       L_main3
	NOP
	NOP
;Apl4.c,10 :: 		for(a;a<8;a++)
	INCF       main_a_L0+0, 1
	BTFSC      STATUS+0, 2
	INCF       main_a_L0+1, 1
;Apl4.c,14 :: 		}
	GOTO       L_main0
L_main1:
;Apl4.c,15 :: 		}
	GOTO       $+0
; end of _main
