
_main:
	MOVLW      191
	MOVWF      main_sir_L0+0
	MOVLW      134
	MOVWF      main_sir_L0+1
	MOVLW      219
	MOVWF      main_sir_L0+2
	MOVLW      207
	MOVWF      main_sir_L0+3
	MOVLW      230
	MOVWF      main_sir_L0+4
	MOVLW      237
	MOVWF      main_sir_L0+5
	MOVLW      253
	MOVWF      main_sir_L0+6
	MOVLW      135
	MOVWF      main_sir_L0+7
	MOVLW      255
	MOVWF      main_sir_L0+8
	MOVLW      239
	MOVWF      main_sir_L0+9
	CLRF       main_a_L0+0
	CLRF       main_a_L0+1
;7seg.c,1 :: 		void main() {
;7seg.c,19 :: 		TRISA=0;
	CLRF       TRISA+0
;7seg.c,20 :: 		TRISB=0;
	CLRF       TRISB+0
;7seg.c,21 :: 		TRISC=0;
	CLRF       TRISC+0
;7seg.c,22 :: 		TRISD=0;
	CLRF       TRISD+0
;7seg.c,23 :: 		while(1){
L_main0:
;7seg.c,25 :: 		PORTA=0b00000111;
	MOVLW      7
	MOVWF      PORTA+0
;7seg.c,26 :: 		PORTB=0b00000100;
	MOVLW      4
	MOVWF      PORTB+0
;7seg.c,27 :: 		for(a=9;a>0;a--)
	MOVLW      9
	MOVWF      main_a_L0+0
	MOVLW      0
	MOVWF      main_a_L0+1
L_main2:
	MOVLW      128
	MOVWF      R0+0
	MOVLW      128
	XORWF      main_a_L0+1, 0
	SUBWF      R0+0, 0
	BTFSS      STATUS+0, 2
	GOTO       L__main6
	MOVF       main_a_L0+0, 0
	SUBLW      0
L__main6:
	BTFSC      STATUS+0, 0
	GOTO       L_main3
;7seg.c,30 :: 		PORTC=sir[a];
	MOVF       main_a_L0+0, 0
	ADDLW      main_sir_L0+0
	MOVWF      R0+0
	MOVF       R0+0, 0
	MOVWF      FSR
	MOVF       INDF+0, 0
	MOVWF      PORTC+0
;7seg.c,31 :: 		PORTD=sir[a];
	MOVF       R0+0, 0
	MOVWF      FSR
	MOVF       INDF+0, 0
	MOVWF      PORTD+0
;7seg.c,32 :: 		delay_ms(100);
	MOVLW      2
	MOVWF      R11+0
	MOVLW      4
	MOVWF      R12+0
	MOVLW      186
	MOVWF      R13+0
L_main5:
	DECFSZ     R13+0, 1
	GOTO       L_main5
	DECFSZ     R12+0, 1
	GOTO       L_main5
	DECFSZ     R11+0, 1
	GOTO       L_main5
	NOP
;7seg.c,27 :: 		for(a=9;a>0;a--)
	MOVLW      1
	SUBWF      main_a_L0+0, 1
	BTFSS      STATUS+0, 0
	DECF       main_a_L0+1, 1
;7seg.c,33 :: 		}
	GOTO       L_main2
L_main3:
;7seg.c,34 :: 		PORTA=0b00000100;
	MOVLW      4
	MOVWF      PORTA+0
;7seg.c,35 :: 		PORTB=0b00000111;
	MOVLW      7
	MOVWF      PORTB+0
;7seg.c,38 :: 		}
	GOTO       L_main0
;7seg.c,40 :: 		}
	GOTO       $+0
; end of _main
