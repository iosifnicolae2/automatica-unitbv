
_main:
;Apl2.c,1 :: 		void main() {
;Apl2.c,2 :: 		TRISB=0;
	CLRF       TRISB+0
;Apl2.c,3 :: 		while(1){
L_main0:
;Apl2.c,4 :: 		PORTB=0b10010001;
	MOVLW      145
	MOVWF      PORTB+0
;Apl2.c,5 :: 		delay_ms(1000);
	MOVLW      11
	MOVWF      R11+0
	MOVLW      38
	MOVWF      R12+0
	MOVLW      93
	MOVWF      R13+0
L_main2:
	DECFSZ     R13+0, 1
	GOTO       L_main2
	DECFSZ     R12+0, 1
	GOTO       L_main2
	DECFSZ     R11+0, 1
	GOTO       L_main2
	NOP
	NOP
;Apl2.c,6 :: 		PORTB=0;
	CLRF       PORTB+0
;Apl2.c,7 :: 		delay_ms(1000);
	MOVLW      11
	MOVWF      R11+0
	MOVLW      38
	MOVWF      R12+0
	MOVLW      93
	MOVWF      R13+0
L_main3:
	DECFSZ     R13+0, 1
	GOTO       L_main3
	DECFSZ     R12+0, 1
	GOTO       L_main3
	DECFSZ     R11+0, 1
	GOTO       L_main3
	NOP
	NOP
;Apl2.c,8 :: 		}
	GOTO       L_main0
;Apl2.c,9 :: 		}
	GOTO       $+0
; end of _main
