
_main:
;Apl1.c,1 :: 		void main() {
;Apl1.c,2 :: 		INTCON.GIE=1;
	BSF        INTCON+0, 7
;Apl1.c,3 :: 		INTCON.INTE=1;
	BSF        INTCON+0, 4
;Apl1.c,4 :: 		TRISD=0;
	CLRF       TRISD+0
;Apl1.c,5 :: 		TRISB=1;
	MOVLW      1
	MOVWF      TRISB+0
;Apl1.c,6 :: 		PORTB=0;
	CLRF       PORTB+0
;Apl1.c,7 :: 		PORTD=0;
	CLRF       PORTD+0
;Apl1.c,8 :: 		while(1)
L_main0:
;Apl1.c,11 :: 		}
	GOTO       L_main0
;Apl1.c,12 :: 		}
	GOTO       $+0
; end of _main

_interrupt:
	MOVWF      R15+0
	SWAPF      STATUS+0, 0
	CLRF       STATUS+0
	MOVWF      ___saveSTATUS+0
	MOVF       PCLATH+0, 0
	MOVWF      ___savePCLATH+0
	CLRF       PCLATH+0
;Apl1.c,13 :: 		void interrupt()
;Apl1.c,15 :: 		if(INTCON.INTF)
	BTFSS      INTCON+0, 1
	GOTO       L_interrupt2
;Apl1.c,17 :: 		PORTD=~PORTD;
	COMF       PORTD+0, 1
;Apl1.c,18 :: 		INTCON.INTF=0;
	BCF        INTCON+0, 1
;Apl1.c,19 :: 		}
L_interrupt2:
;Apl1.c,21 :: 		}
L__interrupt3:
	MOVF       ___savePCLATH+0, 0
	MOVWF      PCLATH+0
	SWAPF      ___saveSTATUS+0, 0
	MOVWF      STATUS+0
	SWAPF      R15+0, 1
	SWAPF      R15+0, 0
	RETFIE
; end of _interrupt
