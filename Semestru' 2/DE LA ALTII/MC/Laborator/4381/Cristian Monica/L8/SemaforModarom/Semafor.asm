
_main:
;Semafor.c,1 :: 		void main() {
;Semafor.c,2 :: 		INTCON.GIE=1;
	BSF        INTCON+0, 7
;Semafor.c,3 :: 		INTCON.INTE=1;
	BSF        INTCON+0, 4
;Semafor.c,4 :: 		TRISD=0;
	CLRF       TRISD+0
;Semafor.c,5 :: 		TRISB=1;
	MOVLW      1
	MOVWF      TRISB+0
;Semafor.c,6 :: 		PORTB=0;
	CLRF       PORTB+0
;Semafor.c,7 :: 		PORTD=0;
	CLRF       PORTD+0
;Semafor.c,8 :: 		while(1)
L_main0:
;Semafor.c,11 :: 		}
	GOTO       L_main0
;Semafor.c,12 :: 		}
	GOTO       $+0
; end of _main

_interrupt:
	MOVWF      R15+0
	SWAPF      STATUS+0, 0
	CLRF       STATUS+0
	MOVWF      ___saveSTATUS+0
	MOVF       PCLATH+0, 0
	MOVWF      ___savePCLATH+0
	CLRF       PCLATH+0
;Semafor.c,13 :: 		void interrupt()
;Semafor.c,16 :: 		}
L__interrupt2:
	MOVF       ___savePCLATH+0, 0
	MOVWF      PCLATH+0
	SWAPF      ___saveSTATUS+0, 0
	MOVWF      STATUS+0
	SWAPF      R15+0, 1
	SWAPF      R15+0, 0
	RETFIE
; end of _interrupt
