
_main:
;test_sem.c,1 :: 		void main() {
;test_sem.c,2 :: 		INTCON.GIE=1;
	BSF        INTCON+0, 7
;test_sem.c,3 :: 		INTCON.INTE=1;
	BSF        INTCON+0, 4
;test_sem.c,4 :: 		TRISD=0;
	CLRF       TRISD+0
;test_sem.c,5 :: 		TRISB=1;
	MOVLW      1
	MOVWF      TRISB+0
;test_sem.c,8 :: 		while(1)
L_main0:
;test_sem.c,10 :: 		PORTD=1;
	MOVLW      1
	MOVWF      PORTD+0
;test_sem.c,11 :: 		}
	GOTO       L_main0
;test_sem.c,12 :: 		}
	GOTO       $+0
; end of _main

_interrupt:
	MOVWF      R15+0
	SWAPF      STATUS+0, 0
	CLRF       STATUS+0
	MOVWF      ___saveSTATUS+0
	MOVF       PCLATH+0, 0
	MOVWF      ___savePCLATH+0
	CLRF       PCLATH+0
;test_sem.c,14 :: 		void interrupt()
;test_sem.c,16 :: 		if(INTCON.INTF)
	BTFSS      INTCON+0, 1
	GOTO       L_interrupt2
;test_sem.c,18 :: 		PORTD=2;
	MOVLW      2
	MOVWF      PORTD+0
;test_sem.c,19 :: 		delay_ms(1000);
	MOVLW      11
	MOVWF      R11+0
	MOVLW      38
	MOVWF      R12+0
	MOVLW      93
	MOVWF      R13+0
L_interrupt3:
	DECFSZ     R13+0, 1
	GOTO       L_interrupt3
	DECFSZ     R12+0, 1
	GOTO       L_interrupt3
	DECFSZ     R11+0, 1
	GOTO       L_interrupt3
	NOP
	NOP
;test_sem.c,20 :: 		PORTD=4;
	MOVLW      4
	MOVWF      PORTD+0
;test_sem.c,22 :: 		INTCON.INTF=0;
	BCF        INTCON+0, 1
;test_sem.c,23 :: 		}
L_interrupt2:
;test_sem.c,25 :: 		}
L__interrupt4:
	MOVF       ___savePCLATH+0, 0
	MOVWF      PCLATH+0
	SWAPF      ___saveSTATUS+0, 0
	MOVWF      STATUS+0
	SWAPF      R15+0, 1
	SWAPF      R15+0, 0
	RETFIE
; end of _interrupt
