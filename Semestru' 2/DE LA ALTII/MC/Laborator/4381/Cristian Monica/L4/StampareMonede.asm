
_main:
;StampareMonede.c,1 :: 		int main()
;StampareMonede.c,8 :: 		TRISD.RD0=1;
	BSF        TRISD+0, 0
;StampareMonede.c,9 :: 		TRISD.RD1=1;
	BSF        TRISD+0, 1
;StampareMonede.c,10 :: 		TRISD.RD2=1 ;
	BSF        TRISD+0, 2
;StampareMonede.c,13 :: 		TRISD.RD3=0;
	BCF        TRISD+0, 3
;StampareMonede.c,14 :: 		TRISD.RD4=0;
	BCF        TRISD+0, 4
;StampareMonede.c,17 :: 		A=1;
	MOVLW      1
	MOVWF      R1+0
;StampareMonede.c,18 :: 		B=0;
	CLRF       R2+0
;StampareMonede.c,19 :: 		C=0;
	CLRF       R3+0
;StampareMonede.c,20 :: 		while(1)
L_main0:
;StampareMonede.c,23 :: 		if ((A==1)&&(PORTD.RD0==1))
	MOVF       R1+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main4
	BTFSS      PORTD+0, 0
	GOTO       L_main4
L__main16:
;StampareMonede.c,24 :: 		{A=0; B=1;}
	CLRF       R1+0
	MOVLW      1
	MOVWF      R2+0
L_main4:
;StampareMonede.c,25 :: 		if ((B==1)&&(PORTD.RD2==1))
	MOVF       R2+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main7
	BTFSS      PORTD+0, 2
	GOTO       L_main7
L__main15:
;StampareMonede.c,26 :: 		{B=0; C=1;}
	CLRF       R2+0
	MOVLW      1
	MOVWF      R3+0
L_main7:
;StampareMonede.c,27 :: 		if ((C==1)&&(PORTD.RD1==1))
	MOVF       R3+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main10
	BTFSS      PORTD+0, 1
	GOTO       L_main10
L__main14:
;StampareMonede.c,28 :: 		{C=0; A=1;}
	CLRF       R3+0
	MOVLW      1
	MOVWF      R1+0
L_main10:
;StampareMonede.c,30 :: 		if (A==1) {PORTD.RD3=0; PORTD.RD4=0;}
	MOVF       R1+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main11
	BCF        PORTD+0, 3
	BCF        PORTD+0, 4
L_main11:
;StampareMonede.c,31 :: 		if (B==1) {PORTD.RD3=1; PORTD.RD4=0;}
	MOVF       R2+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main12
	BSF        PORTD+0, 3
	BCF        PORTD+0, 4
L_main12:
;StampareMonede.c,32 :: 		if (C==1) {PORTD.RD3=0; PORTD.RD4=1;}
	MOVF       R3+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main13
	BCF        PORTD+0, 3
	BSF        PORTD+0, 4
L_main13:
;StampareMonede.c,33 :: 		}
	GOTO       L_main0
;StampareMonede.c,34 :: 		}
	GOTO       $+0
; end of _main
