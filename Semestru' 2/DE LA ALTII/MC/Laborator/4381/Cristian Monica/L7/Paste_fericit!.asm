
_main:
	CLRF       main_a_L0+0
	CLRF       main_a_L0+1
;Paste_fericit!.c,42 :: 		void main(){
;Paste_fericit!.c,45 :: 		TRISD.RD0=1;
	BSF        TRISD+0, 0
;Paste_fericit!.c,46 :: 		TRISD.RD1=1;
	BSF        TRISD+0, 1
;Paste_fericit!.c,48 :: 		Lcd_Init();
	CALL       _Lcd_Init+0
;Paste_fericit!.c,50 :: 		while(1){
L_main0:
;Paste_fericit!.c,53 :: 		if(PORTD==2)    {
	MOVF       PORTD+0, 0
	XORLW      2
	BTFSS      STATUS+0, 2
	GOTO       L_main2
;Paste_fericit!.c,54 :: 		a++;
	INCF       main_a_L0+0, 1
	BTFSC      STATUS+0, 2
	INCF       main_a_L0+1, 1
;Paste_fericit!.c,55 :: 		sprinti(sir,"Am %d piese", a);
	MOVLW      main_sir_L0+0
	MOVWF      FARG_sprinti_wh+0
	MOVLW      ?lstr_1_Paste_fericit!+0
	MOVWF      FARG_sprinti_f+0
	MOVLW      hi_addr(?lstr_1_Paste_fericit!+0
	MOVWF      FARG_sprinti_f+1
	MOVF       main_a_L0+0, 0
	MOVWF      FARG_sprinti_wh+3
	MOVF       main_a_L0+1, 0
	MOVWF      FARG_sprinti_wh+4
	CALL       _sprinti+0
;Paste_fericit!.c,56 :: 		Lcd_Out(1,1,sir);
	MOVLW      1
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      1
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      main_sir_L0+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
;Paste_fericit!.c,57 :: 		while(PORTD==2)
L_main3:
	MOVF       PORTD+0, 0
	XORLW      2
	BTFSS      STATUS+0, 2
	GOTO       L_main4
;Paste_fericit!.c,58 :: 		{}
	GOTO       L_main3
L_main4:
;Paste_fericit!.c,59 :: 		}
L_main2:
;Paste_fericit!.c,62 :: 		}
	GOTO       L_main0
;Paste_fericit!.c,63 :: 		}
	GOTO       $+0
; end of _main
