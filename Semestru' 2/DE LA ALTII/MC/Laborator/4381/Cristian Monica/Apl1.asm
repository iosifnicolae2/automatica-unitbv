
_main:
	CLRF       main_i_L0+0
	CLRF       main_i_L0+1
;Apl1.c,1 :: 		void main() {
;Apl1.c,3 :: 		for (i=0;i<10;i++)
	CLRF       main_i_L0+0
	CLRF       main_i_L0+1
L_main0:
	MOVLW      128
	XORWF      main_i_L0+1, 0
	MOVWF      R0+0
	MOVLW      128
	SUBWF      R0+0, 0
	BTFSS      STATUS+0, 2
	GOTO       L__main3
	MOVLW      10
	SUBWF      main_i_L0+0, 0
L__main3:
	BTFSC      STATUS+0, 0
	GOTO       L_main1
	INCF       main_i_L0+0, 1
	BTFSC      STATUS+0, 2
	INCF       main_i_L0+1, 1
;Apl1.c,5 :: 		}
	GOTO       L_main0
L_main1:
;Apl1.c,7 :: 		}
	GOTO       $+0
; end of _main
