
_interrupt:
	MOVWF      R15+0
	SWAPF      STATUS+0, 0
	CLRF       STATUS+0
	MOVWF      ___saveSTATUS+0
	MOVF       PCLATH+0, 0
	MOVWF      ___savePCLATH+0
	CLRF       PCLATH+0
;Apl1.c,2 :: 		void interrupt()
;Apl1.c,4 :: 		INTCON.GIE=0;
	BCF        INTCON+0, 7
;Apl1.c,5 :: 		if(INTCON.T0IF)
	BTFSS      INTCON+0, 2
	GOTO       L_interrupt0
;Apl1.c,9 :: 		i++;
	INCF       _i+0, 1
	BTFSC      STATUS+0, 2
	INCF       _i+1, 1
;Apl1.c,10 :: 		if(i==99)
	MOVLW      0
	XORWF      _i+1, 0
	BTFSS      STATUS+0, 2
	GOTO       L__interrupt5
	MOVLW      99
	XORWF      _i+0, 0
L__interrupt5:
	BTFSS      STATUS+0, 2
	GOTO       L_interrupt1
;Apl1.c,12 :: 		PORTB=~PORTB;
	COMF       PORTB+0, 1
;Apl1.c,13 :: 		i=0;
	CLRF       _i+0
	CLRF       _i+1
;Apl1.c,14 :: 		}
L_interrupt1:
;Apl1.c,16 :: 		INTCON.T0IF=0;
	BCF        INTCON+0, 2
;Apl1.c,17 :: 		TMR0=99;
	MOVLW      99
	MOVWF      TMR0+0
;Apl1.c,21 :: 		}
L_interrupt0:
;Apl1.c,22 :: 		INTCON.GIE=1;
	BSF        INTCON+0, 7
;Apl1.c,24 :: 		}
L__interrupt4:
	MOVF       ___savePCLATH+0, 0
	MOVWF      PCLATH+0
	SWAPF      ___saveSTATUS+0, 0
	MOVWF      STATUS+0
	SWAPF      R15+0, 1
	SWAPF      R15+0, 0
	RETFIE
; end of _interrupt

_main:
;Apl1.c,25 :: 		void main()
;Apl1.c,27 :: 		TRISB=0;
	CLRF       TRISB+0
;Apl1.c,29 :: 		OPTION_REG=0b00000011;
	MOVLW      3
	MOVWF      OPTION_REG+0
;Apl1.c,30 :: 		INTCON.T0IE=1;
	BSF        INTCON+0, 5
;Apl1.c,32 :: 		INTCON.GIE=1;
	BSF        INTCON+0, 7
;Apl1.c,34 :: 		while(1)
L_main2:
;Apl1.c,36 :: 		}
	GOTO       L_main2
;Apl1.c,37 :: 		}
	GOTO       $+0
; end of _main
