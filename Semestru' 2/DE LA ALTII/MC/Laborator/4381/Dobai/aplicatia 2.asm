
_main:
	MOVLW      1
	MOVWF      main_sir_L0+0
	MOVLW      2
	MOVWF      main_sir_L0+1
	MOVLW      4
	MOVWF      main_sir_L0+2
	MOVLW      8
	MOVWF      main_sir_L0+3
	MOVLW      16
	MOVWF      main_sir_L0+4
	MOVLW      32
	MOVWF      main_sir_L0+5
	MOVLW      64
	MOVWF      main_sir_L0+6
	MOVLW      128
	MOVWF      main_sir_L0+7
;aplicatia 2.c,1 :: 		void main() {
;aplicatia 2.c,4 :: 		TRISB = 0b00000000;
	CLRF       TRISB+0
;aplicatia 2.c,5 :: 		PORTB = 0b11111111;
	MOVLW      255
	MOVWF      PORTB+0
;aplicatia 2.c,7 :: 		TRISD = 0b00000000;
	CLRF       TRISD+0
;aplicatia 2.c,10 :: 		PORTD = 0b01101101;
	MOVLW      109
	MOVWF      PORTD+0
;aplicatia 2.c,12 :: 		while(1) {
L_main0:
;aplicatia 2.c,13 :: 		for (a = 0; a < 8;a++) {
	CLRF       R1+0
	CLRF       R1+1
L_main2:
	MOVLW      128
	XORWF      R1+1, 0
	MOVWF      R0+0
	MOVLW      128
	SUBWF      R0+0, 0
	BTFSS      STATUS+0, 2
	GOTO       L__main6
	MOVLW      8
	SUBWF      R1+0, 0
L__main6:
	BTFSC      STATUS+0, 0
	GOTO       L_main3
;aplicatia 2.c,14 :: 		PORTB = sir[a];
	MOVF       R1+0, 0
	ADDLW      main_sir_L0+0
	MOVWF      FSR
	MOVF       INDF+0, 0
	MOVWF      PORTB+0
;aplicatia 2.c,15 :: 		delay_ms(100);
	MOVLW      2
	MOVWF      R11+0
	MOVLW      4
	MOVWF      R12+0
	MOVLW      186
	MOVWF      R13+0
L_main5:
	DECFSZ     R13+0, 1
	GOTO       L_main5
	DECFSZ     R12+0, 1
	GOTO       L_main5
	DECFSZ     R11+0, 1
	GOTO       L_main5
	NOP
;aplicatia 2.c,13 :: 		for (a = 0; a < 8;a++) {
	INCF       R1+0, 1
	BTFSC      STATUS+0, 2
	INCF       R1+1, 1
;aplicatia 2.c,16 :: 		}
	GOTO       L_main2
L_main3:
;aplicatia 2.c,17 :: 		}
	GOTO       L_main0
;aplicatia 2.c,18 :: 		}
	GOTO       $+0
; end of _main
