
_interrupt:
	MOVWF      R15+0
	SWAPF      STATUS+0, 0
	CLRF       STATUS+0
	MOVWF      ___saveSTATUS+0
	MOVF       PCLATH+0, 0
	MOVWF      ___savePCLATH+0
	CLRF       PCLATH+0
;lab8.c,1 :: 		void interrupt() {
;lab8.c,2 :: 		if (INTCON.INTF) {
	BTFSS      INTCON+0, 1
	GOTO       L_interrupt0
;lab8.c,3 :: 		INTCON.INTF = 0;
	BCF        INTCON+0, 1
;lab8.c,4 :: 		PORTD.RD1 = 0;
	BCF        PORTD+0, 1
;lab8.c,5 :: 		PORTD.RD0 = 0;
	BCF        PORTD+0, 0
;lab8.c,6 :: 		delay_ms(1500);
	MOVLW      16
	MOVWF      R11+0
	MOVLW      57
	MOVWF      R12+0
	MOVLW      13
	MOVWF      R13+0
L_interrupt1:
	DECFSZ     R13+0, 1
	GOTO       L_interrupt1
	DECFSZ     R12+0, 1
	GOTO       L_interrupt1
	DECFSZ     R11+0, 1
	GOTO       L_interrupt1
	NOP
	NOP
;lab8.c,7 :: 		}
L_interrupt0:
;lab8.c,8 :: 		}
L__interrupt24:
	MOVF       ___savePCLATH+0, 0
	MOVWF      PCLATH+0
	SWAPF      ___saveSTATUS+0, 0
	MOVWF      STATUS+0
	SWAPF      R15+0, 1
	SWAPF      R15+0, 0
	RETFIE
; end of _interrupt

_main:
;lab8.c,9 :: 		void main() {
;lab8.c,11 :: 		INTCON = 0b10010000;
	MOVLW      144
	MOVWF      INTCON+0
;lab8.c,12 :: 		TRISD = 0b00011100;
	MOVLW      28
	MOVWF      TRISD+0
;lab8.c,13 :: 		PORTD = 0b00000000;
	CLRF       PORTD+0
;lab8.c,14 :: 		d = 0;
	BCF        main_d_L0+0, BitPos(main_d_L0+0)
;lab8.c,15 :: 		i = 1;
	BSF        main_i_L0+0, BitPos(main_i_L0+0)
;lab8.c,16 :: 		while (1) {
L_main2:
;lab8.c,17 :: 		if ((PORTD.RD4) && (i==1)) {
	BTFSS      PORTD+0, 4
	GOTO       L_main6
	BTFSS      main_i_L0+0, BitPos(main_i_L0+0)
	GOTO       L_main6
L__main23:
;lab8.c,18 :: 		PORTD.RD1 = 1;
	BSF        PORTD+0, 1
;lab8.c,19 :: 		PORTD.RD0 = 1;
	BSF        PORTD+0, 0
;lab8.c,20 :: 		d = 0;
	BCF        main_d_L0+0, BitPos(main_d_L0+0)
;lab8.c,21 :: 		i = 0;
	BCF        main_i_L0+0, BitPos(main_i_L0+0)
;lab8.c,22 :: 		}
L_main6:
;lab8.c,23 :: 		if ((PORTD.RD5) && (d==1)) {
	BTFSS      PORTD+0, 5
	GOTO       L_main9
	BTFSS      main_d_L0+0, BitPos(main_d_L0+0)
	GOTO       L_main9
L__main22:
;lab8.c,24 :: 		PORTD.RD1 = 1;
	BSF        PORTD+0, 1
;lab8.c,25 :: 		PORTD.RD0 = 1;
	BSF        PORTD+0, 0
;lab8.c,26 :: 		d = 0;
	BCF        main_d_L0+0, BitPos(main_d_L0+0)
;lab8.c,27 :: 		i = 0;
	BCF        main_i_L0+0, BitPos(main_i_L0+0)
;lab8.c,28 :: 		}
L_main9:
;lab8.c,29 :: 		if ((PORTD.RD2) && (d == 0) && (i==0)) {
	BTFSS      PORTD+0, 2
	GOTO       L_main12
	BTFSC      main_d_L0+0, BitPos(main_d_L0+0)
	GOTO       L_main12
	BTFSC      main_i_L0+0, BitPos(main_i_L0+0)
	GOTO       L_main12
L__main21:
;lab8.c,30 :: 		PORTD.RD1 = 0;
	BCF        PORTD+0, 1
;lab8.c,31 :: 		PORTD.RD0 = 0;
	BCF        PORTD+0, 0
;lab8.c,32 :: 		d = 1;
	BSF        main_d_L0+0, BitPos(main_d_L0+0)
;lab8.c,33 :: 		i = 0;
	BCF        main_i_L0+0, BitPos(main_i_L0+0)
;lab8.c,34 :: 		}
L_main12:
;lab8.c,35 :: 		if ((PORTD.RD3) && (d == 0) && (i == 0)) {
	BTFSS      PORTD+0, 3
	GOTO       L_main15
	BTFSC      main_d_L0+0, BitPos(main_d_L0+0)
	GOTO       L_main15
	BTFSC      main_i_L0+0, BitPos(main_i_L0+0)
	GOTO       L_main15
L__main20:
;lab8.c,36 :: 		PORTD.RD1 = 0;
	BCF        PORTD+0, 1
;lab8.c,37 :: 		PORTD.RD0 = 0;
	BCF        PORTD+0, 0
;lab8.c,38 :: 		d = 0;
	BCF        main_d_L0+0, BitPos(main_d_L0+0)
;lab8.c,39 :: 		i = 1;
	BSF        main_i_L0+0, BitPos(main_i_L0+0)
;lab8.c,40 :: 		}
L_main15:
;lab8.c,41 :: 		if ((i ==0) && (d == 0)) {
	BTFSC      main_i_L0+0, BitPos(main_i_L0+0)
	GOTO       L_main18
	BTFSC      main_d_L0+0, BitPos(main_d_L0+0)
	GOTO       L_main18
L__main19:
;lab8.c,42 :: 		PORTD.RD1 = 1;
	BSF        PORTD+0, 1
;lab8.c,43 :: 		PORTD.RD0 = 1;
	BSF        PORTD+0, 0
;lab8.c,44 :: 		}
L_main18:
;lab8.c,45 :: 		}
	GOTO       L_main2
;lab8.c,46 :: 		}
	GOTO       $+0
; end of _main
