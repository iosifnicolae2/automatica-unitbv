
_main:
;aplicatia 3.c,1 :: 		void main() {
;aplicatia 3.c,8 :: 		A = 1;
	BSF        main_A_L0+0, BitPos(main_A_L0+0)
;aplicatia 3.c,9 :: 		B = 0;
	BCF        main_B_L0+0, BitPos(main_B_L0+0)
;aplicatia 3.c,10 :: 		C = 0;
	BCF        main_C_L0+0, BitPos(main_C_L0+0)
;aplicatia 3.c,11 :: 		D = 0;
	BCF        main_D_L0+0, BitPos(main_D_L0+0)
;aplicatia 3.c,12 :: 		E = 1;
	BSF        main_E_L0+0, BitPos(main_E_L0+0)
;aplicatia 3.c,14 :: 		TRISD.RD0 = 1;
	BSF        TRISD+0, 0
;aplicatia 3.c,15 :: 		TRISD.RD1 = 1;
	BSF        TRISD+0, 1
;aplicatia 3.c,16 :: 		TRISD.RD2 = 1;
	BSF        TRISD+0, 2
;aplicatia 3.c,17 :: 		TRISD.RD3 = 1;
	BSF        TRISD+0, 3
;aplicatia 3.c,18 :: 		TRISD.RD4 = 1;
	BSF        TRISD+0, 4
;aplicatia 3.c,20 :: 		TRISC.RC0 = 0;
	BCF        TRISC+0, 0
;aplicatia 3.c,21 :: 		TRISC.RC1 = 0;
	BCF        TRISC+0, 1
;aplicatia 3.c,22 :: 		TRISC.RC2 = 0;
	BCF        TRISC+0, 2
;aplicatia 3.c,23 :: 		TRISC.RC3 = 0;
	BCF        TRISC+0, 3
;aplicatia 3.c,24 :: 		TRISC.RC4 = 0;
	BCF        TRISC+0, 4
;aplicatia 3.c,26 :: 		while (1) {
L_main0:
;aplicatia 3.c,27 :: 		if ((A == 1) && (PORTD.RD0 == 1)) { A = 0; B = 1; }
	BTFSS      main_A_L0+0, BitPos(main_A_L0+0)
	GOTO       L_main4
	BTFSS      PORTD+0, 0
	GOTO       L_main4
L__main26:
	BCF        main_A_L0+0, BitPos(main_A_L0+0)
	BSF        main_B_L0+0, BitPos(main_B_L0+0)
L_main4:
;aplicatia 3.c,28 :: 		if ((B == 1) && (PORTD.RD1 == 1)) { B = 0; C = 1; }
	BTFSS      main_B_L0+0, BitPos(main_B_L0+0)
	GOTO       L_main7
	BTFSS      PORTD+0, 1
	GOTO       L_main7
L__main25:
	BCF        main_B_L0+0, BitPos(main_B_L0+0)
	BSF        main_C_L0+0, BitPos(main_C_L0+0)
L_main7:
;aplicatia 3.c,29 :: 		if ((C == 1) && (PORTD.RD2 == 1)) { C = 0; A = 1; }
	BTFSS      main_C_L0+0, BitPos(main_C_L0+0)
	GOTO       L_main10
	BTFSS      PORTD+0, 2
	GOTO       L_main10
L__main24:
	BCF        main_C_L0+0, BitPos(main_C_L0+0)
	BSF        main_A_L0+0, BitPos(main_A_L0+0)
L_main10:
;aplicatia 3.c,31 :: 		if ((D == 1) && (PORTD.RD3 == 1)) { D = 0; E = 1; }
	BTFSS      main_D_L0+0, BitPos(main_D_L0+0)
	GOTO       L_main13
	BTFSS      PORTD+0, 3
	GOTO       L_main13
L__main23:
	BCF        main_D_L0+0, BitPos(main_D_L0+0)
	BSF        main_E_L0+0, BitPos(main_E_L0+0)
L_main13:
;aplicatia 3.c,32 :: 		if ((E == 1) && (PORTD.RD4 == 1)) { E = 0; D = 1; }
	BTFSS      main_E_L0+0, BitPos(main_E_L0+0)
	GOTO       L_main16
	BTFSS      PORTD+0, 4
	GOTO       L_main16
L__main22:
	BCF        main_E_L0+0, BitPos(main_E_L0+0)
	BSF        main_D_L0+0, BitPos(main_D_L0+0)
L_main16:
;aplicatia 3.c,34 :: 		if (A == 1) {
	BTFSS      main_A_L0+0, BitPos(main_A_L0+0)
	GOTO       L_main17
;aplicatia 3.c,35 :: 		PORTC.RC0 = 0;
	BCF        PORTC+0, 0
;aplicatia 3.c,36 :: 		PORTC.RC1 = 0;
	BCF        PORTC+0, 1
;aplicatia 3.c,37 :: 		PORTC.RC2 = 0;
	BCF        PORTC+0, 2
;aplicatia 3.c,38 :: 		PORTC.RC3 = 0;
	BCF        PORTC+0, 3
;aplicatia 3.c,39 :: 		PORTC.RC4 = 0;
	BCF        PORTC+0, 4
;aplicatia 3.c,40 :: 		}
L_main17:
;aplicatia 3.c,41 :: 		if (B == 1) {
	BTFSS      main_B_L0+0, BitPos(main_B_L0+0)
	GOTO       L_main18
;aplicatia 3.c,42 :: 		PORTC.RC0 = 1;
	BSF        PORTC+0, 0
;aplicatia 3.c,43 :: 		PORTC.RC2 = 1;
	BSF        PORTC+0, 2
;aplicatia 3.c,44 :: 		PORTC.RC3 = 1;
	BSF        PORTC+0, 3
;aplicatia 3.c,45 :: 		}
L_main18:
;aplicatia 3.c,46 :: 		if (C == 1) {
	BTFSS      main_C_L0+0, BitPos(main_C_L0+0)
	GOTO       L_main19
;aplicatia 3.c,47 :: 		PORTC.RC0 = 0;
	BCF        PORTC+0, 0
;aplicatia 3.c,48 :: 		PORTC.RC1 = 1;
	BSF        PORTC+0, 1
;aplicatia 3.c,49 :: 		}
L_main19:
;aplicatia 3.c,51 :: 		if (D == 1) {
	BTFSS      main_D_L0+0, BitPos(main_D_L0+0)
	GOTO       L_main20
;aplicatia 3.c,52 :: 		PORTC.RC4 = 1;
	BSF        PORTC+0, 4
;aplicatia 3.c,53 :: 		}
L_main20:
;aplicatia 3.c,54 :: 		if (E == 1) {
	BTFSS      main_E_L0+0, BitPos(main_E_L0+0)
	GOTO       L_main21
;aplicatia 3.c,55 :: 		PORTC.RC4 = 0;
	BCF        PORTC+0, 4
;aplicatia 3.c,56 :: 		}
L_main21:
;aplicatia 3.c,57 :: 		}
	GOTO       L_main0
;aplicatia 3.c,58 :: 		}
	GOTO       $+0
; end of _main
