
_main:
	CLRF       main_a_L0+0
	CLRF       main_a_L0+1
;aplicatia 1.c,1 :: 		void main() {
;aplicatia 1.c,3 :: 		for (a;a<10;a++) {}
L_main0:
	MOVLW      128
	XORWF      main_a_L0+1, 0
	MOVWF      R0+0
	MOVLW      128
	SUBWF      R0+0, 0
	BTFSS      STATUS+0, 2
	GOTO       L__main3
	MOVLW      10
	SUBWF      main_a_L0+0, 0
L__main3:
	BTFSC      STATUS+0, 0
	GOTO       L_main1
	INCF       main_a_L0+0, 1
	BTFSC      STATUS+0, 2
	INCF       main_a_L0+1, 1
	GOTO       L_main0
L_main1:
;aplicatia 1.c,4 :: 		}
	GOTO       $+0
; end of _main
