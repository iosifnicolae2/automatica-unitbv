
_main:
	CLRF       main_s_L0+0
	CLRF       main_s_L0+1
	MOVLW      10
	MOVWF      main_a_L0+0
	MOVLW      0
	MOVWF      main_a_L0+1
	MOVLW      126
	MOVWF      main_sir_L0+0
	MOVLW      48
	MOVWF      main_sir_L0+1
	MOVLW      109
	MOVWF      main_sir_L0+2
	MOVLW      121
	MOVWF      main_sir_L0+3
	MOVLW      51
	MOVWF      main_sir_L0+4
	MOVLW      91
	MOVWF      main_sir_L0+5
	MOVLW      95
	MOVWF      main_sir_L0+6
	MOVLW      112
	MOVWF      main_sir_L0+7
	MOVLW      255
	MOVWF      main_sir_L0+8
	MOVLW      251
	MOVWF      main_sir_L0+9
;7 segmente.c,1 :: 		void main() {
;7 segmente.c,8 :: 		TRISC = 0b00000000;
	CLRF       TRISC+0
;7 segmente.c,9 :: 		PORTC = sir[0];
	MOVF       main_sir_L0+0, 0
	MOVWF      PORTC+0
;7 segmente.c,11 :: 		TRISB = 0b00000000;
	CLRF       TRISB+0
;7 segmente.c,12 :: 		PORTB = 0b00000001;
	MOVLW      1
	MOVWF      PORTB+0
;7 segmente.c,14 :: 		TRISA = 0b00000000;
	CLRF       TRISA+0
;7 segmente.c,15 :: 		PORTA = 0b00100000;
	MOVLW      32
	MOVWF      PORTA+0
;7 segmente.c,17 :: 		while(1) {
L_main0:
;7 segmente.c,18 :: 		PORTC = sir[a];
	MOVF       main_a_L0+0, 0
	ADDLW      main_sir_L0+0
	MOVWF      FSR
	MOVF       INDF+0, 0
	MOVWF      PORTC+0
;7 segmente.c,19 :: 		if (a > 0)
	MOVLW      128
	MOVWF      R0+0
	MOVLW      128
	XORWF      main_a_L0+1, 0
	SUBWF      R0+0, 0
	BTFSS      STATUS+0, 2
	GOTO       L__main4
	MOVF       main_a_L0+0, 0
	SUBLW      0
L__main4:
	BTFSC      STATUS+0, 0
	GOTO       L_main2
;7 segmente.c,20 :: 		--a;
	MOVLW      1
	SUBWF      main_a_L0+0, 1
	BTFSS      STATUS+0, 0
	DECF       main_a_L0+1, 1
L_main2:
;7 segmente.c,22 :: 		delay_ms(500);
	MOVLW      6
	MOVWF      R11+0
	MOVLW      19
	MOVWF      R12+0
	MOVLW      173
	MOVWF      R13+0
L_main3:
	DECFSZ     R13+0, 1
	GOTO       L_main3
	DECFSZ     R12+0, 1
	GOTO       L_main3
	DECFSZ     R11+0, 1
	GOTO       L_main3
	NOP
	NOP
;7 segmente.c,23 :: 		}
	GOTO       L_main0
;7 segmente.c,24 :: 		}
	GOTO       $+0
; end of _main
