#line 1 "D:/studentii/4381/Dobai/lcd.c"
sbit LCD_RS at RB4_bit;
sbit LCD_EN at RB5_bit;
sbit LCD_D7 at RB3_bit;
sbit LCD_D6 at RB2_bit;
sbit LCD_D5 at RB1_bit;
sbit LCD_D4 at RB0_bit;

sbit LCD_RS_Direction at TRISB4_bit;
sbit LCD_EN_Direction at TRISB5_bit;
sbit LCD_D7_Direction at TRISB3_bit;
sbit LCD_D6_Direction at TRISB2_bit;
sbit LCD_D5_Direction at TRISB1_bit;
sbit LCD_D4_Direction at TRISB0_bit;

void main() {
int a = 0;
char txt[12];
int k = 1;

TRISD.RD0 = 0;
TRISD.RD1 = 0;
TRISD.RD2 = 0;

PORTD.RD0 = 0;
PORTD.RD1 = 0;
PORTD.RD2 = 0;

Lcd_Init();
while(1) {
 if (PORTD.RD0 == 1) {
 if (PORTD.RD2 == 0) PORTD.RD2 = 1;
 if ((PORTD.RD1 == 1) && (k == 1)) {
 a++;
 sprinti(txt, "Am %d piese", a);
 Lcd_Out(1,1,txt);
 k = 0;
 }
 if (PORTD.RD1 == 0)
 k = 1;
 if (a == 20) {
 PORTD.RD0 = 0;
 PORTD.RD2 = 0;
 PORTD.RD1 = 0;
 Lcd_Cmd(_LCD_CLEAR);
 a = 0;
 }
 }
}
}
