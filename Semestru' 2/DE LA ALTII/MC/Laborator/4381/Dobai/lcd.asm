
_main:
	MOVLW      1
	MOVWF      main_k_L0+0
	MOVLW      0
	MOVWF      main_k_L0+1
	CLRF       main_a_L0+0
	CLRF       main_a_L0+1
;lcd.c,15 :: 		void main() {
;lcd.c,20 :: 		TRISD.RD0 = 0;
	BCF        TRISD+0, 0
;lcd.c,21 :: 		TRISD.RD1 = 0;
	BCF        TRISD+0, 1
;lcd.c,22 :: 		TRISD.RD2 = 0;
	BCF        TRISD+0, 2
;lcd.c,24 :: 		PORTD.RD0 = 0;
	BCF        PORTD+0, 0
;lcd.c,25 :: 		PORTD.RD1 = 0;
	BCF        PORTD+0, 1
;lcd.c,26 :: 		PORTD.RD2 = 0;
	BCF        PORTD+0, 2
;lcd.c,28 :: 		Lcd_Init();
	CALL       _Lcd_Init+0
;lcd.c,29 :: 		while(1) {
L_main0:
;lcd.c,30 :: 		if (PORTD.RD0 == 1) {
	BTFSS      PORTD+0, 0
	GOTO       L_main2
;lcd.c,31 :: 		if (PORTD.RD2 == 0) PORTD.RD2 = 1;
	BTFSC      PORTD+0, 2
	GOTO       L_main3
	BSF        PORTD+0, 2
L_main3:
;lcd.c,32 :: 		if ((PORTD.RD1 == 1) && (k == 1)) {
	BTFSS      PORTD+0, 1
	GOTO       L_main6
	MOVLW      0
	XORWF      main_k_L0+1, 0
	BTFSS      STATUS+0, 2
	GOTO       L__main10
	MOVLW      1
	XORWF      main_k_L0+0, 0
L__main10:
	BTFSS      STATUS+0, 2
	GOTO       L_main6
L__main9:
;lcd.c,33 :: 		a++;
	INCF       main_a_L0+0, 1
	BTFSC      STATUS+0, 2
	INCF       main_a_L0+1, 1
;lcd.c,34 :: 		sprinti(txt, "Am %d piese", a);
	MOVLW      main_txt_L0+0
	MOVWF      FARG_sprinti_wh+0
	MOVLW      ?lstr_1_lcd+0
	MOVWF      FARG_sprinti_f+0
	MOVLW      hi_addr(?lstr_1_lcd+0
	MOVWF      FARG_sprinti_f+1
	MOVF       main_a_L0+0, 0
	MOVWF      FARG_sprinti_wh+3
	MOVF       main_a_L0+1, 0
	MOVWF      FARG_sprinti_wh+4
	CALL       _sprinti+0
;lcd.c,35 :: 		Lcd_Out(1,1,txt);
	MOVLW      1
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      1
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      main_txt_L0+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
;lcd.c,36 :: 		k = 0;
	CLRF       main_k_L0+0
	CLRF       main_k_L0+1
;lcd.c,37 :: 		}
L_main6:
;lcd.c,38 :: 		if (PORTD.RD1 == 0)
	BTFSC      PORTD+0, 1
	GOTO       L_main7
;lcd.c,39 :: 		k = 1;
	MOVLW      1
	MOVWF      main_k_L0+0
	MOVLW      0
	MOVWF      main_k_L0+1
L_main7:
;lcd.c,40 :: 		if (a == 20) {
	MOVLW      0
	XORWF      main_a_L0+1, 0
	BTFSS      STATUS+0, 2
	GOTO       L__main11
	MOVLW      20
	XORWF      main_a_L0+0, 0
L__main11:
	BTFSS      STATUS+0, 2
	GOTO       L_main8
;lcd.c,41 :: 		PORTD.RD0 = 0;
	BCF        PORTD+0, 0
;lcd.c,42 :: 		PORTD.RD2 = 0;
	BCF        PORTD+0, 2
;lcd.c,43 :: 		PORTD.RD1 = 0;
	BCF        PORTD+0, 1
;lcd.c,44 :: 		Lcd_Cmd(_LCD_CLEAR);
	MOVLW      1
	MOVWF      FARG_Lcd_Cmd_out_char+0
	CALL       _Lcd_Cmd+0
;lcd.c,45 :: 		a = 0;
	CLRF       main_a_L0+0
	CLRF       main_a_L0+1
;lcd.c,46 :: 		}
L_main8:
;lcd.c,47 :: 		}
L_main2:
;lcd.c,48 :: 		}
	GOTO       L_main0
;lcd.c,49 :: 		}
	GOTO       $+0
; end of _main
