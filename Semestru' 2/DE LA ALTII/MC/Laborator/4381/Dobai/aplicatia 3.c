void main() {
     bit A;
     bit B;
     bit C;
     bit D;
     bit E;

     A = 1;
     B = 0;
     C = 0;
     D = 0;
     E = 1;

     TRISD.RD0 = 1;
     TRISD.RD1 = 1;
     TRISD.RD2 = 1;
     TRISD.RD3 = 1;
     TRISD.RD4 = 1;
     
     TRISC.RC0 = 0;
     TRISC.RC1 = 0;
     TRISC.RC2 = 0;
     TRISC.RC3 = 0;
     TRISC.RC4 = 0;
     
     while (1) {
           if ((A == 1) && (PORTD.RD0 == 1)) { A = 0; B = 1; }
           if ((B == 1) && (PORTD.RD1 == 1)) { B = 0; C = 1; }
           if ((C == 1) && (PORTD.RD2 == 1)) { C = 0; A = 1; }
           
           if ((D == 1) && (PORTD.RD3 == 1)) { D = 0; E = 1; }
           if ((E == 1) && (PORTD.RD4 == 1)) { E = 0; D = 1; }
           
           if (A == 1) {
              PORTC.RC0 = 0;
              PORTC.RC1 = 0;
              PORTC.RC2 = 0;
              PORTC.RC3 = 0;
              PORTC.RC4 = 0;
           }
           if (B == 1) {
              PORTC.RC0 = 1;
              PORTC.RC2 = 1;
              PORTC.RC3 = 1;
           }
           if (C == 1) {
              PORTC.RC0 = 0;
              PORTC.RC1 = 1;
           }
           
           if (D == 1) {
              PORTC.RC4 = 1;
           }
           if (E == 1) {
              PORTC.RC4 = 0;
           }
     }
}
