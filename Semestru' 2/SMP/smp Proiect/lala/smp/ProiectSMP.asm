
_main:

;ProiectSMP.c,15 :: 		void main()
;ProiectSMP.c,20 :: 		TRISD=0B000000000;
	CLRF       TRISD+0
;ProiectSMP.c,21 :: 		TRISB=0B000000111;
	MOVLW      7
	MOVWF      TRISB+0
;ProiectSMP.c,22 :: 		LCD_init();
	CALL       _Lcd_Init+0
;ProiectSMP.c,23 :: 		Lcd_Cmd(_LCD_CURSOR_OFF);
	MOVLW      12
	MOVWF      FARG_Lcd_Cmd_out_char+0
	CALL       _Lcd_Cmd+0
;ProiectSMP.c,24 :: 		a=1;b=0;c=0;d=0;
	MOVLW      1
	MOVWF      main_a_L0+0
	CLRF       main_b_L0+0
	CLRF       main_c_L0+0
	CLRF       main_d_L0+0
;ProiectSMP.c,26 :: 		while(1)
L_main0:
;ProiectSMP.c,28 :: 		if((a==1)&&(PORTB.RB0==1))
	MOVF       main_a_L0+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main4
	BTFSS      PORTB+0, 0
	GOTO       L_main4
L__main26:
;ProiectSMP.c,30 :: 		a=0;b=1;c=0;d=0;
	CLRF       main_a_L0+0
	MOVLW      1
	MOVWF      main_b_L0+0
	CLRF       main_c_L0+0
	CLRF       main_d_L0+0
;ProiectSMP.c,31 :: 		while(PORTB.RB0==1)
L_main5:
	BTFSS      PORTB+0, 0
	GOTO       L_main6
;ProiectSMP.c,32 :: 		{ }
	GOTO       L_main5
L_main6:
;ProiectSMP.c,33 :: 		}
L_main4:
;ProiectSMP.c,34 :: 		if(((b==1)||(d==1))&&(PORTB.RB1==1))
	MOVF       main_b_L0+0, 0
	XORLW      1
	BTFSC      STATUS+0, 2
	GOTO       L__main25
	MOVF       main_d_L0+0, 0
	XORLW      1
	BTFSC      STATUS+0, 2
	GOTO       L__main25
	GOTO       L_main11
L__main25:
	BTFSS      PORTB+0, 1
	GOTO       L_main11
L__main24:
;ProiectSMP.c,36 :: 		a=0;b=0;c=1;d=0;
	CLRF       main_a_L0+0
	CLRF       main_b_L0+0
	MOVLW      1
	MOVWF      main_c_L0+0
	CLRF       main_d_L0+0
;ProiectSMP.c,37 :: 		while(PORTB.RB1==1)
L_main12:
	BTFSS      PORTB+0, 1
	GOTO       L_main13
;ProiectSMP.c,38 :: 		{ }
	GOTO       L_main12
L_main13:
;ProiectSMP.c,39 :: 		}
L_main11:
;ProiectSMP.c,40 :: 		if((c==1)&&(PORTB.RB2==1))
	MOVF       main_c_L0+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main16
	BTFSS      PORTB+0, 2
	GOTO       L_main16
L__main23:
;ProiectSMP.c,42 :: 		a=0;b=0;c=0;d=1;
	CLRF       main_a_L0+0
	CLRF       main_b_L0+0
	CLRF       main_c_L0+0
	MOVLW      1
	MOVWF      main_d_L0+0
;ProiectSMP.c,43 :: 		while(PORTB.RB2==1)
L_main17:
	BTFSS      PORTB+0, 2
	GOTO       L_main18
;ProiectSMP.c,44 :: 		{ }
	GOTO       L_main17
L_main18:
;ProiectSMP.c,45 :: 		}
L_main16:
;ProiectSMP.c,48 :: 		if(a==1)
	MOVF       main_a_L0+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main19
;ProiectSMP.c,50 :: 		LCD_out(1,7,"Banca");
	MOVLW      1
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      7
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      ?lstr1_ProiectSMP+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
;ProiectSMP.c,51 :: 		LCD_out(2,3,"Transilvania");
	MOVLW      2
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      3
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      ?lstr2_ProiectSMP+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
;ProiectSMP.c,52 :: 		}
L_main19:
;ProiectSMP.c,53 :: 		if(b==1)
	MOVF       main_b_L0+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main20
;ProiectSMP.c,55 :: 		LCD_out(1,3,"Bine ati Venit");
	MOVLW      1
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      3
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      ?lstr3_ProiectSMP+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
;ProiectSMP.c,56 :: 		LCD_out(2,3,"             ");
	MOVLW      2
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      3
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      ?lstr4_ProiectSMP+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
;ProiectSMP.c,57 :: 		}
L_main20:
;ProiectSMP.c,58 :: 		if(c==1)
	MOVF       main_c_L0+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main21
;ProiectSMP.c,60 :: 		LCD_out(1,3,"  Urmatorul      ");
	MOVLW      1
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      3
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      ?lstr5_ProiectSMP+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
;ProiectSMP.c,61 :: 		LCD_out(2,7,"Client ");
	MOVLW      2
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      7
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      ?lstr6_ProiectSMP+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
;ProiectSMP.c,62 :: 		}
L_main21:
;ProiectSMP.c,63 :: 		if (d==1)
	MOVF       main_d_L0+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main22
;ProiectSMP.c,65 :: 		LCD_out(1,3,"   Momentan      ");
	MOVLW      1
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      3
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      ?lstr7_ProiectSMP+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
;ProiectSMP.c,66 :: 		LCD_out(2,7,"Ocupat ");
	MOVLW      2
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      7
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      ?lstr8_ProiectSMP+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
;ProiectSMP.c,67 :: 		}
L_main22:
;ProiectSMP.c,68 :: 		}
	GOTO       L_main0
;ProiectSMP.c,70 :: 		}
L_end_main:
	GOTO       $+0
; end of _main
