
_interrupt:
	MOVWF      R15+0
	SWAPF      STATUS+0, 0
	CLRF       STATUS+0
	MOVWF      ___saveSTATUS+0
	MOVF       PCLATH+0, 0
	MOVWF      ___savePCLATH+0
	CLRF       PCLATH+0

;apl1.c,3 :: 		void interrupt()
;apl1.c,5 :: 		INTCON.GIE=0;
	BCF        INTCON+0, 7
;apl1.c,6 :: 		if (INTCON.T0IF==1)
	BTFSS      INTCON+0, 2
	GOTO       L_interrupt0
;apl1.c,8 :: 		i=i+1;
	INCF       _i+0, 1
;apl1.c,9 :: 		PORTD=sir[i%6];
	MOVLW      6
	MOVWF      R4+0
	MOVF       _i+0, 0
	MOVWF      R0+0
	CALL       _Div_8x8_S+0
	MOVF       R8+0, 0
	MOVWF      R0+0
	MOVF       R0+0, 0
	ADDLW      _sir+0
	MOVWF      FSR
	MOVF       INDF+0, 0
	MOVWF      PORTD+0
;apl1.c,10 :: 		INTCON.T0IF=0;
	BCF        INTCON+0, 2
;apl1.c,11 :: 		TMR0=110;
	MOVLW      110
	MOVWF      TMR0+0
;apl1.c,12 :: 		}
L_interrupt0:
;apl1.c,13 :: 		INTCON.GIE=1;
	BSF        INTCON+0, 7
;apl1.c,14 :: 		}
L_end_interrupt:
L__interrupt5:
	MOVF       ___savePCLATH+0, 0
	MOVWF      PCLATH+0
	SWAPF      ___saveSTATUS+0, 0
	MOVWF      STATUS+0
	SWAPF      R15+0, 1
	SWAPF      R15+0, 0
	RETFIE
; end of _interrupt

_main:

;apl1.c,15 :: 		void main() {
;apl1.c,16 :: 		INTCON.GIE=1;
	BSF        INTCON+0, 7
;apl1.c,17 :: 		INTCON.T0IE=1;
	BSF        INTCON+0, 5
;apl1.c,18 :: 		INTCON.PEIE=1;
	BSF        INTCON+0, 6
;apl1.c,19 :: 		OPTION_REG=0B00000111;
	MOVLW      7
	MOVWF      OPTION_REG+0
;apl1.c,20 :: 		TMR0=110;
	MOVLW      110
	MOVWF      TMR0+0
;apl1.c,21 :: 		TRISD=0;
	CLRF       TRISD+0
;apl1.c,22 :: 		PORTD=sir[0];
	MOVF       _sir+0, 0
	MOVWF      PORTD+0
;apl1.c,23 :: 		while(1)
L_main1:
;apl1.c,24 :: 		if (i==6) i=i-6;
	MOVF       _i+0, 0
	XORLW      6
	BTFSS      STATUS+0, 2
	GOTO       L_main3
	MOVLW      6
	SUBWF      _i+0, 1
L_main3:
	GOTO       L_main1
;apl1.c,25 :: 		}
L_end_main:
	GOTO       $+0
; end of _main
