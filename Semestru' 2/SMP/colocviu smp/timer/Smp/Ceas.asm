
_interrupt:
	MOVWF      R15+0
	SWAPF      STATUS+0, 0
	CLRF       STATUS+0
	MOVWF      ___saveSTATUS+0
	MOVF       PCLATH+0, 0
	MOVWF      ___savePCLATH+0
	CLRF       PCLATH+0

;Ceas.c,4 :: 		void interrupt ()
;Ceas.c,6 :: 		INTCON.GIE=0;
	BCF        INTCON+0, 7
;Ceas.c,7 :: 		if(INTCON.T0IF==1)
	BTFSS      INTCON+0, 2
	GOTO       L_interrupt0
;Ceas.c,8 :: 		{ i++;
	INCF       _i+0, 1
	BTFSC      STATUS+0, 2
	INCF       _i+1, 1
;Ceas.c,9 :: 		PORTD=sir[i];
	MOVF       _i+0, 0
	ADDLW      _sir+0
	MOVWF      FSR
	MOVF       INDF+0, 0
	MOVWF      PORTD+0
;Ceas.c,10 :: 		delay_ms(100);
	MOVLW      2
	MOVWF      R11+0
	MOVLW      4
	MOVWF      R12+0
	MOVLW      186
	MOVWF      R13+0
L_interrupt1:
	DECFSZ     R13+0, 1
	GOTO       L_interrupt1
	DECFSZ     R12+0, 1
	GOTO       L_interrupt1
	DECFSZ     R11+0, 1
	GOTO       L_interrupt1
	NOP
;Ceas.c,11 :: 		INTCON.T0IF=0;
	BCF        INTCON+0, 2
;Ceas.c,12 :: 		TMR0=110;
	MOVLW      110
	MOVWF      TMR0+0
;Ceas.c,13 :: 		}
L_interrupt0:
;Ceas.c,14 :: 		INTCON.GIE=1;
	BSF        INTCON+0, 7
;Ceas.c,16 :: 		}
L_end_interrupt:
L__interrupt7:
	MOVF       ___savePCLATH+0, 0
	MOVWF      PCLATH+0
	SWAPF      ___saveSTATUS+0, 0
	MOVWF      STATUS+0
	SWAPF      R15+0, 1
	SWAPF      R15+0, 0
	RETFIE
; end of _interrupt

_main:

;Ceas.c,17 :: 		void main() {
;Ceas.c,18 :: 		TRISD=0;
	CLRF       TRISD+0
;Ceas.c,19 :: 		TRISB.RB7=1;
	BSF        TRISB+0, 7
;Ceas.c,20 :: 		INTCON.GIE=1;
	BSF        INTCON+0, 7
;Ceas.c,21 :: 		INTCON.T0IE=1;
	BSF        INTCON+0, 5
;Ceas.c,22 :: 		INTCON.PEIE=1;
	BSF        INTCON+0, 6
;Ceas.c,23 :: 		OPTION_REG=0b00000111;
	MOVLW      7
	MOVWF      OPTION_REG+0
;Ceas.c,24 :: 		TMR0=110;
	MOVLW      110
	MOVWF      TMR0+0
;Ceas.c,25 :: 		PORTD=sir[0];
	MOVF       _sir+0, 0
	MOVWF      PORTD+0
;Ceas.c,26 :: 		Delay_ms(1000);
	MOVLW      11
	MOVWF      R11+0
	MOVLW      38
	MOVWF      R12+0
	MOVLW      93
	MOVWF      R13+0
L_main2:
	DECFSZ     R13+0, 1
	GOTO       L_main2
	DECFSZ     R12+0, 1
	GOTO       L_main2
	DECFSZ     R11+0, 1
	GOTO       L_main2
	NOP
	NOP
;Ceas.c,27 :: 		while(1)
L_main3:
;Ceas.c,28 :: 		{}
	GOTO       L_main3
;Ceas.c,34 :: 		}
L_end_main:
	GOTO       $+0
; end of _main
