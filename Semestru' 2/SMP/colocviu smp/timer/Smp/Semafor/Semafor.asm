
_interrupt:
	MOVWF      R15+0
	SWAPF      STATUS+0, 0
	CLRF       STATUS+0
	MOVWF      ___saveSTATUS+0
	MOVF       PCLATH+0, 0
	MOVWF      ___savePCLATH+0
	CLRF       PCLATH+0

;Semafor.c,4 :: 		void interrupt ()
;Semafor.c,6 :: 		INTCON.GIE=0;
	BCF        INTCON+0, 7
;Semafor.c,7 :: 		if(INTCON.INTF==1)
	BTFSS      INTCON+0, 1
	GOTO       L_interrupt0
;Semafor.c,8 :: 		{  TRISB.RB7=1;
	BSF        TRISB+0, 7
;Semafor.c,9 :: 		INTCON.INTF=0;
	BCF        INTCON+0, 1
;Semafor.c,10 :: 		}
L_interrupt0:
;Semafor.c,12 :: 		}
L_end_interrupt:
L__interrupt14:
	MOVF       ___savePCLATH+0, 0
	MOVWF      PCLATH+0
	SWAPF      ___saveSTATUS+0, 0
	MOVWF      STATUS+0
	SWAPF      R15+0, 1
	SWAPF      R15+0, 0
	RETFIE
; end of _interrupt

_main:

;Semafor.c,15 :: 		void main()
;Semafor.c,17 :: 		INTCON.GIE=1;
	BSF        INTCON+0, 7
;Semafor.c,18 :: 		INTCON.INTE=1;
	BSF        INTCON+0, 4
;Semafor.c,19 :: 		TRISB.RB7=1;
	BSF        TRISB+0, 7
;Semafor.c,20 :: 		TRISC=0;
	CLRF       TRISC+0
;Semafor.c,21 :: 		TRISA=0;
	CLRF       TRISA+0
;Semafor.c,22 :: 		TRISD=0;
	CLRF       TRISD+0
;Semafor.c,23 :: 		PORTC=0;
	CLRF       PORTC+0
;Semafor.c,24 :: 		PORTD=0;
	CLRF       PORTD+0
;Semafor.c,25 :: 		PORTA=0;
	CLRF       PORTA+0
;Semafor.c,26 :: 		while(1)
L_main1:
;Semafor.c,27 :: 		{  PORTD.RD1=0;
	BCF        PORTD+0, 1
;Semafor.c,28 :: 		PORTD.RD2=1;
	BSF        PORTD+0, 2
;Semafor.c,29 :: 		PORTA.RA4=1;
	BSF        PORTA+0, 4
;Semafor.c,30 :: 		delay_ms(100);
	MOVLW      2
	MOVWF      R11+0
	MOVLW      4
	MOVWF      R12+0
	MOVLW      186
	MOVWF      R13+0
L_main3:
	DECFSZ     R13+0, 1
	GOTO       L_main3
	DECFSZ     R12+0, 1
	GOTO       L_main3
	DECFSZ     R11+0, 1
	GOTO       L_main3
	NOP
;Semafor.c,31 :: 		if(PORTB.RB7==1)
	BTFSS      PORTB+0, 7
	GOTO       L_main4
;Semafor.c,32 :: 		{PORTD.RD2=0;
	BCF        PORTD+0, 2
;Semafor.c,33 :: 		PORTA.RD1=1;
	BSF        PORTA+0, 1
;Semafor.c,34 :: 		delay_ms(100);
	MOVLW      2
	MOVWF      R11+0
	MOVLW      4
	MOVWF      R12+0
	MOVLW      186
	MOVWF      R13+0
L_main5:
	DECFSZ     R13+0, 1
	GOTO       L_main5
	DECFSZ     R12+0, 1
	GOTO       L_main5
	DECFSZ     R11+0, 1
	GOTO       L_main5
	NOP
;Semafor.c,35 :: 		PORTA.RA3=1;
	BSF        PORTA+0, 3
;Semafor.c,36 :: 		PORTD.RD0=1;
	BSF        PORTD+0, 0
;Semafor.c,37 :: 		delay_ms(100);
	MOVLW      2
	MOVWF      R11+0
	MOVLW      4
	MOVWF      R12+0
	MOVLW      186
	MOVWF      R13+0
L_main6:
	DECFSZ     R13+0, 1
	GOTO       L_main6
	DECFSZ     R12+0, 1
	GOTO       L_main6
	DECFSZ     R11+0, 1
	GOTO       L_main6
	NOP
;Semafor.c,38 :: 		PORTA.RA4=0;
	BCF        PORTA+0, 4
;Semafor.c,39 :: 		PORTA.RA2=1;
	BSF        PORTA+0, 2
;Semafor.c,41 :: 		for(i=10;i>0;i--)
	MOVLW      10
	MOVWF      _i+0
	MOVLW      0
	MOVWF      _i+1
L_main7:
	MOVLW      128
	MOVWF      R0+0
	MOVLW      128
	XORWF      _i+1, 0
	SUBWF      R0+0, 0
	BTFSS      STATUS+0, 2
	GOTO       L__main16
	MOVF       _i+0, 0
	SUBLW      0
L__main16:
	BTFSC      STATUS+0, 0
	GOTO       L_main8
;Semafor.c,43 :: 		PORTC=sir[i];
	MOVF       _i+0, 0
	ADDLW      _sir+0
	MOVWF      FSR
	MOVF       INDF+0, 0
	MOVWF      PORTC+0
;Semafor.c,44 :: 		delay_ms(100);
	MOVLW      2
	MOVWF      R11+0
	MOVLW      4
	MOVWF      R12+0
	MOVLW      186
	MOVWF      R13+0
L_main10:
	DECFSZ     R13+0, 1
	GOTO       L_main10
	DECFSZ     R12+0, 1
	GOTO       L_main10
	DECFSZ     R11+0, 1
	GOTO       L_main10
	NOP
;Semafor.c,41 :: 		for(i=10;i>0;i--)
	MOVLW      1
	SUBWF      _i+0, 1
	BTFSS      STATUS+0, 0
	DECF       _i+1, 1
;Semafor.c,45 :: 		}
	GOTO       L_main7
L_main8:
;Semafor.c,47 :: 		PORTC=0;
	CLRF       PORTC+0
;Semafor.c,48 :: 		i=10;
	MOVLW      10
	MOVWF      _i+0
	MOVLW      0
	MOVWF      _i+1
;Semafor.c,49 :: 		PORTD.RD0=0;
	BCF        PORTD+0, 0
;Semafor.c,50 :: 		PORTA.RA2=0;
	BCF        PORTA+0, 2
;Semafor.c,51 :: 		PORTA.RA3=1;
	BSF        PORTA+0, 3
;Semafor.c,52 :: 		delay_ms(100);
	MOVLW      2
	MOVWF      R11+0
	MOVLW      4
	MOVWF      R12+0
	MOVLW      186
	MOVWF      R13+0
L_main11:
	DECFSZ     R13+0, 1
	GOTO       L_main11
	DECFSZ     R12+0, 1
	GOTO       L_main11
	DECFSZ     R11+0, 1
	GOTO       L_main11
	NOP
;Semafor.c,53 :: 		PORTD.RD1=1;
	BSF        PORTD+0, 1
;Semafor.c,54 :: 		DELAY_MS(100);
	MOVLW      2
	MOVWF      R11+0
	MOVLW      4
	MOVWF      R12+0
	MOVLW      186
	MOVWF      R13+0
L_main12:
	DECFSZ     R13+0, 1
	GOTO       L_main12
	DECFSZ     R12+0, 1
	GOTO       L_main12
	DECFSZ     R11+0, 1
	GOTO       L_main12
	NOP
;Semafor.c,55 :: 		PORTA.RA4=1;
	BSF        PORTA+0, 4
;Semafor.c,56 :: 		PORTD.RD2=1;
	BSF        PORTD+0, 2
;Semafor.c,58 :: 		}
L_main4:
;Semafor.c,59 :: 		}
	GOTO       L_main1
;Semafor.c,60 :: 		}
L_end_main:
	GOTO       $+0
; end of _main
