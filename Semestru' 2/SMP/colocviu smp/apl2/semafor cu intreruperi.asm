
_interrupt:
	MOVWF      R15+0
	SWAPF      STATUS+0, 0
	CLRF       STATUS+0
	MOVWF      ___saveSTATUS+0
	MOVF       PCLATH+0, 0
	MOVWF      ___savePCLATH+0
	CLRF       PCLATH+0

;semafor cu intreruperi.c,3 :: 		void interrupt()
;semafor cu intreruperi.c,5 :: 		INTCON.GIE=0;
	BCF        INTCON+0, 7
;semafor cu intreruperi.c,6 :: 		if (INTCON.INTF==1)
	BTFSS      INTCON+0, 1
	GOTO       L_interrupt0
;semafor cu intreruperi.c,7 :: 		{bp=1;
	MOVLW      1
	MOVWF      _bp+0
	MOVLW      0
	MOVWF      _bp+1
;semafor cu intreruperi.c,8 :: 		INTCON.INTF=0;
	BCF        INTCON+0, 1
;semafor cu intreruperi.c,9 :: 		}
L_interrupt0:
;semafor cu intreruperi.c,10 :: 		}
L__interrupt12:
	MOVF       ___savePCLATH+0, 0
	MOVWF      PCLATH+0
	SWAPF      ___saveSTATUS+0, 0
	MOVWF      STATUS+0
	SWAPF      R15+0, 1
	SWAPF      R15+0, 0
	RETFIE
; end of _interrupt

_main:

;semafor cu intreruperi.c,11 :: 		void main() {
;semafor cu intreruperi.c,12 :: 		int i=10;
	MOVLW      10
	MOVWF      main_i_L0+0
	MOVLW      0
	MOVWF      main_i_L0+1
;semafor cu intreruperi.c,13 :: 		INTCON.GIE=1;
	BSF        INTCON+0, 7
;semafor cu intreruperi.c,14 :: 		INTCON.INTE=1;
	BSF        INTCON+0, 4
;semafor cu intreruperi.c,15 :: 		TRISD=0B00000000;
	CLRF       TRISD+0
;semafor cu intreruperi.c,16 :: 		PORTD=0B00000000;
	CLRF       PORTD+0
;semafor cu intreruperi.c,17 :: 		TRISC=0;
	CLRF       TRISC+0
;semafor cu intreruperi.c,18 :: 		TRISB.RA0=1;
	BSF        TRISB+0, 0
;semafor cu intreruperi.c,19 :: 		PORTC=0;
	CLRF       PORTC+0
;semafor cu intreruperi.c,20 :: 		while (1)
L_main1:
;semafor cu intreruperi.c,22 :: 		PORTD.RA0=1;
	BSF        PORTD+0, 0
;semafor cu intreruperi.c,23 :: 		PORTD.RA5=1;
	BSF        PORTD+0, 5
;semafor cu intreruperi.c,24 :: 		while (bp==0);
L_main3:
	MOVLW      0
	XORWF      _bp+1, 0
	BTFSS      STATUS+0, 2
	GOTO       L__main13
	MOVLW      0
	XORWF      _bp+0, 0
L__main13:
	BTFSS      STATUS+0, 2
	GOTO       L_main4
	GOTO       L_main3
L_main4:
;semafor cu intreruperi.c,25 :: 		PORTD.RA5=0;
	BCF        PORTD+0, 5
;semafor cu intreruperi.c,26 :: 		PORTD.RA4=1;
	BSF        PORTD+0, 4
;semafor cu intreruperi.c,27 :: 		delay_ms(100);
	MOVLW      2
	MOVWF      R11+0
	MOVLW      4
	MOVWF      R12+0
	MOVLW      186
	MOVWF      R13+0
L_main5:
	DECFSZ     R13+0, 1
	GOTO       L_main5
	DECFSZ     R12+0, 1
	GOTO       L_main5
	DECFSZ     R11+0, 1
	GOTO       L_main5
	NOP
;semafor cu intreruperi.c,28 :: 		PORTD.RA4=0;
	BCF        PORTD+0, 4
;semafor cu intreruperi.c,29 :: 		PORTD.RA3=1;
	BSF        PORTD+0, 3
;semafor cu intreruperi.c,30 :: 		PORTD.RA0=0;
	BCF        PORTD+0, 0
;semafor cu intreruperi.c,31 :: 		PORTD.RA2=1;
	BSF        PORTD+0, 2
;semafor cu intreruperi.c,32 :: 		for(i=10;i>=0;i--)
	MOVLW      10
	MOVWF      main_i_L0+0
	MOVLW      0
	MOVWF      main_i_L0+1
L_main6:
	MOVLW      128
	XORWF      main_i_L0+1, 0
	MOVWF      R0+0
	MOVLW      128
	SUBWF      R0+0, 0
	BTFSS      STATUS+0, 2
	GOTO       L__main14
	MOVLW      0
	SUBWF      main_i_L0+0, 0
L__main14:
	BTFSS      STATUS+0, 0
	GOTO       L_main7
;semafor cu intreruperi.c,34 :: 		PORTC=sir[i];
	MOVF       main_i_L0+0, 0
	ADDLW      _sir+0
	MOVWF      FSR
	MOVF       INDF+0, 0
	MOVWF      PORTC+0
;semafor cu intreruperi.c,35 :: 		delay_ms(100);
	MOVLW      2
	MOVWF      R11+0
	MOVLW      4
	MOVWF      R12+0
	MOVLW      186
	MOVWF      R13+0
L_main9:
	DECFSZ     R13+0, 1
	GOTO       L_main9
	DECFSZ     R12+0, 1
	GOTO       L_main9
	DECFSZ     R11+0, 1
	GOTO       L_main9
	NOP
;semafor cu intreruperi.c,32 :: 		for(i=10;i>=0;i--)
	MOVLW      1
	SUBWF      main_i_L0+0, 1
	BTFSS      STATUS+0, 0
	DECF       main_i_L0+1, 1
;semafor cu intreruperi.c,36 :: 		};
	GOTO       L_main6
L_main7:
;semafor cu intreruperi.c,37 :: 		PORTC=0;
	CLRF       PORTC+0
;semafor cu intreruperi.c,38 :: 		i=10;
	MOVLW      10
	MOVWF      main_i_L0+0
	MOVLW      0
	MOVWF      main_i_L0+1
;semafor cu intreruperi.c,39 :: 		PORTD.RA2=0;
	BCF        PORTD+0, 2
;semafor cu intreruperi.c,40 :: 		PORTD.RA1=1;
	BSF        PORTD+0, 1
;semafor cu intreruperi.c,41 :: 		delay_ms(100);
	MOVLW      2
	MOVWF      R11+0
	MOVLW      4
	MOVWF      R12+0
	MOVLW      186
	MOVWF      R13+0
L_main10:
	DECFSZ     R13+0, 1
	GOTO       L_main10
	DECFSZ     R12+0, 1
	GOTO       L_main10
	DECFSZ     R11+0, 1
	GOTO       L_main10
	NOP
;semafor cu intreruperi.c,42 :: 		PORTD.RA1=0;
	BCF        PORTD+0, 1
;semafor cu intreruperi.c,43 :: 		PORTD.RA0=1;
	BSF        PORTD+0, 0
;semafor cu intreruperi.c,44 :: 		PORTD.RA3=0;
	BCF        PORTD+0, 3
;semafor cu intreruperi.c,45 :: 		PORTD.RA5=1;
	BSF        PORTD+0, 5
;semafor cu intreruperi.c,46 :: 		delay_ms(400);
	MOVLW      5
	MOVWF      R11+0
	MOVLW      15
	MOVWF      R12+0
	MOVLW      241
	MOVWF      R13+0
L_main11:
	DECFSZ     R13+0, 1
	GOTO       L_main11
	DECFSZ     R12+0, 1
	GOTO       L_main11
	DECFSZ     R11+0, 1
	GOTO       L_main11
;semafor cu intreruperi.c,47 :: 		}
	GOTO       L_main1
;semafor cu intreruperi.c,48 :: 		}
	GOTO       $+0
; end of _main
