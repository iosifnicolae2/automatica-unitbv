
_main:

;colocviu.c,5 :: 		void main()
;colocviu.c,7 :: 		TRISB=1;
	MOVLW      1
	MOVWF      TRISB+0
;colocviu.c,8 :: 		TRISD=0;
	CLRF       TRISD+0
;colocviu.c,10 :: 		BP=0;
	CLRF       _BP+0
;colocviu.c,11 :: 		A=0;
	CLRF       _A+0
;colocviu.c,12 :: 		B=0;
	CLRF       _B+0
;colocviu.c,13 :: 		E=0;
	CLRF       _E+0
;colocviu.c,39 :: 		while(1)
L_main0:
;colocviu.c,41 :: 		if(BP==0)
	MOVF       _BP+0, 0
	XORLW      0
	BTFSS      STATUS+0, 2
	GOTO       L_main2
;colocviu.c,42 :: 		{  BP=1; A=0; B=0;E=0;
	MOVLW      1
	MOVWF      _BP+0
	CLRF       _A+0
	CLRF       _B+0
	CLRF       _E+0
;colocviu.c,43 :: 		}
L_main2:
;colocviu.c,44 :: 		if(BP==1)
	MOVF       _BP+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main3
;colocviu.c,45 :: 		{ BP=1;A=1;B=0;E=0;
	MOVLW      1
	MOVWF      _BP+0
	MOVLW      1
	MOVWF      _A+0
	CLRF       _B+0
	CLRF       _E+0
;colocviu.c,46 :: 		PORTD.RD1=1;
	BSF        PORTD+0, 1
;colocviu.c,47 :: 		}
L_main3:
;colocviu.c,48 :: 		if((BP==1)&&(A==1))
	MOVF       _BP+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main6
	MOVF       _A+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main6
L__main15:
;colocviu.c,49 :: 		{ BP=1; A=1;B=1;E=0;
	MOVLW      1
	MOVWF      _BP+0
	MOVLW      1
	MOVWF      _A+0
	MOVLW      1
	MOVWF      _B+0
	CLRF       _E+0
;colocviu.c,50 :: 		PORTD.RD2=1;
	BSF        PORTD+0, 2
;colocviu.c,51 :: 		PORTD.RD1=0;
	BCF        PORTD+0, 1
;colocviu.c,52 :: 		}
L_main6:
;colocviu.c,53 :: 		if((BP==1)&&(B==1))
	MOVF       _BP+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main9
	MOVF       _B+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main9
L__main14:
;colocviu.c,54 :: 		{BP=1; A=1;B=1;E=1;
	MOVLW      1
	MOVWF      _BP+0
	MOVLW      1
	MOVWF      _A+0
	MOVLW      1
	MOVWF      _B+0
	MOVLW      1
	MOVWF      _E+0
;colocviu.c,55 :: 		PORTD.RD0=1;
	BSF        PORTD+0, 0
;colocviu.c,56 :: 		PORTD.RD2=0;
	BCF        PORTD+0, 2
;colocviu.c,57 :: 		}
L_main9:
;colocviu.c,58 :: 		if((BP==1)&&(E==1))
	MOVF       _BP+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main12
	MOVF       _E+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main12
L__main13:
;colocviu.c,59 :: 		{BP=0; A=0;B=0;E=0;
	CLRF       _BP+0
	CLRF       _A+0
	CLRF       _B+0
	CLRF       _E+0
;colocviu.c,60 :: 		PORTD.RD0=0;
	BCF        PORTD+0, 0
;colocviu.c,61 :: 		PORTD.RD1=0;
	BCF        PORTD+0, 1
;colocviu.c,62 :: 		PORTD.RD2=0;
	BCF        PORTD+0, 2
;colocviu.c,63 :: 		}
L_main12:
;colocviu.c,64 :: 		}
	GOTO       L_main0
;colocviu.c,65 :: 		}
L_end_main:
	GOTO       $+0
; end of _main
