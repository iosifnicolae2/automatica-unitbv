
_initializare:

;design.c,17 :: 		void initializare()
;design.c,19 :: 		ANSEL=0b00000001;           // ultimul bit este aferent lui AN0 asa ca il setez 1
	MOVLW      1
	MOVWF      ANSEL+0
;design.c,21 :: 		ADCON0.ADFM=1;              // bitul ADFM aliniaza rezultatul conversiei, 1 inseamna la dreapta
	BSF        ADCON0+0, 7
;design.c,24 :: 		ADCON0.ADON=1;              // Pornesc ADC-ul ca periferic
	BSF        ADCON0+0, 0
;design.c,25 :: 		ADCON0.VCFG1=0;             // Setez tensiunea de sursa la tensiunea interna a uC (5V)
	BCF        ADCON0+0, 6
;design.c,26 :: 		ADCON0.VCFG0=0;             // Setez tensiunea de drena la tensiunea interna a uC (GND)
	BCF        ADCON0+0, 5
;design.c,27 :: 		ADCON0.CHS0=0;              // Folosesc 3 biti pentru selectarea canalului
	BCF        ADCON0+0, 2
;design.c,28 :: 		ADCON0.CHS1=0;              // Combinatia 0-0-0 in CHS0-CHS1-CHS2 imi va da AN0 (din datasheet)
	BCF        ADCON0+0, 3
;design.c,29 :: 		ADCON0.CHS2=0;
	BCF        ADCON0+0, 4
;design.c,30 :: 		ADCON1.ADCS0=0;             // Folosesc 3 biti pentru selectarea clockului ce va if utilizat de ADC
	BCF        ADCON1+0, 4
;design.c,31 :: 		ADCON1.ADCS1=0;             // Combinatia 0-0-1 in ADCS0-ADCS1-ADCS2 imi va da FOSC/4 (din datasheet)
	BCF        ADCON1+0, 5
;design.c,32 :: 		ADCON1.ADCS2=1;             // FOSC/4 = Frecventa ceasului uC / 4
	BSF        ADCON1+0, 6
;design.c,33 :: 		}
	RETURN
; end of _initializare

_citire:

;design.c,35 :: 		unsigned int citire()
;design.c,37 :: 		unsigned int var=0;
;design.c,38 :: 		ADCON0.GO_DONE=1;           // Pornesc conversia
	BSF        ADCON0+0, 1
;design.c,39 :: 		while (ADCON0.GO_DONE)      // Astept pana la terminarea conversiei
L_citire0:
	BTFSS      ADCON0+0, 1
	GOTO       L_citire1
;design.c,40 :: 		{}
	GOTO       L_citire0
L_citire1:
;design.c,41 :: 		var=ADRESH << 8 | ADRESL;
	MOVF       ADRESH+0, 0
	MOVWF      R0+1
	CLRF       R0+0
	MOVF       ADRESL+0, 0
	IORWF      R0+0, 1
	MOVLW      0
	IORWF      R0+1, 1
;design.c,51 :: 		return var;
;design.c,52 :: 		}
	RETURN
; end of _citire

_main:

;design.c,55 :: 		void main() {
;design.c,56 :: 		int t=0; //tensiune
	CLRF       main_t_L0+0
	CLRF       main_t_L0+1
;design.c,58 :: 		unsigned int valoare=0;
;design.c,59 :: 		float pas=0.00488;
	MOVLW      104
	MOVWF      main_pas_L0+0
	MOVLW      232
	MOVWF      main_pas_L0+1
	MOVLW      31
	MOVWF      main_pas_L0+2
	MOVLW      119
	MOVWF      main_pas_L0+3
;design.c,60 :: 		int nr=0;
;design.c,62 :: 		LCD_init();                   // Initializez LCD
	CALL       _Lcd_Init+0
;design.c,63 :: 		TRISB=0;                      // PORTUL B va avea doar iesiri (0) pentru ca se va comanda LCD-ul
	CLRF       TRISB+0
;design.c,64 :: 		TRISA.RA0=1;                  // Pinul RA0 este AN0, 1 pentru intrare
	BSF        TRISA+0, 0
;design.c,65 :: 		initializare();               // Initializez ADC-ul
	CALL       _initializare+0
;design.c,66 :: 		while (1)
L_main2:
;design.c,68 :: 		valoare=citire();            // Citesc de la ADC
	CALL       _citire+0
;design.c,69 :: 		nr=valoare*pas*1000;         // Aflu valoarea tensiunii inmultita cu 1000
	CALL       _Word2Double+0
	MOVF       main_pas_L0+0, 0
	MOVWF      R4+0
	MOVF       main_pas_L0+1, 0
	MOVWF      R4+1
	MOVF       main_pas_L0+2, 0
	MOVWF      R4+2
	MOVF       main_pas_L0+3, 0
	MOVWF      R4+3
	CALL       _Mul_32x32_FP+0
	MOVLW      0
	MOVWF      R4+0
	MOVLW      0
	MOVWF      R4+1
	MOVLW      122
	MOVWF      R4+2
	MOVLW      136
	MOVWF      R4+3
	CALL       _Mul_32x32_FP+0
	CALL       _Double2Int+0
;design.c,70 :: 		t=nr;
	MOVF       R0+0, 0
	MOVWF      main_t_L0+0
	MOVF       R0+1, 0
	MOVWF      main_t_L0+1
;design.c,71 :: 		sir[0]='0'+t/1000;           // Scot prima cifra a numarului de maxim 4 cifre
	MOVLW      232
	MOVWF      R4+0
	MOVLW      3
	MOVWF      R4+1
	CALL       _Div_16x16_S+0
	MOVLW      48
	ADDWF      R0+0, 1
	MOVF       R0+0, 0
	MOVWF      main_sir_L0+0
;design.c,72 :: 		sir[1]=',';                  // Pun virgula :D
	MOVLW      44
	MOVWF      main_sir_L0+1
;design.c,73 :: 		sir[2]='0'+((t/100)%10);     // A 2-a cifra
	MOVLW      100
	MOVWF      R4+0
	MOVLW      0
	MOVWF      R4+1
	MOVF       main_t_L0+0, 0
	MOVWF      R0+0
	MOVF       main_t_L0+1, 0
	MOVWF      R0+1
	CALL       _Div_16x16_S+0
	MOVLW      10
	MOVWF      R4+0
	MOVLW      0
	MOVWF      R4+1
	CALL       _Div_16x16_S+0
	MOVF       R8+0, 0
	MOVWF      R0+0
	MOVF       R8+1, 0
	MOVWF      R0+1
	MOVF       R0+0, 0
	ADDLW      48
	MOVWF      main_sir_L0+2
;design.c,74 :: 		sir[3]='0'+((t/10)%10);      // A 3-a cifra
	MOVLW      10
	MOVWF      R4+0
	MOVLW      0
	MOVWF      R4+1
	MOVF       main_t_L0+0, 0
	MOVWF      R0+0
	MOVF       main_t_L0+1, 0
	MOVWF      R0+1
	CALL       _Div_16x16_S+0
	MOVLW      10
	MOVWF      R4+0
	MOVLW      0
	MOVWF      R4+1
	CALL       _Div_16x16_S+0
	MOVF       R8+0, 0
	MOVWF      R0+0
	MOVF       R8+1, 0
	MOVWF      R0+1
	MOVF       R0+0, 0
	ADDLW      48
	MOVWF      main_sir_L0+3
;design.c,75 :: 		sir[4]='0'+t%10;             // A 4-a cifra
	MOVLW      10
	MOVWF      R4+0
	MOVLW      0
	MOVWF      R4+1
	MOVF       main_t_L0+0, 0
	MOVWF      R0+0
	MOVF       main_t_L0+1, 0
	MOVWF      R0+1
	CALL       _Div_16x16_S+0
	MOVF       R8+0, 0
	MOVWF      R0+0
	MOVF       R8+1, 0
	MOVWF      R0+1
	MOVF       R0+0, 0
	ADDLW      48
	MOVWF      main_sir_L0+4
;design.c,76 :: 		sir[5]='\0';                 // Terminator de sir
	CLRF       main_sir_L0+5
;design.c,77 :: 		LCD_out(1,1,sir);            // Scriu pe LCD
	MOVLW      1
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      1
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      main_sir_L0+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
;design.c,78 :: 		delay_ms(500);               // Astept juma' de secunda
	MOVLW      3
	MOVWF      R11+0
	MOVLW      138
	MOVWF      R12+0
	MOVLW      85
	MOVWF      R13+0
L_main4:
	DECFSZ     R13+0, 1
	GOTO       L_main4
	DECFSZ     R12+0, 1
	GOTO       L_main4
	DECFSZ     R11+0, 1
	GOTO       L_main4
	NOP
	NOP
;design.c,79 :: 		}
	GOTO       L_main2
;design.c,80 :: 		}
	GOTO       $+0
; end of _main
