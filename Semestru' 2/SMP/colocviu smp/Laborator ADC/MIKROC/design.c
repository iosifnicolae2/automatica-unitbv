// LCD module connections
sbit LCD_RS at RB4_bit;
sbit LCD_EN at RB5_bit;
sbit LCD_D4 at RB0_bit;
sbit LCD_D5 at RB1_bit;
sbit LCD_D6 at RB2_bit;
sbit LCD_D7 at RB3_bit;

sbit LCD_RS_Direction at TRISB4_bit;
sbit LCD_EN_Direction at TRISB5_bit;
sbit LCD_D4_Direction at TRISB0_bit;
sbit LCD_D5_Direction at TRISB1_bit;
sbit LCD_D6_Direction at TRISB2_bit;
sbit LCD_D7_Direction at TRISB3_bit;
// End LCD module connections

void initializare()
{
 ANSEL=0b00000001;// ultimul bit este aferent lui AN0 asa ca il setez 1
 //ADCON0=0;
 ADCON0.ADFM=1;// bitul ADFM aliniaza rezultatul conversiei, 1 inseamna la dreapta
                  // asta face ca ADRESH sa contina cei mai semnificativi 2 biti ai rezultatului
                  // iar ADRESL cei mai putin semnificativi 8 biti.
 ADCON0.ADON=1; // Pornesc ADC-ul ca periferic
 ADCON0.VCFG1=0;// Setez tensiunea de sursa la tensiunea interna a uC (5V)
 ADCON0.VCFG0=0;// Setez tensiunea de drena la tensiunea interna a uC (GND)
 ADCON0.CHS0=0; // Folosesc 3 biti pentru selectarea canalului
 ADCON0.CHS1=0; // Combinatia 0-0-0 in CHS0-CHS1-CHS2 imi va da AN0 (din datasheet)
 ADCON0.CHS2=0; 
 ADCON1.ADCS0=0;             // Folosesc 3 biti pentru selectarea clockului ce va if utilizat de ADC
 ADCON1.ADCS1=0;             // Combinatia 0-0-1 in ADCS0-ADCS1-ADCS2 imi va da FOSC/4 (din datasheet)
 ADCON1.ADCS2=1;             // FOSC/4 = Frecventa ceasului uC / 4
}

unsigned int citire()
{
 unsigned int var=0;
 ADCON0.GO_DONE=1;           // Pornesc conversia
 while (ADCON0.GO_DONE)      // Astept pana la terminarea conversiei
 {}
 var=ADRESH << 8 | ADRESL;
 // Rezultatul conversiei e pe 10 biti (0-1023)
 // ADRESH contine primii 2 biti cei mai semnificativi
 // asa ca ii trimit spre stanga "<<" cu 8 pozitii
 // ADRESL contine 8 biti, cei mai putin seminificativi
 // asa ca il las pe loc. Cu operatia de sau logic la nivel de bit "|"
 // reunesc cele 2 numere.

 // Varianta de la scoala, prin inmultire cu 2*8 si ADUNAREA lui ADRESL introduce o mica eroare*
 // *sau cel putin asa mi-a dat mie :P
 return var;
}


void main() {
 int t=0; //tensiune
 char sir[15];
 unsigned int valoare=0;
 float pas=0.00488;
 int nr=0;

 LCD_init();                   // Initializez LCD
 TRISB=0;                      // PORTUL B va avea doar iesiri (0) pentru ca se va comanda LCD-ul
 TRISA.RA0=1;                  // Pinul RA0 este AN0, 1 pentru intrare
 initializare();               // Initializez ADC-ul
 while (1)
 {
  valoare=citire();            // Citesc de la ADC
  nr=valoare*pas*1000;         // Aflu valoarea tensiunii inmultita cu 1000
  t=nr;
  sir[0]='0'+t/1000;           // Scot prima cifra a numarului de maxim 4 cifre
  sir[1]=',';                  // Pun virgula :D
  sir[2]='0'+((t/100)%10);     // A 2-a cifra
  sir[3]='0'+((t/10)%10);      // A 3-a cifra
  sir[4]='0'+t%10;             // A 4-a cifra
  sir[5]='\0';                 // Terminator de sir
  LCD_out(1,1,sir);            // Scriu pe LCD
  delay_ms(500);               // Astept juma' de secunda
 }
}