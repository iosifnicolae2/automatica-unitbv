#line 1 "C:/Users/alex/Desktop/Laborator ADC/MIKROC/design.c"

sbit LCD_RS at RB4_bit;
sbit LCD_EN at RB5_bit;
sbit LCD_D4 at RB0_bit;
sbit LCD_D5 at RB1_bit;
sbit LCD_D6 at RB2_bit;
sbit LCD_D7 at RB3_bit;

sbit LCD_RS_Direction at TRISB4_bit;
sbit LCD_EN_Direction at TRISB5_bit;
sbit LCD_D4_Direction at TRISB0_bit;
sbit LCD_D5_Direction at TRISB1_bit;
sbit LCD_D6_Direction at TRISB2_bit;
sbit LCD_D7_Direction at TRISB3_bit;


void initializare()
{
 ANSEL=0b00000001;

 ADCON0.ADFM=1;


 ADCON0.ADON=1;
 ADCON0.VCFG1=0;
 ADCON0.VCFG0=0;
 ADCON0.CHS0=0;
 ADCON0.CHS1=0;
 ADCON0.CHS2=0;
 ADCON1.ADCS0=0;
 ADCON1.ADCS1=0;
 ADCON1.ADCS2=1;
}

unsigned int citire()
{
 unsigned int var=0;
 ADCON0.GO_DONE=1;
 while (ADCON0.GO_DONE)
 {}
 var=ADRESH << 8 | ADRESL;









 return var;
}


void main() {
 int t=0;
 char sir[15];
 unsigned int valoare=0;
 float pas=0.00488;
 int nr=0;

 LCD_init();
 TRISB=0;
 TRISA.RA0=1;
 initializare();
 while (1)
 {
 valoare=citire();
 nr=valoare*pas*1000;
 t=nr;
 sir[0]='0'+t/1000;
 sir[1]=',';
 sir[2]='0'+((t/100)%10);
 sir[3]='0'+((t/10)%10);
 sir[4]='0'+t%10;
 sir[5]='\0';
 LCD_out(1,1,sir);
 delay_ms(500);
 }
}
