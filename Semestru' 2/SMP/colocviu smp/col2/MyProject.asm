
_main:

;MyProject.c,16 :: 		void main()
;MyProject.c,18 :: 		TRISB.RB0=1;
	BSF        TRISB+0, 0
;MyProject.c,19 :: 		portb.rb0=0;
	BCF        PORTB+0, 0
;MyProject.c,20 :: 		Lcd_Init();
	CALL       _Lcd_Init+0
;MyProject.c,21 :: 		Lcd_Cmd(_LCD_CLEAR);
	MOVLW      1
	MOVWF      FARG_Lcd_Cmd_out_char+0
	CALL       _Lcd_Cmd+0
;MyProject.c,22 :: 		Lcd_Cmd(_LCD_CURSOR_OFF);
	MOVLW      12
	MOVWF      FARG_Lcd_Cmd_out_char+0
	CALL       _Lcd_Cmd+0
;MyProject.c,23 :: 		Lcd_Out(1,4,txt);
	MOVLW      1
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      4
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      _txt+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
;MyProject.c,24 :: 		while(1)
L_main0:
;MyProject.c,26 :: 		if(PORTB.RB0==1)
	BTFSS      PORTB+0, 0
	GOTO       L_main2
;MyProject.c,28 :: 		Lcd_Out(1,4,"start\0");
	MOVLW      1
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      4
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      ?lstr1_MyProject+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
;MyProject.c,29 :: 		while(PORTB.RB0==1)
L_main3:
	BTFSS      PORTB+0, 0
	GOTO       L_main4
;MyProject.c,30 :: 		{}
	GOTO       L_main3
L_main4:
;MyProject.c,31 :: 		}
L_main2:
;MyProject.c,32 :: 		if(portb.rb0==0)
	BTFSC      PORTB+0, 0
	GOTO       L_main5
;MyProject.c,34 :: 		Lcd_out(1,4," stop\0");
	MOVLW      1
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      4
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      ?lstr2_MyProject+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
;MyProject.c,35 :: 		}
L_main5:
;MyProject.c,37 :: 		}
	GOTO       L_main0
;MyProject.c,40 :: 		}
L_end_main:
	GOTO       $+0
; end of _main
