
_main:

;v1.c,17 :: 		void main() {
;v1.c,18 :: 		TRISB=0b00000001;
	MOVLW      1
	MOVWF      TRISB+0
;v1.c,19 :: 		PORTB.RB0=0;
	BCF        PORTB+0, 0
;v1.c,20 :: 		Lcd_Init();
	CALL       _Lcd_Init+0
;v1.c,21 :: 		Lcd_Cmd(_LCD_CLEAR);
	MOVLW      1
	MOVWF      FARG_Lcd_Cmd_out_char+0
	CALL       _Lcd_Cmd+0
;v1.c,22 :: 		Lcd_Cmd(_LCD_CURSOR_OFF);
	MOVLW      12
	MOVWF      FARG_Lcd_Cmd_out_char+0
	CALL       _Lcd_Cmd+0
;v1.c,24 :: 		while(1)
L_main0:
;v1.c,26 :: 		PORTB=0b00000010;
	MOVLW      2
	MOVWF      PORTB+0
;v1.c,28 :: 		Lcd_Out(1,6,"BEC1 ON~~");
	MOVLW      1
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      6
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      ?lstr1_v1+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
;v1.c,30 :: 		if (PORTB.RB0==1)
	BTFSS      PORTB+0, 0
	GOTO       L_main2
;v1.c,32 :: 		PORTB.RB1=0;
	BCF        PORTB+0, 1
;v1.c,33 :: 		PORTB.RB2=1;
	BSF        PORTB+0, 2
;v1.c,35 :: 		Lcd_Cmd(_LCD_CLEAR);
	MOVLW      1
	MOVWF      FARG_Lcd_Cmd_out_char+0
	CALL       _Lcd_Cmd+0
;v1.c,37 :: 		Lcd_Out(1,6,"BEC2 ON");
	MOVLW      1
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      6
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      ?lstr2_v1+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
;v1.c,38 :: 		while(PORTB.RB0==1);
L_main3:
	BTFSS      PORTB+0, 0
	GOTO       L_main4
	GOTO       L_main3
L_main4:
;v1.c,40 :: 		{}}
L_main2:
;v1.c,48 :: 		}
	GOTO       L_main0
;v1.c,49 :: 		}
L_end_main:
	GOTO       $+0
; end of _main
