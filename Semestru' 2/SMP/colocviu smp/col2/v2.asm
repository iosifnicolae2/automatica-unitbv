
_interrupt:
	MOVWF      R15+0
	SWAPF      STATUS+0, 0
	CLRF       STATUS+0
	MOVWF      ___saveSTATUS+0
	MOVF       PCLATH+0, 0
	MOVWF      ___savePCLATH+0
	CLRF       PCLATH+0

;v2.c,14 :: 		void interrupt()
;v2.c,16 :: 		intcon.gie=0;
	BCF        INTCON+0, 7
;v2.c,17 :: 		if (intcon.t0if==1)
	BTFSS      INTCON+0, 2
	GOTO       L_interrupt0
;v2.c,19 :: 		Lcd_Out(1,6,"ana");
	MOVLW      1
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      6
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      ?lstr1_v2+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
;v2.c,21 :: 		}
L_interrupt0:
;v2.c,22 :: 		intcon.t0if=0;
	BCF        INTCON+0, 2
;v2.c,23 :: 		intcon.gie=1;
	BSF        INTCON+0, 7
;v2.c,24 :: 		Tmr0=011;
	MOVLW      9
	MOVWF      TMR0+0
;v2.c,26 :: 		}
L_end_interrupt:
L__interrupt4:
	MOVF       ___savePCLATH+0, 0
	MOVWF      PCLATH+0
	SWAPF      ___saveSTATUS+0, 0
	MOVWF      STATUS+0
	SWAPF      R15+0, 1
	SWAPF      R15+0, 0
	RETFIE
; end of _interrupt

_main:

;v2.c,28 :: 		void main() {
;v2.c,29 :: 		Lcd_Init();
	CALL       _Lcd_Init+0
;v2.c,31 :: 		Lcd_Cmd(_LCD_CLEAR);
	MOVLW      1
	MOVWF      FARG_Lcd_Cmd_out_char+0
	CALL       _Lcd_Cmd+0
;v2.c,32 :: 		Lcd_Cmd(_LCD_CURSOR_OFF);
	MOVLW      12
	MOVWF      FARG_Lcd_Cmd_out_char+0
	CALL       _Lcd_Cmd+0
;v2.c,33 :: 		Lcd_out(1,6,"nuu");
	MOVLW      1
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      6
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      ?lstr2_v2+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
;v2.c,34 :: 		intcon=0b11100000;
	MOVLW      224
	MOVWF      INTCON+0
;v2.c,35 :: 		option_reg =0b00000111;
	MOVLW      7
	MOVWF      OPTION_REG+0
;v2.c,36 :: 		while(1);
L_main1:
	GOTO       L_main1
;v2.c,42 :: 		}
L_end_main:
	GOTO       $+0
; end of _main
