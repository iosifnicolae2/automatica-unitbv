
_initial:

;v4.c,15 :: 		void initial()
;v4.c,17 :: 		ansel=1;
	MOVLW      1
	MOVWF      ANSEL+0
;v4.c,18 :: 		adcon0=0b00000001;
	MOVLW      1
	MOVWF      ADCON0+0
;v4.c,19 :: 		}
L_end_initial:
	RETURN
; end of _initial

_citire:

;v4.c,20 :: 		void citire()
;v4.c,22 :: 		val=ADC_Read(2);
	MOVLW      2
	MOVWF      FARG_ADC_Read_channel+0
	CALL       _ADC_Read+0
	MOVF       R0+0, 0
	MOVWF      _val+0
	MOVF       R0+1, 0
	MOVWF      _val+1
;v4.c,23 :: 		}
L_end_citire:
	RETURN
; end of _citire

_main:

;v4.c,24 :: 		void main() {
;v4.c,26 :: 		}
L_end_main:
	GOTO       $+0
; end of _main
