
_interrupt:
	MOVWF      R15+0
	SWAPF      STATUS+0, 0
	CLRF       STATUS+0
	MOVWF      ___saveSTATUS+0
	MOVF       PCLATH+0, 0
	MOVWF      ___savePCLATH+0
	CLRF       PCLATH+0

;v3.c,16 :: 		void interrupt()
;v3.c,18 :: 		intcon.gie=0;
	BCF        INTCON+0, 7
;v3.c,19 :: 		if (intcon.t0if==1)
	BTFSS      INTCON+0, 2
	GOTO       L_interrupt0
;v3.c,20 :: 		{       a=0;
	CLRF       _a+0
	CLRF       _a+1
;v3.c,23 :: 		}
L_interrupt0:
;v3.c,24 :: 		intcon.t0if=0;
	BCF        INTCON+0, 2
;v3.c,25 :: 		intcon.gie=1;
	BSF        INTCON+0, 7
;v3.c,26 :: 		Tmr0=011;
	MOVLW      9
	MOVWF      TMR0+0
;v3.c,28 :: 		}
L_end_interrupt:
L__interrupt6:
	MOVF       ___savePCLATH+0, 0
	MOVWF      PCLATH+0
	SWAPF      ___saveSTATUS+0, 0
	MOVWF      STATUS+0
	SWAPF      R15+0, 1
	SWAPF      R15+0, 0
	RETFIE
; end of _interrupt

_main:

;v3.c,30 :: 		void main() {
;v3.c,31 :: 		Lcd_Init();
	CALL       _Lcd_Init+0
;v3.c,33 :: 		Lcd_Cmd(_LCD_CLEAR);
	MOVLW      1
	MOVWF      FARG_Lcd_Cmd_out_char+0
	CALL       _Lcd_Cmd+0
;v3.c,34 :: 		Lcd_Cmd(_LCD_CURSOR_OFF);
	MOVLW      12
	MOVWF      FARG_Lcd_Cmd_out_char+0
	CALL       _Lcd_Cmd+0
;v3.c,35 :: 		Lcd_out(1,6,"nuu");
	MOVLW      1
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      6
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      ?lstr1_v3+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
;v3.c,36 :: 		delay_ms(400)  ;
	MOVLW      5
	MOVWF      R11+0
	MOVLW      15
	MOVWF      R12+0
	MOVLW      241
	MOVWF      R13+0
L_main1:
	DECFSZ     R13+0, 1
	GOTO       L_main1
	DECFSZ     R12+0, 1
	GOTO       L_main1
	DECFSZ     R11+0, 1
	GOTO       L_main1
;v3.c,37 :: 		intcon=0b11100000;
	MOVLW      224
	MOVWF      INTCON+0
;v3.c,38 :: 		option_reg =0b00000111;
	MOVLW      7
	MOVWF      OPTION_REG+0
;v3.c,39 :: 		while(1)
L_main2:
;v3.c,41 :: 		if(a==0)
	MOVLW      0
	XORWF      _a+1, 0
	BTFSS      STATUS+0, 2
	GOTO       L__main8
	MOVLW      0
	XORWF      _a+0, 0
L__main8:
	BTFSS      STATUS+0, 2
	GOTO       L_main4
;v3.c,43 :: 		Lcd_out(1,6,"anu");
	MOVLW      1
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      6
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      ?lstr2_v3+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
;v3.c,44 :: 		}
L_main4:
;v3.c,45 :: 		}
	GOTO       L_main2
;v3.c,51 :: 		}
L_end_main:
	GOTO       $+0
; end of _main
