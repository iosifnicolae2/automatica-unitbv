
_interrupt:
	MOVWF      R15+0
	SWAPF      STATUS+0, 0
	CLRF       STATUS+0
	MOVWF      ___saveSTATUS+0
	MOVF       PCLATH+0, 0
	MOVWF      ___savePCLATH+0
	CLRF       PCLATH+0
;L7 P2.c,3 :: 		void interrupt()
;L7 P2.c,5 :: 		INTCON.GIE=0;
	BCF        INTCON+0, 7
;L7 P2.c,6 :: 		if(INTCON.T0IF)
	BTFSS      INTCON+0, 2
	GOTO       L_interrupt0
;L7 P2.c,8 :: 		INTCON.T0IF=0;
	BCF        INTCON+0, 2
;L7 P2.c,9 :: 		i++;
	INCF       _i+0, 1
	BTFSC      STATUS+0, 2
	INCF       _i+1, 1
;L7 P2.c,10 :: 		TMR0 = 158;
	MOVLW      158
	MOVWF      TMR0+0
;L7 P2.c,11 :: 		if (i == 20)
	MOVLW      0
	XORWF      _i+1, 0
	BTFSS      STATUS+0, 2
	GOTO       L__interrupt5
	MOVLW      20
	XORWF      _i+0, 0
L__interrupt5:
	BTFSS      STATUS+0, 2
	GOTO       L_interrupt1
;L7 P2.c,13 :: 		PORTB.RB0 =! PORTB.RB0;
	MOVLW      1
	XORWF      PORTB+0, 1
;L7 P2.c,14 :: 		i = 0;
	CLRF       _i+0
	CLRF       _i+1
;L7 P2.c,15 :: 		}
L_interrupt1:
;L7 P2.c,16 :: 		INTCON.GIE=1;
	BSF        INTCON+0, 7
;L7 P2.c,17 :: 		}
L_interrupt0:
;L7 P2.c,18 :: 		}
L__interrupt4:
	MOVF       ___savePCLATH+0, 0
	MOVWF      PCLATH+0
	SWAPF      ___saveSTATUS+0, 0
	MOVWF      STATUS+0
	SWAPF      R15+0, 1
	SWAPF      R15+0, 0
	RETFIE
; end of _interrupt

_main:
;L7 P2.c,20 :: 		void main()
;L7 P2.c,22 :: 		INTCON = 0b10100000;
	MOVLW      160
	MOVWF      INTCON+0
;L7 P2.c,23 :: 		OPTION_REG = 0b00000101;
	MOVLW      5
	MOVWF      OPTION_REG+0
;L7 P2.c,24 :: 		TRISB = 0;
	CLRF       TRISB+0
;L7 P2.c,25 :: 		TMR0 = 158;
	MOVLW      158
	MOVWF      TMR0+0
;L7 P2.c,26 :: 		while(1)
L_main2:
;L7 P2.c,28 :: 		}
	GOTO       L_main2
;L7 P2.c,29 :: 		}
	GOTO       $+0
; end of _main
