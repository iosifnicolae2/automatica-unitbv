
_interrupt:
	MOVWF      R15+0
	SWAPF      STATUS+0, 0
	CLRF       STATUS+0
	MOVWF      ___saveSTATUS+0
	MOVF       PCLATH+0, 0
	MOVWF      ___savePCLATH+0
	CLRF       PCLATH+0

;L7 P1.c,5 :: 		void interrupt()
;L7 P1.c,7 :: 		INTCON.GIE=0;              //oprirea tuturor intreruperilor
	BCF        INTCON+0, 7
;L7 P1.c,8 :: 		if(INTCON.T0IF)            //rutina de intrerupere pentru timer
	BTFSS      INTCON+0, 2
	GOTO       L_interrupt0
;L7 P1.c,10 :: 		i++;                      //incrementare i;
	INCF       _i+0, 1
	BTFSC      STATUS+0, 2
	INCF       _i+1, 1
;L7 P1.c,11 :: 		INTCON.T0IF=0;            //resetare flag
	BCF        INTCON+0, 2
;L7 P1.c,12 :: 		TMR0=99;                  //resetare valoare initiala timer
	MOVLW      99
	MOVWF      TMR0+0
;L7 P1.c,13 :: 		}
L_interrupt0:
;L7 P1.c,14 :: 		INTCON.GIE=1;              //pornirea intreruperilor
	BSF        INTCON+0, 7
;L7 P1.c,15 :: 		}
L_end_interrupt:
L__interrupt4:
	MOVF       ___savePCLATH+0, 0
	MOVWF      PCLATH+0
	SWAPF      ___saveSTATUS+0, 0
	MOVWF      STATUS+0
	SWAPF      R15+0, 1
	SWAPF      R15+0, 0
	RETFIE
; end of _interrupt

_main:

;L7 P1.c,17 :: 		void main()
;L7 P1.c,19 :: 		OPTION_REG=0b00000101;     //oscilator intern; incrementare pe front crescator cu prescalare 1:32
	MOVLW      5
	MOVWF      OPTION_REG+0
;L7 P1.c,20 :: 		INTCON.T0IE=1;
	BSF        INTCON+0, 5
;L7 P1.c,21 :: 		INTCON.PEIE=1;
	BSF        INTCON+0, 6
;L7 P1.c,22 :: 		INTCON.GIE=0;
	BCF        INTCON+0, 7
;L7 P1.c,23 :: 		TMR0=99;                   //setare valoare initiala timer
	MOVLW      99
	MOVWF      TMR0+0
;L7 P1.c,24 :: 		while(1)
L_main1:
;L7 P1.c,26 :: 		}
	GOTO       L_main1
;L7 P1.c,27 :: 		}
L_end_main:
	GOTO       $+0
; end of _main
