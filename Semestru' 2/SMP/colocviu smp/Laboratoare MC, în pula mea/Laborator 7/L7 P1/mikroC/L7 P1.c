/*
Sa se realizeze un program care incrementeaza variabila i la fiecare 20ms
Frecventa oscilatorului intern este de 1 MHz
*/

int i=0;

void interrupt()
{
     INTCON.GIE=0;              //oprirea tuturor intreruperilor
     if(INTCON.T0IF)            //rutina de intrerupere pentru timer
     {
      i++;                      //incrementare i;
      INTCON.T0IF=0;            //resetare flag
      TMR0=99;                  //resetare valoare initiala timer
     }
     INTCON.GIE=1;              //pornirea intreruperilor
}

void main()
{
     OPTION_REG=0b00000101;     //oscilator intern; incrementare pe front crescator cu prescalare 1:32
     INTCON.T0IE=1;
     INTCON.PEIE=1;
     INTCON.GIE=0;
     TMR0=99;                   //setare valoare initiala timer
     while(1)
     {
     }
}