
_interrupt:
	MOVWF      R15+0
	SWAPF      STATUS+0, 0
	CLRF       STATUS+0
	MOVWF      ___saveSTATUS+0
	MOVF       PCLATH+0, 0
	MOVWF      ___savePCLATH+0
	CLRF       PCLATH+0

;L7 P3.c,24 :: 		void interrupt()
;L7 P3.c,26 :: 		INTCON.GIE=0;
	BCF        INTCON+0, 7
;L7 P3.c,27 :: 		if(INTCON.T0IF)
	BTFSS      INTCON+0, 2
	GOTO       L_interrupt0
;L7 P3.c,29 :: 		INTCON.T0IF=0;
	BCF        INTCON+0, 2
;L7 P3.c,30 :: 		i++;
	INCF       _i+0, 1
	BTFSC      STATUS+0, 2
	INCF       _i+1, 1
;L7 P3.c,31 :: 		TMR0 = 158;
	MOVLW      158
	MOVWF      TMR0+0
;L7 P3.c,32 :: 		if (i == 20)
	MOVLW      0
	XORWF      _i+1, 0
	BTFSS      STATUS+0, 2
	GOTO       L__interrupt9
	MOVLW      20
	XORWF      _i+0, 0
L__interrupt9:
	BTFSS      STATUS+0, 2
	GOTO       L_interrupt1
;L7 P3.c,34 :: 		secunda++;
	INCF       _secunda+0, 1
	BTFSC      STATUS+0, 2
	INCF       _secunda+1, 1
;L7 P3.c,35 :: 		if (secunda == 59)
	MOVLW      0
	XORWF      _secunda+1, 0
	BTFSS      STATUS+0, 2
	GOTO       L__interrupt10
	MOVLW      59
	XORWF      _secunda+0, 0
L__interrupt10:
	BTFSS      STATUS+0, 2
	GOTO       L_interrupt2
;L7 P3.c,37 :: 		minut++;
	INCF       _minut+0, 1
	BTFSC      STATUS+0, 2
	INCF       _minut+1, 1
;L7 P3.c,38 :: 		secunda= 0;
	CLRF       _secunda+0
	CLRF       _secunda+1
;L7 P3.c,39 :: 		if (minut == 59)
	MOVLW      0
	XORWF      _minut+1, 0
	BTFSS      STATUS+0, 2
	GOTO       L__interrupt11
	MOVLW      59
	XORWF      _minut+0, 0
L__interrupt11:
	BTFSS      STATUS+0, 2
	GOTO       L_interrupt3
;L7 P3.c,41 :: 		ora++;
	INCF       _ora+0, 1
	BTFSC      STATUS+0, 2
	INCF       _ora+1, 1
;L7 P3.c,42 :: 		minut = 0;
	CLRF       _minut+0
	CLRF       _minut+1
;L7 P3.c,43 :: 		if (ora == 24)
	MOVLW      0
	XORWF      _ora+1, 0
	BTFSS      STATUS+0, 2
	GOTO       L__interrupt12
	MOVLW      24
	XORWF      _ora+0, 0
L__interrupt12:
	BTFSS      STATUS+0, 2
	GOTO       L_interrupt4
;L7 P3.c,45 :: 		ora = 0;
	CLRF       _ora+0
	CLRF       _ora+1
;L7 P3.c,46 :: 		minut = 0;
	CLRF       _minut+0
	CLRF       _minut+1
;L7 P3.c,47 :: 		}
L_interrupt4:
;L7 P3.c,48 :: 		}
L_interrupt3:
;L7 P3.c,49 :: 		}
L_interrupt2:
;L7 P3.c,50 :: 		i = 0;
	CLRF       _i+0
	CLRF       _i+1
;L7 P3.c,51 :: 		}
L_interrupt1:
;L7 P3.c,52 :: 		INTCON.GIE=1;
	BSF        INTCON+0, 7
;L7 P3.c,53 :: 		}
L_interrupt0:
;L7 P3.c,54 :: 		}
L_end_interrupt:
L__interrupt8:
	MOVF       ___savePCLATH+0, 0
	MOVWF      PCLATH+0
	SWAPF      ___saveSTATUS+0, 0
	MOVWF      STATUS+0
	SWAPF      R15+0, 1
	SWAPF      R15+0, 0
	RETFIE
; end of _interrupt

_main:

;L7 P3.c,56 :: 		void main()
;L7 P3.c,59 :: 		INTCON = 0b10100000;
	MOVLW      160
	MOVWF      INTCON+0
;L7 P3.c,60 :: 		OPTION_REG = 0b00000101;
	MOVLW      5
	MOVWF      OPTION_REG+0
;L7 P3.c,61 :: 		TRISB = 0;
	CLRF       TRISB+0
;L7 P3.c,62 :: 		Lcd_Init();
	CALL       _Lcd_Init+0
;L7 P3.c,63 :: 		Lcd_Cmd(_LCD_CURSOR_OFF);
	MOVLW      12
	MOVWF      FARG_Lcd_Cmd_out_char+0
	CALL       _Lcd_Cmd+0
;L7 P3.c,64 :: 		while(1)
L_main5:
;L7 P3.c,66 :: 		sprinti(sir, "%2d:%2d:%2d", ora, minut, secunda);
	MOVLW      main_sir_L0+0
	MOVWF      FARG_sprinti_wh+0
	MOVLW      ?lstr_1_L7_32P3+0
	MOVWF      FARG_sprinti_f+0
	MOVLW      hi_addr(?lstr_1_L7_32P3+0)
	MOVWF      FARG_sprinti_f+1
	MOVF       _ora+0, 0
	MOVWF      FARG_sprinti_wh+3
	MOVF       _ora+1, 0
	MOVWF      FARG_sprinti_wh+4
	MOVF       _minut+0, 0
	MOVWF      FARG_sprinti_wh+5
	MOVF       _minut+1, 0
	MOVWF      FARG_sprinti_wh+6
	MOVF       _secunda+0, 0
	MOVWF      FARG_sprinti_wh+7
	MOVF       _secunda+1, 0
	MOVWF      FARG_sprinti_wh+8
	CALL       _sprinti+0
;L7 P3.c,67 :: 		Lcd_Out(1, 1, "Ora este: ");
	MOVLW      1
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      1
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      ?lstr2_L7_32P3+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
;L7 P3.c,68 :: 		Lcd_Out(2, 1, sir);
	MOVLW      2
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      1
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      main_sir_L0+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
;L7 P3.c,69 :: 		}
	GOTO       L_main5
;L7 P3.c,70 :: 		}
L_end_main:
	GOTO       $+0
; end of _main
