#line 1 "X:/MC/Laborator 7/L7 P3/mikroC/L7 P3.c"
#line 5 "X:/MC/Laborator 7/L7 P3/mikroC/L7 P3.c"
sbit LCD_EN at RB5_bit;
sbit LCD_RS at RB4_bit;
sbit LCD_D7 at RB3_bit;
sbit LCD_D6 at RB2_bit;
sbit LCD_D5 at RB1_bit;
sbit LCD_D4 at RB0_bit;

sbit LCD_EN_Direction at TRISB5_bit;
sbit LCD_RS_Direction at TRISB4_bit;
sbit LCD_D7_Direction at TRISB3_bit;
sbit LCD_D6_Direction at TRISB2_bit;
sbit LCD_D5_Direction at TRISB1_bit;
sbit LCD_D4_Direction at TRISB0_bit;

int secunda;
int minut;
int ora;
int i;

void interrupt()
{
 INTCON.GIE=0;
 if(INTCON.T0IF)
 {
 INTCON.T0IF=0;
 i++;
 TMR0 = 158;
 if (i == 20)
 {
 secunda++;
 if (secunda == 59)
 {
 minut++;
 secunda= 0;
 if (minut == 59)
 {
 ora++;
 minut = 0;
 if (ora == 24)
 {
 ora = 0;
 minut = 0;
 }
 }
 }
 i = 0;
 }
 INTCON.GIE=1;
 }
}

void main()
{
 char sir[16];
 INTCON = 0b10100000;
 OPTION_REG = 0b00000101;
 TRISB = 0;
 Lcd_Init();
 Lcd_Cmd(_LCD_CURSOR_OFF);
 while(1)
 {
 sprinti(sir, "%2d:%2d:%2d", ora, minut, secunda);
 Lcd_Out(1, 1, "Ora este: ");
 Lcd_Out(2, 1, sir);
 }
}
