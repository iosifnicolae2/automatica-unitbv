
_main:

;L4 P2.c,1 :: 		void main() {
;L4 P2.c,10 :: 		TRISB.RB0 = 1;              //  BP
	BSF        TRISB+0, 0
;L4 P2.c,11 :: 		TRISB.RB1 = 1;              //  S1
	BSF        TRISB+0, 1
;L4 P2.c,12 :: 		TRISB.RB2 = 1;              //  S2
	BSF        TRISB+0, 2
;L4 P2.c,13 :: 		TRISB.RB3 = 1;              //  S3
	BSF        TRISB+0, 3
;L4 P2.c,14 :: 		TRISB.RB4 = 1;              //  S4
	BSF        TRISB+0, 4
;L4 P2.c,17 :: 		TRISD.RD0 = 0;              //  Y1
	BCF        TRISD+0, 0
;L4 P2.c,18 :: 		TRISD.RD1 = 0;              //  Y2
	BCF        TRISD+0, 1
;L4 P2.c,19 :: 		TRISD.RD2 = 0;              //  Y3
	BCF        TRISD+0, 2
;L4 P2.c,20 :: 		TRISD.RD3 = 0;              //  Y4
	BCF        TRISD+0, 3
;L4 P2.c,21 :: 		TRISD.RD4 = 0;              //  M
	BCF        TRISD+0, 4
;L4 P2.c,24 :: 		A = 1;
	MOVLW      1
	MOVWF      R1+0
;L4 P2.c,25 :: 		B = 0;
	CLRF       R2+0
;L4 P2.c,26 :: 		C = 0;
	CLRF       R3+0
;L4 P2.c,27 :: 		D = 1;
	MOVLW      1
	MOVWF      R4+0
;L4 P2.c,28 :: 		E = 0;
	CLRF       R5+0
;L4 P2.c,30 :: 		while(1)
L_main0:
;L4 P2.c,33 :: 		if ((A == 1) && (PORTB.RB0 == 1))
	MOVF       R1+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main4
	BTFSS      PORTB+0, 0
	GOTO       L_main4
L__main26:
;L4 P2.c,35 :: 		A = 0;
	CLRF       R1+0
;L4 P2.c,36 :: 		B = 1;
	MOVLW      1
	MOVWF      R2+0
;L4 P2.c,37 :: 		}
L_main4:
;L4 P2.c,38 :: 		if ((B == 1) && (PORTB.RB2 == 1))
	MOVF       R2+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main7
	BTFSS      PORTB+0, 2
	GOTO       L_main7
L__main25:
;L4 P2.c,40 :: 		B = 0;
	CLRF       R2+0
;L4 P2.c,41 :: 		C = 1;
	MOVLW      1
	MOVWF      R3+0
;L4 P2.c,42 :: 		}
L_main7:
;L4 P2.c,43 :: 		if ((C == 1) && (PORTB.RB1 == 1))
	MOVF       R3+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main10
	BTFSS      PORTB+0, 1
	GOTO       L_main10
L__main24:
;L4 P2.c,45 :: 		C = 0;
	CLRF       R3+0
;L4 P2.c,47 :: 		D = 1;
	MOVLW      1
	MOVWF      R4+0
;L4 P2.c,48 :: 		}
L_main10:
;L4 P2.c,49 :: 		if ((D == 1) && (PORTB.RB3 == 1))// && (PORTB.RB4 == 1))
	MOVF       R4+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main13
	BTFSS      PORTB+0, 3
	GOTO       L_main13
L__main23:
;L4 P2.c,51 :: 		D = 0;
	CLRF       R4+0
;L4 P2.c,52 :: 		E = 1;
	MOVLW      1
	MOVWF      R5+0
;L4 P2.c,53 :: 		}
L_main13:
;L4 P2.c,54 :: 		if ((E == 1) && (PORTB.RB2 == 1))
	MOVF       R5+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main16
	BTFSS      PORTB+0, 2
	GOTO       L_main16
L__main22:
;L4 P2.c,56 :: 		E = 0;
	CLRF       R5+0
;L4 P2.c,57 :: 		D = 1;
	MOVLW      1
	MOVWF      R4+0
;L4 P2.c,59 :: 		}
L_main16:
;L4 P2.c,62 :: 		if (A == 1)
	MOVF       R1+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main17
;L4 P2.c,64 :: 		PORTD.RD0 = 0;
	BCF        PORTD+0, 0
;L4 P2.c,65 :: 		PORTD.RD1 = 0;
	BCF        PORTD+0, 1
;L4 P2.c,66 :: 		PORTD.RD2 = 0;
	BCF        PORTD+0, 2
;L4 P2.c,67 :: 		PORTD.RD3 = 0;
	BCF        PORTD+0, 3
;L4 P2.c,68 :: 		PORTD.RD4 = 0;
	BCF        PORTD+0, 4
;L4 P2.c,69 :: 		}
L_main17:
;L4 P2.c,70 :: 		if (B == 1)
	MOVF       R2+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main18
;L4 P2.c,72 :: 		PORTD.RD0 = 1;
	BSF        PORTD+0, 0
;L4 P2.c,73 :: 		PORTD.RD1 = 0;
	BCF        PORTD+0, 1
;L4 P2.c,74 :: 		PORTD.RD2 = 1;
	BSF        PORTD+0, 2
;L4 P2.c,75 :: 		PORTD.RD3 = 0;
	BCF        PORTD+0, 3
;L4 P2.c,76 :: 		PORTD.RD4 = 1;
	BSF        PORTD+0, 4
;L4 P2.c,77 :: 		}
L_main18:
;L4 P2.c,78 :: 		if (C == 1)
	MOVF       R3+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main19
;L4 P2.c,80 :: 		PORTD.RD0 = 0;
	BCF        PORTD+0, 0
;L4 P2.c,81 :: 		PORTD.RD1 = 1;
	BSF        PORTD+0, 1
;L4 P2.c,82 :: 		PORTD.RD2 = 0;
	BCF        PORTD+0, 2
;L4 P2.c,83 :: 		PORTD.RD3 = 0;
	BCF        PORTD+0, 3
;L4 P2.c,84 :: 		PORTD.RD4 = 0;
	BCF        PORTD+0, 4
;L4 P2.c,85 :: 		}
L_main19:
;L4 P2.c,86 :: 		if (D == 1)
	MOVF       R4+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main20
;L4 P2.c,88 :: 		PORTD.RD0 = 0;
	BCF        PORTD+0, 0
;L4 P2.c,89 :: 		PORTD.RD1 = 0;
	BCF        PORTD+0, 1
;L4 P2.c,90 :: 		PORTD.RD2 = 1;
	BSF        PORTD+0, 2
;L4 P2.c,91 :: 		PORTD.RD3 = 0;
	BCF        PORTD+0, 3
;L4 P2.c,92 :: 		PORTD.RD4 = 0;
	BCF        PORTD+0, 4
;L4 P2.c,93 :: 		}
L_main20:
;L4 P2.c,94 :: 		if (E == 1)
	MOVF       R5+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main21
;L4 P2.c,96 :: 		PORTD.RD0 = 0;
	BCF        PORTD+0, 0
;L4 P2.c,97 :: 		PORTD.RD1 = 0;
	BCF        PORTD+0, 1
;L4 P2.c,98 :: 		PORTD.RD2 = 0;
	BCF        PORTD+0, 2
;L4 P2.c,99 :: 		PORTD.RD3 = 1;
	BSF        PORTD+0, 3
;L4 P2.c,100 :: 		PORTD.RD4 = 0;
	BCF        PORTD+0, 4
;L4 P2.c,101 :: 		}
L_main21:
;L4 P2.c,102 :: 		}
	GOTO       L_main0
;L4 P2.c,103 :: 		}
L_end_main:
	GOTO       $+0
; end of _main
