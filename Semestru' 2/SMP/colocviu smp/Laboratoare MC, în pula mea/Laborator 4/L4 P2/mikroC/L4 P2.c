void main() {
     //  declararea starilor
     short A;
     short B;
     short C;
     short D;
     short E;

     //  configurarea semnalelor de intrare
     TRISB.RB0 = 1;              //  BP
     TRISB.RB1 = 1;              //  S1
     TRISB.RB2 = 1;              //  S2
     TRISB.RB3 = 1;              //  S3
     TRISB.RB4 = 1;              //  S4

     //  configurarea semnalelor de iesire
     TRISD.RD0 = 0;              //  Y1
     TRISD.RD1 = 0;              //  Y2
     TRISD.RD2 = 0;              //  Y3
     TRISD.RD3 = 0;              //  Y4
     TRISD.RD4 = 0;              //  M

     //  initializarea starilor
     A = 1;
     B = 0;
     C = 0;
     D = 1;
     E = 0;

     while(1)
     {
             //  reconfigurarea starilor
             if ((A == 1) && (PORTB.RB0 == 1))
             {
                A = 0;
                B = 1;
             }
             if ((B == 1) && (PORTB.RB2 == 1))
             {
                B = 0;
                C = 1;
             }
             if ((C == 1) && (PORTB.RB1 == 1))
             {
                C = 0;
                //A = 1;
                D = 1;
             }
             if ((D == 1) && (PORTB.RB3 == 1)) && (PORTB.RB4 == 1))
             {
                D = 0;
                E = 1;
             }
             if ((E == 1) && (PORTB.RB2 == 1))
             {
                E = 0;
                D = 1;
                //A = 1;
             }

             //  reconfigurarea iesirilor
             if (A == 1)
             {
                PORTD.RD0 = 0;
                PORTD.RD1 = 0;
                PORTD.RD2 = 0;
                PORTD.RD3 = 0;
                PORTD.RD4 = 0;
             }
             if (B == 1)
             {
                PORTD.RD0 = 1;
                PORTD.RD1 = 0;
                PORTD.RD2 = 1;
                PORTD.RD3 = 0;
                PORTD.RD4 = 1;
             }
             if (C == 1)
             {
                PORTD.RD0 = 0;
                PORTD.RD1 = 1;
                PORTD.RD2 = 0;
                PORTD.RD3 = 0;
                PORTD.RD4 = 0;
             }
             if (D == 1)
             {
                PORTD.RD0 = 0;
                PORTD.RD1 = 0;
                PORTD.RD2 = 1;
                PORTD.RD3 = 0;
                PORTD.RD4 = 0;
             }
             if (E == 1)
             {
                PORTD.RD0 = 0;
                PORTD.RD1 = 0;
                PORTD.RD2 = 0;
                PORTD.RD3 = 1;
                PORTD.RD4 = 0;
             }
     }
}