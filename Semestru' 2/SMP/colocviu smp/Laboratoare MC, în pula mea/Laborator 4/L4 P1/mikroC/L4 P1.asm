
_main:

;L4 P1.c,5 :: 		void main() {
;L4 P1.c,12 :: 		TRISD.RD0=1;
	BSF        TRISD+0, 0
;L4 P1.c,13 :: 		TRISD.RD1=1;
	BSF        TRISD+0, 1
;L4 P1.c,14 :: 		TRISD.RD2=1;
	BSF        TRISD+0, 2
;L4 P1.c,17 :: 		TRISD.RD3=0;
	BCF        TRISD+0, 3
;L4 P1.c,18 :: 		TRISD.RD4=0;
	BCF        TRISD+0, 4
;L4 P1.c,21 :: 		A=1;
	MOVLW      1
	MOVWF      R1+0
;L4 P1.c,22 :: 		B=0;
	CLRF       R2+0
;L4 P1.c,23 :: 		C=0;
	CLRF       R3+0
;L4 P1.c,25 :: 		while(1)
L_main0:
;L4 P1.c,28 :: 		if ((A==1)&&(PORTD.RD0==1))
	MOVF       R1+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main4
	BTFSS      PORTD+0, 0
	GOTO       L_main4
L__main16:
;L4 P1.c,30 :: 		A=0;
	CLRF       R1+0
;L4 P1.c,31 :: 		B=1;
	MOVLW      1
	MOVWF      R2+0
;L4 P1.c,32 :: 		}
L_main4:
;L4 P1.c,33 :: 		if ((B==1)&&(PORTD.RD2==1))
	MOVF       R2+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main7
	BTFSS      PORTD+0, 2
	GOTO       L_main7
L__main15:
;L4 P1.c,35 :: 		B=0;
	CLRF       R2+0
;L4 P1.c,36 :: 		C=1;
	MOVLW      1
	MOVWF      R3+0
;L4 P1.c,37 :: 		}
L_main7:
;L4 P1.c,38 :: 		if ((C==1)&&(PORTD.RD1==1))
	MOVF       R3+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main10
	BTFSS      PORTD+0, 1
	GOTO       L_main10
L__main14:
;L4 P1.c,40 :: 		C=0;
	CLRF       R3+0
;L4 P1.c,41 :: 		A=1;
	MOVLW      1
	MOVWF      R1+0
;L4 P1.c,42 :: 		}
L_main10:
;L4 P1.c,45 :: 		if (A==1)
	MOVF       R1+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main11
;L4 P1.c,47 :: 		PORTD.RA3=0;
	BCF        PORTD+0, 3
;L4 P1.c,48 :: 		PORTD.RA4=0;
	BCF        PORTD+0, 4
;L4 P1.c,49 :: 		}
L_main11:
;L4 P1.c,50 :: 		if (B==1)
	MOVF       R2+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main12
;L4 P1.c,52 :: 		PORTD.RA3=1;
	BSF        PORTD+0, 3
;L4 P1.c,53 :: 		PORTD.RA4=0;
	BCF        PORTD+0, 4
;L4 P1.c,54 :: 		}
L_main12:
;L4 P1.c,55 :: 		if (C==1)
	MOVF       R3+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main13
;L4 P1.c,57 :: 		PORTD.RA3=0;
	BCF        PORTD+0, 3
;L4 P1.c,58 :: 		PORTD.RA4=1;
	BSF        PORTD+0, 4
;L4 P1.c,59 :: 		}
L_main13:
;L4 P1.c,60 :: 		}
	GOTO       L_main0
;L4 P1.c,61 :: 		}
L_end_main:
	GOTO       $+0
; end of _main
