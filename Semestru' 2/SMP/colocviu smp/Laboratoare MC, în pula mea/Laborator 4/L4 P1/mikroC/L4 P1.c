/*
Sa se realizeze automatizarea unei instalatii de stampat monede
*/

void main() {
     //  sectiunea de declarare a starilor
     short A;
     short B;
     short C;

     //  sectiunea de initializare a marimilor de intrare
     TRISD.RD0=1;
     TRISD.RD1=1;
     TRISD.RD2=1;

     //  sectiunea de initializare a marimilor de iesire
     TRISD.RD3=0;
     TRISD.RD4=0;

     //  sectiunea de initializare a starilor
     A=1;
     B=0;
     C=0;

     while(1)
     {
      //sectiunea de reconfigurare a starilor
      if ((A==1)&&(PORTD.RD0==1))
      {
         A=0;
         B=1;
      }
      if ((B==1)&&(PORTD.RD2==1))
      {
         B=0;
         C=1;
      }
      if ((C==1)&&(PORTD.RD1==1))
      {
         C=0;
         A=1;
      }

      //sectiunea de reconfigurare a iesirilor
      if (A==1)
      {
         PORTD.RA3=0;
         PORTD.RA4=0;
      }
      if (B==1)
      {
         PORTD.RA3=1;
         PORTD.RA4=0;
      }
      if (C==1)
      {
         PORTD.RA3=0;
         PORTD.RA4=1;
      }
     }
}