
_init_ADC:

;L8 P1.c,22 :: 		void init_ADC()
;L8 P1.c,24 :: 		ANSEL = 255;
	MOVLW      255
	MOVWF      ANSEL+0
;L8 P1.c,25 :: 		ADCON0.ADFM = 0;
	BCF        ADCON0+0, 7
;L8 P1.c,26 :: 		ADCON0.VCFG1 = 0;
	BCF        ADCON0+0, 6
;L8 P1.c,27 :: 		ADCON0.VCFG0 = 0;
	BCF        ADCON0+0, 5
;L8 P1.c,28 :: 		ADCON0.CHS2 = 0;
	BCF        ADCON0+0, 4
;L8 P1.c,29 :: 		ADCON0.CHS1 = 0;
	BCF        ADCON0+0, 3
;L8 P1.c,30 :: 		ADCON0.CHS0 = 0;
	BCF        ADCON0+0, 2
;L8 P1.c,31 :: 		ADCON0.GO_DONE= 0;
	BCF        ADCON0+0, 1
;L8 P1.c,32 :: 		ADCON0.ADON = 0;
	BCF        ADCON0+0, 0
;L8 P1.c,33 :: 		ADCON1.ADCS2 = 0;
	BCF        ADCON1+0, 6
;L8 P1.c,34 :: 		ADCON1.ADCS1 = 0;
	BCF        ADCON1+0, 5
;L8 P1.c,35 :: 		ADCON1.ADCS0 = 1;
	BSF        ADCON1+0, 4
;L8 P1.c,36 :: 		TRISA = 255;
	MOVLW      255
	MOVWF      TRISA+0
;L8 P1.c,37 :: 		}
L_end_init_ADC:
	RETURN
; end of _init_ADC

_read_ADC:

;L8 P1.c,39 :: 		int read_ADC()
;L8 P1.c,41 :: 		ADCON0.ADON = 1;
	BSF        ADCON0+0, 0
;L8 P1.c,42 :: 		ADCON0.GO_DONE = 1;
	BSF        ADCON0+0, 1
;L8 P1.c,43 :: 		while (ADCON0.GO_DONE == 1)
L_read_ADC0:
	BTFSS      ADCON0+0, 1
	GOTO       L_read_ADC1
;L8 P1.c,45 :: 		}
	GOTO       L_read_ADC0
L_read_ADC1:
;L8 P1.c,46 :: 		ADCON0.ADON = 0;
	BCF        ADCON0+0, 0
;L8 P1.c,47 :: 		return ADRESH;
	MOVF       ADRESH+0, 0
	MOVWF      R0+0
	CLRF       R0+1
;L8 P1.c,48 :: 		}
L_end_read_ADC:
	RETURN
; end of _read_ADC

_main:

;L8 P1.c,50 :: 		void main() {
;L8 P1.c,51 :: 		int conv = 0;
;L8 P1.c,54 :: 		Lcd_Init();
	CALL       _Lcd_Init+0
;L8 P1.c,55 :: 		Lcd_Cmd(_LCD_CURSOR_OFF);
	MOVLW      12
	MOVWF      FARG_Lcd_Cmd_out_char+0
	CALL       _Lcd_Cmd+0
;L8 P1.c,56 :: 		init_ADC();
	CALL       _init_ADC+0
;L8 P1.c,57 :: 		while(1)
L_main2:
;L8 P1.c,60 :: 		conv = (5*read_ADC()) / 255;    //  afiseaza valoarea intreaga a marimii convertite (0-5V)
	CALL       _read_ADC+0
	MOVLW      5
	MOVWF      R4+0
	MOVLW      0
	MOVWF      R4+1
	CALL       _Mul_16x16_U+0
	MOVLW      255
	MOVWF      R4+0
	CLRF       R4+1
	CALL       _Div_16x16_S+0
;L8 P1.c,61 :: 		sprinti(sir, "Conv = %d  ", conv);
	MOVLW      main_sir_L0+0
	MOVWF      FARG_sprinti_wh+0
	MOVLW      ?lstr_1_L8_32P1+0
	MOVWF      FARG_sprinti_f+0
	MOVLW      hi_addr(?lstr_1_L8_32P1+0)
	MOVWF      FARG_sprinti_f+1
	MOVF       R0+0, 0
	MOVWF      FARG_sprinti_wh+3
	MOVF       R0+1, 0
	MOVWF      FARG_sprinti_wh+4
	CALL       _sprinti+0
;L8 P1.c,62 :: 		Lcd_Out(1, 1, sir);
	MOVLW      1
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      1
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      main_sir_L0+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
;L8 P1.c,63 :: 		Delay_ms(100);
	MOVLW      33
	MOVWF      R12+0
	MOVLW      118
	MOVWF      R13+0
L_main4:
	DECFSZ     R13+0, 1
	GOTO       L_main4
	DECFSZ     R12+0, 1
	GOTO       L_main4
	NOP
;L8 P1.c,64 :: 		}
	GOTO       L_main2
;L8 P1.c,65 :: 		}
L_end_main:
	GOTO       $+0
; end of _main
