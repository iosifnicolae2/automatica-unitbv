
_init_ADC:

;L8 P1.c,20 :: 		void init_ADC()
;L8 P1.c,22 :: 		ANSEL = 255;
	MOVLW      255
	MOVWF      ANSEL+0
;L8 P1.c,23 :: 		ADCON0.ADFM = 0;
	BCF        ADCON0+0, 7
;L8 P1.c,24 :: 		ADCON0.VCFG1 = 0;
	BCF        ADCON0+0, 6
;L8 P1.c,25 :: 		ADCON0.VCFG0 = 0;
	BCF        ADCON0+0, 5
;L8 P1.c,26 :: 		ADCON0.CHS2 = 0;
	BCF        ADCON0+0, 4
;L8 P1.c,27 :: 		ADCON0.CHS1 = 0;
	BCF        ADCON0+0, 3
;L8 P1.c,28 :: 		ADCON0.CHS0 = 0;
	BCF        ADCON0+0, 2
;L8 P1.c,29 :: 		ADCON0.GO_DONE= 0;
	BCF        ADCON0+0, 1
;L8 P1.c,30 :: 		ADCON0.ADON = 0;
	BCF        ADCON0+0, 0
;L8 P1.c,31 :: 		ADCON1.ADCS2 = 0;
	BCF        ADCON1+0, 6
;L8 P1.c,32 :: 		ADCON1.ADCS1 = 0;
	BCF        ADCON1+0, 5
;L8 P1.c,33 :: 		ADCON1.ADCS0 = 1;
	BSF        ADCON1+0, 4
;L8 P1.c,34 :: 		TRISA = 255;
	MOVLW      255
	MOVWF      TRISA+0
;L8 P1.c,35 :: 		}
L_end_init_ADC:
	RETURN
; end of _init_ADC

_read_ADC:

;L8 P1.c,37 :: 		int read_ADC()
;L8 P1.c,40 :: 		ADCON0.ADON = 1;
	BSF        ADCON0+0, 0
;L8 P1.c,41 :: 		ADCON0.GO_DONE = 1;
	BSF        ADCON0+0, 1
;L8 P1.c,42 :: 		while (ADCON0.GO_DONE == 1)
L_read_ADC0:
	BTFSS      ADCON0+0, 1
	GOTO       L_read_ADC1
;L8 P1.c,44 :: 		}
	GOTO       L_read_ADC0
L_read_ADC1:
;L8 P1.c,45 :: 		ADCON0.ADON = 0;
	BCF        ADCON0+0, 0
;L8 P1.c,46 :: 		rez |= ADRESH  << 2 | ADRESL >> 6;
	MOVF       ADRESH+0, 0
	MOVWF      R2+0
	CLRF       R2+1
	RLF        R2+0, 1
	RLF        R2+1, 1
	BCF        R2+0, 0
	RLF        R2+0, 1
	RLF        R2+1, 1
	BCF        R2+0, 0
	MOVLW      6
	MOVWF      R1+0
	MOVF       ADRESL+0, 0
	MOVWF      R0+0
	MOVF       R1+0, 0
L__read_ADC7:
	BTFSC      STATUS+0, 2
	GOTO       L__read_ADC8
	RRF        R0+0, 1
	BCF        R0+0, 7
	ADDLW      255
	GOTO       L__read_ADC7
L__read_ADC8:
	MOVLW      0
	MOVWF      R0+1
	MOVF       R2+0, 0
	IORWF      R0+0, 1
	MOVF       R2+1, 0
	IORWF      R0+1, 1
	MOVF       R4+0, 0
	IORWF      R0+0, 1
	MOVF       R4+1, 0
	IORWF      R0+1, 1
	MOVF       R0+0, 0
	MOVWF      R4+0
	MOVF       R0+1, 0
	MOVWF      R4+1
;L8 P1.c,47 :: 		return rez;
;L8 P1.c,48 :: 		}
L_end_read_ADC:
	RETURN
; end of _read_ADC

_main:

;L8 P1.c,50 :: 		void main() {
;L8 P1.c,51 :: 		int conv = 0;
;L8 P1.c,54 :: 		Lcd_Init();
	CALL       _Lcd_Init+0
;L8 P1.c,55 :: 		Lcd_Cmd(_LCD_CURSOR_OFF);
	MOVLW      12
	MOVWF      FARG_Lcd_Cmd_out_char+0
	CALL       _Lcd_Cmd+0
;L8 P1.c,56 :: 		init_ADC();
	CALL       _init_ADC+0
;L8 P1.c,57 :: 		while(1)
L_main2:
;L8 P1.c,60 :: 		conv = (5*read_ADC()) / 1023;  //  afiseaza valoarea intreaga a marimii convertite (0-5V)
	CALL       _read_ADC+0
	MOVLW      5
	MOVWF      R4+0
	MOVLW      0
	MOVWF      R4+1
	CALL       _Mul_16x16_U+0
	MOVLW      255
	MOVWF      R4+0
	MOVLW      3
	MOVWF      R4+1
	CALL       _Div_16x16_S+0
;L8 P1.c,61 :: 		sprinti(sir, "Conv = %d  ", conv);
	MOVLW      main_sir_L0+0
	MOVWF      FARG_sprinti_wh+0
	MOVLW      ?lstr_1_L8_32P1+0
	MOVWF      FARG_sprinti_f+0
	MOVLW      hi_addr(?lstr_1_L8_32P1+0)
	MOVWF      FARG_sprinti_f+1
	MOVF       R0+0, 0
	MOVWF      FARG_sprinti_wh+3
	MOVF       R0+1, 0
	MOVWF      FARG_sprinti_wh+4
	CALL       _sprinti+0
;L8 P1.c,62 :: 		Lcd_Out(1, 1, sir);
	MOVLW      1
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      1
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      main_sir_L0+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
;L8 P1.c,63 :: 		Delay_ms(100);
	MOVLW      33
	MOVWF      R12+0
	MOVLW      118
	MOVWF      R13+0
L_main4:
	DECFSZ     R13+0, 1
	GOTO       L_main4
	DECFSZ     R12+0, 1
	GOTO       L_main4
	NOP
;L8 P1.c,64 :: 		}
	GOTO       L_main2
;L8 P1.c,65 :: 		}
L_end_main:
	GOTO       $+0
; end of _main
