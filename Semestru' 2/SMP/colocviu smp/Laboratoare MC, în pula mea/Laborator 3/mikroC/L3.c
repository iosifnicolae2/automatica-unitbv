/*
Sa se realizeze comanda unei intersectii semaforizate
*/

void main() {

     char sir[] = {123, 127, 112, 95, 91, 51, 121, 109, 48, 126};       //  echivalentul zecimal al valorilor binare necesare afisajului cu 7 segmente
     int i;
     TRISA = 0;
     TRISB = 0;
     TRISC = 0;
     
     while(1)
     {
       PORTA = 0b00000100;             //  setam semaforul pentru masini ca fiind verde
       PORTB = 0b00000001;             //  setam semaforul pentru pietoni ca fiind rosu
       for (i = 0; i < 10; i++)        //  numaram descrescator de la 9 la 0 pentru culoarea verde a semaforului pentru masini
       {
        PORTC = sir[i];
        Delay_ms(1000);
       }
       PORTC = 0b00000000;             //  stingem afisajul cu 7 segmente cat timp culoarea semaforului pentru masini e galbena
       PORTA = 0b00000010;             //  setam semaforul pentru masini ca fiind galben
       Delay_ms(1000);
       PORTA = 0b00000001;             //  setam semaforul pentru masini ca fiind rosu
       PORTB = 0b00000100;             //  setam semaforul pentru pietoni ca fiind verde
       for (i = 0; i < 10; i++)        //  numaram descrescator de la 9 la 0 pentru culoarea verde a semaforului pentru pietoni
       {
        PORTC = sir[i];
        Delay_ms(1000);
       }
     }
     
     //  sau
      /*
     //  setam PORTA, PORTB si PORTC ca porturi de iesire
     TRISA = 0;
     TRISB = 0;
     TRISC = 0;
     
     while (1)
     {
       PORTA = 0b00000100;   //  setam semaforul pentru masini ca fiind verde
       PORTB = 0b00000001;   //  setam semaforul pentru pietoni ca fiind rosu

       //  numaram descrescator de la 9 la 0 pentru culoarea verde a semaforului pentru masini
       TRISC = 0;
       PORTC = 0b01111011;   //  9
       Delay_ms(1000);
       PORTC = 0b01111111;   //  8
       Delay_ms(1000);
       PORTC = 0b01110000;   //  7
       Delay_ms(1000);
       PORTC = 0b01011111;   //  6
       Delay_ms(1000);
       PORTC = 0b01011011;   //  5
       Delay_ms(1000);
       PORTC = 0b00110011;   //  4
       Delay_ms(1000);
       PORTC = 0b01111001;   //  3
       Delay_ms(1000);
       PORTC = 0b01101101;   //  2
       Delay_ms(1000);
       PORTC = 0b00110000;   //  1
       Delay_ms(1000);
       PORTC = 0b01111110;   //  0
       Delay_ms(1000);
       PORTC = 0b00000000;

       PORTA = 0b00000010;   //  setam semaforul pentru masini ca fiind galben
       Delay_ms(1000);
       PORTA = 0b00000001;   //  setam semaforul pentru masini ca fiind rosu
       PORTB = 0b00000100;   //  setam semaforul pentru pietoni ca fiind verde

       //  numaram descrescator de la 9 la 0 pentru culoarea verde a semaforului pentru pietoni
       TRISC = 0;
       PORTC = 0b01111011;   //  9
       Delay_ms(1000);
       PORTC = 0b01111111;   //  8
       Delay_ms(1000);
       PORTC = 0b01110000;   //  7
       Delay_ms(1000);
       PORTC = 0b01011111;   //  6
       Delay_ms(1000);
       PORTC = 0b01011011;   //  5
       Delay_ms(1000);
       PORTC = 0b00110011;   //  4
       Delay_ms(1000);
       PORTC = 0b01111001;   //  3
       Delay_ms(1000);
       PORTC = 0b01101101;   //  2
       Delay_ms(1000);
       PORTC = 0b00110000;   //  1
       Delay_ms(1000);
       PORTC = 0b01111110;   //  0
       Delay_ms(1000);
     }
     */
}