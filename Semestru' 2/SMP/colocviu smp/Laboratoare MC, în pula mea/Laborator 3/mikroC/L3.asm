
_main:

;L3.c,5 :: 		void main() {
;L3.c,7 :: 		char sir[] = {123, 127, 112, 95, 91, 51, 121, 109, 48, 126};       //  echivalentul zecimal al valorilor binare necesare afisajului cu 7 segmente
	MOVLW      123
	MOVWF      main_sir_L0+0
	MOVLW      127
	MOVWF      main_sir_L0+1
	MOVLW      112
	MOVWF      main_sir_L0+2
	MOVLW      95
	MOVWF      main_sir_L0+3
	MOVLW      91
	MOVWF      main_sir_L0+4
	MOVLW      51
	MOVWF      main_sir_L0+5
	MOVLW      121
	MOVWF      main_sir_L0+6
	MOVLW      109
	MOVWF      main_sir_L0+7
	MOVLW      48
	MOVWF      main_sir_L0+8
	MOVLW      126
	MOVWF      main_sir_L0+9
;L3.c,9 :: 		TRISA = 0;
	CLRF       TRISA+0
;L3.c,10 :: 		TRISB = 0;
	CLRF       TRISB+0
;L3.c,11 :: 		TRISC = 0;
	CLRF       TRISC+0
;L3.c,13 :: 		while(1)
L_main0:
;L3.c,15 :: 		PORTA = 0b00000100;             //  setam semaforul pentru masini ca fiind verde
	MOVLW      4
	MOVWF      PORTA+0
;L3.c,16 :: 		PORTB = 0b00000001;             //  setam semaforul pentru pietoni ca fiind rosu
	MOVLW      1
	MOVWF      PORTB+0
;L3.c,17 :: 		for (i = 0; i < 10; i++)        //  numaram descrescator de la 9 la 0 pentru culoarea verde a semaforului pentru masini
	CLRF       R1+0
	CLRF       R1+1
L_main2:
	MOVLW      128
	XORWF      R1+1, 0
	MOVWF      R0+0
	MOVLW      128
	SUBWF      R0+0, 0
	BTFSS      STATUS+0, 2
	GOTO       L__main12
	MOVLW      10
	SUBWF      R1+0, 0
L__main12:
	BTFSC      STATUS+0, 0
	GOTO       L_main3
;L3.c,19 :: 		PORTC = sir[i];
	MOVF       R1+0, 0
	ADDLW      main_sir_L0+0
	MOVWF      FSR
	MOVF       INDF+0, 0
	MOVWF      PORTC+0
;L3.c,20 :: 		Delay_ms(1000);
	MOVLW      11
	MOVWF      R11+0
	MOVLW      38
	MOVWF      R12+0
	MOVLW      93
	MOVWF      R13+0
L_main5:
	DECFSZ     R13+0, 1
	GOTO       L_main5
	DECFSZ     R12+0, 1
	GOTO       L_main5
	DECFSZ     R11+0, 1
	GOTO       L_main5
	NOP
	NOP
;L3.c,17 :: 		for (i = 0; i < 10; i++)        //  numaram descrescator de la 9 la 0 pentru culoarea verde a semaforului pentru masini
	INCF       R1+0, 1
	BTFSC      STATUS+0, 2
	INCF       R1+1, 1
;L3.c,21 :: 		}
	GOTO       L_main2
L_main3:
;L3.c,22 :: 		PORTC = 0b00000000;             //  stingem afisajul cu 7 segmente cat timp culoarea semaforului pentru masini e galbena
	CLRF       PORTC+0
;L3.c,23 :: 		PORTA = 0b00000010;             //  setam semaforul pentru masini ca fiind galben
	MOVLW      2
	MOVWF      PORTA+0
;L3.c,24 :: 		Delay_ms(1000);
	MOVLW      11
	MOVWF      R11+0
	MOVLW      38
	MOVWF      R12+0
	MOVLW      93
	MOVWF      R13+0
L_main6:
	DECFSZ     R13+0, 1
	GOTO       L_main6
	DECFSZ     R12+0, 1
	GOTO       L_main6
	DECFSZ     R11+0, 1
	GOTO       L_main6
	NOP
	NOP
;L3.c,25 :: 		PORTA = 0b00000001;             //  setam semaforul pentru masini ca fiind rosu
	MOVLW      1
	MOVWF      PORTA+0
;L3.c,26 :: 		PORTB = 0b00000100;             //  setam semaforul pentru pietoni ca fiind verde
	MOVLW      4
	MOVWF      PORTB+0
;L3.c,27 :: 		for (i = 0; i < 10; i++)        //  numaram descrescator de la 9 la 0 pentru culoarea verde a semaforului pentru pietoni
	CLRF       R1+0
	CLRF       R1+1
L_main7:
	MOVLW      128
	XORWF      R1+1, 0
	MOVWF      R0+0
	MOVLW      128
	SUBWF      R0+0, 0
	BTFSS      STATUS+0, 2
	GOTO       L__main13
	MOVLW      10
	SUBWF      R1+0, 0
L__main13:
	BTFSC      STATUS+0, 0
	GOTO       L_main8
;L3.c,29 :: 		PORTC = sir[i];
	MOVF       R1+0, 0
	ADDLW      main_sir_L0+0
	MOVWF      FSR
	MOVF       INDF+0, 0
	MOVWF      PORTC+0
;L3.c,30 :: 		Delay_ms(1000);
	MOVLW      11
	MOVWF      R11+0
	MOVLW      38
	MOVWF      R12+0
	MOVLW      93
	MOVWF      R13+0
L_main10:
	DECFSZ     R13+0, 1
	GOTO       L_main10
	DECFSZ     R12+0, 1
	GOTO       L_main10
	DECFSZ     R11+0, 1
	GOTO       L_main10
	NOP
	NOP
;L3.c,27 :: 		for (i = 0; i < 10; i++)        //  numaram descrescator de la 9 la 0 pentru culoarea verde a semaforului pentru pietoni
	INCF       R1+0, 1
	BTFSC      STATUS+0, 2
	INCF       R1+1, 1
;L3.c,31 :: 		}
	GOTO       L_main7
L_main8:
;L3.c,32 :: 		}
	GOTO       L_main0
;L3.c,99 :: 		}
L_end_main:
	GOTO       $+0
; end of _main
