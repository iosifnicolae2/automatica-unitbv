/*
Sa se realizeze un program care compara o tensiune externa aplicata pe intrarea inversoare cu
o tensiune de 2.5V generata de microcontroler si aprinde un LED in cazul in care tensiunea interna
e mai mare decat tensiunea externa
*/

void init()
{
     CMCON0.C2OUT = 0;
     CMCON0.C1OUT = 0;
     CMCON0.C2INV = 0;
     CMCON0.C1INV = 0;
     CMCON0.CIS = 0;
     CMCON0.CM2 = 0;
     CMCON0.CM1 = 1;
     CMCON0.CM0 = 0;
     TRISB = 0;
     VRCON.VREN = 1;
     VRCON.VRR = 1;
     /*VRCON.VR3 = 0;
     VRCON.VR2 = 1;
     VRCON.VR1 = 0;
     VRCON.VR0 = 0;*/
     VRCON.VR3 = 1;
     VRCON.VR2 = 1;
     VRCON.VR1 = 0;
     VRCON.VR0 = 0;
}

void main() {
     init();
     while (1)
     {
           PORTB.RB0 = CMCON0.C1OUT;
           Delay_ms(100);
     }
}