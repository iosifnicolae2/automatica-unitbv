
_init:

;L9 P3.c,1 :: 		void init()
;L9 P3.c,3 :: 		CMCON0.C2OUT = 0;
	BCF        CMCON0+0, 7
;L9 P3.c,4 :: 		CMCON0.C1OUT = 0;
	BCF        CMCON0+0, 6
;L9 P3.c,5 :: 		CMCON0.C2INV = 0;
	BCF        CMCON0+0, 5
;L9 P3.c,6 :: 		CMCON0.C1INV = 0;
	BCF        CMCON0+0, 4
;L9 P3.c,7 :: 		CMCON0.CIS = 0;
	BCF        CMCON0+0, 3
;L9 P3.c,8 :: 		CMCON0.CM2 = 0;
	BCF        CMCON0+0, 2
;L9 P3.c,9 :: 		CMCON0.CM1 = 1;
	BSF        CMCON0+0, 1
;L9 P3.c,10 :: 		CMCON0.CM0 = 0;
	BCF        CMCON0+0, 0
;L9 P3.c,11 :: 		TRISB = 0;
	CLRF       TRISB+0
;L9 P3.c,12 :: 		VRCON.VREN = 1;
	BSF        VRCON+0, 7
;L9 P3.c,13 :: 		VRCON.VRR = 0;
	BCF        VRCON+0, 5
;L9 P3.c,14 :: 		VRCON.VR3 = 0;
	BCF        VRCON+0, 3
;L9 P3.c,15 :: 		VRCON.VR2 = 0;
	BCF        VRCON+0, 2
;L9 P3.c,16 :: 		VRCON.VR1 = 0;
	BCF        VRCON+0, 1
;L9 P3.c,17 :: 		VRCON.VR0 = 1;
	BSF        VRCON+0, 0
;L9 P3.c,18 :: 		}
L_end_init:
	RETURN
; end of _init

_main:

;L9 P3.c,20 :: 		void main() {
;L9 P3.c,21 :: 		init();
	CALL       _init+0
;L9 P3.c,22 :: 		while (1)
L_main0:
;L9 P3.c,24 :: 		PORTB.RB0 = CMCON0.C1OUT;
	BTFSC      CMCON0+0, 6
	GOTO       L__main5
	BCF        PORTB+0, 0
	GOTO       L__main6
L__main5:
	BSF        PORTB+0, 0
L__main6:
;L9 P3.c,25 :: 		Delay_ms(100);
	MOVLW      2
	MOVWF      R11+0
	MOVLW      4
	MOVWF      R12+0
	MOVLW      186
	MOVWF      R13+0
L_main2:
	DECFSZ     R13+0, 1
	GOTO       L_main2
	DECFSZ     R12+0, 1
	GOTO       L_main2
	DECFSZ     R11+0, 1
	GOTO       L_main2
	NOP
;L9 P3.c,26 :: 		}
	GOTO       L_main0
;L9 P3.c,27 :: 		}
L_end_main:
	GOTO       $+0
; end of _main
