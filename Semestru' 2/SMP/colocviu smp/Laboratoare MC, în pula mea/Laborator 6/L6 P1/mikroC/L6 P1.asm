
_main:

;L6 P1.c,1 :: 		void main()
;L6 P1.c,3 :: 		char sir[10] = {126, 48, 109, 121, 51, 91, 95, 112, 127, 123};  //  valorile zecimale ale combinatiilor binare ce comanda afisarea cifrelor
	MOVLW      126
	MOVWF      main_sir_L0+0
	MOVLW      48
	MOVWF      main_sir_L0+1
	MOVLW      109
	MOVWF      main_sir_L0+2
	MOVLW      121
	MOVWF      main_sir_L0+3
	MOVLW      51
	MOVWF      main_sir_L0+4
	MOVLW      91
	MOVWF      main_sir_L0+5
	MOVLW      95
	MOVWF      main_sir_L0+6
	MOVLW      112
	MOVWF      main_sir_L0+7
	MOVLW      127
	MOVWF      main_sir_L0+8
	MOVLW      123
	MOVWF      main_sir_L0+9
;L6 P1.c,5 :: 		TRISB.RB0 = 1;
	BSF        TRISB+0, 0
;L6 P1.c,6 :: 		TRISD = 0b00000000;
	CLRF       TRISD+0
;L6 P1.c,7 :: 		PORTD= 0;
	CLRF       PORTD+0
;L6 P1.c,9 :: 		i = 0;
	CLRF       R1+0
	CLRF       R1+1
;L6 P1.c,10 :: 		while (1)
L_main0:
;L6 P1.c,12 :: 		if (PORTB.RB0 == 1)
	BTFSS      PORTB+0, 0
	GOTO       L_main2
;L6 P1.c,14 :: 		PORTD = sir[i];
	MOVF       R1+0, 0
	ADDLW      main_sir_L0+0
	MOVWF      FSR
	MOVF       INDF+0, 0
	MOVWF      PORTD+0
;L6 P1.c,15 :: 		i++;
	INCF       R1+0, 1
	BTFSC      STATUS+0, 2
	INCF       R1+1, 1
;L6 P1.c,16 :: 		if (i == 10)
	MOVLW      0
	XORWF      R1+1, 0
	BTFSS      STATUS+0, 2
	GOTO       L__main7
	MOVLW      10
	XORWF      R1+0, 0
L__main7:
	BTFSS      STATUS+0, 2
	GOTO       L_main3
;L6 P1.c,18 :: 		i = 0;
	CLRF       R1+0
	CLRF       R1+1
;L6 P1.c,19 :: 		}
L_main3:
;L6 P1.c,20 :: 		while (PORTB.RB0 == 1)    //  oprim ciclarea microcontrolerului in intervalul de timp in care butonul este apasat
L_main4:
	BTFSS      PORTB+0, 0
	GOTO       L_main5
;L6 P1.c,22 :: 		}
	GOTO       L_main4
L_main5:
;L6 P1.c,23 :: 		}
L_main2:
;L6 P1.c,25 :: 		}
	GOTO       L_main0
;L6 P1.c,26 :: 		}
L_end_main:
	GOTO       $+0
; end of _main
