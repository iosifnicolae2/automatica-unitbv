
_main:

;L6 P2.c,1 :: 		void main()
;L6 P2.c,3 :: 		char sir[10] = {126, 48, 109, 121, 51, 91, 95, 112, 127, 123};
	MOVLW      126
	MOVWF      main_sir_L0+0
	MOVLW      48
	MOVWF      main_sir_L0+1
	MOVLW      109
	MOVWF      main_sir_L0+2
	MOVLW      121
	MOVWF      main_sir_L0+3
	MOVLW      51
	MOVWF      main_sir_L0+4
	MOVLW      91
	MOVWF      main_sir_L0+5
	MOVLW      95
	MOVWF      main_sir_L0+6
	MOVLW      112
	MOVWF      main_sir_L0+7
	MOVLW      127
	MOVWF      main_sir_L0+8
	MOVLW      123
	MOVWF      main_sir_L0+9
	CLRF       main_i_L0+0
	CLRF       main_i_L0+1
;L6 P2.c,5 :: 		TRISB = 1;
	MOVLW      1
	MOVWF      TRISB+0
;L6 P2.c,6 :: 		TRISC = 0;
	CLRF       TRISC+0
;L6 P2.c,7 :: 		TRISD = 0;
	CLRF       TRISD+0
;L6 P2.c,8 :: 		INTCON = 0b10010000;
	MOVLW      144
	MOVWF      INTCON+0
;L6 P2.c,10 :: 		while(1)
L_main0:
;L6 P2.c,12 :: 		PORTD = sir[i];
	MOVF       main_i_L0+0, 0
	ADDLW      main_sir_L0+0
	MOVWF      FSR
	MOVF       INDF+0, 0
	MOVWF      PORTD+0
;L6 P2.c,13 :: 		i++;
	INCF       main_i_L0+0, 1
	BTFSC      STATUS+0, 2
	INCF       main_i_L0+1, 1
;L6 P2.c,14 :: 		if (i == 10)
	MOVLW      0
	XORWF      main_i_L0+1, 0
	BTFSS      STATUS+0, 2
	GOTO       L__main6
	MOVLW      10
	XORWF      main_i_L0+0, 0
L__main6:
	BTFSS      STATUS+0, 2
	GOTO       L_main2
;L6 P2.c,16 :: 		i= 0;
	CLRF       main_i_L0+0
	CLRF       main_i_L0+1
;L6 P2.c,17 :: 		}
L_main2:
;L6 P2.c,18 :: 		Delay_ms(1000);
	MOVLW      11
	MOVWF      R11+0
	MOVLW      38
	MOVWF      R12+0
	MOVLW      93
	MOVWF      R13+0
L_main3:
	DECFSZ     R13+0, 1
	GOTO       L_main3
	DECFSZ     R12+0, 1
	GOTO       L_main3
	DECFSZ     R11+0, 1
	GOTO       L_main3
	NOP
	NOP
;L6 P2.c,19 :: 		}
	GOTO       L_main0
;L6 P2.c,20 :: 		}
L_end_main:
	GOTO       $+0
; end of _main

_interrupt:
	MOVWF      R15+0
	SWAPF      STATUS+0, 0
	CLRF       STATUS+0
	MOVWF      ___saveSTATUS+0
	MOVF       PCLATH+0, 0
	MOVWF      ___savePCLATH+0
	CLRF       PCLATH+0

;L6 P2.c,22 :: 		void interrupt()
;L6 P2.c,24 :: 		if (INTCON.INTF == 1)
	BTFSS      INTCON+0, 1
	GOTO       L_interrupt4
;L6 P2.c,26 :: 		PORTC =! PORTC;
	MOVF       PORTC+0, 0
	MOVLW      1
	BTFSS      STATUS+0, 2
	MOVLW      0
	MOVWF      PORTC+0
;L6 P2.c,27 :: 		INTCON.INTF = 0;
	BCF        INTCON+0, 1
;L6 P2.c,28 :: 		}
L_interrupt4:
;L6 P2.c,29 :: 		}
L_end_interrupt:
L__interrupt8:
	MOVF       ___savePCLATH+0, 0
	MOVWF      PCLATH+0
	SWAPF      ___saveSTATUS+0, 0
	MOVWF      STATUS+0
	SWAPF      R15+0, 1
	SWAPF      R15+0, 0
	RETFIE
; end of _interrupt
