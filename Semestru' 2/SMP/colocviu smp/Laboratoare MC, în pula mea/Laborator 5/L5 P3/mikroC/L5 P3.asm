
_main:

;L5 P3.c,16 :: 		void main() {
;L5 P3.c,25 :: 		A = 1;
	MOVLW      1
	MOVWF      main_A_L0+0
;L5 P3.c,26 :: 		B = 0;
	CLRF       main_B_L0+0
;L5 P3.c,27 :: 		C = 0;
	CLRF       main_C_L0+0
;L5 P3.c,28 :: 		D = 0;
	CLRF       main_D_L0+0
;L5 P3.c,29 :: 		E = 0;
	CLRF       main_E_L0+0
;L5 P3.c,33 :: 		TRISB.RB4 = 0;
	BCF        TRISB+0, 4
;L5 P3.c,34 :: 		TRISB.RB3 = 1;
	BSF        TRISB+0, 3
;L5 P3.c,35 :: 		TRISB.RB2 = 1;
	BSF        TRISB+0, 2
;L5 P3.c,36 :: 		TRISB.RB1 = 1;
	BSF        TRISB+0, 1
;L5 P3.c,37 :: 		TRISB.RB0 = 1;
	BSF        TRISB+0, 0
;L5 P3.c,39 :: 		Lcd_Init();
	CALL       _Lcd_Init+0
;L5 P3.c,40 :: 		Lcd_Cmd(_LCD_CURSOR_OFF);
	MOVLW      12
	MOVWF      FARG_Lcd_Cmd_out_char+0
	CALL       _Lcd_Cmd+0
;L5 P3.c,42 :: 		while(1)
L_main0:
;L5 P3.c,45 :: 		if ((A == 1) && (PORTB.RB2 == 1))
	MOVF       main_A_L0+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main4
	BTFSS      PORTB+0, 2
	GOTO       L_main4
L__main42:
;L5 P3.c,47 :: 		A = 0;
	CLRF       main_A_L0+0
;L5 P3.c,48 :: 		B = 1;
	MOVLW      1
	MOVWF      main_B_L0+0
;L5 P3.c,49 :: 		}
L_main4:
;L5 P3.c,50 :: 		if ((B == 1) && (PORTB.RB2 == 1))
	MOVF       main_B_L0+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main7
	BTFSS      PORTB+0, 2
	GOTO       L_main7
L__main41:
;L5 P3.c,52 :: 		B = 0;
	CLRF       main_B_L0+0
;L5 P3.c,53 :: 		D = 1;
	MOVLW      1
	MOVWF      main_D_L0+0
;L5 P3.c,54 :: 		}
L_main7:
;L5 P3.c,55 :: 		if ((B == 1) && (PORTB.RB3 == 1))
	MOVF       main_B_L0+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main10
	BTFSS      PORTB+0, 3
	GOTO       L_main10
L__main40:
;L5 P3.c,57 :: 		B = 0;
	CLRF       main_B_L0+0
;L5 P3.c,58 :: 		A = 1;
	MOVLW      1
	MOVWF      main_A_L0+0
;L5 P3.c,59 :: 		}
L_main10:
;L5 P3.c,60 :: 		if ((B == 1) && (PORTB.RB1 == 1))
	MOVF       main_B_L0+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main13
	BTFSS      PORTB+0, 1
	GOTO       L_main13
L__main39:
;L5 P3.c,62 :: 		B = 0;
	CLRF       main_B_L0+0
;L5 P3.c,63 :: 		C = 1;
	MOVLW      1
	MOVWF      main_C_L0+0
;L5 P3.c,64 :: 		}
L_main13:
;L5 P3.c,65 :: 		if ((C == 1) && (PORTB.RB2 == 1))
	MOVF       main_C_L0+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main16
	BTFSS      PORTB+0, 2
	GOTO       L_main16
L__main38:
;L5 P3.c,67 :: 		C = 0;
	CLRF       main_C_L0+0
;L5 P3.c,68 :: 		E = 1;
	MOVLW      1
	MOVWF      main_E_L0+0
;L5 P3.c,69 :: 		}
L_main16:
;L5 P3.c,70 :: 		if ((C == 1) && (PORTB.RB3 == 1))
	MOVF       main_C_L0+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main19
	BTFSS      PORTB+0, 3
	GOTO       L_main19
L__main37:
;L5 P3.c,72 :: 		C = 0;
	CLRF       main_C_L0+0
;L5 P3.c,73 :: 		A = 1;
	MOVLW      1
	MOVWF      main_A_L0+0
;L5 P3.c,74 :: 		}
L_main19:
;L5 P3.c,75 :: 		if ((C == 1) && (PORTB.RB0 == 1))
	MOVF       main_C_L0+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main22
	BTFSS      PORTB+0, 0
	GOTO       L_main22
L__main36:
;L5 P3.c,77 :: 		C = 0;
	CLRF       main_C_L0+0
;L5 P3.c,78 :: 		B = 1;
	MOVLW      1
	MOVWF      main_B_L0+0
;L5 P3.c,79 :: 		}
L_main22:
;L5 P3.c,80 :: 		if ((D == 1) && (PORTB.RB2 == 1))
	MOVF       main_D_L0+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main25
	BTFSS      PORTB+0, 2
	GOTO       L_main25
L__main35:
;L5 P3.c,82 :: 		D = 0;
	CLRF       main_D_L0+0
;L5 P3.c,83 :: 		A = 1;
	MOVLW      1
	MOVWF      main_A_L0+0
;L5 P3.c,84 :: 		}
L_main25:
;L5 P3.c,85 :: 		if ((E == 1) && (PORTB.RB2 == 1))
	MOVF       main_E_L0+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main28
	BTFSS      PORTB+0, 2
	GOTO       L_main28
L__main34:
;L5 P3.c,87 :: 		E = 0;
	CLRF       main_E_L0+0
;L5 P3.c,88 :: 		A = 1;
	MOVLW      1
	MOVWF      main_A_L0+0
;L5 P3.c,89 :: 		}
L_main28:
;L5 P3.c,92 :: 		if (A == 1)
	MOVF       main_A_L0+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main29
;L5 P3.c,94 :: 		PORTB.RB4 = 0;
	BCF        PORTB+0, 4
;L5 P3.c,95 :: 		Lcd_Out(1, 1, "Set RB4");
	MOVLW      1
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      1
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      ?lstr1_L5_32P3+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
;L5 P3.c,96 :: 		Lcd_Out(2, 1, "OK");
	MOVLW      2
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      1
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      ?lstr2_L5_32P3+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
;L5 P3.c,97 :: 		}
L_main29:
;L5 P3.c,98 :: 		if (B == 1)
	MOVF       main_B_L0+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main30
;L5 P3.c,100 :: 		PORTB.RB4 = 0;
	BCF        PORTB+0, 4
;L5 P3.c,101 :: 		Lcd_Out(1, 1, "Aprinde LED");
	MOVLW      1
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      1
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      ?lstr3_L5_32P3+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
;L5 P3.c,102 :: 		Lcd_Out(2, 1, "OK|Cancel | >");
	MOVLW      2
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      1
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      ?lstr4_L5_32P3+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
;L5 P3.c,103 :: 		}
L_main30:
;L5 P3.c,104 :: 		if (C == 1)
	MOVF       main_C_L0+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main31
;L5 P3.c,106 :: 		PORTB.RB4 = 0;
	BCF        PORTB+0, 4
;L5 P3.c,107 :: 		Lcd_Out(1, 1, "Stinge LED");
	MOVLW      1
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      1
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      ?lstr5_L5_32P3+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
;L5 P3.c,108 :: 		Lcd_Out(2, 1, "OK|Cancel| < | >");
	MOVLW      2
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      1
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      ?lstr6_L5_32P3+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
;L5 P3.c,109 :: 		}
L_main31:
;L5 P3.c,110 :: 		if (D == 1)
	MOVF       main_D_L0+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main32
;L5 P3.c,112 :: 		Lcd_Out(1, 1, "PORTB.RB4 = 1");
	MOVLW      1
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      1
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      ?lstr7_L5_32P3+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
;L5 P3.c,113 :: 		Lcd_Out(2, 1, "OK");
	MOVLW      2
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      1
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      ?lstr8_L5_32P3+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
;L5 P3.c,114 :: 		}
L_main32:
;L5 P3.c,115 :: 		if (E == 1)
	MOVF       main_E_L0+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main33
;L5 P3.c,118 :: 		PORTB.RB4 = 0;
	BCF        PORTB+0, 4
;L5 P3.c,119 :: 		Lcd_Out(1, 1, "PORTB.RB4 = 0");
	MOVLW      1
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      1
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      ?lstr9_L5_32P3+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
;L5 P3.c,120 :: 		Lcd_Out(2, 1, "OK");
	MOVLW      2
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      1
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      ?lstr10_L5_32P3+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
;L5 P3.c,122 :: 		}
L_main33:
;L5 P3.c,123 :: 		}
	GOTO       L_main0
;L5 P3.c,125 :: 		}
L_end_main:
	GOTO       $+0
; end of _main
