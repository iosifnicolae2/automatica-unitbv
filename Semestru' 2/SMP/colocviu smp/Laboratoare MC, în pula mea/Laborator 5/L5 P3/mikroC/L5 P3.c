//  declaram variabilele globale necesare comunicarii cu ecranul LCD
sbit LCD_EN at RD5_bit;
sbit LCD_RS at RD4_bit;
sbit LCD_D7 at RD3_bit;
sbit LCD_D6 at RD2_bit;
sbit LCD_D5 at RD1_bit;
sbit LCD_D4 at RD0_bit;

sbit LCD_EN_Direction at TRISD5_bit;
sbit LCD_RS_Direction at TRISD4_bit;
sbit LCD_D7_Direction at TRISD3_bit;
sbit LCD_D6_Direction at TRISD2_bit;
sbit LCD_D5_Direction at TRISD1_bit;
sbit LCD_D4_Direction at TRISD0_bit;

void main() {
     //  declararea starilor
     short A;
     short B;
     short C;
     short D;
     short E;
     
     //  initializarea starilor
     A = 1;
     B = 0;
     C = 0;
     D = 0;
     E = 0;
     
     //  configurarea porturilor
     //TRISB = 0b00001111;
     TRISB.RB4 = 0;
     TRISB.RB3 = 1;
     TRISB.RB2 = 1;
     TRISB.RB1 = 1;
     TRISB.RB0 = 1;
     
     Lcd_Init();
     Lcd_Cmd(_LCD_CURSOR_OFF);
     
     while(1)
     {
             //  reconfigurare stari
             if ((A == 1) && (PORTB.RB2 == 1))
             {
                A = 0;
                B = 1;
             }
             if ((B == 1) && (PORTB.RB2 == 1))
             {
                B = 0;
                D = 1;
             }
             if ((B == 1) && (PORTB.RB3 == 1))
             {
                B = 0;
                A = 1;
             }
             if ((B == 1) && (PORTB.RB1 == 1))
             {
                B = 0;
                C = 1;
             }
             if ((C == 1) && (PORTB.RB2 == 1))
             {
                C = 0;
                E = 1;
             }
             if ((C == 1) && (PORTB.RB3 == 1))
             {
                C = 0;
                A = 1;
             }
             if ((C == 1) && (PORTB.RB0 == 1))
             {
                C = 0;
                B = 1;
             }
             if ((D == 1) && (PORTB.RB2 == 1))
             {
                D = 0;
                A = 1;
             }
             if ((E == 1) && (PORTB.RB2 == 1))
             {
                E = 0;
                A = 1;
             }
             
             //  reconfigurare iesiri
             if (A == 1)
             {
                PORTB.RB4 = 0;
                Lcd_Out(1, 1, "Set RB4");
                Lcd_Out(2, 1, "OK");
             }
             if (B == 1)
             {
                PORTB.RB4 = 0;
                Lcd_Out(1, 1, "Aprinde LED");
                Lcd_Out(2, 1, "OK|Cancel | >");
             }
             if (C == 1)
             {
                PORTB.RB4 = 0;
                Lcd_Out(1, 1, "Stinge LED");
                Lcd_Out(2, 1, "OK|Cancel| < | >");
             }
             if (D == 1)
             {
                Lcd_Out(1, 1, "PORTB.RB4 = 1");
                Lcd_Out(2, 1, "OK");
             }
             if (E == 1)
             {

                PORTB.RB4 = 0;
                Lcd_Out(1, 1, "PORTB.RB4 = 0");
                Lcd_Out(2, 1, "OK");
                
             }
     }

}