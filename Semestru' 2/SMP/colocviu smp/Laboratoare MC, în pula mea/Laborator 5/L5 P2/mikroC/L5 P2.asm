
_main:

;L5 P2.c,15 :: 		void main() {
;L5 P2.c,19 :: 		Lcd_Init();
	CALL       _Lcd_Init+0
;L5 P2.c,20 :: 		while(1)
L_main0:
;L5 P2.c,22 :: 		for (i = 0; i < 1000; i++)
	CLRF       main_i_L0+0
	CLRF       main_i_L0+1
L_main2:
	MOVLW      128
	XORWF      main_i_L0+1, 0
	MOVWF      R0+0
	MOVLW      128
	XORLW      3
	SUBWF      R0+0, 0
	BTFSS      STATUS+0, 2
	GOTO       L__main7
	MOVLW      232
	SUBWF      main_i_L0+0, 0
L__main7:
	BTFSC      STATUS+0, 0
	GOTO       L_main3
;L5 P2.c,24 :: 		sprinti(sir, "Timpul = %d min", i);
	MOVLW      main_sir_L0+0
	MOVWF      FARG_sprinti_wh+0
	MOVLW      ?lstr_1_L5_32P2+0
	MOVWF      FARG_sprinti_f+0
	MOVLW      hi_addr(?lstr_1_L5_32P2+0)
	MOVWF      FARG_sprinti_f+1
	MOVF       main_i_L0+0, 0
	MOVWF      FARG_sprinti_wh+3
	MOVF       main_i_L0+1, 0
	MOVWF      FARG_sprinti_wh+4
	CALL       _sprinti+0
;L5 P2.c,25 :: 		Lcd_Out(1, 1, sir);
	MOVLW      1
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      1
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      main_sir_L0+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
;L5 P2.c,26 :: 		Delay_ms(1000);
	MOVLW      11
	MOVWF      R11+0
	MOVLW      38
	MOVWF      R12+0
	MOVLW      93
	MOVWF      R13+0
L_main5:
	DECFSZ     R13+0, 1
	GOTO       L_main5
	DECFSZ     R12+0, 1
	GOTO       L_main5
	DECFSZ     R11+0, 1
	GOTO       L_main5
	NOP
	NOP
;L5 P2.c,22 :: 		for (i = 0; i < 1000; i++)
	INCF       main_i_L0+0, 1
	BTFSC      STATUS+0, 2
	INCF       main_i_L0+1, 1
;L5 P2.c,27 :: 		}
	GOTO       L_main2
L_main3:
;L5 P2.c,28 :: 		}
	GOTO       L_main0
;L5 P2.c,29 :: 		}
L_end_main:
	GOTO       $+0
; end of _main
