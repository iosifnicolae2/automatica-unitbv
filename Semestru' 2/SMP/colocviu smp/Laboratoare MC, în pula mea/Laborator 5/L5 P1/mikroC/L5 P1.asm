
_main:

;L5 P1.c,15 :: 		void main() {
;L5 P1.c,16 :: 		Lcd_Init();
	CALL       _Lcd_Init+0
;L5 P1.c,17 :: 		Lcd_Out(1, 1, "A venit");
	MOVLW      1
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      1
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      ?lstr1_L5_32P1+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
;L5 P1.c,18 :: 		Lcd_Out(2, 1, "primavara!");
	MOVLW      2
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      1
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      ?lstr2_L5_32P1+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
;L5 P1.c,19 :: 		Lcd_Cmd(_LCD_CURSOR_OFF);
	MOVLW      12
	MOVWF      FARG_Lcd_Cmd_out_char+0
	CALL       _Lcd_Cmd+0
;L5 P1.c,20 :: 		}
L_end_main:
	GOTO       $+0
; end of _main
