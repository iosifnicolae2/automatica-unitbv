
_main:

;L2 P4.c,5 :: 		void main() {
;L2 P4.c,7 :: 		char sir[] = {1, 2, 4, 8, 16, 32, 64, 128};        //  declaram un sir cu valorile zecimale succesive ale PORTB
;L2 P4.c,8 :: 		TRISB = 0;                                         //  setam toti bitii PORTB ca fiind biti de iesire
	CLRF       TRISB+0
;L2 P4.c,10 :: 		for (i = 0; i < 8; i++)                            //  intram intr-un ciclu for cu 8 iteratii, cate una pentru fiecare bit al portului
	CLRF       R2+0
	CLRF       R2+1
L_main0:
	MOVLW      128
	XORWF      R2+1, 0
	MOVWF      R0+0
	MOVLW      128
	SUBWF      R0+0, 0
	BTFSS      STATUS+0, 2
	GOTO       L__main5
	MOVLW      8
	SUBWF      R2+0, 0
L__main5:
	BTFSC      STATUS+0, 0
	GOTO       L_main1
;L2 P4.c,12 :: 		PORTB = 1 << i;                                //  la fiecare iteratie shift-am la stanga valoarea 1 cu i pozitii
	MOVF       R2+0, 0
	MOVWF      R1+0
	MOVLW      1
	MOVWF      R0+0
	MOVF       R1+0, 0
L__main6:
	BTFSC      STATUS+0, 2
	GOTO       L__main7
	RLF        R0+0, 1
	BCF        R0+0, 0
	ADDLW      255
	GOTO       L__main6
L__main7:
	MOVF       R0+0, 0
	MOVWF      PORTB+0
;L2 P4.c,13 :: 		Delay_ms(1000);                                //  introducem o intarziere de 1s deoarece trecerea la o noua iteratie a ciclului
	MOVLW      11
	MOVWF      R11+0
	MOVLW      38
	MOVWF      R12+0
	MOVLW      93
	MOVWF      R13+0
L_main3:
	DECFSZ     R13+0, 1
	GOTO       L_main3
	DECFSZ     R12+0, 1
	GOTO       L_main3
	DECFSZ     R11+0, 1
	GOTO       L_main3
	NOP
	NOP
;L2 P4.c,10 :: 		for (i = 0; i < 8; i++)                            //  intram intr-un ciclu for cu 8 iteratii, cate una pentru fiecare bit al portului
	INCF       R2+0, 1
	BTFSC      STATUS+0, 2
	INCF       R2+1, 1
;L2 P4.c,15 :: 		}
	GOTO       L_main0
L_main1:
;L2 P4.c,27 :: 		}
L_end_main:
	GOTO       $+0
; end of _main
