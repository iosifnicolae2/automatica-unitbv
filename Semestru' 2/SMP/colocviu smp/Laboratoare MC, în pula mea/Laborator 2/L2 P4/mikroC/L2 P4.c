/*
Sa se realizeze un program care activeaza pe rand toti bitii portului B
*/

void main() {
     int i;                                             //  variabilele se declara inainte de accesarea porturilor
     char sir[] = {1, 2, 4, 8, 16, 32, 64, 128};        //  declaram un sir cu valorile zecimale succesive ale PORTB
     TRISB = 0;                                         //  setam toti bitii PORTB ca fiind biti de iesire

     for (i = 0; i < 8; i++)                            //  intram intr-un ciclu for cu 8 iteratii, cate una pentru fiecare bit al portului
     {
         PORTB = 1 << i;                                //  la fiecare iteratie shift-am la stanga valoarea 1 cu i pozitii
         Delay_ms(1000);                                //  introducem o intarziere de 1s deoarece trecerea la o noua iteratie a ciclului
                                                        //  se face foarte rapid si altfel nu putem vedea daca LED-ul corespunzator s-a aprins
     }

     /*
     //  sau
     PORTB = 0;                                         //  setam toti bitii PORTB ca fiind 0 logic (nu exista semnal de iesire)
     for (i = 0; i < 8; i++)                            //  intram intr-un ciclu for cu 8 iteratii, cate una pentru fiecare bit al portului
     {
         PORTB = sir[i];                                //  la fiecare iteratie PORTB va lua valoarea zecimala stocata in sir[i]
         Delay_ms(1000);                                //  introducem o intarziere de 1s deoarece trecerea la o noua iteratie a ciclului
                                                        //  se face foarte rapid si altfel nu putem vedea daca LED-ul corespunzator s-a aprins
     }
     */
}