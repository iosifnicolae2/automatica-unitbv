
_main:

;L2 P1.c,5 :: 		void main() {
;L2 P1.c,6 :: 		TRISB = 0;             //  setam toti bitii PORTB ca fiind biti de iesire
	CLRF       TRISB+0
;L2 P1.c,7 :: 		PORTB = 0b00100000;    //  setam bitul 5 din PORTB ca fiind 1 logic (exista semnal de iesire)
	MOVLW      32
	MOVWF      PORTB+0
;L2 P1.c,9 :: 		}
L_end_main:
	GOTO       $+0
; end of _main
