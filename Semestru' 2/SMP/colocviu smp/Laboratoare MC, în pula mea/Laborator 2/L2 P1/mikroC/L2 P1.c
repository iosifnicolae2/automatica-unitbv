/*
Sa se realizeze un program care aprinde un LED
*/

void main() {
     TRISB = 0;             //  setam toti bitii PORTB ca fiind biti de iesire
     PORTB = 0b00100000;    //  setam bitul 5 din PORTB ca fiind 1 logic (exista semnal de iesire)
                            //  sau PORTB = 32;
}