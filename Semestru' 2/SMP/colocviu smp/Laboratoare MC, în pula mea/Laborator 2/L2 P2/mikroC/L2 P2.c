/*
Sa se realizeze un program care aprinde un LED si il stinge dupa o secunda
*/

void main() {
     //  Aprinderea si stingerea LED-ului dupa o secunda
     TRISB = 0;                  //  setam toti bitii PORTB ca fiind biti de iesire
     PORTB = 0b00100000;         //  setam bitul 5 din PORTB ca fiind 1 logic (exista semnal de iesire)
     Delay_ms(1000);             //  introducem o intarziere de 1s
     PORTB = 0;                  //  setam toti bitii PORTB ca fiind 0 logic (nu exista semnal de iesire)
}