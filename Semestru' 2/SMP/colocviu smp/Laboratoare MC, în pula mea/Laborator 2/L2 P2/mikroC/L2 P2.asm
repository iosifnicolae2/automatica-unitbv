
_main:

;L2 P2.c,5 :: 		void main() {
;L2 P2.c,7 :: 		TRISB = 0;                  //  setam toti bitii PORTB ca fiind biti de iesire
	CLRF       TRISB+0
;L2 P2.c,8 :: 		PORTB = 0b00100000;         //  setam bitul 5 din PORTB ca fiind 1 logic (exista semnal de iesire)
	MOVLW      32
	MOVWF      PORTB+0
;L2 P2.c,9 :: 		Delay_ms(1000);             //  introducem o intarziere de 1s
	MOVLW      11
	MOVWF      R11+0
	MOVLW      38
	MOVWF      R12+0
	MOVLW      93
	MOVWF      R13+0
L_main0:
	DECFSZ     R13+0, 1
	GOTO       L_main0
	DECFSZ     R12+0, 1
	GOTO       L_main0
	DECFSZ     R11+0, 1
	GOTO       L_main0
	NOP
	NOP
;L2 P2.c,10 :: 		PORTB = 0;                  //  setam toti bitii PORTB ca fiind 0 logic (nu exista semnal de iesire)
	CLRF       PORTB+0
;L2 P2.c,11 :: 		}
L_end_main:
	GOTO       $+0
; end of _main
