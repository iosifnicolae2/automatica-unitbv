/*
Sa se realizeze un program care aprinde si stinge intermitent un LED la perioade de o secunda
*/

void main() {
     int i = 1;                  //  variabilele se declara inainte de accesarea porturilor
     TRISB = 0;                  //  setam toti bitii PORTB ca fiind biti de iesire
     while (i)                   //  intram intr-un ciclu while infinit
     {                           //  sau while(1)
           PORTB = 0b00100000;   //  setam bitul 5 din PORTB ca fiind 1 logic (exista semnal de iesire)
           Delay_ms(1000);       //  introducem o intarziere de 1s
           PORTB = 0;            //  setam toti bitii PORTB ca fiind 0 logic (nu exista semnal de iesire)
           Delay_ms(1000);       //  introducem o intarziere de 1s deoarece trecerea la o noua iteratie a ciclului while
                                 //  se face foarte rapid si altfel nu putem vedea daca LED-ul s-a stins
     }
}