
_main:

;L2 P3.c,5 :: 		void main() {
;L2 P3.c,6 :: 		int i = 1;                  //  variabilele se declara inainte de accesarea porturilor
	MOVLW      1
	MOVWF      main_i_L0+0
	MOVLW      0
	MOVWF      main_i_L0+1
;L2 P3.c,7 :: 		TRISB = 0;                  //  setam toti bitii PORTB ca fiind biti de iesire
	CLRF       TRISB+0
;L2 P3.c,8 :: 		while (i)                   //  intram intr-un ciclu while infinit
L_main0:
	MOVF       main_i_L0+0, 0
	IORWF      main_i_L0+1, 0
	BTFSC      STATUS+0, 2
	GOTO       L_main1
;L2 P3.c,10 :: 		PORTB = 0b00100000;   //  setam bitul 5 din PORTB ca fiind 1 logic (exista semnal de iesire)
	MOVLW      32
	MOVWF      PORTB+0
;L2 P3.c,11 :: 		Delay_ms(1000);       //  introducem o intarziere de 1s
	MOVLW      11
	MOVWF      R11+0
	MOVLW      38
	MOVWF      R12+0
	MOVLW      93
	MOVWF      R13+0
L_main2:
	DECFSZ     R13+0, 1
	GOTO       L_main2
	DECFSZ     R12+0, 1
	GOTO       L_main2
	DECFSZ     R11+0, 1
	GOTO       L_main2
	NOP
	NOP
;L2 P3.c,12 :: 		PORTB = 0;            //  setam toti bitii PORTB ca fiind 0 logic (nu exista semnal de iesire)
	CLRF       PORTB+0
;L2 P3.c,13 :: 		Delay_ms(1000);       //  introducem o intarziere de 1s deoarece trecerea la o noua iteratie a ciclului while
	MOVLW      11
	MOVWF      R11+0
	MOVLW      38
	MOVWF      R12+0
	MOVLW      93
	MOVWF      R13+0
L_main3:
	DECFSZ     R13+0, 1
	GOTO       L_main3
	DECFSZ     R12+0, 1
	GOTO       L_main3
	DECFSZ     R11+0, 1
	GOTO       L_main3
	NOP
	NOP
;L2 P3.c,15 :: 		}
	GOTO       L_main0
L_main1:
;L2 P3.c,16 :: 		}
L_end_main:
	GOTO       $+0
; end of _main
