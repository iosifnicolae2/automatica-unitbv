/*
Sa se realizeze un program care compara doua tensiuni si aprinde un LED cand
prima tensiune (de pe intrarea neinversoare) e mai mare decat a doua tensiune (de pe intrarea inversoare)
*/

void init()
{
     CMCON0.C2OUT = 0;
     CMCON0.C1OUT = 0;
     CMCON0.C2INV = 0;
     CMCON0.C1INV = 0;
     CMCON0.CIS = 0;
     CMCON0.CM2 = 1;
     CMCON0.CM1 = 0;
     CMCON0.CM0 = 0;
     TRISB = 0;
}

void main() {
     init();
     while (1)
     {
           PORTB.RB0 = CMCON0.C1OUT;
           Delay_ms(100);
     }
}