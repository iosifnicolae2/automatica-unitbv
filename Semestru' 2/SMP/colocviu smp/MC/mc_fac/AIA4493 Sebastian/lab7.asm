
_main:
;lab7.c,21 :: 		void main() {
;lab7.c,23 :: 		TMR0=5;
	MOVLW      5
	MOVWF      TMR0+0
;lab7.c,24 :: 		OPTION_REG=0b00001000;
	MOVLW      8
	MOVWF      OPTION_REG+0
;lab7.c,25 :: 		INTCON=0b11100000;
	MOVLW      224
	MOVWF      INTCON+0
;lab7.c,26 :: 		TRISB=0x00;
	CLRF       TRISB+0
;lab7.c,27 :: 		PORTB=0x00;
	CLRF       PORTB+0
;lab7.c,29 :: 		Lcd_init();
	CALL       _Lcd_Init+0
;lab7.c,30 :: 		Lcd_Cmd(_LCD_CLEAR);
	MOVLW      1
	MOVWF      FARG_Lcd_Cmd_out_char+0
	CALL       _Lcd_Cmd+0
;lab7.c,31 :: 		Lcd_Cmd(_LCD_CURSOR_OFF);
	MOVLW      12
	MOVWF      FARG_Lcd_Cmd_out_char+0
	CALL       _Lcd_Cmd+0
;lab7.c,33 :: 		while(1)
L_main0:
;lab7.c,37 :: 		Lcd_Out(1,1,secunde);
	MOVLW      1
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      1
	MOVWF      FARG_Lcd_Out_column+0
	MOVF       _secunde+0, 0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
;lab7.c,38 :: 		Lcd_Out(1,3,":");
	MOVLW      1
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      3
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      ?lstr1_lab7+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
;lab7.c,39 :: 		Lcd_Out(1,4,minute);
	MOVLW      1
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      4
	MOVWF      FARG_Lcd_Out_column+0
	MOVF       _minute+0, 0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
;lab7.c,40 :: 		Lcd_Out(1,6,":");
	MOVLW      1
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      6
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      ?lstr2_lab7+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
;lab7.c,41 :: 		Lcd_Out(1,7,ore);
	MOVLW      1
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      7
	MOVWF      FARG_Lcd_Out_column+0
	MOVF       _ore+0, 0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
;lab7.c,42 :: 		}
	GOTO       L_main0
;lab7.c,43 :: 		}
	GOTO       $+0
; end of _main

_interrupt:
	MOVWF      R15+0
	SWAPF      STATUS+0, 0
	CLRF       STATUS+0
	MOVWF      ___saveSTATUS+0
	MOVF       PCLATH+0, 0
	MOVWF      ___savePCLATH+0
	CLRF       PCLATH+0
;lab7.c,45 :: 		void interrupt()
;lab7.c,46 :: 		{INTCON.GIE=0;
	BCF        INTCON+0, 7
;lab7.c,47 :: 		if(INTCON.T0IF)
	BTFSS      INTCON+0, 2
	GOTO       L_interrupt2
;lab7.c,49 :: 		INTCON.T0IF=0;
	BCF        INTCON+0, 2
;lab7.c,50 :: 		count++;
	INCF       _count+0, 1
	BTFSC      STATUS+0, 2
	INCF       _count+1, 1
;lab7.c,51 :: 		if(count==1000)
	MOVF       _count+1, 0
	XORLW      3
	BTFSS      STATUS+0, 2
	GOTO       L__interrupt7
	MOVLW      232
	XORWF      _count+0, 0
L__interrupt7:
	BTFSS      STATUS+0, 2
	GOTO       L_interrupt3
;lab7.c,52 :: 		{              secunde++;
	INCF       _secunde+0, 1
	BTFSC      STATUS+0, 2
	INCF       _secunde+1, 1
;lab7.c,53 :: 		count = 0;
	CLRF       _count+0
	CLRF       _count+1
;lab7.c,54 :: 		if(secunde==60)
	MOVLW      0
	XORWF      _secunde+1, 0
	BTFSS      STATUS+0, 2
	GOTO       L__interrupt8
	MOVLW      60
	XORWF      _secunde+0, 0
L__interrupt8:
	BTFSS      STATUS+0, 2
	GOTO       L_interrupt4
;lab7.c,55 :: 		{              minute++;
	INCF       _minute+0, 1
	BTFSC      STATUS+0, 2
	INCF       _minute+1, 1
;lab7.c,56 :: 		secunde = 0;
	CLRF       _secunde+0
	CLRF       _secunde+1
;lab7.c,57 :: 		if(minute==60)
	MOVLW      0
	XORWF      _minute+1, 0
	BTFSS      STATUS+0, 2
	GOTO       L__interrupt9
	MOVLW      60
	XORWF      _minute+0, 0
L__interrupt9:
	BTFSS      STATUS+0, 2
	GOTO       L_interrupt5
;lab7.c,58 :: 		{              ore++;
	INCF       _ore+0, 1
	BTFSC      STATUS+0, 2
	INCF       _ore+1, 1
;lab7.c,59 :: 		minute = 0;
	CLRF       _minute+0
	CLRF       _minute+1
;lab7.c,60 :: 		}
L_interrupt5:
;lab7.c,61 :: 		}
L_interrupt4:
;lab7.c,62 :: 		}
L_interrupt3:
;lab7.c,64 :: 		}
L_interrupt2:
;lab7.c,65 :: 		INTCON.GIE=1;
	BSF        INTCON+0, 7
;lab7.c,66 :: 		}
L__interrupt6:
	MOVF       ___savePCLATH+0, 0
	MOVWF      PCLATH+0
	SWAPF      ___saveSTATUS+0, 0
	MOVWF      STATUS+0
	SWAPF      R15+0, 1
	SWAPF      R15+0, 0
	RETFIE
; end of _interrupt
