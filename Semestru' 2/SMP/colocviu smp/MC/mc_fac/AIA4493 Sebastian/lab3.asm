
_main:
	MOVLW      7
	MOVWF      main_i_L0+0
	MOVLW      0
	MOVWF      main_i_L0+1
;lab3.c,1 :: 		void main() {
;lab3.c,4 :: 		a[0]=0b00111111;
	MOVLW      63
	MOVWF      R1+0
;lab3.c,5 :: 		a[1]=0b00000110;
	MOVLW      6
	MOVWF      R1+1
;lab3.c,6 :: 		a[2]=0b01011011;
	MOVLW      91
	MOVWF      R1+2
;lab3.c,7 :: 		a[3]=0b01001111;
	MOVLW      79
	MOVWF      R1+3
;lab3.c,8 :: 		a[4]=0b01100110;
	MOVLW      102
	MOVWF      R1+4
;lab3.c,9 :: 		a[5]=0b01101101;
	MOVLW      109
	MOVWF      R1+5
;lab3.c,10 :: 		a[6]=0b01111101;
	MOVLW      125
	MOVWF      R1+6
;lab3.c,11 :: 		a[7]=0b00000111;
	MOVLW      7
	MOVWF      R1+7
;lab3.c,12 :: 		a[8]=0b01111111;
	MOVLW      127
	MOVWF      R1+8
;lab3.c,13 :: 		a[9]=0b01101111;
	MOVLW      111
	MOVWF      R1+9
;lab3.c,14 :: 		TRISA=0;
	CLRF       TRISA+0
;lab3.c,15 :: 		TRISB=0b10000000;
	MOVLW      128
	MOVWF      TRISB+0
;lab3.c,16 :: 		TRISC=0;
	CLRF       TRISC+0
;lab3.c,17 :: 		while (1)
L_main0:
;lab3.c,19 :: 		PORTA=0b00001100;
	MOVLW      12
	MOVWF      PORTA+0
;lab3.c,21 :: 		while (i>-1)
L_main2:
	MOVLW      127
	MOVWF      R0+0
	MOVLW      128
	XORWF      main_i_L0+1, 0
	SUBWF      R0+0, 0
	BTFSS      STATUS+0, 2
	GOTO       L__main13
	MOVF       main_i_L0+0, 0
	SUBLW      255
L__main13:
	BTFSC      STATUS+0, 0
	GOTO       L_main3
;lab3.c,23 :: 		PORTB=a[i];
	MOVF       main_i_L0+0, 0
	ADDLW      R1+0
	MOVWF      R0+0
	MOVF       R0+0, 0
	MOVWF      FSR
	MOVF       INDF+0, 0
	MOVWF      PORTB+0
;lab3.c,24 :: 		PORTC=a[i--];
	MOVF       R0+0, 0
	MOVWF      FSR
	MOVF       INDF+0, 0
	MOVWF      PORTC+0
	MOVLW      1
	SUBWF      main_i_L0+0, 1
	BTFSS      STATUS+0, 0
	DECF       main_i_L0+1, 1
;lab3.c,25 :: 		if (i<0)
	MOVLW      128
	XORWF      main_i_L0+1, 0
	MOVWF      R0+0
	MOVLW      128
	SUBWF      R0+0, 0
	BTFSS      STATUS+0, 2
	GOTO       L__main14
	MOVLW      0
	SUBWF      main_i_L0+0, 0
L__main14:
	BTFSC      STATUS+0, 0
	GOTO       L_main4
;lab3.c,26 :: 		break;
	GOTO       L_main3
L_main4:
;lab3.c,27 :: 		if (PORTB.RB7)
	BTFSS      PORTB+0, 7
	GOTO       L_main5
;lab3.c,28 :: 		break;
	GOTO       L_main3
L_main5:
;lab3.c,29 :: 		Delay_ms(1000);
	MOVLW      11
	MOVWF      R11+0
	MOVLW      38
	MOVWF      R12+0
	MOVLW      93
	MOVWF      R13+0
L_main6:
	DECFSZ     R13+0, 1
	GOTO       L_main6
	DECFSZ     R12+0, 1
	GOTO       L_main6
	DECFSZ     R11+0, 1
	GOTO       L_main6
	NOP
	NOP
;lab3.c,30 :: 		}
	GOTO       L_main2
L_main3:
;lab3.c,32 :: 		i=3;
	MOVLW      3
	MOVWF      main_i_L0+0
	MOVLW      0
	MOVWF      main_i_L0+1
;lab3.c,33 :: 		PORTB=a[0];
	MOVF       R1+0, 0
	MOVWF      PORTB+0
;lab3.c,34 :: 		PORTC=a[0];
	MOVF       R1+0, 0
	MOVWF      PORTC+0
;lab3.c,35 :: 		PORTA=0b00010010;
	MOVLW      18
	MOVWF      PORTA+0
;lab3.c,36 :: 		Delay_ms(1000);
	MOVLW      11
	MOVWF      R11+0
	MOVLW      38
	MOVWF      R12+0
	MOVLW      93
	MOVWF      R13+0
L_main7:
	DECFSZ     R13+0, 1
	GOTO       L_main7
	DECFSZ     R12+0, 1
	GOTO       L_main7
	DECFSZ     R11+0, 1
	GOTO       L_main7
	NOP
	NOP
;lab3.c,37 :: 		PORTA=0b00100001;
	MOVLW      33
	MOVWF      PORTA+0
;lab3.c,39 :: 		while (i>-1)
L_main8:
	MOVLW      127
	MOVWF      R0+0
	MOVLW      128
	XORWF      main_i_L0+1, 0
	SUBWF      R0+0, 0
	BTFSS      STATUS+0, 2
	GOTO       L__main15
	MOVF       main_i_L0+0, 0
	SUBLW      255
L__main15:
	BTFSC      STATUS+0, 0
	GOTO       L_main9
;lab3.c,41 :: 		PORTB=a[i];
	MOVF       main_i_L0+0, 0
	ADDLW      R1+0
	MOVWF      R0+0
	MOVF       R0+0, 0
	MOVWF      FSR
	MOVF       INDF+0, 0
	MOVWF      PORTB+0
;lab3.c,42 :: 		PORTC=a[i--];
	MOVF       R0+0, 0
	MOVWF      FSR
	MOVF       INDF+0, 0
	MOVWF      PORTC+0
	MOVLW      1
	SUBWF      main_i_L0+0, 1
	BTFSS      STATUS+0, 0
	DECF       main_i_L0+1, 1
;lab3.c,43 :: 		if (i<0)
	MOVLW      128
	XORWF      main_i_L0+1, 0
	MOVWF      R0+0
	MOVLW      128
	SUBWF      R0+0, 0
	BTFSS      STATUS+0, 2
	GOTO       L__main16
	MOVLW      0
	SUBWF      main_i_L0+0, 0
L__main16:
	BTFSC      STATUS+0, 0
	GOTO       L_main10
;lab3.c,44 :: 		break;
	GOTO       L_main9
L_main10:
;lab3.c,45 :: 		Delay_ms(1000);
	MOVLW      11
	MOVWF      R11+0
	MOVLW      38
	MOVWF      R12+0
	MOVLW      93
	MOVWF      R13+0
L_main11:
	DECFSZ     R13+0, 1
	GOTO       L_main11
	DECFSZ     R12+0, 1
	GOTO       L_main11
	DECFSZ     R11+0, 1
	GOTO       L_main11
	NOP
	NOP
;lab3.c,46 :: 		}
	GOTO       L_main8
L_main9:
;lab3.c,48 :: 		i=7;
	MOVLW      7
	MOVWF      main_i_L0+0
	MOVLW      0
	MOVWF      main_i_L0+1
;lab3.c,49 :: 		PORTA=0b00010010;
	MOVLW      18
	MOVWF      PORTA+0
;lab3.c,50 :: 		Delay_ms(1000);
	MOVLW      11
	MOVWF      R11+0
	MOVLW      38
	MOVWF      R12+0
	MOVLW      93
	MOVWF      R13+0
L_main12:
	DECFSZ     R13+0, 1
	GOTO       L_main12
	DECFSZ     R12+0, 1
	GOTO       L_main12
	DECFSZ     R11+0, 1
	GOTO       L_main12
	NOP
	NOP
;lab3.c,51 :: 		}
	GOTO       L_main0
;lab3.c,52 :: 		}
	GOTO       $+0
; end of _main
