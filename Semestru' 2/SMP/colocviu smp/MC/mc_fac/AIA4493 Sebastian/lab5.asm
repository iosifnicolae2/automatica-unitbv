
_main:
;lab5.c,17 :: 		void main() {
;lab5.c,20 :: 		TRISA=0;
	CLRF       TRISA+0
;lab5.c,21 :: 		PORTA.RA0=0;
	BCF        PORTA+0, 0
;lab5.c,22 :: 		TRISD=255;
	MOVLW      255
	MOVWF      TRISD+0
;lab5.c,23 :: 		a=0,b=1,c=0,d=0,e=0;
	CLRF       main_a_L0+0
	MOVLW      1
	MOVWF      main_b_L0+0
	CLRF       main_c_L0+0
	CLRF       main_d_L0+0
	CLRF       main_e_L0+0
;lab5.c,24 :: 		while (1)
L_main0:
;lab5.c,25 :: 		{   Lcd_Init();
	CALL       _Lcd_Init+0
;lab5.c,26 :: 		if (a==1)
	MOVF       main_a_L0+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main2
;lab5.c,28 :: 		Lcd_Out(1,1,"Seteaza LED");
	MOVLW      1
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      1
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      ?lstr1_lab5+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
;lab5.c,29 :: 		Lcd_Out(2,8,"OK");
	MOVLW      2
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      8
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      ?lstr2_lab5+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
;lab5.c,30 :: 		ceva=1;
	MOVLW      1
	MOVWF      main_ceva_L0+0
	MOVLW      0
	MOVWF      main_ceva_L0+1
;lab5.c,31 :: 		}
L_main2:
;lab5.c,32 :: 		if (b==1)
	MOVF       main_b_L0+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main3
;lab5.c,34 :: 		Lcd_Out(1,1,"Aprinde LED");
	MOVLW      1
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      1
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      ?lstr3_lab5+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
;lab5.c,35 :: 		Lcd_Out(2,1,"OK  CANCEL  >");
	MOVLW      2
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      1
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      ?lstr4_lab5+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
;lab5.c,36 :: 		ceva=1;
	MOVLW      1
	MOVWF      main_ceva_L0+0
	MOVLW      0
	MOVWF      main_ceva_L0+1
;lab5.c,37 :: 		}
L_main3:
;lab5.c,38 :: 		if (c==1)
	MOVF       main_c_L0+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main4
;lab5.c,40 :: 		Lcd_Out(1,1,"Stinge LED");
	MOVLW      1
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      1
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      ?lstr5_lab5+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
;lab5.c,41 :: 		Lcd_Out(2,1,"<  CANCEL  OK");
	MOVLW      2
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      1
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      ?lstr6_lab5+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
;lab5.c,42 :: 		ceva=1;
	MOVLW      1
	MOVWF      main_ceva_L0+0
	MOVLW      0
	MOVWF      main_ceva_L0+1
;lab5.c,43 :: 		}
L_main4:
;lab5.c,44 :: 		if (d==1)
	MOVF       main_d_L0+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main5
;lab5.c,46 :: 		Lcd_Out(1,1,"LED Aprins");
	MOVLW      1
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      1
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      ?lstr7_lab5+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
;lab5.c,47 :: 		Lcd_Out(2,8,"OK");
	MOVLW      2
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      8
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      ?lstr8_lab5+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
;lab5.c,48 :: 		ceva=1;
	MOVLW      1
	MOVWF      main_ceva_L0+0
	MOVLW      0
	MOVWF      main_ceva_L0+1
;lab5.c,49 :: 		}
L_main5:
;lab5.c,50 :: 		if (e==1)
	MOVF       main_e_L0+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main6
;lab5.c,52 :: 		Lcd_Out(1,1,"LED Stins");
	MOVLW      1
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      1
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      ?lstr9_lab5+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
;lab5.c,53 :: 		Lcd_Out(2,8,"OK");
	MOVLW      2
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      8
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      ?lstr10_lab5+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
;lab5.c,54 :: 		ceva=1;
	MOVLW      1
	MOVWF      main_ceva_L0+0
	MOVLW      0
	MOVWF      main_ceva_L0+1
;lab5.c,55 :: 		}
L_main6:
;lab5.c,56 :: 		Lcd_Cmd(_LCD_CURSOR_OFF);
	MOVLW      12
	MOVWF      FARG_Lcd_Cmd_out_char+0
	CALL       _Lcd_Cmd+0
;lab5.c,57 :: 		while (ceva)
L_main7:
	MOVF       main_ceva_L0+0, 0
	IORWF      main_ceva_L0+1, 0
	BTFSC      STATUS+0, 2
	GOTO       L_main8
;lab5.c,59 :: 		if (a==1 && PORTD.RD1==1)   {b=1;a=0;ceva=0;  }
	MOVF       main_a_L0+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main11
	MOVF       PORTD+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main11
L__main44:
	MOVLW      1
	MOVWF      main_b_L0+0
	CLRF       main_a_L0+0
	CLRF       main_ceva_L0+0
	CLRF       main_ceva_L0+1
L_main11:
;lab5.c,60 :: 		if (b==1 && PORTD.RD1==1)   {d=1;b=0;ceva=0;PORTA.RA0=1;  }
	MOVF       main_b_L0+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main14
	MOVF       PORTD+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main14
L__main43:
	MOVLW      1
	MOVWF      main_d_L0+0
	CLRF       main_b_L0+0
	CLRF       main_ceva_L0+0
	CLRF       main_ceva_L0+1
	BSF        PORTA+0, 0
L_main14:
;lab5.c,61 :: 		if (b==1 && PORTD.RD2==1)   {a=1;b=0;ceva=0;  }
	MOVF       main_b_L0+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main17
	MOVF       PORTD+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main17
L__main42:
	MOVLW      1
	MOVWF      main_a_L0+0
	CLRF       main_b_L0+0
	CLRF       main_ceva_L0+0
	CLRF       main_ceva_L0+1
L_main17:
;lab5.c,62 :: 		if (b==1 && PORTD.RD4==1)   {c=1;b=0;ceva=0;  }
	MOVF       main_b_L0+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main20
	MOVF       PORTD+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main20
L__main41:
	MOVLW      1
	MOVWF      main_c_L0+0
	CLRF       main_b_L0+0
	CLRF       main_ceva_L0+0
	CLRF       main_ceva_L0+1
L_main20:
;lab5.c,63 :: 		if (c==1 && PORTD.RD1==1)   {e=1;c=0;ceva=0;PORTA.RA0=0;  }
	MOVF       main_c_L0+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main23
	MOVF       PORTD+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main23
L__main40:
	MOVLW      1
	MOVWF      main_e_L0+0
	CLRF       main_c_L0+0
	CLRF       main_ceva_L0+0
	CLRF       main_ceva_L0+1
	BCF        PORTA+0, 0
L_main23:
;lab5.c,64 :: 		if (c==1 && PORTD.RD2==1)   {a=1;c=0;ceva=0;  }
	MOVF       main_c_L0+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main26
	MOVF       PORTD+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main26
L__main39:
	MOVLW      1
	MOVWF      main_a_L0+0
	CLRF       main_c_L0+0
	CLRF       main_ceva_L0+0
	CLRF       main_ceva_L0+1
L_main26:
;lab5.c,65 :: 		if (c==1 && PORTD.RD3==1)   {b=1;c=0;ceva=0;  }
	MOVF       main_c_L0+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main29
	MOVF       PORTD+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main29
L__main38:
	MOVLW      1
	MOVWF      main_b_L0+0
	CLRF       main_c_L0+0
	CLRF       main_ceva_L0+0
	CLRF       main_ceva_L0+1
L_main29:
;lab5.c,66 :: 		if (d==1 && PORTD.RD1==1)   {a=1;d=0;ceva=0;  }
	MOVF       main_d_L0+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main32
	MOVF       PORTD+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main32
L__main37:
	MOVLW      1
	MOVWF      main_a_L0+0
	CLRF       main_d_L0+0
	CLRF       main_ceva_L0+0
	CLRF       main_ceva_L0+1
L_main32:
;lab5.c,67 :: 		if (e==1 && PORTD.RD1==1)   {a=1;e=0;ceva=0;  }
	MOVF       main_e_L0+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main35
	MOVF       PORTD+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main35
L__main36:
	MOVLW      1
	MOVWF      main_a_L0+0
	CLRF       main_e_L0+0
	CLRF       main_ceva_L0+0
	CLRF       main_ceva_L0+1
L_main35:
;lab5.c,68 :: 		}
	GOTO       L_main7
L_main8:
;lab5.c,71 :: 		Lcd_Cmd(_LCD_CLEAR);
	MOVLW      1
	MOVWF      FARG_Lcd_Cmd_out_char+0
	CALL       _Lcd_Cmd+0
;lab5.c,73 :: 		}
	GOTO       L_main0
;lab5.c,75 :: 		}
	GOTO       $+0
; end of _main
