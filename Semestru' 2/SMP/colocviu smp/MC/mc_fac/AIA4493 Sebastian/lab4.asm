
_main:
;lab4.c,1 :: 		void main() {
;lab4.c,3 :: 		TRISB=0b00000111;
	MOVLW      7
	MOVWF      TRISB+0
;lab4.c,4 :: 		a=1;
	MOVLW      1
	MOVWF      R1+0
;lab4.c,5 :: 		b=0;
	CLRF       R2+0
;lab4.c,6 :: 		c=0;
	CLRF       R3+0
;lab4.c,8 :: 		while (1)
L_main0:
;lab4.c,10 :: 		if ((a==1) && (PORTB.RB0==1))
	MOVF       R1+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main4
	BTFSS      PORTB+0, 0
	GOTO       L_main4
L__main16:
;lab4.c,11 :: 		{a=0;b=1;}
	CLRF       R1+0
	MOVLW      1
	MOVWF      R2+0
L_main4:
;lab4.c,12 :: 		if ((b==1) && (PORTB.RB1==1))
	MOVF       R2+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main7
	BTFSS      PORTB+0, 1
	GOTO       L_main7
L__main15:
;lab4.c,13 :: 		{b=0;c=1;}
	CLRF       R2+0
	MOVLW      1
	MOVWF      R3+0
L_main7:
;lab4.c,14 :: 		if ((c==1) && (PORTB.RB2==1))
	MOVF       R3+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main10
	BTFSS      PORTB+0, 2
	GOTO       L_main10
L__main14:
;lab4.c,15 :: 		{c=0;a=1;}
	CLRF       R3+0
	MOVLW      1
	MOVWF      R1+0
L_main10:
;lab4.c,17 :: 		if (a==1)
	MOVF       R1+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main11
;lab4.c,18 :: 		{PORTB.RB0=0;PORTB.RB1=0;}
	BCF        PORTB+0, 0
	BCF        PORTB+0, 1
L_main11:
;lab4.c,19 :: 		if (b==1)
	MOVF       R2+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main12
;lab4.c,20 :: 		{PORTB.RB0=1;PORTB.RB1=0;}
	BSF        PORTB+0, 0
	BCF        PORTB+0, 1
L_main12:
;lab4.c,21 :: 		if (c==1)
	MOVF       R3+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main13
;lab4.c,22 :: 		{PORTB.RB0=0;PORTB.RB1=1;}
	BCF        PORTB+0, 0
	BSF        PORTB+0, 1
L_main13:
;lab4.c,23 :: 		}
	GOTO       L_main0
;lab4.c,25 :: 		}
	GOTO       $+0
; end of _main
