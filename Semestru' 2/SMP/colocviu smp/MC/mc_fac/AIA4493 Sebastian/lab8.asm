
_init_ADC:
;lab8.c,17 :: 		void init_ADC()
;lab8.c,19 :: 		ANSEL=255;
	MOVLW      255
	MOVWF      ANSEL+0
;lab8.c,20 :: 		ADCON0.ADFM=0;
	BCF        ADCON0+0, 7
;lab8.c,21 :: 		ADCON0.VCFG1=0;
	BCF        ADCON0+0, 6
;lab8.c,22 :: 		ADCON0.VCFG0=0;
	BCF        ADCON0+0, 5
;lab8.c,23 :: 		ADCON0.CHS2=0;
	BCF        ADCON0+0, 4
;lab8.c,24 :: 		ADCON0.CHS1=0;
	BCF        ADCON0+0, 3
;lab8.c,25 :: 		ADCON0.CHS0=0;
	BCF        ADCON0+0, 2
;lab8.c,26 :: 		ADCON0.GO_DONE=0;
	BCF        ADCON0+0, 1
;lab8.c,27 :: 		ADCON0.ADON=0;
	BCF        ADCON0+0, 0
;lab8.c,28 :: 		ADCON1.ADCS2=0;
	BCF        ADCON1+0, 6
;lab8.c,29 :: 		ADCON1.ADCS1=0;
	BCF        ADCON1+0, 5
;lab8.c,30 :: 		ADCON1.ADCS0=1;
	BSF        ADCON1+0, 4
;lab8.c,31 :: 		TRISA=255;
	MOVLW      255
	MOVWF      TRISA+0
;lab8.c,32 :: 		}
	RETURN
; end of _init_ADC

_READ_ADC:
;lab8.c,33 :: 		int READ_ADC()
;lab8.c,35 :: 		ADCON0.ADON=1;
	BSF        ADCON0+0, 0
;lab8.c,36 :: 		ADCON0.GO_DONE=1;
	BSF        ADCON0+0, 1
;lab8.c,37 :: 		while (ADCON0.GO_DONE==1)
L_READ_ADC0:
	BTFSS      ADCON0+0, 1
	GOTO       L_READ_ADC1
;lab8.c,38 :: 		ADCON0.ADON=0;
	BCF        ADCON0+0, 0
	GOTO       L_READ_ADC0
L_READ_ADC1:
;lab8.c,39 :: 		rez|=ADRESH<<2|ADRESL>>6;
	MOVF       ADRESH+0, 0
	MOVWF      R2+0
	CLRF       R2+1
	RLF        R2+0, 1
	RLF        R2+1, 1
	BCF        R2+0, 0
	RLF        R2+0, 1
	RLF        R2+1, 1
	BCF        R2+0, 0
	MOVLW      6
	MOVWF      R1+0
	MOVF       ADRESL+0, 0
	MOVWF      R0+0
	MOVF       R1+0, 0
L__READ_ADC5:
	BTFSC      STATUS+0, 2
	GOTO       L__READ_ADC6
	RRF        R0+0, 1
	BCF        R0+0, 7
	ADDLW      255
	GOTO       L__READ_ADC5
L__READ_ADC6:
	MOVLW      0
	MOVWF      R0+1
	MOVF       R2+0, 0
	IORWF      R0+0, 1
	MOVF       R2+1, 0
	IORWF      R0+1, 1
	MOVF       R4+0, 0
	IORWF      R0+0, 1
	MOVF       R4+1, 0
	IORWF      R0+1, 1
	MOVF       R0+0, 0
	MOVWF      R4+0
	MOVF       R0+1, 0
	MOVWF      R4+1
;lab8.c,40 :: 		return rez;
;lab8.c,41 :: 		}
	RETURN
; end of _READ_ADC

_main:
	CLRF       main_rest_L0+0
	CLRF       main_rest_L0+1
	CLRF       main_rest_L0+2
	CLRF       main_rest_L0+3
	CLRF       main_intreg_L0+0
	CLRF       main_intreg_L0+1
	CLRF       main_intreg_L0+2
	CLRF       main_intreg_L0+3
	CLRF       main_conv_L0+0
	CLRF       main_conv_L0+1
;lab8.c,43 :: 		void main() {
;lab8.c,48 :: 		LCD_INIT();
	CALL       _Lcd_Init+0
;lab8.c,49 :: 		init_ADC();
	CALL       _init_ADC+0
;lab8.c,50 :: 		while(1)
L_main2:
;lab8.c,51 :: 		{intreg=conv*x/1000;
	MOVF       main_conv_L0+0, 0
	MOVWF      R0+0
	MOVF       main_conv_L0+1, 0
	MOVWF      R0+1
	RLF        R0+0, 1
	RLF        R0+1, 1
	BCF        R0+0, 0
	RLF        R0+0, 1
	RLF        R0+1, 1
	BCF        R0+0, 0
	MOVLW      232
	MOVWF      R4+0
	MOVLW      3
	MOVWF      R4+1
	CALL       _Div_16x16_S+0
	CALL       _Int2Double+0
	MOVF       R0+0, 0
	MOVWF      main_intreg_L0+0
	MOVF       R0+1, 0
	MOVWF      main_intreg_L0+1
	MOVF       R0+2, 0
	MOVWF      main_intreg_L0+2
	MOVF       R0+3, 0
	MOVWF      main_intreg_L0+3
;lab8.c,52 :: 		rest=intreg%1000;
	MOVLW      0
	ANDWF      R0+0, 1
	CLRF       R0+1
	ANDWF      R0+1, 1
	MOVF       R0+0, 0
	MOVWF      main_rest_L0+0
	MOVF       R0+1, 0
	MOVWF      main_rest_L0+1
	MOVF       R0+2, 0
	MOVWF      main_rest_L0+2
	MOVF       R0+3, 0
	MOVWF      main_rest_L0+3
;lab8.c,53 :: 		conv=READ_ADC();
	CALL       _READ_ADC+0
	MOVF       R0+0, 0
	MOVWF      main_conv_L0+0
	MOVF       R0+1, 0
	MOVWF      main_conv_L0+1
;lab8.c,54 :: 		sprinti(sir1,"conv=%d ",conv);
	MOVLW      main_sir1_L0+0
	MOVWF      FARG_sprinti_wh+0
	MOVLW      ?lstr_1_lab8+0
	MOVWF      FARG_sprinti_f+0
	MOVLW      hi_addr(?lstr_1_lab8+0
	MOVWF      FARG_sprinti_f+1
	MOVF       R0+0, 0
	MOVWF      FARG_sprinti_wh+3
	MOVF       R0+1, 0
	MOVWF      FARG_sprinti_wh+4
	CALL       _sprinti+0
;lab8.c,55 :: 		LCD_Out(1,1,sir1);
	MOVLW      1
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      1
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      main_sir1_L0+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
;lab8.c,56 :: 		sprinti(sir2,"tens=%d,%d V",intreg,rest);
	MOVLW      main_sir2_L0+0
	MOVWF      FARG_sprinti_wh+0
	MOVLW      ?lstr_2_lab8+0
	MOVWF      FARG_sprinti_f+0
	MOVLW      hi_addr(?lstr_2_lab8+0
	MOVWF      FARG_sprinti_f+1
	MOVF       main_intreg_L0+0, 0
	MOVWF      FARG_sprinti_wh+3
	MOVF       main_intreg_L0+1, 0
	MOVWF      FARG_sprinti_wh+4
	MOVF       main_intreg_L0+2, 0
	MOVWF      FARG_sprinti_wh+5
	MOVF       main_intreg_L0+3, 0
	MOVWF      FARG_sprinti_wh+6
	MOVF       main_rest_L0+0, 0
	MOVWF      FARG_sprinti_wh+7
	MOVF       main_rest_L0+1, 0
	MOVWF      FARG_sprinti_wh+8
	MOVF       main_rest_L0+2, 0
	MOVWF      FARG_sprinti_wh+9
	MOVF       main_rest_L0+3, 0
	MOVWF      FARG_sprinti_wh+10
	CALL       _sprinti+0
;lab8.c,57 :: 		LCD_Out(2,1,sir2);
	MOVLW      2
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      1
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      main_sir2_L0+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
;lab8.c,58 :: 		Delay_ms(1000);
	MOVLW      2
	MOVWF      R11+0
	MOVLW      69
	MOVWF      R12+0
	MOVLW      169
	MOVWF      R13+0
L_main4:
	DECFSZ     R13+0, 1
	GOTO       L_main4
	DECFSZ     R12+0, 1
	GOTO       L_main4
	DECFSZ     R11+0, 1
	GOTO       L_main4
	NOP
	NOP
;lab8.c,59 :: 		}
	GOTO       L_main2
;lab8.c,61 :: 		}
	GOTO       $+0
; end of _main
