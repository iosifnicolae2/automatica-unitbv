
_main:
;laboratorul9.c,5 :: 		void main() {
;laboratorul9.c,7 :: 		CMCON0 = 0b00000010;
	MOVLW      2
	MOVWF      CMCON0+0
;laboratorul9.c,8 :: 		VRCON = 0b10100111;
	MOVLW      167
	MOVWF      VRCON+0
;laboratorul9.c,10 :: 		PIE2.C1IE = 1;
	BSF        PIE2+0, 5
;laboratorul9.c,11 :: 		INTCON.PEIE = 1;
	BSF        INTCON+0, 6
;laboratorul9.c,12 :: 		INTCON.GIE = 1;
	BSF        INTCON+0, 7
;laboratorul9.c,14 :: 		TRISD.RD0 = 1;
	BSF        TRISD+0, 0
;laboratorul9.c,15 :: 		TRISD.RD1 = 1;
	BSF        TRISD+0, 1
;laboratorul9.c,16 :: 		TRISD.RD2 = 0;
	BCF        TRISD+0, 2
;laboratorul9.c,18 :: 		PORTD.RD2 = 0;
	BCF        PORTD+0, 2
;laboratorul9.c,20 :: 		if(PORTD.RD0)
	BTFSS      PORTD+0, 0
	GOTO       L_main0
;laboratorul9.c,21 :: 		stare1 =1;
	BSF        _stare1+0, BitPos(_stare1+0)
L_main0:
;laboratorul9.c,22 :: 		if(stare1 == 1 && PORTD.RD0 == 0)
	BTFSS      _stare1+0, BitPos(_stare1+0)
	GOTO       L_main3
	MOVF       PORTD+0, 0
	XORLW      0
	BTFSS      STATUS+0, 2
	GOTO       L_main3
L__main16:
;laboratorul9.c,24 :: 		if(voltaj +=0.2 <= 1.7)
	MOVF       _voltaj+0, 0
	MOVWF      R0+0
	MOVF       _voltaj+1, 0
	MOVWF      R0+1
	MOVF       _voltaj+2, 0
	MOVWF      R0+2
	MOVF       _voltaj+3, 0
	MOVWF      R0+3
	MOVLW      0
	MOVWF      R4+0
	MOVLW      0
	MOVWF      R4+1
	MOVLW      0
	MOVWF      R4+2
	MOVLW      127
	MOVWF      R4+3
	CALL       _Add_32x32_FP+0
	MOVF       R0+0, 0
	MOVWF      _voltaj+0
	MOVF       R0+1, 0
	MOVWF      _voltaj+1
	MOVF       R0+2, 0
	MOVWF      _voltaj+2
	MOVF       R0+3, 0
	MOVWF      _voltaj+3
	MOVF       _voltaj+0, 0
	IORWF      _voltaj+1, 0
	IORWF      _voltaj+2, 0
	IORWF      _voltaj+3, 0
	BTFSC      STATUS+0, 2
	GOTO       L_main4
;laboratorul9.c,25 :: 		voltaj+=0.2;
	MOVF       _voltaj+0, 0
	MOVWF      R0+0
	MOVF       _voltaj+1, 0
	MOVWF      R0+1
	MOVF       _voltaj+2, 0
	MOVWF      R0+2
	MOVF       _voltaj+3, 0
	MOVWF      R0+3
	MOVLW      205
	MOVWF      R4+0
	MOVLW      204
	MOVWF      R4+1
	MOVLW      76
	MOVWF      R4+2
	MOVLW      124
	MOVWF      R4+3
	CALL       _Add_32x32_FP+0
	MOVF       R0+0, 0
	MOVWF      _voltaj+0
	MOVF       R0+1, 0
	MOVWF      _voltaj+1
	MOVF       R0+2, 0
	MOVWF      _voltaj+2
	MOVF       R0+3, 0
	MOVWF      _voltaj+3
L_main4:
;laboratorul9.c,26 :: 		VRCON= 150 + (int) (voltaj*24/5);
	MOVF       _voltaj+0, 0
	MOVWF      R0+0
	MOVF       _voltaj+1, 0
	MOVWF      R0+1
	MOVF       _voltaj+2, 0
	MOVWF      R0+2
	MOVF       _voltaj+3, 0
	MOVWF      R0+3
	MOVLW      0
	MOVWF      R4+0
	MOVLW      0
	MOVWF      R4+1
	MOVLW      64
	MOVWF      R4+2
	MOVLW      131
	MOVWF      R4+3
	CALL       _Mul_32x32_FP+0
	MOVLW      0
	MOVWF      R4+0
	MOVLW      0
	MOVWF      R4+1
	MOVLW      32
	MOVWF      R4+2
	MOVLW      129
	MOVWF      R4+3
	CALL       _Div_32x32_FP+0
	CALL       _Double2Int+0
	MOVF       R0+0, 0
	ADDLW      150
	MOVWF      VRCON+0
;laboratorul9.c,28 :: 		stare1 = 0;
	BCF        _stare1+0, BitPos(_stare1+0)
;laboratorul9.c,29 :: 		}
L_main3:
;laboratorul9.c,30 :: 		if(PORTD.RD1)
	BTFSS      PORTD+0, 1
	GOTO       L_main5
;laboratorul9.c,31 :: 		stare2 =1;
	BSF        _stare2+0, BitPos(_stare2+0)
L_main5:
;laboratorul9.c,32 :: 		if(stare2 == 1 && PORTD.RD1 == 0)
	BTFSS      _stare2+0, BitPos(_stare2+0)
	GOTO       L_main8
	MOVF       PORTD+0, 0
	XORLW      0
	BTFSS      STATUS+0, 2
	GOTO       L_main8
L__main15:
;laboratorul9.c,34 :: 		if(voltaj -=0.2 >0)
	MOVLW      0
	MOVWF      R4+0
	MOVLW      0
	MOVWF      R4+1
	MOVLW      0
	MOVWF      R4+2
	MOVLW      127
	MOVWF      R4+3
	MOVF       _voltaj+0, 0
	MOVWF      R0+0
	MOVF       _voltaj+1, 0
	MOVWF      R0+1
	MOVF       _voltaj+2, 0
	MOVWF      R0+2
	MOVF       _voltaj+3, 0
	MOVWF      R0+3
	CALL       _Sub_32x32_FP+0
	MOVF       R0+0, 0
	MOVWF      _voltaj+0
	MOVF       R0+1, 0
	MOVWF      _voltaj+1
	MOVF       R0+2, 0
	MOVWF      _voltaj+2
	MOVF       R0+3, 0
	MOVWF      _voltaj+3
	MOVF       _voltaj+0, 0
	IORWF      _voltaj+1, 0
	IORWF      _voltaj+2, 0
	IORWF      _voltaj+3, 0
	BTFSC      STATUS+0, 2
	GOTO       L_main9
;laboratorul9.c,35 :: 		voltaj -=0.2;
	MOVLW      205
	MOVWF      R4+0
	MOVLW      204
	MOVWF      R4+1
	MOVLW      76
	MOVWF      R4+2
	MOVLW      124
	MOVWF      R4+3
	MOVF       _voltaj+0, 0
	MOVWF      R0+0
	MOVF       _voltaj+1, 0
	MOVWF      R0+1
	MOVF       _voltaj+2, 0
	MOVWF      R0+2
	MOVF       _voltaj+3, 0
	MOVWF      R0+3
	CALL       _Sub_32x32_FP+0
	MOVF       R0+0, 0
	MOVWF      _voltaj+0
	MOVF       R0+1, 0
	MOVWF      _voltaj+1
	MOVF       R0+2, 0
	MOVWF      _voltaj+2
	MOVF       R0+3, 0
	MOVWF      _voltaj+3
L_main9:
;laboratorul9.c,36 :: 		VRCON= 150 + (int) (voltaj*24/5);
	MOVF       _voltaj+0, 0
	MOVWF      R0+0
	MOVF       _voltaj+1, 0
	MOVWF      R0+1
	MOVF       _voltaj+2, 0
	MOVWF      R0+2
	MOVF       _voltaj+3, 0
	MOVWF      R0+3
	MOVLW      0
	MOVWF      R4+0
	MOVLW      0
	MOVWF      R4+1
	MOVLW      64
	MOVWF      R4+2
	MOVLW      131
	MOVWF      R4+3
	CALL       _Mul_32x32_FP+0
	MOVLW      0
	MOVWF      R4+0
	MOVLW      0
	MOVWF      R4+1
	MOVLW      32
	MOVWF      R4+2
	MOVLW      129
	MOVWF      R4+3
	CALL       _Div_32x32_FP+0
	CALL       _Double2Int+0
	MOVF       R0+0, 0
	ADDLW      150
	MOVWF      VRCON+0
;laboratorul9.c,38 :: 		stare2 = 0;
	BCF        _stare2+0, BitPos(_stare2+0)
;laboratorul9.c,39 :: 		}
L_main8:
;laboratorul9.c,41 :: 		while(1)
L_main10:
;laboratorul9.c,43 :: 		if(CMCON0.C1OUT)
	BTFSS      CMCON0+0, 6
	GOTO       L_main12
;laboratorul9.c,44 :: 		PORTD.RD2 = 0;
	BCF        PORTD+0, 2
	GOTO       L_main13
L_main12:
;laboratorul9.c,46 :: 		PORTD.RD2 = 1;
	BSF        PORTD+0, 2
L_main13:
;laboratorul9.c,47 :: 		}
	GOTO       L_main10
;laboratorul9.c,48 :: 		}
	GOTO       $+0
; end of _main

_interrupt:
	MOVWF      R15+0
	SWAPF      STATUS+0, 0
	CLRF       STATUS+0
	MOVWF      ___saveSTATUS+0
	MOVF       PCLATH+0, 0
	MOVWF      ___savePCLATH+0
	CLRF       PCLATH+0
;laboratorul9.c,49 :: 		void interrupt()
;laboratorul9.c,51 :: 		INTCON.GIE = 0;
	BCF        INTCON+0, 7
;laboratorul9.c,52 :: 		if(PIE2.C1IF)
	BTFSS      PIE2+0, 5
	GOTO       L_interrupt14
;laboratorul9.c,54 :: 		PORTD.RD2 = ~PORTD.RD2;
	MOVLW      4
	XORWF      PORTD+0, 1
;laboratorul9.c,55 :: 		PIE2.C1IF = 0;
	BCF        PIE2+0, 5
;laboratorul9.c,56 :: 		}
L_interrupt14:
;laboratorul9.c,57 :: 		INTCON.GIE = 1;
	BSF        INTCON+0, 7
;laboratorul9.c,58 :: 		}
L__interrupt17:
	MOVF       ___savePCLATH+0, 0
	MOVWF      PCLATH+0
	SWAPF      ___saveSTATUS+0, 0
	MOVWF      STATUS+0
	SWAPF      R15+0, 1
	SWAPF      R15+0, 0
	RETFIE
; end of _interrupt
