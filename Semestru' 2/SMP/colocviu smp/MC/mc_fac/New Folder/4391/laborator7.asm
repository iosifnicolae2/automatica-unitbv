
_interrupt:
	MOVWF      R15+0
	SWAPF      STATUS+0, 0
	CLRF       STATUS+0
	MOVWF      ___saveSTATUS+0
	MOVF       PCLATH+0, 0
	MOVWF      ___savePCLATH+0
	CLRF       PCLATH+0
;laborator7.c,15 :: 		void interrupt(){
;laborator7.c,16 :: 		if(TMR0IF_bit){
	BTFSS      TMR0IF_bit+0, 2
	GOTO       L_interrupt0
;laborator7.c,17 :: 		TMR0=0;
	CLRF       TMR0+0
;laborator7.c,18 :: 		PORTB.RB0=~PORTB.RB0;
	MOVLW      1
	XORWF      PORTB+0, 1
;laborator7.c,19 :: 		TMR0IF_bit=0;
	BCF        TMR0IF_bit+0, 2
;laborator7.c,20 :: 		}
L_interrupt0:
;laborator7.c,21 :: 		}
L__interrupt3:
	MOVF       ___savePCLATH+0, 0
	MOVWF      PCLATH+0
	SWAPF      ___saveSTATUS+0, 0
	MOVWF      STATUS+0
	SWAPF      R15+0, 1
	SWAPF      R15+0, 0
	RETFIE
; end of _interrupt

_main:
;laborator7.c,23 :: 		void main() {
;laborator7.c,25 :: 		INTCON.GIE=1;
	BSF        INTCON+0, 7
;laborator7.c,26 :: 		INTCON.T0IE=1;
	BSF        INTCON+0, 5
;laborator7.c,28 :: 		TMR0=0;
	CLRF       TMR0+0
;laborator7.c,29 :: 		OPTION_REG=0b0000110;
	MOVLW      6
	MOVWF      OPTION_REG+0
;laborator7.c,31 :: 		TRISB.RB0=0;
	BCF        TRISB+0, 0
;laborator7.c,32 :: 		PORTB.RB0=1;
	BSF        PORTB+0, 0
;laborator7.c,34 :: 		while(1)
L_main1:
;laborator7.c,36 :: 		}
	GOTO       L_main1
;laborator7.c,37 :: 		}
	GOTO       $+0
; end of _main
