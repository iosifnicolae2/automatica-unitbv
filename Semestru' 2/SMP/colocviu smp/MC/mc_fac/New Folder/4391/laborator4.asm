
_main:
;laborator4.c,21 :: 		void main() {
;laborator4.c,23 :: 		trisb.rb0=1;
	BSF        TRISB+0, 0
;laborator4.c,24 :: 		trisb.rb1=1;
	BSF        TRISB+0, 1
;laborator4.c,25 :: 		trisb.rb2=1;
	BSF        TRISB+0, 2
;laborator4.c,26 :: 		trisb.rb3=1;
	BSF        TRISB+0, 3
;laborator4.c,27 :: 		trisb.rb4=0;
	BCF        TRISB+0, 4
;laborator4.c,29 :: 		A=1;
	MOVLW      1
	MOVWF      _A+0
;laborator4.c,30 :: 		B=0;
	CLRF       _B+0
;laborator4.c,36 :: 		Lcd_init();
	CALL       _Lcd_Init+0
;laborator4.c,37 :: 		Lcd_out(1,4,"Set RB4");
	MOVLW      1
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      4
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      ?lstr1_laborator4+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
;laborator4.c,38 :: 		Lcd_out(2,7,"Ok");
	MOVLW      2
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      7
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      ?lstr2_laborator4+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
;laborator4.c,40 :: 		while(1)
L_main0:
;laborator4.c,42 :: 		if((A==1)&&(portb.rb0==1)) {A=0;B=1;
	MOVF       _A+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main4
	BTFSS      PORTB+0, 0
	GOTO       L_main4
L__main5:
	CLRF       _A+0
	MOVLW      1
	MOVWF      _B+0
;laborator4.c,43 :: 		Lcd_Cmd(_LCD_CLEAR);
	MOVLW      1
	MOVWF      FARG_Lcd_Cmd_out_char+0
	CALL       _Lcd_Cmd+0
;laborator4.c,44 :: 		Lcd_out(1,2,"Aprinde RB4");
	MOVLW      1
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      2
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      ?lstr3_laborator4+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
;laborator4.c,45 :: 		Lcd_out(2,2,"Ok Cancel >");
	MOVLW      2
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      2
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      ?lstr4_laborator4+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
;laborator4.c,46 :: 		}
L_main4:
;laborator4.c,60 :: 		}
	GOTO       L_main0
;laborator4.c,61 :: 		}
	GOTO       $+0
; end of _main
