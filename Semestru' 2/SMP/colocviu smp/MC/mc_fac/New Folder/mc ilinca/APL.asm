
_interrupt:
	MOVWF      R15+0
	SWAPF      STATUS+0, 0
	CLRF       STATUS+0
	MOVWF      ___saveSTATUS+0
	MOVF       PCLATH+0, 0
	MOVWF      ___savePCLATH+0
	CLRF       PCLATH+0
;APL.c,2 :: 		void interrupt()
;APL.c,3 :: 		{INTCON.GIE=0;
	BCF        INTCON+0, 7
;APL.c,4 :: 		if(INTCON.T0IF)
	BTFSS      INTCON+0, 2
	GOTO       L_interrupt0
;APL.c,5 :: 		{ i++;
	INCF       _i+0, 1
	BTFSC      STATUS+0, 2
	INCF       _i+1, 1
;APL.c,6 :: 		INTCON.T0IF=0;
	BCF        INTCON+0, 2
;APL.c,7 :: 		TMR0=99;
	MOVLW      99
	MOVWF      TMR0+0
;APL.c,8 :: 		}
L_interrupt0:
;APL.c,9 :: 		INTCON.GIE=1;
	BSF        INTCON+0, 7
;APL.c,10 :: 		}
L__interrupt1:
	MOVF       ___savePCLATH+0, 0
	MOVWF      PCLATH+0
	SWAPF      ___saveSTATUS+0, 0
	MOVWF      STATUS+0
	SWAPF      R15+0, 1
	SWAPF      R15+0, 0
	RETFIE
; end of _interrupt

_main:
;APL.c,11 :: 		void main() {
;APL.c,29 :: 		}
	GOTO       $+0
; end of _main
