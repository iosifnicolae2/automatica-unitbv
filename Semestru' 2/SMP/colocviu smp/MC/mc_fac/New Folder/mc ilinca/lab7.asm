
_interrupt:
	MOVWF      R15+0
	SWAPF      STATUS+0, 0
	CLRF       STATUS+0
	MOVWF      ___saveSTATUS+0
	MOVF       PCLATH+0, 0
	MOVWF      ___savePCLATH+0
	CLRF       PCLATH+0
;lab7.c,2 :: 		void interrupt()
;lab7.c,3 :: 		{INTCON.GIE=0;
	BCF        INTCON+0, 7
;lab7.c,4 :: 		if(INTCON.T0IF)
	BTFSS      INTCON+0, 2
	GOTO       L_interrupt0
;lab7.c,5 :: 		{ i++;
	INCF       _i+0, 1
	BTFSC      STATUS+0, 2
	INCF       _i+1, 1
;lab7.c,6 :: 		INTCON.T0IF=0;
	BCF        INTCON+0, 2
;lab7.c,7 :: 		TMR0=99;
	MOVLW      99
	MOVWF      TMR0+0
;lab7.c,8 :: 		}
L_interrupt0:
;lab7.c,9 :: 		INTCON.GIE=1;
	BSF        INTCON+0, 7
;lab7.c,10 :: 		}
L__interrupt4:
	MOVF       ___savePCLATH+0, 0
	MOVWF      PCLATH+0
	SWAPF      ___saveSTATUS+0, 0
	MOVWF      STATUS+0
	SWAPF      R15+0, 1
	SWAPF      R15+0, 0
	RETFIE
; end of _interrupt

_main:
;lab7.c,12 :: 		void main() {
;lab7.c,13 :: 		OPTION_REG=0b00000101;
	MOVLW      5
	MOVWF      OPTION_REG+0
;lab7.c,14 :: 		INTCON.T0IE=1;
	BSF        INTCON+0, 5
;lab7.c,15 :: 		INTCON.GIE=1;
	BSF        INTCON+0, 7
;lab7.c,17 :: 		TMR0=99;
	MOVLW      99
	MOVWF      TMR0+0
;lab7.c,18 :: 		TRISB=0;
	CLRF       TRISB+0
;lab7.c,19 :: 		while(1)
L_main1:
;lab7.c,20 :: 		{if(i==50)
	MOVLW      0
	XORWF      _i+1, 0
	BTFSS      STATUS+0, 2
	GOTO       L__main5
	MOVLW      50
	XORWF      _i+0, 0
L__main5:
	BTFSS      STATUS+0, 2
	GOTO       L_main3
;lab7.c,21 :: 		{PORTB.RB0=! PORTB.RB0;
	MOVLW      1
	XORWF      PORTB+0, 1
;lab7.c,22 :: 		i=0;}
	CLRF       _i+0
	CLRF       _i+1
L_main3:
;lab7.c,25 :: 		}
	GOTO       L_main1
;lab7.c,26 :: 		}
	GOTO       $+0
; end of _main
