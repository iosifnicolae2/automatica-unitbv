
_main:
;lab4.c,16 :: 		void main() {
;lab4.c,39 :: 		A=0;
	CLRF       R1+0
;lab4.c,40 :: 		B=0;
	CLRF       R2+0
;lab4.c,41 :: 		C=0;
	CLRF       R3+0
;lab4.c,42 :: 		D=1;
	MOVLW      1
	MOVWF      R4+0
;lab4.c,43 :: 		E=0;
	CLRF       R5+0
;lab4.c,44 :: 		TRISA.RB0=1; //"OK"
	BSF        TRISA+0, 0
;lab4.c,45 :: 		TRISA.RB1=1; //"CANCEL"
	BSF        TRISA+0, 1
;lab4.c,46 :: 		TRISA.RB2=1;    //<
	BSF        TRISA+0, 2
;lab4.c,47 :: 		TRISA.RB3=1;   //>
	BSF        TRISA+0, 3
;lab4.c,48 :: 		TRISA.RB4=1;
	BSF        TRISA+0, 4
;lab4.c,49 :: 		if((A==1)&&(PORTB.RB0 == 1)){A=0;B=1;}
L_main2:
;lab4.c,50 :: 		if((B==1)&&(PORTB.RB2 == 1)){B=0;C=1;}
	MOVF       R2+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main5
	BTFSS      PORTB+0, 2
	GOTO       L_main5
L__main29:
	CLRF       R2+0
	MOVLW      1
	MOVWF      R3+0
L_main5:
;lab4.c,51 :: 		if((C==1)&&(PORTB.RB1 == 1)){C=0;B=1;}
	MOVF       R3+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main8
	BTFSS      PORTB+0, 1
	GOTO       L_main8
L__main28:
	CLRF       R3+0
	MOVLW      1
	MOVWF      R2+0
L_main8:
;lab4.c,52 :: 		if((D==1)&&(PORTB.RB0 == 1)){E=1;C=0;}
	MOVF       R4+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main11
	BTFSS      PORTB+0, 0
	GOTO       L_main11
L__main27:
	MOVLW      1
	MOVWF      R5+0
	CLRF       R3+0
L_main11:
;lab4.c,53 :: 		if((E==1)&&(PORTB.RA0 == 1)){E=0;A=1;}
	MOVF       R5+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main14
	BTFSS      PORTB+0, 0
	GOTO       L_main14
L__main26:
	CLRF       R5+0
	MOVLW      1
	MOVWF      R1+0
L_main14:
;lab4.c,54 :: 		if((B==1)&&(PORTA.RB0 == 1)){D=1;B=0;}
	MOVF       R2+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main17
	BTFSS      PORTA+0, 0
	GOTO       L_main17
L__main25:
	MOVLW      1
	MOVWF      R4+0
	CLRF       R2+0
L_main17:
;lab4.c,55 :: 		if((D==1)&&(PORTB.RB0 == 1)){A=1;D=0;}
	MOVF       R4+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main20
	BTFSS      PORTB+0, 0
	GOTO       L_main20
L__main24:
	MOVLW      1
	MOVWF      R1+0
	CLRF       R4+0
L_main20:
;lab4.c,56 :: 		if(A==1){PORTB.RB3=1;PORTB.RB4=0;}
	MOVF       R1+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main21
	BSF        PORTB+0, 3
	BCF        PORTB+0, 4
L_main21:
;lab4.c,57 :: 		if(B==1){PORTB.RB3=0;PORTB.RB4=0;}
	MOVF       R2+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main22
	BCF        PORTB+0, 3
	BCF        PORTB+0, 4
L_main22:
;lab4.c,58 :: 		if(C==1){PORTB.RB3=0;PORTB.RB4=1;}
	MOVF       R3+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main23
	BCF        PORTB+0, 3
	BSF        PORTB+0, 4
L_main23:
;lab4.c,64 :: 		}
	GOTO       $+0
; end of _main
