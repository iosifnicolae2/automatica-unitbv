
_main:
;lab2.c,1 :: 		void main() {
;lab2.c,3 :: 		TRISB=0;
	CLRF       TRISB+0
;lab2.c,8 :: 		for(i=0;i<=7;i++)
	CLRF       main_i_L0+0
	CLRF       main_i_L0+1
L_main0:
	MOVLW      128
	MOVWF      R0+0
	MOVLW      128
	XORWF      main_i_L0+1, 0
	SUBWF      R0+0, 0
	BTFSS      STATUS+0, 2
	GOTO       L__main4
	MOVF       main_i_L0+0, 0
	SUBLW      7
L__main4:
	BTFSS      STATUS+0, 0
	GOTO       L_main1
;lab2.c,9 :: 		PORTB=pow(2,i);
	MOVLW      0
	MOVWF      FARG_pow_x+0
	MOVLW      0
	MOVWF      FARG_pow_x+1
	MOVLW      0
	MOVWF      FARG_pow_x+2
	MOVLW      128
	MOVWF      FARG_pow_x+3
	MOVF       main_i_L0+0, 0
	MOVWF      R0+0
	MOVF       main_i_L0+1, 0
	MOVWF      R0+1
	CALL       _Int2Double+0
	MOVF       R0+0, 0
	MOVWF      FARG_pow_y+0
	MOVF       R0+1, 0
	MOVWF      FARG_pow_y+1
	MOVF       R0+2, 0
	MOVWF      FARG_pow_y+2
	MOVF       R0+3, 0
	MOVWF      FARG_pow_y+3
	CALL       _pow+0
	CALL       _Double2Byte+0
	MOVF       R0+0, 0
	MOVWF      PORTB+0
;lab2.c,8 :: 		for(i=0;i<=7;i++)
	INCF       main_i_L0+0, 1
	BTFSC      STATUS+0, 2
	INCF       main_i_L0+1, 1
;lab2.c,9 :: 		PORTB=pow(2,i);
	GOTO       L_main0
L_main1:
;lab2.c,10 :: 		Delay_ms(1000);
	MOVLW      11
	MOVWF      R11+0
	MOVLW      38
	MOVWF      R12+0
	MOVLW      93
	MOVWF      R13+0
L_main3:
	DECFSZ     R13+0, 1
	GOTO       L_main3
	DECFSZ     R12+0, 1
	GOTO       L_main3
	DECFSZ     R11+0, 1
	GOTO       L_main3
	NOP
	NOP
;lab2.c,13 :: 		}
	GOTO       $+0
; end of _main
