
_init:
;lab8.c,1 :: 		void init()
;lab8.c,3 :: 		CMCON0.C2OUT=0;
	BCF        CMCON0+0, 7
;lab8.c,4 :: 		CMCON0.C1OUT=0;
	BCF        CMCON0+0, 6
;lab8.c,5 :: 		CMCON0.C2INV=1;
	BSF        CMCON0+0, 5
;lab8.c,6 :: 		CMCON0.C1INV=0;
	BCF        CMCON0+0, 4
;lab8.c,7 :: 		CMCON0.CIS=0;
	BCF        CMCON0+0, 3
;lab8.c,8 :: 		CMCON0.CM2=0;
	BCF        CMCON0+0, 2
;lab8.c,9 :: 		CMCON0.CM1=1;
	BSF        CMCON0+0, 1
;lab8.c,10 :: 		CMCON0.CM0=0;
	BCF        CMCON0+0, 0
;lab8.c,11 :: 		CMCON1=0;
	CLRF       CMCON1+0
;lab8.c,12 :: 		VRCON.VREN=1;  //1
	BSF        VRCON+0, 7
;lab8.c,13 :: 		VRCON.VRR=1;   //1
	BSF        VRCON+0, 5
;lab8.c,14 :: 		VRCON.VR3=0;
	BCF        VRCON+0, 3
;lab8.c,15 :: 		VRCON.VR2=0;
	BCF        VRCON+0, 2
;lab8.c,16 :: 		VRCON.VR1=1;
	BSF        VRCON+0, 1
;lab8.c,17 :: 		VRCON.VR0=0;
	BCF        VRCON+0, 0
;lab8.c,18 :: 		TRISA=0b00001111;
	MOVLW      15
	MOVWF      TRISA+0
;lab8.c,19 :: 		TRISB=0;
	CLRF       TRISB+0
;lab8.c,20 :: 		PORTB=0;
	CLRF       PORTB+0
;lab8.c,22 :: 		}
	RETURN
; end of _init

_main:
;lab8.c,23 :: 		void main() {
;lab8.c,24 :: 		init();
	CALL       _init+0
;lab8.c,25 :: 		while(1);
L_main0:
	GOTO       L_main0
;lab8.c,31 :: 		}
	GOTO       $+0
; end of _main
