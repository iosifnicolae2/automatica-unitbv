
_interrupt:
	MOVWF      R15+0
	SWAPF      STATUS+0, 0
	CLRF       STATUS+0
	MOVWF      ___saveSTATUS+0
	MOVF       PCLATH+0, 0
	MOVWF      ___savePCLATH+0
	CLRF       PCLATH+0
;laborator7.c,1 :: 		void interrupt(){
;laborator7.c,2 :: 		if (TMR0IF_bit){
	BTFSS      TMR0IF_bit+0, 2
	GOTO       L_interrupt0
;laborator7.c,3 :: 		TMR0 =0;
	CLRF       TMR0+0
;laborator7.c,4 :: 		PORTB.RB0 = ~PORTB.RB0;
	MOVLW      1
	XORWF      PORTB+0, 1
;laborator7.c,5 :: 		TMR0IF_bit = 0;
	BCF        TMR0IF_bit+0, 2
;laborator7.c,6 :: 		}
L_interrupt0:
;laborator7.c,7 :: 		}
L__interrupt1:
	MOVF       ___savePCLATH+0, 0
	MOVWF      PCLATH+0
	SWAPF      ___saveSTATUS+0, 0
	MOVWF      STATUS+0
	SWAPF      R15+0, 1
	SWAPF      R15+0, 0
	RETFIE
; end of _interrupt

_main:
;laborator7.c,11 :: 		void main() {
;laborator7.c,13 :: 		INTCON.GIE = 1; //Global
	BSF        INTCON+0, 7
;laborator7.c,14 :: 		INTCON.T0IE = 1; //External
	BSF        INTCON+0, 5
;laborator7.c,15 :: 		INTCON.PEIE = 1;
	BSF        INTCON+0, 6
;laborator7.c,16 :: 		TMR0=0;
	CLRF       TMR0+0
;laborator7.c,17 :: 		OPTION_REG = 0b0000110;
	MOVLW      6
	MOVWF      OPTION_REG+0
;laborator7.c,19 :: 		TRISB.RB0=0;
	BCF        TRISB+0, 0
;laborator7.c,20 :: 		PORTB.RB0=1;
	BSF        PORTB+0, 0
;laborator7.c,25 :: 		}
	GOTO       $+0
; end of _main
