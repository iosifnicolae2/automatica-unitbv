
_main:
;semafor_cu _buton.c,1 :: 		void main() {
;semafor_cu _buton.c,4 :: 		TRISD.RD0=0;
	BCF        TRISD+0, 0
;semafor_cu _buton.c,5 :: 		trisd.RD1=0;
	BCF        TRISD+0, 1
;semafor_cu _buton.c,6 :: 		trisd.rd2=0;
	BCF        TRISD+0, 2
;semafor_cu _buton.c,7 :: 		trisd.rd3=0;
	BCF        TRISD+0, 3
;semafor_cu _buton.c,8 :: 		trisd.rd4=0;
	BCF        TRISD+0, 4
;semafor_cu _buton.c,9 :: 		trisd.rd5=0;
	BCF        TRISD+0, 5
;semafor_cu _buton.c,11 :: 		trisd.rd6=1;
	BSF        TRISD+0, 6
;semafor_cu _buton.c,15 :: 		while(1)
L_main0:
;semafor_cu _buton.c,48 :: 		if(portd.rd6 == 0)
	BTFSC      PORTD+0, 6
	GOTO       L_main2
;semafor_cu _buton.c,50 :: 		portd.RD0=1;
	BSF        PORTD+0, 0
;semafor_cu _buton.c,51 :: 		portd.RD1=0;
	BCF        PORTD+0, 1
;semafor_cu _buton.c,52 :: 		portd.rd2=0;
	BCF        PORTD+0, 2
;semafor_cu _buton.c,53 :: 		portd.rd3=0;
	BCF        PORTD+0, 3
;semafor_cu _buton.c,54 :: 		portd.rd4=0;
	BCF        PORTD+0, 4
;semafor_cu _buton.c,55 :: 		portd.rd5=1;
	BSF        PORTD+0, 5
;semafor_cu _buton.c,56 :: 		}
	GOTO       L_main3
L_main2:
;semafor_cu _buton.c,59 :: 		portd.rd0=0;
	BCF        PORTD+0, 0
;semafor_cu _buton.c,60 :: 		portd.rd5=0;
	BCF        PORTD+0, 5
;semafor_cu _buton.c,61 :: 		portd.rd1=1;
	BSF        PORTD+0, 1
;semafor_cu _buton.c,62 :: 		portd.rd4=1;
	BSF        PORTD+0, 4
;semafor_cu _buton.c,63 :: 		delay_ms(1000);
	MOVLW      2
	MOVWF      R11+0
	MOVLW      69
	MOVWF      R12+0
	MOVLW      169
	MOVWF      R13+0
L_main4:
	DECFSZ     R13+0, 1
	GOTO       L_main4
	DECFSZ     R12+0, 1
	GOTO       L_main4
	DECFSZ     R11+0, 1
	GOTO       L_main4
	NOP
	NOP
;semafor_cu _buton.c,64 :: 		portd.rd1=0;
	BCF        PORTD+0, 1
;semafor_cu _buton.c,65 :: 		portd.rd4=0;
	BCF        PORTD+0, 4
;semafor_cu _buton.c,66 :: 		portd.rd2=1;
	BSF        PORTD+0, 2
;semafor_cu _buton.c,67 :: 		portd.rd3=1;
	BSF        PORTD+0, 3
;semafor_cu _buton.c,68 :: 		delay_ms(1000);
	MOVLW      2
	MOVWF      R11+0
	MOVLW      69
	MOVWF      R12+0
	MOVLW      169
	MOVWF      R13+0
L_main5:
	DECFSZ     R13+0, 1
	GOTO       L_main5
	DECFSZ     R12+0, 1
	GOTO       L_main5
	DECFSZ     R11+0, 1
	GOTO       L_main5
	NOP
	NOP
;semafor_cu _buton.c,69 :: 		portd.rd2=0;
	BCF        PORTD+0, 2
;semafor_cu _buton.c,70 :: 		portd.rd3=0;
	BCF        PORTD+0, 3
;semafor_cu _buton.c,71 :: 		portd.rd1=1;
	BSF        PORTD+0, 1
;semafor_cu _buton.c,72 :: 		portd.rd4=1;
	BSF        PORTD+0, 4
;semafor_cu _buton.c,73 :: 		delay_ms(1000);
	MOVLW      2
	MOVWF      R11+0
	MOVLW      69
	MOVWF      R12+0
	MOVLW      169
	MOVWF      R13+0
L_main6:
	DECFSZ     R13+0, 1
	GOTO       L_main6
	DECFSZ     R12+0, 1
	GOTO       L_main6
	DECFSZ     R11+0, 1
	GOTO       L_main6
	NOP
	NOP
;semafor_cu _buton.c,76 :: 		}
L_main3:
;semafor_cu _buton.c,77 :: 		}
	GOTO       L_main0
;semafor_cu _buton.c,78 :: 		}
	GOTO       $+0
; end of _main
