
_main:
;ex2.c,1 :: 		void main() {
;ex2.c,4 :: 		TRISD.RD0=0;
	BCF        TRISD+0, 0
;ex2.c,5 :: 		trisd.RD1=0;
	BCF        TRISD+0, 1
;ex2.c,6 :: 		trisd.rd2=0;
	BCF        TRISD+0, 2
;ex2.c,7 :: 		trisd.rd3=0;
	BCF        TRISD+0, 3
;ex2.c,8 :: 		trisd.rd4=0;
	BCF        TRISD+0, 4
;ex2.c,9 :: 		trisd.rd5=0;
	BCF        TRISD+0, 5
;ex2.c,16 :: 		while(1){
L_main0:
;ex2.c,17 :: 		portd.rd0=1;
	BSF        PORTD+0, 0
;ex2.c,18 :: 		delay_ms (500);
	MOVLW      163
	MOVWF      R12+0
	MOVLW      85
	MOVWF      R13+0
L_main2:
	DECFSZ     R13+0, 1
	GOTO       L_main2
	DECFSZ     R12+0, 1
	GOTO       L_main2
;ex2.c,19 :: 		portd.rd5=1;
	BSF        PORTD+0, 5
;ex2.c,20 :: 		portd.rd0=0;
	BCF        PORTD+0, 0
;ex2.c,21 :: 		delay_ms(500);
	MOVLW      163
	MOVWF      R12+0
	MOVLW      85
	MOVWF      R13+0
L_main3:
	DECFSZ     R13+0, 1
	GOTO       L_main3
	DECFSZ     R12+0, 1
	GOTO       L_main3
;ex2.c,22 :: 		portd.rd5=0;
	BCF        PORTD+0, 5
;ex2.c,24 :: 		portd.rd2=1;
	BSF        PORTD+0, 2
;ex2.c,25 :: 		delay_ms(500);
	MOVLW      163
	MOVWF      R12+0
	MOVLW      85
	MOVWF      R13+0
L_main4:
	DECFSZ     R13+0, 1
	GOTO       L_main4
	DECFSZ     R12+0, 1
	GOTO       L_main4
;ex2.c,26 :: 		portd.rd3=1;
	BSF        PORTD+0, 3
;ex2.c,27 :: 		portd.rd2=0;
	BCF        PORTD+0, 2
;ex2.c,28 :: 		delay_ms(500);
	MOVLW      163
	MOVWF      R12+0
	MOVLW      85
	MOVWF      R13+0
L_main5:
	DECFSZ     R13+0, 1
	GOTO       L_main5
	DECFSZ     R12+0, 1
	GOTO       L_main5
;ex2.c,29 :: 		portd.rd3=0;
	BCF        PORTD+0, 3
;ex2.c,30 :: 		portd.rd1=1;
	BSF        PORTD+0, 1
;ex2.c,31 :: 		delay_ms(500);
	MOVLW      163
	MOVWF      R12+0
	MOVLW      85
	MOVWF      R13+0
L_main6:
	DECFSZ     R13+0, 1
	GOTO       L_main6
	DECFSZ     R12+0, 1
	GOTO       L_main6
;ex2.c,32 :: 		portd.rd4=0;
	BCF        PORTD+0, 4
;ex2.c,33 :: 		portd.rd1=0;
	BCF        PORTD+0, 1
;ex2.c,34 :: 		delay_ms(500);
	MOVLW      163
	MOVWF      R12+0
	MOVLW      85
	MOVWF      R13+0
L_main7:
	DECFSZ     R13+0, 1
	GOTO       L_main7
	DECFSZ     R12+0, 1
	GOTO       L_main7
;ex2.c,35 :: 		portd.rd4=1;
	BSF        PORTD+0, 4
;ex2.c,39 :: 		}
	GOTO       L_main0
;ex2.c,41 :: 		}
	GOTO       $+0
; end of _main
