#line 1 "D:/Lab MC/4391/MikroC/Lab09.c"
float voltaj = 1.5;
bit stare1;
bit stare2;
void main() {
CMCON0 = 0b00000010;
VRCON = 0b10100111;

PIE2.C1IE = 1;
INTCON.PEIE = 1;
INTCON.GIE = 1;

TRISD.RD0 = 1;
TRISD.RD1 = 1;
TRISD.RD2 = 0;
PORTD.RD2 = 0;

if(PORTD.RD0)
 stare1 = 1;
if(stare1 == 1 && PORTD.RD0 == 0){
 if(voltaj + 0.2 <= 1.75)
 voltaj += 0.2;
 VRCON = 150 + (int) (voltaj * 24 / 5);
 stare1 = 0;
}

if(PORTD.RD1)
 stare2 = 1;
if(stare2 == 1 && PORTD.RD1 == 0){
 if(voltaj - 0.2 > 0)
 voltaj -= 0.2;
 VRCON = 150 + (int) (voltaj * 24 / 5);
 stare2 = 0;
}

while(1){
 if(CMCON0.C1OUT)
 PORTD.RD2 = 0;
 else
 PORTD.RD2 = 1;
}

}

void interrupt() {
 INTCON.GIE = 0;
 if (PIE2.C1IF) {
 PORTD.RD2 = ~PORTD.RD2;
 PIE2.C1IF = 0;
 }
 INTCON.GIE = 1;
}
