
_INIT:
;16mai.c,1 :: 		void INIT()
;16mai.c,3 :: 		CMCON0.C2OUT=0;
	BCF        CMCON0+0, 7
;16mai.c,4 :: 		CMCON0.C1OUT=0;
	BCF        CMCON0+0, 6
;16mai.c,5 :: 		CMCON0.C2INV=0;
	BCF        CMCON0+0, 5
;16mai.c,6 :: 		CMCON0.C1INV=0;
	BCF        CMCON0+0, 4
;16mai.c,7 :: 		CMCON0.CIS=0;
	BCF        CMCON0+0, 3
;16mai.c,8 :: 		CMCON0.CM2=0;
	BCF        CMCON0+0, 2
;16mai.c,9 :: 		CMCON0.CM1=1;
	BSF        CMCON0+0, 1
;16mai.c,10 :: 		CMCON0.CM0=0;
	BCF        CMCON0+0, 0
;16mai.c,11 :: 		TRISB=0;
	CLRF       TRISB+0
;16mai.c,12 :: 		VRCON.VREN=1;
	BSF        VRCON+0, 7
;16mai.c,13 :: 		VRCON.VRR=0;
	BCF        VRCON+0, 5
;16mai.c,14 :: 		VRCON.VR3=0;
	BCF        VRCON+0, 3
;16mai.c,15 :: 		VRCON.VR2=0;
	BCF        VRCON+0, 2
;16mai.c,16 :: 		VRCON.VR1=1;
	BSF        VRCON+0, 1
;16mai.c,17 :: 		VRCON.VR0=0;
	BCF        VRCON+0, 0
;16mai.c,18 :: 		}
	RETURN
; end of _INIT

_main:
;16mai.c,20 :: 		void main()
;16mai.c,22 :: 		INIT();
	CALL       _INIT+0
;16mai.c,23 :: 		while(1)
L_main0:
;16mai.c,25 :: 		PORTB.RB0=CMCON0.C1OUT;
	BTFSC      CMCON0+0, 6
	GOTO       L__main3
	BCF        PORTB+0, 0
	GOTO       L__main4
L__main3:
	BSF        PORTB+0, 0
L__main4:
;16mai.c,26 :: 		Delay_ms(100);
	MOVLW      2
	MOVWF      R11+0
	MOVLW      4
	MOVWF      R12+0
	MOVLW      186
	MOVWF      R13+0
L_main2:
	DECFSZ     R13+0, 1
	GOTO       L_main2
	DECFSZ     R12+0, 1
	GOTO       L_main2
	DECFSZ     R11+0, 1
	GOTO       L_main2
	NOP
;16mai.c,27 :: 		}
	GOTO       L_main0
;16mai.c,28 :: 		}
	GOTO       $+0
; end of _main
