
_main:
;lab3_dia.c,1 :: 		void main() {
;lab3_dia.c,10 :: 		TRISD.RD0=1;
	BSF        TRISD+0, 0
;lab3_dia.c,11 :: 		TRISD.RD1=1;
	BSF        TRISD+0, 1
;lab3_dia.c,12 :: 		TRISD.RD2=1;
	BSF        TRISD+0, 2
;lab3_dia.c,13 :: 		TRISC.RC0=1;
	BSF        TRISC+0, 0
;lab3_dia.c,14 :: 		TRISC.RC1=1;
	BSF        TRISC+0, 1
;lab3_dia.c,17 :: 		TRISD.RD3=0;
	BCF        TRISD+0, 3
;lab3_dia.c,18 :: 		TRISD.RD4=0;
	BCF        TRISD+0, 4
;lab3_dia.c,19 :: 		TRISC.RC2=0;
	BCF        TRISC+0, 2
;lab3_dia.c,20 :: 		TRISC.RC3=0;
	BCF        TRISC+0, 3
;lab3_dia.c,21 :: 		TRISC.RC4=0;
	BCF        TRISC+0, 4
;lab3_dia.c,24 :: 		A=1;
	MOVLW      1
	MOVWF      R1+0
;lab3_dia.c,25 :: 		B=0;
	CLRF       R2+0
;lab3_dia.c,26 :: 		C=0;
	CLRF       R3+0
;lab3_dia.c,27 :: 		D=0;
	CLRF       R4+0
;lab3_dia.c,28 :: 		E=0;
	CLRF       R5+0
;lab3_dia.c,30 :: 		while(1)
L_main0:
;lab3_dia.c,33 :: 		if ((A==1)&&(PORTD.RD0==1))
	MOVF       R1+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main4
	BTFSS      PORTD+0, 0
	GOTO       L_main4
L__main26:
;lab3_dia.c,34 :: 		{ A=0;
	CLRF       R1+0
;lab3_dia.c,35 :: 		B=1; }
	MOVLW      1
	MOVWF      R2+0
L_main4:
;lab3_dia.c,36 :: 		if ((B==1)&&(PORTD.RD2==1))
	MOVF       R2+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main7
	BTFSS      PORTD+0, 2
	GOTO       L_main7
L__main25:
;lab3_dia.c,37 :: 		{ B=0;
	CLRF       R2+0
;lab3_dia.c,38 :: 		C=1; }
	MOVLW      1
	MOVWF      R3+0
L_main7:
;lab3_dia.c,39 :: 		if ((C==1)&&(PORTD.RD1==1))
	MOVF       R3+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main10
	BTFSS      PORTD+0, 1
	GOTO       L_main10
L__main24:
;lab3_dia.c,40 :: 		{ C=0;
	CLRF       R3+0
;lab3_dia.c,41 :: 		A=1; }
	MOVLW      1
	MOVWF      R1+0
L_main10:
;lab3_dia.c,42 :: 		if ((D==1)&&(PORTC.RC0==1))
	MOVF       R4+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main13
	BTFSS      PORTC+0, 0
	GOTO       L_main13
L__main23:
;lab3_dia.c,43 :: 		{ D=0;
	CLRF       R4+0
;lab3_dia.c,44 :: 		E=1; }
	MOVLW      1
	MOVWF      R5+0
L_main13:
;lab3_dia.c,45 :: 		if ((E==1)&&(PORTC.RC1==1))
	MOVF       R5+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main16
	BTFSS      PORTC+0, 1
	GOTO       L_main16
L__main22:
;lab3_dia.c,46 :: 		{ E=0;
	CLRF       R5+0
;lab3_dia.c,47 :: 		D=1; }
	MOVLW      1
	MOVWF      R4+0
L_main16:
;lab3_dia.c,50 :: 		if(A==1)
	MOVF       R1+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main17
;lab3_dia.c,51 :: 		{ PORTD.RD3=0;
	BCF        PORTD+0, 3
;lab3_dia.c,52 :: 		PORTD.RD4=0;
	BCF        PORTD+0, 4
;lab3_dia.c,53 :: 		PORTC.RC2=0;
	BCF        PORTC+0, 2
;lab3_dia.c,54 :: 		PORTC.RC3=0;
	BCF        PORTC+0, 3
;lab3_dia.c,55 :: 		PORTC.RC4=0; }
	BCF        PORTC+0, 4
L_main17:
;lab3_dia.c,56 :: 		if(B==1)
	MOVF       R2+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main18
;lab3_dia.c,57 :: 		{ PORTD.RD3=1;
	BSF        PORTD+0, 3
;lab3_dia.c,58 :: 		PORTD.RD4=0;
	BCF        PORTD+0, 4
;lab3_dia.c,59 :: 		PORTC.RC2=1;
	BSF        PORTC+0, 2
;lab3_dia.c,60 :: 		PORTC.RC3=0;
	BCF        PORTC+0, 3
;lab3_dia.c,61 :: 		PORTC.RC4=1; }
	BSF        PORTC+0, 4
L_main18:
;lab3_dia.c,62 :: 		if(C==1)
	MOVF       R3+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main19
;lab3_dia.c,63 :: 		{ PORTD.RD3=0;
	BCF        PORTD+0, 3
;lab3_dia.c,64 :: 		PORTD.RD4=1;
	BSF        PORTD+0, 4
;lab3_dia.c,65 :: 		PORTC.RC2=0;
	BCF        PORTC+0, 2
;lab3_dia.c,66 :: 		PORTC.RC3=0;
	BCF        PORTC+0, 3
;lab3_dia.c,67 :: 		PORTC.RC4=0; }
	BCF        PORTC+0, 4
L_main19:
;lab3_dia.c,68 :: 		if(D==1)
	MOVF       R4+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main20
;lab3_dia.c,69 :: 		{ PORTD.RD3=0;
	BCF        PORTD+0, 3
;lab3_dia.c,70 :: 		PORTD.RD4=0;
	BCF        PORTD+0, 4
;lab3_dia.c,71 :: 		PORTC.RC2=1;
	BSF        PORTC+0, 2
;lab3_dia.c,72 :: 		PORTC.RC3=0;
	BCF        PORTC+0, 3
;lab3_dia.c,73 :: 		PORTC.RC4=0; }
	BCF        PORTC+0, 4
L_main20:
;lab3_dia.c,74 :: 		if(E==1)
	MOVF       R5+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main21
;lab3_dia.c,75 :: 		{ PORTD.RD3=0;
	BCF        PORTD+0, 3
;lab3_dia.c,76 :: 		PORTD.RD4=0;
	BCF        PORTD+0, 4
;lab3_dia.c,77 :: 		PORTC.RC3=0;
	BCF        PORTC+0, 3
;lab3_dia.c,78 :: 		PORTC.RC4=0;
	BCF        PORTC+0, 4
;lab3_dia.c,79 :: 		PORTC.RC4=1; }
	BSF        PORTC+0, 4
L_main21:
;lab3_dia.c,80 :: 		}
	GOTO       L_main0
;lab3_dia.c,81 :: 		}
	GOTO       $+0
; end of _main
