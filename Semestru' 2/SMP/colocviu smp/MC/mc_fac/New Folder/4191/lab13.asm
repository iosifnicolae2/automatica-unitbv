
_main:
;lab13.c,9 :: 		void main(){
;lab13.c,12 :: 		TRISB.RA0=1;
	BSF        TRISB+0, 0
;lab13.c,13 :: 		TRISB.RA1=1;
	BSF        TRISB+0, 1
;lab13.c,14 :: 		TRISB.RA2=1;
	BSF        TRISB+0, 2
;lab13.c,15 :: 		TRISB.RA3=1;
	BSF        TRISB+0, 3
;lab13.c,16 :: 		TRISB.RA4=1;
	BSF        TRISB+0, 4
;lab13.c,17 :: 		TRISD.RA0=0;
	BCF        TRISD+0, 0
;lab13.c,18 :: 		TRISD.RA1=0;
	BCF        TRISD+0, 1
;lab13.c,19 :: 		TRISD.RA2=0;
	BCF        TRISD+0, 2
;lab13.c,20 :: 		TRISD.RA3=0;
	BCF        TRISD+0, 3
;lab13.c,21 :: 		TRISD.RA4=0;
	BCF        TRISD+0, 4
;lab13.c,23 :: 		A=1;
	MOVLW      1
	MOVWF      _A+0
;lab13.c,24 :: 		B=0;
	CLRF       _B+0
;lab13.c,25 :: 		C1=0;
	CLRF       _C1+0
;lab13.c,26 :: 		E=1;
	MOVLW      1
	MOVWF      _E+0
;lab13.c,27 :: 		F5=0;
	CLRF       _F5+0
;lab13.c,29 :: 		while(1){
L_main0:
;lab13.c,30 :: 		if ((TRISB.RA0==1)&&(A==1)) {A=0;B=1;}
	BTFSS      TRISB+0, 0
	GOTO       L_main4
	MOVF       _A+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main4
L__main21:
	CLRF       _A+0
	MOVLW      1
	MOVWF      _B+0
L_main4:
;lab13.c,31 :: 		if ((TRISB.RA1==1)&&(B==1)) {A=0;B=1;}
	BTFSS      TRISB+0, 1
	GOTO       L_main7
	MOVF       _B+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main7
L__main20:
	CLRF       _A+0
	MOVLW      1
	MOVWF      _B+0
L_main7:
;lab13.c,32 :: 		if ((TRISB.RA2==1)&&(C1==1)){A=0;B=1;}
	BTFSS      TRISB+0, 2
	GOTO       L_main10
	MOVF       _C1+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main10
L__main19:
	CLRF       _A+0
	MOVLW      1
	MOVWF      _B+0
L_main10:
;lab13.c,34 :: 		if((TRISB.RA4==0) &&(E==1)) {E=0;F5=1;}
	BTFSC      TRISB+0, 4
	GOTO       L_main13
	MOVF       _E+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main13
L__main18:
	CLRF       _E+0
	MOVLW      1
	MOVWF      _F5+0
L_main13:
;lab13.c,35 :: 		if((TRISB.RA3==1) &&(F5=1)) {E=1;F5=0;}
	BTFSS      TRISB+0, 3
	GOTO       L_main16
	MOVLW      1
	MOVWF      _F5+0
L__main17:
	MOVLW      1
	MOVWF      _E+0
	CLRF       _F5+0
L_main16:
;lab13.c,37 :: 		}
	GOTO       L_main0
;lab13.c,38 :: 		}
	GOTO       $+0
; end of _main
