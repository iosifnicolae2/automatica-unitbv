
_interrupt:
	MOVWF      R15+0
	SWAPF      STATUS+0, 0
	CLRF       STATUS+0
	MOVWF      ___saveSTATUS+0
	MOVF       PCLATH+0, 0
	MOVWF      ___savePCLATH+0
	CLRF       PCLATH+0
;timer.c,19 :: 		void interrupt()
;timer.c,21 :: 		INTCON.GIE=0;
	BCF        INTCON+0, 7
;timer.c,22 :: 		if(INTCON.T0IF)
	BTFSS      INTCON+0, 2
	GOTO       L_interrupt0
;timer.c,23 :: 		{INTCON.T0IF=0;
	BCF        INTCON+0, 2
;timer.c,24 :: 		i++;
	INCF       _i+0, 1
	BTFSC      STATUS+0, 2
	INCF       _i+1, 1
;timer.c,25 :: 		if(i==1000)
	MOVF       _i+1, 0
	XORLW      3
	BTFSS      STATUS+0, 2
	GOTO       L__interrupt6
	MOVLW      232
	XORWF      _i+0, 0
L__interrupt6:
	BTFSS      STATUS+0, 2
	GOTO       L_interrupt1
;timer.c,27 :: 		i=0;
	CLRF       _i+0
	CLRF       _i+1
;timer.c,28 :: 		sec++;
	INCF       _sec+0, 1
	BTFSC      STATUS+0, 2
	INCF       _sec+1, 1
;timer.c,29 :: 		if(sec==60)
	MOVLW      0
	XORWF      _sec+1, 0
	BTFSS      STATUS+0, 2
	GOTO       L__interrupt7
	MOVLW      60
	XORWF      _sec+0, 0
L__interrupt7:
	BTFSS      STATUS+0, 2
	GOTO       L_interrupt2
;timer.c,31 :: 		sec=0;
	CLRF       _sec+0
	CLRF       _sec+1
;timer.c,32 :: 		ghj++;
	INCF       _ghj+0, 1
	BTFSC      STATUS+0, 2
	INCF       _ghj+1, 1
;timer.c,33 :: 		}
L_interrupt2:
;timer.c,34 :: 		}
L_interrupt1:
;timer.c,35 :: 		}
L_interrupt0:
;timer.c,36 :: 		INTCON.GIE=1;
	BSF        INTCON+0, 7
;timer.c,37 :: 		}
L__interrupt5:
	MOVF       ___savePCLATH+0, 0
	MOVWF      PCLATH+0
	SWAPF      ___saveSTATUS+0, 0
	MOVWF      STATUS+0
	SWAPF      R15+0, 1
	SWAPF      R15+0, 0
	RETFIE
; end of _interrupt

_main:
;timer.c,38 :: 		void main() {
;timer.c,39 :: 		TRISB=0b00000000;
	CLRF       TRISB+0
;timer.c,40 :: 		PORTB=0b00000000;
	CLRF       PORTB+0
;timer.c,41 :: 		INTCON=0b11100000;
	MOVLW      224
	MOVWF      INTCON+0
;timer.c,42 :: 		OPTION_REG=0b0001000;
	MOVLW      8
	MOVWF      OPTION_REG+0
;timer.c,43 :: 		LCD_INIT();
	CALL       _Lcd_Init+0
;timer.c,44 :: 		while(1)
L_main3:
;timer.c,46 :: 		sprinti(str,"sec=%d %d",sec,ghj);
	MOVLW      _str+0
	MOVWF      FARG_sprinti_wh+0
	MOVLW      ?lstr_1_timer+0
	MOVWF      FARG_sprinti_f+0
	MOVLW      hi_addr(?lstr_1_timer+0
	MOVWF      FARG_sprinti_f+1
	MOVF       _sec+0, 0
	MOVWF      FARG_sprinti_wh+3
	MOVF       _sec+1, 0
	MOVWF      FARG_sprinti_wh+4
	MOVF       _ghj+0, 0
	MOVWF      FARG_sprinti_wh+5
	MOVF       _ghj+1, 0
	MOVWF      FARG_sprinti_wh+6
	CALL       _sprinti+0
;timer.c,47 :: 		LCD_OUT(1,1,Str);
	MOVLW      1
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      1
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      _str+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
;timer.c,48 :: 		}
	GOTO       L_main3
;timer.c,49 :: 		}
	GOTO       $+0
; end of _main
