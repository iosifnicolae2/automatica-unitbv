
_Move_Delay:
;lab5.c,19 :: 		void Move_Delay(){
;lab5.c,20 :: 		Delay_ms(500);
	MOVLW      6
	MOVWF      R11+0
	MOVLW      19
	MOVWF      R12+0
	MOVLW      173
	MOVWF      R13+0
L_Move_Delay0:
	DECFSZ     R13+0, 1
	GOTO       L_Move_Delay0
	DECFSZ     R12+0, 1
	GOTO       L_Move_Delay0
	DECFSZ     R11+0, 1
	GOTO       L_Move_Delay0
	NOP
	NOP
;lab5.c,21 :: 		}
	RETURN
; end of _Move_Delay

_main:
;lab5.c,23 :: 		void main(){
;lab5.c,24 :: 		TRISB = 0;
	CLRF       TRISB+0
;lab5.c,25 :: 		PORTB = 0xFF;
	MOVLW      255
	MOVWF      PORTB+0
;lab5.c,26 :: 		TRISB = 0xff;
	MOVLW      255
	MOVWF      TRISB+0
;lab5.c,27 :: 		ANSEL  = 0;                        // Configure AN pins as digital I/O
	CLRF       ANSEL+0
;lab5.c,29 :: 		Lcd_Init();                        // Initialize LCD
	CALL       _Lcd_Init+0
;lab5.c,31 :: 		Lcd_Cmd(_LCD_CLEAR);                // Clear display
	MOVLW      1
	MOVWF      FARG_Lcd_Cmd_out_char+0
	CALL       _Lcd_Cmd+0
;lab5.c,32 :: 		Lcd_Cmd(_LCD_CURSOR_OFF);           // Cursor off
	MOVLW      12
	MOVWF      FARG_Lcd_Cmd_out_char+0
	CALL       _Lcd_Cmd+0
;lab5.c,34 :: 		i=0;
	CLRF       _i+0
;lab5.c,35 :: 		for(i=0; i<21; i++) {
	CLRF       _i+0
L_main1:
	MOVLW      21
	SUBWF      _i+0, 0
	BTFSC      STATUS+0, 0
	GOTO       L_main2
;lab5.c,37 :: 		delay_ms(500);
	MOVLW      6
	MOVWF      R11+0
	MOVLW      19
	MOVWF      R12+0
	MOVLW      173
	MOVWF      R13+0
L_main4:
	DECFSZ     R13+0, 1
	GOTO       L_main4
	DECFSZ     R12+0, 1
	GOTO       L_main4
	DECFSZ     R11+0, 1
	GOTO       L_main4
	NOP
	NOP
;lab5.c,38 :: 		Lcd_Chr(1,1,i);                 // Write text in first row
	MOVLW      1
	MOVWF      FARG_Lcd_Chr_row+0
	MOVLW      1
	MOVWF      FARG_Lcd_Chr_column+0
	MOVF       _i+0, 0
	MOVWF      FARG_Lcd_Chr_out_char+0
	CALL       _Lcd_Chr+0
;lab5.c,35 :: 		for(i=0; i<21; i++) {
	INCF       _i+0, 1
;lab5.c,39 :: 		}
	GOTO       L_main1
L_main2:
;lab5.c,41 :: 		Delay_ms(2000);
	MOVLW      21
	MOVWF      R11+0
	MOVLW      75
	MOVWF      R12+0
	MOVLW      190
	MOVWF      R13+0
L_main5:
	DECFSZ     R13+0, 1
	GOTO       L_main5
	DECFSZ     R12+0, 1
	GOTO       L_main5
	DECFSZ     R11+0, 1
	GOTO       L_main5
	NOP
;lab5.c,44 :: 		}
	GOTO       $+0
; end of _main
