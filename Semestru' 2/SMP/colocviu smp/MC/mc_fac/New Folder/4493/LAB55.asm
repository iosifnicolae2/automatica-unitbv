
_main:
;LAB55.c,4 :: 		void main() {
;LAB55.c,7 :: 		TRISD=0;
	CLRF       TRISD+0
;LAB55.c,8 :: 		TRISB=1;
	MOVLW      1
	MOVWF      TRISB+0
;LAB55.c,9 :: 		TRISC=0;
	CLRF       TRISC+0
;LAB55.c,10 :: 		PORTD=0;
	CLRF       PORTD+0
;LAB55.c,11 :: 		INTCON=0B10010000;
	MOVLW      144
	MOVWF      INTCON+0
;LAB55.c,12 :: 		while (1)
L_main0:
;LAB55.c,14 :: 		PORTD=sir[i];
	MOVF       _i+0, 0
	ADDLW      _sir+0
	MOVWF      FSR
	MOVF       INDF+0, 0
	MOVWF      PORTD+0
;LAB55.c,15 :: 		i++;
	INCF       _i+0, 1
	BTFSC      STATUS+0, 2
	INCF       _i+1, 1
;LAB55.c,16 :: 		if (i==10)
	MOVLW      0
	XORWF      _i+1, 0
	BTFSS      STATUS+0, 2
	GOTO       L__main5
	MOVLW      10
	XORWF      _i+0, 0
L__main5:
	BTFSS      STATUS+0, 2
	GOTO       L_main2
;LAB55.c,17 :: 		i=0;
	CLRF       _i+0
	CLRF       _i+1
L_main2:
;LAB55.c,18 :: 		Delay_ms(1000);
	MOVLW      11
	MOVWF      R11+0
	MOVLW      38
	MOVWF      R12+0
	MOVLW      93
	MOVWF      R13+0
L_main3:
	DECFSZ     R13+0, 1
	GOTO       L_main3
	DECFSZ     R12+0, 1
	GOTO       L_main3
	DECFSZ     R11+0, 1
	GOTO       L_main3
	NOP
	NOP
;LAB55.c,19 :: 		}
	GOTO       L_main0
;LAB55.c,20 :: 		}
	GOTO       $+0
; end of _main

_interrupt:
	MOVWF      R15+0
	SWAPF      STATUS+0, 0
	CLRF       STATUS+0
	MOVWF      ___saveSTATUS+0
	MOVF       PCLATH+0, 0
	MOVWF      ___savePCLATH+0
	CLRF       PCLATH+0
;LAB55.c,21 :: 		void interrupt()
;LAB55.c,23 :: 		if (INTCON.INTF==1)
	BTFSS      INTCON+0, 1
	GOTO       L_interrupt4
;LAB55.c,24 :: 		{  PORTC=!PORTC;
	MOVF       PORTC+0, 0
	MOVLW      1
	BTFSS      STATUS+0, 2
	MOVLW      0
	MOVWF      PORTC+0
;LAB55.c,25 :: 		INTCON.INTF=0;
	BCF        INTCON+0, 1
;LAB55.c,26 :: 		}
L_interrupt4:
;LAB55.c,27 :: 		}
L__interrupt6:
	MOVF       ___savePCLATH+0, 0
	MOVWF      PCLATH+0
	SWAPF      ___saveSTATUS+0, 0
	MOVWF      STATUS+0
	SWAPF      R15+0, 1
	SWAPF      R15+0, 0
	RETFIE
; end of _interrupt
