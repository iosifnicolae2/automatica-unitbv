
_main:
;alina.c,17 :: 		void main ()
;alina.c,18 :: 		{ Lcd_init();
	CALL       _Lcd_Init+0
;alina.c,19 :: 		Lcd_Out(1,1,"alina");
	MOVLW      1
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      1
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      ?lstr1_alina+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
;alina.c,20 :: 		Lcd_out(2,2,"22ani") ;
	MOVLW      2
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      2
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      ?lstr2_alina+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
;alina.c,21 :: 		}
	GOTO       $+0
; end of _main
