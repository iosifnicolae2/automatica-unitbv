
_main:
;Lab 5M.c,18 :: 		void main()
;Lab 5M.c,20 :: 		Lcd_Init();
	CALL       _Lcd_Init+0
;Lab 5M.c,21 :: 		while(1)
L_main0:
;Lab 5M.c,23 :: 		for (i=0; i<10; i++)
	CLRF       _i+0
	CLRF       _i+1
L_main2:
	MOVLW      128
	XORWF      _i+1, 0
	MOVWF      R0+0
	MOVLW      128
	SUBWF      R0+0, 0
	BTFSS      STATUS+0, 2
	GOTO       L__main5
	MOVLW      10
	SUBWF      _i+0, 0
L__main5:
	BTFSC      STATUS+0, 0
	GOTO       L_main3
;Lab 5M.c,25 :: 		sprinti(text,"%d %d",i,j);
	MOVLW      _text+0
	MOVWF      FARG_sprinti_wh+0
	MOVLW      ?lstr_1_Lab 5M+0
	MOVWF      FARG_sprinti_f+0
	MOVLW      hi_addr(?lstr_1_Lab 5M+0
	MOVWF      FARG_sprinti_f+1
	MOVF       _i+0, 0
	MOVWF      FARG_sprinti_wh+3
	MOVF       _i+1, 0
	MOVWF      FARG_sprinti_wh+4
	MOVF       _j+0, 0
	MOVWF      FARG_sprinti_wh+5
	MOVF       _j+1, 0
	MOVWF      FARG_sprinti_wh+6
	CALL       _sprinti+0
;Lab 5M.c,26 :: 		Lcd_Out(1,1,text);
	MOVLW      1
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      1
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      _text+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
;Lab 5M.c,23 :: 		for (i=0; i<10; i++)
	INCF       _i+0, 1
	BTFSC      STATUS+0, 2
	INCF       _i+1, 1
;Lab 5M.c,29 :: 		}
	GOTO       L_main2
L_main3:
;Lab 5M.c,31 :: 		}
	GOTO       L_main0
;Lab 5M.c,32 :: 		}
	GOTO       $+0
; end of _main
