
_main:
	CLRF       main_n_L0+0
	CLRF       main_n_L0+1
	CLRF       main_test_L0+0
	CLRF       main_test_L0+1
	CLRF       main_b_L0+0
	CLRF       main_b_L0+1
	CLRF       main_a_L0+0
	CLRF       main_a_L0+1
	MOVLW      63
	MOVWF      main_num_L0+0
	MOVLW      0
	MOVWF      main_num_L0+1
	MOVLW      6
	MOVWF      main_num_L0+2
	MOVLW      0
	MOVWF      main_num_L0+3
	MOVLW      91
	MOVWF      main_num_L0+4
	MOVLW      0
	MOVWF      main_num_L0+5
	MOVLW      79
	MOVWF      main_num_L0+6
	MOVLW      0
	MOVWF      main_num_L0+7
	MOVLW      102
	MOVWF      main_num_L0+8
	MOVLW      0
	MOVWF      main_num_L0+9
	MOVLW      1
	MOVWF      main_sirb_L0+0
	MOVLW      0
	MOVWF      main_sirb_L0+1
	MOVLW      2
	MOVWF      main_sirb_L0+2
	MOVLW      0
	MOVWF      main_sirb_L0+3
	MOVLW      4
	MOVWF      main_sirb_L0+4
	MOVLW      0
	MOVWF      main_sirb_L0+5
	MOVLW      128
	MOVWF      main_sir_L0+0
	MOVLW      0
	MOVWF      main_sir_L0+1
	MOVLW      64
	MOVWF      main_sir_L0+2
	MOVLW      0
	MOVWF      main_sir_L0+3
	MOVLW      32
	MOVWF      main_sir_L0+4
	MOVLW      0
	MOVWF      main_sir_L0+5
;lab3.c,1 :: 		void main() {
;lab3.c,10 :: 		TRISB=0;
	CLRF       TRISB+0
;lab3.c,11 :: 		TRISA=0;
	CLRF       TRISA+0
;lab3.c,12 :: 		TRISD=0;
	CLRF       TRISD+0
;lab3.c,15 :: 		for (test=0; test<3; test++)
	CLRF       main_test_L0+0
	CLRF       main_test_L0+1
L_main0:
	MOVLW      128
	XORWF      main_test_L0+1, 0
	MOVWF      R0+0
	MOVLW      128
	SUBWF      R0+0, 0
	BTFSS      STATUS+0, 2
	GOTO       L__main10
	MOVLW      3
	SUBWF      main_test_L0+0, 0
L__main10:
	BTFSC      STATUS+0, 0
	GOTO       L_main1
;lab3.c,17 :: 		if (test=0)
	CLRF       main_test_L0+0
	CLRF       main_test_L0+1
;lab3.c,22 :: 		}
L_main3:
;lab3.c,24 :: 		if (test=1)
	MOVLW      1
	MOVWF      main_test_L0+0
	MOVLW      0
	MOVWF      main_test_L0+1
;lab3.c,26 :: 		PORTB=sir[1];
	MOVF       main_sir_L0+2, 0
	MOVWF      PORTB+0
;lab3.c,27 :: 		PORTA=sirb[1];
	MOVF       main_sirb_L0+2, 0
	MOVWF      PORTA+0
;lab3.c,28 :: 		delay_ms(1000);
	MOVLW      11
	MOVWF      R11+0
	MOVLW      38
	MOVWF      R12+0
	MOVLW      93
	MOVWF      R13+0
L_main7:
	DECFSZ     R13+0, 1
	GOTO       L_main7
	DECFSZ     R12+0, 1
	GOTO       L_main7
	DECFSZ     R11+0, 1
	GOTO       L_main7
	NOP
	NOP
;lab3.c,35 :: 		}
L_main8:
;lab3.c,15 :: 		for (test=0; test<3; test++)
	INCF       main_test_L0+0, 1
	BTFSC      STATUS+0, 2
	INCF       main_test_L0+1, 1
;lab3.c,36 :: 		}
	GOTO       L_main0
L_main1:
;lab3.c,58 :: 		}
	GOTO       $+0
; end of _main
