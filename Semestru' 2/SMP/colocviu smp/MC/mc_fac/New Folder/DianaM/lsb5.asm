
_main:
;lsb5.c,1 :: 		void main() {
;lsb5.c,2 :: 		TRISC=0x00;
	CLRF       TRISC+0
;lsb5.c,3 :: 		TRISD=0x00;
	CLRF       TRISD+0
;lsb5.c,4 :: 		PORTD=0x00;
	CLRF       PORTD+0
;lsb5.c,5 :: 		INTCON.GIE=1;
	BSF        INTCON+0, 7
;lsb5.c,6 :: 		INTCON.INTE=1;
	BSF        INTCON+0, 4
;lsb5.c,9 :: 		while(1)
L_main0:
;lsb5.c,11 :: 		PORTD=0b0010001;
	MOVLW      17
	MOVWF      PORTD+0
;lsb5.c,12 :: 		Delay_ms(1000);
	MOVLW      11
	MOVWF      R11+0
	MOVLW      38
	MOVWF      R12+0
	MOVLW      93
	MOVWF      R13+0
L_main2:
	DECFSZ     R13+0, 1
	GOTO       L_main2
	DECFSZ     R12+0, 1
	GOTO       L_main2
	DECFSZ     R11+0, 1
	GOTO       L_main2
	NOP
	NOP
;lsb5.c,13 :: 		PORTD=0b0000110;
	MOVLW      6
	MOVWF      PORTD+0
;lsb5.c,14 :: 		Delay_ms(1000);
	MOVLW      11
	MOVWF      R11+0
	MOVLW      38
	MOVWF      R12+0
	MOVLW      93
	MOVWF      R13+0
L_main3:
	DECFSZ     R13+0, 1
	GOTO       L_main3
	DECFSZ     R12+0, 1
	GOTO       L_main3
	DECFSZ     R11+0, 1
	GOTO       L_main3
	NOP
	NOP
;lsb5.c,16 :: 		}
	GOTO       L_main0
;lsb5.c,17 :: 		}
	GOTO       $+0
; end of _main

_interrupt:
	MOVWF      R15+0
	SWAPF      STATUS+0, 0
	CLRF       STATUS+0
	MOVWF      ___saveSTATUS+0
	MOVF       PCLATH+0, 0
	MOVWF      ___savePCLATH+0
	CLRF       PCLATH+0
;lsb5.c,18 :: 		void interrupt()
;lsb5.c,20 :: 		INTCON.GIE=0;
	BCF        INTCON+0, 7
;lsb5.c,21 :: 		if (INTCON.RBIF=0)
	BCF        INTCON+0, 0
	BTFSS      INTCON+0, 0
	GOTO       L_interrupt4
;lsb5.c,23 :: 		}
L_interrupt4:
;lsb5.c,24 :: 		if (INTCON.INTF)
	BTFSS      INTCON+0, 1
	GOTO       L_interrupt5
;lsb5.c,26 :: 		INTCON.INTF=0;
	BCF        INTCON+0, 1
;lsb5.c,27 :: 		PORTD=0b0100000;
	MOVLW      32
	MOVWF      PORTD+0
;lsb5.c,28 :: 		Delay_ms(10000);
	MOVLW      102
	MOVWF      R11+0
	MOVLW      118
	MOVWF      R12+0
	MOVLW      193
	MOVWF      R13+0
L_interrupt6:
	DECFSZ     R13+0, 1
	GOTO       L_interrupt6
	DECFSZ     R12+0, 1
	GOTO       L_interrupt6
	DECFSZ     R11+0, 1
	GOTO       L_interrupt6
;lsb5.c,30 :: 		}
L_interrupt5:
;lsb5.c,31 :: 		INTCON.GIE=1;
	BSF        INTCON+0, 7
;lsb5.c,32 :: 		}
L__interrupt7:
	MOVF       ___savePCLATH+0, 0
	MOVWF      PCLATH+0
	SWAPF      ___saveSTATUS+0, 0
	MOVWF      STATUS+0
	SWAPF      R15+0, 1
	SWAPF      R15+0, 0
	RETFIE
; end of _interrupt
