
_main:
;lab4_lcd.c,19 :: 		void main() {
;lab4_lcd.c,37 :: 		A=1;
	MOVLW      1
	MOVWF      main_A_L0+0
;lab4_lcd.c,38 :: 		B=0;
	CLRF       main_B_L0+0
;lab4_lcd.c,39 :: 		C=0;
	CLRF       main_C_L0+0
;lab4_lcd.c,40 :: 		D=0;
	CLRF       main_D_L0+0
;lab4_lcd.c,41 :: 		E=0;
	CLRF       main_E_L0+0
;lab4_lcd.c,42 :: 		TRISD.RD0=1;
	BSF        TRISD+0, 0
;lab4_lcd.c,43 :: 		TRISD.RD1=1;
	BSF        TRISD+0, 1
;lab4_lcd.c,44 :: 		TRISD.RD2=1;
	BSF        TRISD+0, 2
;lab4_lcd.c,45 :: 		TRISD.RD3=1;
	BSF        TRISD+0, 3
;lab4_lcd.c,46 :: 		TRISD.RD4=1;
	BSF        TRISD+0, 4
;lab4_lcd.c,48 :: 		Lcd_Init();
	CALL       _Lcd_Init+0
;lab4_lcd.c,50 :: 		while(1)
L_main0:
;lab4_lcd.c,52 :: 		if ((A==1)&&(PORTD.RD0==1))
	MOVF       main_A_L0+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main4
	BTFSS      PORTD+0, 0
	GOTO       L_main4
L__main26:
;lab4_lcd.c,53 :: 		{A=0; B=1; C=0; D=0; E=0;
	CLRF       main_A_L0+0
	MOVLW      1
	MOVWF      main_B_L0+0
	CLRF       main_C_L0+0
	CLRF       main_D_L0+0
	CLRF       main_E_L0+0
;lab4_lcd.c,54 :: 		}
L_main4:
;lab4_lcd.c,55 :: 		if ((B==1)&&(PORTD.RD1==1))
	MOVF       main_B_L0+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main7
	BTFSS      PORTD+0, 1
	GOTO       L_main7
L__main25:
;lab4_lcd.c,56 :: 		{A=0; B=0; C=1; D=0; E=0;
	CLRF       main_A_L0+0
	CLRF       main_B_L0+0
	MOVLW      1
	MOVWF      main_C_L0+0
	CLRF       main_D_L0+0
	CLRF       main_E_L0+0
;lab4_lcd.c,57 :: 		}
L_main7:
;lab4_lcd.c,58 :: 		if ((C==1)&&(PORTD.RD2==1))
	MOVF       main_C_L0+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main10
	BTFSS      PORTD+0, 2
	GOTO       L_main10
L__main24:
;lab4_lcd.c,59 :: 		{A=0; B=0; C=0; D=1; E=0;
	CLRF       main_A_L0+0
	CLRF       main_B_L0+0
	CLRF       main_C_L0+0
	MOVLW      1
	MOVWF      main_D_L0+0
	CLRF       main_E_L0+0
;lab4_lcd.c,60 :: 		}
L_main10:
;lab4_lcd.c,61 :: 		if ((D==1)&&(PORTD.RD3==1))
	MOVF       main_D_L0+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main13
	BTFSS      PORTD+0, 3
	GOTO       L_main13
L__main23:
;lab4_lcd.c,62 :: 		{A=0; B=0; C=0; D=0; E=1;
	CLRF       main_A_L0+0
	CLRF       main_B_L0+0
	CLRF       main_C_L0+0
	CLRF       main_D_L0+0
	MOVLW      1
	MOVWF      main_E_L0+0
;lab4_lcd.c,63 :: 		}
L_main13:
;lab4_lcd.c,64 :: 		if ((E==1)&&(PORTD.RD4==1))
	MOVF       main_E_L0+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main16
	BTFSS      PORTD+0, 4
	GOTO       L_main16
L__main22:
;lab4_lcd.c,65 :: 		{A=1; B=0; C=0; D=0; E=0;
	MOVLW      1
	MOVWF      main_A_L0+0
	CLRF       main_B_L0+0
	CLRF       main_C_L0+0
	CLRF       main_D_L0+0
	CLRF       main_E_L0+0
;lab4_lcd.c,66 :: 		}
L_main16:
;lab4_lcd.c,68 :: 		if (A==1)
	MOVF       main_A_L0+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main17
;lab4_lcd.c,69 :: 		{Lcd_Out(1,1,"SETEAZA");
	MOVLW      1
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      1
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      ?lstr1_lab4_lcd+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
;lab4_lcd.c,70 :: 		Lcd_Out(2,1,"OK");}
	MOVLW      2
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      1
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      ?lstr2_lab4_lcd+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
L_main17:
;lab4_lcd.c,71 :: 		if (B==1)
	MOVF       main_B_L0+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main18
;lab4_lcd.c,72 :: 		{PORTD.RD4=1;
	BSF        PORTD+0, 4
;lab4_lcd.c,73 :: 		Lcd_Out(1,1,"APRINDE");
	MOVLW      1
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      1
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      ?lstr3_lab4_lcd+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
;lab4_lcd.c,74 :: 		Lcd_Out(2,1,"OK C >");}
	MOVLW      2
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      1
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      ?lstr4_lab4_lcd+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
L_main18:
;lab4_lcd.c,75 :: 		if (C==1)
	MOVF       main_C_L0+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main19
;lab4_lcd.c,76 :: 		{PORTD.RD4=1;
	BSF        PORTD+0, 4
;lab4_lcd.c,77 :: 		Lcd_Out(1,1,"STINGE ");
	MOVLW      1
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      1
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      ?lstr5_lab4_lcd+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
;lab4_lcd.c,78 :: 		Lcd_Out(2,1,"OK C <");}
	MOVLW      2
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      1
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      ?lstr6_lab4_lcd+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
L_main19:
;lab4_lcd.c,79 :: 		if (D==1)
	MOVF       main_D_L0+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main20
;lab4_lcd.c,80 :: 		{TRISD.RD4=1;
	BSF        TRISD+0, 4
;lab4_lcd.c,81 :: 		Lcd_Out(1,1,"APRINS");
	MOVLW      1
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      1
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      ?lstr7_lab4_lcd+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
;lab4_lcd.c,82 :: 		Lcd_Out(2,1,"OK");}
	MOVLW      2
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      1
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      ?lstr8_lab4_lcd+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
L_main20:
;lab4_lcd.c,83 :: 		if (D==1)
	MOVF       main_D_L0+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main21
;lab4_lcd.c,84 :: 		{PORTD.RD4=1;
	BSF        PORTD+0, 4
;lab4_lcd.c,85 :: 		Lcd_Out(1,1,"STINS  ");
	MOVLW      1
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      1
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      ?lstr9_lab4_lcd+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
;lab4_lcd.c,86 :: 		Lcd_Out(2,1,"OK");}
	MOVLW      2
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      1
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      ?lstr10_lab4_lcd+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
L_main21:
;lab4_lcd.c,90 :: 		}
	GOTO       L_main0
;lab4_lcd.c,91 :: 		}
	GOTO       $+0
; end of _main
