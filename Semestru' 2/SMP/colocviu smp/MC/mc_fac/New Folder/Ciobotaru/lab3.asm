
_main:
	CLRF       main_t_L0+0
	CLRF       main_t_L0+1
;lab3.c,1 :: 		void main() {   int t=0;
;lab3.c,2 :: 		TRISA=0;
	CLRF       TRISA+0
;lab3.c,3 :: 		TRISB=0    ;
	CLRF       TRISB+0
;lab3.c,4 :: 		PORTA=1;
	MOVLW      1
	MOVWF      PORTA+0
;lab3.c,5 :: 		while(1){
L_main0:
;lab3.c,6 :: 		PORTB=0b11101111;
	MOVLW      239
	MOVWF      PORTB+0
;lab3.c,7 :: 		Delay_ms(200);   PORTB=0;
	MOVLW      3
	MOVWF      R11+0
	MOVLW      8
	MOVWF      R12+0
	MOVLW      119
	MOVWF      R13+0
L_main2:
	DECFSZ     R13+0, 1
	GOTO       L_main2
	DECFSZ     R12+0, 1
	GOTO       L_main2
	DECFSZ     R11+0, 1
	GOTO       L_main2
	CLRF       PORTB+0
;lab3.c,8 :: 		PORTB=0b11111111;
	MOVLW      255
	MOVWF      PORTB+0
;lab3.c,9 :: 		Delay_ms(200);   PORTB=0;
	MOVLW      3
	MOVWF      R11+0
	MOVLW      8
	MOVWF      R12+0
	MOVLW      119
	MOVWF      R13+0
L_main3:
	DECFSZ     R13+0, 1
	GOTO       L_main3
	DECFSZ     R12+0, 1
	GOTO       L_main3
	DECFSZ     R11+0, 1
	GOTO       L_main3
	CLRF       PORTB+0
;lab3.c,10 :: 		PORTB=0b10001111;
	MOVLW      143
	MOVWF      PORTB+0
;lab3.c,11 :: 		Delay_ms(200);     PORTB=0;
	MOVLW      3
	MOVWF      R11+0
	MOVLW      8
	MOVWF      R12+0
	MOVLW      119
	MOVWF      R13+0
L_main4:
	DECFSZ     R13+0, 1
	GOTO       L_main4
	DECFSZ     R12+0, 1
	GOTO       L_main4
	DECFSZ     R11+0, 1
	GOTO       L_main4
	CLRF       PORTB+0
;lab3.c,12 :: 		PORTB=0b11111101;
	MOVLW      253
	MOVWF      PORTB+0
;lab3.c,13 :: 		Delay_ms(200);         PORTB=0;
	MOVLW      3
	MOVWF      R11+0
	MOVLW      8
	MOVWF      R12+0
	MOVLW      119
	MOVWF      R13+0
L_main5:
	DECFSZ     R13+0, 1
	GOTO       L_main5
	DECFSZ     R12+0, 1
	GOTO       L_main5
	DECFSZ     R11+0, 1
	GOTO       L_main5
	CLRF       PORTB+0
;lab3.c,14 :: 		PORTB=0b11101101;
	MOVLW      237
	MOVWF      PORTB+0
;lab3.c,15 :: 		Delay_ms(200);       PORTB=0;
	MOVLW      3
	MOVWF      R11+0
	MOVLW      8
	MOVWF      R12+0
	MOVLW      119
	MOVWF      R13+0
L_main6:
	DECFSZ     R13+0, 1
	GOTO       L_main6
	DECFSZ     R12+0, 1
	GOTO       L_main6
	DECFSZ     R11+0, 1
	GOTO       L_main6
	CLRF       PORTB+0
;lab3.c,16 :: 		PORTB=0b11100110;
	MOVLW      230
	MOVWF      PORTB+0
;lab3.c,17 :: 		Delay_ms(200);        PORTB=0;
	MOVLW      3
	MOVWF      R11+0
	MOVLW      8
	MOVWF      R12+0
	MOVLW      119
	MOVWF      R13+0
L_main7:
	DECFSZ     R13+0, 1
	GOTO       L_main7
	DECFSZ     R12+0, 1
	GOTO       L_main7
	DECFSZ     R11+0, 1
	GOTO       L_main7
	CLRF       PORTB+0
;lab3.c,18 :: 		PORTB=0b11001111;
	MOVLW      207
	MOVWF      PORTB+0
;lab3.c,19 :: 		Delay_ms(200);          PORTB=0;
	MOVLW      3
	MOVWF      R11+0
	MOVLW      8
	MOVWF      R12+0
	MOVLW      119
	MOVWF      R13+0
L_main8:
	DECFSZ     R13+0, 1
	GOTO       L_main8
	DECFSZ     R12+0, 1
	GOTO       L_main8
	DECFSZ     R11+0, 1
	GOTO       L_main8
	CLRF       PORTB+0
;lab3.c,20 :: 		PORTB=0b11011011;
	MOVLW      219
	MOVWF      PORTB+0
;lab3.c,21 :: 		Delay_ms(200);       PORTB=0;
	MOVLW      3
	MOVWF      R11+0
	MOVLW      8
	MOVWF      R12+0
	MOVLW      119
	MOVWF      R13+0
L_main9:
	DECFSZ     R13+0, 1
	GOTO       L_main9
	DECFSZ     R12+0, 1
	GOTO       L_main9
	DECFSZ     R11+0, 1
	GOTO       L_main9
	CLRF       PORTB+0
;lab3.c,22 :: 		PORTB=0b00000110;     Delay_ms(200);
	MOVLW      6
	MOVWF      PORTB+0
	MOVLW      3
	MOVWF      R11+0
	MOVLW      8
	MOVWF      R12+0
	MOVLW      119
	MOVWF      R13+0
L_main10:
	DECFSZ     R13+0, 1
	GOTO       L_main10
	DECFSZ     R12+0, 1
	GOTO       L_main10
	DECFSZ     R11+0, 1
	GOTO       L_main10
;lab3.c,23 :: 		if(t==0) {PORTA=2;t++;}
	MOVLW      0
	XORWF      main_t_L0+1, 0
	BTFSS      STATUS+0, 2
	GOTO       L__main15
	MOVLW      0
	XORWF      main_t_L0+0, 0
L__main15:
	BTFSS      STATUS+0, 2
	GOTO       L_main11
	MOVLW      2
	MOVWF      PORTA+0
	INCF       main_t_L0+0, 1
	BTFSC      STATUS+0, 2
	INCF       main_t_L0+1, 1
	GOTO       L_main12
L_main11:
;lab3.c,24 :: 		else {PORTA=4;Delay_ms(200);t=0; Delay_ms(200); TRISB=0    ; }
	MOVLW      4
	MOVWF      PORTA+0
	MOVLW      3
	MOVWF      R11+0
	MOVLW      8
	MOVWF      R12+0
	MOVLW      119
	MOVWF      R13+0
L_main13:
	DECFSZ     R13+0, 1
	GOTO       L_main13
	DECFSZ     R12+0, 1
	GOTO       L_main13
	DECFSZ     R11+0, 1
	GOTO       L_main13
	CLRF       main_t_L0+0
	CLRF       main_t_L0+1
	MOVLW      3
	MOVWF      R11+0
	MOVLW      8
	MOVWF      R12+0
	MOVLW      119
	MOVWF      R13+0
L_main14:
	DECFSZ     R13+0, 1
	GOTO       L_main14
	DECFSZ     R12+0, 1
	GOTO       L_main14
	DECFSZ     R11+0, 1
	GOTO       L_main14
	CLRF       TRISB+0
L_main12:
;lab3.c,25 :: 		}
	GOTO       L_main0
;lab3.c,26 :: 		}
	GOTO       $+0
; end of _main
