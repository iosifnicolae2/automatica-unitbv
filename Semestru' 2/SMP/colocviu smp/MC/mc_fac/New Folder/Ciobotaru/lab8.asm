
_main:
;lab8.c,1 :: 		void main() {
;lab8.c,2 :: 		TRISA=0b00000000;
	CLRF       TRISA+0
;lab8.c,3 :: 		TRISD=0;
	CLRF       TRISD+0
;lab8.c,4 :: 		TRISB=1;
	MOVLW      1
	MOVWF      TRISB+0
;lab8.c,5 :: 		PORTA=0x00;
	CLRF       PORTA+0
;lab8.c,6 :: 		INTCON.GIE=1;
	BSF        INTCON+0, 7
;lab8.c,7 :: 		INTCON.INTE=1;
	BSF        INTCON+0, 4
;lab8.c,8 :: 		INTCON.INTEDG=1;
	BSF        INTCON+0, 6
;lab8.c,9 :: 		while(1)
L_main0:
;lab8.c,11 :: 		PORTA=0b00100001;
	MOVLW      33
	MOVWF      PORTA+0
;lab8.c,15 :: 		}
	GOTO       L_main0
;lab8.c,17 :: 		}
	GOTO       $+0
; end of _main

_interrupt:
	MOVWF      R15+0
	SWAPF      STATUS+0, 0
	CLRF       STATUS+0
	MOVWF      ___saveSTATUS+0
	MOVF       PCLATH+0, 0
	MOVWF      ___savePCLATH+0
	CLRF       PCLATH+0
;lab8.c,19 :: 		void interrupt()
;lab8.c,21 :: 		INTCON.GIE=0;
	BCF        INTCON+0, 7
;lab8.c,23 :: 		if(INTCON.INTF)
	BTFSS      INTCON+0, 1
	GOTO       L_interrupt2
;lab8.c,25 :: 		INTCON.INTF=0;
	BCF        INTCON+0, 1
;lab8.c,26 :: 		PORTA=0b00000001;
	MOVLW      1
	MOVWF      PORTA+0
;lab8.c,27 :: 		delay_ms(5000);
	MOVLW      51
	MOVWF      R11+0
	MOVLW      187
	MOVWF      R12+0
	MOVLW      223
	MOVWF      R13+0
L_interrupt3:
	DECFSZ     R13+0, 1
	GOTO       L_interrupt3
	DECFSZ     R12+0, 1
	GOTO       L_interrupt3
	DECFSZ     R11+0, 1
	GOTO       L_interrupt3
	NOP
	NOP
;lab8.c,28 :: 		}
L_interrupt2:
;lab8.c,29 :: 		INTCON.GIE=1;
	BSF        INTCON+0, 7
;lab8.c,30 :: 		}
L__interrupt4:
	MOVF       ___savePCLATH+0, 0
	MOVWF      PCLATH+0
	SWAPF      ___saveSTATUS+0, 0
	MOVWF      STATUS+0
	SWAPF      R15+0, 1
	SWAPF      R15+0, 0
	RETFIE
; end of _interrupt
