
_initializare:
;lab_11.c,1 :: 		void initializare()
;lab_11.c,3 :: 		CMCON0.C2OUT=0;
	BCF        CMCON0+0, 7
;lab_11.c,4 :: 		CMCON0.C1OUT=0;
	BCF        CMCON0+0, 6
;lab_11.c,5 :: 		CMCON0.C2INV=0;
	BCF        CMCON0+0, 5
;lab_11.c,6 :: 		CMCON0.C1INV=0;
	BCF        CMCON0+0, 4
;lab_11.c,7 :: 		CMCON0.CIS=0;
	BCF        CMCON0+0, 3
;lab_11.c,8 :: 		CMCON0.CM2=0;
	BCF        CMCON0+0, 2
;lab_11.c,9 :: 		CMCON0.CM1=1;
	BSF        CMCON0+0, 1
;lab_11.c,10 :: 		CMCON0.CM0=0;
	BCF        CMCON0+0, 0
;lab_11.c,12 :: 		VRCON.VREN=1;
	BSF        VRCON+0, 7
;lab_11.c,13 :: 		VRCON.VRR=1;
	BSF        VRCON+0, 5
;lab_11.c,14 :: 		VRCON.VR3=1;
	BSF        VRCON+0, 3
;lab_11.c,15 :: 		VRCON.VR2=0;
	BCF        VRCON+0, 2
;lab_11.c,16 :: 		VRCON.VR1=1;
	BSF        VRCON+0, 1
;lab_11.c,17 :: 		VRCON.VR0=0;
	BCF        VRCON+0, 0
;lab_11.c,19 :: 		TRISB=0;
	CLRF       TRISB+0
;lab_11.c,20 :: 		}
	RETURN
; end of _initializare

_main:
;lab_11.c,21 :: 		void main()
;lab_11.c,23 :: 		initializare();
	CALL       _initializare+0
;lab_11.c,24 :: 		PORTB=0;
	CLRF       PORTB+0
;lab_11.c,25 :: 		while(1)
L_main0:
;lab_11.c,27 :: 		PORTB.RB0=CMCON0.C1OUT;
	BTFSC      CMCON0+0, 6
	GOTO       L__main3
	BCF        PORTB+0, 0
	GOTO       L__main4
L__main3:
	BSF        PORTB+0, 0
L__main4:
;lab_11.c,28 :: 		DELAY_MS(100);
	MOVLW      2
	MOVWF      R11+0
	MOVLW      4
	MOVWF      R12+0
	MOVLW      186
	MOVWF      R13+0
L_main2:
	DECFSZ     R13+0, 1
	GOTO       L_main2
	DECFSZ     R12+0, 1
	GOTO       L_main2
	DECFSZ     R11+0, 1
	GOTO       L_main2
	NOP
;lab_11.c,29 :: 		}
	GOTO       L_main0
;lab_11.c,30 :: 		}
	GOTO       $+0
; end of _main
