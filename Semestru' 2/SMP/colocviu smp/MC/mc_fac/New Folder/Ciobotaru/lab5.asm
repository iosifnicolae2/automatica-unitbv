
_main:
;lab5.c,17 :: 		void main() {
;lab5.c,25 :: 		TRISD.RD0=1;
	BSF        TRISD+0, 0
;lab5.c,26 :: 		TRISD.RD1=1;
	BSF        TRISD+0, 1
;lab5.c,27 :: 		TRISD.RD2=1;
	BSF        TRISD+0, 2
;lab5.c,28 :: 		TRISD.RD3=1;
	BSF        TRISD+0, 3
;lab5.c,29 :: 		TRISD.RD4=0;   PORTD.RD4=0;
	BCF        TRISD+0, 4
	BCF        PORTD+0, 4
;lab5.c,30 :: 		A=1;B=0;C=0;D=0;E=0;
	MOVLW      1
	MOVWF      main_A_L0+0
	CLRF       main_B_L0+0
	CLRF       main_C_L0+0
	CLRF       main_D_L0+0
	CLRF       main_E_L0+0
;lab5.c,32 :: 		Lcd_Init();
	CALL       _Lcd_Init+0
;lab5.c,34 :: 		while(1)
L_main0:
;lab5.c,36 :: 		if((A==1)&&(PORTD.RD0==1)) {A=0;B=1;C=0;D=0;E=0;}
	MOVF       main_A_L0+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main4
	BTFSS      PORTD+0, 0
	GOTO       L_main4
L__main26:
	CLRF       main_A_L0+0
	MOVLW      1
	MOVWF      main_B_L0+0
	CLRF       main_C_L0+0
	CLRF       main_D_L0+0
	CLRF       main_E_L0+0
L_main4:
;lab5.c,37 :: 		if((B==1)&&(PORTD.RD1==1)) {A=0;B=0;C=1;D=0;E=0;}
	MOVF       main_B_L0+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main7
	BTFSS      PORTD+0, 1
	GOTO       L_main7
L__main25:
	CLRF       main_A_L0+0
	CLRF       main_B_L0+0
	MOVLW      1
	MOVWF      main_C_L0+0
	CLRF       main_D_L0+0
	CLRF       main_E_L0+0
L_main7:
;lab5.c,38 :: 		if((C==1)&&(PORTD.RD2==1)) {A=0;B=0;C=0;D=1;E=0;}
	MOVF       main_C_L0+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main10
	BTFSS      PORTD+0, 2
	GOTO       L_main10
L__main24:
	CLRF       main_A_L0+0
	CLRF       main_B_L0+0
	CLRF       main_C_L0+0
	MOVLW      1
	MOVWF      main_D_L0+0
	CLRF       main_E_L0+0
L_main10:
;lab5.c,39 :: 		if((D==1)&&(PORTD.RD3==1)) {A=0;B=0;C=0;D=0;E=1;}
	MOVF       main_D_L0+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main13
	BTFSS      PORTD+0, 3
	GOTO       L_main13
L__main23:
	CLRF       main_A_L0+0
	CLRF       main_B_L0+0
	CLRF       main_C_L0+0
	CLRF       main_D_L0+0
	MOVLW      1
	MOVWF      main_E_L0+0
L_main13:
;lab5.c,40 :: 		if((E==1)&&(PORTD.RD4==1)) {A=1;B=0;C=0;D=0;E=0;}
	MOVF       main_E_L0+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main16
	BTFSS      PORTD+0, 4
	GOTO       L_main16
L__main22:
	MOVLW      1
	MOVWF      main_A_L0+0
	CLRF       main_B_L0+0
	CLRF       main_C_L0+0
	CLRF       main_D_L0+0
	CLRF       main_E_L0+0
L_main16:
;lab5.c,42 :: 		if(A==1) {Lcd_Out(1,1,"seteaza rb4  ");Lcd_Out(2,1,"ok       ");}
	MOVF       main_A_L0+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main17
	MOVLW      1
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      1
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      ?lstr1_lab5+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
	MOVLW      2
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      1
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      ?lstr2_lab5+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
L_main17:
;lab5.c,43 :: 		if(B==1) {PORTD.RD4=1;Lcd_Out(1,1,"aprinde LED       ");Lcd_Out(2,1,"cancel ok  >    ");}
	MOVF       main_B_L0+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main18
	BSF        PORTD+0, 4
	MOVLW      1
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      1
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      ?lstr3_lab5+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
	MOVLW      2
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      1
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      ?lstr4_lab5+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
L_main18:
;lab5.c,44 :: 		if(C==1) {PORTD.RD4=0;Lcd_Out(1,1,"stinge LED      ");Lcd_Out(2,1,"cancel ok  <    ");}
	MOVF       main_C_L0+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main19
	BCF        PORTD+0, 4
	MOVLW      1
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      1
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      ?lstr5_lab5+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
	MOVLW      2
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      1
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      ?lstr6_lab5+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
L_main19:
;lab5.c,45 :: 		if(D==1) {PORTD.RD4=1;Lcd_Out(1,1,"LED apris      ");Lcd_Out(2,1,"ok      ");}
	MOVF       main_D_L0+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main20
	BSF        PORTD+0, 4
	MOVLW      1
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      1
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      ?lstr7_lab5+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
	MOVLW      2
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      1
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      ?lstr8_lab5+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
L_main20:
;lab5.c,46 :: 		if(E==1) {PORTD.RD4=0;Lcd_Out(1,1,"LED stins     "  );Lcd_Out(2,1,"ok      ");}
	MOVF       main_E_L0+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main21
	BCF        PORTD+0, 4
	MOVLW      1
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      1
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      ?lstr9_lab5+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
	MOVLW      2
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      1
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      ?lstr10_lab5+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
L_main21:
;lab5.c,47 :: 		}
	GOTO       L_main0
;lab5.c,69 :: 		}
	GOTO       $+0
; end of _main
