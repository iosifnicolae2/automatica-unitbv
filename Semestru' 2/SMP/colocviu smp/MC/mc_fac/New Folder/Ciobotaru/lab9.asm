
_interrupt:
	MOVWF      R15+0
	SWAPF      STATUS+0, 0
	CLRF       STATUS+0
	MOVWF      ___saveSTATUS+0
	MOVF       PCLATH+0, 0
	MOVWF      ___savePCLATH+0
	CLRF       PCLATH+0
;lab9.c,21 :: 		void interrupt()
;lab9.c,22 :: 		{INTCON.GIE=0;
	BCF        INTCON+0, 7
;lab9.c,23 :: 		if(INTCON.T0IF)
	BTFSS      INTCON+0, 2
	GOTO       L_interrupt0
;lab9.c,24 :: 		{sec++;
	INCF       _sec+0, 1
	BTFSC      STATUS+0, 2
	INCF       _sec+1, 1
;lab9.c,25 :: 		if(sec==50)
	MOVLW      0
	XORWF      _sec+1, 0
	BTFSS      STATUS+0, 2
	GOTO       L__interrupt8
	MOVLW      50
	XORWF      _sec+0, 0
L__interrupt8:
	BTFSS      STATUS+0, 2
	GOTO       L_interrupt1
;lab9.c,28 :: 		TMR0=99;
	MOVLW      99
	MOVWF      TMR0+0
;lab9.c,29 :: 		INTCON.T0IF=0;
	BCF        INTCON+0, 2
;lab9.c,30 :: 		if(sec=60)
	MOVLW      60
	MOVWF      _sec+0
	MOVLW      0
	MOVWF      _sec+1
;lab9.c,32 :: 		sec=0;
	CLRF       _sec+0
	CLRF       _sec+1
;lab9.c,33 :: 		m++;
	INCF       _m+0, 1
	BTFSC      STATUS+0, 2
	INCF       _m+1, 1
;lab9.c,34 :: 		if(m>59)
	MOVLW      128
	MOVWF      R0+0
	MOVLW      128
	XORWF      _m+1, 0
	SUBWF      R0+0, 0
	BTFSS      STATUS+0, 2
	GOTO       L__interrupt9
	MOVF       _m+0, 0
	SUBLW      59
L__interrupt9:
	BTFSC      STATUS+0, 0
	GOTO       L_interrupt3
;lab9.c,36 :: 		m=0;
	CLRF       _m+0
	CLRF       _m+1
;lab9.c,37 :: 		h++;
	INCF       _h+0, 1
	BTFSC      STATUS+0, 2
	INCF       _h+1, 1
;lab9.c,38 :: 		if(h>23) h=0;
	MOVLW      128
	MOVWF      R0+0
	MOVLW      128
	XORWF      _h+1, 0
	SUBWF      R0+0, 0
	BTFSS      STATUS+0, 2
	GOTO       L__interrupt10
	MOVF       _h+0, 0
	SUBLW      23
L__interrupt10:
	BTFSC      STATUS+0, 0
	GOTO       L_interrupt4
	CLRF       _h+0
	CLRF       _h+1
L_interrupt4:
;lab9.c,39 :: 		}
L_interrupt3:
;lab9.c,41 :: 		}
L_interrupt1:
;lab9.c,42 :: 		}
L_interrupt0:
;lab9.c,43 :: 		INTCON.GIE=1;
	BSF        INTCON+0, 7
;lab9.c,44 :: 		}
L__interrupt7:
	MOVF       ___savePCLATH+0, 0
	MOVWF      PCLATH+0
	SWAPF      ___saveSTATUS+0, 0
	MOVWF      STATUS+0
	SWAPF      R15+0, 1
	SWAPF      R15+0, 0
	RETFIE
; end of _interrupt

_main:
;lab9.c,46 :: 		void main() {
;lab9.c,48 :: 		INTCON=0b10100000;
	MOVLW      160
	MOVWF      INTCON+0
;lab9.c,49 :: 		OPTION_REG=0b00000101;
	MOVLW      5
	MOVWF      OPTION_REG+0
;lab9.c,50 :: 		TMR0=99;
	MOVLW      99
	MOVWF      TMR0+0
;lab9.c,51 :: 		LCD_Init();
	CALL       _Lcd_Init+0
;lab9.c,52 :: 		while(1)
L_main5:
;lab9.c,53 :: 		{      sir[0]='0'+sec/10;
	MOVLW      10
	MOVWF      R4+0
	MOVLW      0
	MOVWF      R4+1
	MOVF       _sec+0, 0
	MOVWF      R0+0
	MOVF       _sec+1, 0
	MOVWF      R0+1
	CALL       _Div_16x16_S+0
	MOVF       R0+0, 0
	ADDLW      48
	MOVWF      main_sir_L0+0
;lab9.c,54 :: 		sec=sec%10;
	MOVLW      10
	MOVWF      R4+0
	MOVLW      0
	MOVWF      R4+1
	MOVF       _sec+0, 0
	MOVWF      R0+0
	MOVF       _sec+1, 0
	MOVWF      R0+1
	CALL       _Div_16x16_S+0
	MOVF       R8+0, 0
	MOVWF      R0+0
	MOVF       R8+1, 0
	MOVWF      R0+1
	MOVF       R0+0, 0
	MOVWF      _sec+0
	MOVF       R0+1, 0
	MOVWF      _sec+1
;lab9.c,55 :: 		LCD_Out(1,1,sir);
	MOVLW      1
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      1
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      main_sir_L0+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
;lab9.c,57 :: 		sir[1]='0'+sec/10;
	MOVLW      10
	MOVWF      R4+0
	MOVLW      0
	MOVWF      R4+1
	MOVF       _sec+0, 0
	MOVWF      R0+0
	MOVF       _sec+1, 0
	MOVWF      R0+1
	CALL       _Div_16x16_S+0
	MOVF       R0+0, 0
	ADDLW      48
	MOVWF      main_sir_L0+1
;lab9.c,58 :: 		sec=sec%10;
	MOVLW      10
	MOVWF      R4+0
	MOVLW      0
	MOVWF      R4+1
	MOVF       _sec+0, 0
	MOVWF      R0+0
	MOVF       _sec+1, 0
	MOVWF      R0+1
	CALL       _Div_16x16_S+0
	MOVF       R8+0, 0
	MOVWF      R0+0
	MOVF       R8+1, 0
	MOVWF      R0+1
	MOVF       R0+0, 0
	MOVWF      _sec+0
	MOVF       R0+1, 0
	MOVWF      _sec+1
;lab9.c,59 :: 		LCD_Out(1,5,sir);
	MOVLW      1
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      5
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      main_sir_L0+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
;lab9.c,61 :: 		sir[2]='0'+m/10;
	MOVLW      10
	MOVWF      R4+0
	MOVLW      0
	MOVWF      R4+1
	MOVF       _m+0, 0
	MOVWF      R0+0
	MOVF       _m+1, 0
	MOVWF      R0+1
	CALL       _Div_16x16_S+0
	MOVF       R0+0, 0
	ADDLW      48
	MOVWF      main_sir_L0+2
;lab9.c,62 :: 		m=m%10;
	MOVLW      10
	MOVWF      R4+0
	MOVLW      0
	MOVWF      R4+1
	MOVF       _m+0, 0
	MOVWF      R0+0
	MOVF       _m+1, 0
	MOVWF      R0+1
	CALL       _Div_16x16_S+0
	MOVF       R8+0, 0
	MOVWF      R0+0
	MOVF       R8+1, 0
	MOVWF      R0+1
	MOVF       R0+0, 0
	MOVWF      _m+0
	MOVF       R0+1, 0
	MOVWF      _m+1
;lab9.c,63 :: 		LCD_Out(1,8,sir);
	MOVLW      1
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      8
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      main_sir_L0+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
;lab9.c,65 :: 		sir[3]='0'+m/10;
	MOVLW      10
	MOVWF      R4+0
	MOVLW      0
	MOVWF      R4+1
	MOVF       _m+0, 0
	MOVWF      R0+0
	MOVF       _m+1, 0
	MOVWF      R0+1
	CALL       _Div_16x16_S+0
	MOVF       R0+0, 0
	ADDLW      48
	MOVWF      main_sir_L0+3
;lab9.c,66 :: 		m=m%10;
	MOVLW      10
	MOVWF      R4+0
	MOVLW      0
	MOVWF      R4+1
	MOVF       _m+0, 0
	MOVWF      R0+0
	MOVF       _m+1, 0
	MOVWF      R0+1
	CALL       _Div_16x16_S+0
	MOVF       R8+0, 0
	MOVWF      R0+0
	MOVF       R8+1, 0
	MOVWF      R0+1
	MOVF       R0+0, 0
	MOVWF      _m+0
	MOVF       R0+1, 0
	MOVWF      _m+1
;lab9.c,67 :: 		LCD_Out(1,10,sir);
	MOVLW      1
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      10
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      main_sir_L0+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
;lab9.c,69 :: 		sir[4]='0'+h/10;
	MOVLW      10
	MOVWF      R4+0
	MOVLW      0
	MOVWF      R4+1
	MOVF       _h+0, 0
	MOVWF      R0+0
	MOVF       _h+1, 0
	MOVWF      R0+1
	CALL       _Div_16x16_S+0
	MOVF       R0+0, 0
	ADDLW      48
	MOVWF      main_sir_L0+4
;lab9.c,70 :: 		h=h%10;
	MOVLW      10
	MOVWF      R4+0
	MOVLW      0
	MOVWF      R4+1
	MOVF       _h+0, 0
	MOVWF      R0+0
	MOVF       _h+1, 0
	MOVWF      R0+1
	CALL       _Div_16x16_S+0
	MOVF       R8+0, 0
	MOVWF      R0+0
	MOVF       R8+1, 0
	MOVWF      R0+1
	MOVF       R0+0, 0
	MOVWF      _h+0
	MOVF       R0+1, 0
	MOVWF      _h+1
;lab9.c,71 :: 		LCD_Out(1,15,sir);
	MOVLW      1
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      15
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      main_sir_L0+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
;lab9.c,73 :: 		sir[5]='0'+h/10;
	MOVLW      10
	MOVWF      R4+0
	MOVLW      0
	MOVWF      R4+1
	MOVF       _h+0, 0
	MOVWF      R0+0
	MOVF       _h+1, 0
	MOVWF      R0+1
	CALL       _Div_16x16_S+0
	MOVF       R0+0, 0
	ADDLW      48
	MOVWF      main_sir_L0+5
;lab9.c,74 :: 		h=h%10;
	MOVLW      10
	MOVWF      R4+0
	MOVLW      0
	MOVWF      R4+1
	MOVF       _h+0, 0
	MOVWF      R0+0
	MOVF       _h+1, 0
	MOVWF      R0+1
	CALL       _Div_16x16_S+0
	MOVF       R8+0, 0
	MOVWF      R0+0
	MOVF       R8+1, 0
	MOVWF      R0+1
	MOVF       R0+0, 0
	MOVWF      _h+0
	MOVF       R0+1, 0
	MOVWF      _h+1
;lab9.c,75 :: 		LCD_Out(1,13,sir);
	MOVLW      1
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      13
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      main_sir_L0+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
;lab9.c,76 :: 		}
	GOTO       L_main5
;lab9.c,78 :: 		}
	GOTO       $+0
; end of _main
