#line 1 "C:/Documents and Settings/student/My Documents/Ciobotaru/lab9.c"
int h=0;
int m=0;
int sec=0;


sbit LCD_RS at RB4_bit;
sbit LCD_EN at RB5_bit;
sbit LCD_D4 at RB0_bit;
sbit LCD_D5 at RB1_bit;
sbit LCD_D6 at RB2_bit;
sbit LCD_D7 at RB3_bit;

sbit LCD_RS_Direction at TRISB4_bit;
sbit LCD_EN_Direction at TRISB5_bit;
sbit LCD_D4_Direction at TRISB0_bit;
sbit LCD_D5_Direction at TRISB1_bit;
sbit LCD_D6_Direction at TRISB2_bit;
sbit LCD_D7_Direction at TRISB3_bit;


void interrupt()
 {INTCON.GIE=0;
 if(INTCON.T0IF)
 {sec++;
 if(sec==50)
 {

 TMR0=99;
 INTCON.T0IF=0;
 if(sec=60)
 {
 sec=0;
 m++;
 if(m>59)
 {
 m=0;
 h++;
 if(h>23) h=0;
 }
 }
 }
 }
 INTCON.GIE=1;
 }

void main() {
char sir[15];
 INTCON=0b10100000;
 OPTION_REG=0b00000101;
 TMR0=99;
 LCD_Init();
 while(1)
 { sir[0]='0'+sec/10;
 sec=sec%10;
 LCD_Out(1,1,sir);

 sir[1]='0'+sec/10;
 sec=sec%10;
 LCD_Out(1,5,sir);

 sir[2]='0'+m/10;
 m=m%10;
 LCD_Out(1,8,sir);

 sir[3]='0'+m/10;
 m=m%10;
 LCD_Out(1,10,sir);

 sir[4]='0'+h/10;
 h=h%10;
 LCD_Out(1,15,sir);

 sir[5]='0'+h/10;
 h=h%10;
 LCD_Out(1,13,sir);
 }

}
