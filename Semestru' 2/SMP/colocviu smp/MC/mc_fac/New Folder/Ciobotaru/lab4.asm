
_main:
;lab4.c,1 :: 		void main() {
;lab4.c,8 :: 		TRISD.RD0=1;
	BSF        TRISD+0, 0
;lab4.c,9 :: 		TRISD.RD1=1;
	BSF        TRISD+0, 1
;lab4.c,10 :: 		TRISD.RD2=1;
	BSF        TRISD+0, 2
;lab4.c,11 :: 		TRISD.RD3=1;
	BSF        TRISD+0, 3
;lab4.c,12 :: 		TRISD.RD4=1;
	BSF        TRISD+0, 4
;lab4.c,15 :: 		TRISA.RA0=0;
	BCF        TRISA+0, 0
;lab4.c,16 :: 		TRISA.RA1=0;
	BCF        TRISA+0, 1
;lab4.c,17 :: 		TRISA.RA2=0;
	BCF        TRISA+0, 2
;lab4.c,18 :: 		TRISA.RA3=0;
	BCF        TRISA+0, 3
;lab4.c,19 :: 		TRISA.RA4=0;
	BCF        TRISA+0, 4
;lab4.c,21 :: 		A=1;
	MOVLW      1
	MOVWF      R1+0
;lab4.c,22 :: 		B=0;
	CLRF       R2+0
;lab4.c,23 :: 		C=0;
	CLRF       R3+0
;lab4.c,24 :: 		D=0;
	CLRF       R4+0
;lab4.c,25 :: 		E=0;
	CLRF       R5+0
;lab4.c,27 :: 		while(1)
L_main0:
;lab4.c,29 :: 		if((A==1)&&(PORTD.RD0==1)){A=0;B=1;C=0;D=0;E=0;}
	MOVF       R1+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main4
	BTFSS      PORTD+0, 0
	GOTO       L_main4
L__main26:
	CLRF       R1+0
	MOVLW      1
	MOVWF      R2+0
	CLRF       R3+0
	CLRF       R4+0
	CLRF       R5+0
L_main4:
;lab4.c,30 :: 		if((B==1)&&(PORTD.RD2==1)){B=0;C=1;D=0;E=0;}
	MOVF       R2+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main7
	BTFSS      PORTD+0, 2
	GOTO       L_main7
L__main25:
	CLRF       R2+0
	MOVLW      1
	MOVWF      R3+0
	CLRF       R4+0
	CLRF       R5+0
L_main7:
;lab4.c,31 :: 		if((C==1)&&(PORTD.RD1==1)){C=0;A=1;D=0;E=0;}
	MOVF       R3+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main10
	BTFSS      PORTD+0, 1
	GOTO       L_main10
L__main24:
	CLRF       R3+0
	MOVLW      1
	MOVWF      R1+0
	CLRF       R4+0
	CLRF       R5+0
L_main10:
;lab4.c,32 :: 		if((D==1)&&(PORTD.RD3==1)){D=0;E=1;A=0;B=0;C=0;}
	MOVF       R4+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main13
	BTFSS      PORTD+0, 3
	GOTO       L_main13
L__main23:
	CLRF       R4+0
	MOVLW      1
	MOVWF      R5+0
	CLRF       R1+0
	CLRF       R2+0
	CLRF       R3+0
L_main13:
;lab4.c,33 :: 		if((E==1)&&(PORTD.RD4==1)){E=0;D=1;A=0;B=0;C=0;}
	MOVF       R5+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main16
	BTFSS      PORTD+0, 4
	GOTO       L_main16
L__main22:
	CLRF       R5+0
	MOVLW      1
	MOVWF      R4+0
	CLRF       R1+0
	CLRF       R2+0
	CLRF       R3+0
L_main16:
;lab4.c,35 :: 		if((A==1)){PORTA.RA0=0;PORTA.RA1=0;PORTA.RA2=0;PORTA.RA3=0;PORTA.RA4=0;}
	MOVF       R1+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main17
	BCF        PORTA+0, 0
	BCF        PORTA+0, 1
	BCF        PORTA+0, 2
	BCF        PORTA+0, 3
	BCF        PORTA+0, 4
L_main17:
;lab4.c,36 :: 		if((B==1)){PORTA.RA0=1;PORTA.RA1=0;PORTA.RA2=1;PORTA.RA3=0;PORTA.RA4=1;}
	MOVF       R2+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main18
	BSF        PORTA+0, 0
	BCF        PORTA+0, 1
	BSF        PORTA+0, 2
	BCF        PORTA+0, 3
	BSF        PORTA+0, 4
L_main18:
;lab4.c,37 :: 		if((C==1)){PORTA.RA0=0;PORTA.RA1=1;PORTA.RA2=0;PORTA.RA3=0;PORTA.RA4=0;}
	MOVF       R3+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main19
	BCF        PORTA+0, 0
	BSF        PORTA+0, 1
	BCF        PORTA+0, 2
	BCF        PORTA+0, 3
	BCF        PORTA+0, 4
L_main19:
;lab4.c,38 :: 		if((D==1)){PORTA.RA0=0;PORTA.RA1=0;PORTA.RA2=1;PORTA.RA3=0;PORTA.RA4=0;}
	MOVF       R4+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main20
	BCF        PORTA+0, 0
	BCF        PORTA+0, 1
	BSF        PORTA+0, 2
	BCF        PORTA+0, 3
	BCF        PORTA+0, 4
L_main20:
;lab4.c,39 :: 		if((E==1)){PORTA.RA0=0;PORTA.RA1=0;PORTA.RA2=0;PORTA.RA3=1;PORTA.RA4=0;}
	MOVF       R5+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main21
	BCF        PORTA+0, 0
	BCF        PORTA+0, 1
	BCF        PORTA+0, 2
	BSF        PORTA+0, 3
	BCF        PORTA+0, 4
L_main21:
;lab4.c,41 :: 		}
	GOTO       L_main0
;lab4.c,44 :: 		}
	GOTO       $+0
; end of _main
