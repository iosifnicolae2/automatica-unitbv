
_interrupt:
	MOVWF      R15+0
	SWAPF      STATUS+0, 0
	CLRF       STATUS+0
	MOVWF      ___saveSTATUS+0
	MOVF       PCLATH+0, 0
	MOVWF      ___savePCLATH+0
	CLRF       PCLATH+0
;lab_81teo.c,1 :: 		void interrupt()
;lab_81teo.c,3 :: 		INTCON.GIE=0;
	BCF        INTCON+0, 7
;lab_81teo.c,4 :: 		if (INTCON.INTF)
	BTFSS      INTCON+0, 1
	GOTO       L_interrupt0
;lab_81teo.c,6 :: 		PORTD=0b00100001;
	MOVLW      33
	MOVWF      PORTD+0
;lab_81teo.c,7 :: 		delay_ms(5000);
	MOVLW      51
	MOVWF      R11+0
	MOVLW      187
	MOVWF      R12+0
	MOVLW      223
	MOVWF      R13+0
L_interrupt1:
	DECFSZ     R13+0, 1
	GOTO       L_interrupt1
	DECFSZ     R12+0, 1
	GOTO       L_interrupt1
	DECFSZ     R11+0, 1
	GOTO       L_interrupt1
	NOP
	NOP
;lab_81teo.c,8 :: 		INTCON.INTF=0;
	BCF        INTCON+0, 1
;lab_81teo.c,9 :: 		}
L_interrupt0:
;lab_81teo.c,10 :: 		}
L__interrupt4:
	MOVF       ___savePCLATH+0, 0
	MOVWF      PCLATH+0
	SWAPF      ___saveSTATUS+0, 0
	MOVWF      STATUS+0
	SWAPF      R15+0, 1
	SWAPF      R15+0, 0
	RETFIE
; end of _interrupt

_main:
;lab_81teo.c,12 :: 		void main() {
;lab_81teo.c,13 :: 		TRISB=0b00000001;
	MOVLW      1
	MOVWF      TRISB+0
;lab_81teo.c,14 :: 		TRISC=0b00000000;
	CLRF       TRISC+0
;lab_81teo.c,15 :: 		TRISD=0b00000000;
	CLRF       TRISD+0
;lab_81teo.c,16 :: 		PORTB=0b00000000;
	CLRF       PORTB+0
;lab_81teo.c,17 :: 		PORTD=0b00000000;
	CLRF       PORTD+0
;lab_81teo.c,18 :: 		INTCON.GIE=1;
	BSF        INTCON+0, 7
;lab_81teo.c,19 :: 		INTCON.INTE=1;
	BSF        INTCON+0, 4
;lab_81teo.c,22 :: 		while (1)
L_main2:
;lab_81teo.c,24 :: 		PORTD=0b00001100;
	MOVLW      12
	MOVWF      PORTD+0
;lab_81teo.c,25 :: 		}
	GOTO       L_main2
;lab_81teo.c,27 :: 		}
	GOTO       $+0
; end of _main
