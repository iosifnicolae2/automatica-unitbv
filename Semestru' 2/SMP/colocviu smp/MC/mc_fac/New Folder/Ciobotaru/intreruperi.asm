
_interrupt:
	MOVWF      R15+0
	SWAPF      STATUS+0, 0
	CLRF       STATUS+0
	MOVWF      ___saveSTATUS+0
	MOVF       PCLATH+0, 0
	MOVWF      ___savePCLATH+0
	CLRF       PCLATH+0
;intreruperi.c,1 :: 		void interrupt()
;intreruperi.c,3 :: 		INTCON.GIE=0;
	BCF        INTCON+0, 7
;intreruperi.c,4 :: 		if( INTCON.INTF)
	BTFSS      INTCON+0, 1
	GOTO       L_interrupt0
;intreruperi.c,6 :: 		INTCON.INTF=0;
	BCF        INTCON+0, 1
;intreruperi.c,7 :: 		PORTA=0b00010001;
	MOVLW      17
	MOVWF      PORTA+0
;intreruperi.c,8 :: 		Delay_ms(3000);
	MOVLW      31
	MOVWF      R11+0
	MOVLW      113
	MOVWF      R12+0
	MOVLW      30
	MOVWF      R13+0
L_interrupt1:
	DECFSZ     R13+0, 1
	GOTO       L_interrupt1
	DECFSZ     R12+0, 1
	GOTO       L_interrupt1
	DECFSZ     R11+0, 1
	GOTO       L_interrupt1
	NOP
;intreruperi.c,9 :: 		}
L_interrupt0:
;intreruperi.c,10 :: 		INTCON.GIE=1;
	BSF        INTCON+0, 7
;intreruperi.c,11 :: 		}
L__interrupt4:
	MOVF       ___savePCLATH+0, 0
	MOVWF      PCLATH+0
	SWAPF      ___saveSTATUS+0, 0
	MOVWF      STATUS+0
	SWAPF      R15+0, 1
	SWAPF      R15+0, 0
	RETFIE
; end of _interrupt

_main:
;intreruperi.c,12 :: 		void main() {
;intreruperi.c,13 :: 		TRISA=0b00000000;
	CLRF       TRISA+0
;intreruperi.c,14 :: 		TRISB=0b00000001;
	MOVLW      1
	MOVWF      TRISB+0
;intreruperi.c,15 :: 		PORTA=0b00000000;
	CLRF       PORTA+0
;intreruperi.c,16 :: 		PORTB=0b00000000;
	CLRF       PORTB+0
;intreruperi.c,17 :: 		INTCON.GIE=1;
	BSF        INTCON+0, 7
;intreruperi.c,18 :: 		INTCON.INTE=1;
	BSF        INTCON+0, 4
;intreruperi.c,19 :: 		while(1)
L_main2:
;intreruperi.c,23 :: 		PORTA=0b00100100;
	MOVLW      36
	MOVWF      PORTA+0
;intreruperi.c,25 :: 		}
	GOTO       L_main2
;intreruperi.c,26 :: 		}
	GOTO       $+0
; end of _main
