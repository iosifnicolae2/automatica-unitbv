
_set_PWM:
;FRECVENTA.c,1 :: 		void  set_PWM()
;FRECVENTA.c,4 :: 		val=(ADC_Read(0))*51/1023;
	CLRF       FARG_ADC_Read_channel+0
	CALL       _ADC_Read+0
	MOVLW      51
	MOVWF      R4+0
	MOVLW      0
	MOVWF      R4+1
	CALL       _Mul_16x16_U+0
	MOVLW      255
	MOVWF      R4+0
	MOVLW      3
	MOVWF      R4+1
	CALL       _Div_16x16_U+0
;FRECVENTA.c,5 :: 		CCP2CON.CCP2Y=((val&(1<<0))==(1<<0));
	MOVLW      1
	ANDWF      R0+0, 0
	MOVWF      R3+0
	MOVF       R0+1, 0
	MOVWF      R3+1
	MOVLW      0
	ANDWF      R3+1, 1
	MOVLW      0
	XORWF      R3+1, 0
	BTFSS      STATUS+0, 2
	GOTO       L__set_PWM19
	MOVLW      1
	XORWF      R3+0, 0
L__set_PWM19:
	MOVLW      1
	BTFSS      STATUS+0, 2
	MOVLW      0
	MOVWF      R2+0
	BTFSC      R2+0, 0
	GOTO       L__set_PWM20
	BCF        CCP2CON+0, 4
	GOTO       L__set_PWM21
L__set_PWM20:
	BSF        CCP2CON+0, 4
L__set_PWM21:
;FRECVENTA.c,6 :: 		CCP2CON.CCP2X=((val&(1<<1))==(1<<1));
	MOVLW      2
	ANDWF      R0+0, 0
	MOVWF      R3+0
	MOVF       R0+1, 0
	MOVWF      R3+1
	MOVLW      0
	ANDWF      R3+1, 1
	MOVLW      0
	XORWF      R3+1, 0
	BTFSS      STATUS+0, 2
	GOTO       L__set_PWM22
	MOVLW      2
	XORWF      R3+0, 0
L__set_PWM22:
	MOVLW      1
	BTFSS      STATUS+0, 2
	MOVLW      0
	MOVWF      R2+0
	BTFSC      R2+0, 0
	GOTO       L__set_PWM23
	BCF        CCP2CON+0, 5
	GOTO       L__set_PWM24
L__set_PWM23:
	BSF        CCP2CON+0, 5
L__set_PWM24:
;FRECVENTA.c,7 :: 		CCPR2L=val>>2;
	MOVF       R0+0, 0
	MOVWF      R2+0
	MOVF       R0+1, 0
	MOVWF      R2+1
	RRF        R2+1, 1
	RRF        R2+0, 1
	BCF        R2+1, 7
	BTFSC      R2+1, 6
	BSF        R2+1, 7
	RRF        R2+1, 1
	RRF        R2+0, 1
	BCF        R2+1, 7
	BTFSC      R2+1, 6
	BSF        R2+1, 7
	MOVF       R2+0, 0
	MOVWF      CCPR2L+0
;FRECVENTA.c,8 :: 		}
	RETURN
; end of _set_PWM

_interrupt:
	MOVWF      R15+0
	SWAPF      STATUS+0, 0
	CLRF       STATUS+0
	MOVWF      ___saveSTATUS+0
	MOVF       PCLATH+0, 0
	MOVWF      ___savePCLATH+0
	CLRF       PCLATH+0
;FRECVENTA.c,10 :: 		void interrupt ()
;FRECVENTA.c,12 :: 		INTCON.GIE=0;
	BCF        INTCON+0, 7
;FRECVENTA.c,13 :: 		if (PIR1.CCP1IF==1)
	BTFSS      PIR1+0, 2
	GOTO       L_interrupt0
;FRECVENTA.c,16 :: 		TMR1H=0;
	CLRF       TMR1H+0
;FRECVENTA.c,17 :: 		TMR1L=0;
	CLRF       TMR1L+0
;FRECVENTA.c,18 :: 		var=CCPR1H;
	MOVF       CCPR1H+0, 0
	MOVWF      _var+0
	CLRF       _var+1
	CLRF       _var+2
	CLRF       _var+3
;FRECVENTA.c,19 :: 		var=var<<8;
	MOVF       _var+2, 0
	MOVWF      R8+3
	MOVF       _var+1, 0
	MOVWF      R8+2
	MOVF       _var+0, 0
	MOVWF      R8+1
	CLRF       R8+0
	MOVF       R8+0, 0
	MOVWF      _var+0
	MOVF       R8+1, 0
	MOVWF      _var+1
	MOVF       R8+2, 0
	MOVWF      _var+2
	MOVF       R8+3, 0
	MOVWF      _var+3
;FRECVENTA.c,20 :: 		var=var+CCPR1L;
	MOVF       CCPR1L+0, 0
	MOVWF      R4+0
	CLRF       R4+1
	CLRF       R4+2
	CLRF       R4+3
	MOVF       R8+0, 0
	ADDWF      R4+0, 1
	MOVF       R8+1, 0
	BTFSC      STATUS+0, 0
	INCFSZ     R8+1, 0
	ADDWF      R4+1, 1
	MOVF       R8+2, 0
	BTFSC      STATUS+0, 0
	INCFSZ     R8+2, 0
	ADDWF      R4+2, 1
	MOVF       R8+3, 0
	BTFSC      STATUS+0, 0
	INCFSZ     R8+3, 0
	ADDWF      R4+3, 1
	MOVF       R4+0, 0
	MOVWF      _var+0
	MOVF       R4+1, 0
	MOVWF      _var+1
	MOVF       R4+2, 0
	MOVWF      _var+2
	MOVF       R4+3, 0
	MOVWF      _var+3
;FRECVENTA.c,21 :: 		n=937500/var;
	MOVLW      28
	MOVWF      R0+0
	MOVLW      78
	MOVWF      R0+1
	MOVLW      14
	MOVWF      R0+2
	MOVLW      0
	MOVWF      R0+3
	CALL       _Div_32x32_S+0
	MOVF       R0+0, 0
	MOVWF      _n+0
	MOVF       R0+1, 0
	MOVWF      _n+1
	MOVF       R0+2, 0
	MOVWF      _n+2
	MOVF       R0+3, 0
	MOVWF      _n+3
;FRECVENTA.c,23 :: 		PIR1.CCP1IF=0;
	BCF        PIR1+0, 2
;FRECVENTA.c,24 :: 		}
L_interrupt0:
;FRECVENTA.c,25 :: 		INTCON.GIE=1;
	BSF        INTCON+0, 7
;FRECVENTA.c,26 :: 		}
L__interrupt25:
	MOVF       ___savePCLATH+0, 0
	MOVWF      PCLATH+0
	SWAPF      ___saveSTATUS+0, 0
	MOVWF      STATUS+0
	SWAPF      R15+0, 1
	SWAPF      R15+0, 0
	RETFIE
; end of _interrupt

_main:
;FRECVENTA.c,27 :: 		void main()
;FRECVENTA.c,30 :: 		PR2 = 0b00001100 ;
	MOVLW      12
	MOVWF      PR2+0
;FRECVENTA.c,31 :: 		T2CON = 0b00000100 ;
	MOVLW      4
	MOVWF      T2CON+0
;FRECVENTA.c,32 :: 		CCPR2L = 0b00000000 ;
	CLRF       CCPR2L+0
;FRECVENTA.c,33 :: 		CCP2CON = 0b00001100 ;
	MOVLW      12
	MOVWF      CCP2CON+0
;FRECVENTA.c,34 :: 		TRISD.RD2=0;
	BCF        TRISD+0, 2
;FRECVENTA.c,35 :: 		TRISC.RC5=1;
	BSF        TRISC+0, 5
;FRECVENTA.c,36 :: 		TRISB=0;
	CLRF       TRISB+0
;FRECVENTA.c,38 :: 		T1CON=0b00110001;
	MOVLW      49
	MOVWF      T1CON+0
;FRECVENTA.c,39 :: 		CCP1CON=0x04;
	MOVLW      4
	MOVWF      CCP1CON+0
;FRECVENTA.c,42 :: 		TRISA.RA0=1;
	BSF        TRISA+0, 0
;FRECVENTA.c,43 :: 		ANSEL.AN0=1;
	BSF        ANSEL+0, 0
;FRECVENTA.c,45 :: 		INTCON.GIE=1;
	BSF        INTCON+0, 7
;FRECVENTA.c,46 :: 		INTCON.PEIE=1;
	BSF        INTCON+0, 6
;FRECVENTA.c,47 :: 		PIE1.CCP1IE=1;
	BSF        PIE1+0, 2
;FRECVENTA.c,54 :: 		while(1)
L_main1:
;FRECVENTA.c,56 :: 		set_PWM();
	CALL       _set_PWM+0
;FRECVENTA.c,58 :: 		if(n>50)
	MOVLW      128
	MOVWF      R0+0
	MOVLW      128
	XORWF      _n+3, 0
	SUBWF      R0+0, 0
	BTFSS      STATUS+0, 2
	GOTO       L__main26
	MOVF       _n+2, 0
	SUBLW      0
	BTFSS      STATUS+0, 2
	GOTO       L__main26
	MOVF       _n+1, 0
	SUBLW      0
	BTFSS      STATUS+0, 2
	GOTO       L__main26
	MOVF       _n+0, 0
	SUBLW      50
L__main26:
	BTFSC      STATUS+0, 0
	GOTO       L_main3
;FRECVENTA.c,59 :: 		PORTB.RB0=1;
	BSF        PORTB+0, 0
	GOTO       L_main4
L_main3:
;FRECVENTA.c,60 :: 		else PORTB.RB0=0;
	BCF        PORTB+0, 0
L_main4:
;FRECVENTA.c,62 :: 		if(n>100)
	MOVLW      128
	MOVWF      R0+0
	MOVLW      128
	XORWF      _n+3, 0
	SUBWF      R0+0, 0
	BTFSS      STATUS+0, 2
	GOTO       L__main27
	MOVF       _n+2, 0
	SUBLW      0
	BTFSS      STATUS+0, 2
	GOTO       L__main27
	MOVF       _n+1, 0
	SUBLW      0
	BTFSS      STATUS+0, 2
	GOTO       L__main27
	MOVF       _n+0, 0
	SUBLW      100
L__main27:
	BTFSC      STATUS+0, 0
	GOTO       L_main5
;FRECVENTA.c,63 :: 		PORTB.RB1=1;
	BSF        PORTB+0, 1
	GOTO       L_main6
L_main5:
;FRECVENTA.c,64 :: 		else PORTB.RB1=0;
	BCF        PORTB+0, 1
L_main6:
;FRECVENTA.c,66 :: 		if(n>150)
	MOVLW      128
	MOVWF      R0+0
	MOVLW      128
	XORWF      _n+3, 0
	SUBWF      R0+0, 0
	BTFSS      STATUS+0, 2
	GOTO       L__main28
	MOVF       _n+2, 0
	SUBLW      0
	BTFSS      STATUS+0, 2
	GOTO       L__main28
	MOVF       _n+1, 0
	SUBLW      0
	BTFSS      STATUS+0, 2
	GOTO       L__main28
	MOVF       _n+0, 0
	SUBLW      150
L__main28:
	BTFSC      STATUS+0, 0
	GOTO       L_main7
;FRECVENTA.c,67 :: 		PORTB.RB2=1;
	BSF        PORTB+0, 2
	GOTO       L_main8
L_main7:
;FRECVENTA.c,68 :: 		else PORTB.RB2=0;
	BCF        PORTB+0, 2
L_main8:
;FRECVENTA.c,70 :: 		if(n>200)
	MOVLW      128
	MOVWF      R0+0
	MOVLW      128
	XORWF      _n+3, 0
	SUBWF      R0+0, 0
	BTFSS      STATUS+0, 2
	GOTO       L__main29
	MOVF       _n+2, 0
	SUBLW      0
	BTFSS      STATUS+0, 2
	GOTO       L__main29
	MOVF       _n+1, 0
	SUBLW      0
	BTFSS      STATUS+0, 2
	GOTO       L__main29
	MOVF       _n+0, 0
	SUBLW      200
L__main29:
	BTFSC      STATUS+0, 0
	GOTO       L_main9
;FRECVENTA.c,71 :: 		PORTB.RB3=1;
	BSF        PORTB+0, 3
	GOTO       L_main10
L_main9:
;FRECVENTA.c,72 :: 		else PORTB.RB3=0;
	BCF        PORTB+0, 3
L_main10:
;FRECVENTA.c,74 :: 		if(n>250)
	MOVLW      128
	MOVWF      R0+0
	MOVLW      128
	XORWF      _n+3, 0
	SUBWF      R0+0, 0
	BTFSS      STATUS+0, 2
	GOTO       L__main30
	MOVF       _n+2, 0
	SUBLW      0
	BTFSS      STATUS+0, 2
	GOTO       L__main30
	MOVF       _n+1, 0
	SUBLW      0
	BTFSS      STATUS+0, 2
	GOTO       L__main30
	MOVF       _n+0, 0
	SUBLW      250
L__main30:
	BTFSC      STATUS+0, 0
	GOTO       L_main11
;FRECVENTA.c,75 :: 		PORTB.RB4=1;
	BSF        PORTB+0, 4
	GOTO       L_main12
L_main11:
;FRECVENTA.c,76 :: 		else PORTB.RB4=0;
	BCF        PORTB+0, 4
L_main12:
;FRECVENTA.c,78 :: 		if(n>300)
	MOVLW      128
	MOVWF      R0+0
	MOVLW      128
	XORWF      _n+3, 0
	SUBWF      R0+0, 0
	BTFSS      STATUS+0, 2
	GOTO       L__main31
	MOVF       _n+2, 0
	SUBLW      0
	BTFSS      STATUS+0, 2
	GOTO       L__main31
	MOVF       _n+1, 0
	SUBLW      1
	BTFSS      STATUS+0, 2
	GOTO       L__main31
	MOVF       _n+0, 0
	SUBLW      44
L__main31:
	BTFSC      STATUS+0, 0
	GOTO       L_main13
;FRECVENTA.c,79 :: 		PORTB.RB5=1;
	BSF        PORTB+0, 5
	GOTO       L_main14
L_main13:
;FRECVENTA.c,80 :: 		else PORTB.RB5=0;
	BCF        PORTB+0, 5
L_main14:
;FRECVENTA.c,82 :: 		if(n>350)
	MOVLW      128
	MOVWF      R0+0
	MOVLW      128
	XORWF      _n+3, 0
	SUBWF      R0+0, 0
	BTFSS      STATUS+0, 2
	GOTO       L__main32
	MOVF       _n+2, 0
	SUBLW      0
	BTFSS      STATUS+0, 2
	GOTO       L__main32
	MOVF       _n+1, 0
	SUBLW      1
	BTFSS      STATUS+0, 2
	GOTO       L__main32
	MOVF       _n+0, 0
	SUBLW      94
L__main32:
	BTFSC      STATUS+0, 0
	GOTO       L_main15
;FRECVENTA.c,83 :: 		PORTB.RB6=1;
	BSF        PORTB+0, 6
	GOTO       L_main16
L_main15:
;FRECVENTA.c,84 :: 		else PORTB.RB6=0;
	BCF        PORTB+0, 6
L_main16:
;FRECVENTA.c,86 :: 		if(n>400)
	MOVLW      128
	MOVWF      R0+0
	MOVLW      128
	XORWF      _n+3, 0
	SUBWF      R0+0, 0
	BTFSS      STATUS+0, 2
	GOTO       L__main33
	MOVF       _n+2, 0
	SUBLW      0
	BTFSS      STATUS+0, 2
	GOTO       L__main33
	MOVF       _n+1, 0
	SUBLW      1
	BTFSS      STATUS+0, 2
	GOTO       L__main33
	MOVF       _n+0, 0
	SUBLW      144
L__main33:
	BTFSC      STATUS+0, 0
	GOTO       L_main17
;FRECVENTA.c,87 :: 		PORTB.RB7=1;
	BSF        PORTB+0, 7
	GOTO       L_main18
L_main17:
;FRECVENTA.c,88 :: 		else PORTB.RB7=0;
	BCF        PORTB+0, 7
L_main18:
;FRECVENTA.c,89 :: 		}
	GOTO       L_main1
;FRECVENTA.c,90 :: 		}
	GOTO       $+0
; end of _main
