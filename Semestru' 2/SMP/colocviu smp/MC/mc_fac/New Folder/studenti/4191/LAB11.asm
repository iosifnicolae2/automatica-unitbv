
_main:
;LAB11.c,8 :: 		void main() {
;LAB11.c,10 :: 		TRISB.RA1=1;
	BSF        TRISB+0, 1
;LAB11.c,11 :: 		TRISB.RA2=1;
	BSF        TRISB+0, 2
;LAB11.c,12 :: 		TRISB.RA3=1;
	BSF        TRISB+0, 3
;LAB11.c,13 :: 		TRISB.RA4=1;
	BSF        TRISB+0, 4
;LAB11.c,14 :: 		TRISD.RA0=0;
	BCF        TRISD+0, 0
;LAB11.c,15 :: 		TRISD.RA1=0;
	BCF        TRISD+0, 1
;LAB11.c,16 :: 		TRISD.RA2=0;
	BCF        TRISD+0, 2
;LAB11.c,17 :: 		TRISD.RA3=0;
	BCF        TRISD+0, 3
;LAB11.c,18 :: 		TRISD.RA4=0;
	BCF        TRISD+0, 4
;LAB11.c,21 :: 		A=1;
	MOVLW      1
	MOVWF      _A+0
;LAB11.c,22 :: 		B=0;
	CLRF       _B+0
;LAB11.c,23 :: 		C1=0;
	CLRF       _C1+0
;LAB11.c,24 :: 		E=1;
	MOVLW      1
	MOVWF      _E+0
;LAB11.c,25 :: 		FS=0;
	CLRF       _FS+0
;LAB11.c,27 :: 		while(1)
L_main0:
;LAB11.c,29 :: 		if((A==1)&&(TRISB.RA0==1)){A=0;B=1;}
	MOVF       _A+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main4
	BTFSS      TRISB+0, 0
	GOTO       L_main4
L__main18:
	CLRF       _A+0
	MOVLW      1
	MOVWF      _B+0
L_main4:
;LAB11.c,30 :: 		if((B==1)&&(TRISB.RA1==1)){B=0;C1=1;}
	MOVF       _B+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main7
	BTFSS      TRISB+0, 1
	GOTO       L_main7
L__main17:
	CLRF       _B+0
	MOVLW      1
	MOVWF      _C1+0
L_main7:
;LAB11.c,31 :: 		if((C==1)&&(TRISB.RA2==1)){C1=0;A=1;}
L_main8:
;LAB11.c,33 :: 		if((E==1)&&(TRISB.RA3==1)){E=0;FS=1;}
	MOVF       _E+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main11
	BTFSS      TRISB+0, 3
	GOTO       L_main11
L__main16:
	CLRF       _E+0
	MOVLW      1
	MOVWF      _FS+0
L_main11:
;LAB11.c,34 :: 		if((FS==1)&&(TRISB.RA4==1)){FS=0;E=1;}
	MOVF       _FS+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main14
	BTFSS      TRISB+0, 4
	GOTO       L_main14
L__main15:
	CLRF       _FS+0
	MOVLW      1
	MOVWF      _E+0
L_main14:
;LAB11.c,36 :: 		}
	GOTO       L_main0
;LAB11.c,40 :: 		}
	GOTO       $+0
; end of _main
