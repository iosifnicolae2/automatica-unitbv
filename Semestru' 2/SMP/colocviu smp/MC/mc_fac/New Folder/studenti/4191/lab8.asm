
_citeste_ADC:
;lab8.c,17 :: 		int citeste_ADC(short chanel)
;lab8.c,21 :: 		chanel=chanel&0b00000111;
	MOVLW      7
	ANDWF      FARG_citeste_ADC_chanel+0, 0
	MOVWF      R2+0
	MOVF       R2+0, 0
	MOVWF      FARG_citeste_ADC_chanel+0
;lab8.c,22 :: 		chanel=chanel<<2;
	MOVF       R2+0, 0
	MOVWF      R0+0
	RLF        R0+0, 1
	BCF        R0+0, 0
	RLF        R0+0, 1
	BCF        R0+0, 0
	MOVF       R0+0, 0
	MOVWF      FARG_citeste_ADC_chanel+0
;lab8.c,23 :: 		ADCON0=ADCON0&0b11100011;
	MOVLW      227
	ANDWF      ADCON0+0, 1
;lab8.c,24 :: 		ADCON0=ADCON0|chanel;
	MOVF       R0+0, 0
	IORWF      ADCON0+0, 1
;lab8.c,25 :: 		Delay_ms(10);
	MOVLW      33
	MOVWF      R12+0
	MOVLW      118
	MOVWF      R13+0
L_citeste_ADC0:
	DECFSZ     R13+0, 1
	GOTO       L_citeste_ADC0
	DECFSZ     R12+0, 1
	GOTO       L_citeste_ADC0
	NOP
;lab8.c,26 :: 		ADCON0.GO_DONE=1;
	BSF        ADCON0+0, 1
;lab8.c,27 :: 		while(ADCON0.GO_DONE==1)
L_citeste_ADC1:
	BTFSS      ADCON0+0, 1
	GOTO       L_citeste_ADC2
;lab8.c,29 :: 		}
	GOTO       L_citeste_ADC1
L_citeste_ADC2:
;lab8.c,30 :: 		rez=ADRESH;
	MOVF       ADRESH+0, 0
	MOVWF      R4+0
	CLRF       R4+1
;lab8.c,31 :: 		rez=rez<<2;
	MOVF       R4+0, 0
	MOVWF      R2+0
	MOVF       R4+1, 0
	MOVWF      R2+1
	RLF        R2+0, 1
	RLF        R2+1, 1
	BCF        R2+0, 0
	RLF        R2+0, 1
	RLF        R2+1, 1
	BCF        R2+0, 0
	MOVF       R2+0, 0
	MOVWF      R4+0
	MOVF       R2+1, 0
	MOVWF      R4+1
;lab8.c,32 :: 		ADRESL=ADRESL>>6;
	MOVLW      6
	MOVWF      R1+0
	MOVF       ADRESL+0, 0
	MOVWF      R0+0
	MOVF       R1+0, 0
L__citeste_ADC5:
	BTFSC      STATUS+0, 2
	GOTO       L__citeste_ADC6
	RRF        R0+0, 1
	BCF        R0+0, 7
	ADDLW      255
	GOTO       L__citeste_ADC5
L__citeste_ADC6:
	MOVF       R0+0, 0
	MOVWF      ADRESL+0
;lab8.c,33 :: 		rez=rez|ADRESL;
	MOVF       ADRESL+0, 0
	IORWF      R2+0, 0
	MOVWF      R0+0
	MOVF       R2+1, 0
	MOVWF      R0+1
	MOVLW      0
	IORWF      R0+1, 1
	MOVF       R0+0, 0
	MOVWF      R4+0
	MOVF       R0+1, 0
	MOVWF      R4+1
;lab8.c,34 :: 		return rez;
;lab8.c,36 :: 		}
	RETURN
; end of _citeste_ADC

_main:
;lab8.c,40 :: 		void main() {
;lab8.c,41 :: 		TRISA.RA0=1;
	BSF        TRISA+0, 0
;lab8.c,42 :: 		ANSEL=0X01;
	MOVLW      1
	MOVWF      ANSEL+0
;lab8.c,43 :: 		ADCON0=0X01;
	MOVLW      1
	MOVWF      ADCON0+0
;lab8.c,44 :: 		while(1)
L_main3:
;lab8.c,46 :: 		}
	GOTO       L_main3
;lab8.c,49 :: 		}
	GOTO       $+0
; end of _main
