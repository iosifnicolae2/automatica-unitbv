
_init_ADC:
;lab07.c,17 :: 		void init_ADC()
;lab07.c,19 :: 		ANSEL=255;
	MOVLW      255
	MOVWF      ANSEL+0
;lab07.c,20 :: 		ADCON0.ADFM=0;
	BCF        ADCON0+0, 7
;lab07.c,21 :: 		ADCON0.VCFG1=0;
	BCF        ADCON0+0, 6
;lab07.c,22 :: 		ADCON0.VCFG0=0;
	BCF        ADCON0+0, 5
;lab07.c,23 :: 		ADCON0.CHS2=0;
	BCF        ADCON0+0, 4
;lab07.c,24 :: 		ADCON0.CHS1=0;
	BCF        ADCON0+0, 3
;lab07.c,25 :: 		ADCON0.CHS0=0;
	BCF        ADCON0+0, 2
;lab07.c,26 :: 		ADCON0.GO_DONE=0;
	BCF        ADCON0+0, 1
;lab07.c,27 :: 		ADCON0.ADON=0;
	BCF        ADCON0+0, 0
;lab07.c,28 :: 		ADCON1.ADCS2=0;
	BCF        ADCON1+0, 6
;lab07.c,29 :: 		ADCON1.ADCS1=0;
	BCF        ADCON1+0, 5
;lab07.c,30 :: 		ADCON1.ADCS0=1;
	BSF        ADCON1+0, 4
;lab07.c,31 :: 		TRISA=255;
	MOVLW      255
	MOVWF      TRISA+0
;lab07.c,32 :: 		}
	RETURN
; end of _init_ADC

_READ_ADC:
;lab07.c,33 :: 		int READ_ADC()
;lab07.c,34 :: 		{ ADCON0.ADON=1;
	BSF        ADCON0+0, 0
;lab07.c,35 :: 		ADCON0.GO_DONE=1;
	BSF        ADCON0+0, 1
;lab07.c,36 :: 		while (ADCON0.GO_DONE==1)
L_READ_ADC0:
	BTFSS      ADCON0+0, 1
	GOTO       L_READ_ADC1
;lab07.c,37 :: 		ADCON0.ADON=0;
	BCF        ADCON0+0, 0
	GOTO       L_READ_ADC0
L_READ_ADC1:
;lab07.c,38 :: 		return ADRESH;
	MOVF       ADRESH+0, 0
	MOVWF      R0+0
	CLRF       R0+1
;lab07.c,39 :: 		}
	RETURN
; end of _READ_ADC

_main:
;lab07.c,41 :: 		void main() {
;lab07.c,44 :: 		LCD_INIT();
	CALL       _Lcd_Init+0
;lab07.c,45 :: 		init_ADC();
	CALL       _init_ADC+0
;lab07.c,46 :: 		while(1)
L_main2:
;lab07.c,48 :: 		conv=READ_ADC();
	CALL       _READ_ADC+0
;lab07.c,49 :: 		sprinti(sir,"conv=%d ",conv);
	MOVLW      main_sir_L0+0
	MOVWF      FARG_sprinti_wh+0
	MOVLW      ?lstr_1_lab07+0
	MOVWF      FARG_sprinti_f+0
	MOVLW      hi_addr(?lstr_1_lab07+0
	MOVWF      FARG_sprinti_f+1
	MOVF       R0+0, 0
	MOVWF      FARG_sprinti_wh+3
	MOVF       R0+1, 0
	MOVWF      FARG_sprinti_wh+4
	CALL       _sprinti+0
;lab07.c,50 :: 		LCD_Out(1,1,sir);
	MOVLW      1
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      1
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      main_sir_L0+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
;lab07.c,51 :: 		Delay_ms(100);
	MOVLW      33
	MOVWF      R12+0
	MOVLW      118
	MOVWF      R13+0
L_main4:
	DECFSZ     R13+0, 1
	GOTO       L_main4
	DECFSZ     R12+0, 1
	GOTO       L_main4
	NOP
;lab07.c,52 :: 		}
	GOTO       L_main2
;lab07.c,54 :: 		}
	GOTO       $+0
; end of _main
