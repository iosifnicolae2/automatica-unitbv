
_main:

;Lab05.c,37 :: 		void main() {
;Lab05.c,40 :: 		TRISB.RB0 = 1;
	BSF        TRISB+0, 0
;Lab05.c,41 :: 		TRISB.RB1 = 1;
	BSF        TRISB+0, 1
;Lab05.c,42 :: 		TRISB.RB2 = 1;
	BSF        TRISB+0, 2
;Lab05.c,44 :: 		TRISB.RB4 = 0;
	BCF        TRISB+0, 4
;Lab05.c,45 :: 		PORTB.RB4 = 0;
	BCF        PORTB+0, 4
;Lab05.c,48 :: 		Lcd_Init();
	CALL       _Lcd_Init+0
;Lab05.c,49 :: 		Lcd_Cmd(_LCD_CLEAR);
	MOVLW      1
	MOVWF      FARG_Lcd_Cmd_out_char+0
	CALL       _Lcd_Cmd+0
;Lab05.c,50 :: 		Lcd_Cmd(_LCD_CURSOR_OFF);
	MOVLW      12
	MOVWF      FARG_Lcd_Cmd_out_char+0
	CALL       _Lcd_Cmd+0
;Lab05.c,51 :: 		Lcd_Out(1,1,txset);
	MOVLW      1
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      1
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      _txset+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
;Lab05.c,52 :: 		Lcd_Out(2,1,txok);
	MOVLW      2
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      1
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      _txok+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
;Lab05.c,55 :: 		SA=1;
	MOVLW      1
	MOVWF      _SA+0
;Lab05.c,56 :: 		SB=0;
	CLRF       _SB+0
;Lab05.c,57 :: 		SC=0;
	CLRF       _SC+0
;Lab05.c,58 :: 		SD=0;
	CLRF       _SD+0
;Lab05.c,59 :: 		SE=0;
	CLRF       _SE+0
;Lab05.c,61 :: 		oldstateB0 = 0;
	BCF        _oldstateB0+0, BitPos(_oldstateB0+0)
;Lab05.c,62 :: 		oldstateB1 = 0;
	BCF        _oldstateB1+0, BitPos(_oldstateB1+0)
;Lab05.c,63 :: 		oldstateB2 = 0;
	BCF        _oldstateB2+0, BitPos(_oldstateB2+0)
;Lab05.c,65 :: 		while(1)
L_main0:
;Lab05.c,68 :: 		if(PORTB.RB0 == 1)
	BTFSS      PORTB+0, 0
	GOTO       L_main2
;Lab05.c,69 :: 		oldstateB0 = 1;
	BSF        _oldstateB0+0, BitPos(_oldstateB0+0)
L_main2:
;Lab05.c,70 :: 		if(oldstateB0 == 1 && PORTB.RB0 == 0)
	BTFSS      _oldstateB0+0, BitPos(_oldstateB0+0)
	GOTO       L_main5
	BTFSC      PORTB+0, 0
	GOTO       L_main5
L__main31:
;Lab05.c,72 :: 		if(SA){ SA = 0; SB = 1; Lcd_Cmd(_LCD_CLEAR); Lcd_Out(1,1,txon) ; Lcd_Out(2,1,txcan);} // A->B
	MOVF       _SA+0, 0
	BTFSC      STATUS+0, 2
	GOTO       L_main6
	CLRF       _SA+0
	MOVLW      1
	MOVWF      _SB+0
	MOVLW      1
	MOVWF      FARG_Lcd_Cmd_out_char+0
	CALL       _Lcd_Cmd+0
	MOVLW      1
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      1
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      _txon+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
	MOVLW      2
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      1
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      _txcan+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
	GOTO       L_main7
L_main6:
;Lab05.c,74 :: 		if(SB){ SB = 0; SD = 1; Lcd_Cmd(_LCD_CLEAR); Lcd_Out(1,1,txapr); Lcd_Out(2,1,txok); PORTB.RB4=1;} // B->D
	MOVF       _SB+0, 0
	BTFSC      STATUS+0, 2
	GOTO       L_main8
	CLRF       _SB+0
	MOVLW      1
	MOVWF      _SD+0
	MOVLW      1
	MOVWF      FARG_Lcd_Cmd_out_char+0
	CALL       _Lcd_Cmd+0
	MOVLW      1
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      1
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      _txapr+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
	MOVLW      2
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      1
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      _txok+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
	BSF        PORTB+0, 4
	GOTO       L_main9
L_main8:
;Lab05.c,76 :: 		if(SD){ SD = 0; SA = 1; Lcd_Cmd(_LCD_CLEAR); Lcd_Out(1,1,txset); Lcd_Out(2,1,txok);} // D->A
	MOVF       _SD+0, 0
	BTFSC      STATUS+0, 2
	GOTO       L_main10
	CLRF       _SD+0
	MOVLW      1
	MOVWF      _SA+0
	MOVLW      1
	MOVWF      FARG_Lcd_Cmd_out_char+0
	CALL       _Lcd_Cmd+0
	MOVLW      1
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      1
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      _txset+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
	MOVLW      2
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      1
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      _txok+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
L_main10:
L_main9:
L_main7:
;Lab05.c,77 :: 		if(SC){ SC = 0; SE = 1; Lcd_Cmd(_LCD_CLEAR); Lcd_Out(1,1,txsti); Lcd_Out(2,1,txok); PORTB.RB4=0;} // C->E
	MOVF       _SC+0, 0
	BTFSC      STATUS+0, 2
	GOTO       L_main11
	CLRF       _SC+0
	MOVLW      1
	MOVWF      _SE+0
	MOVLW      1
	MOVWF      FARG_Lcd_Cmd_out_char+0
	CALL       _Lcd_Cmd+0
	MOVLW      1
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      1
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      _txsti+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
	MOVLW      2
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      1
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      _txok+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
	BCF        PORTB+0, 4
	GOTO       L_main12
L_main11:
;Lab05.c,79 :: 		if(SE){ SE = 0; SA = 1; Lcd_Cmd(_LCD_CLEAR); Lcd_Out(1,1,txset); Lcd_Out(2,1,txok);} // E->A
	MOVF       _SE+0, 0
	BTFSC      STATUS+0, 2
	GOTO       L_main13
	CLRF       _SE+0
	MOVLW      1
	MOVWF      _SA+0
	MOVLW      1
	MOVWF      FARG_Lcd_Cmd_out_char+0
	CALL       _Lcd_Cmd+0
	MOVLW      1
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      1
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      _txset+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
	MOVLW      2
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      1
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      _txok+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
L_main13:
L_main12:
;Lab05.c,80 :: 		oldstateB0 = 0;
	BCF        _oldstateB0+0, BitPos(_oldstateB0+0)
;Lab05.c,81 :: 		}
L_main5:
;Lab05.c,84 :: 		if(PORTB.RB1 == 1)
	BTFSS      PORTB+0, 1
	GOTO       L_main14
;Lab05.c,85 :: 		oldstateB1 = 1;
	BSF        _oldstateB1+0, BitPos(_oldstateB1+0)
L_main14:
;Lab05.c,86 :: 		if(oldstateB1 == 1 && PORTB.RB1 == 0)
	BTFSS      _oldstateB1+0, BitPos(_oldstateB1+0)
	GOTO       L_main17
	BTFSC      PORTB+0, 1
	GOTO       L_main17
L__main30:
;Lab05.c,88 :: 		if(SB || SC){ SB = 0; SC = 0; SA = 1; Lcd_Out(1,1,txset); Lcd_Out(2,1,txok);} // B/C->A
	MOVF       _SB+0, 0
	BTFSS      STATUS+0, 2
	GOTO       L__main29
	MOVF       _SC+0, 0
	BTFSS      STATUS+0, 2
	GOTO       L__main29
	GOTO       L_main20
L__main29:
	CLRF       _SB+0
	CLRF       _SC+0
	MOVLW      1
	MOVWF      _SA+0
	MOVLW      1
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      1
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      _txset+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
	MOVLW      2
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      1
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      _txok+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
L_main20:
;Lab05.c,89 :: 		oldstateB1 = 0;
	BCF        _oldstateB1+0, BitPos(_oldstateB1+0)
;Lab05.c,90 :: 		}
L_main17:
;Lab05.c,93 :: 		if(PORTB.RB2 == 1)
	BTFSS      PORTB+0, 2
	GOTO       L_main21
;Lab05.c,94 :: 		oldstateB2 = 1;
	BSF        _oldstateB2+0, BitPos(_oldstateB2+0)
L_main21:
;Lab05.c,95 :: 		if(oldstateB2 == 1 && PORTB.RB2 == 0)
	BTFSS      _oldstateB2+0, BitPos(_oldstateB2+0)
	GOTO       L_main24
	BTFSC      PORTB+0, 2
	GOTO       L_main24
L__main28:
;Lab05.c,97 :: 		if(SB){ SB = 0; SC = 1; Lcd_Cmd(_LCD_CLEAR); Lcd_Out(1,1,txoff); Lcd_Out(2,1,txcan);} // B->C
	MOVF       _SB+0, 0
	BTFSC      STATUS+0, 2
	GOTO       L_main25
	CLRF       _SB+0
	MOVLW      1
	MOVWF      _SC+0
	MOVLW      1
	MOVWF      FARG_Lcd_Cmd_out_char+0
	CALL       _Lcd_Cmd+0
	MOVLW      1
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      1
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      _txoff+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
	MOVLW      2
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      1
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      _txcan+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
	GOTO       L_main26
L_main25:
;Lab05.c,99 :: 		if(SC){ SC = 0; SB = 1; Lcd_Cmd(_LCD_CLEAR); Lcd_Out(1,1,txon) ; Lcd_Out(2,1,txcan);} // C->B
	MOVF       _SC+0, 0
	BTFSC      STATUS+0, 2
	GOTO       L_main27
	CLRF       _SC+0
	MOVLW      1
	MOVWF      _SB+0
	MOVLW      1
	MOVWF      FARG_Lcd_Cmd_out_char+0
	CALL       _Lcd_Cmd+0
	MOVLW      1
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      1
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      _txon+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
	MOVLW      2
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      1
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      _txcan+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
L_main27:
L_main26:
;Lab05.c,100 :: 		oldstateB2 = 0;
	BCF        _oldstateB2+0, BitPos(_oldstateB2+0)
;Lab05.c,101 :: 		}
L_main24:
;Lab05.c,103 :: 		}
	GOTO       L_main0
;Lab05.c,104 :: 		}
	GOTO       $+0
; end of _main
