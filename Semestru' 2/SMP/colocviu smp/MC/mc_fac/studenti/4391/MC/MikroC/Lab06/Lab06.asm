
_main:

;Lab06.c,6 :: 		void main() {
;Lab06.c,8 :: 		INTCON.GIE = 1; //Global
	BSF        INTCON+0, 7
;Lab06.c,9 :: 		INTCON.INTE = 1; //External
	BSF        INTCON+0, 4
;Lab06.c,12 :: 		TRISD.RD0 = 0;
	BCF        TRISD+0, 0
;Lab06.c,13 :: 		TRISD.RD1 = 0;
	BCF        TRISD+0, 1
;Lab06.c,14 :: 		TRISD.RD2 = 0;
	BCF        TRISD+0, 2
;Lab06.c,15 :: 		TRISD.RD3 = 0;
	BCF        TRISD+0, 3
;Lab06.c,16 :: 		TRISD.RD4 = 0;
	BCF        TRISD+0, 4
;Lab06.c,17 :: 		TRISD.RD5 = 0;
	BCF        TRISD+0, 5
;Lab06.c,18 :: 		TRISD.RD6 = 0;
	BCF        TRISD+0, 6
;Lab06.c,20 :: 		TRISB.RB0 = 1;
	BSF        TRISB+0, 0
;Lab06.c,21 :: 		TRISB.RB1 = 0;
	BCF        TRISB+0, 1
;Lab06.c,24 :: 		PORTD = cifre[0];
	MOVF       _cifre+0, 0
	MOVWF      PORTD+0
;Lab06.c,27 :: 		oldstate = 0;
	BCF        _oldstate+0, BitPos(_oldstate+0)
;Lab06.c,28 :: 		counter = 0;
	CLRF       _counter+0
;Lab06.c,30 :: 		while(1)
L_main0:
;Lab06.c,32 :: 		PORTB.RB1 = 1;
	BSF        PORTB+0, 1
;Lab06.c,33 :: 		Delay_ms(1000);
	MOVLW      2
	MOVWF      R11+0
	MOVLW      69
	MOVWF      R12+0
	MOVLW      169
	MOVWF      R13+0
L_main2:
	DECFSZ     R13+0, 1
	GOTO       L_main2
	DECFSZ     R12+0, 1
	GOTO       L_main2
	DECFSZ     R11+0, 1
	GOTO       L_main2
	NOP
	NOP
;Lab06.c,34 :: 		PORTB.RB1 = 0;
	BCF        PORTB+0, 1
;Lab06.c,35 :: 		Delay_ms(1000);
	MOVLW      2
	MOVWF      R11+0
	MOVLW      69
	MOVWF      R12+0
	MOVLW      169
	MOVWF      R13+0
L_main3:
	DECFSZ     R13+0, 1
	GOTO       L_main3
	DECFSZ     R12+0, 1
	GOTO       L_main3
	DECFSZ     R11+0, 1
	GOTO       L_main3
	NOP
	NOP
;Lab06.c,36 :: 		}
	GOTO       L_main0
;Lab06.c,37 :: 		}
	GOTO       $+0
; end of _main

_interrupt:
	MOVWF      R15+0
	SWAPF      STATUS+0, 0
	CLRF       STATUS+0
	MOVWF      ___saveSTATUS+0
	MOVF       PCLATH+0, 0
	MOVWF      ___savePCLATH+0
	CLRF       PCLATH+0

;Lab06.c,40 :: 		void interrupt()
;Lab06.c,42 :: 		if (INTCON.INTF == 1)
	BTFSS      INTCON+0, 1
	GOTO       L_interrupt4
;Lab06.c,44 :: 		counter++;
	INCF       _counter+0, 1
;Lab06.c,45 :: 		if(counter == 10)
	MOVF       _counter+0, 0
	XORLW      10
	BTFSS      STATUS+0, 2
	GOTO       L_interrupt5
;Lab06.c,46 :: 		counter = 0;
	CLRF       _counter+0
L_interrupt5:
;Lab06.c,47 :: 		PORTD = cifre[counter];
	MOVF       _counter+0, 0
	ADDLW      _cifre+0
	MOVWF      FSR
	MOVF       INDF+0, 0
	MOVWF      PORTD+0
;Lab06.c,49 :: 		INTCON.INTF = 0;
	BCF        INTCON+0, 1
;Lab06.c,50 :: 		}
L_interrupt4:
;Lab06.c,51 :: 		}
L__interrupt6:
	MOVF       ___savePCLATH+0, 0
	MOVWF      PCLATH+0
	SWAPF      ___saveSTATUS+0, 0
	MOVWF      STATUS+0
	SWAPF      R15+0, 1
	SWAPF      R15+0, 0
	RETFIE
; end of _interrupt
