// Variablile globale
char cifre[] = {0b01111110,0b00000110,0b01101101,0b01001111,0b00010111,0b01011011,0b01111011,0b00001110,0b01111111,0b01011111};
bit oldstate;
char counter;

void main() {
// Declararea intreruperilor
INTCON.GIE = 1; //Global
INTCON.INTE = 1; //External

// Declararea porturilor
TRISD.RD0 = 0;
TRISD.RD1 = 0;
TRISD.RD2 = 0;
TRISD.RD3 = 0;
TRISD.RD4 = 0;
TRISD.RD5 = 0;
TRISD.RD6 = 0;

TRISB.RB0 = 1;
TRISB.RB1 = 0;

// Initilizarea porturilor
PORTD = cifre[0];

// Stari initiale
oldstate = 0;
counter = 0;

while(1)
  {
   PORTB.RB1 = 1;
   Delay_ms(1000);
   PORTB.RB1 = 0;
   Delay_ms(1000);
  }
}

// Programarea intreruperilor
void interrupt()
      {
       if (INTCON.INTF == 1)
          {
           counter++;
           if(counter == 10)
              counter = 0;
           PORTD = cifre[counter];

           INTCON.INTF = 0;
          }
      }
