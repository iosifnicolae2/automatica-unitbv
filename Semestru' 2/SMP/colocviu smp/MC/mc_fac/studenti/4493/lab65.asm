
_interrupt:
	MOVWF      R15+0
	SWAPF      STATUS+0, 0
	CLRF       STATUS+0
	MOVWF      ___saveSTATUS+0
	MOVF       PCLATH+0, 0
	MOVWF      ___savePCLATH+0
	CLRF       PCLATH+0
;lab65.c,21 :: 		void interrupt()
;lab65.c,23 :: 		INTCON.GIE=0;
	BCF        INTCON+0, 7
;lab65.c,25 :: 		{INTCON.T0IF=0;
	BCF        INTCON+0, 2
;lab65.c,26 :: 		i++;
	INCF       _i+0, 1
	BTFSC      STATUS+0, 2
	INCF       _i+1, 1
;lab65.c,27 :: 		if (i==1000)
	MOVF       _i+1, 0
	XORLW      3
	BTFSS      STATUS+0, 2
	GOTO       L__interrupt5
	MOVLW      232
	XORWF      _i+0, 0
L__interrupt5:
	BTFSS      STATUS+0, 2
	GOTO       L_interrupt1
;lab65.c,28 :: 		{i=0;
	CLRF       _i+0
	CLRF       _i+1
;lab65.c,29 :: 		sec++;
	INCF       _sec+0, 1
	BTFSC      STATUS+0, 2
	INCF       _sec+1, 1
;lab65.c,30 :: 		}
L_interrupt1:
;lab65.c,32 :: 		INTCON.GIE=1;
	BSF        INTCON+0, 7
;lab65.c,33 :: 		}
L__interrupt4:
	MOVF       ___savePCLATH+0, 0
	MOVWF      PCLATH+0
	SWAPF      ___saveSTATUS+0, 0
	MOVWF      STATUS+0
	SWAPF      R15+0, 1
	SWAPF      R15+0, 0
	RETFIE
; end of _interrupt

_main:
;lab65.c,36 :: 		void main()
;lab65.c,38 :: 		TRISB=0B00000000;
	CLRF       TRISB+0
;lab65.c,39 :: 		PORTB=0B00000000;
	CLRF       PORTB+0
;lab65.c,40 :: 		INTCON=0B11100000;
	MOVLW      224
	MOVWF      INTCON+0
;lab65.c,41 :: 		OPTION_REG=0B00010000;
	MOVLW      16
	MOVWF      OPTION_REG+0
;lab65.c,42 :: 		LCD_INIT();
	CALL       _Lcd_Init+0
;lab65.c,43 :: 		while(1)
L_main2:
;lab65.c,45 :: 		sprinti(str,"sec=%d",sec);
	MOVLW      _str+0
	MOVWF      FARG_sprinti_wh+0
	MOVLW      ?lstr_1_lab65+0
	MOVWF      FARG_sprinti_f+0
	MOVLW      hi_addr(?lstr_1_lab65+0
	MOVWF      FARG_sprinti_f+1
	MOVF       _sec+0, 0
	MOVWF      FARG_sprinti_wh+3
	MOVF       _sec+1, 0
	MOVWF      FARG_sprinti_wh+4
	CALL       _sprinti+0
;lab65.c,46 :: 		LCD_OUT(1,1,str);
	MOVLW      1
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      1
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      _str+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
;lab65.c,48 :: 		}
	GOTO       L_main2
;lab65.c,49 :: 		}
	GOTO       $+0
; end of _main
