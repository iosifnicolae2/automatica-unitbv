
_main:
;lab6.c,19 :: 		void main() {
;lab6.c,20 :: 		Lcd_Cmd(_LCD_CLEAR);                // Clear display
	MOVLW      1
	MOVWF      FARG_Lcd_Cmd_out_char+0
	CALL       _Lcd_Cmd+0
;lab6.c,21 :: 		Lcd_Cmd(_LCD_CURSOR_OFF);
	MOVLW      12
	MOVWF      FARG_Lcd_Cmd_out_char+0
	CALL       _Lcd_Cmd+0
;lab6.c,22 :: 		TMR0=5;
	MOVLW      5
	MOVWF      TMR0+0
;lab6.c,23 :: 		OPTION_REG=0b00000010;
	MOVLW      2
	MOVWF      OPTION_REG+0
;lab6.c,24 :: 		INTCON=0b11100000;
	MOVLW      224
	MOVWF      INTCON+0
;lab6.c,25 :: 		TRISB=0b00000000;
	CLRF       TRISB+0
;lab6.c,26 :: 		PORTB=0b00000000;
	CLRF       PORTB+0
;lab6.c,27 :: 		Lcd_init();
	CALL       _Lcd_Init+0
;lab6.c,28 :: 		while(1)
L_main0:
;lab6.c,29 :: 		{sprinti(sir,"Secunde=%d",secunde);
	MOVLW      _sir+0
	MOVWF      FARG_sprinti_wh+0
	MOVLW      ?lstr_1_lab6+0
	MOVWF      FARG_sprinti_f+0
	MOVLW      hi_addr(?lstr_1_lab6+0
	MOVWF      FARG_sprinti_f+1
	MOVF       _secunde+0, 0
	MOVWF      FARG_sprinti_wh+3
	MOVF       _secunde+1, 0
	MOVWF      FARG_sprinti_wh+4
	CALL       _sprinti+0
;lab6.c,30 :: 		Lcd_Out(1,1,sir);
	MOVLW      1
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      1
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      _sir+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
;lab6.c,31 :: 		}
	GOTO       L_main0
;lab6.c,32 :: 		}
	GOTO       $+0
; end of _main

_interrupt:
	MOVWF      R15+0
	SWAPF      STATUS+0, 0
	CLRF       STATUS+0
	MOVWF      ___saveSTATUS+0
	MOVF       PCLATH+0, 0
	MOVWF      ___savePCLATH+0
	CLRF       PCLATH+0
;lab6.c,34 :: 		void interrupt()
;lab6.c,35 :: 		{INTCON.GIE=0;
	BCF        INTCON+0, 7
;lab6.c,36 :: 		if(INTCON.T0IF)
	BTFSS      INTCON+0, 2
	GOTO       L_interrupt2
;lab6.c,37 :: 		{INTCON.T0IF=0;
	BCF        INTCON+0, 2
;lab6.c,38 :: 		count++;
	INCF       _count+0, 1
	BTFSC      STATUS+0, 2
	INCF       _count+1, 1
;lab6.c,39 :: 		if(count==500)
	MOVF       _count+1, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L__interrupt5
	MOVLW      244
	XORWF      _count+0, 0
L__interrupt5:
	BTFSS      STATUS+0, 2
	GOTO       L_interrupt3
;lab6.c,40 :: 		{secunde++;
	INCF       _secunde+0, 1
	BTFSC      STATUS+0, 2
	INCF       _secunde+1, 1
;lab6.c,41 :: 		count = 0;
	CLRF       _count+0
	CLRF       _count+1
;lab6.c,42 :: 		}TMR0=5;
L_interrupt3:
	MOVLW      5
	MOVWF      TMR0+0
;lab6.c,44 :: 		}
L_interrupt2:
;lab6.c,45 :: 		INTCON.GIE=1;
	BSF        INTCON+0, 7
;lab6.c,46 :: 		}
L__interrupt4:
	MOVF       ___savePCLATH+0, 0
	MOVWF      PCLATH+0
	SWAPF      ___saveSTATUS+0, 0
	MOVWF      STATUS+0
	SWAPF      R15+0, 1
	SWAPF      R15+0, 0
	RETFIE
; end of _interrupt
