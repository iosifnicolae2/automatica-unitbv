
_interrupt:
	MOVWF      R15+0
	SWAPF      STATUS+0, 0
	CLRF       STATUS+0
	MOVWF      ___saveSTATUS+0
	MOVF       PCLATH+0, 0
	MOVWF      ___savePCLATH+0
	CLRF       PCLATH+0
;labint.c,1 :: 		void interrupt()
;labint.c,2 :: 		{    INTCON.GIE=0;
	BCF        INTCON+0, 7
;labint.c,3 :: 		if(INTCON.INTF==1)
	BTFSS      INTCON+0, 1
	GOTO       L_interrupt0
;labint.c,4 :: 		{INTCON.INTF=0;
	BCF        INTCON+0, 1
;labint.c,5 :: 		PORTA=0b00100001;
	MOVLW      33
	MOVWF      PORTA+0
;labint.c,6 :: 		delay_ms(6000);
	MOVLW      61
	MOVWF      R11+0
	MOVLW      225
	MOVWF      R12+0
	MOVLW      63
	MOVWF      R13+0
L_interrupt1:
	DECFSZ     R13+0, 1
	GOTO       L_interrupt1
	DECFSZ     R12+0, 1
	GOTO       L_interrupt1
	DECFSZ     R11+0, 1
	GOTO       L_interrupt1
	NOP
	NOP
;labint.c,7 :: 		}
L_interrupt0:
;labint.c,8 :: 		INTCON.GIE=1;
	BSF        INTCON+0, 7
;labint.c,10 :: 		}
L__interrupt4:
	MOVF       ___savePCLATH+0, 0
	MOVWF      PCLATH+0
	SWAPF      ___saveSTATUS+0, 0
	MOVWF      STATUS+0
	SWAPF      R15+0, 1
	SWAPF      R15+0, 0
	RETFIE
; end of _interrupt

_main:
;labint.c,12 :: 		void main() {
;labint.c,13 :: 		TRISA=0b00000000;
	CLRF       TRISA+0
;labint.c,14 :: 		TRISB=0b00000001;
	MOVLW      1
	MOVWF      TRISB+0
;labint.c,15 :: 		PORTA=0b00000000;
	CLRF       PORTA+0
;labint.c,16 :: 		PORTB=0b00000000;
	CLRF       PORTB+0
;labint.c,17 :: 		INTCON.GIE=1;
	BSF        INTCON+0, 7
;labint.c,18 :: 		INTCON.INTE=1;
	BSF        INTCON+0, 4
;labint.c,19 :: 		while(1){
L_main2:
;labint.c,22 :: 		PORTA=0b00001100;
	MOVLW      12
	MOVWF      PORTA+0
;labint.c,25 :: 		}
	GOTO       L_main2
;labint.c,26 :: 		}
	GOTO       $+0
; end of _main
