
_main:
;Lab2.c,1 :: 		void main() {
;Lab2.c,3 :: 		TRISB=0;
	CLRF       TRISB+0
;Lab2.c,5 :: 		while(1)
L_main0:
;Lab2.c,7 :: 		PORTB=1;
	MOVLW      1
	MOVWF      PORTB+0
;Lab2.c,8 :: 		Delay_ms(1000);
	MOVLW      11
	MOVWF      R11+0
	MOVLW      38
	MOVWF      R12+0
	MOVLW      93
	MOVWF      R13+0
L_main2:
	DECFSZ     R13+0, 1
	GOTO       L_main2
	DECFSZ     R12+0, 1
	GOTO       L_main2
	DECFSZ     R11+0, 1
	GOTO       L_main2
	NOP
	NOP
;Lab2.c,9 :: 		for(i=0;i<8;i++)
	CLRF       R2+0
	CLRF       R2+1
L_main3:
	MOVLW      128
	XORWF      R2+1, 0
	MOVWF      R0+0
	MOVLW      128
	SUBWF      R0+0, 0
	BTFSS      STATUS+0, 2
	GOTO       L__main7
	MOVLW      8
	SUBWF      R2+0, 0
L__main7:
	BTFSC      STATUS+0, 0
	GOTO       L_main4
;Lab2.c,11 :: 		PORTB=PORTB * 2;
	MOVF       PORTB+0, 0
	MOVWF      R0+0
	RLF        R0+0, 1
	BCF        R0+0, 0
	MOVF       R0+0, 0
	MOVWF      PORTB+0
;Lab2.c,12 :: 		delay_ms(1000);
	MOVLW      11
	MOVWF      R11+0
	MOVLW      38
	MOVWF      R12+0
	MOVLW      93
	MOVWF      R13+0
L_main6:
	DECFSZ     R13+0, 1
	GOTO       L_main6
	DECFSZ     R12+0, 1
	GOTO       L_main6
	DECFSZ     R11+0, 1
	GOTO       L_main6
	NOP
	NOP
;Lab2.c,9 :: 		for(i=0;i<8;i++)
	INCF       R2+0, 1
	BTFSC      STATUS+0, 2
	INCF       R2+1, 1
;Lab2.c,13 :: 		}
	GOTO       L_main3
L_main4:
;Lab2.c,14 :: 		}
	GOTO       L_main0
;Lab2.c,35 :: 		}*/ }
	GOTO       $+0
; end of _main
