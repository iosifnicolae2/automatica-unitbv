
_main:
;Semafor.c,1 :: 		void main() {
;Semafor.c,2 :: 		TRISB=0;
	CLRF       TRISB+0
;Semafor.c,3 :: 		TRISD=0;
	CLRF       TRISD+0
;Semafor.c,4 :: 		TRISC=0;
	CLRF       TRISC+0
;Semafor.c,5 :: 		while(1)
L_main0:
;Semafor.c,6 :: 		{ PORTC=96;
	MOVLW      96
	MOVWF      PORTC+0
;Semafor.c,7 :: 		PORTB=230;
	MOVLW      230
	MOVWF      PORTB+0
;Semafor.c,8 :: 		DELAY_MS(1000);
	MOVLW      11
	MOVWF      R11+0
	MOVLW      38
	MOVWF      R12+0
	MOVLW      93
	MOVWF      R13+0
L_main2:
	DECFSZ     R13+0, 1
	GOTO       L_main2
	DECFSZ     R12+0, 1
	GOTO       L_main2
	DECFSZ     R11+0, 1
	GOTO       L_main2
	NOP
	NOP
;Semafor.c,9 :: 		PORTB=254;
	MOVLW      254
	MOVWF      PORTB+0
;Semafor.c,10 :: 		DELAY_MS(1000) ;
	MOVLW      11
	MOVWF      R11+0
	MOVLW      38
	MOVWF      R12+0
	MOVLW      93
	MOVWF      R13+0
L_main3:
	DECFSZ     R13+0, 1
	GOTO       L_main3
	DECFSZ     R12+0, 1
	GOTO       L_main3
	DECFSZ     R11+0, 1
	GOTO       L_main3
	NOP
	NOP
;Semafor.c,11 :: 		PORTB=224;
	MOVLW      224
	MOVWF      PORTB+0
;Semafor.c,12 :: 		DELAY_MS(1000);
	MOVLW      11
	MOVWF      R11+0
	MOVLW      38
	MOVWF      R12+0
	MOVLW      93
	MOVWF      R13+0
L_main4:
	DECFSZ     R13+0, 1
	GOTO       L_main4
	DECFSZ     R12+0, 1
	GOTO       L_main4
	DECFSZ     R11+0, 1
	GOTO       L_main4
	NOP
	NOP
;Semafor.c,13 :: 		PORTB=190;
	MOVLW      190
	MOVWF      PORTB+0
;Semafor.c,14 :: 		DELAY_MS(1000) ;
	MOVLW      11
	MOVWF      R11+0
	MOVLW      38
	MOVWF      R12+0
	MOVLW      93
	MOVWF      R13+0
L_main5:
	DECFSZ     R13+0, 1
	GOTO       L_main5
	DECFSZ     R12+0, 1
	GOTO       L_main5
	DECFSZ     R11+0, 1
	GOTO       L_main5
	NOP
	NOP
;Semafor.c,15 :: 		PORTB=182;
	MOVLW      182
	MOVWF      PORTB+0
;Semafor.c,16 :: 		DELAY_MS(1000);
	MOVLW      11
	MOVWF      R11+0
	MOVLW      38
	MOVWF      R12+0
	MOVLW      93
	MOVWF      R13+0
L_main6:
	DECFSZ     R13+0, 1
	GOTO       L_main6
	DECFSZ     R12+0, 1
	GOTO       L_main6
	DECFSZ     R11+0, 1
	GOTO       L_main6
	NOP
	NOP
;Semafor.c,17 :: 		PORTB=102;
	MOVLW      102
	MOVWF      PORTB+0
;Semafor.c,18 :: 		DELAY_MS(1000) ;
	MOVLW      11
	MOVWF      R11+0
	MOVLW      38
	MOVWF      R12+0
	MOVLW      93
	MOVWF      R13+0
L_main7:
	DECFSZ     R13+0, 1
	GOTO       L_main7
	DECFSZ     R12+0, 1
	GOTO       L_main7
	DECFSZ     R11+0, 1
	GOTO       L_main7
	NOP
	NOP
;Semafor.c,19 :: 		PORTB=242;
	MOVLW      242
	MOVWF      PORTB+0
;Semafor.c,20 :: 		DELAY_MS(1000)  ;
	MOVLW      11
	MOVWF      R11+0
	MOVLW      38
	MOVWF      R12+0
	MOVLW      93
	MOVWF      R13+0
L_main8:
	DECFSZ     R13+0, 1
	GOTO       L_main8
	DECFSZ     R12+0, 1
	GOTO       L_main8
	DECFSZ     R11+0, 1
	GOTO       L_main8
	NOP
	NOP
;Semafor.c,21 :: 		PORTB=218;
	MOVLW      218
	MOVWF      PORTB+0
;Semafor.c,22 :: 		DELAY_MS(1000);
	MOVLW      11
	MOVWF      R11+0
	MOVLW      38
	MOVWF      R12+0
	MOVLW      93
	MOVWF      R13+0
L_main9:
	DECFSZ     R13+0, 1
	GOTO       L_main9
	DECFSZ     R12+0, 1
	GOTO       L_main9
	DECFSZ     R11+0, 1
	GOTO       L_main9
	NOP
	NOP
;Semafor.c,23 :: 		PORTB=96;
	MOVLW      96
	MOVWF      PORTB+0
;Semafor.c,24 :: 		DELAY_MS(1000);
	MOVLW      11
	MOVWF      R11+0
	MOVLW      38
	MOVWF      R12+0
	MOVLW      93
	MOVWF      R13+0
L_main10:
	DECFSZ     R13+0, 1
	GOTO       L_main10
	DECFSZ     R12+0, 1
	GOTO       L_main10
	DECFSZ     R11+0, 1
	GOTO       L_main10
	NOP
	NOP
;Semafor.c,25 :: 		PORTB=252;
	MOVLW      252
	MOVWF      PORTB+0
;Semafor.c,26 :: 		DELAY_MS(1000);
	MOVLW      11
	MOVWF      R11+0
	MOVLW      38
	MOVWF      R12+0
	MOVLW      93
	MOVWF      R13+0
L_main11:
	DECFSZ     R13+0, 1
	GOTO       L_main11
	DECFSZ     R12+0, 1
	GOTO       L_main11
	DECFSZ     R11+0, 1
	GOTO       L_main11
	NOP
	NOP
;Semafor.c,27 :: 		PORTC=252;
	MOVLW      252
	MOVWF      PORTC+0
;Semafor.c,28 :: 		PORTD=33;
	MOVLW      33
	MOVWF      PORTD+0
;Semafor.c,29 :: 		PORTB=230;
	MOVLW      230
	MOVWF      PORTB+0
;Semafor.c,30 :: 		DELAY_MS(1000);
	MOVLW      11
	MOVWF      R11+0
	MOVLW      38
	MOVWF      R12+0
	MOVLW      93
	MOVWF      R13+0
L_main12:
	DECFSZ     R13+0, 1
	GOTO       L_main12
	DECFSZ     R12+0, 1
	GOTO       L_main12
	DECFSZ     R11+0, 1
	GOTO       L_main12
	NOP
	NOP
;Semafor.c,31 :: 		PORTB=254;
	MOVLW      254
	MOVWF      PORTB+0
;Semafor.c,32 :: 		DELAY_MS(1000) ;
	MOVLW      11
	MOVWF      R11+0
	MOVLW      38
	MOVWF      R12+0
	MOVLW      93
	MOVWF      R13+0
L_main13:
	DECFSZ     R13+0, 1
	GOTO       L_main13
	DECFSZ     R12+0, 1
	GOTO       L_main13
	DECFSZ     R11+0, 1
	GOTO       L_main13
	NOP
	NOP
;Semafor.c,33 :: 		PORTB=224;
	MOVLW      224
	MOVWF      PORTB+0
;Semafor.c,34 :: 		DELAY_MS(1000);
	MOVLW      11
	MOVWF      R11+0
	MOVLW      38
	MOVWF      R12+0
	MOVLW      93
	MOVWF      R13+0
L_main14:
	DECFSZ     R13+0, 1
	GOTO       L_main14
	DECFSZ     R12+0, 1
	GOTO       L_main14
	DECFSZ     R11+0, 1
	GOTO       L_main14
	NOP
	NOP
;Semafor.c,35 :: 		PORTB=190;
	MOVLW      190
	MOVWF      PORTB+0
;Semafor.c,36 :: 		DELAY_MS(1000) ;
	MOVLW      11
	MOVWF      R11+0
	MOVLW      38
	MOVWF      R12+0
	MOVLW      93
	MOVWF      R13+0
L_main15:
	DECFSZ     R13+0, 1
	GOTO       L_main15
	DECFSZ     R12+0, 1
	GOTO       L_main15
	DECFSZ     R11+0, 1
	GOTO       L_main15
	NOP
	NOP
;Semafor.c,37 :: 		PORTB=182;
	MOVLW      182
	MOVWF      PORTB+0
;Semafor.c,38 :: 		DELAY_MS(1000);
	MOVLW      11
	MOVWF      R11+0
	MOVLW      38
	MOVWF      R12+0
	MOVLW      93
	MOVWF      R13+0
L_main16:
	DECFSZ     R13+0, 1
	GOTO       L_main16
	DECFSZ     R12+0, 1
	GOTO       L_main16
	DECFSZ     R11+0, 1
	GOTO       L_main16
	NOP
	NOP
;Semafor.c,39 :: 		PORTB=102;
	MOVLW      102
	MOVWF      PORTB+0
;Semafor.c,40 :: 		DELAY_MS(1000) ;
	MOVLW      11
	MOVWF      R11+0
	MOVLW      38
	MOVWF      R12+0
	MOVLW      93
	MOVWF      R13+0
L_main17:
	DECFSZ     R13+0, 1
	GOTO       L_main17
	DECFSZ     R12+0, 1
	GOTO       L_main17
	DECFSZ     R11+0, 1
	GOTO       L_main17
	NOP
	NOP
;Semafor.c,41 :: 		PORTB=242;
	MOVLW      242
	MOVWF      PORTB+0
;Semafor.c,42 :: 		DELAY_MS(1000)  ;
	MOVLW      11
	MOVWF      R11+0
	MOVLW      38
	MOVWF      R12+0
	MOVLW      93
	MOVWF      R13+0
L_main18:
	DECFSZ     R13+0, 1
	GOTO       L_main18
	DECFSZ     R12+0, 1
	GOTO       L_main18
	DECFSZ     R11+0, 1
	GOTO       L_main18
	NOP
	NOP
;Semafor.c,43 :: 		PORTB=218;
	MOVLW      218
	MOVWF      PORTB+0
;Semafor.c,44 :: 		DELAY_MS(1000);
	MOVLW      11
	MOVWF      R11+0
	MOVLW      38
	MOVWF      R12+0
	MOVLW      93
	MOVWF      R13+0
L_main19:
	DECFSZ     R13+0, 1
	GOTO       L_main19
	DECFSZ     R12+0, 1
	GOTO       L_main19
	DECFSZ     R11+0, 1
	GOTO       L_main19
	NOP
	NOP
;Semafor.c,45 :: 		PORTB=96;
	MOVLW      96
	MOVWF      PORTB+0
;Semafor.c,46 :: 		PORTD=2;
	MOVLW      2
	MOVWF      PORTD+0
;Semafor.c,47 :: 		DELAY_MS(1000);
	MOVLW      11
	MOVWF      R11+0
	MOVLW      38
	MOVWF      R12+0
	MOVLW      93
	MOVWF      R13+0
L_main20:
	DECFSZ     R13+0, 1
	GOTO       L_main20
	DECFSZ     R12+0, 1
	GOTO       L_main20
	DECFSZ     R11+0, 1
	GOTO       L_main20
	NOP
	NOP
;Semafor.c,48 :: 		PORTB=252;
	MOVLW      252
	MOVWF      PORTB+0
;Semafor.c,49 :: 		PORTD=12;
	MOVLW      12
	MOVWF      PORTD+0
;Semafor.c,50 :: 		DELAY_MS(1000) ;
	MOVLW      11
	MOVWF      R11+0
	MOVLW      38
	MOVWF      R12+0
	MOVLW      93
	MOVWF      R13+0
L_main21:
	DECFSZ     R13+0, 1
	GOTO       L_main21
	DECFSZ     R12+0, 1
	GOTO       L_main21
	DECFSZ     R11+0, 1
	GOTO       L_main21
	NOP
	NOP
;Semafor.c,51 :: 		}
	GOTO       L_main0
;Semafor.c,52 :: 		}
	GOTO       $+0
; end of _main
