
_main:
;lab3.c,2 :: 		void main() {
;lab3.c,3 :: 		for(i=0;i<10;i++)
	CLRF       _i+0
	CLRF       _i+1
L_main0:
	MOVLW      128
	XORWF      _i+1, 0
	MOVWF      R0+0
	MOVLW      128
	SUBWF      R0+0, 0
	BTFSS      STATUS+0, 2
	GOTO       L__main24
	MOVLW      10
	SUBWF      _i+0, 0
L__main24:
	BTFSC      STATUS+0, 0
	GOTO       L_main1
;lab3.c,5 :: 		TRISD=0;
	CLRF       TRISD+0
;lab3.c,6 :: 		PORTD=0b000000001;   // Rosu pietoni
	MOVLW      1
	MOVWF      PORTD+0
;lab3.c,7 :: 		TRISA=0;
	CLRF       TRISA+0
;lab3.c,8 :: 		PORTA=0b00000100;    // Verde masini
	MOVLW      4
	MOVWF      PORTA+0
;lab3.c,9 :: 		TRISB=0;
	CLRF       TRISB+0
;lab3.c,10 :: 		PORTB=0b1101111; //9
	MOVLW      111
	MOVWF      PORTB+0
;lab3.c,11 :: 		Delay_ms(1000);
	MOVLW      26
	MOVWF      R11+0
	MOVLW      94
	MOVWF      R12+0
	MOVLW      110
	MOVWF      R13+0
L_main3:
	DECFSZ     R13+0, 1
	GOTO       L_main3
	DECFSZ     R12+0, 1
	GOTO       L_main3
	DECFSZ     R11+0, 1
	GOTO       L_main3
	NOP
;lab3.c,12 :: 		PORTB=0b1111111; //8
	MOVLW      127
	MOVWF      PORTB+0
;lab3.c,13 :: 		Delay_ms(1000);
	MOVLW      26
	MOVWF      R11+0
	MOVLW      94
	MOVWF      R12+0
	MOVLW      110
	MOVWF      R13+0
L_main4:
	DECFSZ     R13+0, 1
	GOTO       L_main4
	DECFSZ     R12+0, 1
	GOTO       L_main4
	DECFSZ     R11+0, 1
	GOTO       L_main4
	NOP
;lab3.c,14 :: 		PORTB=0b0000111; //7
	MOVLW      7
	MOVWF      PORTB+0
;lab3.c,15 :: 		Delay_ms(1000);
	MOVLW      26
	MOVWF      R11+0
	MOVLW      94
	MOVWF      R12+0
	MOVLW      110
	MOVWF      R13+0
L_main5:
	DECFSZ     R13+0, 1
	GOTO       L_main5
	DECFSZ     R12+0, 1
	GOTO       L_main5
	DECFSZ     R11+0, 1
	GOTO       L_main5
	NOP
;lab3.c,16 :: 		PORTB=0b1111101; //6
	MOVLW      125
	MOVWF      PORTB+0
;lab3.c,17 :: 		Delay_ms(1000);
	MOVLW      26
	MOVWF      R11+0
	MOVLW      94
	MOVWF      R12+0
	MOVLW      110
	MOVWF      R13+0
L_main6:
	DECFSZ     R13+0, 1
	GOTO       L_main6
	DECFSZ     R12+0, 1
	GOTO       L_main6
	DECFSZ     R11+0, 1
	GOTO       L_main6
	NOP
;lab3.c,18 :: 		PORTB=0b1101101; //5
	MOVLW      109
	MOVWF      PORTB+0
;lab3.c,19 :: 		Delay_ms(1000);
	MOVLW      26
	MOVWF      R11+0
	MOVLW      94
	MOVWF      R12+0
	MOVLW      110
	MOVWF      R13+0
L_main7:
	DECFSZ     R13+0, 1
	GOTO       L_main7
	DECFSZ     R12+0, 1
	GOTO       L_main7
	DECFSZ     R11+0, 1
	GOTO       L_main7
	NOP
;lab3.c,20 :: 		PORTB=0b1100110; //4
	MOVLW      102
	MOVWF      PORTB+0
;lab3.c,21 :: 		Delay_ms(1000);
	MOVLW      26
	MOVWF      R11+0
	MOVLW      94
	MOVWF      R12+0
	MOVLW      110
	MOVWF      R13+0
L_main8:
	DECFSZ     R13+0, 1
	GOTO       L_main8
	DECFSZ     R12+0, 1
	GOTO       L_main8
	DECFSZ     R11+0, 1
	GOTO       L_main8
	NOP
;lab3.c,22 :: 		PORTB=0b1001111; //3
	MOVLW      79
	MOVWF      PORTB+0
;lab3.c,23 :: 		Delay_ms(1000);
	MOVLW      26
	MOVWF      R11+0
	MOVLW      94
	MOVWF      R12+0
	MOVLW      110
	MOVWF      R13+0
L_main9:
	DECFSZ     R13+0, 1
	GOTO       L_main9
	DECFSZ     R12+0, 1
	GOTO       L_main9
	DECFSZ     R11+0, 1
	GOTO       L_main9
	NOP
;lab3.c,24 :: 		PORTB=0b1011011; //2
	MOVLW      91
	MOVWF      PORTB+0
;lab3.c,25 :: 		Delay_ms(1000);
	MOVLW      26
	MOVWF      R11+0
	MOVLW      94
	MOVWF      R12+0
	MOVLW      110
	MOVWF      R13+0
L_main10:
	DECFSZ     R13+0, 1
	GOTO       L_main10
	DECFSZ     R12+0, 1
	GOTO       L_main10
	DECFSZ     R11+0, 1
	GOTO       L_main10
	NOP
;lab3.c,26 :: 		PORTB=0b0000110; //1
	MOVLW      6
	MOVWF      PORTB+0
;lab3.c,27 :: 		Delay_ms(1000);
	MOVLW      26
	MOVWF      R11+0
	MOVLW      94
	MOVWF      R12+0
	MOVLW      110
	MOVWF      R13+0
L_main11:
	DECFSZ     R13+0, 1
	GOTO       L_main11
	DECFSZ     R12+0, 1
	GOTO       L_main11
	DECFSZ     R11+0, 1
	GOTO       L_main11
	NOP
;lab3.c,28 :: 		PORTB=0b0111111; //0
	MOVLW      63
	MOVWF      PORTB+0
;lab3.c,29 :: 		PORTB=0b0000000;
	CLRF       PORTB+0
;lab3.c,31 :: 		TRISA=0;
	CLRF       TRISA+0
;lab3.c,32 :: 		PORTA=0b00000010;    // Galben masini
	MOVLW      2
	MOVWF      PORTA+0
;lab3.c,33 :: 		Delay_ms(2000);
	MOVLW      51
	MOVWF      R11+0
	MOVLW      187
	MOVWF      R12+0
	MOVLW      223
	MOVWF      R13+0
L_main12:
	DECFSZ     R13+0, 1
	GOTO       L_main12
	DECFSZ     R12+0, 1
	GOTO       L_main12
	DECFSZ     R11+0, 1
	GOTO       L_main12
	NOP
	NOP
;lab3.c,35 :: 		TRISA=0;
	CLRF       TRISA+0
;lab3.c,36 :: 		PORTA=0b00000001;   //Rosu masini
	MOVLW      1
	MOVWF      PORTA+0
;lab3.c,37 :: 		TRISD=0;
	CLRF       TRISD+0
;lab3.c,38 :: 		PORTD=0b0000100;       //VErde pietoni
	MOVLW      4
	MOVWF      PORTD+0
;lab3.c,41 :: 		TRISB=0;
	CLRF       TRISB+0
;lab3.c,42 :: 		PORTB=0b1101111; //9
	MOVLW      111
	MOVWF      PORTB+0
;lab3.c,43 :: 		Delay_ms(1000);
	MOVLW      26
	MOVWF      R11+0
	MOVLW      94
	MOVWF      R12+0
	MOVLW      110
	MOVWF      R13+0
L_main13:
	DECFSZ     R13+0, 1
	GOTO       L_main13
	DECFSZ     R12+0, 1
	GOTO       L_main13
	DECFSZ     R11+0, 1
	GOTO       L_main13
	NOP
;lab3.c,44 :: 		PORTB=0b1111111; //8
	MOVLW      127
	MOVWF      PORTB+0
;lab3.c,45 :: 		Delay_ms(1000);
	MOVLW      26
	MOVWF      R11+0
	MOVLW      94
	MOVWF      R12+0
	MOVLW      110
	MOVWF      R13+0
L_main14:
	DECFSZ     R13+0, 1
	GOTO       L_main14
	DECFSZ     R12+0, 1
	GOTO       L_main14
	DECFSZ     R11+0, 1
	GOTO       L_main14
	NOP
;lab3.c,46 :: 		PORTB=0b0000111; //7
	MOVLW      7
	MOVWF      PORTB+0
;lab3.c,47 :: 		Delay_ms(1000);
	MOVLW      26
	MOVWF      R11+0
	MOVLW      94
	MOVWF      R12+0
	MOVLW      110
	MOVWF      R13+0
L_main15:
	DECFSZ     R13+0, 1
	GOTO       L_main15
	DECFSZ     R12+0, 1
	GOTO       L_main15
	DECFSZ     R11+0, 1
	GOTO       L_main15
	NOP
;lab3.c,48 :: 		PORTB=0b1111101; //6
	MOVLW      125
	MOVWF      PORTB+0
;lab3.c,49 :: 		Delay_ms(1000);
	MOVLW      26
	MOVWF      R11+0
	MOVLW      94
	MOVWF      R12+0
	MOVLW      110
	MOVWF      R13+0
L_main16:
	DECFSZ     R13+0, 1
	GOTO       L_main16
	DECFSZ     R12+0, 1
	GOTO       L_main16
	DECFSZ     R11+0, 1
	GOTO       L_main16
	NOP
;lab3.c,50 :: 		PORTB=0b1101101; //5
	MOVLW      109
	MOVWF      PORTB+0
;lab3.c,51 :: 		Delay_ms(1000);
	MOVLW      26
	MOVWF      R11+0
	MOVLW      94
	MOVWF      R12+0
	MOVLW      110
	MOVWF      R13+0
L_main17:
	DECFSZ     R13+0, 1
	GOTO       L_main17
	DECFSZ     R12+0, 1
	GOTO       L_main17
	DECFSZ     R11+0, 1
	GOTO       L_main17
	NOP
;lab3.c,52 :: 		PORTB=0b1100110; //4
	MOVLW      102
	MOVWF      PORTB+0
;lab3.c,53 :: 		Delay_ms(1000);
	MOVLW      26
	MOVWF      R11+0
	MOVLW      94
	MOVWF      R12+0
	MOVLW      110
	MOVWF      R13+0
L_main18:
	DECFSZ     R13+0, 1
	GOTO       L_main18
	DECFSZ     R12+0, 1
	GOTO       L_main18
	DECFSZ     R11+0, 1
	GOTO       L_main18
	NOP
;lab3.c,54 :: 		PORTB=0b1001111; //3
	MOVLW      79
	MOVWF      PORTB+0
;lab3.c,55 :: 		Delay_ms(1000);
	MOVLW      26
	MOVWF      R11+0
	MOVLW      94
	MOVWF      R12+0
	MOVLW      110
	MOVWF      R13+0
L_main19:
	DECFSZ     R13+0, 1
	GOTO       L_main19
	DECFSZ     R12+0, 1
	GOTO       L_main19
	DECFSZ     R11+0, 1
	GOTO       L_main19
	NOP
;lab3.c,56 :: 		PORTB=0b1011011; //2
	MOVLW      91
	MOVWF      PORTB+0
;lab3.c,57 :: 		Delay_ms(1000);
	MOVLW      26
	MOVWF      R11+0
	MOVLW      94
	MOVWF      R12+0
	MOVLW      110
	MOVWF      R13+0
L_main20:
	DECFSZ     R13+0, 1
	GOTO       L_main20
	DECFSZ     R12+0, 1
	GOTO       L_main20
	DECFSZ     R11+0, 1
	GOTO       L_main20
	NOP
;lab3.c,58 :: 		PORTB=0b0000110; //1
	MOVLW      6
	MOVWF      PORTB+0
;lab3.c,59 :: 		Delay_ms(1000);
	MOVLW      26
	MOVWF      R11+0
	MOVLW      94
	MOVWF      R12+0
	MOVLW      110
	MOVWF      R13+0
L_main21:
	DECFSZ     R13+0, 1
	GOTO       L_main21
	DECFSZ     R12+0, 1
	GOTO       L_main21
	DECFSZ     R11+0, 1
	GOTO       L_main21
	NOP
;lab3.c,60 :: 		PORTB=0b0111111; //0
	MOVLW      63
	MOVWF      PORTB+0
;lab3.c,61 :: 		Delay_ms(1000);
	MOVLW      26
	MOVWF      R11+0
	MOVLW      94
	MOVWF      R12+0
	MOVLW      110
	MOVWF      R13+0
L_main22:
	DECFSZ     R13+0, 1
	GOTO       L_main22
	DECFSZ     R12+0, 1
	GOTO       L_main22
	DECFSZ     R11+0, 1
	GOTO       L_main22
	NOP
;lab3.c,63 :: 		PORTB=0b0000000;
	CLRF       PORTB+0
;lab3.c,65 :: 		PORTA=0b00000010;
	MOVLW      2
	MOVWF      PORTA+0
;lab3.c,66 :: 		Delay_ms(2000);
	MOVLW      51
	MOVWF      R11+0
	MOVLW      187
	MOVWF      R12+0
	MOVLW      223
	MOVWF      R13+0
L_main23:
	DECFSZ     R13+0, 1
	GOTO       L_main23
	DECFSZ     R12+0, 1
	GOTO       L_main23
	DECFSZ     R11+0, 1
	GOTO       L_main23
	NOP
	NOP
;lab3.c,67 :: 		PORTA=0b00000100;
	MOVLW      4
	MOVWF      PORTA+0
;lab3.c,68 :: 		PORTD=0b000000001;   // Rosu pietoni
	MOVLW      1
	MOVWF      PORTD+0
;lab3.c,3 :: 		for(i=0;i<10;i++)
	INCF       _i+0, 1
	BTFSC      STATUS+0, 2
	INCF       _i+1, 1
;lab3.c,69 :: 		;}
	GOTO       L_main0
L_main1:
;lab3.c,70 :: 		}
	GOTO       $+0
; end of _main
