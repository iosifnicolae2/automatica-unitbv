
_main:
;mc.c,1 :: 		void main()
;mc.c,6 :: 		TRISB=0;
	CLRF       TRISB+0
;mc.c,8 :: 		while (1)
L_main0:
;mc.c,11 :: 		for (i=1;i<129;2*i)
	MOVLW      1
	MOVWF      R1+0
	MOVLW      0
	MOVWF      R1+1
L_main2:
	MOVLW      128
	XORWF      R1+1, 0
	MOVWF      R0+0
	MOVLW      128
	SUBWF      R0+0, 0
	BTFSS      STATUS+0, 2
	GOTO       L__main7
	MOVLW      129
	SUBWF      R1+0, 0
L__main7:
	BTFSC      STATUS+0, 0
	GOTO       L_main3
;mc.c,13 :: 		PORTB=i;
	MOVF       R1+0, 0
	MOVWF      PORTB+0
;mc.c,14 :: 		Delay_ms(500);
	MOVLW      6
	MOVWF      R11+0
	MOVLW      19
	MOVWF      R12+0
	MOVLW      173
	MOVWF      R13+0
L_main5:
	DECFSZ     R13+0, 1
	GOTO       L_main5
	DECFSZ     R12+0, 1
	GOTO       L_main5
	DECFSZ     R11+0, 1
	GOTO       L_main5
	NOP
	NOP
;mc.c,15 :: 		PORTB=0;
	CLRF       PORTB+0
;mc.c,16 :: 		Delay_ms(500);   }
	MOVLW      6
	MOVWF      R11+0
	MOVLW      19
	MOVWF      R12+0
	MOVLW      173
	MOVWF      R13+0
L_main6:
	DECFSZ     R13+0, 1
	GOTO       L_main6
	DECFSZ     R12+0, 1
	GOTO       L_main6
	DECFSZ     R11+0, 1
	GOTO       L_main6
	NOP
	NOP
	GOTO       L_main2
L_main3:
;mc.c,18 :: 		}
	GOTO       L_main0
;mc.c,20 :: 		}
	GOTO       $+0
; end of _main
