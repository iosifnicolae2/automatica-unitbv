
_main:
;semafor.c,1 :: 		void main()
;semafor.c,5 :: 		TRISB=0;
	CLRF       TRISB+0
;semafor.c,6 :: 		TRISC=0;
	CLRF       TRISC+0
;semafor.c,7 :: 		TRISD=0;
	CLRF       TRISD+0
;semafor.c,8 :: 		TRISA=0;
	CLRF       TRISA+0
;semafor.c,9 :: 		TRISE=1;
	MOVLW      1
	MOVWF      TRISE+0
;semafor.c,10 :: 		TRISB.RB7=1;
	BSF        TRISB+0, 7
;semafor.c,11 :: 		a[0]=0b00111111;
	MOVLW      63
	MOVWF      main_a_L0+0
	MOVLW      0
	MOVWF      main_a_L0+1
;semafor.c,12 :: 		a[1]=0b00000110;
	MOVLW      6
	MOVWF      main_a_L0+2
	MOVLW      0
	MOVWF      main_a_L0+3
;semafor.c,13 :: 		a[2]=0b01011011;
	MOVLW      91
	MOVWF      main_a_L0+4
	MOVLW      0
	MOVWF      main_a_L0+5
;semafor.c,14 :: 		a[3]=0b01001111;
	MOVLW      79
	MOVWF      main_a_L0+6
	MOVLW      0
	MOVWF      main_a_L0+7
;semafor.c,15 :: 		a[4]=0b01100110;
	MOVLW      102
	MOVWF      main_a_L0+8
	MOVLW      0
	MOVWF      main_a_L0+9
;semafor.c,16 :: 		a[5]=0b01101101;
	MOVLW      109
	MOVWF      main_a_L0+10
	MOVLW      0
	MOVWF      main_a_L0+11
;semafor.c,17 :: 		a[6]=0b01111101;
	MOVLW      125
	MOVWF      main_a_L0+12
	MOVLW      0
	MOVWF      main_a_L0+13
;semafor.c,18 :: 		a[7]=0b00000111;
	MOVLW      7
	MOVWF      main_a_L0+14
	MOVLW      0
	MOVWF      main_a_L0+15
;semafor.c,19 :: 		a[8]=0b01111111;
	MOVLW      127
	MOVWF      main_a_L0+16
	MOVLW      0
	MOVWF      main_a_L0+17
;semafor.c,20 :: 		a[9]=0b01101111;
	MOVLW      111
	MOVWF      main_a_L0+18
	MOVLW      0
	MOVWF      main_a_L0+19
;semafor.c,24 :: 		while(1)
L_main0:
;semafor.c,26 :: 		PORTD=4;
	MOVLW      4
	MOVWF      PORTD+0
;semafor.c,27 :: 		PORTC=1;
	MOVLW      1
	MOVWF      PORTC+0
;semafor.c,28 :: 		if (PORTB.RB7==1)
	BTFSS      PORTB+0, 7
	GOTO       L_main2
;semafor.c,30 :: 		PORTD=4;
	MOVLW      4
	MOVWF      PORTD+0
;semafor.c,31 :: 		PORTC=1;
	MOVLW      1
	MOVWF      PORTC+0
;semafor.c,32 :: 		PORTC=2;
	MOVLW      2
	MOVWF      PORTC+0
;semafor.c,33 :: 		delay_ms(1000);
	MOVLW      11
	MOVWF      R11+0
	MOVLW      38
	MOVWF      R12+0
	MOVLW      93
	MOVWF      R13+0
L_main3:
	DECFSZ     R13+0, 1
	GOTO       L_main3
	DECFSZ     R12+0, 1
	GOTO       L_main3
	DECFSZ     R11+0, 1
	GOTO       L_main3
	NOP
	NOP
;semafor.c,34 :: 		PORTC=1;
	MOVLW      1
	MOVWF      PORTC+0
;semafor.c,35 :: 		PORTD=4;
	MOVLW      4
	MOVWF      PORTD+0
;semafor.c,36 :: 		for (i=9;i>=0;i--)
	MOVLW      9
	MOVWF      R3+0
	MOVLW      0
	MOVWF      R3+1
L_main4:
	MOVLW      128
	XORWF      R3+1, 0
	MOVWF      R0+0
	MOVLW      128
	SUBWF      R0+0, 0
	BTFSS      STATUS+0, 2
	GOTO       L__main22
	MOVLW      0
	SUBWF      R3+0, 0
L__main22:
	BTFSS      STATUS+0, 0
	GOTO       L_main5
;semafor.c,38 :: 		PORTB=a[i];
	MOVF       R3+0, 0
	MOVWF      R0+0
	MOVF       R3+1, 0
	MOVWF      R0+1
	RLF        R0+0, 1
	RLF        R0+1, 1
	BCF        R0+0, 0
	MOVF       R0+0, 0
	ADDLW      main_a_L0+0
	MOVWF      FSR
	MOVF       INDF+0, 0
	MOVWF      PORTB+0
;semafor.c,39 :: 		delay_ms(1000);
	MOVLW      11
	MOVWF      R11+0
	MOVLW      38
	MOVWF      R12+0
	MOVLW      93
	MOVWF      R13+0
L_main7:
	DECFSZ     R13+0, 1
	GOTO       L_main7
	DECFSZ     R12+0, 1
	GOTO       L_main7
	DECFSZ     R11+0, 1
	GOTO       L_main7
	NOP
	NOP
;semafor.c,36 :: 		for (i=9;i>=0;i--)
	MOVLW      1
	SUBWF      R3+0, 1
	BTFSS      STATUS+0, 0
	DECF       R3+1, 1
;semafor.c,40 :: 		}
	GOTO       L_main4
L_main5:
;semafor.c,42 :: 		}
L_main2:
;semafor.c,45 :: 		PORTA= 0b0000110;
	MOVLW      6
	MOVWF      PORTA+0
;semafor.c,46 :: 		PORTE=0;
	CLRF       PORTE+0
;semafor.c,48 :: 		for (i=9;i>=0;i--)
	MOVLW      9
	MOVWF      R3+0
	MOVLW      0
	MOVWF      R3+1
L_main8:
	MOVLW      128
	XORWF      R3+1, 0
	MOVWF      R0+0
	MOVLW      128
	SUBWF      R0+0, 0
	BTFSS      STATUS+0, 2
	GOTO       L__main23
	MOVLW      0
	SUBWF      R3+0, 0
L__main23:
	BTFSS      STATUS+0, 0
	GOTO       L_main9
;semafor.c,50 :: 		PORTB=a[i];
	MOVF       R3+0, 0
	MOVWF      R0+0
	MOVF       R3+1, 0
	MOVWF      R0+1
	RLF        R0+0, 1
	RLF        R0+1, 1
	BCF        R0+0, 0
	MOVF       R0+0, 0
	ADDLW      main_a_L0+0
	MOVWF      FSR
	MOVF       INDF+0, 0
	MOVWF      PORTB+0
;semafor.c,51 :: 		delay_ms(1000);
	MOVLW      11
	MOVWF      R11+0
	MOVLW      38
	MOVWF      R12+0
	MOVLW      93
	MOVWF      R13+0
L_main11:
	DECFSZ     R13+0, 1
	GOTO       L_main11
	DECFSZ     R12+0, 1
	GOTO       L_main11
	DECFSZ     R11+0, 1
	GOTO       L_main11
	NOP
	NOP
;semafor.c,48 :: 		for (i=9;i>=0;i--)
	MOVLW      1
	SUBWF      R3+0, 1
	BTFSS      STATUS+0, 0
	DECF       R3+1, 1
;semafor.c,52 :: 		}
	GOTO       L_main8
L_main9:
;semafor.c,53 :: 		PORTA= 0b00011111;
	MOVLW      31
	MOVWF      PORTA+0
;semafor.c,54 :: 		PORTC=128;
	MOVLW      128
	MOVWF      PORTC+0
;semafor.c,55 :: 		for (i=9;i>=0;i--)
	MOVLW      9
	MOVWF      R3+0
	MOVLW      0
	MOVWF      R3+1
L_main12:
	MOVLW      128
	XORWF      R3+1, 0
	MOVWF      R0+0
	MOVLW      128
	SUBWF      R0+0, 0
	BTFSS      STATUS+0, 2
	GOTO       L__main24
	MOVLW      0
	SUBWF      R3+0, 0
L__main24:
	BTFSS      STATUS+0, 0
	GOTO       L_main13
;semafor.c,57 :: 		PORTB=a[i];
	MOVF       R3+0, 0
	MOVWF      R0+0
	MOVF       R3+1, 0
	MOVWF      R0+1
	RLF        R0+0, 1
	RLF        R0+1, 1
	BCF        R0+0, 0
	MOVF       R0+0, 0
	ADDLW      main_a_L0+0
	MOVWF      FSR
	MOVF       INDF+0, 0
	MOVWF      PORTB+0
;semafor.c,58 :: 		delay_ms(1000);
	MOVLW      11
	MOVWF      R11+0
	MOVLW      38
	MOVWF      R12+0
	MOVLW      93
	MOVWF      R13+0
L_main15:
	DECFSZ     R13+0, 1
	GOTO       L_main15
	DECFSZ     R12+0, 1
	GOTO       L_main15
	DECFSZ     R11+0, 1
	GOTO       L_main15
	NOP
	NOP
;semafor.c,55 :: 		for (i=9;i>=0;i--)
	MOVLW      1
	SUBWF      R3+0, 1
	BTFSS      STATUS+0, 0
	DECF       R3+1, 1
;semafor.c,59 :: 		}
	GOTO       L_main12
L_main13:
;semafor.c,60 :: 		PORTD=2;
	MOVLW      2
	MOVWF      PORTD+0
;semafor.c,61 :: 		delay_ms(1000);
	MOVLW      11
	MOVWF      R11+0
	MOVLW      38
	MOVWF      R12+0
	MOVLW      93
	MOVWF      R13+0
L_main16:
	DECFSZ     R13+0, 1
	GOTO       L_main16
	DECFSZ     R12+0, 1
	GOTO       L_main16
	DECFSZ     R11+0, 1
	GOTO       L_main16
	NOP
	NOP
;semafor.c,62 :: 		PORTD=1;
	MOVLW      1
	MOVWF      PORTD+0
;semafor.c,63 :: 		PORTC=4;
	MOVLW      4
	MOVWF      PORTC+0
;semafor.c,65 :: 		for (i=9;i>=0;i--)
	MOVLW      9
	MOVWF      R3+0
	MOVLW      0
	MOVWF      R3+1
L_main17:
	MOVLW      128
	XORWF      R3+1, 0
	MOVWF      R0+0
	MOVLW      128
	SUBWF      R0+0, 0
	BTFSS      STATUS+0, 2
	GOTO       L__main25
	MOVLW      0
	SUBWF      R3+0, 0
L__main25:
	BTFSS      STATUS+0, 0
	GOTO       L_main18
;semafor.c,67 :: 		PORTB=a[i];
	MOVF       R3+0, 0
	MOVWF      R0+0
	MOVF       R3+1, 0
	MOVWF      R0+1
	RLF        R0+0, 1
	RLF        R0+1, 1
	BCF        R0+0, 0
	MOVF       R0+0, 0
	ADDLW      main_a_L0+0
	MOVWF      FSR
	MOVF       INDF+0, 0
	MOVWF      PORTB+0
;semafor.c,68 :: 		delay_ms(1000);
	MOVLW      11
	MOVWF      R11+0
	MOVLW      38
	MOVWF      R12+0
	MOVLW      93
	MOVWF      R13+0
L_main20:
	DECFSZ     R13+0, 1
	GOTO       L_main20
	DECFSZ     R12+0, 1
	GOTO       L_main20
	DECFSZ     R11+0, 1
	GOTO       L_main20
	NOP
	NOP
;semafor.c,65 :: 		for (i=9;i>=0;i--)
	MOVLW      1
	SUBWF      R3+0, 1
	BTFSS      STATUS+0, 0
	DECF       R3+1, 1
;semafor.c,69 :: 		}
	GOTO       L_main17
L_main18:
;semafor.c,70 :: 		PORTC=2;
	MOVLW      2
	MOVWF      PORTC+0
;semafor.c,71 :: 		delay_ms(1000);
	MOVLW      11
	MOVWF      R11+0
	MOVLW      38
	MOVWF      R12+0
	MOVLW      93
	MOVWF      R13+0
L_main21:
	DECFSZ     R13+0, 1
	GOTO       L_main21
	DECFSZ     R12+0, 1
	GOTO       L_main21
	DECFSZ     R11+0, 1
	GOTO       L_main21
	NOP
	NOP
;semafor.c,73 :: 		}
	GOTO       L_main0
;semafor.c,74 :: 		}
	GOTO       $+0
; end of _main
