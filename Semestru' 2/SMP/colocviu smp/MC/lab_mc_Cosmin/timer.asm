
_interrupt:
	MOVWF      R15+0
	SWAPF      STATUS+0, 0
	CLRF       STATUS+0
	MOVWF      ___saveSTATUS+0
	MOVF       PCLATH+0, 0
	MOVWF      ___savePCLATH+0
	CLRF       PCLATH+0
;timer.c,20 :: 		void interrupt()
;timer.c,22 :: 		INTCON.GIE = 0;
	BCF        INTCON+0, 7
;timer.c,23 :: 		if(INTCON.T0IF)
	BTFSS      INTCON+0, 2
	GOTO       L_interrupt0
;timer.c,25 :: 		count++;
	INCF       _count+0, 1
	BTFSC      STATUS+0, 2
	INCF       _count+1, 1
;timer.c,26 :: 		INTCON.T0IF = 0;
	BCF        INTCON+0, 2
;timer.c,27 :: 		if(count == 1000)
	MOVF       _count+1, 0
	XORLW      3
	BTFSS      STATUS+0, 2
	GOTO       L__interrupt5
	MOVLW      232
	XORWF      _count+0, 0
L__interrupt5:
	BTFSS      STATUS+0, 2
	GOTO       L_interrupt1
;timer.c,29 :: 		count = 0;
	CLRF       _count+0
	CLRF       _count+1
;timer.c,30 :: 		secunde ++;
	INCF       _secunde+0, 1
	BTFSC      STATUS+0, 2
	INCF       _secunde+1, 1
;timer.c,31 :: 		}
L_interrupt1:
;timer.c,32 :: 		TMR0 = 5;
	MOVLW      5
	MOVWF      TMR0+0
;timer.c,33 :: 		}
L_interrupt0:
;timer.c,34 :: 		INTCON.GIE = 1;
	BSF        INTCON+0, 7
;timer.c,35 :: 		}
L__interrupt4:
	MOVF       ___savePCLATH+0, 0
	MOVWF      PCLATH+0
	SWAPF      ___saveSTATUS+0, 0
	MOVWF      STATUS+0
	SWAPF      R15+0, 1
	SWAPF      R15+0, 0
	RETFIE
; end of _interrupt

_main:
;timer.c,36 :: 		void main()
;timer.c,38 :: 		TRISB = 0;
	CLRF       TRISB+0
;timer.c,39 :: 		PORTB = 0b00000000;
	CLRF       PORTB+0
;timer.c,40 :: 		TMR0 = 5;
	MOVLW      5
	MOVWF      TMR0+0
;timer.c,42 :: 		INTCON = 0b11100000;
	MOVLW      224
	MOVWF      INTCON+0
;timer.c,43 :: 		OPTION_REG = 0b00000000;
	CLRF       OPTION_REG+0
;timer.c,44 :: 		OPTION_REG.PSA = 1;
	BSF        OPTION_REG+0, 3
;timer.c,45 :: 		Lcd_Init();
	CALL       _Lcd_Init+0
;timer.c,46 :: 		Lcd_Cmd(_LCD_CURSOR_OFF);
	MOVLW      12
	MOVWF      FARG_Lcd_Cmd_out_char+0
	CALL       _Lcd_Cmd+0
;timer.c,47 :: 		while(1)
L_main2:
;timer.c,49 :: 		sprinti(sir,"secunde = %d",secunde);
	MOVLW      _sir+0
	MOVWF      FARG_sprinti_wh+0
	MOVLW      ?lstr_1_timer+0
	MOVWF      FARG_sprinti_f+0
	MOVLW      hi_addr(?lstr_1_timer+0
	MOVWF      FARG_sprinti_f+1
	MOVF       _secunde+0, 0
	MOVWF      FARG_sprinti_wh+3
	MOVF       _secunde+1, 0
	MOVWF      FARG_sprinti_wh+4
	CALL       _sprinti+0
;timer.c,50 :: 		Lcd_Out(1,1,sir);
	MOVLW      1
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      1
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      _sir+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
;timer.c,51 :: 		}
	GOTO       L_main2
;timer.c,52 :: 		}
	GOTO       $+0
; end of _main
