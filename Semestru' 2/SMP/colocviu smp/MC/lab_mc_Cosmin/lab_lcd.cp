#line 1 "D:/studenti/4493/nitaCosmin/lab_lcd.c"

sbit LCD_RS at RB4_bit;
sbit LCD_EN at RB5_bit;
sbit LCD_D4 at RB0_bit;
sbit LCD_D5 at RB1_bit;
sbit LCD_D6 at RB2_bit;
sbit LCD_D7 at RB3_bit;

sbit LCD_RS_Direction at TRISB4_bit;
sbit LCD_EN_Direction at TRISB5_bit;
sbit LCD_D4_Direction at TRISB0_bit;
sbit LCD_D5_Direction at TRISB1_bit;
sbit LCD_D6_Direction at TRISB2_bit;
sbit LCD_D7_Direction at TRISB3_bit;


char text[16];

 int numar;
void main()
{
 numar = 5;
 TRISB = 0;
 PORTB = 0xFF;
 TRISB = 0xff;

 Lcd_Init();

 Lcd_Cmd(_LCD_CLEAR);



 while(1)
 {

 sprinti(text, "X = %d", numar);
 Lcd_Out(1,1,text);
 numar++;
 delay_ms(1000);

 }

}
