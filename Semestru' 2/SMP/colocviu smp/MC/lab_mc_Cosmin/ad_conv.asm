
_readAdc:
;ad_conv.c,15 :: 		unsigned int readAdc()
;ad_conv.c,17 :: 		ADCON0.ADON = 1;
	BSF        ADCON0+0, 0
;ad_conv.c,19 :: 		ADCON0.GO_DONE = 1;
	BSF        ADCON0+0, 1
;ad_conv.c,20 :: 		while(ADCON0.GO_DONE == 1)
L_readAdc0:
	BTFSS      ADCON0+0, 1
	GOTO       L_readAdc1
;ad_conv.c,21 :: 		{;}
	GOTO       L_readAdc0
L_readAdc1:
;ad_conv.c,23 :: 		ADCON0.ADON = 0;
	BCF        ADCON0+0, 0
;ad_conv.c,25 :: 		return ADRESL;
	MOVF       ADRESL+0, 0
	MOVWF      R0+0
	CLRF       R0+1
;ad_conv.c,26 :: 		}
	RETURN
; end of _readAdc

_main:
;ad_conv.c,29 :: 		void main()
;ad_conv.c,31 :: 		ADCON1.ADFM = 0;
	BCF        ADCON1+0, 7
;ad_conv.c,32 :: 		ADCON1.VCFG1 = 0;
	BCF        ADCON1+0, 6
;ad_conv.c,33 :: 		ADCON1.VCFG1 = 0;
	BCF        ADCON1+0, 6
;ad_conv.c,34 :: 		ADCON1.CHS0 = 0;
	BCF        ADCON1+0, 2
;ad_conv.c,35 :: 		ADCON1.CHS1 = 0;
	BCF        ADCON1+0, 3
;ad_conv.c,36 :: 		ADCON1.CHS2 = 0;
	BCF        ADCON1+0, 4
;ad_conv.c,37 :: 		ADCON1.GO_DONE = 0;
	BCF        ADCON1+0, 1
;ad_conv.c,38 :: 		ADCON1.ADON = 0;
	BCF        ADCON1+0, 0
;ad_conv.c,39 :: 		ADCON1.ADCS2 = 1;
	BSF        ADCON1+0, 6
;ad_conv.c,40 :: 		ADCON1.ADCS1 = 0;
	BCF        ADCON1+0, 5
;ad_conv.c,41 :: 		ADCON1.ADCS0 = 0;
	BCF        ADCON1+0, 4
;ad_conv.c,42 :: 		TRISA = 255;
	MOVLW      255
	MOVWF      TRISA+0
;ad_conv.c,43 :: 		TRISB = 0b00000000;
	CLRF       TRISB+0
;ad_conv.c,44 :: 		ANSEL = 255;
	MOVLW      255
	MOVWF      ANSEL+0
;ad_conv.c,45 :: 		Lcd_Init();
	CALL       _Lcd_Init+0
;ad_conv.c,46 :: 		Lcd_Cmd(_LCD_CURSOR_OFF);
	MOVLW      12
	MOVWF      FARG_Lcd_Cmd_out_char+0
	CALL       _Lcd_Cmd+0
;ad_conv.c,48 :: 		while(1)
L_main2:
;ad_conv.c,50 :: 		val = readADc();
	CALL       _readAdc+0
	MOVF       R0+0, 0
	MOVWF      _val+0
	MOVF       R0+1, 0
	MOVWF      _val+1
;ad_conv.c,51 :: 		sprinti(ADC,"ADC = %d", val);
	MOVLW      _ADC+0
	MOVWF      FARG_sprinti_wh+0
	MOVLW      ?lstr_1_ad_conv+0
	MOVWF      FARG_sprinti_f+0
	MOVLW      hi_addr(?lstr_1_ad_conv+0
	MOVWF      FARG_sprinti_f+1
	MOVF       R0+0, 0
	MOVWF      FARG_sprinti_wh+3
	MOVF       R0+1, 0
	MOVWF      FARG_sprinti_wh+4
	CALL       _sprinti+0
;ad_conv.c,52 :: 		Lcd_out(1,1,ADC);
	MOVLW      1
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      1
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      _ADC+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
;ad_conv.c,53 :: 		Delay_ms(500);
	MOVLW      6
	MOVWF      R11+0
	MOVLW      19
	MOVWF      R12+0
	MOVLW      173
	MOVWF      R13+0
L_main4:
	DECFSZ     R13+0, 1
	GOTO       L_main4
	DECFSZ     R12+0, 1
	GOTO       L_main4
	DECFSZ     R11+0, 1
	GOTO       L_main4
	NOP
	NOP
;ad_conv.c,54 :: 		}
	GOTO       L_main2
;ad_conv.c,56 :: 		}
	GOTO       $+0
; end of _main
