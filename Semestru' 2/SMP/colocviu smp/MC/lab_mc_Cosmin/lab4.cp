#line 1 "D:/studenti/4493/nitaCosmin/lab4.c"
enum State
{
 STATE_A = 0,
 STATE_B = 1,
 STATE_C = 2
} CurrentState;
void main()
{

 char Output[3];
 Output[ STATE_A ] = 0b00000000;
 Output[ STATE_B ] = 0b00000001;
 Output[ STATE_C ] = 0b00000010;
 CurrentState = STATE_A;

 TRISB = 0b11111111;
 TRISC = 0b00000000;

 while(1)
 {
 if( CurrentState == STATE_A && PORTB.RB0 == 1)
 CurrentState = STATE_B;
 if( CurrentState == STATE_B && PORTB.RB1 == 1 )
 CurrentState = STATE_C;
 if( CurrentState == STATE_C && PORTB.RB2 == 1 )
 CurrentState = STATE_A;

 PORTC = Output[ CurrentState ];

 }
}
