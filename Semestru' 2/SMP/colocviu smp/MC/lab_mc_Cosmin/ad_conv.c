sbit LCD_RS at RB4_bit;
sbit LCD_EN at RB5_bit;
sbit LCD_D4 at RB0_bit;
sbit LCD_D5 at RB1_bit;
sbit LCD_D6 at RB2_bit;
sbit LCD_D7 at RB3_bit;

sbit LCD_RS_Direction at TRISB4_bit;
sbit LCD_EN_Direction at TRISB5_bit;
sbit LCD_D4_Direction at TRISB0_bit;
sbit LCD_D5_Direction at TRISB1_bit;
sbit LCD_D6_Direction at TRISB2_bit;
sbit LCD_D7_Direction at TRISB3_bit;

unsigned int readAdc()
{
  ADCON0.ADON = 1;

  ADCON0.GO_DONE = 1;
  while(ADCON0.GO_DONE == 1)
  {;}

  ADCON0.ADON = 0;

  return ADRESL;
}
unsigned int val = 0;
char ADC[10];
void main()
{
   ADCON1.ADFM = 0;
   ADCON1.VCFG1 = 0;
   ADCON1.VCFG1 = 0;
   ADCON1.CHS0 = 0;
   ADCON1.CHS1 = 0;
   ADCON1.CHS2 = 0;
   ADCON1.GO_DONE = 0;
   ADCON1.ADON = 0;
   ADCON1.ADCS2 = 1;
   ADCON1.ADCS1 = 0;
   ADCON1.ADCS0 = 0;
   TRISA = 255;
   TRISB = 0b00000000;
   ANSEL = 255;
   Lcd_Init();
   Lcd_Cmd(_LCD_CURSOR_OFF);

   while(1)
   {
       val = readADc();
       sprinti(ADC,"ADC = %d", val);
       Lcd_out(1,1,ADC);
       Delay_ms(500);
   }

}
