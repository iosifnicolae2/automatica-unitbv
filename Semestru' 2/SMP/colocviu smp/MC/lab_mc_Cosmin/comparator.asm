
_InitCm:
;comparator.c,1 :: 		void InitCm()
;comparator.c,3 :: 		CMCON0.C2OUT = 0;
	BCF        CMCON0+0, 7
;comparator.c,4 :: 		CMCON0.C1OUT = 0;
	BCF        CMCON0+0, 6
;comparator.c,5 :: 		CMCON0.C2INV = 0;
	BCF        CMCON0+0, 5
;comparator.c,6 :: 		CMCON0.C1INV = 0;
	BCF        CMCON0+0, 4
;comparator.c,7 :: 		CMCON0.CIS = 0;
	BCF        CMCON0+0, 3
;comparator.c,8 :: 		CMCON0.CM2 = 1;
	BSF        CMCON0+0, 2
;comparator.c,9 :: 		CMCON0.CM1 = 0;
	BCF        CMCON0+0, 1
;comparator.c,10 :: 		CMCON0.CM0 = 0;
	BCF        CMCON0+0, 0
;comparator.c,11 :: 		CMCON1 = 0x00;
	CLRF       CMCON1+0
;comparator.c,12 :: 		TRISA = 0b00001111;
	MOVLW      15
	MOVWF      TRISA+0
;comparator.c,14 :: 		TRISB = 0x00;
	CLRF       TRISB+0
;comparator.c,15 :: 		PORTB = 0x00;
	CLRF       PORTB+0
;comparator.c,16 :: 		}
	RETURN
; end of _InitCm

_Stare:
;comparator.c,17 :: 		char Stare(char comparator)
;comparator.c,19 :: 		if(comparator)
	MOVF       FARG_Stare_comparator+0, 0
	BTFSC      STATUS+0, 2
	GOTO       L_Stare0
;comparator.c,20 :: 		return CMCON0.C2OUT;
	MOVLW      0
	BTFSC      CMCON0+0, 7
	MOVLW      1
	MOVWF      R0+0
	RETURN
L_Stare0:
;comparator.c,22 :: 		return CMCON0.C1OUT;
	MOVLW      0
	BTFSC      CMCON0+0, 6
	MOVLW      1
	MOVWF      R0+0
;comparator.c,23 :: 		}
	RETURN
; end of _Stare

_main:
;comparator.c,24 :: 		void main()
;comparator.c,26 :: 		InitCm();
	CALL       _InitCm+0
;comparator.c,27 :: 		while(1)
L_main2:
;comparator.c,29 :: 		PORTB.RB0 = CMCON0.C2OUT;;
	BTFSC      CMCON0+0, 7
	GOTO       L__main5
	BCF        PORTB+0, 0
	GOTO       L__main6
L__main5:
	BSF        PORTB+0, 0
L__main6:
;comparator.c,30 :: 		delay_ms(100);
	MOVLW      2
	MOVWF      R11+0
	MOVLW      4
	MOVWF      R12+0
	MOVLW      186
	MOVWF      R13+0
L_main4:
	DECFSZ     R13+0, 1
	GOTO       L_main4
	DECFSZ     R12+0, 1
	GOTO       L_main4
	DECFSZ     R11+0, 1
	GOTO       L_main4
	NOP
;comparator.c,31 :: 		}
	GOTO       L_main2
;comparator.c,32 :: 		}
	GOTO       $+0
; end of _main
