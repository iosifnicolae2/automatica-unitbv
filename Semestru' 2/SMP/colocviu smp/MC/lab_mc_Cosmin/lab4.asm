
_main:
;lab4.c,7 :: 		void main()
;lab4.c,11 :: 		Output[ STATE_A ] = 0b00000000;
	CLRF       R1+0
;lab4.c,12 :: 		Output[ STATE_B ] = 0b00000001;
	MOVLW      1
	MOVWF      R1+1
;lab4.c,13 :: 		Output[ STATE_C ] = 0b00000010;
	MOVLW      2
	MOVWF      R1+2
;lab4.c,14 :: 		CurrentState = STATE_A;
	CLRF       _CurrentState+0
;lab4.c,16 :: 		TRISB = 0b11111111;
	MOVLW      255
	MOVWF      TRISB+0
;lab4.c,17 :: 		TRISC = 0b00000000;
	CLRF       TRISC+0
;lab4.c,19 :: 		while(1)
L_main0:
;lab4.c,21 :: 		if( CurrentState == STATE_A && PORTB.RB0 == 1)
	MOVF       _CurrentState+0, 0
	XORLW      0
	BTFSS      STATUS+0, 2
	GOTO       L_main4
	MOVF       PORTB+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main4
L__main13:
;lab4.c,22 :: 		CurrentState = STATE_B;
	MOVLW      1
	MOVWF      _CurrentState+0
L_main4:
;lab4.c,23 :: 		if( CurrentState == STATE_B && PORTB.RB1 == 1 )
	MOVF       _CurrentState+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main7
	MOVF       PORTB+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main7
L__main12:
;lab4.c,24 :: 		CurrentState = STATE_C;
	MOVLW      2
	MOVWF      _CurrentState+0
L_main7:
;lab4.c,25 :: 		if( CurrentState == STATE_C && PORTB.RB2 == 1 )
	MOVF       _CurrentState+0, 0
	XORLW      2
	BTFSS      STATUS+0, 2
	GOTO       L_main10
	MOVF       PORTB+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main10
L__main11:
;lab4.c,26 :: 		CurrentState = STATE_A;
	CLRF       _CurrentState+0
L_main10:
;lab4.c,28 :: 		PORTC = Output[ CurrentState ];
	MOVF       _CurrentState+0, 0
	ADDLW      R1+0
	MOVWF      FSR
	MOVF       INDF+0, 0
	MOVWF      PORTC+0
;lab4.c,30 :: 		}
	GOTO       L_main0
;lab4.c,31 :: 		}
	GOTO       $+0
; end of _main
