
_init_adc:
;lab7.c,16 :: 		void init_adc()
;lab7.c,18 :: 		ANSEL=255;
	MOVLW      255
	MOVWF      ANSEL+0
;lab7.c,19 :: 		ADCON0.adfm=0;
	BCF        ADCON0+0, 7
;lab7.c,20 :: 		ADCON0.VCFG1=0;
	BCF        ADCON0+0, 6
;lab7.c,21 :: 		ADCON0.VCFG0=0;
	BCF        ADCON0+0, 5
;lab7.c,22 :: 		ADCON0.CHS2=0;
	BCF        ADCON0+0, 4
;lab7.c,23 :: 		ADCON0.CHS1=0;
	BCF        ADCON0+0, 3
;lab7.c,24 :: 		ADCON0.CHS0=0;
	BCF        ADCON0+0, 2
;lab7.c,25 :: 		ADCON0.GO_DONE=0;
	BCF        ADCON0+0, 1
;lab7.c,26 :: 		ADCON0.ADON=0;
	BCF        ADCON0+0, 0
;lab7.c,27 :: 		ADCON0.ADCS2=0;
	BCF        ADCON0+0, 6
;lab7.c,28 :: 		ADCON0.ADCS1=0;
	BCF        ADCON0+0, 5
;lab7.c,29 :: 		ADCON0.ADCS0=0;
	BCF        ADCON0+0, 4
;lab7.c,30 :: 		trisb=0b00000000;
	CLRF       TRISB+0
;lab7.c,31 :: 		portb=0;
	CLRF       PORTB+0
;lab7.c,32 :: 		}
	RETURN
; end of _init_adc

_read_adc:
;lab7.c,35 :: 		int read_adc()
;lab7.c,38 :: 		ADCON0.ADON=1;
	BSF        ADCON0+0, 0
;lab7.c,39 :: 		ADCON0.GO_DONE=1;
	BSF        ADCON0+0, 1
;lab7.c,40 :: 		while(ADCON0.GO_DONE)
L_read_adc0:
	BTFSS      ADCON0+0, 1
	GOTO       L_read_adc1
;lab7.c,41 :: 		{}
	GOTO       L_read_adc0
L_read_adc1:
;lab7.c,42 :: 		ADCON0.GO_DONE=0;
	BCF        ADCON0+0, 1
;lab7.c,44 :: 		result|=ADRESL>>6|ADRESH<<2;
	MOVLW      6
	MOVWF      R0+0
	MOVF       ADRESL+0, 0
	MOVWF      R3+0
	MOVF       R0+0, 0
L__read_adc5:
	BTFSC      STATUS+0, 2
	GOTO       L__read_adc6
	RRF        R3+0, 1
	BCF        R3+0, 7
	ADDLW      255
	GOTO       L__read_adc5
L__read_adc6:
	MOVF       ADRESH+0, 0
	MOVWF      R0+0
	CLRF       R0+1
	RLF        R0+0, 1
	RLF        R0+1, 1
	BCF        R0+0, 0
	RLF        R0+0, 1
	RLF        R0+1, 1
	BCF        R0+0, 0
	MOVF       R3+0, 0
	IORWF      R0+0, 1
	MOVLW      0
	IORWF      R0+1, 1
	MOVF       R4+0, 0
	IORWF      R0+0, 1
	MOVF       R4+1, 0
	IORWF      R0+1, 1
	MOVF       R0+0, 0
	MOVWF      R4+0
	MOVF       R0+1, 0
	MOVWF      R4+1
;lab7.c,45 :: 		return result;
;lab7.c,46 :: 		}
	RETURN
; end of _read_adc

_main:
;lab7.c,57 :: 		void main() {
;lab7.c,62 :: 		Lcd_Init();
	CALL       _Lcd_Init+0
;lab7.c,63 :: 		init_adc();
	CALL       _init_adc+0
;lab7.c,64 :: 		while(1)
L_main2:
;lab7.c,66 :: 		a=read_adc();
	CALL       _read_adc+0
;lab7.c,67 :: 		b=5*a/1024;
	MOVLW      5
	MOVWF      R4+0
	MOVLW      0
	MOVWF      R4+1
	CALL       _Mul_16x16_U+0
	MOVLW      10
	MOVWF      R2+0
	MOVF       R0+0, 0
	MOVWF      FARG_sprinti_wh+3
	MOVF       R0+1, 0
	MOVWF      FARG_sprinti_wh+4
	MOVF       R2+0, 0
L__main7:
	BTFSC      STATUS+0, 2
	GOTO       L__main8
	RRF        FARG_sprinti_wh+4, 1
	RRF        FARG_sprinti_wh+3, 1
	BCF        FARG_sprinti_wh+4, 7
	BTFSC      FARG_sprinti_wh+4, 6
	BSF        FARG_sprinti_wh+4, 7
	ADDLW      255
	GOTO       L__main7
L__main8:
;lab7.c,68 :: 		sprinti(sir,"temp=%d",b);
	MOVLW      main_sir_L0+0
	MOVWF      FARG_sprinti_wh+0
	MOVLW      ?lstr_1_lab7+0
	MOVWF      FARG_sprinti_f+0
	MOVLW      hi_addr(?lstr_1_lab7+0
	MOVWF      FARG_sprinti_f+1
	CALL       _sprinti+0
;lab7.c,69 :: 		Lcd_Out(1,1,sir);
	MOVLW      1
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      1
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      main_sir_L0+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
;lab7.c,70 :: 		delay_ms(1000);
	MOVLW      2
	MOVWF      R11+0
	MOVLW      69
	MOVWF      R12+0
	MOVLW      169
	MOVWF      R13+0
L_main4:
	DECFSZ     R13+0, 1
	GOTO       L_main4
	DECFSZ     R12+0, 1
	GOTO       L_main4
	DECFSZ     R11+0, 1
	GOTO       L_main4
	NOP
	NOP
;lab7.c,71 :: 		}
	GOTO       L_main2
;lab7.c,79 :: 		}
	GOTO       $+0
; end of _main
