
_init_cm:
;lab9.c,3 :: 		void init_cm()
;lab9.c,4 :: 		{    CMCON0.C2OUT=0;//DACA SEMNALUL DE P INTRAREA POZITIVA> SEMN IN NEGATVA SI INVERS;INITIALIZARE
	BCF        CMCON0+0, 7
;lab9.c,5 :: 		CMCON0.C1OUT=0;
	BCF        CMCON0+0, 6
;lab9.c,6 :: 		CMCON0.C2INV=0;//INVERSEAZA SEMNALELE DAK C1>C2
	BCF        CMCON0+0, 5
;lab9.c,7 :: 		CMCON0.C1INV=0;
	BCF        CMCON0+0, 4
;lab9.c,8 :: 		CMCON0.CIS=0;      //INTRAREA NEGATIVA P C PIN SA FIE RA0 SAU RA1
	BCF        CMCON0+0, 3
;lab9.c,9 :: 		CMCON0.CM2=1;
	BSF        CMCON0+0, 2
;lab9.c,10 :: 		CMCON0.CM1=0;
	BCF        CMCON0+0, 1
;lab9.c,11 :: 		CMCON0.CM0=0;
	BCF        CMCON0+0, 0
;lab9.c,12 :: 		CMCON1=0b00000000;// NU AVEM TIMER SAU CEVA
	CLRF       CMCON1+0
;lab9.c,17 :: 		TRISA=0b00001111;//PT SEMNALE DE INTRARE
	MOVLW      15
	MOVWF      TRISA+0
;lab9.c,18 :: 		PORTA=0;
	CLRF       PORTA+0
;lab9.c,19 :: 		TRISB=0b00000000;//LED
	CLRF       TRISB+0
;lab9.c,20 :: 		PORTB=0;
	CLRF       PORTB+0
;lab9.c,21 :: 		}
	RETURN
; end of _init_cm

_stare:
	CLRF       stare_raspuns_L0+0
;lab9.c,23 :: 		short stare( short comparator)
;lab9.c,26 :: 		if(comparator)        //comparator=1=>c2out si =0 =>c1out
	MOVF       FARG_stare_comparator+0, 0
	BTFSC      STATUS+0, 2
	GOTO       L_stare0
;lab9.c,28 :: 		raspuns=CMCON0.C2OUT;   //1 sau 2 in functie de ce comparator alegem
	MOVLW      0
	BTFSC      CMCON0+0, 7
	MOVLW      1
	MOVWF      stare_raspuns_L0+0
;lab9.c,29 :: 		}
	GOTO       L_stare1
L_stare0:
;lab9.c,32 :: 		raspuns=CMCON0.C1OUT;
	MOVLW      0
	BTFSC      CMCON0+0, 6
	MOVLW      1
	MOVWF      stare_raspuns_L0+0
;lab9.c,33 :: 		}
L_stare1:
;lab9.c,34 :: 		return raspuns;
	MOVF       stare_raspuns_L0+0, 0
	MOVWF      R0+0
;lab9.c,35 :: 		}
	RETURN
; end of _stare

_main:
;lab9.c,37 :: 		void main() {
;lab9.c,38 :: 		init_cm();
	CALL       _init_cm+0
;lab9.c,39 :: 		while(1)
L_main2:
;lab9.c,41 :: 		PORTB.RB1=stare(false);
	CLRF       FARG_stare_comparator+0
	CALL       _stare+0
	BTFSC      R0+0, 0
	GOTO       L__main5
	BCF        PORTB+0, 1
	GOTO       L__main6
L__main5:
	BSF        PORTB+0, 1
L__main6:
;lab9.c,42 :: 		delay_ms(1000);
	MOVLW      11
	MOVWF      R11+0
	MOVLW      38
	MOVWF      R12+0
	MOVLW      93
	MOVWF      R13+0
L_main4:
	DECFSZ     R13+0, 1
	GOTO       L_main4
	DECFSZ     R12+0, 1
	GOTO       L_main4
	DECFSZ     R11+0, 1
	GOTO       L_main4
	NOP
	NOP
;lab9.c,43 :: 		}
	GOTO       L_main2
;lab9.c,45 :: 		}
	GOTO       $+0
; end of _main
