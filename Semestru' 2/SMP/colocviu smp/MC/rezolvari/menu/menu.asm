
_main:

;menu.c,22 :: 		void main() {
;menu.c,26 :: 		a=0;b=1;c=0;d=0;e=0;
	CLRF       main_a_L0+0
	MOVLW      1
	MOVWF      main_b_L0+0
	CLRF       main_c_L0+0
	CLRF       main_d_L0+0
	CLRF       main_e_L0+0
;menu.c,29 :: 		TRISD=0b00011111;
	MOVLW      31
	MOVWF      TRISD+0
;menu.c,30 :: 		TRISA=0;
	CLRF       TRISA+0
;menu.c,31 :: 		TRISA=0;
	CLRF       TRISA+0
;menu.c,32 :: 		TRISB=0;
	CLRF       TRISB+0
;menu.c,33 :: 		Lcd_Init();
	CALL       _Lcd_Init+0
;menu.c,34 :: 		Lcd_Cmd(_LCD_CURSOR_OFF);
	MOVLW      12
	MOVWF      FARG_Lcd_Cmd_out_char+0
	CALL       _Lcd_Cmd+0
;menu.c,35 :: 		Lcd_Cmd(_LCD_CLEAR);
	MOVLW      1
	MOVWF      FARG_Lcd_Cmd_out_char+0
	CALL       _Lcd_Cmd+0
;menu.c,37 :: 		while(1)
L_main0:
;menu.c,39 :: 		if (a==1) { Lcd_Out(1,1,"Setare LED:"); Lcd_Out(2,1,"Ok");}
	MOVF       main_a_L0+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main2
	MOVLW      1
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      1
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      ?lstr1_menu+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
	MOVLW      2
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      1
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      ?lstr2_menu+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
L_main2:
;menu.c,40 :: 		if (b==1) { Lcd_Out(1,1,"Aprindere LED:"); Lcd_Out(2,1,"Ok  Cancel > ");}
	MOVF       main_b_L0+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main3
	MOVLW      1
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      1
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      ?lstr3_menu+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
	MOVLW      2
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      1
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      ?lstr4_menu+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
L_main3:
;menu.c,41 :: 		if (c==1) { Lcd_Out(1,1,"Stingere LED:"); Lcd_Out(2,1,"Ok  Cancel <");}
	MOVF       main_c_L0+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main4
	MOVLW      1
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      1
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      ?lstr5_menu+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
	MOVLW      2
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      1
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      ?lstr6_menu+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
L_main4:
;menu.c,42 :: 		if (d==1) { Lcd_Out(1,1,"LED Aprins"); Lcd_Out(2,1,"Ok"); PORTA=0b000000001;; }
	MOVF       main_d_L0+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main5
	MOVLW      1
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      1
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      ?lstr7_menu+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
	MOVLW      2
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      1
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      ?lstr8_menu+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
	MOVLW      1
	MOVWF      PORTA+0
L_main5:
;menu.c,43 :: 		if (e==1) { Lcd_Out(1,1,"LED Stins"); Lcd_Out(2,1,"Ok"); PORTA=0b00000000; }
	MOVF       main_e_L0+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main6
	MOVLW      1
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      1
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      ?lstr9_menu+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
	MOVLW      2
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      1
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      ?lstr10_menu+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
	CLRF       PORTA+0
L_main6:
;menu.c,47 :: 		if ((a==1) && (RD1_bit))   {a=0;b=1;c=0;d=0;e=0;  }
	MOVF       main_a_L0+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main9
	BTFSS      RD1_bit+0, 1
	GOTO       L_main9
L__main38:
	CLRF       main_a_L0+0
	MOVLW      1
	MOVWF      main_b_L0+0
	CLRF       main_c_L0+0
	CLRF       main_d_L0+0
	CLRF       main_e_L0+0
L_main9:
;menu.c,48 :: 		if ((b==1) && (PORTD.RD1==1)){a=0;b=0;c=0;d=1;e=0;}
	MOVF       main_b_L0+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main12
	BTFSS      PORTD+0, 1
	GOTO       L_main12
L__main37:
	CLRF       main_a_L0+0
	CLRF       main_b_L0+0
	CLRF       main_c_L0+0
	MOVLW      1
	MOVWF      main_d_L0+0
	CLRF       main_e_L0+0
L_main12:
;menu.c,49 :: 		if ((b==1) && (RD2_bit))   {a=1;b=0;c=0;d=0;e=0;}
	MOVF       main_b_L0+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main15
	BTFSS      RD2_bit+0, 2
	GOTO       L_main15
L__main36:
	MOVLW      1
	MOVWF      main_a_L0+0
	CLRF       main_b_L0+0
	CLRF       main_c_L0+0
	CLRF       main_d_L0+0
	CLRF       main_e_L0+0
L_main15:
;menu.c,50 :: 		if ((b==1) && (RD3_bit))   {a=0;b=0;c=1;d=0;e=0;}
	MOVF       main_b_L0+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main18
	BTFSS      RD3_bit+0, 3
	GOTO       L_main18
L__main35:
	CLRF       main_a_L0+0
	CLRF       main_b_L0+0
	MOVLW      1
	MOVWF      main_c_L0+0
	CLRF       main_d_L0+0
	CLRF       main_e_L0+0
L_main18:
;menu.c,51 :: 		if ((c==1) && (RD1_bit))   {a=0;b=0;c=0;d=0;e=1;}
	MOVF       main_c_L0+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main21
	BTFSS      RD1_bit+0, 1
	GOTO       L_main21
L__main34:
	CLRF       main_a_L0+0
	CLRF       main_b_L0+0
	CLRF       main_c_L0+0
	CLRF       main_d_L0+0
	MOVLW      1
	MOVWF      main_e_L0+0
L_main21:
;menu.c,52 :: 		if ((c==1) && (RD2_bit))   {a=1;b=0;c=0;d=0;e=0; }
	MOVF       main_c_L0+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main24
	BTFSS      RD2_bit+0, 2
	GOTO       L_main24
L__main33:
	MOVLW      1
	MOVWF      main_a_L0+0
	CLRF       main_b_L0+0
	CLRF       main_c_L0+0
	CLRF       main_d_L0+0
	CLRF       main_e_L0+0
L_main24:
;menu.c,53 :: 		if ((d==1) && (RD1_bit))   {a=1;b=0;c=0;d=0;e=0;  }
	MOVF       main_d_L0+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main27
	BTFSS      RD1_bit+0, 1
	GOTO       L_main27
L__main32:
	MOVLW      1
	MOVWF      main_a_L0+0
	CLRF       main_b_L0+0
	CLRF       main_c_L0+0
	CLRF       main_d_L0+0
	CLRF       main_e_L0+0
L_main27:
;menu.c,54 :: 		if ((e==1) && (RD1_bit))   {a=1;b=0;c=0;d=0;e=0;}
	MOVF       main_e_L0+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main30
	BTFSS      RD1_bit+0, 1
	GOTO       L_main30
L__main31:
	MOVLW      1
	MOVWF      main_a_L0+0
	CLRF       main_b_L0+0
	CLRF       main_c_L0+0
	CLRF       main_d_L0+0
	CLRF       main_e_L0+0
L_main30:
;menu.c,59 :: 		}
	GOTO       L_main0
;menu.c,60 :: 		}
	GOTO       $+0
; end of _main
