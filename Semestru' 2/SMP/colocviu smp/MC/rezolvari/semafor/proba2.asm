
_interrupt:
	MOVWF      R15+0
	SWAPF      STATUS+0, 0
	CLRF       STATUS+0
	MOVWF      ___saveSTATUS+0
	MOVF       PCLATH+0, 0
	MOVWF      ___savePCLATH+0
	CLRF       PCLATH+0
;proba2.c,22 :: 		void interrupt()
;proba2.c,24 :: 		INTCON.GIE=0; //disables all interrupt
	BCF        INTCON+0, 7
;proba2.c,26 :: 		if (INTCON.INTF) //external interrupt occured
	BTFSS      INTCON+0, 1
	GOTO       L_interrupt0
;proba2.c,28 :: 		buttonPressed=1;
	MOVLW      1
	MOVWF      _buttonPressed+0
;proba2.c,31 :: 		INTCON.INTF=0;//external interrupt did not occur
	BCF        INTCON+0, 1
;proba2.c,32 :: 		}
L_interrupt0:
;proba2.c,33 :: 		INTCON.GIE=1;
	BSF        INTCON+0, 7
;proba2.c,34 :: 		}
L__interrupt15:
	MOVF       ___savePCLATH+0, 0
	MOVWF      PCLATH+0
	SWAPF      ___saveSTATUS+0, 0
	MOVWF      STATUS+0
	SWAPF      R15+0, 1
	SWAPF      R15+0, 0
	RETFIE
; end of _interrupt

_main:
;proba2.c,38 :: 		void main() {
;proba2.c,39 :: 		Lcd_init();
	CALL       _Lcd_Init+0
;proba2.c,40 :: 		Lcd_Cmd(_LCD_CLEAR);
	MOVLW      1
	MOVWF      FARG_Lcd_Cmd_out_char+0
	CALL       _Lcd_Cmd+0
;proba2.c,41 :: 		Lcd_Cmd(_LCD_CURSOR_OFF);
	MOVLW      12
	MOVWF      FARG_Lcd_Cmd_out_char+0
	CALL       _Lcd_Cmd+0
;proba2.c,43 :: 		Lcd_out(1,1,"Asteptati culoarea");
	MOVLW      1
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      1
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      ?lstr1_proba2+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
;proba2.c,44 :: 		Lcd_out(2,2,"verde a semaforului!");
	MOVLW      2
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      2
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      ?lstr2_proba2+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
;proba2.c,46 :: 		TRISD=0;
	CLRF       TRISD+0
;proba2.c,47 :: 		TRISA=0;
	CLRF       TRISA+0
;proba2.c,48 :: 		TRISC=0;
	CLRF       TRISC+0
;proba2.c,49 :: 		TRISB = 0xff;
	MOVLW      255
	MOVWF      TRISB+0
;proba2.c,51 :: 		INTCON.GIE = 1 ;
	BSF        INTCON+0, 7
;proba2.c,52 :: 		INTCON.INTE = 1;
	BSF        INTCON+0, 4
;proba2.c,54 :: 		while(1){
L_main1:
;proba2.c,55 :: 		TRISD=0;
	CLRF       TRISD+0
;proba2.c,56 :: 		PORTD=0b00100001;
	MOVLW      33
	MOVWF      PORTD+0
;proba2.c,57 :: 		Delay_ms(3000);
	MOVLW      31
	MOVWF      R11+0
	MOVLW      113
	MOVWF      R12+0
	MOVLW      30
	MOVWF      R13+0
L_main3:
	DECFSZ     R13+0, 1
	GOTO       L_main3
	DECFSZ     R12+0, 1
	GOTO       L_main3
	DECFSZ     R11+0, 1
	GOTO       L_main3
	NOP
;proba2.c,58 :: 		PORTD=0b00010001;
	MOVLW      17
	MOVWF      PORTD+0
;proba2.c,59 :: 		Delay_ms(4000);
	MOVLW      41
	MOVWF      R11+0
	MOVLW      150
	MOVWF      R12+0
	MOVLW      127
	MOVWF      R13+0
L_main4:
	DECFSZ     R13+0, 1
	GOTO       L_main4
	DECFSZ     R12+0, 1
	GOTO       L_main4
	DECFSZ     R11+0, 1
	GOTO       L_main4
;proba2.c,60 :: 		PORTD=0b00001100;
	MOVLW      12
	MOVWF      PORTD+0
;proba2.c,62 :: 		for(i=9;i>=0;i--)
	MOVLW      9
	MOVWF      _i+0
	MOVLW      0
	MOVWF      _i+1
L_main5:
	MOVLW      128
	XORWF      _i+1, 0
	MOVWF      R0+0
	MOVLW      128
	SUBWF      R0+0, 0
	BTFSS      STATUS+0, 2
	GOTO       L__main16
	MOVLW      0
	SUBWF      _i+0, 0
L__main16:
	BTFSS      STATUS+0, 0
	GOTO       L_main6
;proba2.c,64 :: 		Lcd_Cmd(_LCD_CLEAR);
	MOVLW      1
	MOVWF      FARG_Lcd_Cmd_out_char+0
	CALL       _Lcd_Cmd+0
;proba2.c,65 :: 		Lcd_out(1,1,"Puteti traversa!");
	MOVLW      1
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      1
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      ?lstr3_proba2+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
;proba2.c,66 :: 		PORTA=i;
	MOVF       _i+0, 0
	MOVWF      PORTA+0
;proba2.c,67 :: 		Delay_ms(1000);
	MOVLW      11
	MOVWF      R11+0
	MOVLW      38
	MOVWF      R12+0
	MOVLW      93
	MOVWF      R13+0
L_main8:
	DECFSZ     R13+0, 1
	GOTO       L_main8
	DECFSZ     R12+0, 1
	GOTO       L_main8
	DECFSZ     R11+0, 1
	GOTO       L_main8
	NOP
	NOP
;proba2.c,62 :: 		for(i=9;i>=0;i--)
	MOVLW      1
	SUBWF      _i+0, 1
	BTFSS      STATUS+0, 0
	DECF       _i+1, 1
;proba2.c,69 :: 		}
	GOTO       L_main5
L_main6:
;proba2.c,70 :: 		Lcd_Cmd(_LCD_CLEAR);
	MOVLW      1
	MOVWF      FARG_Lcd_Cmd_out_char+0
	CALL       _Lcd_Cmd+0
;proba2.c,71 :: 		Lcd_out(1,1,"Asteptati culoarea");
	MOVLW      1
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      1
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      ?lstr4_proba2+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
;proba2.c,72 :: 		Lcd_out(2,2,"verde a semaforului!");
	MOVLW      2
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      2
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      ?lstr5_proba2+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
;proba2.c,73 :: 		}
	GOTO       L_main1
;proba2.c,90 :: 		}
	GOTO       $+0
; end of _main
