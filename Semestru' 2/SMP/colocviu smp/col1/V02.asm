
_INTERRUPT:
	MOVWF      R15+0
	SWAPF      STATUS+0, 0
	CLRF       STATUS+0
	MOVWF      ___saveSTATUS+0
	MOVF       PCLATH+0, 0
	MOVWF      ___savePCLATH+0
	CLRF       PCLATH+0

;V02.c,3 :: 		void INTERRUPT()
;V02.c,5 :: 		INTCON.GIE=0;
	BCF        INTCON+0, 7
;V02.c,6 :: 		if (INTCON.INTF)
	BTFSS      INTCON+0, 1
	GOTO       L_INTERRUPT0
;V02.c,8 :: 		DP=1;
	MOVLW      1
	MOVWF      _DP+0
	MOVLW      0
	MOVWF      _DP+1
;V02.c,9 :: 		INTCON.INTF=0;
	BCF        INTCON+0, 1
;V02.c,10 :: 		}
L_INTERRUPT0:
;V02.c,11 :: 		INTCON.GIE=1;
	BSF        INTCON+0, 7
;V02.c,12 :: 		}
L_end_INTERRUPT:
L__INTERRUPT13:
	MOVF       ___savePCLATH+0, 0
	MOVWF      PCLATH+0
	SWAPF      ___saveSTATUS+0, 0
	MOVWF      STATUS+0
	SWAPF      R15+0, 1
	SWAPF      R15+0, 0
	RETFIE
; end of _INTERRUPT

_main:

;V02.c,14 :: 		void main() {
;V02.c,15 :: 		INTCON=0B10010000;
	MOVLW      144
	MOVWF      INTCON+0
;V02.c,16 :: 		TRISD=0;
	CLRF       TRISD+0
;V02.c,17 :: 		TRISC=0;
	CLRF       TRISC+0
;V02.c,18 :: 		while(1)
L_main1:
;V02.c,20 :: 		if(DP==0)
	MOVLW      0
	XORWF      _DP+1, 0
	BTFSS      STATUS+0, 2
	GOTO       L__main15
	MOVLW      0
	XORWF      _DP+0, 0
L__main15:
	BTFSS      STATUS+0, 2
	GOTO       L_main3
;V02.c,22 :: 		I=1;
	MOVLW      1
	MOVWF      _I+0
	MOVLW      0
	MOVWF      _I+1
;V02.c,23 :: 		PORTD.RD0=1;
	BSF        PORTD+0, 0
;V02.c,24 :: 		PORTD.RD1=0;
	BCF        PORTD+0, 1
;V02.c,25 :: 		PORTD.RD2=0;
	BCF        PORTD+0, 2
;V02.c,26 :: 		PORTD.RD3=0;
	BCF        PORTD+0, 3
;V02.c,27 :: 		PORTD.RD4=0;
	BCF        PORTD+0, 4
;V02.c,28 :: 		PORTD.RD5=1;
	BSF        PORTD+0, 5
;V02.c,29 :: 		PORTC=SIR[0];
	MOVF       _SIR+0, 0
	MOVWF      PORTC+0
;V02.c,30 :: 		}
L_main3:
;V02.c,31 :: 		if(DP==1)
	MOVLW      0
	XORWF      _DP+1, 0
	BTFSS      STATUS+0, 2
	GOTO       L__main16
	MOVLW      1
	XORWF      _DP+0, 0
L__main16:
	BTFSS      STATUS+0, 2
	GOTO       L_main4
;V02.c,33 :: 		DELAY_MS(400);
	MOVLW      5
	MOVWF      R11+0
	MOVLW      15
	MOVWF      R12+0
	MOVLW      241
	MOVWF      R13+0
L_main5:
	DECFSZ     R13+0, 1
	GOTO       L_main5
	DECFSZ     R12+0, 1
	GOTO       L_main5
	DECFSZ     R11+0, 1
	GOTO       L_main5
;V02.c,34 :: 		PORTD.RD0=1;
	BSF        PORTD+0, 0
;V02.c,35 :: 		PORTD.RD1=0;
	BCF        PORTD+0, 1
;V02.c,36 :: 		PORTD.RD2=0;
	BCF        PORTD+0, 2
;V02.c,37 :: 		PORTD.RD3=0;
	BCF        PORTD+0, 3
;V02.c,38 :: 		PORTD.RD4=1;
	BSF        PORTD+0, 4
;V02.c,39 :: 		PORTD.RD5=0;
	BCF        PORTD+0, 5
;V02.c,41 :: 		DELAY_MS(400);
	MOVLW      5
	MOVWF      R11+0
	MOVLW      15
	MOVWF      R12+0
	MOVLW      241
	MOVWF      R13+0
L_main6:
	DECFSZ     R13+0, 1
	GOTO       L_main6
	DECFSZ     R12+0, 1
	GOTO       L_main6
	DECFSZ     R11+0, 1
	GOTO       L_main6
;V02.c,42 :: 		PORTD.RD0=0;
	BCF        PORTD+0, 0
;V02.c,43 :: 		PORTD.RD1=0;
	BCF        PORTD+0, 1
;V02.c,44 :: 		PORTD.RD2=1;
	BSF        PORTD+0, 2
;V02.c,45 :: 		PORTD.RD3=1;
	BSF        PORTD+0, 3
;V02.c,46 :: 		PORTD.RD4=0;
	BCF        PORTD+0, 4
;V02.c,47 :: 		PORTD.RD5=0;
	BCF        PORTD+0, 5
;V02.c,48 :: 		for (I=1;I<11;I++)
	MOVLW      1
	MOVWF      _I+0
	MOVLW      0
	MOVWF      _I+1
L_main7:
	MOVLW      128
	XORWF      _I+1, 0
	MOVWF      R0+0
	MOVLW      128
	SUBWF      R0+0, 0
	BTFSS      STATUS+0, 2
	GOTO       L__main17
	MOVLW      11
	SUBWF      _I+0, 0
L__main17:
	BTFSC      STATUS+0, 0
	GOTO       L_main8
;V02.c,51 :: 		PORTC=SIR[I];
	MOVF       _I+0, 0
	MOVWF      R0+0
	MOVF       _I+1, 0
	MOVWF      R0+1
	RLF        R0+0, 1
	RLF        R0+1, 1
	BCF        R0+0, 0
	MOVF       R0+0, 0
	ADDLW      _SIR+0
	MOVWF      FSR
	MOVF       INDF+0, 0
	MOVWF      PORTC+0
;V02.c,52 :: 		DELAY_MS(400);
	MOVLW      5
	MOVWF      R11+0
	MOVLW      15
	MOVWF      R12+0
	MOVLW      241
	MOVWF      R13+0
L_main10:
	DECFSZ     R13+0, 1
	GOTO       L_main10
	DECFSZ     R12+0, 1
	GOTO       L_main10
	DECFSZ     R11+0, 1
	GOTO       L_main10
;V02.c,53 :: 		SEC=1;
	MOVLW      1
	MOVWF      _SEC+0
	MOVLW      0
	MOVWF      _SEC+1
;V02.c,48 :: 		for (I=1;I<11;I++)
	INCF       _I+0, 1
	BTFSC      STATUS+0, 2
	INCF       _I+1, 1
;V02.c,55 :: 		}
	GOTO       L_main7
L_main8:
;V02.c,56 :: 		if (I==11)
	MOVLW      0
	XORWF      _I+1, 0
	BTFSS      STATUS+0, 2
	GOTO       L__main18
	MOVLW      11
	XORWF      _I+0, 0
L__main18:
	BTFSS      STATUS+0, 2
	GOTO       L_main11
;V02.c,58 :: 		DP=0;
	CLRF       _DP+0
	CLRF       _DP+1
;V02.c,59 :: 		SEC=0;
	CLRF       _SEC+0
	CLRF       _SEC+1
;V02.c,60 :: 		}
L_main11:
;V02.c,61 :: 		}
L_main4:
;V02.c,62 :: 		}
	GOTO       L_main1
;V02.c,68 :: 		}
L_end_main:
	GOTO       $+0
; end of _main
