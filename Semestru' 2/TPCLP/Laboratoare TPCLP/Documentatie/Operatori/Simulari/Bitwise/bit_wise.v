module bit_wise; //autor: svh, Mai 2010

reg [3:0] a, b ,c; // variabile vector de 4 biti, tip registru
real rel_1=12.5; // variabila tip real de 32 biti, atribuita cu constanta 12.5
real rel_2=12.499;
 
 initial 
   begin
     a = 4'b1100; //constanta numerica binara, de 4 biti 
     b = 4'b0011;
     c = 4'b0101;

     $display ("Operatori bitweise. Actioneaza intre bitii de acelasi rang ai doi operanzi.");
     $display ("1a. ~a   = ~ 1100 =%b ", ~a);   // operator bitwise, negatie, evaluat cu valoarea: 0011
     $display ("1b. ~a   = ~ 1100 =%d ", ~a);  // operator bitwise, negatie, evaluat la: 3
     $display ("1c. &a   = & 1100 =%b ", &a);  // rezultatul este 0
     $display ("1d. |a   =  | 1100 =%b ", |a);  // rezultatul este 1
     $display ("1e.^a   = ^ 1100 =%b ", ^a);  // rezultatul este 0
     $display ();
     $display ("2a. a&c  = (1100 & 0101) =%b ",  (a & c) );  // evaluat: 0100
     $display ("2b. a&c  = (1100 & 0101) =%d ",  (a & c) );  // evaluat: 4
        $display();
     $display ("3a. a|b  = (1100 | 0011) =%b ",  (a | b) );  // evaluat: 1111
     $display ("3b. a|b  = (1100 | 0011) =%d ",  (a | b) );  // evaluat: 15
     $display ();
     $display ("4a. b^c  = (0011 ^ 0101) =%b ",  (b ^ c) );  // evaluat: 0110
     $display ("4b. b^c  = (0011 ^ 0101) =%d ",  (b ^ c) );  // evaluat: 6
        $display();
     $display ("5a. (in binar = %b,  in zecimal = %d",(4'b0010 << 1),(4'b0010 << 1)); //evaluare: (binar)=0100,(zecimal)=4
     $display ("5b. (in binar = %b,  in zecimal = %d",(4'b0011 << 1),(4'b0011 << 1)); //evaluare: (binar)=0110,(zecimal)=6
        $display();
     $display ("6a.  4'b11xz & 4'bxz00 =%b ",  (4'b11xz & 4'bxz00) ); // evaluat: xx00
     $display ("6b.  4'b11xz & 4'b1z00 =%b ",  (4'b11xz & 4'b1z00) ); // evaluat: 1x00
     $display ();
     $display ("7a.  4'b11xz | 4'b0011 =%b ",  (4'b11xz | 4'b0011) ); // evaluat: 111x
     $display ("7b.  4'b11xz | 4'b0000 =%b ",  (4'b11xz | 4'b0000) ); // evaluat: 11x1
        $display();
     $display ("8a.  4'b1100 | 'd3 =%b",      (4'b1100 | 'd3) ); // evaluat: 0...01111 (32 cifre)
     $display ("8b.  4'b1100 | 'd3 =%0b",     (4'b1100 | 'd3) ); // evaluat: 1111
     $display ("8c.  4'b1100 | 'd3 =%d",      (4'b1100 | 'd3) ); // evaluat: 15
        $display();
     $display ("9a.  4'b0100 | 'd11 =%b ",    (4'b0100 | 'd11) );  // evaluat: 0...01111 (32 cifre)
     $display ("9b.  4'b0100 | 'd11 =%0b ",   (4'b0100 | 'd11) );  // evaluat: 1111
     $display ("9c.  4'b0100 | 2'd11 =%b ",   (4'b0100 | 2'd11) ); // evaluat: 0111
     $display ("9d.  4'b0100 | 2'd11 =%0b ",  (4'b0100 | 2'd11) ); // evaluat: 111
        $display();
     $display ("10a.  4'b11xz ^ 4'b0011 =%b ", (4'b11xz ^ 4'b0011) );  // evaluat: 11xx
     $display ("10b.  4'b11xz ^ 4'b1100 =%b ", (4'b11xz ^ 4'b1100) );  // evaluat: 00xx
        $display();
     $display ("11a. (2'bxz11 = %b",  (2'bxz11) ); // evaluat: 11 
     $display ("11b. (3'bxz11 = %b",  (3'bxz11) ); // evaluat: z11
     $display ("11c. (4'bxz11 = %b",  (4'bxz11) ); // evaluat: xz11
        $display();
     $display ("12a.  2'd11 = %b", (2'd11) ); // evaluat: 11 (binar; s-au luat ultimile 2 cifre din 1011 bin = 11 dec)
     $display ("12b.  2'd11 = %d", (2'd11) ); // evaluat: 3 zecimal (1011 binar,apoi trunchiat si citit in zecimal
        $display();      
     $display ("13. ~(01) =%b",    ~(2'b01) ); // evaluat: 10 (negata operandului 01 binar)
     $display ("14. (4'b1011 ^ 4'b10) = %b",  (4'b1011 ^ 4'b10) ); // evaluat: 1001 (deoarece 1011^0010=1001)
     $display();      
     $display ("15. rel_1=12.5   [%%0d]= %0d,  [%%0b]= %0b,  [%%d]= %d,  [%%b]= %b", rel_1,rel_1,rel_1,rel_1); // evaluat:13, 1101. 
     $display ("15. rel_2=12.499 [%%0d]= %0d  [%%0b]= %0b",   rel_2,rel_2 ); // evaluat: // evaluat:13, 1101.
     
   end
endmodule

