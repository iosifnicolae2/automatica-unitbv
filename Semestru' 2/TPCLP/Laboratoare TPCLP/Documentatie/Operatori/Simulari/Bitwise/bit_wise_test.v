module bit_wise_test;   //autor: svh, Mai 2010

  reg [3:0] a, b ,c;

  initial 
   begin
    a = 4'b1100;
    b = 4'b0011;
    c = 4'b0101;

  // Operatori logici pe vectori (bitwise operations). 
  // Opereaza �ntre bitii de acelasi rang ai doi operanzi. Intorc drept rezultat un operand vector.
     
     $display ("1. ~a=~1100  [%%b]= %b", ~a);  // se complementeaza bitii lui a. Rezultat: 0011
     $display ("2. ~a=~1100  [%%d]= %d", ~a);  // idem. Rezultat: 3 decimal
     $display ();
     $display ("3.  &a = &(1100)= ",  (&a) );  // operator AND de reducere aplicat vectorului a. Rezultat: 0
     $display ("4. a&c = 1100 & 0101= ",  (a&c) );  // bitwise AND. Rezultat: (zecimal,implicit)= 4 (lipsa format afisare)
     $displayb("5. a&c = 1100 & 0101= ",  (a&c) );  // bitwise AND. Rezultat: (binar)= 0100 (vezi $displayb)
     $display ("6. a|b = 1100 | 0011= ",  (a|b) );  // bitwise OR.  Rezultat: (zecimal,implicit)= 15 (lipsa format afisare)
     $displayb("7. a|b = 1100 | 0011= ",  (a|b) );  // bitwise OR.  Rezultat: (binar)= 1111 
     $display ("8. b^c = 0011 ^ 0101= ",  (b^c) );  // bitwise XOR. Rezultat: (zecimal,implicit)= 6 (lipsa format afisare)
     $displayb("9. b^c = 0011 ^ 0101= ",  (b^c) );  // bitwise XOR. Rezultat: (binar)= 0110 (vezi $displayb)
     $display ("10. 4'b0010<<1= [%%b]= %b,  [%%d]= %d",(4'b0010 << 1),(4'b0010 << 1)); //Rezultat: (binar)= 0100,(zecimal)= 4
     $display ("11. 4'b0011<<1= [%%b]= %b,  [%%d]= %d",(4'b0011 << 1),(4'b0011 << 1)); //Rezultat: (binar)= 0110,(zecimal)= 6
     $display ();
     $display ("12a.  4'b11xz & 4'bxz00= [%%b]=%b,  [%%d]=%d", (4'b11xz & 4'bxz00), (4'b11xz & 4'bxz00));//bitwise AND.Rezultat:(binar)=xx00,(zecimal)=X
     $display ("12b.  4'b11xz & 4'bxz00= [%%b]=%b,  ", (4'b11xz & 4'bxz00), (4'b11xz & 4'bxz00));//bitwise AND.Rezultat: (binar)=xx00,(zecimal)=X
     $display ();
     $display ("13.  4'b11xz | 4'b1100= [%%b]=%b ", (4'b11xz)|(4'b1100) ); // bitwise AND. Rezultat: (binar)= 11xx
     $display ("14.  4'b11xz | 4'b1100= [%%d]=%d ", (4'b11xz)|(4'b1100) ); // bitwise AND. Rezultat: (zecimal)= X
     $display ();
     $display ("15.  4'b11xz | 'd3= [%%b]= %b", (4'b11xz)|('d3) ); // bitwise AND. Rezultat: 0...0_1111 (32 cifre binare)
     $display ("16.  4'b11xz | 'd3= [%%0b]= %0b", (4'b11xz)|('d3) ); // bitwise AND. Rezultat: 1111 (completarile cu 0 se ignora)
     $display ();
     $display ("17.  4'b11xz | 11= [%%d]= %d",   (4'b11xz)|(11) ); // bitwise AND. Rezultat: (zecimal)= 15
     $display ("18.  4'b11xz | 11= [%%b]= %b",   (4'b11xz)|(11) ); // bitwise AND. Rezultat: 0...01111 (32 cifre binare)
     $display ("19.  4'b11xz | 11= [%%0b]= %0b ",(4'b11xz)|(11) ); // bitwise AND. Rezultat: (binar)= 1111 (completarile cu 0 se ignora)
     $display ();
     $display ("20.  4'b11xz ^ 11= [%%b]=  %b",  (4'b11xz)^(11) ); // bitwise XOR. Rezultat: 0...0_01xx (32 cifre binare)
     $display ("21.  4'b11xz ^ 11= [%%0b]= %0b", (4'b11xz)^(11) ); // bitwise XOR. Rezultat: (binar)= 1xx (completarile cu 0 se ignora)
     $display ("22.  4'b11xz ^ 11= [%%d]=  %d",  (4'b11xz)^(11) ); // bitwise XOR. Rezultat: (zecimal)=      X
     $display ("23.  4'b11xz ^ 11= [%%0d]= %0d", (4'b11xz)^(11) ); // bitwise XOR. Rezultat: (zecimal)=  X
     $display ("24.  4'b11xz ^ 'd11= [%%b]=  %b",  (4'b11xz)^('d11) ); // bitwise XOR. Rezultat: 0...0_01xx (32 cifre binare) 
     $display ("25.  4'b11xz ^ 'd11= [%%0b]= %0b", (4'b11xz)^('d11) ); // bitwise XOR. Rezultat: 1xx (completarile cu 0 se ignora)
     $display ();
     $display ("26.  'd11 = ",          ('d11) ); // Rezultat: (zecimal)= 11 (lipsa format afisare)
     $display ("27.  'd11 = [%%d]= %d", ('d11) ); // Rezultat: (zecimal)= 11
     $display ("28.  'd11 = [%%b]= %b",       ('d11) ); // Rezultat: 0..01011 (32 cifre binare)
     $display ("29.  'd11 = [%%0b]= %0b ",    ('d11) ); // Rezultat: 1011 (binar) (completarile cu 0 se ignora)
     $display ();
     $display ("30. ~(2'b01)= [%%b]=  %b", ~(2'b01) ); // bitwise negatie. Rezultat: 10
     $display ("31. ~(2'b01)= [%%d]=  %d", ~(2'b01) ); // bitwise negatie. Rezultat: 2
     $display ("32.  4'b1011 ^ 4'b10= [%%b]= %b", (4'b1011)^(4'b10) ); // bitwise XOR. Rezultat: (binar)= 1001
     $display ("33.  4'bxzxx | 3'b101= [%%b]= %b  ", (4'bxzxx)|(3'b101) ); // bitwise OR. Rezultat: (binar)= x1x1
     $display ("34.  4'bxzxx | 3'b101= [%%d]= %d", (4'bxzxx)|(3'b101) ); // bitwise OR. Rezultat: (zecimal)= X
   end
endmodule
