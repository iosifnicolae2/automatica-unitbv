module equality_operators();
 //autor: svh, Mai 2010 
 reg [11:0] A = 8'd511;
 real U=12.625;
 realtime V=15.5;
  
  initial
    begin
           // Case Equality
      $display ("Case Equality");
      $display ("    Expresii cu operator              Rezultat logic");
      $display ("1. 4'bX001  ===  4'bX001   ==>     %b", (4'bx001 === 4'bx001));
      $display ("2. 4'bX001  ===  4'bXX01   ==>     %b", (4'bx001 === 4'bxx01));
      $display ("3. 4'bZ0X1  ===  4'bZ0X1   ==>     %b", (4'bz0x1 === 4'bz0x1));
      $display ("4. 4'bZ0X1  ===  4'bZ001   ==>     %b", (4'bz0x1 === 4'bz001));
      $display ("5. 3'bX01   ===  4'b0X01   ==>     %b", (3'bx01 === 4'b0x01));
      $display ("6. 3'bX01   ===  4'b1X01   ==>     %b", (3'bx01 === 4'b1x01));
      $display ("7. 3'b111   ===   'd111     ==>     %b ", (3'b111 === 'd111));
      $display ("8. 3'b111   ===   'd7        ==>     %b ", (3'b111 === 'd7));
      $display ();
     
       // Case Inequality
      $display ("Case Inequality");
      $display ("    Expresii cu operator              Rezultat logic");
      $display ("1. 4'bX001  !==   4'bX001   ==>     %b", (4'bx001 !== 4'bx001));
      $display ("2. 4'bX001  !==   4'bXX01   ==>     %b", (4'bx001 !== 4'bxx01));
      $display ("3. 4'bZ0X1  !==   4'bZ0X1   ==>     %b", (4'bz0x1 !== 4'bz0x1));
      $display ("4. 4'bZ0X1  !==   4'bZ001   ==>     %b", (4'bz0x1 !== 4'bz001));
      $display (); 
    
      // Logical Equality
      $display ("Logical Equality");
      $display ("    Expresii cu operator                Rezultat logic");
      $display ("00.   A =8'd511, A=[%%b]==> %b, A=[%%d]==> %d", A, A);
      $display ("00.   8'd511==8'd255, ", 8'd511==8'd255);
      $display("U=12.625 =%d =%b",U,U);
      $display("V=15.5     =%d =%b",V,V);
      $display ("01.            5  ==  5           [%%b]==>     %b", (5 == 5));   // = 1
      $display ("02.           5  == 10          [%%b]==>     %b", (5 == 10));    // = 0
      $display ("03.         10d  ==  Ah       [%%b]==>     %b", ('d10 == 'ha));  // = 1
      $display ("04.         15d  ==  Fh       [%%b]==>     %b", ('d15 == 'hf));  // = 1
      $display ("05.      2'b10  ==  2'b10  [%%b]==>      %b", (2'b10 == 2'b10)); // = 1
      $display ("06.      2'b1X  ==  2'b1X  [%%b]==>      %b", (2'b1x == 2'b1x)); // = X
      $display ("07.      2'b1Z  ==  2'b1Z  [%%b]==>      %b", (2'b1z == 2'b1z)); // = X
      $display ("08.      2'b1Z  ==  2'b1X  [%%b]==>      %b", (2'b1z == 2'b1x)); // = X
      $display ("09.      2'b1Z  ==  2'b11  [%%b]==>      %b", (2'b1z == 2'b11)); // = X
      $display ("10.      2'b1x  == 1'b1    [%%b]==>      %b", (2'b1x == 1'b1));  // = 0 (11==01 sau 10=01)
      $display ("11.      2'b1x  == 2'b0x   [%%b]==>      %b", (2'b1x == 2'b0x)); // = 0 (11==0x sau 10=0x)
      $display ("12.      2'b1x  == 2'b1x   [%%b]==>      %b", (2'b1x == 2'b1x)); // = X
      $display ("13.      2'b0x  == 1'bx    [%%b]==>      %b", (2'b0x == 1'bx));  // = X
      $display ("14.      2'b0x  == 1'b1    [%%b]==>      %b", (2'b0x == 1'b1));  // = X
      $display ("15.      2'b0x  == 15      [%%b]==>      %b", (2'b0x == 15));    // = 0 ?
      $display ("16.      4'd125 == 4'd13 [%%b]==>  %b", (4'd125 == 4'd13));  // = 1
      $display ();
    
      // Logical Inequality
      $display ("Logical Inequality");
      $display ("    Expresii cu operator              Rezultat logic");
      $display ("1.            5  !=  5               ==>     %b", (5 != 5));
      $display ("2.            5  != 10              ==>     %b", (5 != 10));
      $display ("3.         10d  !=  Ah            ==>     %b", ('d10 != 'ha));
      $display ("4.         15d  !=  Fh            ==>     %b", ('d15 != 'hf));
      $display ("5.      2'b10  !=  2'b10       ==>      %b", (2'b10 != 2'b10));
      $display ("6.      2'b1X  !=  2'b1X       ==>      %b", (2'b1x != 2'b1x));
      $display ("7.      2'b1Z  !=  2'b1Z       ==>      %b", (2'b1z != 2'b1z));
      $display ("8.      2'b1Z  !=  2'b1X       ==>      %b", (2'b1z != 2'b1x));
      $display ("9.      2'b1Z  !=  2'b11       ==>      %b", (2'b1z != 2'b11));
      $display ();

    end
  
endmodule
