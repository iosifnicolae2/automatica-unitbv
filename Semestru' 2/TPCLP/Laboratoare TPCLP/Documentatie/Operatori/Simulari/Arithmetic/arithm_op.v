module arithm_op();
  //Arithmetic operators:
  //  - Binary: +(addition), -(subtraction), *(multiplication), /(divisation), %(the modulus operator)
  //  - Unary: +, - (This is used to specify the sign)
  //  - Integer division truncates any fractional part
  //  - The result of a modulus operation takes the sign of the first operand
  //  - If any operand bit value is the unknown value x, then the entire result value is x
  //  - Register data types are used as unsigned values (Negative numbers are stored in two's complement form)
  //Oeratorii aritmetici -reguli: 
  //  -	Binar: +, -, *, /, % (% este operatorul denumit modulo).
  //  -	Unar: +, -  (acesta este utilizat pentru specificarea semnului).
  //  -	Impartire cu cat intreg. Trunchiaza orice parte fractionara.
  //  -	Rezultatul unei operatii modulo primeste semnul primului operand.
  //  -	Daca oricare dintre operanzi are cel putin un bit de valoare x necunoscuta, atunci rezultatul este �ntotdeauna x.
  //  -	Datele de tip reg (register) sunt utilizate ca valori fara semn,numerele negative fiind stocate �n complement fata de 2.

  initial
    begin
      $display("Operatori aritmetici");
      $display("============");
      $display("5 + 10= %0d", 5+10);
      $display("5 - 10= %0d", 5-10);
      $display("10 - 5= %0d", 10-5);
      $display("10 * 5=   %0d", 5*10);
      $display("10 * 5.5= %0d", 5.5*10);
      $display("1.2 * 2= %0d", 1.2*2);
      $display("1.2 * 4= %0d", 1.2*4);
      $display("10 / 5= %0d", 10/5);
      $display("10 / -5= %0d", 10/-5);
      $display("11 / 2= %0d", 11/2);
      $display("11 / 3= %0d", 11/3);
      $display("11 / 4= %0d", 11/4);
      $display("  9 %% 3= %0d", 9%3);
      $display("10 %% 3= %0d", 10%3);
      $display("18 %% 4= %0d", 18%4);
      $display("19 %% 4= %0d", 19%4);
      $display("-19 %% 4= %0d", -19%4);
      $display("19 %% -4= %0d", 19%-4);
      $display("  5[%%d]= %0d,      5[%%b]= %b", 5,5);
      $display("+5[%%d]= %0d,   +5[%%b]= %b", +5,+5);
      $display("-5[%%d]=  %0d,   -5[%%b]= %b", -5,-5);
      $display();
      #10 $stop;
    end
endmodule
