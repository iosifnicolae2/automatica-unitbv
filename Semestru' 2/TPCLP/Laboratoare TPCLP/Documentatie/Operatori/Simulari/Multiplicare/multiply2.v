module multiply2 (a, b, product1,product2); // se multiplica a cu 4, se imparte b la 2.
   input [4:0] a, b;
   output [4:0] product1,product2;
   wire [4:0] product;
      
   assign a=5'b00101;
   assign b=5'b00100;
   
   initial
    $monitor("a=%b, ax4=%b, b=%b, b:2=%b", a, product1, b, product2);
      
   assign product1 = a << 2; // product1 se obtine prin deplasarea lui a cu 2 pasi la stanga
   assign product2 = b >> 1; // product2 se obtine prin deplasarea lui b cu 1 pas la dreapta    	  	 
 endmodule
