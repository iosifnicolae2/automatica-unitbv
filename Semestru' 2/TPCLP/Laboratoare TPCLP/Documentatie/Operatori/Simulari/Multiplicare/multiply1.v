module multiply1 (); // se multiplica a cu 2 si se obtine product
 
    wire [4:0] product;
    reg [3:0] a;
   
   initial
    begin
      a=3'b001;
      $monitor("Time=%0d, a=%b, a<<1=%b, product=%b", $time, a, a<<1, product);
    end
 
    always
     begin
      #1 a <= 3'b011;
      #2 a <= 3'b111;
     end
               	 
    assign product = a << 1; // product se obtine prin deplasare a un pas la stanga
    	  	 
 endmodule
