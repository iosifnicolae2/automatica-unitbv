library verilog;
use verilog.vl_types.all;
entity muliply2 is
    port(
        a               : in     vl_logic_vector(4 downto 0);
        b               : in     vl_logic_vector(4 downto 0);
        product1        : out    vl_logic_vector(4 downto 0);
        product2        : out    vl_logic_vector(4 downto 0)
    );
end muliply2;
