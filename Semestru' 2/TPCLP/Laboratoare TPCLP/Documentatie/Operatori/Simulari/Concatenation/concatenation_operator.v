module concatenation_operator();
  
  initial 
    begin
    // concatenation
      $display ("Operatorul de concatenare");
      $display ("-reprezentari:");
      $display (" 1.  'b1   [%%b]= %b", 'b1);
      $display (" 2.  'b1   [%%0b]= %0b", 'b1);
      $display (" 3. 6'b1   [%%b]= %b", 6'b1);
      $display (" 4. 6'b1   [%%0b]= %0b", 6'b1);
      $display ();
      $display ("-concatenari:");
      $display (" 5. {6'b11} [%%b]= %b", {6'b11});
      $display (" 6. {4'b1001,4'b10x1}  [%%b]= %b", {4'b1001,4'b10x1});
      $display ();
      $display (" 7. {1{_1111}}  [%%s]= %s", {1{"_1111"}});
      $display (" 8. {1111,{7{_1111}}}  [%%s]= %s", {"1111",{7{"_1111"}}});
      $display ();
      $display (" 9. {32{1'b1}}       [%%b]=  %b", {32{1'b1}} );
      $display ("10. {32{1'b1}} [%%d]= %d", {32{1'b1}} );
      $display ("11. 'd4294967295 [%%b]= %b",'d4294967295 );
      $display ();
      $display ("12. {8'b11}            [%%b]= %b", {8'b11});
      $display ("13. {8'b11, 1'bxz} [%%b]= %b", {8'b11, 1'bxz});
      $display ("14. {8'b11, 2'bxz} [%%b]= %b", {8'b11, 2'bxz});
      $display ("15. {8'b11, 3'bxz} [%%b]= %b", {8'b11, 3'bxz});
      $display ("16. {8'b11, 3'bzx} [%%b]= %b", {8'b11, 3'bzx});
      $display ();
      $display ("17. 3'bxz [%%b]= %b", 3'bxz );
      $display ("18. 3'bzx [%%b]= %b", 3'bzx );
      $display ();
      
    end
  
  endmodule
