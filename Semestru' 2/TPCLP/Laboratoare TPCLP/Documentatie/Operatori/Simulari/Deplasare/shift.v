 module shift_operators();
   
  reg [3:0] a, b, c;
     
   initial
    begin
     $monitor ("time=%g,  a= %b,  b= %b,  c= %b", $time, a, b, c);
     a <= 4'b0000;
     b <= 4'b0000;
     #10 a <= 4'b1x1z;
     #20 b <= a<<1;
     #5  c <= a<<1;
    end
     
   initial
    begin
     $display ();
     $display ("Left shift");
     $display ("=====");
     $display (" 4'b0001<<1 [%%b]= %b,  [%%d]= %d", (4'b0001<<1),(4'b0001<<1));
     $display (" 4'b0010<<1 [%%b]= %b,  [%%d]= %d", (4'b0010<<1),(4'b0010<<1));
     $display (" 4'b0011<<1 [%%b]= %b,  [%%d]= %d", (4'b0011<<1),(4'b0011<<1));
     $display ();
     $display (" (4'b1001<<1)>>1 [%%b]= %b,  [%%d]= %d",(4'b1001<<1)>>1,(4'b1001<<1)>>1);
     $display (" 4'b1001<<1 = %b", (4'b1001<<1));
     $display (" 4'b10x1<<1 [%%b]= %b,  [%%d]= %d", (4'b10x1<<1),(4'b10x1<<1));
     $display (" 4'b10z1<<1 [%%b]= %b,  [%%d]= %d", (4'b10z1<<1),(4'b10z1<<1));
     $display ();
     $display ("Right shift");
     $display ("======");
     $display (" 4'b1001>>1 [%%b]= %b,  [%%d]= %d", (4'b1001>>1),(4'b1001>>1));
     $display (" 4'b10x1>>1 [%%b]= %b,  [%%d]= %d", (4'b10x1>>1),(4'b10x1>>1));
     $display (" 4'b10z1>>1 [%%b]= %b,  [%%d]= %d", (4'b10z1>>1),(4'b10z1>>1));
     $display ();
    end
     
 endmodule

