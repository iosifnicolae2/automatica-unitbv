module output_decoder(); // decodificator 3:8 (3 intrari : 8 iesiri)
   
  reg [2:0] in;  // cele 3 intrari ale DCD
  reg [7:0] out; // cele 8 iesiri ale DCD
       
  initial
    begin
     $monitor ("Time=%g, out <= (1'b1 << in)[%%b]= %b, in[%%b]= %b", $time,out,in);
        in = 3'b000;
     #1 in = 3'b001;
     #1 in = 3'b010;
     #1 in = 3'b011;
     #1 in = 3'b100;
     #1 in = 3'b101;
     #1 in = 3'b110;
     #1 in = 3'b111;
    end
    
    always @(in) out = (1'b1 << in);
endmodule
