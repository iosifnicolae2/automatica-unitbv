module decoder();
	reg [2:0] in;
	reg [7:0] out;

	initial
		begin
			$monitor ("Time[%%g]=%g, out[%%b]=%b, in[%%b]=%b, in[%%d]=%d", $time, out, in, in);
			
			in <= 0;
			#1 in <= 3'b1;
			#1 in <= 3'b010;
			#1 in <= 3'b011;
			#1 in <= 3'b100;
			#1 in <= 3'b101;
			#1 in <= 3'b110;
			#1 in <= 3'b111;
		end

	always @(in)
		out <= (1'b1 << in);
endmodule
