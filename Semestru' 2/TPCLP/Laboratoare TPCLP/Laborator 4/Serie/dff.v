module dff (q, d, ck); // device flip-flop, de tip d
  	   input d, ck; // porturi de intrare, de 1 bit
  	   output q; // port de iesire, de 1 bit
  	   reg q; // iesirea q declarata de tip registru
   	  always @(posedge ck)
    q = d;  // atribuirea se efectueaza pe frontul poz. de tact
endmodule