module test_bench_sreg (d, clk); //d,clk sunt stimulii generati
output d, clk;
reg d, clk;
reg wordin[3:0]; //wordin este cuvantul ce se incarca in registru

initial  //se seteaza compozitia cuvantului wordin
 begin
  wordin[3]=1;//asignere bit wordin[3] destinat incarcarii in registru
  wordin[2]=0;//asignere bit wordin[2] destinat incarcarii in registru
  wordin[1]=1;//asignere bit wordin[1] destinat incarcarii in registru
  wordin[0]=1;//asignere bit wordin[0] destinat incarcarii in registru
 
  clk = 1'b0;  // se pozitioneaza pe zero semnalul clock
 end
  
always #5 clk <= ~clk; //clock-ul este comutat tot la 5 u.t.

initial  //sunt expediati succesiv bitii cuvantului spre registru
 begin
  d <= #4 wordin[0];  // se aplica wordin[0]=1 la intrarea registrului
  d <= #14 wordin[1]; // se aplica wordin[1]=1 la intrarea registrului
  d <= #24 wordin[2]; // se aplica wordin[2]=0 la intrarea registrului
  d <= #34 wordin[3]; // se aplica wordin[3]=1 la intrarea registrului
  d <= #44 1'bx;    /* se aplica X la intrarea registrului,in vederea  incarcarii lui x in bistabile la descarcarea acestora */
 end
endmodule