module test_sreg ();  //modulul parinte
  wire shiftout, shiftin, clk;  //cele 3 fire de legare cu exteriorul
  test_bench_sreg TB (.d(shiftin), .clk(clk));
  sreg4b DUT (.shiftin(shiftin), .clk(clk), .shiftout(shiftout));
endmodule