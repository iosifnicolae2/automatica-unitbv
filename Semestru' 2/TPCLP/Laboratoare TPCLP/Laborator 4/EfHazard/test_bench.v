module test_bench (in);
   output  in;
   reg  in;
   initial  in = 1'b0;            // la momentul de start, semnalul in este atribuit cu valoarea binara 0
   always #5 in  <=  ~ in;  // valoarea lui in se complementeaza din 5 in 5 unitati de timp
endmodule