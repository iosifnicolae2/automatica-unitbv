module sumator(a, b, cin, s, cout);     //  nume modul si (lista de porturi)

input a, b, cin;                        //  declarare a porturilor de intrare
output s, cout;                         //  declarare a porturilor de iesire
wire w1, w2, w3;                        /*  declararea tipului wire, pentru conexiunile dintre
                                          iesirea portilor AND si intrarile poartii OR */

assign w1 = (a & b);                    //  atribuire continua a valorii pentru w1 
assign w2 = (a & cin);
assign w3 = (b & cin);

assign cout = (w1 | w2 | w3);           //  atribuire continua a bitului cout
assign s = (a ^ b ^ cin);               //  atribuire continua a bitului s

endmodule                               //  cuvantul cheie de incheiere corp modul
