module test_sumator();            //  numele modulului. Nu exista lista  de porturi.

parameter per = 5;                //  asignerea cu 5 unitati de timp a parametrului per 

wire a,b,cin,s,cout;              /*  declarare fire de legatura TB-DUT si DUT-TEST. Se poate ignora declararea lor ele fiind si porturi.*/

test_bench_sumator #(per) TB (    //  este citata instanta modulului TB
    .a(a),                        //  semnifica ca portul a al TB se leaga la firul (a) de iesire
    .b(b),                        //  portul b al TB se leaga la firul (b) de iesire
    .cin(cin)                     //  portul cin al TB se leaga la firul (cin) de iesire
		                  );

sumator DUT (                     //  este citata instanta modulului DUT
	.a(a),                          //  portul a (notat .a) al DUT se leaga la firul (a) de intrare
	.b(b),                          //  portul b al DUT se leaga la firul (b) de intrare
	.cin(cin), 	                    //  portul cin al DUT se leaga la firul (cin) de intrare
	.s(s),                          //  portul s al DUT se leaga la firul (s) de iesire
	.cout(cout)                     //  portul cout al DUT se leaga la firul (cout)de iesire
		);
endmodule