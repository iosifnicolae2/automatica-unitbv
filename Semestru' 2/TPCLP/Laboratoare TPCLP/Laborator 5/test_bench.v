module test_bench (enable, clear, clk, load, read, D3, D2, D1, D0);
  output enable, clear, clk, load, read, D3, D2, D1, D0;
  reg enable, clear, clk, load, read;
  reg[3:0] wordin;
  
  initial
   begin
    wordin <= 4'b1011;
    
    clk = 1'b0; // se pozitioneaza pe zero semnalul clk de ceas
    enable = 0; // sunt trecute toate firele d de intrare date pe 0 (zero)
    clear = 0;  // sunt trecute toate firele q de iesire date pe 0 (zero)
    load = 0;   // se blocheaza aplicarea tactelor de clock
    read = 0;  // se dezactiveaza citirea datelor la iesirile Q din registru
    clear <= #7 1; // se dezactiveaza stergerea asincrona de clear
    enable <= #8 1; // se autorizeaza aplicarea datelor D registrului
    load <= #9 1; // se autorizeaza aplicarea tactelor de clock
    read  <= #13 1; // se autorizeaza citirea datelor la iesirile Q ale registrului
    
    repeat(4) // se repeta de 4 ori comutarea ceasului clk
      begin
       #5 clk <= ~clk; // se comanda comutarea ceasului din 5 in 5 unitati timp
      end
   end
   
   assign D3 = wordin[3]; //atribuire bit D3 date aplicate registrului
   assign D2 = wordin[2]; // idem bit D2
   assign D1 = wordin[1]; // idem bit D1
   assign D0 = wordin[0]; // idem bit D0

endmodule