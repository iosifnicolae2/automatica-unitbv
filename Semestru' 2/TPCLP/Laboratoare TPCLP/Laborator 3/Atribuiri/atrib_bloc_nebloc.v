module atrib_bloc_nebloc ();
  reg a, b, c, d;
  reg e, f, g, h;
  
  initial                                                     //  atribuiri de valori initiale
   begin
     a = 1'b0;
     b = 1'b0;
     c = 1'b0;
     d = 1'b0;
          
     e <= 1'b0;
     f <= 1'b0;
     g <= 1'b0;
     h <= 1'b0;
  
     
     //  proceduri de atribuiri blocante
     $display(" atribuiri blocante");                         //  comanda afisarea sirului dintre ghilimele
     $display("Time\t a\t b\t c\t d");                        //  comanda afisarea etichetelor, tabulate
     $monitor("%2d\t %b\t %b\t %b\t %b", $time, a, b, c, d);  //  comanda afisarea valorilor
     
     a = #10 1'b1;                                            //  prima atribuire blocanta, la 10 ut de la start simulare
     b = #2 1'b1;                                             //  a 2-a atribuire blocanta, la 10+2=12 ut de la start simulare
     c = #4 1'b1;                                             //  a 3-a atribuire blocanta, la 12+4=16 ut de la start simulare
     d = #6 1'b1;                                             //  a 4-a atribuire blocanta, la 16+6=22 ut de la start simulare
     $display();
     
     //  proceduri de atribuiri neblocante
     $display(" atribuiri neblocante");                       //  comanda afisarea sirului dintre ghilimele
     $display("Time\t e\t f\t g\t h");                        //  comanda afisarea etichetelor, tabulate
     $monitor("%2d\t %b\t %b\t %b\t %b", $time, e, f, g, h);  //  comanda afisarea valorilor
     
     e <= #10 1'b1;                                           //  a 4-a atribuire neblocanta, la 10 ut de la start simulare
     f <= #2 1'b1;                                            //  prima atribuire neblocanta, la 2 ut de la start simulare
     g <= #4 1'b1;                                            //  a 2-a atribuire neblocanta, la 4 ut de la start simulare
     h <= #6 1'b1;                                            //  a 3-a atribuire neblocanta, la 6 ut de la start simulare
          
   end
endmodule