module varianta_instruire;  // Varianta de instruire pentru testul de laborator TPCLP, anul univ. 2012, sem. 2

 reg [3:0] a, b ,c;  // variabilele vector a,b,c sunt de tipul registru, de 4 biti.
 reg [11:0] m, n, p, q, r;  // variabilele vector m,n,p,q,r sunt de tipul registru, de 12 biti.
 
 initial // se executa o singura data blocul de instructiuni cuprinse intre begin si end
  begin
   a = 4'b1100;
   b = 4'b0011;
   c = 4'b0101;
   
   m = 8'b1111_1111_0000; // la evaluare, ModelSim semnaleaza Warning:[RDGN]-Redundant Digits in Numeric literal
   n = 4'b11001100; // idem ca mai sus (adica expresia atribuie biti in plus fata de trunchierea indicata)
   p = 'b101;
   q = 8'b0X1;
   r = 8'bX01;
   
   $display("varianta_instruire.v");
   //La simularea modulului, rezultatul simularii este afisat in panoul Transcript.
   //Comentariul de la finele fiecarei linii $display de mai jos are rol explicativ.
   //$display afiseaza textul dintre ghilimele. Expresia de dupa text este elementul supus procesarii.
   
   $display (); // se afiseaza un rand gol pentru facilitarea citirii rezultatelor.
   $display (" 1. ~a = ~1100 %%b = %b ", ~a); //rezulta un nou operand cu bitii lui a negati, afisat in binar
   $display (" 2. ~a = ~1100 %%d = %d ", ~a); //rezulta un nou operand cu bitii lui a negati, afisat in zecimal
   $display ();
   $display (" 3. a&c  = (1100 & 0101) %%b = %b ",  (a & c) ); //rezultatul actiunii lui AND, afisat in binar 
   $display (" 4. a&c  = (1100 & 0101) %%d = %d ",  (a & c) ); //idem, afisat in zecimal
   $display ();
   $display (" 5. a|b  = (1100 | 0011) %%b = %b ",  (a | b) ); //rezultatul actiunii lui OR, afisat in binar
   $display (" 6. a|b  = (1100 | 0011) %%d = %d ",  (a | b) ); //idem, afisat in zecimal
   $display ();
   $display (" 7. b^c  = (0011 ^ 0101) %%b = %b ",  (b ^ c) ); //rezultatul actiunii lui XOR, afisat in binar
   $display (" 8. b^c  = (0011 ^ 0101) %%d = %d ",  (b ^ c) ); //idem, afisat in zecimal
   $display ();
   $display (" 9. (4'b0010 << 1)%%b= %b,  (4'b0010 << 1)%%d= %d", (4'b0010 << 1),(4'b0010 << 1)); //deplasare stanga, 1 pas(in binar si zecimal)
   $display ("10. (4'b0100 >> 1)%%b= %b,  (4'b0100 >> 1)%%d= %d", (4'b0100 >> 1),(4'b0100 >> 1)); //deplasare dreapta, 1 pas(in binar si zecimal)
   $display ();
   $display ("11. reg[11:0] m=8'b1111_1111_0000: %%b= %b, %%h= %h, %%d= %d", m,m,m); // m trunchiat la 8 biti,afisat in binar,hexazecimal,zecimal.
   $display ("12. reg[11:0] n=4'b11001100: %%b= %b, %%h= %h, %%d= %d", n,n,n); // n trunchiat la 4 biti, afisat in binar, hexazecimal si zecimal.
   $display ("13. reg[11:0] p='b101: %%b= %b, %%h= %h, %%d= %d", p,p,p); //valoarea lui p,afisata in binar si decimal.
   $display ("14. reg[11:0] q=8'b0X1 %%b= %b, %%h= %h, %%d= %d", q,q,q); //valoarea lui q extinsa la 8 biti,afisata in binar,hexadecimal,zecimal.
   $display ("15. reg[11:0] r=8'bX01 %%b= %b, %%h= %h, %%d= %d", r,r,r); //valoarea lui r extinsa la 8 biti,afisata in binar,hexadecimal,zecimal.
   $display ();
   $display ("16.  (4'b11xz & 4'bxz00) %%b = %b ",  (4'b11xz & 4'bxz00) ); //rezultatul operatorului AND, afisat in binar
   $display ("17.  (4'b11xz & 4'b11zz) %%b = %b ",  (4'b11xz & 4'b11zz) );
   $display ();
   $display ("18.  (4'b11xz | 4'b1000) %%b = %b ",  (4'b11xz | 4'b1000) ); //rezultatul operatorului OR, afisat in binar
   $display ("19.  (4'b11xz | 4'b0011) %%b = %b ",  (4'b11xz | 4'b0011) );
   $display ();
   $display ("20.  (4'b11xz ^ 4'b1000) %%b = %b ",  (4'b11xz ^ 4'b1000) ); //rezultatul operatorului XOR, afisat in binar
   $display ("21.  (4'b11xz ^ 4'b1011) %%b = %b ",  (4'b11xz ^ 4'b1011) );
   $display ();
   $display ("22. (2'bxz11) %%b = %b",  (2'bxz11) ); //afisare in binar a numarului binar xz11, trunchiat la 2 biti.
   $display ("23. (3'bxz11) %%b = %b",  (3'bxz11) ); //idem, trunchiat la 3 biti.
   $display ("24. (4'bxz11) %%b = %b",  (4'bxz11) ); //afisare pe 4 biti a numarului binar xz11
   $display ("25. (8'bxz11) %%b = %b, %%h = %h",  (8'bxz11),(8'bxz11) ); //afisare xz11 in binar si hexazecimal, completare cu x pana la inclusiv bitul 8.
   $display ("26. (8'b1xz11) %%b = %b, %%h = %h", (8'b1xz11),(8'b1xz11) ); //afisare in binar si hexazecimal, completare cu 0 pana la inclusiv bitul 8.
   $display ();
   $display ("27. {8{1'b1}} %%d = %d", {8{1'b1}} ); //concatenare de 8 ori a bitului 1, reprezentata in zecimal.
   $display ("28. {8{1'b1}} %%b = %b, %%h = %h", {8{1'b1}},{8{1'b1}} ); //concatenare de 8 ori a bitului 1, reprezentata in binar si hexazecimal.
   $display ();
   $display ("29. ('d11) %%b = %b", ('d11) ); //numarul zecimal 11 reprezentat in binar pe 32 biti, afisat in binar.
   $display ("30. ('d11) %%h = %h", ('d11) ); //idem, afisat in hexagesimal.
   $display ("31.  (3'd11) %%b = %b", (3'd11) ); //numarul zecimal 11 trunchiat in binar la 3 biti din cei 32 rezervati, afisat in binar.
   $display ("32.  (3'd11) %%d = %d", (3'd11) ); //idem, afisat in zecimal.
   $display ();
   $display ("33. (2'd11 + 2'd10) %%b = %b",  (2'd11 + 2'd10) ); //suma aritm. intre operanzii zecimali 11 si 10 trunchiati la 2 cifre in binar, afisare in binar.
   $display ("34. (3'd11 + 2'd10) %%d = %d",  (3'd11 + 2'd10) ); //suma aritm. intre operanzii zecimali 11 si 10 trunchiati la 3 si 2 cifre in binar,afis. zecimal.
   $display ("35. (3'd111 + 2'd10): %%b= %b, %%d= %d",  (3'd111 + 2'd10), (3'd111 + 2'd10));//suma aritm. intre 111 si 10 trunchiati la 3 si 2 cifre in binar, afisare in binar si zecimal.
  end
endmodule

