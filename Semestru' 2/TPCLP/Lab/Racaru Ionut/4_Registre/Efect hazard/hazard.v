module  hazard (in,  out);
   input  in;
   output  out;
   reg  out, not_in;
always  @(in or not_in)  // lista senzitiva supravegheaza semnalele in si not_in
     begin
        out <=  in & not_in;   // out este evaluat intai pentru in initial si mai apoi pentru noul in
        not_in  <=  ~ in;   // aici not_in este ulterior lui not_in utilizat in atribuirea lui out
    end
endmodule