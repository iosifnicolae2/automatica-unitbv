module AND (in1, in2, out);  // poarta SI cu 2 intrari
   input in1, in2;       // porturi de intrare, de 1 bit
   output out;            // port de iesire, de 1 bit
   wire in1, in2, out; // ietrari si iesire tip fir
  
   assign out = in1 & in2;
endmodule