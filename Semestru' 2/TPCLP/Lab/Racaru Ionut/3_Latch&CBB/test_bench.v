module test_bench (d, clk);
  output d, clk;
  reg d, clk;
  
  initial
    begin
      d=0;
      clk=0;
      #8  d=1;
      #16 d=0;
      #27 d=1;
      #3  d=0;
      #2  d=1;
      #32 d=0;
      #1  $stop;
    end
  
  always #5 clk<=~clk;
    
endmodule