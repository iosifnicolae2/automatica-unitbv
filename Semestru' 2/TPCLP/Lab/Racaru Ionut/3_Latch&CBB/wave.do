onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -color Red /test/clk
add wave -noupdate -color Gold /test/d
add wave -noupdate /test/q_latch
add wave -noupdate -color Cyan /test/qb
add wave -noupdate -color Magenta /test/qbn
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {18 ns} 0}
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {0 ns} {93 ns}
