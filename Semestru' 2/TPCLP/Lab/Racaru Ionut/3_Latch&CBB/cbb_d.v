module cbb_d (d, clk, q, qn);
  input d, clk;
  output q, qn;
  reg q, qn;

  always @ (posedge clk)
    begin
      q<=d;
      qn<=~d;
    end
    
endmodule    