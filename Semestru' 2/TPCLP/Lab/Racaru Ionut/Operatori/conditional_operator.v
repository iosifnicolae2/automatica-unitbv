module conditional_operator();  // vezi proiectul Proba4
  wire out;
  reg enable,data;
     // Tri state buffer
  assign out = (enable) ? data : 1'bz;  // daca <enable> este true, atunci out=data, altfel out=z
  initial
   begin
     $display ("");  // afiseaza un rand liber
     $display ("time\t enable\t data\t out");  // caracterele \t comanda o tabulare
     $monitor ("%0d\t %b\t %b\t %b",$time,enable,data,out);  //$time este o functie sistem de timp
     enable = 0;
     data = 0;
     data <= #1 1;
     enable <= #2 1;
     data <= #3 0;
     #4 $stop;  // opreste procedura de simulare la 4 u.t. de la start
   end	
endmodule