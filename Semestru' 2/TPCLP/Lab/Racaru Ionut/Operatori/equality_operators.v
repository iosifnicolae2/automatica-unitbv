module equality_operators();  // vezi proiectul Proba1
 //autor: svh, Mai 2010  
  initial
    begin
     // Case Equality
      $display ("Case Equality");
      $display ("    Expresii cu operator              Rezultat logic");
      $display (" 4'bX001  ===  4'bX001   ==>     %b", (4'bx001 === 4'bx001));
      $display (" 4'bX0X1  ===  4'bX001   ==>     %b", (4'bx0x1 === 4'bx001));
      $display (" 4'bZ0X1  ===  4'bZ0X1   ==>     %b", (4'bz0x1 === 4'bz0x1));
      $display (" 4'bZ0X1  ===  4'bZ001   ==>     %b", (4'bz0x1 === 4'bz001));
      $display ();
     
       // Case Inequality
      $display ("Case Inequality");
      $display ("    Expresii cu operator              Rezultat logic");
      $display (" 4'bX0X1  !==   4'bX0X1   ==>     %b", (4'bx0x1 !== 4'bx0x1));
      $display (" 4'bX0X1  !==   4'bX001   ==>     %b", (4'bx0x1 !== 4'bx001));
      $display (" 4'bZ0X1  !==   4'bZ001   ==>     %b", (4'bz0x1 !== 4'bz001));
      $display (); 
    
      // Logical Equality
      $display ("Logical Equality");
      $display ("    Expresii cu operator              Rezultat logic");
      $display ("            5  ==  5               ==>     %b", (5 == 5));
      $display ("            5  == 10              ==>     %b", (5 == 10));
      $display ("         'd15  ==  'hF            ==>     %b", ('d15 == 'hf));
      $display ("      2'b10  ==  2'b10       ==>      %b", (2'b10 == 2'b10));
      $display ("      2'b1X  ==  2'b1X       ==>      %b", (2'b1x == 2'b1x));
      $display ("      2'bZ1  ==  2'bZ1       ==>      %b", (2'bz1 == 2'bz1));
      $display ();
    
      // Logical Inequality
      $display ("Logical Inequality");
      $display ("    Expresii cu operator              Rezultat logic");
      $display ("     2'b10  !=  2'b10       ==>      %b", (2'b10 != 2'b10));
      $display ("     2'b1X  !=  2'b1X       ==>      %b", (2'b1x != 2'b1x));
      $display ("     2'b1Z  !=  2'b1Z       ==>      %b", (2'b1z != 2'b1z));
      $display ("     2'b1Z  !=  2'b1X       ==>      %b", (2'b1z != 2'b1x));
      $display ("     2'b10  !=  2'b11       ==>      %b", (2'b10 != 2'b11));
      $display ();
      #1  $stop;
    end
 endmodule
