module bitwise_operators();  // vezi proiectul Proba2
 initial
  begin
       // Bit Wise Negation
     $display ();
     $display (" Bit Wise Negation");
     $display ("  ~0001 = %b", (~4'b0001));
     $display ("  ~x001 = %b", (~4'bx001));
     $display ("  ~z001 = %b", (~4'bz001));
     
       // Bit Wise AND
     $display ();
     $display (" Bit Wise AND");
     $display ("  0001 & 1001 = %b", (4'b0001 & 4'b1001));
     $display ("  1001 & x001 = %b", (4'b1001 & 4'bx001));
     $display ("  1001 & z001 = %b", (4'b1001 & 4'bz001));
     $display ("  x001 & z001 = %b", (4'bx001 & 4'bz001));
     
      // Bit Wise OR
     $display ();
     $display (" Bit Wise OR");
     $display ("  0001 | 1001 = %b", (4'b0001 | 4'b1001));
     $display ("  0001 | x001 = %b", (4'b0001 | 4'bx001));
     $display ("  1001 | x001 = %b", (4'b1001 | 4'bx001));
     $display ("  0001 | z001 = %b", (4'b0001 | 4'bz001));
     $display ("  1001 | z001 = %b", (4'b1001 | 4'bz001));
     $display ("  x001 | z001 = %b", (4'bx001 | 4'bz001));
     
      // Bit Wise XOR
     $display ();
     $display (" Bit Wise XOR");
     $display ("  0001 ^ 1001 = %b", (4'b0001 ^ 4'b1001));
     $display ("  0001 ^ x001 = %b", (4'b0001 ^ 4'bx001));
     $display ("  0001 ^ z001 = %b", (4'b1001 ^ 4'bz001));
     $display ("  x001 ^ z001 = %b", (4'bx001 ^ 4'bz001));
     
      // Bit Wise XNOR
     $display ();
     $display (" Bit Wise XNOR");
     $display (" 0001 ~^ 1001 = %b", (4'b0001 ~^ 4'b1001));
     $display (" 0001 ~^ x001 = %b", (4'b0001 ~^ 4'bx001));
     $display (" 0001 ~^ z001 = %b", (4'b0001 ~^ 4'bz001));
     $display (" x001 ~^ z001 = %b", (4'bx001 ~^ 4'bz001));
     $display ();
     #10  $stop;
  end
endmodule