onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -color {Orange Red} -expand -subitemconfig {{/test_mux/TB/counter[2]} {-color #ffff45450000 -height 15} {/test_mux/TB/counter[1]} {-color #ffff45450000 -height 15} {/test_mux/TB/counter[0]} {-color #ffff45450000 -height 15}} /test_mux/TB/counter
add wave -noupdate -color Brown /test_mux/TB/clk
add wave -noupdate -color Gold /test_mux/i0
add wave -noupdate /test_mux/i1
add wave -noupdate /test_mux/out
add wave -noupdate -color Gray75 /test_mux/per
add wave -noupdate -color Violet /test_mux/sel
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {24 ns} 0}
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {0 ns} {79 ns}
