module test_mux();
parameter per = 5;

wire i0, i1, sel, out;//declaratia se poate elimina,fiind implicita

  test_bench_mux #(per) TB (.i0(i0), .i1(i1), .sel(sel));

  mux DUT (.i0(i0), .i1(i1), .sel(sel), .out(out));

endmodule

