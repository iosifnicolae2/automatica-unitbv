module mux(i0, i1, sel, out);
 input i0, i1, sel; // declararea porturilor de intrare
 output out; // declararea porturilor de iesire
 assign out = (sel) ? i1 : i0;//daca sel e true,out=i1,altfel =i2
endmodule

