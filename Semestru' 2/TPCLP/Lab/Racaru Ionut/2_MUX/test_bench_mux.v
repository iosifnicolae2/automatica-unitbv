module test_bench_mux (i0, i1, sel);
parameter per = 5; // perioada de repetitie a tactului de clock 
output i0, i1, sel;

reg clk;
reg [2:0] counter;//contor generator de stimuli 

initial
  begin
    clk = 0;
    counter = 0;
  end

always @( posedge clk )//pe front clk se executa blocul begin-end
  begin
    if (counter == {3{1'b1}})
      $stop;  //este stopat procesul de simulare
    else
      counter = counter + 1; //este incrementat contorul
  end 

assign i0 = counter[0]; //e atribuita val.lui counter-bitul poz.0
assign i1 = counter[1]; //e atribuita val.lui counter-bitul poz.1
assign sel = counter[2];//e atribuita val.lui counter-bitul poz.2

always
  #per clk = ~clk; //cadentarea tactului cu perioada per

endmodule

