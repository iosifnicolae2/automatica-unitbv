onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /test_sumator/a
add wave -noupdate /test_sumator/b
add wave -noupdate /test_sumator/cin
add wave -noupdate /test_sumator/s
add wave -noupdate /test_sumator/cout
add wave -noupdate /test_sumator/TB/counter
add wave -noupdate /test_sumator/TB/clk
add wave -noupdate /test_sumator/DUT/w1
add wave -noupdate /test_sumator/DUT/w2
add wave -noupdate /test_sumator/DUT/w3
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {0 ns} 0}
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {0 ns} {1 us}
