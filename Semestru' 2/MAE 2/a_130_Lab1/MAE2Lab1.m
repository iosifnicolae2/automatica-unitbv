function varargout = MAE2Lab1(varargin)
% MAE2LAB1 M-file for MAE2Lab1.fig
%      MAE2LAB1, by itself, creates a new MAE2LAB1 or raises the existing
%      singleton*.
%
%      H = MAE2LAB1 returns the handle to a new MAE2LAB1 or the handle to
%      the existing singleton*.
%
%      MAE2LAB1('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in MAE2LAB1.M with the given input arguments.
%
%      MAE2LAB1('Property','Value',...) creates a new MAE2LAB1 or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before MAE2Lab1_OpeningFunction gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to MAE2Lab1_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Copyright 2002-2003 The MathWorks, Inc.

% Edit the above text to modify the response to help MAE2Lab1

% Last Modified by GUIDE v2.5 29-Feb-2012 00:01:13

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @MAE2Lab1_OpeningFcn, ...
                   'gui_OutputFcn',  @MAE2Lab1_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% Deschidere aplicatie
%--------------------------------------------------------------------------
function MAE2Lab1_OpeningFcn(hObject, eventdata, handles, varargin)

handles.output = hObject;

guidata(hObject, handles);

Comutator='Initializare';%executa rutina de initializare

Valori=handles;%preia valorile initiale ale manerelor obiectelor

Calcule(Comutator,Valori);%transfera variabilele spre aplicatia principala

% Nu este utilizat
%--------------------------------------------------------------------------
function varargout = MAE2Lab1_OutputFcn(hObject, eventdata, handles) 

varargout{1} = handles.output;

% Selecteaza tipul motorului de curent continuu
%--------------------------------------------------------------------------
function uipanelTipMotor_SelectionChangeFcn(hObject, eventdata, handles)

if get(handles.radiobuttonMotorExcitatieSeparata,'Value')==1.0;
    
    Calcule('ExcitatieSeparata',1);
    
elseif  get(handles.radiobuttonExcitatieDerivatie,'Value')==1.0;
    
    Calcule('ExcitatieDerivatie',2);
    
else
    aux=0;
end

% Selecteaza tipul graficului
%--------------------------------------------------------------------------
function uipanelGrafice_SelectionChangeFcn(hObject, eventdata, handles)

if get(handles.radiobuttonCurentDeExcitatie,'Value')==1.0;
    
    Calcule('GraficCurentDeExcitatie',1);
    
elseif get(handles.radiobuttonCurentRotoric,'Value')==1.0;
    
    Calcule('GraficCurentIndus',2);
    
elseif get(handles.radiobuttonCupluElectromagnetic,'Value')==1.0;
    
    Calcule('GraficCuplu',3);
    
elseif get(handles.radiobuttonTuratia,'Value')==1.0;

    Calcule('GraficTuratie',4);
    
else
end
    
% Parasire aplicatie
%--------------------------------------------------------------------------
function pushbuttonParasire_Callback(hObject, eventdata, handles)

close(MAE2Lab1);%inchide figura ...

% Aplicatia principala
%--------------------------------------------------------------------------
function Calcule(Comutator,Valori)

persistent Manere
persistent TipExcitatie
persistent TipGrafic
persistent detT
persistent Ta
persistent Tf
persistent Tj
persistent rA
persistent rF
persistent Nfinal

switch Comutator
    case 'Initializare'
        Manere=Valori;%salveaza manerele obiectelor interfetei grafice
        TipExcitatie=1;%initial motor cu excitatie separata
        TipGrafic=1;%initial graficul turatiei
        Nfinal=300;%numarul maxim de esantioane
        detT=0.01;%perioada de esantionare
        Ta=0.013;%constanta de timp a rotorului
        Tf=0.42;%constanta de timp a inf. de excitatie
        Tj=0.57;%constanta de timp mecanica
        rA=0.11;%rezistenta raportata a rotorului
        rF=1;%rezistenta raportata a inf. de excitatie
    case 'ExcitatieSeparata'
        TipExcitatie=Valori;%seteaza variabila interna care corespunde tipului de excitatie
    case 'ExcitatieDerivatie'
        TipExcitatie=Valori;%seteaza variabila interna care corespunde tipului de excitatie
    case 'GraficCurentDeExcitatie'
        TipGrafic=Valori;%seteaza variabila interna care corespunde tipului de grafic
    case 'GraficCurentIndus'
        TipGrafic=Valori;%seteaza variabila interna care corespunde tipului de grafic
    case 'GraficCuplu'
        TipGrafic=Valori;%seteaza variabila interna care corespunde tipului de grafic
    case 'GraficTuratie'
        TipGrafic=Valori;%seteaza variabila interna care corespunde tipului de grafic
end
    
timp(1)=0;
iA(1)=0;
iF(1)=0;
n(1)=0;
m(1)=0;

for k1=1:Nfinal
    timp(k1+1)=timp(k1)+detT;%vectorul valorilor esantioanelor de timp
end

for k1=1:Nfinal
    if TipExcitatie==1
        iF(k1+1)=1;%pt.excitatie separata curentul de excitatie este constant
    elseif TipExcitatie==2
        iF(k1+1)=iF(k1)+(detT/Tf)*(1/rF-iF(k1));%pt.excitatie derivatie curentul de excitatie ...
    else
    aux=0;
    end
    iA(k1+1)=(1-detT/Ta)*iA(k1)+(detT/Ta)*((1-n(k1)*iF(k1))/rA);
    n(k1+1)=n(k1)+(detT/Tj)*iF(k1)*iA(k1);
    m(k1+1)=iA(k1)*iF(k1);
end

axes(Manere.axesGrafic1);%activeaza obiectul axesGrafic1

switch TipGrafic
    case 1
        plot(timp,iF,'b');%reprezentarea curentului de excitatie
        grid
        axis([timp(1),timp(end),iF(1),1.25*iF(end)])
        xlabel 'Timp'
        ylabel 'iF'
        title 'CURENTUL DE EXCITATIE'
    case 2
        plot(timp,iA,'r');%reprezinta curentul prin indus
        grid
        xlabel 'Timp'
        ylabel 'iA'
        title 'CURENTUL INDUS'
    case 3
        plot(timp,m,'m');%reprezinta cuplul electromagnetic
        grid
        xlabel 'Timp'
        ylabel 'm'
        title 'CUPLUL ELECTROMAGNETIC'
    case 4
        plot(timp,n,'g');%reprezinta turatia
        grid
        axis([timp(1),timp(end),n(1),1.25*max(n)])
        xlabel 'Timp'
        ylabel 'n'
        title 'TURATIA'
end
aux=0;








