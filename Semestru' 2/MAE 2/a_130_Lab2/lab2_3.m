function lab2_3
%Fazori spatiali; alimentarea de la invertoare trifazate

close all                           %inchide toate ferestrele active
clear                               %sterge toate variabilele si functiile declarate anterior
j=sqrt(-1);                         %numarul j
omega1=100*pi;                      %pulsatia marimilor electrice 
Te=0.1e-3;                          %perioada de esantionare este de 1 milisecunda 
Imax=sqrt(2)*1;                     %amplitudinea curentului
Ne=720;                             %numarul de esantioane
for k=1:Ne                          %curentii din indus
    timp(k)=Te*k;
    unghi(k)=omega1*timp(k);%unghiul electric
    detunghi(k)=mod(unghi(k),2*pi);%unghiul electric redus la intervalul [0; 2*pi]  
    if (detunghi(k)>=0)&(detunghi(k)<pi/3);% primul cadran
            iA(k)=2/3*Imax;
            iB(k)=-1/3*Imax;
            iC(k)=-1/3*Imax;
        elseif (detunghi(k)>=pi/3)&(detunghi(k)<2*pi/3);%al doilea cadran     
            iA(k)=1/3*Imax;
            iB(k)=1/3*Imax;
            iC(k)=-2/3*Imax;   
        elseif (detunghi(k)>=2*pi/3)&(detunghi(k)<pi);%al treilea cadran
            iA(k)=-1/3*Imax;
            iB(k)=2/3*Imax;
            iC(k)=-1/3*Imax;
        elseif (detunghi(k)>=pi)&(detunghi(k)<4*pi/3);%al patrulea cadran
            iA(k)=-2/3*Imax;
            iB(k)=1/3*Imax;
            iC(k)=1/3*Imax;    
        elseif (detunghi(k)>=4*pi/3)&(detunghi(k)<5*pi/3);%al cincilea cadran
            iA(k)=-1/3*Imax;
            iB(k)=-1/3*Imax;
            iC(k)=2/3*Imax;
        else (detunghi(k)>=5*pi/3)&(detunghi(k)<2*pi);%al saselea cadran
            iA(k)=1/3*Imax;
            iB(k)=-2/3*Imax;
            iC(k)=1/3*Imax;
        pause(0.1)    
        end
end
a=-1/2+sqrt(3)/2*j;                          %radacina complexa a unitatii

for k=1:Ne
    Is(k)=2/3*(iA(k)+a*iB(k)+a^2*iC(k));     %vectorul spatial al curentului 
    Isr(k)=real(Is(k));
    Isi(k)=imag(Is(k));
end

subplot 221
plot(timp,iA,'r')
title('Curentul iA')
xlabel('timp [s]')
grid

subplot 222
plot(timp,iB,'b')
title('Curentul iB')
xlabel('timp [s]')
grid

subplot 223
plot(timp,iC,'g')
title('Curentul iC')
xlabel('timp [s]')
grid

subplot 224
plot(Isr,Isi,'m')
title('Fazorul spatial al curentilor')
xlabel('Axa reala')
ylabel('Axa imaginara')
grid



