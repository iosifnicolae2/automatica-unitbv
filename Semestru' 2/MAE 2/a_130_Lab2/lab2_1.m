function lab2_1
%Fazori spatiali; regimul stationar simetric

close all%inchide toate ferestrele active
clear%sterge toate variabilele si functiile declarate anterior
j=sqrt(-1);
omega1=100*pi;                      %pulsatia marimilor electrice 
Te=0.1e-3;                          %perioada de esantionare este de 1 milisecunda 
Imax=sqrt(2)*1;                     %amplitudinea curentului
Ne=720;                              %numarul de esantioane

for k=1:Ne                         %curentii din indus
    timp(k)=Te*k;
    iA(k)=Imax*sin(omega1*timp(k));
    iB(k)=Imax*sin(omega1*timp(k)-2*pi/3);
    iC(k)=Imax*sin(omega1*timp(k)-4*pi/3);
end
a=-1/2+sqrt(3)/2*j;                  %radacina complexa a unitatii

for k=1:Ne
    Is(k)=2/3*(iA(k)+a*iB(k)+a^2*iC(k));  %vectorul spatial al curentului 
    Isr(k)=real(Is(k));
    Isi(k)=imag(Is(k));
end

subplot 221
plot(timp,iA,'r')
title('Curentul iA')
xlabel('timp [s]')
grid

subplot 222
plot(timp,iB,'b')
title('Curentul iB')
xlabel('timp [s]')
grid

subplot 223
plot(timp,iC,'g')
title('Curentul iC')
grid
xlabel('timp [s]')
subplot 224

plot(Isr,Isi,'m')
title('Locul geometric al fazorului spatial al curentilor')
grid
xlabel('Axa reala')
ylabel('Axa imaginara')



