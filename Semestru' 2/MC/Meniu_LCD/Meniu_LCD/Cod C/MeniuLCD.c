sbit LCD_RS at RB4_bit;
sbit LCD_EN at RB5_bit;
sbit LCD_D7 at RB3_bit;
sbit LCD_D6 at RB2_bit;
sbit LCD_D5 at RB1_bit;
sbit LCD_D4 at RB0_bit;

// Pin direction
sbit LCD_RS_Direction at TRISB4_bit;
sbit LCD_EN_Direction at TRISB5_bit;
sbit LCD_D7_Direction at TRISB3_bit;
sbit LCD_D6_Direction at TRISB2_bit;
sbit LCD_D5_Direction at TRISB1_bit;
sbit LCD_D4_Direction at TRISB0_bit;


void main() {

char sir[16];
int i=0;


//sectiunea de declarare a st�rilor
short A;
short B;
short C;
short D;
short E;

//sectiunea de initializare a m�rimilor de intrare
TRISD.RD0 = 1;  // OK
TRISD.RD1 = 1;  // Cancel
TRISD.RD2 = 1;  // <
TRISD.RD3 = 1;  // >

//sectiunea de initializare a m�rimilor de iesire
TRISD.RD4 = 0;
PORTD.RD4 = 0;

//sectiunea de initializare a st�rilor
A = 1;
B = 0;
C = 0;
D = 0;
E = 0;

//initializare LCD
Lcd_Init();
Lcd_Cmd(_LCD_CURSOR_OFF);

while(1)
 {

  //sectiunea de reconfigurare a st�rilor
  if ((A==1)&&(PORTD.RD0==1)) {A=0; B=1; C=0; D=0; E=0; while( PORTD.RD0 == 1 ){}}
  if ((B==1)&&(PORTD.RD0==1)) {A=0; B=0; C=0; D=1; E=0; while( PORTD.RD0 == 1 ){}}  // B -> OK -> D
  if ((B==1)&&(PORTD.RD1==1)) {A=1; B=0; C=0; D=0; E=0; while( PORTD.RD1 == 1 ){}}  // B -> Cancel -> A
  if ((B==1)&&(PORTD.RD3==1)) {A=0; B=0; C=1; D=0; E=0; while( PORTD.RD3 == 1 ){}}  // B -> > -> C
  if ((C==1)&&(PORTD.RD0==1)) {A=0; B=0; C=0; D=0; E=1; while( PORTD.RD0 == 1 ){}}   // C -> OK -> E
  if ((C==1)&&(PORTD.RD1==1)) {A=1; B=0; C=0; D=0; E=0; while( PORTD.RD1 == 1 ){}}   // C -> Cancel -> A
  if ((C==1)&&(PORTD.RD2==1)) {A=0; B=1; C=0; D=0; E=0; while( PORTD.RD2 == 1 ){}}   // C -> < -> B
  if ((D==1)&&(PORTD.RD2==1)) {A=0; B=1; C=0; D=0; E=0; while( PORTD.RD2 == 1 ){}}   // D -> < -> B
  if ((E==1)&&(PORTD.RD2==1)) {A=0; B=0; C=1; D=0; E=0; while( PORTD.RD2 == 1 ){}}   // E -> < -> C

 
  //sectiunea de reconfigurare a iesirilor
  if (A==1) { Lcd_Out(1,1,"  Seteaza RB4 "); Lcd_Out(2,1,"       OK      ");}
  if (B==1) { Lcd_Out(1,1,"  Aprinde LED "); Lcd_Out(2,1,"Cancel | OK |  >");}
  if (C==1) { Lcd_Out(1,1,"  Stinge LED  "); Lcd_Out(2,1,"<  | OK | Cancel");}
  if (D==1) { PORTD.RD4 = 1; Lcd_Out(1,1,"  LED Aprins  "); Lcd_Out(2,1,"   Back <       ");}
  if (E==1) { PORTD.RD4 = 0; Lcd_Out(1,1,"  LED Stins  ");  Lcd_Out(2,1,"   Back <       ");}
 }


 
 
 

 
}
