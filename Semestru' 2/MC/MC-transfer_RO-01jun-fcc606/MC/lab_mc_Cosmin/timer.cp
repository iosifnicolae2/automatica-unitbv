#line 1 "D:/studenti/4493/nitaCosmin/timer.c"
sbit LCD_RS at RB4_bit;
sbit LCD_EN at RB5_bit;
sbit LCD_D4 at RB0_bit;
sbit LCD_D5 at RB1_bit;
sbit LCD_D6 at RB2_bit;
sbit LCD_D7 at RB3_bit;

sbit LCD_RS_Direction at TRISB4_bit;
sbit LCD_EN_Direction at TRISB5_bit;
sbit LCD_D4_Direction at TRISB0_bit;
sbit LCD_D5_Direction at TRISB1_bit;
sbit LCD_D6_Direction at TRISB2_bit;
sbit LCD_D7_Direction at TRISB3_bit;



int secunde = 0;
int count = 0;
char sir[20];
void interrupt()
{
 INTCON.GIE = 0;
 if(INTCON.T0IF)
 {
 count++;
 INTCON.T0IF = 0;
 if(count == 1000)
 {
 count = 0;
 secunde ++;
 }
 TMR0 = 5;
 }
 INTCON.GIE = 1;
}
void main()
{
 TRISB = 0;
 PORTB = 0b00000000;
 TMR0 = 5;

 INTCON = 0b11100000;
 OPTION_REG = 0b00000000;
 OPTION_REG.PSA = 1;
 Lcd_Init();
 Lcd_Cmd(_LCD_CURSOR_OFF);
 while(1)
 {
 sprinti(sir,"secunde = %d",secunde);
 Lcd_Out(1,1,sir);
 }
}
