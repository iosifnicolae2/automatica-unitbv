
_interrupt:
	MOVWF      R15+0
	SWAPF      STATUS+0, 0
	CLRF       STATUS+0
	MOVWF      ___saveSTATUS+0
	MOVF       PCLATH+0, 0
	MOVWF      ___savePCLATH+0
	CLRF       PCLATH+0
;lab5.c,7 :: 		void interrupt()
;lab5.c,9 :: 		INTCON.GIE = 0;
	BCF        INTCON+0, 7
;lab5.c,10 :: 		if (INTCON.INTF)
	BTFSS      INTCON+0, 1
	GOTO       L_interrupt0
;lab5.c,12 :: 		buttonPressed = 1;
	MOVLW      1
	MOVWF      _buttonPressed+0
;lab5.c,13 :: 		INTCON.INTF = 0;
	BCF        INTCON+0, 1
;lab5.c,14 :: 		}
L_interrupt0:
;lab5.c,15 :: 		INTCON.GIE = 1;
	BSF        INTCON+0, 7
;lab5.c,16 :: 		}
L__interrupt8:
	MOVF       ___savePCLATH+0, 0
	MOVWF      PCLATH+0
	SWAPF      ___saveSTATUS+0, 0
	MOVWF      STATUS+0
	SWAPF      R15+0, 1
	SWAPF      R15+0, 0
	RETFIE
; end of _interrupt

_main:
;lab5.c,17 :: 		void main()
;lab5.c,19 :: 		TRISA = 0x0;
	CLRF       TRISA+0
;lab5.c,20 :: 		TRISB = 0xff;
	MOVLW      255
	MOVWF      TRISB+0
;lab5.c,21 :: 		TRISD = 0x0;
	CLRF       TRISD+0
;lab5.c,22 :: 		nr[0] = 0b11111100;
	MOVLW      252
	MOVWF      _nr+0
;lab5.c,23 :: 		nr[1] = 0b01100000;
	MOVLW      96
	MOVWF      _nr+1
;lab5.c,24 :: 		nr[2] = 0b11011010;
	MOVLW      218
	MOVWF      _nr+2
;lab5.c,25 :: 		nr[3] = 0b11110010;
	MOVLW      242
	MOVWF      _nr+3
;lab5.c,26 :: 		nr[4] = 0b00100110;
	MOVLW      38
	MOVWF      _nr+4
;lab5.c,27 :: 		nr[5] = 0b10110110;
	MOVLW      182
	MOVWF      _nr+5
;lab5.c,28 :: 		nr[6] = 0b10111110;
	MOVLW      190
	MOVWF      _nr+6
;lab5.c,29 :: 		nr[7] = 0b11100000;
	MOVLW      224
	MOVWF      _nr+7
;lab5.c,30 :: 		nr[8] = 0b11111110;
	MOVLW      254
	MOVWF      _nr+8
;lab5.c,31 :: 		nr[9] = 0b11100110;
	MOVLW      230
	MOVWF      _nr+9
;lab5.c,33 :: 		PORTD = 0b00000000;
	CLRF       PORTD+0
;lab5.c,34 :: 		PORTA = 0b00001001;
	MOVLW      9
	MOVWF      PORTA+0
;lab5.c,35 :: 		INTCON.GIE = 1 ;
	BSF        INTCON+0, 7
;lab5.c,36 :: 		INTCON.INTE = 1;
	BSF        INTCON+0, 4
;lab5.c,37 :: 		while(1)
L_main1:
;lab5.c,39 :: 		if(buttonPressed)
	MOVF       _buttonPressed+0, 0
	BTFSC      STATUS+0, 2
	GOTO       L_main3
;lab5.c,41 :: 		PORTA = 0b00001001;
	MOVLW      9
	MOVWF      PORTA+0
;lab5.c,43 :: 		for(  i = 9; i >= 0; i--)
	MOVLW      9
	MOVWF      _i+0
	MOVLW      0
	MOVWF      _i+1
L_main4:
	MOVLW      0
	SUBWF      _i+1, 0
	BTFSS      STATUS+0, 2
	GOTO       L__main9
	MOVLW      0
	SUBWF      _i+0, 0
L__main9:
	BTFSS      STATUS+0, 0
	GOTO       L_main5
;lab5.c,45 :: 		PORTD = nr[i];
	MOVF       _i+0, 0
	ADDLW      _nr+0
	MOVWF      FSR
	MOVF       INDF+0, 0
	MOVWF      PORTD+0
;lab5.c,46 :: 		Delay_ms(1000);
	MOVLW      11
	MOVWF      R11+0
	MOVLW      38
	MOVWF      R12+0
	MOVLW      93
	MOVWF      R13+0
L_main7:
	DECFSZ     R13+0, 1
	GOTO       L_main7
	DECFSZ     R12+0, 1
	GOTO       L_main7
	DECFSZ     R11+0, 1
	GOTO       L_main7
	NOP
	NOP
;lab5.c,43 :: 		for(  i = 9; i >= 0; i--)
	MOVLW      1
	SUBWF      _i+0, 1
	BTFSS      STATUS+0, 0
	DECF       _i+1, 1
;lab5.c,47 :: 		}
	GOTO       L_main4
L_main5:
;lab5.c,48 :: 		PORTA = 0b00100001;
	MOVLW      33
	MOVWF      PORTA+0
;lab5.c,49 :: 		buttonPressed = 0;
	CLRF       _buttonPressed+0
;lab5.c,50 :: 		}
L_main3:
;lab5.c,51 :: 		}
	GOTO       L_main1
;lab5.c,53 :: 		}
	GOTO       $+0
; end of _main
