#define false 0
#define true 1

char nr[9];
short buttonPressed = 0;
unsigned int i = 0;
void interrupt()
{
    INTCON.GIE = 0;
    if (INTCON.INTF)
    {
        buttonPressed = 1;
        INTCON.INTF = 0;
    }
    INTCON.GIE = 1;
}
void main()
{
   TRISA = 0x0;
   TRISB = 0xff;
   TRISD = 0x0;
    nr[0] = 0b11111100;
    nr[1] = 0b01100000;
    nr[2] = 0b11011010;
    nr[3] = 0b11110010;
    nr[4] = 0b00100110;
    nr[5] = 0b10110110;
    nr[6] = 0b10111110;
    nr[7] = 0b11100000;
    nr[8] = 0b11111110;
    nr[9] = 0b11100110;

    PORTD = 0b00000000;
    PORTA = 0b00001001;
    INTCON.GIE = 1 ;
    INTCON.INTE = 1;
    while(1)
    {
         if(buttonPressed)
         {
             PORTA = 0b00001001;

             for(  i=9;i>=0; i--)
             {
                 PORTD = nr[i];
                 Delay_ms(1000);
             }
             PORTA = 0b00100001;
             buttonPressed = 0;
         }
    }
    
}