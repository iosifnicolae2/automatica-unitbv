
_main:
;lab_lcd.c,20 :: 		void main()
;lab_lcd.c,22 :: 		numar = 5;
	MOVLW      5
	MOVWF      _numar+0
	MOVLW      0
	MOVWF      _numar+1
;lab_lcd.c,23 :: 		TRISB = 0;
	CLRF       TRISB+0
;lab_lcd.c,24 :: 		PORTB = 0xFF;
	MOVLW      255
	MOVWF      PORTB+0
;lab_lcd.c,25 :: 		TRISB = 0xff;
	MOVLW      255
	MOVWF      TRISB+0
;lab_lcd.c,27 :: 		Lcd_Init();                        // Initialize LCD
	CALL       _Lcd_Init+0
;lab_lcd.c,29 :: 		Lcd_Cmd(_LCD_CLEAR);                // Clear display
	MOVLW      1
	MOVWF      FARG_Lcd_Cmd_out_char+0
	CALL       _Lcd_Cmd+0
;lab_lcd.c,33 :: 		while(1)
L_main0:
;lab_lcd.c,36 :: 		sprinti(text, "X = %d", numar);
	MOVLW      _text+0
	MOVWF      FARG_sprinti_wh+0
	MOVLW      ?lstr_1_lab_lcd+0
	MOVWF      FARG_sprinti_f+0
	MOVLW      hi_addr(?lstr_1_lab_lcd+0
	MOVWF      FARG_sprinti_f+1
	MOVF       _numar+0, 0
	MOVWF      FARG_sprinti_wh+3
	MOVF       _numar+1, 0
	MOVWF      FARG_sprinti_wh+4
	CALL       _sprinti+0
;lab_lcd.c,37 :: 		Lcd_Out(1,1,text);
	MOVLW      1
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      1
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      _text+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
;lab_lcd.c,38 :: 		numar++;
	INCF       _numar+0, 1
	BTFSC      STATUS+0, 2
	INCF       _numar+1, 1
;lab_lcd.c,39 :: 		delay_ms(1000);
	MOVLW      11
	MOVWF      R11+0
	MOVLW      38
	MOVWF      R12+0
	MOVLW      93
	MOVWF      R13+0
L_main2:
	DECFSZ     R13+0, 1
	GOTO       L_main2
	DECFSZ     R12+0, 1
	GOTO       L_main2
	DECFSZ     R11+0, 1
	GOTO       L_main2
	NOP
	NOP
;lab_lcd.c,41 :: 		}
	GOTO       L_main0
;lab_lcd.c,43 :: 		}
	GOTO       $+0
; end of _main
