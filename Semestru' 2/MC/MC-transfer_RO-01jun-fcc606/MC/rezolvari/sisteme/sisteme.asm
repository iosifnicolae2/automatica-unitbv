
_main:

;sisteme.c,1 :: 		void main() {
;sisteme.c,6 :: 		TRISD=0b00000111;
	MOVLW      7
	MOVWF      TRISD+0
;sisteme.c,7 :: 		TRISA=0;
	CLRF       TRISA+0
;sisteme.c,10 :: 		a=1;b=0;c=0;
	MOVLW      1
	MOVWF      R1+0
	CLRF       R2+0
	CLRF       R3+0
;sisteme.c,12 :: 		while(1)
L_main0:
;sisteme.c,14 :: 		if ((a==1)&&(PORTD.RD0==1)) {a=0; b=1;}
	MOVF       R1+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main4
	BTFSS      PORTD+0, 0
	GOTO       L_main4
L__main16:
	CLRF       R1+0
	MOVLW      1
	MOVWF      R2+0
L_main4:
;sisteme.c,15 :: 		if ((b==1)&&(PORTD.RD2==1)) {b=0; c=1;}
	MOVF       R2+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main7
	BTFSS      PORTD+0, 2
	GOTO       L_main7
L__main15:
	CLRF       R2+0
	MOVLW      1
	MOVWF      R3+0
L_main7:
;sisteme.c,16 :: 		if ((c==1)&&(PORTD.RD1==1)) {c=0; a=1;}
	MOVF       R3+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main10
	BTFSS      PORTD+0, 1
	GOTO       L_main10
L__main14:
	CLRF       R3+0
	MOVLW      1
	MOVWF      R1+0
L_main10:
;sisteme.c,18 :: 		if (a==1) {PORTA.RA3=0; PORTA.RA4=0;}
	MOVF       R1+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main11
	BCF        PORTA+0, 3
	BCF        PORTA+0, 4
L_main11:
;sisteme.c,19 :: 		if (b==1) {PORTA.RA3=1; PORTA.RA4=0;}
	MOVF       R2+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main12
	BSF        PORTA+0, 3
	BCF        PORTA+0, 4
L_main12:
;sisteme.c,20 :: 		if (c==1) {PORTA.RA3=0; PORTA.RA4=1;}
	MOVF       R3+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main13
	BCF        PORTA+0, 3
	BSF        PORTA+0, 4
L_main13:
;sisteme.c,21 :: 		}
	GOTO       L_main0
;sisteme.c,23 :: 		}
	GOTO       $+0
; end of _main
