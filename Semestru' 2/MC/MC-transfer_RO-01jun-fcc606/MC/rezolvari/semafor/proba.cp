#line 1 "D:/cursuri/An3/sem2/MC/rezolvari/proba.c"




sbit LCD_RS at RC7_bit;
sbit LCD_EN at RC6_bit;
sbit LCD_D4 at RC4_bit;
sbit LCD_D5 at RC5_bit;
sbit LCD_D6 at RC1_bit;
sbit LCD_D7 at RC0_bit;

sbit LCD_RS_Direction at TRISC7_bit;
sbit LCD_EN_Direction at TRISC6_bit;
sbit LCD_D4_Direction at TRISC4_bit;
sbit LCD_D5_Direction at TRISC5_bit;
sbit LCD_D6_Direction at TRISC1_bit;
sbit LCD_D7_Direction at TRISC0_bit;

short buttonPressed = 0;


void interrupt()
{ int i;
 INTCON.GIE=0;

 if (INTCON.INTF)
 {
 buttonPressed=1;


 INTCON.INTF=0;
 }
 INTCON.GIE=1;
}


int i;
void main() {
 Lcd_init();
 Lcd_Cmd(_LCD_CLEAR);
 Lcd_Cmd(_LCD_CURSOR_OFF);

 Lcd_out(1,1,"Asteptati culoarea");
 Lcd_out(2,2,"verde a semaforului!");

 TRISD=0;
 TRISA=0;
 TRISC=0;
 TRISB = 0xff;

 INTCON.GIE = 1 ;
 INTCON.INTE = 1;

while(1){
 TRISD=0;
 PORTD=0b00100001;
 Delay_ms(3000);
 PORTD=0b00010001;
 Delay_ms(4000);
 PORTD=0b00001100;

 for(i=9;i>=0;i--)
 {
 Lcd_Cmd(_LCD_CLEAR);
 Lcd_out(1,1,"Puteti traversa!");
 PORTA=i;
 Delay_ms(1000);

}
 Lcd_Cmd(_LCD_CLEAR);
 Lcd_out(1,1,"Asteptati culoarea");
 Lcd_out(2,2,"verde a semaforului!");
 }
 if(buttonPressed)
 { PORTD=0;
 Delay_ms(500);
 PORTD=0b00001100;

 for(i=9;i>=0;i--)
 {

 PORTA=i;
 Delay_ms(1000);

 }
 buttonPressed=0;
 }


}
