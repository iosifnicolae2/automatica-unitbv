
_main:
;lab3.c,20 :: 		void main(){
;lab3.c,23 :: 		Lcd_Init();
	CALL       _Lcd_Init+0
;lab3.c,24 :: 		Lcd_Cmd(_LCD_CLEAR);                // Clear display
	MOVLW      1
	MOVWF      FARG_Lcd_Cmd_out_char+0
	CALL       _Lcd_Cmd+0
;lab3.c,25 :: 		Lcd_Cmd(_LCD_CURSOR_OFF);
	MOVLW      12
	MOVWF      FARG_Lcd_Cmd_out_char+0
	CALL       _Lcd_Cmd+0
;lab3.c,26 :: 		Lcd_Out(1,1,"Cata are");
	MOVLW      1
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      1
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      ?lstr1_lab3+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
;lab3.c,27 :: 		while(1){
L_main0:
;lab3.c,31 :: 		for(i=0;i<=9;i++)
	CLRF       main_i_L0+0
	CLRF       main_i_L0+1
L_main2:
	MOVLW      128
	MOVWF      R0+0
	MOVLW      128
	XORWF      main_i_L0+1, 0
	SUBWF      R0+0, 0
	BTFSS      STATUS+0, 2
	GOTO       L__main6
	MOVF       main_i_L0+0, 0
	SUBLW      9
L__main6:
	BTFSS      STATUS+0, 0
	GOTO       L_main3
;lab3.c,33 :: 		Delay_ms(500) ;
	MOVLW      6
	MOVWF      R11+0
	MOVLW      19
	MOVWF      R12+0
	MOVLW      173
	MOVWF      R13+0
L_main5:
	DECFSZ     R13+0, 1
	GOTO       L_main5
	DECFSZ     R12+0, 1
	GOTO       L_main5
	DECFSZ     R11+0, 1
	GOTO       L_main5
	NOP
	NOP
;lab3.c,34 :: 		sprinti(buffer,"%d",i);
	MOVLW      main_buffer_L0+0
	MOVWF      FARG_sprinti_wh+0
	MOVLW      ?lstr_2_lab3+0
	MOVWF      FARG_sprinti_f+0
	MOVLW      hi_addr(?lstr_2_lab3+0
	MOVWF      FARG_sprinti_f+1
	MOVF       main_i_L0+0, 0
	MOVWF      FARG_sprinti_wh+3
	MOVF       main_i_L0+1, 0
	MOVWF      FARG_sprinti_wh+4
	CALL       _sprinti+0
;lab3.c,35 :: 		Lcd_Out(2,2,buffer);
	MOVLW      2
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      2
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      main_buffer_L0+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
;lab3.c,31 :: 		for(i=0;i<=9;i++)
	INCF       main_i_L0+0, 1
	BTFSC      STATUS+0, 2
	INCF       main_i_L0+1, 1
;lab3.c,36 :: 		}                }
	GOTO       L_main2
L_main3:
	GOTO       L_main0
;lab3.c,39 :: 		}
	GOTO       $+0
; end of _main
