sbit LCD_RS at RB4_bit;
sbit LCD_EN at RB5_bit;
sbit LCD_D4 at RB0_bit;
sbit LCD_D5 at RB1_bit;
sbit LCD_D6 at RB2_bit;
sbit LCD_D7 at RB3_bit;

sbit LCD_RS_Direction at TRISB4_bit;
sbit LCD_EN_Direction at TRISB5_bit;
sbit LCD_D4_Direction at TRISB0_bit;
sbit LCD_D5_Direction at TRISB1_bit;
sbit LCD_D6_Direction at TRISB2_bit;
sbit LCD_D7_Direction at TRISB3_bit;


void init_adc()
{
ANSEL=255;
ADCON0.adfm=0;
ADCON0.VCFG1=0;
ADCON0.VCFG0=0;
ADCON0.CHS2=0;
ADCON0.CHS1=0;
ADCON0.CHS0=0;
ADCON0.GO_DONE=0;
ADCON0.ADON=0;
ADCON0.ADCS2=0;
ADCON0.ADCS1=0;
ADCON0.ADCS0=0;
trisb=0b00000000;
portb=0;
}


int read_adc()
{
int result;
ADCON0.ADON=1;
ADCON0.GO_DONE=1;
 while(ADCON0.GO_DONE)
 {}
 ADCON0.GO_DONE=0;

result|=ADRESL>>6|ADRESH<<2;
return result;
}










void main() {

int a=0;
int b=0;
char sir[12];
 Lcd_Init();
 init_adc();
 while(1)
 {
 a=read_adc();
 b=5*a/1024;
 sprinti(sir,"temp=%d",b);
 Lcd_Out(1,1,sir);
 delay_ms(1000);
 }
 






}
