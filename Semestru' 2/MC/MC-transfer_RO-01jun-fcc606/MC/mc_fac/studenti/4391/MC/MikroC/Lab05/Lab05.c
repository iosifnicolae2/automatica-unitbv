// LCD module connections
sbit LCD_RS at RD4_bit;
sbit LCD_EN at RD5_bit;
sbit LCD_D4 at RD0_bit;
sbit LCD_D5 at RD1_bit;
sbit LCD_D6 at RD2_bit;
sbit LCD_D7 at RD3_bit;

sbit LCD_RS_Direction at TRISD4_bit;
sbit LCD_EN_Direction at TRISD5_bit;
sbit LCD_D4_Direction at TRISD0_bit;
sbit LCD_D5_Direction at TRISD1_bit;
sbit LCD_D6_Direction at TRISD2_bit;
sbit LCD_D7_Direction at TRISD3_bit;
// End LCD module connections

//Pseudonime
char txset[] = "     SET RB4    ";
char txon[]  = "   APRINDE RB4  ";
char txoff[] = "   STINGE RB4   ";
char txapr[] = "   RB4 APRINS   ";
char txsti[] = "    RB4 STINS   ";
char txok[]  = "       OK       ";
char txcan[] = "   OK    CANCEL ";

//Declarare Stari
short SA;
short SB;
short SC;
short SD;
short SE;

bit oldstateB0;
bit oldstateB1;
bit oldstateB2;

void main() {

//Initializare Registri si Porturi
TRISB.RB0 = 1;
TRISB.RB1 = 1;
TRISB.RB2 = 1;

TRISB.RB4 = 0;
PORTB.RB4 = 0;

//LCD Display
Lcd_Init();
Lcd_Cmd(_LCD_CLEAR);
Lcd_Cmd(_LCD_CURSOR_OFF);
Lcd_Out(1,1,txset);
Lcd_Out(2,1,txok);

//Starile initiale
SA=1;
SB=0;
SC=0;
SD=0;
SE=0;

oldstateB0 = 0;
oldstateB1 = 0;
oldstateB2 = 0;

while(1)
{
 // OK Button
 if(PORTB.RB0 == 1)
    oldstateB0 = 1;
  if(oldstateB0 == 1 && PORTB.RB0 == 0)
  {
    if(SA){ SA = 0; SB = 1; Lcd_Cmd(_LCD_CLEAR); Lcd_Out(1,1,txon) ; Lcd_Out(2,1,txcan);} // A->B
    else
      if(SB){ SB = 0; SD = 1; Lcd_Cmd(_LCD_CLEAR); Lcd_Out(1,1,txapr); Lcd_Out(2,1,txok); PORTB.RB4=1;} // B->D
      else
        if(SD){ SD = 0; SA = 1; Lcd_Cmd(_LCD_CLEAR); Lcd_Out(1,1,txset); Lcd_Out(2,1,txok);} // D->A
    if(SC){ SC = 0; SE = 1; Lcd_Cmd(_LCD_CLEAR); Lcd_Out(1,1,txsti); Lcd_Out(2,1,txok); PORTB.RB4=0;} // C->E
    else
      if(SE){ SE = 0; SA = 1; Lcd_Cmd(_LCD_CLEAR); Lcd_Out(1,1,txset); Lcd_Out(2,1,txok);} // E->A
    oldstateB0 = 0;
  }
  
  // Cancel Button
  if(PORTB.RB1 == 1)
    oldstateB1 = 1;
  if(oldstateB1 == 1 && PORTB.RB1 == 0)
  {
    if(SB || SC){ SB = 0; SC = 0; SA = 1; Lcd_Out(1,1,txset); Lcd_Out(2,1,txok);} // B/C->A
    oldstateB1 = 0;
  }

  // <> Button
  if(PORTB.RB2 == 1)
    oldstateB2 = 1;
  if(oldstateB2 == 1 && PORTB.RB2 == 0)
  {
    if(SB){ SB = 0; SC = 1; Lcd_Cmd(_LCD_CLEAR); Lcd_Out(1,1,txoff); Lcd_Out(2,1,txcan);} // B->C
    else
      if(SC){ SC = 0; SB = 1; Lcd_Cmd(_LCD_CLEAR); Lcd_Out(1,1,txon) ; Lcd_Out(2,1,txcan);} // C->B
    oldstateB2 = 0;
  }
  
 }
}
