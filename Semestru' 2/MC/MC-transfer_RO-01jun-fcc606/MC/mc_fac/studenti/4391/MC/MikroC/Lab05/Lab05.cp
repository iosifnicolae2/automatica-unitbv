#line 1 "S:/My Stuff/Working Space/MikroC/Lab05/Lab05.c"

sbit LCD_RS at RD4_bit;
sbit LCD_EN at RD5_bit;
sbit LCD_D4 at RD0_bit;
sbit LCD_D5 at RD1_bit;
sbit LCD_D6 at RD2_bit;
sbit LCD_D7 at RD3_bit;

sbit LCD_RS_Direction at TRISD4_bit;
sbit LCD_EN_Direction at TRISD5_bit;
sbit LCD_D4_Direction at TRISD0_bit;
sbit LCD_D5_Direction at TRISD1_bit;
sbit LCD_D6_Direction at TRISD2_bit;
sbit LCD_D7_Direction at TRISD3_bit;



char txset[] = "     SET RB4    ";
char txon[] = "   APRINDE RB4  ";
char txoff[] = "   STINGE RB4   ";
char txapr[] = "   RB4 APRINS   ";
char txsti[] = "    RB4 STINS   ";
char txok[] = "       OK       ";
char txcan[] = "   OK    CANCEL ";


short SA;
short SB;
short SC;
short SD;
short SE;

bit oldstateB0;
bit oldstateB1;
bit oldstateB2;

void main() {


TRISB.RB0 = 1;
TRISB.RB1 = 1;
TRISB.RB2 = 1;

TRISB.RB4 = 0;
PORTB.RB4 = 0;


Lcd_Init();
Lcd_Cmd(_LCD_CLEAR);
Lcd_Cmd(_LCD_CURSOR_OFF);
Lcd_Out(1,1,txset);
Lcd_Out(2,1,txok);


SA=1;
SB=0;
SC=0;
SD=0;
SE=0;

oldstateB0 = 0;
oldstateB1 = 0;
oldstateB2 = 0;

while(1)
{

 if(PORTB.RB0 == 1)
 oldstateB0 = 1;
 if(oldstateB0 == 1 && PORTB.RB0 == 0)
 {
 if(SA){ SA = 0; SB = 1; Lcd_Cmd(_LCD_CLEAR); Lcd_Out(1,1,txon) ; Lcd_Out(2,1,txcan);}
 else
 if(SB){ SB = 0; SD = 1; Lcd_Cmd(_LCD_CLEAR); Lcd_Out(1,1,txapr); Lcd_Out(2,1,txok); PORTB.RB4=1;}
 else
 if(SD){ SD = 0; SA = 1; Lcd_Cmd(_LCD_CLEAR); Lcd_Out(1,1,txset); Lcd_Out(2,1,txok);}
 if(SC){ SC = 0; SE = 1; Lcd_Cmd(_LCD_CLEAR); Lcd_Out(1,1,txsti); Lcd_Out(2,1,txok); PORTB.RB4=0;}
 else
 if(SE){ SE = 0; SA = 1; Lcd_Cmd(_LCD_CLEAR); Lcd_Out(1,1,txset); Lcd_Out(2,1,txok);}
 oldstateB0 = 0;
 }


 if(PORTB.RB1 == 1)
 oldstateB1 = 1;
 if(oldstateB1 == 1 && PORTB.RB1 == 0)
 {
 if(SB || SC){ SB = 0; SC = 0; SA = 1; Lcd_Out(1,1,txset); Lcd_Out(2,1,txok);}
 oldstateB1 = 0;
 }


 if(PORTB.RB2 == 1)
 oldstateB2 = 1;
 if(oldstateB2 == 1 && PORTB.RB2 == 0)
 {
 if(SB){ SB = 0; SC = 1; Lcd_Cmd(_LCD_CLEAR); Lcd_Out(1,1,txoff); Lcd_Out(2,1,txcan);}
 else
 if(SC){ SC = 0; SB = 1; Lcd_Cmd(_LCD_CLEAR); Lcd_Out(1,1,txon) ; Lcd_Out(2,1,txcan);}
 oldstateB2 = 0;
 }

 }
}
