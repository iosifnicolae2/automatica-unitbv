
_main:
;Lab5.c,15 :: 		void main() {
;Lab5.c,17 :: 		Lcd_Init();
	CALL       _Lcd_Init+0
;Lab5.c,18 :: 		TRISB=0b00001111;
	MOVLW      15
	MOVWF      TRISB+0
;Lab5.c,19 :: 		A=1;
	MOVLW      1
	MOVWF      main_A_L0+0
;Lab5.c,20 :: 		B=0;
	CLRF       main_B_L0+0
;Lab5.c,21 :: 		C=0;
	CLRF       main_C_L0+0
;Lab5.c,22 :: 		D=0;
	CLRF       main_D_L0+0
;Lab5.c,23 :: 		E=0;
	CLRF       main_E_L0+0
;Lab5.c,24 :: 		while(1)
L_main0:
;Lab5.c,25 :: 		{       if ((A==1)&&(PORTB.RB0==1)) {A=0;B=1;}
	MOVF       main_A_L0+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main4
	BTFSS      PORTB+0, 0
	GOTO       L_main4
L__main42:
	CLRF       main_A_L0+0
	MOVLW      1
	MOVWF      main_B_L0+0
L_main4:
;Lab5.c,26 :: 		if ((B==1)&&(PORTB.RB1==1)) {B=0;C=1;}
	MOVF       main_B_L0+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main7
	BTFSS      PORTB+0, 1
	GOTO       L_main7
L__main41:
	CLRF       main_B_L0+0
	MOVLW      1
	MOVWF      main_C_L0+0
L_main7:
;Lab5.c,27 :: 		if ((B==1)&&(PORTB.RB3==1)) {B=0;A=1;}
	MOVF       main_B_L0+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main10
	BTFSS      PORTB+0, 3
	GOTO       L_main10
L__main40:
	CLRF       main_B_L0+0
	MOVLW      1
	MOVWF      main_A_L0+0
L_main10:
;Lab5.c,28 :: 		if ((B==1)&&(PORTB.RB0==1)) {B=0;D=1;}
	MOVF       main_B_L0+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main13
	BTFSS      PORTB+0, 0
	GOTO       L_main13
L__main39:
	CLRF       main_B_L0+0
	MOVLW      1
	MOVWF      main_D_L0+0
L_main13:
;Lab5.c,29 :: 		if ((C==1)&&(PORTB.RB0==1)) {C=0;E=1;}
	MOVF       main_C_L0+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main16
	BTFSS      PORTB+0, 0
	GOTO       L_main16
L__main38:
	CLRF       main_C_L0+0
	MOVLW      1
	MOVWF      main_E_L0+0
L_main16:
;Lab5.c,30 :: 		if ((C==1)&&(PORTB.RB2==1)) {C=0;B=1;}
	MOVF       main_C_L0+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main19
	BTFSS      PORTB+0, 2
	GOTO       L_main19
L__main37:
	CLRF       main_C_L0+0
	MOVLW      1
	MOVWF      main_B_L0+0
L_main19:
;Lab5.c,31 :: 		if ((C==1)&&(PORTB.RB3==1)) {C=0;A=1;}
	MOVF       main_C_L0+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main22
	BTFSS      PORTB+0, 3
	GOTO       L_main22
L__main36:
	CLRF       main_C_L0+0
	MOVLW      1
	MOVWF      main_A_L0+0
L_main22:
;Lab5.c,32 :: 		if ((D==1)&&(PORTB.RB0==1)) {D=0;A=1;}
	MOVF       main_D_L0+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main25
	BTFSS      PORTB+0, 0
	GOTO       L_main25
L__main35:
	CLRF       main_D_L0+0
	MOVLW      1
	MOVWF      main_A_L0+0
L_main25:
;Lab5.c,33 :: 		if ((E==1)&&(PORTB.RB0==1)) {E=0;A=1;}
	MOVF       main_E_L0+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main28
	BTFSS      PORTB+0, 0
	GOTO       L_main28
L__main34:
	CLRF       main_E_L0+0
	MOVLW      1
	MOVWF      main_A_L0+0
L_main28:
;Lab5.c,35 :: 		if (A==1) { Lcd_Out(1,1,"Setare RB4:"); Lcd_Out(2,1,"Ok");};
	MOVF       main_A_L0+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main29
	MOVLW      1
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      1
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      ?lstr1_Lab5+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
	MOVLW      2
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      1
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      ?lstr2_Lab5+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
L_main29:
;Lab5.c,36 :: 		if (B==1) { Lcd_Out(1,1,"Aprindere RB4:"); Lcd_Out(2,1,"Ok > Cancel");};
	MOVF       main_B_L0+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main30
	MOVLW      1
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      1
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      ?lstr3_Lab5+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
	MOVLW      2
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      1
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      ?lstr4_Lab5+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
L_main30:
;Lab5.c,37 :: 		if (C==1) { Lcd_Out(1,1,"Stingere RB4:"); Lcd_Out(2,1,"Ok < Cancel");};
	MOVF       main_C_L0+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main31
	MOVLW      1
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      1
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      ?lstr5_Lab5+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
	MOVLW      2
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      1
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      ?lstr6_Lab5+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
L_main31:
;Lab5.c,38 :: 		if (D==1) { Lcd_Out(1,1,"RB4=1:"); Lcd_Out(2,1,"Ok"); PORTB.RB4=1; };
	MOVF       main_D_L0+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main32
	MOVLW      1
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      1
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      ?lstr7_Lab5+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
	MOVLW      2
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      1
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      ?lstr8_Lab5+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
	BSF        PORTB+0, 4
L_main32:
;Lab5.c,39 :: 		if (E==1) { Lcd_Out(1,1,"RB4=0:"); Lcd_Out(2,1,"Ok"); PORTB.RB4=0; };
	MOVF       main_E_L0+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main33
	MOVLW      1
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      1
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      ?lstr9_Lab5+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
	MOVLW      2
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      1
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      ?lstr10_Lab5+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
	BCF        PORTB+0, 4
L_main33:
;Lab5.c,40 :: 		}
	GOTO       L_main0
;Lab5.c,41 :: 		}
	GOTO       $+0
; end of _main
