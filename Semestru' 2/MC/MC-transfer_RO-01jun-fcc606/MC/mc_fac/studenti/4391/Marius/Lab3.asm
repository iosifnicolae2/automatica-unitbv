
_main:
;Lab3.c,1 :: 		void main() {
;Lab3.c,6 :: 		TRISD.RD0=0;
	BCF        TRISD+0, 0
;Lab3.c,7 :: 		TRISD.RD1=0;
	BCF        TRISD+0, 1
;Lab3.c,8 :: 		TRISD.RD2=0;
	BCF        TRISD+0, 2
;Lab3.c,9 :: 		TRISD.RD3=0;
	BCF        TRISD+0, 3
;Lab3.c,10 :: 		TRISD.RD4=0;
	BCF        TRISD+0, 4
;Lab3.c,11 :: 		TRISD.RD5=0;
	BCF        TRISD+0, 5
;Lab3.c,12 :: 		TRISD.RD6=1;
	BSF        TRISD+0, 6
;Lab3.c,17 :: 		while(1){
L_main0:
;Lab3.c,19 :: 		PORTD.RD0=1;
	BSF        PORTD+0, 0
;Lab3.c,20 :: 		PORTD.RD1=0;
	BCF        PORTD+0, 1
;Lab3.c,21 :: 		PORTD.RD2=0;
	BCF        PORTD+0, 2
;Lab3.c,23 :: 		PORTD.RD3=0;
	BCF        PORTD+0, 3
;Lab3.c,24 :: 		PORTD.RD4=0;
	BCF        PORTD+0, 4
;Lab3.c,25 :: 		PORTD.RD5=1;
	BSF        PORTD+0, 5
;Lab3.c,28 :: 		if (PORTD.RD6==0)
	BTFSC      PORTD+0, 6
	GOTO       L_main2
;Lab3.c,31 :: 		delay_ms(4000);
	MOVLW      6
	MOVWF      R11+0
	MOVLW      19
	MOVWF      R12+0
	MOVLW      173
	MOVWF      R13+0
L_main3:
	DECFSZ     R13+0, 1
	GOTO       L_main3
	DECFSZ     R12+0, 1
	GOTO       L_main3
	DECFSZ     R11+0, 1
	GOTO       L_main3
	NOP
	NOP
;Lab3.c,33 :: 		PORTD.RD0=0;
	BCF        PORTD+0, 0
;Lab3.c,34 :: 		PORTD.RD1=1;
	BSF        PORTD+0, 1
;Lab3.c,35 :: 		PORTD.RD2=0;
	BCF        PORTD+0, 2
;Lab3.c,36 :: 		PORTD.RD3=0;
	BCF        PORTD+0, 3
;Lab3.c,37 :: 		PORTD.RD4=1;
	BSF        PORTD+0, 4
;Lab3.c,38 :: 		PORTD.RD5=0;
	BCF        PORTD+0, 5
;Lab3.c,40 :: 		delay_ms(2000);
	MOVLW      3
	MOVWF      R11+0
	MOVLW      138
	MOVWF      R12+0
	MOVLW      85
	MOVWF      R13+0
L_main4:
	DECFSZ     R13+0, 1
	GOTO       L_main4
	DECFSZ     R12+0, 1
	GOTO       L_main4
	DECFSZ     R11+0, 1
	GOTO       L_main4
	NOP
	NOP
;Lab3.c,42 :: 		PORTD.RD0=0;
	BCF        PORTD+0, 0
;Lab3.c,43 :: 		PORTD.RD1=0;
	BCF        PORTD+0, 1
;Lab3.c,44 :: 		PORTD.RD2=1;
	BSF        PORTD+0, 2
;Lab3.c,45 :: 		PORTD.RD3=1;
	BSF        PORTD+0, 3
;Lab3.c,46 :: 		PORTD.RD4=0;
	BCF        PORTD+0, 4
;Lab3.c,47 :: 		PORTD.RD5=0;
	BCF        PORTD+0, 5
;Lab3.c,49 :: 		delay_ms(4000);
	MOVLW      6
	MOVWF      R11+0
	MOVLW      19
	MOVWF      R12+0
	MOVLW      173
	MOVWF      R13+0
L_main5:
	DECFSZ     R13+0, 1
	GOTO       L_main5
	DECFSZ     R12+0, 1
	GOTO       L_main5
	DECFSZ     R11+0, 1
	GOTO       L_main5
	NOP
	NOP
;Lab3.c,50 :: 		}
L_main2:
;Lab3.c,52 :: 		}
	GOTO       L_main0
;Lab3.c,53 :: 		}
	GOTO       $+0
; end of _main
