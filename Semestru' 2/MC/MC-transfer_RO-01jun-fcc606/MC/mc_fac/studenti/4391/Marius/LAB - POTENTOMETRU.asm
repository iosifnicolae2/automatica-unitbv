
_main:
;LAB - POTENTOMETRU.c,25 :: 		void main() {
;LAB - POTENTOMETRU.c,26 :: 		Lcd_Init();
	CALL       _Lcd_Init+0
;LAB - POTENTOMETRU.c,27 :: 		TRISA=1;
	MOVLW      1
	MOVWF      TRISA+0
;LAB - POTENTOMETRU.c,29 :: 		PORTA.AN0=1;
	BSF        PORTA+0, 0
;LAB - POTENTOMETRU.c,31 :: 		while(1){
L_main0:
;LAB - POTENTOMETRU.c,35 :: 		tmp = ADC_Read(0);
	CLRF       FARG_ADC_Read_channel+0
	CALL       _ADC_Read+0
	MOVF       R0+0, 0
	MOVWF      _tmp+0
	MOVF       R0+1, 0
	MOVWF      _tmp+1
	CLRF       _tmp+2
	CLRF       _tmp+3
;LAB - POTENTOMETRU.c,37 :: 		tmp=tmp*5000/1203;
	MOVF       _tmp+0, 0
	MOVWF      R0+0
	MOVF       _tmp+1, 0
	MOVWF      R0+1
	MOVF       _tmp+2, 0
	MOVWF      R0+2
	MOVF       _tmp+3, 0
	MOVWF      R0+3
	MOVLW      136
	MOVWF      R4+0
	MOVLW      19
	MOVWF      R4+1
	CLRF       R4+2
	CLRF       R4+3
	CALL       _Mul_32x32_U+0
	MOVLW      179
	MOVWF      R4+0
	MOVLW      4
	MOVWF      R4+1
	CLRF       R4+2
	CLRF       R4+3
	CALL       _Div_32x32_U+0
	MOVF       R0+0, 0
	MOVWF      _tmp+0
	MOVF       R0+1, 0
	MOVWF      _tmp+1
	MOVF       R0+2, 0
	MOVWF      _tmp+2
	MOVF       R0+3, 0
	MOVWF      _tmp+3
;LAB - POTENTOMETRU.c,39 :: 		sir[4]='\0';
	CLRF       _sir+4
;LAB - POTENTOMETRU.c,40 :: 		sir[3]=tmp%10+'0';
	MOVLW      10
	MOVWF      R4+0
	CLRF       R4+1
	CLRF       R4+2
	CLRF       R4+3
	CALL       _Div_32x32_U+0
	MOVF       R8+0, 0
	MOVWF      R0+0
	MOVF       R8+1, 0
	MOVWF      R0+1
	MOVF       R8+2, 0
	MOVWF      R0+2
	MOVF       R8+3, 0
	MOVWF      R0+3
	MOVLW      48
	ADDWF      R0+0, 0
	MOVWF      _sir+3
;LAB - POTENTOMETRU.c,41 :: 		tmp=tmp/10;
	MOVLW      10
	MOVWF      R4+0
	CLRF       R4+1
	CLRF       R4+2
	CLRF       R4+3
	MOVF       _tmp+0, 0
	MOVWF      R0+0
	MOVF       _tmp+1, 0
	MOVWF      R0+1
	MOVF       _tmp+2, 0
	MOVWF      R0+2
	MOVF       _tmp+3, 0
	MOVWF      R0+3
	CALL       _Div_32x32_U+0
	MOVF       R0+0, 0
	MOVWF      _tmp+0
	MOVF       R0+1, 0
	MOVWF      _tmp+1
	MOVF       R0+2, 0
	MOVWF      _tmp+2
	MOVF       R0+3, 0
	MOVWF      _tmp+3
;LAB - POTENTOMETRU.c,42 :: 		sir[2]=tmp%10+'0';
	MOVLW      10
	MOVWF      R4+0
	CLRF       R4+1
	CLRF       R4+2
	CLRF       R4+3
	CALL       _Div_32x32_U+0
	MOVF       R8+0, 0
	MOVWF      R0+0
	MOVF       R8+1, 0
	MOVWF      R0+1
	MOVF       R8+2, 0
	MOVWF      R0+2
	MOVF       R8+3, 0
	MOVWF      R0+3
	MOVLW      48
	ADDWF      R0+0, 0
	MOVWF      _sir+2
;LAB - POTENTOMETRU.c,43 :: 		tmp=tmp/10;
	MOVLW      10
	MOVWF      R4+0
	CLRF       R4+1
	CLRF       R4+2
	CLRF       R4+3
	MOVF       _tmp+0, 0
	MOVWF      R0+0
	MOVF       _tmp+1, 0
	MOVWF      R0+1
	MOVF       _tmp+2, 0
	MOVWF      R0+2
	MOVF       _tmp+3, 0
	MOVWF      R0+3
	CALL       _Div_32x32_U+0
	MOVF       R0+0, 0
	MOVWF      _tmp+0
	MOVF       R0+1, 0
	MOVWF      _tmp+1
	MOVF       R0+2, 0
	MOVWF      _tmp+2
	MOVF       R0+3, 0
	MOVWF      _tmp+3
;LAB - POTENTOMETRU.c,44 :: 		sir[1]=tmp%10+'0';
	MOVLW      10
	MOVWF      R4+0
	CLRF       R4+1
	CLRF       R4+2
	CLRF       R4+3
	CALL       _Div_32x32_U+0
	MOVF       R8+0, 0
	MOVWF      R0+0
	MOVF       R8+1, 0
	MOVWF      R0+1
	MOVF       R8+2, 0
	MOVWF      R0+2
	MOVF       R8+3, 0
	MOVWF      R0+3
	MOVLW      48
	ADDWF      R0+0, 0
	MOVWF      _sir+1
;LAB - POTENTOMETRU.c,45 :: 		tmp=tmp/10;
	MOVLW      10
	MOVWF      R4+0
	CLRF       R4+1
	CLRF       R4+2
	CLRF       R4+3
	MOVF       _tmp+0, 0
	MOVWF      R0+0
	MOVF       _tmp+1, 0
	MOVWF      R0+1
	MOVF       _tmp+2, 0
	MOVWF      R0+2
	MOVF       _tmp+3, 0
	MOVWF      R0+3
	CALL       _Div_32x32_U+0
	MOVF       R0+0, 0
	MOVWF      _tmp+0
	MOVF       R0+1, 0
	MOVWF      _tmp+1
	MOVF       R0+2, 0
	MOVWF      _tmp+2
	MOVF       R0+3, 0
	MOVWF      _tmp+3
;LAB - POTENTOMETRU.c,46 :: 		sir[0]=tmp%10+'0';
	MOVLW      10
	MOVWF      R4+0
	CLRF       R4+1
	CLRF       R4+2
	CLRF       R4+3
	CALL       _Div_32x32_U+0
	MOVF       R8+0, 0
	MOVWF      R0+0
	MOVF       R8+1, 0
	MOVWF      R0+1
	MOVF       R8+2, 0
	MOVWF      R0+2
	MOVF       R8+3, 0
	MOVWF      R0+3
	MOVLW      48
	ADDWF      R0+0, 0
	MOVWF      _sir+0
;LAB - POTENTOMETRU.c,50 :: 		Lcd_Out(1,1,sir);
	MOVLW      1
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      1
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      _sir+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
;LAB - POTENTOMETRU.c,53 :: 		}
	GOTO       L_main0
;LAB - POTENTOMETRU.c,56 :: 		}
	GOTO       $+0
; end of _main
