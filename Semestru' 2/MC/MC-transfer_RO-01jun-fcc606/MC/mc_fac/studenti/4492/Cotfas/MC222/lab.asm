
_main:
	CLRF       main_i_L0+0
	CLRF       main_i_L0+1
	MOVLW      63
	MOVWF      main_sir_L0+0
	MOVLW      249
	MOVWF      main_sir_L0+1
	MOVLW      109
	MOVWF      main_sir_L0+2
	MOVLW      121
	MOVWF      main_sir_L0+3
	MOVLW      82
	MOVWF      main_sir_L0+4
	MOVLW      91
	MOVWF      main_sir_L0+5
	MOVLW      95
	MOVWF      main_sir_L0+6
	MOVLW      49
	MOVWF      main_sir_L0+7
	MOVLW      127
	MOVWF      main_sir_L0+8
	MOVLW      123
	MOVWF      main_sir_L0+9
;lab.c,1 :: 		void main()
;lab.c,5 :: 		TRISD=0;
	CLRF       TRISD+0
;lab.c,6 :: 		TRISB=1;
	MOVLW      1
	MOVWF      TRISB+0
;lab.c,7 :: 		TRISC=0;
	CLRF       TRISC+0
;lab.c,8 :: 		PORTC=0;
	CLRF       PORTC+0
;lab.c,9 :: 		INTCON=0b10010000;
	MOVLW      144
	MOVWF      INTCON+0
;lab.c,11 :: 		while(1)
L_main0:
;lab.c,13 :: 		PORTD=sir[i];
	MOVF       main_i_L0+0, 0
	ADDLW      main_sir_L0+0
	MOVWF      FSR
	MOVF       INDF+0, 0
	MOVWF      PORTD+0
;lab.c,14 :: 		i++;
	INCF       main_i_L0+0, 1
	BTFSC      STATUS+0, 2
	INCF       main_i_L0+1, 1
;lab.c,15 :: 		if(i==9)
	MOVLW      0
	XORWF      main_i_L0+1, 0
	BTFSS      STATUS+0, 2
	GOTO       L__main5
	MOVLW      9
	XORWF      main_i_L0+0, 0
L__main5:
	BTFSS      STATUS+0, 2
	GOTO       L_main2
;lab.c,16 :: 		i=0;
	CLRF       main_i_L0+0
	CLRF       main_i_L0+1
L_main2:
;lab.c,17 :: 		Delay_ms(1000);
	MOVLW      11
	MOVWF      R11+0
	MOVLW      38
	MOVWF      R12+0
	MOVLW      93
	MOVWF      R13+0
L_main3:
	DECFSZ     R13+0, 1
	GOTO       L_main3
	DECFSZ     R12+0, 1
	GOTO       L_main3
	DECFSZ     R11+0, 1
	GOTO       L_main3
	NOP
	NOP
;lab.c,18 :: 		}
	GOTO       L_main0
;lab.c,20 :: 		}
	GOTO       $+0
; end of _main

_interrupt:
	MOVWF      R15+0
	SWAPF      STATUS+0, 0
	CLRF       STATUS+0
	MOVWF      ___saveSTATUS+0
	MOVF       PCLATH+0, 0
	MOVWF      ___savePCLATH+0
	CLRF       PCLATH+0
;lab.c,22 :: 		void interrupt()
;lab.c,25 :: 		if(INTCON.INTE==1)
	BTFSS      INTCON+0, 4
	GOTO       L_interrupt4
;lab.c,27 :: 		PORTC=!PORTC;
	MOVF       PORTC+0, 0
	MOVLW      1
	BTFSS      STATUS+0, 2
	MOVLW      0
	MOVWF      PORTC+0
;lab.c,28 :: 		INTCON.INTF=0;
	BCF        INTCON+0, 1
;lab.c,29 :: 		}
L_interrupt4:
;lab.c,31 :: 		}
L__interrupt6:
	MOVF       ___savePCLATH+0, 0
	MOVWF      PCLATH+0
	SWAPF      ___saveSTATUS+0, 0
	MOVWF      STATUS+0
	SWAPF      R15+0, 1
	SWAPF      R15+0, 0
	RETFIE
; end of _interrupt
