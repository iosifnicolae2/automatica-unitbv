
_main:
	CLRF       main_i_L0+0
	CLRF       main_i_L0+1
	MOVLW      63
	MOVWF      main_sir_L0+0
	MOVLW      249
	MOVWF      main_sir_L0+1
	MOVLW      109
	MOVWF      main_sir_L0+2
	MOVLW      121
	MOVWF      main_sir_L0+3
	MOVLW      82
	MOVWF      main_sir_L0+4
	MOVLW      91
	MOVWF      main_sir_L0+5
	MOVLW      95
	MOVWF      main_sir_L0+6
	MOVLW      49
	MOVWF      main_sir_L0+7
	MOVLW      127
	MOVWF      main_sir_L0+8
	MOVLW      123
	MOVWF      main_sir_L0+9
;lab.c,1 :: 		void main()
;lab.c,5 :: 		TRISD=0;
	CLRF       TRISD+0
;lab.c,6 :: 		TRISB=1;
	MOVLW      1
	MOVWF      TRISB+0
;lab.c,8 :: 		while(1)
L_main0:
;lab.c,11 :: 		for ( i = 0; i<10 ; i++)
	CLRF       main_i_L0+0
	CLRF       main_i_L0+1
L_main2:
	MOVLW      128
	XORWF      main_i_L0+1, 0
	MOVWF      R0+0
	MOVLW      128
	SUBWF      R0+0, 0
	BTFSS      STATUS+0, 2
	GOTO       L__main8
	MOVLW      10
	SUBWF      main_i_L0+0, 0
L__main8:
	BTFSC      STATUS+0, 0
	GOTO       L_main3
;lab.c,15 :: 		if(PORTB.RB0==1)
	BTFSS      PORTB+0, 0
	GOTO       L_main5
;lab.c,16 :: 		PORTD=sir[i];
	MOVF       main_i_L0+0, 0
	ADDLW      main_sir_L0+0
	MOVWF      FSR
	MOVF       INDF+0, 0
	MOVWF      PORTD+0
L_main5:
;lab.c,17 :: 		while(PORTB.RB0=1) {}
L_main6:
	BSF        PORTB+0, 0
	BTFSS      PORTB+0, 0
	GOTO       L_main7
	GOTO       L_main6
L_main7:
;lab.c,11 :: 		for ( i = 0; i<10 ; i++)
	INCF       main_i_L0+0, 1
	BTFSC      STATUS+0, 2
	INCF       main_i_L0+1, 1
;lab.c,18 :: 		}
	GOTO       L_main2
L_main3:
;lab.c,20 :: 		}
	GOTO       L_main0
;lab.c,22 :: 		}
	GOTO       $+0
; end of _main
