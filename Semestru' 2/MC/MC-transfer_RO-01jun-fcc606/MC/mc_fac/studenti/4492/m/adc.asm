
_initializare:
;adc.c,17 :: 		void initializare()
;adc.c,19 :: 		TRISA=0b11111111;
	MOVLW      255
	MOVWF      TRISA+0
;adc.c,20 :: 		ANSEL=255;
	MOVLW      255
	MOVWF      ANSEL+0
;adc.c,21 :: 		ADCON0.ADFM=0;
	BCF        ADCON0+0, 7
;adc.c,22 :: 		ADCON0.VCFG1=0;
	BCF        ADCON0+0, 6
;adc.c,23 :: 		ADCON0.VCFG0=0;
	BCF        ADCON0+0, 5
;adc.c,24 :: 		ADCON0.CHS2=0;
	BCF        ADCON0+0, 4
;adc.c,25 :: 		ADCON0.CHS1=0;
	BCF        ADCON0+0, 3
;adc.c,26 :: 		ADCON0.CHS0=0;
	BCF        ADCON0+0, 2
;adc.c,27 :: 		ADCON0.GO_DONE=0;
	BCF        ADCON0+0, 1
;adc.c,28 :: 		ADCON0.ADON=0;
	BCF        ADCON0+0, 0
;adc.c,29 :: 		ADCON1.ADCS2=0;
	BCF        ADCON1+0, 6
;adc.c,30 :: 		ADCON1.ADCS1=0;
	BCF        ADCON1+0, 5
;adc.c,31 :: 		ADCON1.ADCS0=1;
	BSF        ADCON1+0, 4
;adc.c,32 :: 		}
	RETURN
; end of _initializare

_citire_ADC:
	CLRF       citire_ADC_conv_L0+0
	CLRF       citire_ADC_conv_L0+1
;adc.c,35 :: 		int citire_ADC()
;adc.c,38 :: 		ADCON0.ADON=1;
	BSF        ADCON0+0, 0
;adc.c,39 :: 		Delay_ms(1);
	MOVLW      3
	MOVWF      R12+0
	MOVLW      151
	MOVWF      R13+0
L_citire_ADC0:
	DECFSZ     R13+0, 1
	GOTO       L_citire_ADC0
	DECFSZ     R12+0, 1
	GOTO       L_citire_ADC0
	NOP
	NOP
;adc.c,40 :: 		ADCON0.GO_DONE=1;
	BSF        ADCON0+0, 1
;adc.c,41 :: 		while (ADCON0.GO_DONE){}
L_citire_ADC1:
	BTFSS      ADCON0+0, 1
	GOTO       L_citire_ADC2
	GOTO       L_citire_ADC1
L_citire_ADC2:
;adc.c,42 :: 		ADCON0.ADON=0;
	BCF        ADCON0+0, 0
;adc.c,43 :: 		conv|=ADRESH<<2|ADRESL>>6;
	MOVF       ADRESH+0, 0
	MOVWF      R2+0
	CLRF       R2+1
	RLF        R2+0, 1
	RLF        R2+1, 1
	BCF        R2+0, 0
	RLF        R2+0, 1
	RLF        R2+1, 1
	BCF        R2+0, 0
	MOVLW      6
	MOVWF      R1+0
	MOVF       ADRESL+0, 0
	MOVWF      R0+0
	MOVF       R1+0, 0
L__citire_ADC6:
	BTFSC      STATUS+0, 2
	GOTO       L__citire_ADC7
	RRF        R0+0, 1
	BCF        R0+0, 7
	ADDLW      255
	GOTO       L__citire_ADC6
L__citire_ADC7:
	MOVLW      0
	MOVWF      R0+1
	MOVF       R2+0, 0
	IORWF      R0+0, 1
	MOVF       R2+1, 0
	IORWF      R0+1, 1
	MOVF       citire_ADC_conv_L0+0, 0
	IORWF      R0+0, 1
	MOVF       citire_ADC_conv_L0+1, 0
	IORWF      R0+1, 1
	MOVF       R0+0, 0
	MOVWF      citire_ADC_conv_L0+0
	MOVF       R0+1, 0
	MOVWF      citire_ADC_conv_L0+1
;adc.c,44 :: 		return conv;
;adc.c,46 :: 		}
	RETURN
; end of _citire_ADC

_main:
;adc.c,48 :: 		void main()
;adc.c,52 :: 		LCD_init();
	CALL       _Lcd_Init+0
;adc.c,53 :: 		initializare();
	CALL       _initializare+0
;adc.c,54 :: 		while(1)
L_main3:
;adc.c,56 :: 		rezistor=citire_ADC();
	CALL       _citire_ADC+0
;adc.c,57 :: 		sprinti(sir,"val conv=%d  ",rezistor);
	MOVLW      main_sir_L0+0
	MOVWF      FARG_sprinti_wh+0
	MOVLW      ?lstr_1_adc+0
	MOVWF      FARG_sprinti_f+0
	MOVLW      hi_addr(?lstr_1_adc+0
	MOVWF      FARG_sprinti_f+1
	MOVF       R0+0, 0
	MOVWF      FARG_sprinti_wh+3
	MOVF       R0+1, 0
	MOVWF      FARG_sprinti_wh+4
	CALL       _sprinti+0
;adc.c,58 :: 		LCD_out(1,1,sir);
	MOVLW      1
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      1
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      main_sir_L0+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
;adc.c,59 :: 		Delay_ms(100);
	MOVLW      2
	MOVWF      R11+0
	MOVLW      4
	MOVWF      R12+0
	MOVLW      186
	MOVWF      R13+0
L_main5:
	DECFSZ     R13+0, 1
	GOTO       L_main5
	DECFSZ     R12+0, 1
	GOTO       L_main5
	DECFSZ     R11+0, 1
	GOTO       L_main5
	NOP
;adc.c,60 :: 		}
	GOTO       L_main3
;adc.c,61 :: 		}
	GOTO       $+0
; end of _main
