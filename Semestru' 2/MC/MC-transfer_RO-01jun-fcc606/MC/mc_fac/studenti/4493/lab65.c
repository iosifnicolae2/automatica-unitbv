int i=0;
int sec=0;
char str[10];
// LCD module connections
sbit LCD_RS at RB4_bit;
sbit LCD_EN at RB5_bit;
sbit LCD_D4 at RB0_bit;
sbit LCD_D5 at RB1_bit;
sbit LCD_D6 at RB2_bit;
sbit LCD_D7 at RB3_bit;

sbit LCD_RS_Direction at TRISB4_bit;
sbit LCD_EN_Direction at TRISB5_bit;
sbit LCD_D4_Direction at TRISB0_bit;
sbit LCD_D5_Direction at TRISB1_bit;
sbit LCD_D6_Direction at TRISB2_bit;
sbit LCD_D7_Direction at TRISB3_bit;
// End LCD module connections


void interrupt()
{
INTCON.GIE=0;
if (INTCON,T0IF)
   {INTCON.T0IF=0;
   i++;
   if (i==1000)
      {i=0;
      sec++;
      }
   }
INTCON.GIE=1;
}


void main()
{
TRISB=0B00000000;
PORTB=0B00000000;
INTCON=0B11100000;
OPTION_REG=0B00010000;
LCD_INIT();
while(1)
        {
        sprinti(str,"sec=%d",sec);
         LCD_OUT(1,1,str);

         }
}

