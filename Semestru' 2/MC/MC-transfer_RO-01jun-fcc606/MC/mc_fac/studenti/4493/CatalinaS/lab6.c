char sir[20];
int secunde =0;
unsigned int count=0;
sbit LCD_RS at RB4_bit;
sbit LCD_EN at RB5_bit;
sbit LCD_D4 at RB0_bit;
sbit LCD_D5 at RB1_bit;
sbit LCD_D6 at RB2_bit;
sbit LCD_D7 at RB3_bit;

sbit LCD_RS_Direction at TRISB4_bit;
sbit LCD_EN_Direction at TRISB5_bit;
sbit LCD_D4_Direction at TRISB0_bit;
sbit LCD_D5_Direction at TRISB1_bit;
sbit LCD_D6_Direction at TRISB2_bit;
sbit LCD_D7_Direction at TRISB3_bit;


void main() {
     Lcd_Cmd(_LCD_CLEAR);                // Clear display
  Lcd_Cmd(_LCD_CURSOR_OFF);
TMR0=5;
OPTION_REG=0b00000000;
INTCON=0b11100000;
TRISB=0b00000000;
PORTB=0b00000000;
Lcd_init();
while(1)
{sprinti(sir,"Secunde=%d",secunde);
Lcd_Out(1,1,sir);
}
}

void interrupt()
{INTCON.GIE=0;
if(INTCON.T0IF)
{INTCON.T0IF=0;
count++;
if(count==500)
{secunde++;
 count = 0;
}TMR0=5;

}
INTCON.GIE=1;
}
