
_intrerrupt:
;Lab06.c,21 :: 		void intrerrupt()
;Lab06.c,23 :: 		INTCON.GIE=0;
	BCF        INTCON+0, 7
;Lab06.c,26 :: 		sec++;
	INCF       _sec+0, 1
	BTFSC      STATUS+0, 2
	INCF       _sec+1, 1
;Lab06.c,27 :: 		else sec=0;
L_intrerrupt1:
;Lab06.c,28 :: 		m++;
	INCF       _m+0, 1
	BTFSC      STATUS+0, 2
	INCF       _m+1, 1
;Lab06.c,30 :: 		if(m>59) {
	MOVLW      128
	MOVWF      R0+0
	MOVLW      128
	XORWF      _m+1, 0
	SUBWF      R0+0, 0
	BTFSS      STATUS+0, 2
	GOTO       L__intrerrupt4
	MOVF       _m+0, 0
	SUBLW      59
L__intrerrupt4:
	BTFSC      STATUS+0, 0
	GOTO       L_intrerrupt2
;Lab06.c,31 :: 		h++;
	INCF       _h+0, 1
	BTFSC      STATUS+0, 2
	INCF       _h+1, 1
;Lab06.c,32 :: 		m=0;
	CLRF       _m+0
	CLRF       _m+1
;Lab06.c,33 :: 		}
L_intrerrupt2:
;Lab06.c,34 :: 		if (h>23)
	MOVLW      128
	MOVWF      R0+0
	MOVLW      128
	XORWF      _h+1, 0
	SUBWF      R0+0, 0
	BTFSS      STATUS+0, 2
	GOTO       L__intrerrupt5
	MOVF       _h+0, 0
	SUBLW      23
L__intrerrupt5:
	BTFSC      STATUS+0, 0
	GOTO       L_intrerrupt3
;Lab06.c,36 :: 		h=0;
	CLRF       _h+0
	CLRF       _h+1
;Lab06.c,37 :: 		}
L_intrerrupt3:
;Lab06.c,38 :: 		INTCON.GIE=1;
	BSF        INTCON+0, 7
;Lab06.c,40 :: 		}
	RETURN
; end of _intrerrupt

_main:
;Lab06.c,41 :: 		void main() {
;Lab06.c,42 :: 		Lcd_Init();
	CALL       _Lcd_Init+0
;Lab06.c,44 :: 		Lcd_Cmd(_LCD_CLEAR);
	MOVLW      1
	MOVWF      FARG_Lcd_Cmd_out_char+0
	CALL       _Lcd_Cmd+0
;Lab06.c,45 :: 		OPTION_REG=0b00000101;
	MOVLW      5
	MOVWF      OPTION_REG+0
;Lab06.c,46 :: 		INTCON=0b10100000;
	MOVLW      160
	MOVWF      INTCON+0
;Lab06.c,47 :: 		TMR0=99;
	MOVLW      99
	MOVWF      TMR0+0
;Lab06.c,48 :: 		LCD_chr(1,1,yt);
	MOVLW      1
	MOVWF      FARG_Lcd_Chr_row+0
	MOVLW      1
	MOVWF      FARG_Lcd_Chr_column+0
	MOVF       _yt+0, 0
	MOVWF      FARG_Lcd_Chr_out_char+0
	CALL       _Lcd_Chr+0
;Lab06.c,49 :: 		}
	GOTO       $+0
; end of _main
