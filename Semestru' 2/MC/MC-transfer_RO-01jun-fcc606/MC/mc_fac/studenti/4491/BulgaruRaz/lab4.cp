#line 1 "D:/studenti/4491/BulgaruRaz/lab4.c"
sbit LCD_RS at RD4_bit;
sbit LCD_EN at RD5_bit;
sbit LCD_D4 at RD0_bit;
sbit LCD_D5 at RD1_bit;
sbit LCD_D6 at RD2_bit;
sbit LCD_D7 at RD3_bit;

sbit LCD_RS_Direction at TRISD4_bit;
sbit LCD_EN_Direction at TRISD5_bit;
sbit LCD_D4_Direction at TRISD0_bit;
sbit LCD_D5_Direction at TRISD1_bit;
sbit LCD_D6_Direction at TRISD2_bit;
sbit LCD_D7_Direction at TRISD3_bit;

void main() {
 short A,B,C,D,E;
 Lcd_Init();
 TRISB=0b00001111;
 A=1;
 B=0;
 C=0;
 D=0;
 E=0;
 while(1)
 { if ((A==1)&&(PORTB.RB0==1)) {A=0;B=1;}
 if ((B==1)&&(PORTB.RB1==1)) {B=0;C=1;}
 if ((B==1)&&(PORTB.RB3==1)) {B=0;A=1;}
 if ((B==1)&&(PORTB.RB0==1)) {B=0;D=1;}
 if ((C==1)&&(PORTB.RB0==1)) {C=0;E=1;}
 if ((C==1)&&(PORTB.RB2==1)) {C=0;B=1;}
 if ((C==1)&&(PORTB.RB3==1)) {C=0;A=1;}
 if ((D==1)&&(PORTB.RB0==1)) {D=0;A=1;}
 if ((E==1)&&(PORTB.RB0==1)) {E=0;A=1;}

 if (A==1) { Lcd_Out(1,1,"Setare RB4:"); Lcd_Out(2,1,"Ok"); PORTB.RB0=1; };
 if (B==1) { Lcd_Out(1,1,"Aprindere RB4:"); Lcd_Out(2,1,"Ok > Cancel"); PORTB.RB0=1; PORTB.RB1=1; PORTB.RB3=1; };
 if (C==1) { Lcd_Out(1,1,"Stingere RB4:"); Lcd_Out(2,1,"Ok < Cancel"); PORTB.RB0=1; PORTB.RB2=1; PORTB.RB3=1;};
 if (D==1) { Lcd_Out(1,1,"RB4=1:"); Lcd_Out(2,1,"Ok"); PORTB.RB4=1; };
 if (E==1) { Lcd_Out(1,1,"RB4=0:"); Lcd_Out(2,1,"Ok"); PORTB.RB4=0; };
 }
}
