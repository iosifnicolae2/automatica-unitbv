
_main:
;lab3.c,1 :: 		void main() {
;lab3.c,3 :: 		TRISD=0b00000111;
	MOVLW      7
	MOVWF      TRISD+0
;lab3.c,4 :: 		A=1;
	MOVLW      1
	MOVWF      R1+0
;lab3.c,5 :: 		B=0;
	CLRF       R2+0
;lab3.c,6 :: 		C=0;
	CLRF       R3+0
;lab3.c,7 :: 		while(1)
L_main0:
;lab3.c,8 :: 		{       if ((A==1)&&(PORTD.RD0==1)) {A=0;B=1;}
	MOVF       R1+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main4
	BTFSS      PORTD+0, 0
	GOTO       L_main4
L__main16:
	CLRF       R1+0
	MOVLW      1
	MOVWF      R2+0
L_main4:
;lab3.c,9 :: 		if ((B==1)&&(PORTD.RD1==1)) {B=0;C=1;}
	MOVF       R2+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main7
	BTFSS      PORTD+0, 1
	GOTO       L_main7
L__main15:
	CLRF       R2+0
	MOVLW      1
	MOVWF      R3+0
L_main7:
;lab3.c,10 :: 		if ((C==1)&&(PORTD.RD2==1)) {C=0;A=1;}
	MOVF       R3+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main10
	BTFSS      PORTD+0, 2
	GOTO       L_main10
L__main14:
	CLRF       R3+0
	MOVLW      1
	MOVWF      R1+0
L_main10:
;lab3.c,12 :: 		if (A==1) PORTD=0b00000000;
	MOVF       R1+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main11
	CLRF       PORTD+0
L_main11:
;lab3.c,13 :: 		if (B==1) PORTD=0b00001000;
	MOVF       R2+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main12
	MOVLW      8
	MOVWF      PORTD+0
L_main12:
;lab3.c,14 :: 		if (C==1) PORTD=0b00010000;
	MOVF       R3+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main13
	MOVLW      16
	MOVWF      PORTD+0
L_main13:
;lab3.c,15 :: 		}
	GOTO       L_main0
;lab3.c,16 :: 		}
	GOTO       $+0
; end of _main
