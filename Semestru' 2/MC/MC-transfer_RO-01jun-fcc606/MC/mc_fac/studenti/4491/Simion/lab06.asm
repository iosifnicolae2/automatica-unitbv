
_interrupt:
	MOVWF      R15+0
	SWAPF      STATUS+0, 0
	CLRF       STATUS+0
	MOVWF      ___saveSTATUS+0
	MOVF       PCLATH+0, 0
	MOVWF      ___savePCLATH+0
	CLRF       PCLATH+0
;lab06.c,3 :: 		void interrupt()
;lab06.c,5 :: 		INTCON.GIE=0;
	BCF        INTCON+0, 7
;lab06.c,6 :: 		if(INTCON.T0IF==1)
	BTFSS      INTCON+0, 2
	GOTO       L_interrupt0
;lab06.c,8 :: 		i++;
	INCF       _i+0, 1
	BTFSC      STATUS+0, 2
	INCF       _i+1, 1
;lab06.c,9 :: 		INTCON.T0IF=0;
	BCF        INTCON+0, 2
;lab06.c,10 :: 		i++;
	INCF       _i+0, 1
	BTFSC      STATUS+0, 2
	INCF       _i+1, 1
;lab06.c,11 :: 		TMR0=158;
	MOVLW      158
	MOVWF      TMR0+0
;lab06.c,12 :: 		if(i==20)
	MOVLW      0
	XORWF      _i+1, 0
	BTFSS      STATUS+0, 2
	GOTO       L__interrupt5
	MOVLW      20
	XORWF      _i+0, 0
L__interrupt5:
	BTFSS      STATUS+0, 2
	GOTO       L_interrupt1
;lab06.c,14 :: 		PORTB.RB0=!PORTB.RB0;
	MOVLW      1
	XORWF      PORTB+0, 1
;lab06.c,15 :: 		i=0;
	CLRF       _i+0
	CLRF       _i+1
;lab06.c,16 :: 		}
L_interrupt1:
;lab06.c,17 :: 		}
L_interrupt0:
;lab06.c,18 :: 		INTCON.GIE=1;
	BSF        INTCON+0, 7
;lab06.c,19 :: 		}
L__interrupt4:
	MOVF       ___savePCLATH+0, 0
	MOVWF      PCLATH+0
	SWAPF      ___saveSTATUS+0, 0
	MOVWF      STATUS+0
	SWAPF      R15+0, 1
	SWAPF      R15+0, 0
	RETFIE
; end of _interrupt

_main:
;lab06.c,21 :: 		void main()
;lab06.c,23 :: 		INTCON=0b10100000;
	MOVLW      160
	MOVWF      INTCON+0
;lab06.c,24 :: 		OPTION_REG=0b00000111;
	MOVLW      7
	MOVWF      OPTION_REG+0
;lab06.c,25 :: 		TRISB=0;
	CLRF       TRISB+0
;lab06.c,29 :: 		TMR0=158;
	MOVLW      158
	MOVWF      TMR0+0
;lab06.c,30 :: 		while(1)
L_main2:
;lab06.c,32 :: 		}
	GOTO       L_main2
;lab06.c,33 :: 		}
	GOTO       $+0
; end of _main
