sbit LCD_RS at RB4_bit;
sbit LCD_EN at RB5_bit;
sbit LCD_D4 at RB0_bit;
sbit LCD_D5 at RB1_bit;
sbit LCD_D6 at RB2_bit;
sbit LCD_D7 at RB3_bit;

sbit LCD_RS_Direction at TRISB4_bit;
sbit LCD_EN_Direction at TRISB5_bit;
sbit LCD_D4_Direction at TRISB0_bit;
sbit LCD_D5_Direction at TRISB1_bit;
sbit LCD_D6_Direction at TRISB2_bit;
sbit LCD_D7_Direction at TRISB3_bit;



void init_ADC()
{
ANSEL=255;
ADCON0.ADFM=0;
ADCON0.VCFG1=0;
ADCON0.VCFG0=0;
ADCON0.CHS2=0;
ADCON0.CHS1=0;
ADCON0.CHS0=0;
ADCON0.GO_DONE=0;
ADCON0.ADON=0;
ADCON1.ADCS2=0;
ADCON1.ADCS1=0;
ADCON1.ADCS0=1;
TRISA=255;
}
int READ_ADC()
{ ADCON0.ADON=1;
ADCON0.GO_DONE=1;
while (ADCON0.GO_DONE==1)
ADCON0.ADON=0;
return ADRESH;
}

void main() {
 int conv=0;
 char sir[15];
 LCD_INIT();
 init_ADC();
 while(1)
 {
 conv=READ_ADC();
 sprinti(sir,"conv=%d ",conv);
 LCD_Out(1,1,sir);
 Delay_ms(100);
 }
 
}
