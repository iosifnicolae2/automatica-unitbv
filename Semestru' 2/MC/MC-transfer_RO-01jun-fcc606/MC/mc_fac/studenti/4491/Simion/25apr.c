//VARIANTA FARA VOID INTERRUPT
/*
//2 variabile: 'i' si 'sirul'
  int i;
  char sir[10]={63, 48, 109, 121, 82, 91, 95, 49, 127, 123};

void main()
{
  TRISD=0;  //legam 7 segmente pe portul D, valoarea zero, adica de iesire; toti bitii au valoarea 0
  TRISB.RB0=1;   //butonul, RB0=1 adica bitul 0 (din 8 biti de la 0 la 7) are valoarea 1 (din zecimal e valoarea in binar a unui bit)
  // daca era RB0=4, pe bitul 2 era valoarea 1
   while (1)     //verificam daca butonu e apasat
   {
     if (PORTB.RB0==1)         //apasam butonu
     {
       PORTD = sir[i];      //afisam
       i++;
       if (i==10)
        i=0;
       while(PORTB.RB0==1)      //aici e gresit
         {}
     }
   }
}
*/
////////////////////////////////////////////////////////////////////////
//VARIANTA CU INTERRUPT







void main()
{
   int i=0;
   char sir[10]={63, 48, 109, 121, 82, 91, 95, 49, 127, 123};
   TRISD=0;
   TRISB=1;
   TRISC=0;
   INTCON=0b10010000;
   while(1)
   {
    PORTD=sir[i];
    i++;
    if (i==10)
       i=0;
    Delay_ms(1000);
   }
}

void interrupt()
{
  if(INTCON.INTF==1)
  {
   PORTC =! PORTC;
   INTCON.INTF=0;
   }
}



























