
_init_conv:
;lab10.c,18 :: 		void init_conv()
;lab10.c,20 :: 		ANSEL=0b11111111;
	MOVLW      255
	MOVWF      ANSEL+0
;lab10.c,21 :: 		ADCON0.ADFM=0;
	BCF        ADCON0+0, 7
;lab10.c,22 :: 		ADCON0.VCFG0=0;
	BCF        ADCON0+0, 5
;lab10.c,23 :: 		ADCON0.VCFG1=0;
	BCF        ADCON0+0, 6
;lab10.c,24 :: 		ADCON0.CHS0=0;
	BCF        ADCON0+0, 2
;lab10.c,25 :: 		ADCON0.CHS1=0;
	BCF        ADCON0+0, 3
;lab10.c,26 :: 		ADCON0.CHS2=0;
	BCF        ADCON0+0, 4
;lab10.c,27 :: 		ADCON0.GO_DONE=0;
	BCF        ADCON0+0, 1
;lab10.c,28 :: 		ADCON0.ADON=0;
	BCF        ADCON0+0, 0
;lab10.c,29 :: 		ADCON1.ADCS2=0;
	BCF        ADCON1+0, 6
;lab10.c,30 :: 		ADCON1.ADCS1=0;
	BCF        ADCON1+0, 5
;lab10.c,31 :: 		ADCON1.ADCS0=1;
	BSF        ADCON1+0, 4
;lab10.c,32 :: 		TRISA=255;
	MOVLW      255
	MOVWF      TRISA+0
;lab10.c,33 :: 		}
	RETURN
; end of _init_conv

_read_conv:
	CLRF       read_conv_rez_L0+0
	CLRF       read_conv_rez_L0+1
;lab10.c,35 :: 		int read_conv()
;lab10.c,37 :: 		ADCON0.ADON=1;
	BSF        ADCON0+0, 0
;lab10.c,38 :: 		delay_ms(1);
	MOVLW      83
	MOVWF      R13+0
L_read_conv0:
	DECFSZ     R13+0, 1
	GOTO       L_read_conv0
;lab10.c,39 :: 		ADCON0.GO_DONE=1;
	BSF        ADCON0+0, 1
;lab10.c,40 :: 		while( ADCON0.GO_DONE)   {}
L_read_conv1:
	BTFSS      ADCON0+0, 1
	GOTO       L_read_conv2
	GOTO       L_read_conv1
L_read_conv2:
;lab10.c,41 :: 		ADCON0.ADON=0;
	BCF        ADCON0+0, 0
;lab10.c,42 :: 		rez|=ADRESH<<2|ADRESL>>6;
	MOVF       ADRESH+0, 0
	MOVWF      R2+0
	CLRF       R2+1
	RLF        R2+0, 1
	RLF        R2+1, 1
	BCF        R2+0, 0
	RLF        R2+0, 1
	RLF        R2+1, 1
	BCF        R2+0, 0
	MOVLW      6
	MOVWF      R1+0
	MOVF       ADRESL+0, 0
	MOVWF      R0+0
	MOVF       R1+0, 0
L__read_conv8:
	BTFSC      STATUS+0, 2
	GOTO       L__read_conv9
	RRF        R0+0, 1
	BCF        R0+0, 7
	ADDLW      255
	GOTO       L__read_conv8
L__read_conv9:
	MOVLW      0
	MOVWF      R0+1
	MOVF       R2+0, 0
	IORWF      R0+0, 1
	MOVF       R2+1, 0
	IORWF      R0+1, 1
	MOVF       read_conv_rez_L0+0, 0
	IORWF      R0+0, 1
	MOVF       read_conv_rez_L0+1, 0
	IORWF      R0+1, 1
	MOVF       R0+0, 0
	MOVWF      read_conv_rez_L0+0
	MOVF       R0+1, 0
	MOVWF      read_conv_rez_L0+1
;lab10.c,43 :: 		return rez;
;lab10.c,44 :: 		}
	RETURN
; end of _read_conv

_main:
	CLRF       main_t_L0+0
	CLRF       main_t_L0+1
	CLRF       main_tensiune_L0+0
	CLRF       main_tensiune_L0+1
;lab10.c,46 :: 		void main() {
;lab10.c,49 :: 		Lcd_Init();
	CALL       _Lcd_Init+0
;lab10.c,50 :: 		init_conv();
	CALL       _init_conv+0
;lab10.c,51 :: 		while(1)
L_main3:
;lab10.c,52 :: 		{conversie=read_conv();
	CALL       _read_conv+0
;lab10.c,53 :: 		tensiune=conversie*5;
	MOVLW      5
	MOVWF      R4+0
	MOVLW      0
	MOVWF      R4+1
	CALL       _Mul_16x16_U+0
	MOVF       R0+0, 0
	MOVWF      main_tensiune_L0+0
	MOVF       R0+1, 0
	MOVWF      main_tensiune_L0+1
;lab10.c,55 :: 		t=tensiune%10;
	MOVLW      10
	MOVWF      R4+0
	MOVLW      0
	MOVWF      R4+1
	CALL       _Div_16x16_S+0
	MOVF       R8+0, 0
	MOVWF      R0+0
	MOVF       R8+1, 0
	MOVWF      R0+1
	MOVF       R0+0, 0
	MOVWF      main_t_L0+0
	MOVF       R0+1, 0
	MOVWF      main_t_L0+1
;lab10.c,56 :: 		sprinti(sir1,"tens=%d  ,",tensiune);
	MOVLW      main_sir1_L0+0
	MOVWF      FARG_sprinti_wh+0
	MOVLW      ?lstr_1_lab10+0
	MOVWF      FARG_sprinti_f+0
	MOVLW      hi_addr(?lstr_1_lab10+0
	MOVWF      FARG_sprinti_f+1
	MOVF       main_tensiune_L0+0, 0
	MOVWF      FARG_sprinti_wh+3
	MOVF       main_tensiune_L0+1, 0
	MOVWF      FARG_sprinti_wh+4
	CALL       _sprinti+0
;lab10.c,57 :: 		sprinti(sir2," ",t);
	MOVLW      main_sir2_L0+0
	MOVWF      FARG_sprinti_wh+0
	MOVLW      ?lstr_2_lab10+0
	MOVWF      FARG_sprinti_f+0
	MOVLW      hi_addr(?lstr_2_lab10+0
	MOVWF      FARG_sprinti_f+1
	MOVF       main_t_L0+0, 0
	MOVWF      FARG_sprinti_wh+3
	MOVF       main_t_L0+1, 0
	MOVWF      FARG_sprinti_wh+4
	CALL       _sprinti+0
;lab10.c,59 :: 		delay_ms(100);
	MOVLW      33
	MOVWF      R12+0
	MOVLW      118
	MOVWF      R13+0
L_main5:
	DECFSZ     R13+0, 1
	GOTO       L_main5
	DECFSZ     R12+0, 1
	GOTO       L_main5
	NOP
;lab10.c,60 :: 		Lcd_Out(2,1,sir1);
	MOVLW      2
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      1
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      main_sir1_L0+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
;lab10.c,61 :: 		delay_ms(100);
	MOVLW      33
	MOVWF      R12+0
	MOVLW      118
	MOVWF      R13+0
L_main6:
	DECFSZ     R13+0, 1
	GOTO       L_main6
	DECFSZ     R12+0, 1
	GOTO       L_main6
	NOP
;lab10.c,62 :: 		Lcd_Out(2,6,sir2);
	MOVLW      2
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      6
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      main_sir2_L0+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
;lab10.c,63 :: 		delay_ms(50);
	MOVLW      17
	MOVWF      R12+0
	MOVLW      58
	MOVWF      R13+0
L_main7:
	DECFSZ     R13+0, 1
	GOTO       L_main7
	DECFSZ     R12+0, 1
	GOTO       L_main7
	NOP
;lab10.c,64 :: 		}
	GOTO       L_main3
;lab10.c,65 :: 		}
	GOTO       $+0
; end of _main
