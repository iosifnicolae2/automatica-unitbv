
_main:
	MOVLW      128
	MOVWF      main_sir_L0+0
	MOVLW      0
	MOVWF      main_sir_L0+1
	MOVLW      64
	MOVWF      main_sir_L0+2
	MOVLW      0
	MOVWF      main_sir_L0+3
	MOVLW      32
	MOVWF      main_sir_L0+4
	MOVLW      0
	MOVWF      main_sir_L0+5
	MOVLW      16
	MOVWF      main_sir_L0+6
	MOVLW      0
	MOVWF      main_sir_L0+7
	MOVLW      8
	MOVWF      main_sir_L0+8
	MOVLW      0
	MOVWF      main_sir_L0+9
	MOVLW      4
	MOVWF      main_sir_L0+10
	MOVLW      0
	MOVWF      main_sir_L0+11
	MOVLW      2
	MOVWF      main_sir_L0+12
	MOVLW      0
	MOVWF      main_sir_L0+13
	MOVLW      1
	MOVWF      main_sir_L0+14
	MOVLW      0
	MOVWF      main_sir_L0+15
	CLRF       main_a_L0+0
	CLRF       main_a_L0+1
;lab2.c,1 :: 		void main() {  int a=0;
;lab2.c,3 :: 		TRISB=0;
	CLRF       TRISB+0
;lab2.c,23 :: 		while(1)
L_main0:
;lab2.c,24 :: 		{for(a=0;a<8;a++)
	CLRF       main_a_L0+0
	CLRF       main_a_L0+1
L_main2:
	MOVLW      128
	XORWF      main_a_L0+1, 0
	MOVWF      R0+0
	MOVLW      128
	SUBWF      R0+0, 0
	BTFSS      STATUS+0, 2
	GOTO       L__main6
	MOVLW      8
	SUBWF      main_a_L0+0, 0
L__main6:
	BTFSC      STATUS+0, 0
	GOTO       L_main3
;lab2.c,25 :: 		PORTB=sir[a];
	MOVF       main_a_L0+0, 0
	MOVWF      R0+0
	MOVF       main_a_L0+1, 0
	MOVWF      R0+1
	RLF        R0+0, 1
	RLF        R0+1, 1
	BCF        R0+0, 0
	MOVF       R0+0, 0
	ADDLW      main_sir_L0+0
	MOVWF      FSR
	MOVF       INDF+0, 0
	MOVWF      PORTB+0
;lab2.c,24 :: 		{for(a=0;a<8;a++)
	INCF       main_a_L0+0, 1
	BTFSC      STATUS+0, 2
	INCF       main_a_L0+1, 1
;lab2.c,25 :: 		PORTB=sir[a];
	GOTO       L_main2
L_main3:
;lab2.c,26 :: 		Delay_ms(100);
	MOVLW      2
	MOVWF      R11+0
	MOVLW      4
	MOVWF      R12+0
	MOVLW      186
	MOVWF      R13+0
L_main5:
	DECFSZ     R13+0, 1
	GOTO       L_main5
	DECFSZ     R12+0, 1
	GOTO       L_main5
	DECFSZ     R11+0, 1
	GOTO       L_main5
	NOP
;lab2.c,27 :: 		}
	GOTO       L_main0
;lab2.c,29 :: 		}
	GOTO       $+0
; end of _main
