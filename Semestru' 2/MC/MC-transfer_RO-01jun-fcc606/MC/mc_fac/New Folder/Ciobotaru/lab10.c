// LCD module connections
sbit LCD_RS at RB4_bit;
sbit LCD_EN at RB5_bit;
sbit LCD_D4 at RB0_bit;
sbit LCD_D5 at RB1_bit;
sbit LCD_D6 at RB2_bit;
sbit LCD_D7 at RB3_bit;

sbit LCD_RS_Direction at TRISB4_bit;
sbit LCD_EN_Direction at TRISB5_bit;
sbit LCD_D4_Direction at TRISB0_bit;
sbit LCD_D5_Direction at TRISB1_bit;
sbit LCD_D6_Direction at TRISB2_bit;
sbit LCD_D7_Direction at TRISB3_bit;
// End LCD module connections


void init_conv()
{
 ANSEL=0b11111111;
 ADCON0.ADFM=0;
 ADCON0.VCFG0=0;
 ADCON0.VCFG1=0;
 ADCON0.CHS0=0;
 ADCON0.CHS1=0;
 ADCON0.CHS2=0;
 ADCON0.GO_DONE=0;
 ADCON0.ADON=0;
 ADCON1.ADCS2=0;
 ADCON1.ADCS1=0;
 ADCON1.ADCS0=1;
 TRISA=255;
}

int read_conv()
{int rez=0;
 ADCON0.ADON=1;
 delay_ms(1);
 ADCON0.GO_DONE=1;
 while( ADCON0.GO_DONE)   {}
 ADCON0.ADON=0;
 rez|=ADRESH<<2|ADRESL>>6;
 return rez;
}

void main() {
     char sir[15],sir1[15],sir2[15];
     int conversie=0,tensiune=0,t=0;
     Lcd_Init();
     init_conv();
     while(1)
             {conversie=read_conv();
             tensiune=conversie*5;
             sprinti(sir,"conv=%d  ",conversie);
             t=tensiune%10;
             sprinti(sir1,"tens=%d  ,",tensiune);
              sprinti(sir2," ",t);
             Lcd_Out(1,1,sir);
             delay_ms(100);
             Lcd_Out(2,1,sir1);
             delay_ms(100);
             Lcd_Out(2,6,sir2);
             delay_ms(50);
             }
}


