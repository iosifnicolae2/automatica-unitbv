#line 1 "C:/Documents and Settings/student/My Documents/4391/timer.c"

sbit LCD_RS at Rd4_bit;
sbit LCD_EN at Rd5_bit;
sbit LCD_D7 at Rd3_bit;
sbit LCD_D6 at Rd2_bit;
sbit LCD_D5 at Rd1_bit;
sbit LCD_D4 at Rd0_bit;


sbit LCD_RS_Direction at TRISd4_bit;
sbit LCD_EN_Direction at TRISd5_bit;
sbit LCD_D7_Direction at TRISd3_bit;
sbit LCD_D6_Direction at TRISd2_bit;
sbit LCD_D5_Direction at TRISd1_bit;
sbit LCD_D4_Direction at TRISd0_bit;

short secunde;
short minute;
short ore;
char timer[9];

void timp()
{
secunde++;
if(secunde==60)
{
minute++;
secunde=0;
}
if(minute==60)
{
ore++;
minute=0;
}
if(ore==24)
{
ore=0;
}
}

void afisaj()
{
timer[0]='0'+ ore/10;
timer[1]='0'+ ore%10;
timer[2]=':';
timer[3]='0'+ minute/10;
timer[4]='0'+ minute%10;
timer[5]=':';
timer[6]='0'+ secunde/10;
timer[7]='0'+ secunde%10;
timer[8]='\0';
}


void interrupt(){
if(TMR0IF_bit)
{
TMR0=0;
timp();
}
INTCON.TMR0IF=0;
}


void main() {
 INTCON.GIE = 1;
 INTCON.T0IE = 1;
 TMR0 = 0;
 OPTION_REG = 0b0000110;

 Lcd_Init();
 Lcd_Cmd(_LCD_CLEAR);
 Lcd_Cmd(_LCD_CURSOR_OFF);

 while(1)
 {
 afisaj();
 Lcd_Out(1,5,timer);
 }
}
