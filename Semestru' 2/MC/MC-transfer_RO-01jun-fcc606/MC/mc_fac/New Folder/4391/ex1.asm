
_main:
;ex1.c,1 :: 		void main() {
;ex1.c,4 :: 		trisd.rd0=0;
	BCF        TRISD+0, 0
;ex1.c,5 :: 		trisd.rd1=0;
	BCF        TRISD+0, 1
;ex1.c,6 :: 		trisd.rd2=0;
	BCF        TRISD+0, 2
;ex1.c,7 :: 		trisd.rd3=0;
	BCF        TRISD+0, 3
;ex1.c,8 :: 		trisd.rd4=0;
	BCF        TRISD+0, 4
;ex1.c,9 :: 		trisd.RD5=0;
	BCF        TRISD+0, 5
;ex1.c,14 :: 		while(1){
L_main0:
;ex1.c,15 :: 		portd.rd0=0;
	BCF        PORTD+0, 0
;ex1.c,16 :: 		portd.rd1=0;
	BCF        PORTD+0, 1
;ex1.c,17 :: 		portd.rd2=0;
	BCF        PORTD+0, 2
;ex1.c,18 :: 		portd.rd3=0;
	BCF        PORTD+0, 3
;ex1.c,19 :: 		portd.rd4=0;
	BCF        PORTD+0, 4
;ex1.c,20 :: 		portd.RD5=0;
	BCF        PORTD+0, 5
;ex1.c,22 :: 		portd.rd0=1;
	BSF        PORTD+0, 0
;ex1.c,23 :: 		portd.rd5=1;
	BSF        PORTD+0, 5
;ex1.c,24 :: 		delay_ms(1000);
	MOVLW      2
	MOVWF      R11+0
	MOVLW      69
	MOVWF      R12+0
	MOVLW      169
	MOVWF      R13+0
L_main2:
	DECFSZ     R13+0, 1
	GOTO       L_main2
	DECFSZ     R12+0, 1
	GOTO       L_main2
	DECFSZ     R11+0, 1
	GOTO       L_main2
	NOP
	NOP
;ex1.c,25 :: 		portd.rd0=0;
	BCF        PORTD+0, 0
;ex1.c,26 :: 		portd.RD5=0;
	BCF        PORTD+0, 5
;ex1.c,27 :: 		portd.rd1=1;
	BSF        PORTD+0, 1
;ex1.c,28 :: 		portd.rd4=1;
	BSF        PORTD+0, 4
;ex1.c,29 :: 		delay_ms(1000);
	MOVLW      2
	MOVWF      R11+0
	MOVLW      69
	MOVWF      R12+0
	MOVLW      169
	MOVWF      R13+0
L_main3:
	DECFSZ     R13+0, 1
	GOTO       L_main3
	DECFSZ     R12+0, 1
	GOTO       L_main3
	DECFSZ     R11+0, 1
	GOTO       L_main3
	NOP
	NOP
;ex1.c,30 :: 		portd.rd1=0;
	BCF        PORTD+0, 1
;ex1.c,31 :: 		portd.rd4=0;
	BCF        PORTD+0, 4
;ex1.c,32 :: 		portd.rd2=1;
	BSF        PORTD+0, 2
;ex1.c,33 :: 		portd.rd3=1;
	BSF        PORTD+0, 3
;ex1.c,34 :: 		delay_ms(1000);
	MOVLW      2
	MOVWF      R11+0
	MOVLW      69
	MOVWF      R12+0
	MOVLW      169
	MOVWF      R13+0
L_main4:
	DECFSZ     R13+0, 1
	GOTO       L_main4
	DECFSZ     R12+0, 1
	GOTO       L_main4
	DECFSZ     R11+0, 1
	GOTO       L_main4
	NOP
	NOP
;ex1.c,35 :: 		}
	GOTO       L_main0
;ex1.c,41 :: 		}
	GOTO       $+0
; end of _main
