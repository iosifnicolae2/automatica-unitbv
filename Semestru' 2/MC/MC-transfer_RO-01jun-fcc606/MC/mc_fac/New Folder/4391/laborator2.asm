
_main:
;laborator2.c,1 :: 		void main() {
;laborator2.c,3 :: 		trisd.rd0=0;
	BCF        TRISD+0, 0
;laborator2.c,4 :: 		trisd.rd1=0;
	BCF        TRISD+0, 1
;laborator2.c,5 :: 		trisd.rd2=0;
	BCF        TRISD+0, 2
;laborator2.c,6 :: 		trisd.rd3=0;
	BCF        TRISD+0, 3
;laborator2.c,7 :: 		trisd.rd4=0;
	BCF        TRISD+0, 4
;laborator2.c,8 :: 		trisd.rd5=0;
	BCF        TRISD+0, 5
;laborator2.c,9 :: 		trisd.rd6=1;
	BSF        TRISD+0, 6
;laborator2.c,14 :: 		while(1)
L_main0:
;laborator2.c,16 :: 		portd=0;
	CLRF       PORTD+0
;laborator2.c,19 :: 		if(portd.rd6==0)
	BTFSC      PORTD+0, 6
	GOTO       L_main2
;laborator2.c,21 :: 		portd.rd0=1;
	BSF        PORTD+0, 0
;laborator2.c,22 :: 		portd.rd5=1;
	BSF        PORTD+0, 5
;laborator2.c,24 :: 		}
	GOTO       L_main3
L_main2:
;laborator2.c,27 :: 		portd.rd0=0;
	BCF        PORTD+0, 0
;laborator2.c,28 :: 		portd.rd5=0;
	BCF        PORTD+0, 5
;laborator2.c,30 :: 		portd.rd1=1;
	BSF        PORTD+0, 1
;laborator2.c,31 :: 		portd.rd4=1;
	BSF        PORTD+0, 4
;laborator2.c,32 :: 		delay_ms(1000);
	MOVLW      2
	MOVWF      R11+0
	MOVLW      69
	MOVWF      R12+0
	MOVLW      169
	MOVWF      R13+0
L_main4:
	DECFSZ     R13+0, 1
	GOTO       L_main4
	DECFSZ     R12+0, 1
	GOTO       L_main4
	DECFSZ     R11+0, 1
	GOTO       L_main4
	NOP
	NOP
;laborator2.c,33 :: 		portd.rd1=0;
	BCF        PORTD+0, 1
;laborator2.c,34 :: 		portd.rd4=0;
	BCF        PORTD+0, 4
;laborator2.c,36 :: 		portd.rd2=1;
	BSF        PORTD+0, 2
;laborator2.c,37 :: 		portd.rd3=1;
	BSF        PORTD+0, 3
;laborator2.c,38 :: 		delay_ms(1000);
	MOVLW      2
	MOVWF      R11+0
	MOVLW      69
	MOVWF      R12+0
	MOVLW      169
	MOVWF      R13+0
L_main5:
	DECFSZ     R13+0, 1
	GOTO       L_main5
	DECFSZ     R12+0, 1
	GOTO       L_main5
	DECFSZ     R11+0, 1
	GOTO       L_main5
	NOP
	NOP
;laborator2.c,39 :: 		portd.rd2=0;
	BCF        PORTD+0, 2
;laborator2.c,40 :: 		portd.rd3=0;
	BCF        PORTD+0, 3
;laborator2.c,42 :: 		portd.rd1=1;
	BSF        PORTD+0, 1
;laborator2.c,43 :: 		portd.rd4=1;
	BSF        PORTD+0, 4
;laborator2.c,44 :: 		delay_ms(1000);
	MOVLW      2
	MOVWF      R11+0
	MOVLW      69
	MOVWF      R12+0
	MOVLW      169
	MOVWF      R13+0
L_main6:
	DECFSZ     R13+0, 1
	GOTO       L_main6
	DECFSZ     R12+0, 1
	GOTO       L_main6
	DECFSZ     R11+0, 1
	GOTO       L_main6
	NOP
	NOP
;laborator2.c,45 :: 		}
L_main3:
;laborator2.c,46 :: 		}
	GOTO       L_main0
;laborator2.c,47 :: 		}
	GOTO       $+0
; end of _main
