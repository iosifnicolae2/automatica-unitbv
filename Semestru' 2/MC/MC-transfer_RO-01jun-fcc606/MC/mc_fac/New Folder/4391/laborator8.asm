
_main:
;laborator8.c,18 :: 		void main() {
;laborator8.c,19 :: 		Lcd_init();
	CALL       _Lcd_Init+0
;laborator8.c,22 :: 		while(1)
L_main0:
;laborator8.c,26 :: 		read=ADC_read(0);
	CLRF       FARG_ADC_Read_channel+0
	CALL       _ADC_Read+0
	MOVF       R0+0, 0
	MOVWF      _read+0
	MOVF       R0+1, 0
	MOVWF      _read+1
	CLRF       _read+2
	CLRF       _read+3
;laborator8.c,28 :: 		read=read*5000/1023;
	MOVF       _read+0, 0
	MOVWF      R0+0
	MOVF       _read+1, 0
	MOVWF      R0+1
	MOVF       _read+2, 0
	MOVWF      R0+2
	MOVF       _read+3, 0
	MOVWF      R0+3
	MOVLW      136
	MOVWF      R4+0
	MOVLW      19
	MOVWF      R4+1
	CLRF       R4+2
	CLRF       R4+3
	CALL       _Mul_32x32_U+0
	MOVLW      255
	MOVWF      R4+0
	MOVLW      3
	MOVWF      R4+1
	CLRF       R4+2
	CLRF       R4+3
	CALL       _Div_32x32_U+0
	MOVF       R0+0, 0
	MOVWF      _read+0
	MOVF       R0+1, 0
	MOVWF      _read+1
	MOVF       R0+2, 0
	MOVWF      _read+2
	MOVF       R0+3, 0
	MOVWF      _read+3
;laborator8.c,30 :: 		sir[4]='\0';
	CLRF       _sir+4
;laborator8.c,31 :: 		sir[3]=read%10+'0';
	MOVLW      10
	MOVWF      R4+0
	CLRF       R4+1
	CLRF       R4+2
	CLRF       R4+3
	CALL       _Div_32x32_U+0
	MOVF       R8+0, 0
	MOVWF      R0+0
	MOVF       R8+1, 0
	MOVWF      R0+1
	MOVF       R8+2, 0
	MOVWF      R0+2
	MOVF       R8+3, 0
	MOVWF      R0+3
	MOVLW      48
	ADDWF      R0+0, 0
	MOVWF      _sir+3
;laborator8.c,32 :: 		read=read/10;
	MOVLW      10
	MOVWF      R4+0
	CLRF       R4+1
	CLRF       R4+2
	CLRF       R4+3
	MOVF       _read+0, 0
	MOVWF      R0+0
	MOVF       _read+1, 0
	MOVWF      R0+1
	MOVF       _read+2, 0
	MOVWF      R0+2
	MOVF       _read+3, 0
	MOVWF      R0+3
	CALL       _Div_32x32_U+0
	MOVF       R0+0, 0
	MOVWF      _read+0
	MOVF       R0+1, 0
	MOVWF      _read+1
	MOVF       R0+2, 0
	MOVWF      _read+2
	MOVF       R0+3, 0
	MOVWF      _read+3
;laborator8.c,33 :: 		sir[2]=read%10+'0';
	MOVLW      10
	MOVWF      R4+0
	CLRF       R4+1
	CLRF       R4+2
	CLRF       R4+3
	CALL       _Div_32x32_U+0
	MOVF       R8+0, 0
	MOVWF      R0+0
	MOVF       R8+1, 0
	MOVWF      R0+1
	MOVF       R8+2, 0
	MOVWF      R0+2
	MOVF       R8+3, 0
	MOVWF      R0+3
	MOVLW      48
	ADDWF      R0+0, 0
	MOVWF      _sir+2
;laborator8.c,34 :: 		read=read/10;
	MOVLW      10
	MOVWF      R4+0
	CLRF       R4+1
	CLRF       R4+2
	CLRF       R4+3
	MOVF       _read+0, 0
	MOVWF      R0+0
	MOVF       _read+1, 0
	MOVWF      R0+1
	MOVF       _read+2, 0
	MOVWF      R0+2
	MOVF       _read+3, 0
	MOVWF      R0+3
	CALL       _Div_32x32_U+0
	MOVF       R0+0, 0
	MOVWF      _read+0
	MOVF       R0+1, 0
	MOVWF      _read+1
	MOVF       R0+2, 0
	MOVWF      _read+2
	MOVF       R0+3, 0
	MOVWF      _read+3
;laborator8.c,35 :: 		sir[1]=read%10+'0';
	MOVLW      10
	MOVWF      R4+0
	CLRF       R4+1
	CLRF       R4+2
	CLRF       R4+3
	CALL       _Div_32x32_U+0
	MOVF       R8+0, 0
	MOVWF      R0+0
	MOVF       R8+1, 0
	MOVWF      R0+1
	MOVF       R8+2, 0
	MOVWF      R0+2
	MOVF       R8+3, 0
	MOVWF      R0+3
	MOVLW      48
	ADDWF      R0+0, 0
	MOVWF      _sir+1
;laborator8.c,36 :: 		read=read/10;
	MOVLW      10
	MOVWF      R4+0
	CLRF       R4+1
	CLRF       R4+2
	CLRF       R4+3
	MOVF       _read+0, 0
	MOVWF      R0+0
	MOVF       _read+1, 0
	MOVWF      R0+1
	MOVF       _read+2, 0
	MOVWF      R0+2
	MOVF       _read+3, 0
	MOVWF      R0+3
	CALL       _Div_32x32_U+0
	MOVF       R0+0, 0
	MOVWF      _read+0
	MOVF       R0+1, 0
	MOVWF      _read+1
	MOVF       R0+2, 0
	MOVWF      _read+2
	MOVF       R0+3, 0
	MOVWF      _read+3
;laborator8.c,37 :: 		sir[0]=read+'0';
	MOVLW      48
	ADDWF      R0+0, 0
	MOVWF      _sir+0
;laborator8.c,39 :: 		Lcd_out(1,4,sir);
	MOVLW      1
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      4
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      _sir+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
;laborator8.c,43 :: 		}
	GOTO       L_main0
;laborator8.c,44 :: 		}
	GOTO       $+0
; end of _main
