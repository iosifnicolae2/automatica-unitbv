
_timp:
;timer.c,22 :: 		void timp()
;timer.c,24 :: 		secunde++;
	INCF       _secunde+0, 1
;timer.c,25 :: 		if(secunde==60)
	MOVF       _secunde+0, 0
	XORLW      60
	BTFSS      STATUS+0, 2
	GOTO       L_timp0
;timer.c,27 :: 		minute++;
	INCF       _minute+0, 1
;timer.c,28 :: 		secunde=0;
	CLRF       _secunde+0
;timer.c,29 :: 		}
L_timp0:
;timer.c,30 :: 		if(minute==60)
	MOVF       _minute+0, 0
	XORLW      60
	BTFSS      STATUS+0, 2
	GOTO       L_timp1
;timer.c,32 :: 		ore++;
	INCF       _ore+0, 1
;timer.c,33 :: 		minute=0;
	CLRF       _minute+0
;timer.c,34 :: 		}
L_timp1:
;timer.c,35 :: 		if(ore==24)
	MOVF       _ore+0, 0
	XORLW      24
	BTFSS      STATUS+0, 2
	GOTO       L_timp2
;timer.c,37 :: 		ore=0;
	CLRF       _ore+0
;timer.c,38 :: 		}
L_timp2:
;timer.c,39 :: 		}
	RETURN
; end of _timp

_afisaj:
;timer.c,41 :: 		void afisaj()
;timer.c,43 :: 		timer[0]='0'+ ore/10;
	MOVLW      10
	MOVWF      R4+0
	MOVF       _ore+0, 0
	MOVWF      R0+0
	CALL       _Div_8x8_S+0
	MOVF       R0+0, 0
	ADDLW      48
	MOVWF      _timer+0
;timer.c,44 :: 		timer[1]='0'+ ore%10;
	MOVLW      10
	MOVWF      R4+0
	MOVF       _ore+0, 0
	MOVWF      R0+0
	CALL       _Div_8x8_S+0
	MOVF       R8+0, 0
	MOVWF      R0+0
	MOVF       R0+0, 0
	ADDLW      48
	MOVWF      _timer+1
;timer.c,45 :: 		timer[2]=':';
	MOVLW      58
	MOVWF      _timer+2
;timer.c,46 :: 		timer[3]='0'+ minute/10;
	MOVLW      10
	MOVWF      R4+0
	MOVF       _minute+0, 0
	MOVWF      R0+0
	CALL       _Div_8x8_S+0
	MOVF       R0+0, 0
	ADDLW      48
	MOVWF      _timer+3
;timer.c,47 :: 		timer[4]='0'+ minute%10;
	MOVLW      10
	MOVWF      R4+0
	MOVF       _minute+0, 0
	MOVWF      R0+0
	CALL       _Div_8x8_S+0
	MOVF       R8+0, 0
	MOVWF      R0+0
	MOVF       R0+0, 0
	ADDLW      48
	MOVWF      _timer+4
;timer.c,48 :: 		timer[5]=':';
	MOVLW      58
	MOVWF      _timer+5
;timer.c,49 :: 		timer[6]='0'+ secunde/10;
	MOVLW      10
	MOVWF      R4+0
	MOVF       _secunde+0, 0
	MOVWF      R0+0
	CALL       _Div_8x8_S+0
	MOVF       R0+0, 0
	ADDLW      48
	MOVWF      _timer+6
;timer.c,50 :: 		timer[7]='0'+ secunde%10;
	MOVLW      10
	MOVWF      R4+0
	MOVF       _secunde+0, 0
	MOVWF      R0+0
	CALL       _Div_8x8_S+0
	MOVF       R8+0, 0
	MOVWF      R0+0
	MOVF       R0+0, 0
	ADDLW      48
	MOVWF      _timer+7
;timer.c,51 :: 		timer[8]='\0';
	CLRF       _timer+8
;timer.c,52 :: 		}
	RETURN
; end of _afisaj

_interrupt:
	MOVWF      R15+0
	SWAPF      STATUS+0, 0
	CLRF       STATUS+0
	MOVWF      ___saveSTATUS+0
	MOVF       PCLATH+0, 0
	MOVWF      ___savePCLATH+0
	CLRF       PCLATH+0
;timer.c,55 :: 		void interrupt(){
;timer.c,56 :: 		if(TMR0IF_bit)
	BTFSS      TMR0IF_bit+0, 2
	GOTO       L_interrupt3
;timer.c,58 :: 		TMR0=0;
	CLRF       TMR0+0
;timer.c,59 :: 		timp();
	CALL       _timp+0
;timer.c,60 :: 		}
L_interrupt3:
;timer.c,61 :: 		INTCON.TMR0IF=0;
	BCF        INTCON+0, 2
;timer.c,62 :: 		}
L__interrupt6:
	MOVF       ___savePCLATH+0, 0
	MOVWF      PCLATH+0
	SWAPF      ___saveSTATUS+0, 0
	MOVWF      STATUS+0
	SWAPF      R15+0, 1
	SWAPF      R15+0, 0
	RETFIE
; end of _interrupt

_main:
;timer.c,65 :: 		void main() {
;timer.c,66 :: 		INTCON.GIE = 1;
	BSF        INTCON+0, 7
;timer.c,67 :: 		INTCON.T0IE = 1;
	BSF        INTCON+0, 5
;timer.c,68 :: 		TMR0 = 0;
	CLRF       TMR0+0
;timer.c,69 :: 		OPTION_REG = 0b0000110;
	MOVLW      6
	MOVWF      OPTION_REG+0
;timer.c,71 :: 		Lcd_Init();
	CALL       _Lcd_Init+0
;timer.c,72 :: 		Lcd_Cmd(_LCD_CLEAR);
	MOVLW      1
	MOVWF      FARG_Lcd_Cmd_out_char+0
	CALL       _Lcd_Cmd+0
;timer.c,73 :: 		Lcd_Cmd(_LCD_CURSOR_OFF);
	MOVLW      12
	MOVWF      FARG_Lcd_Cmd_out_char+0
	CALL       _Lcd_Cmd+0
;timer.c,75 :: 		while(1)
L_main4:
;timer.c,77 :: 		afisaj();
	CALL       _afisaj+0
;timer.c,78 :: 		Lcd_Out(1,5,timer);
	MOVLW      1
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      5
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      _timer+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
;timer.c,79 :: 		}
	GOTO       L_main4
;timer.c,80 :: 		}
	GOTO       $+0
; end of _main
