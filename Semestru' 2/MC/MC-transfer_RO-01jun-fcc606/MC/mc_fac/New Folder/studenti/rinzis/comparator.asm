
_init:
;comparator.c,1 :: 		void init()
;comparator.c,3 :: 		CMCON0.CM0=0;
	BCF        CMCON0+0, 0
;comparator.c,4 :: 		CMCON0.CM1=1;
	BSF        CMCON0+0, 1
;comparator.c,5 :: 		CMCON0.CM2=0;
	BCF        CMCON0+0, 2
;comparator.c,6 :: 		CMCON0.CIS=0;
	BCF        CMCON0+0, 3
;comparator.c,7 :: 		CMCON0.C1INV=0;
	BCF        CMCON0+0, 4
;comparator.c,8 :: 		CMCON0.C2INV=1;
	BSF        CMCON0+0, 5
;comparator.c,9 :: 		CMCON0.C1OUT=0;
	BCF        CMCON0+0, 6
;comparator.c,10 :: 		CMCON0.C2OUT=0;
	BCF        CMCON0+0, 7
;comparator.c,11 :: 		CMCON1=0X00;
	CLRF       CMCON1+0
;comparator.c,12 :: 		VRCON.VR0=0;
	BCF        VRCON+0, 0
;comparator.c,13 :: 		VRCON.VR1=1;
	BSF        VRCON+0, 1
;comparator.c,14 :: 		VRCON.VR2=0;
	BCF        VRCON+0, 2
;comparator.c,15 :: 		VRCON.VR3=0;
	BCF        VRCON+0, 3
;comparator.c,16 :: 		VRCON.VRR=1;
	BSF        VRCON+0, 5
;comparator.c,17 :: 		VRCON.VREN=1;
	BSF        VRCON+0, 7
;comparator.c,18 :: 		TRISA=0XFF;
	MOVLW      255
	MOVWF      TRISA+0
;comparator.c,19 :: 		TRISB=0X00;
	CLRF       TRISB+0
;comparator.c,20 :: 		}
	RETURN
; end of _init

_main:
;comparator.c,21 :: 		void main() {
;comparator.c,22 :: 		init();
	CALL       _init+0
;comparator.c,23 :: 		while(1)
L_main0:
;comparator.c,25 :: 		PORTB.RB0=CMCON0.C2OUT;
	BTFSC      CMCON0+0, 7
	GOTO       L__main3
	BCF        PORTB+0, 0
	GOTO       L__main4
L__main3:
	BSF        PORTB+0, 0
L__main4:
;comparator.c,26 :: 		delay_ms(200);
	MOVLW      65
	MOVWF      R12+0
	MOVLW      238
	MOVWF      R13+0
L_main2:
	DECFSZ     R13+0, 1
	GOTO       L_main2
	DECFSZ     R12+0, 1
	GOTO       L_main2
	NOP
;comparator.c,27 :: 		}
	GOTO       L_main0
;comparator.c,28 :: 		}
	GOTO       $+0
; end of _main
