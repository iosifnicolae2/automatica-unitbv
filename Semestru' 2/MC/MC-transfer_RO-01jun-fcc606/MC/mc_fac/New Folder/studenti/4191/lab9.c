sbit LCD_RS at RD4_bit;
sbit LCD_EN at RD5_bit;
sbit LCD_D4 at RD0_bit;
sbit LCD_D5 at RD1_bit;
sbit LCD_D6 at RD2_bit;
sbit LCD_D7 at RD3_bit;

sbit LCD_RS_Direction at TRISD4_bit;
sbit LCD_EN_Direction at TRISD5_bit;
sbit LCD_D4_Direction at TRISD0_bit;
sbit LCD_D5_Direction at TRISD1_bit;
sbit LCD_D6_Direction at TRISD2_bit;
sbit LCD_D7_Direction at TRISD3_bit;


 int citeste_ADC(short chanel)
  {

  int rez;
  chanel=chanel&0b00000111;
  chanel=chanel<<2;
  ADCON0=ADCON0&0b11100011;
  ADCON0=ADCON0|chanel;
  Delay_ms(10);
  ADCON0.GO_DONE=1;
  while(ADCON0.GO_DONE==1)
  {
  }
 rez=ADRESH;
 rez=rez<<2;
 ADRESL=ADRESL>>6;
 rez=rez|ADRESL;
 return rez;

  }



void main() {
  TRISA.RA0=1;
  ANSEL=0X01;
  ADCON0=0X01;
  while(1)
  {
  Lcd_Out(1, 3, "Hello!");
  }


}
