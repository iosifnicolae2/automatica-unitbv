void main() {
//sectiunea de declarae a starilor
short A;
short B;
short C;
short D;
short E;

//sectiunea de initializare a marimilor de intrare
TRISD.RD0=1;
TRISD.RD1=1;
TRISD.RD2=1;
TRISC.RC0=1;
TRISC.RC1=1;

//sectiunea de declarare A marimilor de iesire
TRISD.RD3=0;
TRISD.RD4=0;
TRISC.RC2=0;
TRISC.RC3=0;
TRISC.RC4=0;

//sectiunea de initializare A starilor
A=1;
B=0;
C=0;
D=0;
E=0;

while(1)
{
        //sectiunea de reconfigurare a starilor
        if ((A==1)&&(PORTD.RD0==1))
           { A=0;
             B=1; }
        if ((B==1)&&(PORTD.RD2==1))
           { B=0;
             C=1; }
        if ((C==1)&&(PORTD.RD1==1))
           { C=0;
             A=1; }
        if ((D==1)&&(PORTC.RC0==1))
           { D=0;
             E=1; }
        if ((E==1)&&(PORTC.RC1==1))
           { E=0;
             D=1; }
             
        //SECTIUNEA DE RECONFIGURARE A IESIRILOR
         if(A==1)
           { PORTD.RD3=0;
             PORTD.RD4=0;
             PORTC.RC2=0;
             PORTC.RC3=0;
             PORTC.RC4=0; }
         if(B==1)
           { PORTD.RD3=1;
             PORTD.RD4=0;
             PORTC.RC2=1;
             PORTC.RC3=0;
             PORTC.RC4=1; }
         if(C==1)
           { PORTD.RD3=0;
             PORTD.RD4=1;
             PORTC.RC2=0;
             PORTC.RC3=0;
             PORTC.RC4=0; }
          if(D==1)
           { PORTD.RD3=0;
             PORTD.RD4=0;
             PORTC.RC2=1;
             PORTC.RC3=0;
             PORTC.RC4=0; }
           if(E==1)
           { PORTD.RD3=0;
             PORTD.RD4=0;
             PORTC.RC3=0;
             PORTC.RC4=0;
             PORTC.RC4=1; }
  }
}
