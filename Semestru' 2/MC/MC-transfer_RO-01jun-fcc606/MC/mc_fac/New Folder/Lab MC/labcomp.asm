
_init:
;labcomp.c,2 :: 		void init()
;labcomp.c,4 :: 		CMCON0.C2OUT=0;
	BCF        CMCON0+0, 7
;labcomp.c,5 :: 		CMCON0.C1OUT=0;
	BCF        CMCON0+0, 6
;labcomp.c,6 :: 		CMCON0.C2INV=1;
	BSF        CMCON0+0, 5
;labcomp.c,7 :: 		CMCON0.C1INV=0;
	BCF        CMCON0+0, 4
;labcomp.c,8 :: 		CMCON0.CIS=0;
	BCF        CMCON0+0, 3
;labcomp.c,9 :: 		CMCON0.CM2=0;
	BCF        CMCON0+0, 2
;labcomp.c,10 :: 		CMCON0.CM1=1;
	BSF        CMCON0+0, 1
;labcomp.c,11 :: 		CMCON0.CM0=0;
	BCF        CMCON0+0, 0
;labcomp.c,12 :: 		CMCON1=0b00000000;
	CLRF       CMCON1+0
;labcomp.c,13 :: 		VRCON.VREN=1 ;
	BSF        VRCON+0, 7
;labcomp.c,14 :: 		VRCON.VRR=1 ;
	BSF        VRCON+0, 5
;labcomp.c,15 :: 		VRCON.VR3= 0   ;
	BCF        VRCON+0, 3
;labcomp.c,16 :: 		VRCON.VR2=0;
	BCF        VRCON+0, 2
;labcomp.c,17 :: 		VRCON.VR1= 1;
	BSF        VRCON+0, 1
;labcomp.c,18 :: 		VRCON.VR0= 0 ;
	BCF        VRCON+0, 0
;labcomp.c,19 :: 		TRISA=0b00001111;
	MOVLW      15
	MOVWF      TRISA+0
;labcomp.c,20 :: 		PORTA=0;
	CLRF       PORTA+0
;labcomp.c,21 :: 		TRISB=0b00000000;
	CLRF       TRISB+0
;labcomp.c,22 :: 		PORTB=0;
	CLRF       PORTB+0
;labcomp.c,23 :: 		}
	RETURN
; end of _init

_stare:
	CLRF       stare_raspuns_L0+0
;labcomp.c,24 :: 		char stare(char comparator)
;labcomp.c,26 :: 		if (comparator)
	MOVF       FARG_stare_comparator+0, 0
	BTFSC      STATUS+0, 2
	GOTO       L_stare0
;labcomp.c,28 :: 		raspuns=CMCON0.C2OUT;
	MOVLW      0
	BTFSC      CMCON0+0, 7
	MOVLW      1
	MOVWF      stare_raspuns_L0+0
;labcomp.c,29 :: 		}
	GOTO       L_stare1
L_stare0:
;labcomp.c,32 :: 		raspuns=CMCON0.C1OUT;
	MOVLW      0
	BTFSC      CMCON0+0, 6
	MOVLW      1
	MOVWF      stare_raspuns_L0+0
;labcomp.c,33 :: 		}
L_stare1:
;labcomp.c,34 :: 		return raspuns;
	MOVF       stare_raspuns_L0+0, 0
	MOVWF      R0+0
;labcomp.c,35 :: 		}
	RETURN
; end of _stare

_main:
;labcomp.c,37 :: 		void main()
;labcomp.c,39 :: 		init();
	CALL       _init+0
;labcomp.c,40 :: 		while(1)
L_main2:
;labcomp.c,42 :: 		PORTB.RB1=stare(1);
	MOVLW      1
	MOVWF      FARG_stare_comparator+0
	CALL       _stare+0
	BTFSC      R0+0, 0
	GOTO       L__main5
	BCF        PORTB+0, 1
	GOTO       L__main6
L__main5:
	BSF        PORTB+0, 1
L__main6:
;labcomp.c,43 :: 		delay_ms(100);
	MOVLW      2
	MOVWF      R11+0
	MOVLW      4
	MOVWF      R12+0
	MOVLW      186
	MOVWF      R13+0
L_main4:
	DECFSZ     R13+0, 1
	GOTO       L_main4
	DECFSZ     R12+0, 1
	GOTO       L_main4
	DECFSZ     R11+0, 1
	GOTO       L_main4
	NOP
;labcomp.c,44 :: 		}
	GOTO       L_main2
;labcomp.c,45 :: 		}
	GOTO       $+0
; end of _main
