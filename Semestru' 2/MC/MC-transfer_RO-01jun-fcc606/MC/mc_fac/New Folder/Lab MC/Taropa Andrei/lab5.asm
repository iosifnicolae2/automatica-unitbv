
_interrupt:
	MOVWF      R15+0
	SWAPF      STATUS+0, 0
	CLRF       STATUS+0
	MOVWF      ___saveSTATUS+0
	MOVF       PCLATH+0, 0
	MOVWF      ___savePCLATH+0
	CLRF       PCLATH+0
;lab5.c,1 :: 		void interrupt()
;lab5.c,3 :: 		INTCON.GIE=0;
	BCF        INTCON+0, 7
;lab5.c,4 :: 		if (INTCON.INTF){
	BTFSS      INTCON+0, 1
	GOTO       L_interrupt0
;lab5.c,5 :: 		INTCON.INTF=0;
	BCF        INTCON+0, 1
;lab5.c,6 :: 		PORTA=0B00100001;
	MOVLW      33
	MOVWF      PORTA+0
;lab5.c,7 :: 		delay_ms(2000);
	MOVLW      21
	MOVWF      R11+0
	MOVLW      75
	MOVWF      R12+0
	MOVLW      190
	MOVWF      R13+0
L_interrupt1:
	DECFSZ     R13+0, 1
	GOTO       L_interrupt1
	DECFSZ     R12+0, 1
	GOTO       L_interrupt1
	DECFSZ     R11+0, 1
	GOTO       L_interrupt1
	NOP
;lab5.c,8 :: 		}
L_interrupt0:
;lab5.c,9 :: 		INTCON.GIE=1;
	BSF        INTCON+0, 7
;lab5.c,10 :: 		}
L__interrupt4:
	MOVF       ___savePCLATH+0, 0
	MOVWF      PCLATH+0
	SWAPF      ___saveSTATUS+0, 0
	MOVWF      STATUS+0
	SWAPF      R15+0, 1
	SWAPF      R15+0, 0
	RETFIE
; end of _interrupt

_main:
;lab5.c,11 :: 		void main() {
;lab5.c,12 :: 		TRISA=0b00000000;
	CLRF       TRISA+0
;lab5.c,13 :: 		TRISB=0b00000001;
	MOVLW      1
	MOVWF      TRISB+0
;lab5.c,14 :: 		PORTA=0b00000000;
	CLRF       PORTA+0
;lab5.c,15 :: 		PORTB=0b00000001;
	MOVLW      1
	MOVWF      PORTB+0
;lab5.c,16 :: 		INTCON.GIE=1;
	BSF        INTCON+0, 7
;lab5.c,17 :: 		INTCON.INTE=1;
	BSF        INTCON+0, 4
;lab5.c,18 :: 		while(1){
L_main2:
;lab5.c,22 :: 		PORTA=0b00100100;
	MOVLW      36
	MOVWF      PORTA+0
;lab5.c,24 :: 		}
	GOTO       L_main2
;lab5.c,25 :: 		}
	GOTO       $+0
; end of _main
