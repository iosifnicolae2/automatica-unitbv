
_init:
;cata.c,1 :: 		void init()
;cata.c,3 :: 		CMCON0.C2OUT=0;
	BCF        CMCON0+0, 7
;cata.c,4 :: 		CMCON0.C1OUT=0;
	BCF        CMCON0+0, 6
;cata.c,5 :: 		CMCON0.C2INV=0;
	BCF        CMCON0+0, 5
;cata.c,6 :: 		CMCON0.C1INV=0;
	BCF        CMCON0+0, 4
;cata.c,7 :: 		CMCON0.CIS=0;
	BCF        CMCON0+0, 3
;cata.c,8 :: 		CMCON0.CM2=0;
	BCF        CMCON0+0, 2
;cata.c,9 :: 		CMCON0.CM1=1;
	BSF        CMCON0+0, 1
;cata.c,10 :: 		CMCON0.CM0=0;
	BCF        CMCON0+0, 0
;cata.c,11 :: 		TRISB=0;
	CLRF       TRISB+0
;cata.c,12 :: 		VRCON.VREN=1;
	BSF        VRCON+0, 7
;cata.c,13 :: 		VRCON.VRR=1;
	BSF        VRCON+0, 5
;cata.c,14 :: 		VRCON.VR3=1;
	BSF        VRCON+0, 3
;cata.c,15 :: 		VRCON.VR2=0;
	BCF        VRCON+0, 2
;cata.c,16 :: 		VRCON.VR1=1;
	BSF        VRCON+0, 1
;cata.c,17 :: 		VRCON.VR0=0;
	BCF        VRCON+0, 0
;cata.c,18 :: 		}
	RETURN
; end of _init

_main:
;cata.c,19 :: 		void main()
;cata.c,21 :: 		init();
	CALL       _init+0
;cata.c,22 :: 		PORTB=0;
	CLRF       PORTB+0
;cata.c,23 :: 		while(1)
L_main0:
;cata.c,25 :: 		PORTB.RB0=CMCON0.C1OUT;
	BTFSC      CMCON0+0, 6
	GOTO       L__main3
	BCF        PORTB+0, 0
	GOTO       L__main4
L__main3:
	BSF        PORTB+0, 0
L__main4:
;cata.c,26 :: 		delay_ms(100);
	MOVLW      2
	MOVWF      R11+0
	MOVLW      4
	MOVWF      R12+0
	MOVLW      186
	MOVWF      R13+0
L_main2:
	DECFSZ     R13+0, 1
	GOTO       L_main2
	DECFSZ     R12+0, 1
	GOTO       L_main2
	DECFSZ     R11+0, 1
	GOTO       L_main2
	NOP
;cata.c,27 :: 		}
	GOTO       L_main0
;cata.c,29 :: 		}
	GOTO       $+0
; end of _main
