
_main:
;lab6.c,24 :: 		void main() {
;lab6.c,25 :: 		OPTION_REG=0b00000101;
	MOVLW      5
	MOVWF      OPTION_REG+0
;lab6.c,26 :: 		INTCON=0b10100000;
	MOVLW      160
	MOVWF      INTCON+0
;lab6.c,27 :: 		TMR0=99;
	MOVLW      99
	MOVWF      TMR0+0
;lab6.c,28 :: 		Lcd_Init();
	CALL       _Lcd_Init+0
;lab6.c,29 :: 		while (1)
L_main0:
;lab6.c,31 :: 		if (i==50)
	MOVLW      0
	XORWF      _i+1, 0
	BTFSS      STATUS+0, 2
	GOTO       L__main8
	MOVLW      50
	XORWF      _i+0, 0
L__main8:
	BTFSS      STATUS+0, 2
	GOTO       L_main2
;lab6.c,33 :: 		sprinti(sir,"%d :%d :%d",sec,m,h);
	MOVLW      _sir+0
	MOVWF      FARG_sprinti_wh+0
	MOVLW      ?lstr_1_lab6+0
	MOVWF      FARG_sprinti_f+0
	MOVLW      hi_addr(?lstr_1_lab6+0
	MOVWF      FARG_sprinti_f+1
	MOVF       _sec+0, 0
	MOVWF      FARG_sprinti_wh+3
	MOVF       _sec+1, 0
	MOVWF      FARG_sprinti_wh+4
	MOVF       _m+0, 0
	MOVWF      FARG_sprinti_wh+5
	MOVF       _m+1, 0
	MOVWF      FARG_sprinti_wh+6
	MOVF       _h+0, 0
	MOVWF      FARG_sprinti_wh+7
	MOVF       _h+1, 0
	MOVWF      FARG_sprinti_wh+8
	CALL       _sprinti+0
;lab6.c,34 :: 		Lcd_Out(1,1,sir);
	MOVLW      1
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      1
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      _sir+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
;lab6.c,35 :: 		i=0;
	CLRF       _i+0
	CLRF       _i+1
;lab6.c,36 :: 		}
L_main2:
;lab6.c,37 :: 		}
	GOTO       L_main0
;lab6.c,38 :: 		}
	GOTO       $+0
; end of _main

_interrupt:
	MOVWF      R15+0
	SWAPF      STATUS+0, 0
	CLRF       STATUS+0
	MOVWF      ___saveSTATUS+0
	MOVF       PCLATH+0, 0
	MOVWF      ___savePCLATH+0
	CLRF       PCLATH+0
;lab6.c,39 :: 		void interrupt()
;lab6.c,41 :: 		INTCON.GIE=0;
	BCF        INTCON+0, 7
;lab6.c,42 :: 		if (INTCON.T0IF)
	BTFSS      INTCON+0, 2
	GOTO       L_interrupt3
;lab6.c,44 :: 		i++;
	INCF       _i+0, 1
	BTFSC      STATUS+0, 2
	INCF       _i+1, 1
;lab6.c,45 :: 		INTCON.T0IF=0;
	BCF        INTCON+0, 2
;lab6.c,46 :: 		TMR0=99;
	MOVLW      99
	MOVWF      TMR0+0
;lab6.c,47 :: 		if (i==50)
	MOVLW      0
	XORWF      _i+1, 0
	BTFSS      STATUS+0, 2
	GOTO       L__interrupt10
	MOVLW      50
	XORWF      _i+0, 0
L__interrupt10:
	BTFSS      STATUS+0, 2
	GOTO       L_interrupt4
;lab6.c,49 :: 		sec++;
	INCF       _sec+0, 1
	BTFSC      STATUS+0, 2
	INCF       _sec+1, 1
;lab6.c,50 :: 		i=0;
	CLRF       _i+0
	CLRF       _i+1
;lab6.c,51 :: 		}
L_interrupt4:
;lab6.c,52 :: 		if (sec>59)
	MOVLW      128
	MOVWF      R0+0
	MOVLW      128
	XORWF      _sec+1, 0
	SUBWF      R0+0, 0
	BTFSS      STATUS+0, 2
	GOTO       L__interrupt11
	MOVF       _sec+0, 0
	SUBLW      59
L__interrupt11:
	BTFSC      STATUS+0, 0
	GOTO       L_interrupt5
;lab6.c,54 :: 		sec=0;
	CLRF       _sec+0
	CLRF       _sec+1
;lab6.c,55 :: 		m++;
	INCF       _m+0, 1
	BTFSC      STATUS+0, 2
	INCF       _m+1, 1
;lab6.c,56 :: 		if (m>59)
	MOVLW      128
	MOVWF      R0+0
	MOVLW      128
	XORWF      _m+1, 0
	SUBWF      R0+0, 0
	BTFSS      STATUS+0, 2
	GOTO       L__interrupt12
	MOVF       _m+0, 0
	SUBLW      59
L__interrupt12:
	BTFSC      STATUS+0, 0
	GOTO       L_interrupt6
;lab6.c,58 :: 		h++;
	INCF       _h+0, 1
	BTFSC      STATUS+0, 2
	INCF       _h+1, 1
;lab6.c,59 :: 		m=0;
	CLRF       _m+0
	CLRF       _m+1
;lab6.c,60 :: 		}
L_interrupt6:
;lab6.c,61 :: 		if (h>23)
	MOVLW      128
	MOVWF      R0+0
	MOVLW      128
	XORWF      _h+1, 0
	SUBWF      R0+0, 0
	BTFSS      STATUS+0, 2
	GOTO       L__interrupt13
	MOVF       _h+0, 0
	SUBLW      23
L__interrupt13:
	BTFSC      STATUS+0, 0
	GOTO       L_interrupt7
;lab6.c,63 :: 		h=0;
	CLRF       _h+0
	CLRF       _h+1
;lab6.c,64 :: 		}
L_interrupt7:
;lab6.c,66 :: 		}
L_interrupt5:
;lab6.c,67 :: 		}
L_interrupt3:
;lab6.c,68 :: 		INTCON.GIE=1;
	BSF        INTCON+0, 7
;lab6.c,69 :: 		}
L__interrupt9:
	MOVF       ___savePCLATH+0, 0
	MOVWF      PCLATH+0
	SWAPF      ___saveSTATUS+0, 0
	MOVWF      STATUS+0
	SWAPF      R15+0, 1
	SWAPF      R15+0, 0
	RETFIE
; end of _interrupt
