
_init_conv:
;Lab7_ADC.c,15 :: 		void init_conv() {
;Lab7_ADC.c,16 :: 		ANSEL=0b00000000;  //255
	CLRF       ANSEL+0
;Lab7_ADC.c,17 :: 		ADCON0.ADFM=0;
	BCF        ADCON0+0, 7
;Lab7_ADC.c,18 :: 		ADCON0.VCFG1=0;
	BCF        ADCON0+0, 6
;Lab7_ADC.c,19 :: 		ADCON0.VCFG0=0;
	BCF        ADCON0+0, 5
;Lab7_ADC.c,20 :: 		ADCON0.CHS2=0;
	BCF        ADCON0+0, 4
;Lab7_ADC.c,21 :: 		ADCON0.CHS1=0;
	BCF        ADCON0+0, 3
;Lab7_ADC.c,22 :: 		ADCON0.CHS0=0;
	BCF        ADCON0+0, 2
;Lab7_ADC.c,23 :: 		ADCON0.GO_DONE=0;
	BCF        ADCON0+0, 1
;Lab7_ADC.c,24 :: 		ADCON0.ADON=0;
	BCF        ADCON0+0, 0
;Lab7_ADC.c,26 :: 		ADCON1.ADCS2=0;
	BCF        ADCON1+0, 6
;Lab7_ADC.c,27 :: 		ADCON1.ADCS1=0;
	BCF        ADCON1+0, 5
;Lab7_ADC.c,28 :: 		ADCON1.ADCS0=0;
	BCF        ADCON1+0, 4
;Lab7_ADC.c,30 :: 		TRISA=255;
	MOVLW      255
	MOVWF      TRISA+0
;Lab7_ADC.c,32 :: 		}
	RETURN
; end of _init_conv

_read_conv:
	CLRF       read_conv_rez_L0+0
	CLRF       read_conv_rez_L0+1
;Lab7_ADC.c,33 :: 		int read_conv() {
;Lab7_ADC.c,36 :: 		ADCON0.ADON=1;
	BSF        ADCON0+0, 0
;Lab7_ADC.c,37 :: 		Delay_ms(1);
	MOVLW      83
	MOVWF      R13+0
L_read_conv0:
	DECFSZ     R13+0, 1
	GOTO       L_read_conv0
;Lab7_ADC.c,38 :: 		ADCON0.GO_DONE=1;
	BSF        ADCON0+0, 1
;Lab7_ADC.c,39 :: 		while (ADCON0.GO_DONE)
L_read_conv1:
	BTFSS      ADCON0+0, 1
	GOTO       L_read_conv2
;Lab7_ADC.c,41 :: 		}
	GOTO       L_read_conv1
L_read_conv2:
;Lab7_ADC.c,43 :: 		ADCON0.ADON=0;
	BCF        ADCON0+0, 0
;Lab7_ADC.c,45 :: 		rez|=ADRESH<<2|ADRESL>>6;
	MOVF       ADRESH+0, 0
	MOVWF      R2+0
	CLRF       R2+1
	RLF        R2+0, 1
	RLF        R2+1, 1
	BCF        R2+0, 0
	RLF        R2+0, 1
	RLF        R2+1, 1
	BCF        R2+0, 0
	MOVLW      6
	MOVWF      R1+0
	MOVF       ADRESL+0, 0
	MOVWF      R0+0
	MOVF       R1+0, 0
L__read_conv6:
	BTFSC      STATUS+0, 2
	GOTO       L__read_conv7
	RRF        R0+0, 1
	BCF        R0+0, 7
	ADDLW      255
	GOTO       L__read_conv6
L__read_conv7:
	MOVLW      0
	MOVWF      R0+1
	MOVF       R2+0, 0
	IORWF      R0+0, 1
	MOVF       R2+1, 0
	IORWF      R0+1, 1
	MOVF       read_conv_rez_L0+0, 0
	IORWF      R0+0, 1
	MOVF       read_conv_rez_L0+1, 0
	IORWF      R0+1, 1
	MOVF       R0+0, 0
	MOVWF      read_conv_rez_L0+0
	MOVF       R0+1, 0
	MOVWF      read_conv_rez_L0+1
;Lab7_ADC.c,46 :: 		return rez;
;Lab7_ADC.c,47 :: 		}
	RETURN
; end of _read_conv

_main:
	CLRF       main_fin_L0+0
	CLRF       main_fin_L0+1
;Lab7_ADC.c,48 :: 		void main() {
;Lab7_ADC.c,55 :: 		Lcd_Init();
	CALL       _Lcd_Init+0
;Lab7_ADC.c,57 :: 		init_conv();
	CALL       _init_conv+0
;Lab7_ADC.c,58 :: 		while(1) {
L_main3:
;Lab7_ADC.c,59 :: 		conversie=read_conv();
	CALL       _read_conv+0
;Lab7_ADC.c,61 :: 		sprinti(sir,"conv=%d  ",conversie);
	MOVLW      main_sir_L0+0
	MOVWF      FARG_sprinti_wh+0
	MOVLW      ?lstr_1_Lab7_ADC+0
	MOVWF      FARG_sprinti_f+0
	MOVLW      hi_addr(?lstr_1_Lab7_ADC+0
	MOVWF      FARG_sprinti_f+1
	MOVF       R0+0, 0
	MOVWF      FARG_sprinti_wh+3
	MOVF       R0+1, 0
	MOVWF      FARG_sprinti_wh+4
	CALL       _sprinti+0
;Lab7_ADC.c,62 :: 		Lcd_Out(1,1,sir);
	MOVLW      1
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      1
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      main_sir_L0+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
;Lab7_ADC.c,63 :: 		Delay_ms(5);
	MOVLW      2
	MOVWF      R12+0
	MOVLW      158
	MOVWF      R13+0
L_main5:
	DECFSZ     R13+0, 1
	GOTO       L_main5
	DECFSZ     R12+0, 1
	GOTO       L_main5
	NOP
;Lab7_ADC.c,73 :: 		}
	GOTO       L_main3
;Lab7_ADC.c,75 :: 		}
	GOTO       $+0
; end of _main
