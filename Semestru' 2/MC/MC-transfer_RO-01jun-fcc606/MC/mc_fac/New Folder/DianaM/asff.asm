
_main:
;asff.c,22 :: 		void main() {
;asff.c,25 :: 		TMR0=5;
	MOVLW      5
	MOVWF      TMR0+0
;asff.c,26 :: 		OPTION_REG=0b00000000;
	CLRF       OPTION_REG+0
;asff.c,27 :: 		INTCON=0b11100000;
	MOVLW      224
	MOVWF      INTCON+0
;asff.c,28 :: 		TRISB=0b00000000;
	CLRF       TRISB+0
;asff.c,29 :: 		PORTB=0b00000000;
	CLRF       PORTB+0
;asff.c,30 :: 		Lcd_init();
	CALL       _Lcd_Init+0
;asff.c,32 :: 		while (1)
L_main0:
;asff.c,34 :: 		sprinti(sir,"secunde=%d",secunda);
	MOVLW      _sir+0
	MOVWF      FARG_sprinti_wh+0
	MOVLW      ?lstr_1_asff+0
	MOVWF      FARG_sprinti_f+0
	MOVLW      hi_addr(?lstr_1_asff+0
	MOVWF      FARG_sprinti_f+1
	MOVF       _secunda+0, 0
	MOVWF      FARG_sprinti_wh+3
	MOVF       _secunda+1, 0
	MOVWF      FARG_sprinti_wh+4
	CALL       _sprinti+0
;asff.c,35 :: 		Lcd_out(1,1,sir);
	MOVLW      1
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      1
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      _sir+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
;asff.c,36 :: 		}
	GOTO       L_main0
;asff.c,37 :: 		}
	GOTO       $+0
; end of _main

_interrupt:
	MOVWF      R15+0
	SWAPF      STATUS+0, 0
	CLRF       STATUS+0
	MOVWF      ___saveSTATUS+0
	MOVF       PCLATH+0, 0
	MOVWF      ___savePCLATH+0
	CLRF       PCLATH+0
;asff.c,39 :: 		void interrupt()
;asff.c,41 :: 		INTCON.GIE=0;
	BCF        INTCON+0, 7
;asff.c,45 :: 		INTCON.T0IF=0;
	BCF        INTCON+0, 2
;asff.c,46 :: 		count=0;
	CLRF       _count+0
	CLRF       _count+1
;asff.c,47 :: 		count++;
	MOVLW      1
	MOVWF      _count+0
	MOVLW      0
	MOVWF      _count+1
;asff.c,53 :: 		}
L_interrupt3:
;asff.c,54 :: 		TMR0=5;
	MOVLW      5
	MOVWF      TMR0+0
;asff.c,56 :: 		INTCON.GIE=1;
	BSF        INTCON+0, 7
;asff.c,57 :: 		}
L__interrupt4:
	MOVF       ___savePCLATH+0, 0
	MOVWF      PCLATH+0
	SWAPF      ___saveSTATUS+0, 0
	MOVWF      STATUS+0
	SWAPF      R15+0, 1
	SWAPF      R15+0, 0
	RETFIE
; end of _interrupt
