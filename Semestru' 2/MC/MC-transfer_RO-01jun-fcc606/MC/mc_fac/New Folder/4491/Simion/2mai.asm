
_main:
;2mai.c,3 :: 		void main()
;2mai.c,5 :: 		INTCON=0b10100000;
	MOVLW      160
	MOVWF      INTCON+0
;2mai.c,6 :: 		OPTION_REG=0b00000111;
	MOVLW      7
	MOVWF      OPTION_REG+0
;2mai.c,7 :: 		TRISB=0;
	CLRF       TRISB+0
;2mai.c,9 :: 		TMR0=158;
	MOVLW      158
	MOVWF      TMR0+0
;2mai.c,10 :: 		while(1)
L_main0:
;2mai.c,11 :: 		{}
	GOTO       L_main0
;2mai.c,12 :: 		}
	GOTO       $+0
; end of _main

_interrupt:
	MOVWF      R15+0
	SWAPF      STATUS+0, 0
	CLRF       STATUS+0
	MOVWF      ___saveSTATUS+0
	MOVF       PCLATH+0, 0
	MOVWF      ___savePCLATH+0
	CLRF       PCLATH+0
;2mai.c,14 :: 		void interrupt()
;2mai.c,16 :: 		INTCON.GIE=0;
	BCF        INTCON+0, 7
;2mai.c,17 :: 		if (INTCON.T0IF==1)
	BTFSS      INTCON+0, 2
	GOTO       L_interrupt2
;2mai.c,19 :: 		INTCON.INTF=0;
	BCF        INTCON+0, 1
;2mai.c,20 :: 		i++;
	INCF       _i+0, 1
	BTFSC      STATUS+0, 2
	INCF       _i+1, 1
;2mai.c,21 :: 		TMR0=158;
	MOVLW      158
	MOVWF      TMR0+0
;2mai.c,22 :: 		if (i==160)
	MOVLW      0
	XORWF      _i+1, 0
	BTFSS      STATUS+0, 2
	GOTO       L__interrupt5
	MOVLW      160
	XORWF      _i+0, 0
L__interrupt5:
	BTFSS      STATUS+0, 2
	GOTO       L_interrupt3
;2mai.c,24 :: 		PORTB.RB0=!PORTB.RB0;
	MOVLW      1
	XORWF      PORTB+0, 1
;2mai.c,25 :: 		i=0;
	CLRF       _i+0
	CLRF       _i+1
;2mai.c,26 :: 		};
L_interrupt3:
;2mai.c,27 :: 		}
L_interrupt2:
;2mai.c,28 :: 		}
L__interrupt4:
	MOVF       ___savePCLATH+0, 0
	MOVWF      PCLATH+0
	SWAPF      ___saveSTATUS+0, 0
	MOVWF      STATUS+0
	SWAPF      R15+0, 1
	SWAPF      R15+0, 0
	RETFIE
; end of _interrupt
