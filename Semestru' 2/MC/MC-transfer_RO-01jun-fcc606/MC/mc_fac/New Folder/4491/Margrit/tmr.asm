
_main:
;tmr.c,21 :: 		void main() {
;tmr.c,22 :: 		TMR0=5;
	MOVLW      5
	MOVWF      TMR0+0
;tmr.c,23 :: 		OPTION_REG=0b00001000;
	MOVLW      8
	MOVWF      OPTION_REG+0
;tmr.c,24 :: 		INTCON=0b11100000;
	MOVLW      224
	MOVWF      INTCON+0
;tmr.c,25 :: 		TRISB=0;
	CLRF       TRISB+0
;tmr.c,26 :: 		PORTB=0;
	CLRF       PORTB+0
;tmr.c,27 :: 		LCD_Init ();
	CALL       _Lcd_Init+0
;tmr.c,28 :: 		while (1){
L_main0:
;tmr.c,29 :: 		sprinti (sir,"sunt :%d",secunda);
	MOVLW      _sir+0
	MOVWF      FARG_sprinti_wh+0
	MOVLW      ?lstr_1_tmr+0
	MOVWF      FARG_sprinti_f+0
	MOVLW      hi_addr(?lstr_1_tmr+0
	MOVWF      FARG_sprinti_f+1
	MOVF       _secunda+0, 0
	MOVWF      FARG_sprinti_wh+3
	MOVF       _secunda+1, 0
	MOVWF      FARG_sprinti_wh+4
	CALL       _sprinti+0
;tmr.c,30 :: 		LCD_Out(1,1,sir);
	MOVLW      1
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      1
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      _sir+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
;tmr.c,31 :: 		}
	GOTO       L_main0
;tmr.c,32 :: 		}
	GOTO       $+0
; end of _main

_interrupt:
	MOVWF      R15+0
	SWAPF      STATUS+0, 0
	CLRF       STATUS+0
	MOVWF      ___saveSTATUS+0
	MOVF       PCLATH+0, 0
	MOVWF      ___savePCLATH+0
	CLRF       PCLATH+0
;tmr.c,33 :: 		void interrupt () {
;tmr.c,34 :: 		INTCON.GIE=0;
	BCF        INTCON+0, 7
;tmr.c,36 :: 		{ INTCON.T0IF=0;
	BCF        INTCON+0, 2
;tmr.c,37 :: 		count++;
	INCF       _count+0, 1
	BTFSC      STATUS+0, 2
	INCF       _count+1, 1
;tmr.c,38 :: 		TMR0=5;
	MOVLW      5
	MOVWF      TMR0+0
;tmr.c,39 :: 		if (count==1000)
	MOVF       _count+1, 0
	XORLW      3
	BTFSS      STATUS+0, 2
	GOTO       L__interrupt5
	MOVLW      232
	XORWF      _count+0, 0
L__interrupt5:
	BTFSS      STATUS+0, 2
	GOTO       L_interrupt3
;tmr.c,41 :: 		secunda ++;
	INCF       _secunda+0, 1
	BTFSC      STATUS+0, 2
	INCF       _secunda+1, 1
;tmr.c,42 :: 		TMR0=5;
	MOVLW      5
	MOVWF      TMR0+0
;tmr.c,43 :: 		count=0;}
	CLRF       _count+0
	CLRF       _count+1
L_interrupt3:
;tmr.c,45 :: 		INTCON.GIE=1;
	BSF        INTCON+0, 7
;tmr.c,46 :: 		}
L__interrupt4:
	MOVF       ___savePCLATH+0, 0
	MOVWF      PCLATH+0
	SWAPF      ___saveSTATUS+0, 0
	MOVWF      STATUS+0
	SWAPF      R15+0, 1
	SWAPF      R15+0, 0
	RETFIE
; end of _interrupt
