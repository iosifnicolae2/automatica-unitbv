
_main:
;lab 8.c,1 :: 		void main() {
;lab 8.c,2 :: 		TRISC.RC5=0;
	BCF        TRISC+0, 5
;lab 8.c,3 :: 		OSCCON=0b00110000;
	MOVLW      48
	MOVWF      OSCCON+0
;lab 8.c,4 :: 		T2CON=0b00000110;
	MOVLW      6
	MOVWF      T2CON+0
;lab 8.c,5 :: 		PR2=155;
	MOVLW      155
	MOVWF      PR2+0
;lab 8.c,7 :: 		CCPR1L=0b00000111;
	MOVLW      7
	MOVWF      CCPR1L+0
;lab 8.c,8 :: 		CCP1CON=0b00011100;
	MOVLW      28
	MOVWF      CCP1CON+0
;lab 8.c,9 :: 		while(1){
L_main0:
;lab 8.c,10 :: 		CCPR1L=CCPR1L++;
	INCF       CCPR1L+0, 0
	MOVWF      R0+0
	MOVF       R0+0, 0
	MOVWF      CCPR1L+0
;lab 8.c,11 :: 		if (CCPR1L>14){
	MOVF       CCPR1L+0, 0
	SUBLW      14
	BTFSC      STATUS+0, 0
	GOTO       L_main2
;lab 8.c,12 :: 		CCPR1L=7;
	MOVLW      7
	MOVWF      CCPR1L+0
;lab 8.c,13 :: 		}
L_main2:
;lab 8.c,14 :: 		delay_ms(300);
	MOVLW      4
	MOVWF      R11+0
	MOVLW      12
	MOVWF      R12+0
	MOVLW      51
	MOVWF      R13+0
L_main3:
	DECFSZ     R13+0, 1
	GOTO       L_main3
	DECFSZ     R12+0, 1
	GOTO       L_main3
	DECFSZ     R11+0, 1
	GOTO       L_main3
	NOP
	NOP
;lab 8.c,15 :: 		}
	GOTO       L_main0
;lab 8.c,18 :: 		}
	GOTO       $+0
; end of _main
