
_main:
;lab13.2.c,6 :: 		void main() {
;lab13.2.c,8 :: 		TRISB.RA0=1;
	BSF        TRISB+0, 0
;lab13.2.c,9 :: 		TRISB.RA1=1;
	BSF        TRISB+0, 1
;lab13.2.c,10 :: 		TRISB.RA2=1;
	BSF        TRISB+0, 2
;lab13.2.c,11 :: 		TRISB.RA3=1;
	BSF        TRISB+0, 3
;lab13.2.c,12 :: 		TRISB.RA4=1;
	BSF        TRISB+0, 4
;lab13.2.c,13 :: 		TRISD.RA0=0;
	BCF        TRISD+0, 0
;lab13.2.c,14 :: 		TRISD.RA1=0;
	BCF        TRISD+0, 1
;lab13.2.c,15 :: 		TRISD.RA2=0;
	BCF        TRISD+0, 2
;lab13.2.c,16 :: 		TRISD.RA3=0;
	BCF        TRISD+0, 3
;lab13.2.c,17 :: 		TRISD.RA4=0;
	BCF        TRISD+0, 4
;lab13.2.c,19 :: 		A=1;
	MOVLW      1
	MOVWF      _A+0
;lab13.2.c,20 :: 		B=1;
	MOVLW      1
	MOVWF      _B+0
;lab13.2.c,21 :: 		C1=0;
	CLRF       _C1+0
;lab13.2.c,22 :: 		E=1;
	MOVLW      1
	MOVWF      _E+0
;lab13.2.c,23 :: 		F5=1;
	MOVLW      1
	MOVWF      _F5+0
;lab13.2.c,24 :: 		while(1){
L_main0:
;lab13.2.c,25 :: 		if((TRISB.RA0==1)&(A==1)) {A=0;B=1;}
	BTFSC      TRISB+0, 0
	GOTO       L__main7
	BCF        114, 0
	GOTO       L__main8
L__main7:
	BSF        114, 0
L__main8:
	MOVF       _A+0, 0
	XORLW      1
	MOVLW      1
	BTFSS      STATUS+0, 2
	MOVLW      0
	MOVWF      R1+0
	CLRF       R0+0
	BTFSC      114, 0
	INCF       R0+0, 1
	MOVF       R1+0, 0
	ANDWF      R0+0, 1
	BTFSC      STATUS+0, 2
	GOTO       L_main2
	CLRF       _A+0
	MOVLW      1
	MOVWF      _B+0
L_main2:
;lab13.2.c,26 :: 		if((TRISB.RA1==1)&(B==1)) {A=0;B=1;}
	BTFSC      TRISB+0, 1
	GOTO       L__main9
	BCF        114, 0
	GOTO       L__main10
L__main9:
	BSF        114, 0
L__main10:
	MOVF       _B+0, 0
	XORLW      1
	MOVLW      1
	BTFSS      STATUS+0, 2
	MOVLW      0
	MOVWF      R1+0
	CLRF       R0+0
	BTFSC      114, 0
	INCF       R0+0, 1
	MOVF       R1+0, 0
	ANDWF      R0+0, 1
	BTFSC      STATUS+0, 2
	GOTO       L_main3
	CLRF       _A+0
	MOVLW      1
	MOVWF      _B+0
L_main3:
;lab13.2.c,27 :: 		if((TRISB.RA2==1)&(C1==1)) {A=0;B=1;}
	BTFSC      TRISB+0, 2
	GOTO       L__main11
	BCF        114, 0
	GOTO       L__main12
L__main11:
	BSF        114, 0
L__main12:
	MOVF       _C1+0, 0
	XORLW      1
	MOVLW      1
	BTFSS      STATUS+0, 2
	MOVLW      0
	MOVWF      R1+0
	CLRF       R0+0
	BTFSC      114, 0
	INCF       R0+0, 1
	MOVF       R1+0, 0
	ANDWF      R0+0, 1
	BTFSC      STATUS+0, 2
	GOTO       L_main4
	CLRF       _A+0
	MOVLW      1
	MOVWF      _B+0
L_main4:
;lab13.2.c,28 :: 		if((TRISB.RA3==1)&(E==1)) {E=0;F5=1;}
	BTFSC      TRISB+0, 3
	GOTO       L__main13
	BCF        114, 0
	GOTO       L__main14
L__main13:
	BSF        114, 0
L__main14:
	MOVF       _E+0, 0
	XORLW      1
	MOVLW      1
	BTFSS      STATUS+0, 2
	MOVLW      0
	MOVWF      R1+0
	CLRF       R0+0
	BTFSC      114, 0
	INCF       R0+0, 1
	MOVF       R1+0, 0
	ANDWF      R0+0, 1
	BTFSC      STATUS+0, 2
	GOTO       L_main5
	CLRF       _E+0
	MOVLW      1
	MOVWF      _F5+0
L_main5:
;lab13.2.c,29 :: 		if((TRISB.RB4==1)&(F5==1)) {E=1;F5=0;}
	BTFSC      TRISB+0, 4
	GOTO       L__main15
	BCF        114, 0
	GOTO       L__main16
L__main15:
	BSF        114, 0
L__main16:
	MOVF       _F5+0, 0
	XORLW      1
	MOVLW      1
	BTFSS      STATUS+0, 2
	MOVLW      0
	MOVWF      R1+0
	CLRF       R0+0
	BTFSC      114, 0
	INCF       R0+0, 1
	MOVF       R1+0, 0
	ANDWF      R0+0, 1
	BTFSC      STATUS+0, 2
	GOTO       L_main6
	MOVLW      1
	MOVWF      _E+0
	CLRF       _F5+0
L_main6:
;lab13.2.c,30 :: 		}
	GOTO       L_main0
;lab13.2.c,31 :: 		}
	GOTO       $+0
; end of _main
