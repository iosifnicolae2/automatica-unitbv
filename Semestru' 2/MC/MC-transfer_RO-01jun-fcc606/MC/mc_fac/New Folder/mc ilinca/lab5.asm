
_intrerupt:
;lab5.c,1 :: 		void intrerupt()
;lab5.c,3 :: 		if(INTCON.INTF=1)
	BSF        INTCON+0, 1
	BTFSS      INTCON+0, 1
	GOTO       L_intrerupt0
;lab5.c,5 :: 		PORTC=! PORTC;
	MOVF       PORTC+0, 0
	MOVLW      1
	BTFSS      STATUS+0, 2
	MOVLW      0
	MOVWF      PORTC+0
;lab5.c,7 :: 		INTCON.INTF=0;
	BCF        INTCON+0, 1
;lab5.c,8 :: 		}
L_intrerupt0:
;lab5.c,9 :: 		}
	RETURN
; end of _intrerupt

_main:
	MOVLW      63
	MOVWF      main_sir_L0+0
	MOVLW      6
	MOVWF      main_sir_L0+1
	MOVLW      109
	MOVWF      main_sir_L0+2
	MOVLW      121
	MOVWF      main_sir_L0+3
	MOVLW      82
	MOVWF      main_sir_L0+4
	MOVLW      91
	MOVWF      main_sir_L0+5
	MOVLW      95
	MOVWF      main_sir_L0+6
	MOVLW      49
	MOVWF      main_sir_L0+7
	MOVLW      127
	MOVWF      main_sir_L0+8
	MOVLW      123
	MOVWF      main_sir_L0+9
;lab5.c,10 :: 		void main() {
;lab5.c,13 :: 		TRISB=1;
	MOVLW      1
	MOVWF      TRISB+0
;lab5.c,14 :: 		TRISD=0;
	CLRF       TRISD+0
;lab5.c,15 :: 		TRISC=0;
	CLRF       TRISC+0
;lab5.c,16 :: 		INTCON=0b100100000;
	MOVLW      32
	MOVWF      INTCON+0
;lab5.c,17 :: 		PORTC=1;
	MOVLW      1
	MOVWF      PORTC+0
;lab5.c,18 :: 		i=0;
	CLRF       R1+0
	CLRF       R1+1
;lab5.c,20 :: 		while(1)
L_main1:
;lab5.c,22 :: 		PORTD=sir[i];
	MOVF       R1+0, 0
	ADDLW      main_sir_L0+0
	MOVWF      FSR
	MOVF       INDF+0, 0
	MOVWF      PORTD+0
;lab5.c,24 :: 		i++;
	INCF       R1+0, 1
	BTFSC      STATUS+0, 2
	INCF       R1+1, 1
;lab5.c,25 :: 		if(i==9)
	MOVLW      0
	XORWF      R1+1, 0
	BTFSS      STATUS+0, 2
	GOTO       L__main5
	MOVLW      9
	XORWF      R1+0, 0
L__main5:
	BTFSS      STATUS+0, 2
	GOTO       L_main3
;lab5.c,26 :: 		i=0;
	CLRF       R1+0
	CLRF       R1+1
L_main3:
;lab5.c,27 :: 		Delay_ms(1000);
	MOVLW      11
	MOVWF      R11+0
	MOVLW      38
	MOVWF      R12+0
	MOVLW      93
	MOVWF      R13+0
L_main4:
	DECFSZ     R13+0, 1
	GOTO       L_main4
	DECFSZ     R12+0, 1
	GOTO       L_main4
	DECFSZ     R11+0, 1
	GOTO       L_main4
	NOP
	NOP
;lab5.c,28 :: 		}
	GOTO       L_main1
;lab5.c,30 :: 		}
	GOTO       $+0
; end of _main
