
_initializare:
;labor.c,1 :: 		void initializare()
;labor.c,3 :: 		VRCON.VREN=1;
	BSF        VRCON+0, 7
;labor.c,4 :: 		VRCON.VRR=0;
	BCF        VRCON+0, 5
;labor.c,5 :: 		VRCON.VR3=0;
	BCF        VRCON+0, 3
;labor.c,6 :: 		VRCON.VR2=0;
	BCF        VRCON+0, 2
;labor.c,7 :: 		VRCON.VR1=1;
	BSF        VRCON+0, 1
;labor.c,8 :: 		VRCON.VR0=1;
	BSF        VRCON+0, 0
;labor.c,9 :: 		CMCON0.C2OUT=0;
	BCF        CMCON0+0, 7
;labor.c,10 :: 		CMCON0.C1OUT=0;
	BCF        CMCON0+0, 6
;labor.c,11 :: 		CMCON0.C2INV=0;
	BCF        CMCON0+0, 5
;labor.c,12 :: 		CMCON0.C1INV=0;
	BCF        CMCON0+0, 4
;labor.c,13 :: 		CMCON0.CIS=0;
	BCF        CMCON0+0, 3
;labor.c,14 :: 		CMCON0.CM2=0;
	BCF        CMCON0+0, 2
;labor.c,15 :: 		CMCON0.CM1=1;
	BSF        CMCON0+0, 1
;labor.c,16 :: 		CMCON0.CM0=0;
	BCF        CMCON0+0, 0
;labor.c,17 :: 		TRISB=0;
	CLRF       TRISB+0
;labor.c,19 :: 		}
	RETURN
; end of _initializare

_main:
;labor.c,20 :: 		void main()
;labor.c,22 :: 		initializare();
	CALL       _initializare+0
;labor.c,23 :: 		while(1)
L_main0:
;labor.c,24 :: 		{ PORTB.RB0=CMCON0.C1OUT;
	BTFSC      CMCON0+0, 6
	GOTO       L__main3
	BCF        PORTB+0, 0
	GOTO       L__main4
L__main3:
	BSF        PORTB+0, 0
L__main4:
;labor.c,25 :: 		Delay_ms(500);
	MOVLW      6
	MOVWF      R11+0
	MOVLW      19
	MOVWF      R12+0
	MOVLW      173
	MOVWF      R13+0
L_main2:
	DECFSZ     R13+0, 1
	GOTO       L_main2
	DECFSZ     R12+0, 1
	GOTO       L_main2
	DECFSZ     R11+0, 1
	GOTO       L_main2
	NOP
	NOP
;labor.c,26 :: 		}
	GOTO       L_main0
;labor.c,29 :: 		}
	GOTO       $+0
; end of _main
