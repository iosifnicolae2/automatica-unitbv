
_init_C:
;adc.c,18 :: 		void init_C()
;adc.c,21 :: 		TRISA=0b11111111;
	MOVLW      255
	MOVWF      TRISA+0
;adc.c,22 :: 		ANSEL=0b11111111;
	MOVLW      255
	MOVWF      ANSEL+0
;adc.c,23 :: 		ADCON0.VCFG1=0;
	BCF        ADCON0+0, 6
;adc.c,24 :: 		ADCON0.VCFG0=0;
	BCF        ADCON0+0, 5
;adc.c,25 :: 		ADCON0.VCFG0=0;
	BCF        ADCON0+0, 5
;adc.c,26 :: 		ADCON0.CHS2=0;
	BCF        ADCON0+0, 4
;adc.c,27 :: 		ADCON0.CHS1=0;
	BCF        ADCON0+0, 3
;adc.c,28 :: 		ADCON0.CHS0=0;
	BCF        ADCON0+0, 2
;adc.c,29 :: 		ADCON0.GO_DONE=0;
	BCF        ADCON0+0, 1
;adc.c,30 :: 		ADCON0.ADON=0;
	BCF        ADCON0+0, 0
;adc.c,31 :: 		ADCON0.ADCS2=1;
	BSF        ADCON0+0, 6
;adc.c,32 :: 		ADCON0.ADCS1=0;
	BCF        ADCON0+0, 5
;adc.c,33 :: 		ADCON0.ADCS0=0;
	BCF        ADCON0+0, 4
;adc.c,35 :: 		}
	RETURN
; end of _init_C

_read_ADC:
	CLRF       read_ADC_result_L0+0
	CLRF       read_ADC_result_L0+1
;adc.c,37 :: 		unsigned int read_ADC()
;adc.c,40 :: 		ADCON0.ADON=1;
	BSF        ADCON0+0, 0
;adc.c,41 :: 		ADCON0.GO_DONE=1;
	BSF        ADCON0+0, 1
;adc.c,42 :: 		while(ADCON0.GO_DONE==1)
L_read_ADC0:
	BTFSS      ADCON0+0, 1
	GOTO       L_read_ADC1
;adc.c,43 :: 		{}
	GOTO       L_read_ADC0
L_read_ADC1:
;adc.c,44 :: 		ADCON0.ADON=0;
	BCF        ADCON0+0, 0
;adc.c,46 :: 		result |=ADRESH<<2|ADRESL>>6;
	MOVF       ADRESH+0, 0
	MOVWF      R2+0
	CLRF       R2+1
	RLF        R2+0, 1
	RLF        R2+1, 1
	BCF        R2+0, 0
	RLF        R2+0, 1
	RLF        R2+1, 1
	BCF        R2+0, 0
	MOVLW      6
	MOVWF      R1+0
	MOVF       ADRESL+0, 0
	MOVWF      R0+0
	MOVF       R1+0, 0
L__read_ADC5:
	BTFSC      STATUS+0, 2
	GOTO       L__read_ADC6
	RRF        R0+0, 1
	BCF        R0+0, 7
	ADDLW      255
	GOTO       L__read_ADC5
L__read_ADC6:
	MOVLW      0
	MOVWF      R0+1
	MOVF       R2+0, 0
	IORWF      R0+0, 1
	MOVF       R2+1, 0
	IORWF      R0+1, 1
	MOVF       read_ADC_result_L0+0, 0
	IORWF      R0+0, 1
	MOVF       read_ADC_result_L0+1, 0
	IORWF      R0+1, 1
	MOVF       R0+0, 0
	MOVWF      read_ADC_result_L0+0
	MOVF       R0+1, 0
	MOVWF      read_ADC_result_L0+1
;adc.c,47 :: 		return result;
;adc.c,48 :: 		}
	RETURN
; end of _read_ADC

_main:
;adc.c,50 :: 		void main()
;adc.c,58 :: 		LCD_Init();
	CALL       _Lcd_Init+0
;adc.c,59 :: 		init_c();
	CALL       _init_C+0
;adc.c,61 :: 		while(1)
L_main2:
;adc.c,63 :: 		val=read_ADC();
	CALL       _read_ADC+0
;adc.c,64 :: 		sprinti(ADC,"ADC=%d",VAL);
	MOVLW      main_ADC_L0+0
	MOVWF      FARG_sprinti_wh+0
	MOVLW      ?lstr_1_adc+0
	MOVWF      FARG_sprinti_f+0
	MOVLW      hi_addr(?lstr_1_adc+0
	MOVWF      FARG_sprinti_f+1
	MOVF       R0+0, 0
	MOVWF      FARG_sprinti_wh+3
	MOVF       R0+1, 0
	MOVWF      FARG_sprinti_wh+4
	CALL       _sprinti+0
;adc.c,65 :: 		lcd_out(1,1,ADC);
	MOVLW      1
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      1
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      main_ADC_L0+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
;adc.c,66 :: 		delay_ms(500);
	MOVLW      6
	MOVWF      R11+0
	MOVLW      19
	MOVWF      R12+0
	MOVLW      173
	MOVWF      R13+0
L_main4:
	DECFSZ     R13+0, 1
	GOTO       L_main4
	DECFSZ     R12+0, 1
	GOTO       L_main4
	DECFSZ     R11+0, 1
	GOTO       L_main4
	NOP
	NOP
;adc.c,67 :: 		}
	GOTO       L_main2
;adc.c,73 :: 		}
	GOTO       $+0
; end of _main
