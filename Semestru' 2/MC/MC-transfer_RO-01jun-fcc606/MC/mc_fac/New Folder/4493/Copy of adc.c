// Lcd pinout settings
sbit LCD_RS at RB4_bit;
sbit LCD_EN at RB5_bit;
sbit LCD_D7 at RB3_bit;
sbit LCD_D6 at RB2_bit;
sbit LCD_D5 at RB1_bit;
sbit LCD_D4 at RB0_bit;

// Pin direction
sbit LCD_RS_Direction at TRISB4_bit;
sbit LCD_EN_Direction at TRISB5_bit;
sbit LCD_D7_Direction at TRISB3_bit;
sbit LCD_D6_Direction at TRISB2_bit;
sbit LCD_D5_Direction at TRISB1_bit;
sbit LCD_D4_Direction at TRISB0_bit;

void init_C()
{

TRISA=0b11111111;
ANSEL=0b11111111;
ADCON0.VCFG1=0;
ADCON0.VCFG0=0;
ADCON0.VCFG0=0;
ADCON0.CHS2=0;
ADCON0.CHS1=0;
ADCON0.CHS0=0;
ADCON0.GO_DONE=0;
ADCON0.ADON=0;
ADCON0.ADCS2=1;
ADCON0.ADCS1=0;
ADCON0.ADCS0=0;

}

unsigned int read_ADC()
{ADCON0.ADON=1;
ADCON0.GO_DONE=1;
while(ADCON0.GO_DONE==1)
{}
ADCON0.ADON=0;


return ADRESL;
}

void main()
{
unsigned int val=0;

unsigned int temp;


char ADC[10];
LCD_Init();
init_c();

while(1)
{
val=read_ADC();
sprinti(ADC,"ADC=%d",VAL);
lcd_out(1,1,ADC);
delay_ms(500);
}





}
