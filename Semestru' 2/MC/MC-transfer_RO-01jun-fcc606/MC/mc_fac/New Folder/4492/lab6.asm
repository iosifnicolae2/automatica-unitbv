
_interupt:
;lab6.c,22 :: 		void interupt()
;lab6.c,25 :: 		INTCON.GIE=0;
	BCF        INTCON+0, 7
;lab6.c,27 :: 		if(INTCON.T0IF)
	BTFSS      INTCON+0, 2
	GOTO       L_interupt0
;lab6.c,29 :: 		counter++;
	INCF       _counter+0, 1
	BTFSC      STATUS+0, 2
	INCF       _counter+1, 1
;lab6.c,30 :: 		if(counter==3907)
	MOVF       _counter+1, 0
	XORLW      15
	BTFSS      STATUS+0, 2
	GOTO       L__interupt6
	MOVLW      67
	XORWF      _counter+0, 0
L__interupt6:
	BTFSS      STATUS+0, 2
	GOTO       L_interupt1
;lab6.c,32 :: 		secunde++;
	INCF       _secunde+0, 1
	BTFSC      STATUS+0, 2
	INCF       _secunde+1, 1
;lab6.c,33 :: 		counter=0;
	CLRF       _counter+0
	CLRF       _counter+1
;lab6.c,34 :: 		if(secunde==60)
	MOVLW      0
	XORWF      _secunde+1, 0
	BTFSS      STATUS+0, 2
	GOTO       L__interupt7
	MOVLW      60
	XORWF      _secunde+0, 0
L__interupt7:
	BTFSS      STATUS+0, 2
	GOTO       L_interupt2
;lab6.c,36 :: 		minute++;
	INCF       _minute+0, 1
	BTFSC      STATUS+0, 2
	INCF       _minute+1, 1
;lab6.c,37 :: 		secunde=0;
	CLRF       _secunde+0
	CLRF       _secunde+1
;lab6.c,39 :: 		if(minute=60)
	MOVLW      60
	MOVWF      _minute+0
	MOVLW      0
	MOVWF      _minute+1
;lab6.c,41 :: 		ore++;
	INCF       _ore+0, 1
	BTFSC      STATUS+0, 2
	INCF       _ore+1, 1
;lab6.c,42 :: 		minute=0;
	CLRF       _minute+0
	CLRF       _minute+1
;lab6.c,44 :: 		}
L_interupt2:
;lab6.c,47 :: 		}
L_interupt1:
;lab6.c,48 :: 		}
L_interupt0:
;lab6.c,50 :: 		}
	RETURN
; end of _interupt

_main:
;lab6.c,53 :: 		void main()
;lab6.c,57 :: 		OPTION_REG=0b00001000;
	MOVLW      8
	MOVWF      OPTION_REG+0
;lab6.c,58 :: 		INTCON=0b11100000;
	MOVLW      224
	MOVWF      INTCON+0
;lab6.c,59 :: 		TRISB=0;
	CLRF       TRISB+0
;lab6.c,60 :: 		PORTB=0;
	CLRF       PORTB+0
;lab6.c,61 :: 		lcd_init();
	CALL       _Lcd_Init+0
;lab6.c,62 :: 		TMR0=5;
	MOVLW      5
	MOVWF      TMR0+0
;lab6.c,64 :: 		while(1)
L_main4:
;lab6.c,67 :: 		sprinti(buffer,"ore=%d, min=%d, sec=%d", ore, minute, secunde);
	MOVLW      _buffer+0
	MOVWF      FARG_sprinti_wh+0
	MOVLW      ?lstr_1_lab6+0
	MOVWF      FARG_sprinti_f+0
	MOVLW      hi_addr(?lstr_1_lab6+0
	MOVWF      FARG_sprinti_f+1
	MOVF       _ore+0, 0
	MOVWF      FARG_sprinti_wh+3
	MOVF       _ore+1, 0
	MOVWF      FARG_sprinti_wh+4
	MOVF       _minute+0, 0
	MOVWF      FARG_sprinti_wh+5
	MOVF       _minute+1, 0
	MOVWF      FARG_sprinti_wh+6
	MOVF       _secunde+0, 0
	MOVWF      FARG_sprinti_wh+7
	MOVF       _secunde+1, 0
	MOVWF      FARG_sprinti_wh+8
	CALL       _sprinti+0
;lab6.c,68 :: 		lcd_out(1,1,buffer);
	MOVLW      1
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      1
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      _buffer+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
;lab6.c,71 :: 		}
	GOTO       L_main4
;lab6.c,73 :: 		}
	GOTO       $+0
; end of _main
