
_interrupt:
	MOVWF      R15+0
	SWAPF      STATUS+0, 0
	CLRF       STATUS+0
	MOVWF      ___saveSTATUS+0
	MOVF       PCLATH+0, 0
	MOVWF      ___savePCLATH+0
	CLRF       PCLATH+0
;lab6.c,1 :: 		void interrupt()
;lab6.c,3 :: 		if (INTCON.INTF)
	BTFSS      INTCON+0, 1
	GOTO       L_interrupt0
;lab6.c,5 :: 		INTCON.GIE=0;
	BCF        INTCON+0, 7
;lab6.c,6 :: 		delay_ms(1000);
	MOVLW      11
	MOVWF      R11+0
	MOVLW      38
	MOVWF      R12+0
	MOVLW      93
	MOVWF      R13+0
L_interrupt1:
	DECFSZ     R13+0, 1
	GOTO       L_interrupt1
	DECFSZ     R12+0, 1
	GOTO       L_interrupt1
	DECFSZ     R11+0, 1
	GOTO       L_interrupt1
	NOP
	NOP
;lab6.c,7 :: 		PORTB=0;
	CLRF       PORTB+0
;lab6.c,8 :: 		PORTD=0;
	CLRF       PORTD+0
;lab6.c,11 :: 		PORTB.RB2=1;
	BSF        PORTB+0, 2
;lab6.c,13 :: 		delay_ms(2000);
	MOVLW      21
	MOVWF      R11+0
	MOVLW      75
	MOVWF      R12+0
	MOVLW      190
	MOVWF      R13+0
L_interrupt2:
	DECFSZ     R13+0, 1
	GOTO       L_interrupt2
	DECFSZ     R12+0, 1
	GOTO       L_interrupt2
	DECFSZ     R11+0, 1
	GOTO       L_interrupt2
	NOP
;lab6.c,14 :: 		PORTB.RB1=1;
	BSF        PORTB+0, 1
;lab6.c,15 :: 		PORTB.RB2=0;
	BCF        PORTB+0, 2
;lab6.c,16 :: 		PORTB.RB7=1;
	BSF        PORTB+0, 7
;lab6.c,18 :: 		for(i=9;i>=0;i--)
	MOVLW      9
	MOVWF      R2+0
	MOVLW      0
	MOVWF      R2+1
L_interrupt3:
	MOVLW      128
	XORWF      R2+1, 0
	MOVWF      R0+0
	MOVLW      128
	SUBWF      R0+0, 0
	BTFSS      STATUS+0, 2
	GOTO       L__interrupt12
	MOVLW      0
	SUBWF      R2+0, 0
L__interrupt12:
	BTFSS      STATUS+0, 0
	GOTO       L_interrupt4
;lab6.c,19 :: 		{       PORTD=i*2;
	MOVF       R2+0, 0
	MOVWF      R0+0
	RLF        R0+0, 1
	BCF        R0+0, 0
	MOVF       R0+0, 0
	MOVWF      PORTD+0
;lab6.c,18 :: 		for(i=9;i>=0;i--)
	MOVLW      1
	SUBWF      R2+0, 1
	BTFSS      STATUS+0, 0
	DECF       R2+1, 1
;lab6.c,20 :: 		}
	GOTO       L_interrupt3
L_interrupt4:
;lab6.c,21 :: 		INTCON.GIE=1;
	BSF        INTCON+0, 7
;lab6.c,22 :: 		INTCON.INTF=0;
	BCF        INTCON+0, 1
;lab6.c,23 :: 		}
L_interrupt0:
;lab6.c,24 :: 		}
L__interrupt11:
	MOVF       ___savePCLATH+0, 0
	MOVWF      PCLATH+0
	SWAPF      ___saveSTATUS+0, 0
	MOVWF      STATUS+0
	SWAPF      R15+0, 1
	SWAPF      R15+0, 0
	RETFIE
; end of _interrupt

_main:
;lab6.c,26 :: 		void main() {
;lab6.c,27 :: 		INTCON.GIE=0;
	BCF        INTCON+0, 7
;lab6.c,28 :: 		INTCON.INTE=1;
	BSF        INTCON+0, 4
;lab6.c,29 :: 		INTcON.INTEDG=0;
	BCF        INTCON+0, 6
;lab6.c,30 :: 		TRISB=0;
	CLRF       TRISB+0
;lab6.c,31 :: 		TRISD=0;
	CLRF       TRISD+0
;lab6.c,32 :: 		PORTB=0;
	CLRF       PORTB+0
;lab6.c,33 :: 		PORTD=0;
	CLRF       PORTD+0
;lab6.c,35 :: 		while (1)
L_main6:
;lab6.c,37 :: 		PORTB.RB4=1;
	BSF        PORTB+0, 4
;lab6.c,38 :: 		PORTB.RB3=1;
	BSF        PORTB+0, 3
;lab6.c,40 :: 		delay_ms(3000);
	MOVLW      31
	MOVWF      R11+0
	MOVLW      113
	MOVWF      R12+0
	MOVLW      30
	MOVWF      R13+0
L_main8:
	DECFSZ     R13+0, 1
	GOTO       L_main8
	DECFSZ     R12+0, 1
	GOTO       L_main8
	DECFSZ     R11+0, 1
	GOTO       L_main8
	NOP
;lab6.c,41 :: 		PORTB.RB5=1;
	BSF        PORTB+0, 5
;lab6.c,42 :: 		PORTB.RB4=0;
	BCF        PORTB+0, 4
;lab6.c,43 :: 		delay_ms(1000);
	MOVLW      11
	MOVWF      R11+0
	MOVLW      38
	MOVWF      R12+0
	MOVLW      93
	MOVWF      R13+0
L_main9:
	DECFSZ     R13+0, 1
	GOTO       L_main9
	DECFSZ     R12+0, 1
	GOTO       L_main9
	DECFSZ     R11+0, 1
	GOTO       L_main9
	NOP
	NOP
;lab6.c,44 :: 		PORTB.RB3=0;
	BCF        PORTB+0, 3
;lab6.c,45 :: 		PORTB.RB1=1;
	BSF        PORTB+0, 1
;lab6.c,46 :: 		PORTB.RB5=0;
	BCF        PORTB+0, 5
;lab6.c,47 :: 		PORTB.RB7=1;
	BSF        PORTB+0, 7
;lab6.c,48 :: 		delay_ms(3000);
	MOVLW      31
	MOVWF      R11+0
	MOVLW      113
	MOVWF      R12+0
	MOVLW      30
	MOVWF      R13+0
L_main10:
	DECFSZ     R13+0, 1
	GOTO       L_main10
	DECFSZ     R12+0, 1
	GOTO       L_main10
	DECFSZ     R11+0, 1
	GOTO       L_main10
	NOP
;lab6.c,49 :: 		PORTB.RB1=0;
	BCF        PORTB+0, 1
;lab6.c,50 :: 		PORTB.RB7=0;
	BCF        PORTB+0, 7
;lab6.c,52 :: 		}
	GOTO       L_main6
;lab6.c,53 :: 		}
	GOTO       $+0
; end of _main
