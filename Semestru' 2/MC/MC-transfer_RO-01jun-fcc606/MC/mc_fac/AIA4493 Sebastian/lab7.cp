#line 1 "D:/AIA4493 Sebastian/lab7.c"
char buffer[20];

int secunde =0,minute=0,ore=0;
unsigned int count=0;

sbit LCD_RS at RB4_bit;
sbit LCD_EN at RB5_bit;
sbit LCD_D4 at RB0_bit;
sbit LCD_D5 at RB1_bit;
sbit LCD_D6 at RB2_bit;
sbit LCD_D7 at RB3_bit;

sbit LCD_RS_Direction at TRISB4_bit;
sbit LCD_EN_Direction at TRISB5_bit;
sbit LCD_D4_Direction at TRISB0_bit;
sbit LCD_D5_Direction at TRISB1_bit;
sbit LCD_D6_Direction at TRISB2_bit;
sbit LCD_D7_Direction at TRISB3_bit;


void main() {

TMR0=5;
OPTION_REG=0b00001000;
INTCON=0b11100000;
TRISB=0x00;
PORTB=0x00;

Lcd_init();
Lcd_Cmd(_LCD_CLEAR);
Lcd_Cmd(_LCD_CURSOR_OFF);

 while(1)
 {


 Lcd_Out(1,1,secunde);
 Lcd_Out(1,3,":");
 Lcd_Out(1,4,minute);
 Lcd_Out(1,6,":");
 Lcd_Out(1,7,ore);
 }
}

void interrupt()
{INTCON.GIE=0;
if(INTCON.T0IF)
{
 INTCON.T0IF=0;
 count++;
 if(count==1000)
 { secunde++;
 count = 0;
 if(secunde==60)
 { minute++;
 secunde = 0;
 if(minute==60)
 { ore++;
 minute = 0;
 }
 }
 }

}
INTCON.GIE=1;
}
