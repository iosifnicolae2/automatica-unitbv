
_main:
;L3.c,1 :: 		int main()
;L3.c,9 :: 		TRISD.RD0=1;
	BSF        TRISD+0, 0
;L3.c,10 :: 		TRISD.RD1=1;
	BSF        TRISD+0, 1
;L3.c,11 :: 		TRISD.RD2=1 ;
	BSF        TRISD+0, 2
;L3.c,13 :: 		TRISD.RD3=0;
	BCF        TRISD+0, 3
;L3.c,14 :: 		TRISD.RD4=0;
	BCF        TRISD+0, 4
;L3.c,16 :: 		A=1;
	MOVLW      1
	MOVWF      R1+0
;L3.c,17 :: 		B=0;
	CLRF       R2+0
;L3.c,18 :: 		C=0;
	CLRF       R3+0
;L3.c,19 :: 		while(1)
L_main0:
;L3.c,22 :: 		if ((A==1)&&(PORTD.RD0==1)) {A=0; B=1;}
	MOVF       R1+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main4
	BTFSS      PORTD+0, 0
	GOTO       L_main4
L__main16:
	CLRF       R1+0
	MOVLW      1
	MOVWF      R2+0
L_main4:
;L3.c,23 :: 		if ((B==1)&&(PORTD.RD2==1)) {B=0; C=1;}
	MOVF       R2+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main7
	BTFSS      PORTD+0, 2
	GOTO       L_main7
L__main15:
	CLRF       R2+0
	MOVLW      1
	MOVWF      R3+0
L_main7:
;L3.c,24 :: 		if ((C==1)&&(PORTD.RD1==1)) {C=0; A=1;}
	MOVF       R3+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main10
	BTFSS      PORTD+0, 1
	GOTO       L_main10
L__main14:
	CLRF       R3+0
	MOVLW      1
	MOVWF      R1+0
L_main10:
;L3.c,26 :: 		if (A==1) {PORTD.RD3=0; PORTD.RD4=0;}
	MOVF       R1+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main11
	BCF        PORTD+0, 3
	BCF        PORTD+0, 4
L_main11:
;L3.c,27 :: 		if (B==1) {PORTD.RD3=1; PORTD.RD4=0;}
	MOVF       R2+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main12
	BSF        PORTD+0, 3
	BCF        PORTD+0, 4
L_main12:
;L3.c,28 :: 		if (C==1) {PORTD.RD3=0; PORTD.RD4=1;}
	MOVF       R3+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main13
	BCF        PORTD+0, 3
	BSF        PORTD+0, 4
L_main13:
;L3.c,29 :: 		}
	GOTO       L_main0
;L3.c,30 :: 		}
	GOTO       $+0
; end of _main
