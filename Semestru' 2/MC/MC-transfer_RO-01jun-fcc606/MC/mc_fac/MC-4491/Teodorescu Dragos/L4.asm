
_main:
;L4.c,18 :: 		void main() {
;L4.c,21 :: 		Lcd_Init();
	CALL       _Lcd_Init+0
;L4.c,22 :: 		for( i=0;i<=999;i++)
	CLRF       main_i_L0+0
	CLRF       main_i_L0+1
L_main0:
	MOVLW      128
	XORLW      3
	MOVWF      R0+0
	MOVLW      128
	XORWF      main_i_L0+1, 0
	SUBWF      R0+0, 0
	BTFSS      STATUS+0, 2
	GOTO       L__main4
	MOVF       main_i_L0+0, 0
	SUBLW      231
L__main4:
	BTFSS      STATUS+0, 0
	GOTO       L_main1
;L4.c,26 :: 		sprinti (sir,"Timpul=%d  min",i)  ;
	MOVLW      main_sir_L0+0
	MOVWF      FARG_sprinti_wh+0
	MOVLW      ?lstr_1_L4+0
	MOVWF      FARG_sprinti_f+0
	MOVLW      hi_addr(?lstr_1_L4+0
	MOVWF      FARG_sprinti_f+1
	MOVF       main_i_L0+0, 0
	MOVWF      FARG_sprinti_wh+3
	MOVF       main_i_L0+1, 0
	MOVWF      FARG_sprinti_wh+4
	CALL       _sprinti+0
;L4.c,27 :: 		Lcd_Out(1,1,sir);
	MOVLW      1
	MOVWF      FARG_Lcd_Out_row+0
	MOVLW      1
	MOVWF      FARG_Lcd_Out_column+0
	MOVLW      main_sir_L0+0
	MOVWF      FARG_Lcd_Out_text+0
	CALL       _Lcd_Out+0
;L4.c,28 :: 		Delay_ms(1000);
	MOVLW      11
	MOVWF      R11+0
	MOVLW      38
	MOVWF      R12+0
	MOVLW      93
	MOVWF      R13+0
L_main3:
	DECFSZ     R13+0, 1
	GOTO       L_main3
	DECFSZ     R12+0, 1
	GOTO       L_main3
	DECFSZ     R11+0, 1
	GOTO       L_main3
	NOP
	NOP
;L4.c,29 :: 		Lcd_Cmd(_LCD_CLEAR);
	MOVLW      1
	MOVWF      FARG_Lcd_Cmd_out_char+0
	CALL       _Lcd_Cmd+0
;L4.c,30 :: 		Lcd_Cmd(_LCD_CURSOR_OFF);
	MOVLW      12
	MOVWF      FARG_Lcd_Cmd_out_char+0
	CALL       _Lcd_Cmd+0
;L4.c,22 :: 		for( i=0;i<=999;i++)
	INCF       main_i_L0+0, 1
	BTFSC      STATUS+0, 2
	INCF       main_i_L0+1, 1
;L4.c,32 :: 		}
	GOTO       L_main0
L_main1:
;L4.c,33 :: 		}
	GOTO       $+0
; end of _main
