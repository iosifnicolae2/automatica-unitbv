
_interrupt:
	MOVWF      R15+0
	SWAPF      STATUS+0, 0
	CLRF       STATUS+0
	MOVWF      ___saveSTATUS+0
	MOVF       PCLATH+0, 0
	MOVWF      ___savePCLATH+0
	CLRF       PCLATH+0
;lab5.c,2 :: 		void interrupt() {
;lab5.c,3 :: 		INTCON.GIE=0 ;
	BCF        INTCON+0, 7
;lab5.c,4 :: 		if(INTCON.INTF)
	BTFSS      INTCON+0, 1
	GOTO       L_interrupt0
;lab5.c,6 :: 		PORTD=0b01000100;
	MOVLW      68
	MOVWF      PORTD+0
;lab5.c,7 :: 		delay_ms(5000);
	MOVLW      51
	MOVWF      R11+0
	MOVLW      187
	MOVWF      R12+0
	MOVLW      223
	MOVWF      R13+0
L_interrupt1:
	DECFSZ     R13+0, 1
	GOTO       L_interrupt1
	DECFSZ     R12+0, 1
	GOTO       L_interrupt1
	DECFSZ     R11+0, 1
	GOTO       L_interrupt1
	NOP
	NOP
;lab5.c,8 :: 		INTCON.INTF=0;
	BCF        INTCON+0, 1
;lab5.c,10 :: 		}
L_interrupt0:
;lab5.c,12 :: 		}
L__interrupt4:
	MOVF       ___savePCLATH+0, 0
	MOVWF      PCLATH+0
	SWAPF      ___saveSTATUS+0, 0
	MOVWF      STATUS+0
	SWAPF      R15+0, 1
	SWAPF      R15+0, 0
	RETFIE
; end of _interrupt

_main:
;lab5.c,14 :: 		void main()
;lab5.c,16 :: 		TRISB=0B00000001;
	MOVLW      1
	MOVWF      TRISB+0
;lab5.c,17 :: 		TRISD=0B00000000;
	CLRF       TRISD+0
;lab5.c,18 :: 		PORTB=0X00;
	CLRF       PORTB+0
;lab5.c,19 :: 		PORTD=0X00;
	CLRF       PORTD+0
;lab5.c,20 :: 		INTCON.GIE=1;
	BSF        INTCON+0, 7
;lab5.c,21 :: 		INTCON.INTE=1;
	BSF        INTCON+0, 4
;lab5.c,22 :: 		while(1)
L_main2:
;lab5.c,24 :: 		PORTD = 0b00110000;
	MOVLW      48
	MOVWF      PORTD+0
;lab5.c,26 :: 		}
	GOTO       L_main2
;lab5.c,28 :: 		}
	GOTO       $+0
; end of _main
