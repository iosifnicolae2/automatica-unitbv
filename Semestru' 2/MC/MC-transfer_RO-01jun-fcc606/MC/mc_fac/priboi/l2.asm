
_main:
;l2.c,1 :: 		void main() {
;l2.c,2 :: 		TRISA=0;
	CLRF       TRISA+0
;l2.c,3 :: 		TRISB=0;
	CLRF       TRISB+0
;l2.c,4 :: 		TRISC=0;
	CLRF       TRISC+0
;l2.c,5 :: 		while(1){
L_main0:
;l2.c,6 :: 		PORTA=1;
	MOVLW      1
	MOVWF      PORTA+0
;l2.c,7 :: 		PORTB=2;
	MOVLW      2
	MOVWF      PORTB+0
;l2.c,8 :: 		PORTC=111;
	MOVLW      111
	MOVWF      PORTC+0
;l2.c,9 :: 		Delay_ms( 100);
	MOVLW      2
	MOVWF      R11+0
	MOVLW      4
	MOVWF      R12+0
	MOVLW      186
	MOVWF      R13+0
L_main2:
	DECFSZ     R13+0, 1
	GOTO       L_main2
	DECFSZ     R12+0, 1
	GOTO       L_main2
	DECFSZ     R11+0, 1
	GOTO       L_main2
	NOP
;l2.c,10 :: 		PORTC=127;
	MOVLW      127
	MOVWF      PORTC+0
;l2.c,11 :: 		Delay_ms( 100)  ;
	MOVLW      2
	MOVWF      R11+0
	MOVLW      4
	MOVWF      R12+0
	MOVLW      186
	MOVWF      R13+0
L_main3:
	DECFSZ     R13+0, 1
	GOTO       L_main3
	DECFSZ     R12+0, 1
	GOTO       L_main3
	DECFSZ     R11+0, 1
	GOTO       L_main3
	NOP
;l2.c,12 :: 		PORTC=7  ;
	MOVLW      7
	MOVWF      PORTC+0
;l2.c,13 :: 		Delay_ms( 100);
	MOVLW      2
	MOVWF      R11+0
	MOVLW      4
	MOVWF      R12+0
	MOVLW      186
	MOVWF      R13+0
L_main4:
	DECFSZ     R13+0, 1
	GOTO       L_main4
	DECFSZ     R12+0, 1
	GOTO       L_main4
	DECFSZ     R11+0, 1
	GOTO       L_main4
	NOP
;l2.c,14 :: 		PORTC=125;
	MOVLW      125
	MOVWF      PORTC+0
;l2.c,15 :: 		Delay_ms( 100);
	MOVLW      2
	MOVWF      R11+0
	MOVLW      4
	MOVWF      R12+0
	MOVLW      186
	MOVWF      R13+0
L_main5:
	DECFSZ     R13+0, 1
	GOTO       L_main5
	DECFSZ     R12+0, 1
	GOTO       L_main5
	DECFSZ     R11+0, 1
	GOTO       L_main5
	NOP
;l2.c,16 :: 		PORTC=109;
	MOVLW      109
	MOVWF      PORTC+0
;l2.c,17 :: 		Delay_ms( 100);
	MOVLW      2
	MOVWF      R11+0
	MOVLW      4
	MOVWF      R12+0
	MOVLW      186
	MOVWF      R13+0
L_main6:
	DECFSZ     R13+0, 1
	GOTO       L_main6
	DECFSZ     R12+0, 1
	GOTO       L_main6
	DECFSZ     R11+0, 1
	GOTO       L_main6
	NOP
;l2.c,18 :: 		PORTC=100;
	MOVLW      100
	MOVWF      PORTC+0
;l2.c,19 :: 		Delay_ms( 100);
	MOVLW      2
	MOVWF      R11+0
	MOVLW      4
	MOVWF      R12+0
	MOVLW      186
	MOVWF      R13+0
L_main7:
	DECFSZ     R13+0, 1
	GOTO       L_main7
	DECFSZ     R12+0, 1
	GOTO       L_main7
	DECFSZ     R11+0, 1
	GOTO       L_main7
	NOP
;l2.c,20 :: 		PORTC=79;
	MOVLW      79
	MOVWF      PORTC+0
;l2.c,21 :: 		Delay_ms( 100);
	MOVLW      2
	MOVWF      R11+0
	MOVLW      4
	MOVWF      R12+0
	MOVLW      186
	MOVWF      R13+0
L_main8:
	DECFSZ     R13+0, 1
	GOTO       L_main8
	DECFSZ     R12+0, 1
	GOTO       L_main8
	DECFSZ     R11+0, 1
	GOTO       L_main8
	NOP
;l2.c,22 :: 		PORTC=91;
	MOVLW      91
	MOVWF      PORTC+0
;l2.c,23 :: 		Delay_ms( 100);
	MOVLW      2
	MOVWF      R11+0
	MOVLW      4
	MOVWF      R12+0
	MOVLW      186
	MOVWF      R13+0
L_main9:
	DECFSZ     R13+0, 1
	GOTO       L_main9
	DECFSZ     R12+0, 1
	GOTO       L_main9
	DECFSZ     R11+0, 1
	GOTO       L_main9
	NOP
;l2.c,24 :: 		PORTC=91;
	MOVLW      91
	MOVWF      PORTC+0
;l2.c,25 :: 		Delay_ms( 100);
	MOVLW      2
	MOVWF      R11+0
	MOVLW      4
	MOVWF      R12+0
	MOVLW      186
	MOVWF      R13+0
L_main10:
	DECFSZ     R13+0, 1
	GOTO       L_main10
	DECFSZ     R12+0, 1
	GOTO       L_main10
	DECFSZ     R11+0, 1
	GOTO       L_main10
	NOP
;l2.c,26 :: 		PORTC=6;
	MOVLW      6
	MOVWF      PORTC+0
;l2.c,27 :: 		Delay_ms(100);
	MOVLW      2
	MOVWF      R11+0
	MOVLW      4
	MOVWF      R12+0
	MOVLW      186
	MOVWF      R13+0
L_main11:
	DECFSZ     R13+0, 1
	GOTO       L_main11
	DECFSZ     R12+0, 1
	GOTO       L_main11
	DECFSZ     R11+0, 1
	GOTO       L_main11
	NOP
;l2.c,28 :: 		PORTC=0;
	CLRF       PORTC+0
;l2.c,29 :: 		Delay_ms(10);
	MOVLW      26
	MOVWF      R12+0
	MOVLW      248
	MOVWF      R13+0
L_main12:
	DECFSZ     R13+0, 1
	GOTO       L_main12
	DECFSZ     R12+0, 1
	GOTO       L_main12
	NOP
;l2.c,30 :: 		PORTA=4;
	MOVLW      4
	MOVWF      PORTA+0
;l2.c,31 :: 		PORTB=1;
	MOVLW      1
	MOVWF      PORTB+0
;l2.c,33 :: 		PORTC=111;
	MOVLW      111
	MOVWF      PORTC+0
;l2.c,34 :: 		Delay_ms( 100);
	MOVLW      2
	MOVWF      R11+0
	MOVLW      4
	MOVWF      R12+0
	MOVLW      186
	MOVWF      R13+0
L_main13:
	DECFSZ     R13+0, 1
	GOTO       L_main13
	DECFSZ     R12+0, 1
	GOTO       L_main13
	DECFSZ     R11+0, 1
	GOTO       L_main13
	NOP
;l2.c,35 :: 		PORTC=127;
	MOVLW      127
	MOVWF      PORTC+0
;l2.c,36 :: 		Delay_ms( 100)  ;
	MOVLW      2
	MOVWF      R11+0
	MOVLW      4
	MOVWF      R12+0
	MOVLW      186
	MOVWF      R13+0
L_main14:
	DECFSZ     R13+0, 1
	GOTO       L_main14
	DECFSZ     R12+0, 1
	GOTO       L_main14
	DECFSZ     R11+0, 1
	GOTO       L_main14
	NOP
;l2.c,37 :: 		PORTC=7  ;
	MOVLW      7
	MOVWF      PORTC+0
;l2.c,38 :: 		Delay_ms( 100);
	MOVLW      2
	MOVWF      R11+0
	MOVLW      4
	MOVWF      R12+0
	MOVLW      186
	MOVWF      R13+0
L_main15:
	DECFSZ     R13+0, 1
	GOTO       L_main15
	DECFSZ     R12+0, 1
	GOTO       L_main15
	DECFSZ     R11+0, 1
	GOTO       L_main15
	NOP
;l2.c,39 :: 		PORTC=125;
	MOVLW      125
	MOVWF      PORTC+0
;l2.c,40 :: 		Delay_ms( 100);
	MOVLW      2
	MOVWF      R11+0
	MOVLW      4
	MOVWF      R12+0
	MOVLW      186
	MOVWF      R13+0
L_main16:
	DECFSZ     R13+0, 1
	GOTO       L_main16
	DECFSZ     R12+0, 1
	GOTO       L_main16
	DECFSZ     R11+0, 1
	GOTO       L_main16
	NOP
;l2.c,41 :: 		PORTC=109;
	MOVLW      109
	MOVWF      PORTC+0
;l2.c,42 :: 		Delay_ms( 100);
	MOVLW      2
	MOVWF      R11+0
	MOVLW      4
	MOVWF      R12+0
	MOVLW      186
	MOVWF      R13+0
L_main17:
	DECFSZ     R13+0, 1
	GOTO       L_main17
	DECFSZ     R12+0, 1
	GOTO       L_main17
	DECFSZ     R11+0, 1
	GOTO       L_main17
	NOP
;l2.c,43 :: 		PORTC=100;
	MOVLW      100
	MOVWF      PORTC+0
;l2.c,44 :: 		Delay_ms( 100);
	MOVLW      2
	MOVWF      R11+0
	MOVLW      4
	MOVWF      R12+0
	MOVLW      186
	MOVWF      R13+0
L_main18:
	DECFSZ     R13+0, 1
	GOTO       L_main18
	DECFSZ     R12+0, 1
	GOTO       L_main18
	DECFSZ     R11+0, 1
	GOTO       L_main18
	NOP
;l2.c,45 :: 		PORTC=79;
	MOVLW      79
	MOVWF      PORTC+0
;l2.c,46 :: 		Delay_ms( 100);
	MOVLW      2
	MOVWF      R11+0
	MOVLW      4
	MOVWF      R12+0
	MOVLW      186
	MOVWF      R13+0
L_main19:
	DECFSZ     R13+0, 1
	GOTO       L_main19
	DECFSZ     R12+0, 1
	GOTO       L_main19
	DECFSZ     R11+0, 1
	GOTO       L_main19
	NOP
;l2.c,47 :: 		PORTC=91;
	MOVLW      91
	MOVWF      PORTC+0
;l2.c,48 :: 		Delay_ms( 100);
	MOVLW      2
	MOVWF      R11+0
	MOVLW      4
	MOVWF      R12+0
	MOVLW      186
	MOVWF      R13+0
L_main20:
	DECFSZ     R13+0, 1
	GOTO       L_main20
	DECFSZ     R12+0, 1
	GOTO       L_main20
	DECFSZ     R11+0, 1
	GOTO       L_main20
	NOP
;l2.c,49 :: 		PORTC=91;
	MOVLW      91
	MOVWF      PORTC+0
;l2.c,50 :: 		Delay_ms( 100);
	MOVLW      2
	MOVWF      R11+0
	MOVLW      4
	MOVWF      R12+0
	MOVLW      186
	MOVWF      R13+0
L_main21:
	DECFSZ     R13+0, 1
	GOTO       L_main21
	DECFSZ     R12+0, 1
	GOTO       L_main21
	DECFSZ     R11+0, 1
	GOTO       L_main21
	NOP
;l2.c,51 :: 		PORTC=6;
	MOVLW      6
	MOVWF      PORTC+0
;l2.c,52 :: 		Delay_ms(100);
	MOVLW      2
	MOVWF      R11+0
	MOVLW      4
	MOVWF      R12+0
	MOVLW      186
	MOVWF      R13+0
L_main22:
	DECFSZ     R13+0, 1
	GOTO       L_main22
	DECFSZ     R12+0, 1
	GOTO       L_main22
	DECFSZ     R11+0, 1
	GOTO       L_main22
	NOP
;l2.c,53 :: 		PORTC=0;
	CLRF       PORTC+0
;l2.c,54 :: 		Delay_ms(10);
	MOVLW      26
	MOVWF      R12+0
	MOVLW      248
	MOVWF      R13+0
L_main23:
	DECFSZ     R13+0, 1
	GOTO       L_main23
	DECFSZ     R12+0, 1
	GOTO       L_main23
	NOP
;l2.c,57 :: 		PORTA=2;
	MOVLW      2
	MOVWF      PORTA+0
;l2.c,58 :: 		Delay_ms(100);
	MOVLW      2
	MOVWF      R11+0
	MOVLW      4
	MOVWF      R12+0
	MOVLW      186
	MOVWF      R13+0
L_main24:
	DECFSZ     R13+0, 1
	GOTO       L_main24
	DECFSZ     R12+0, 1
	GOTO       L_main24
	DECFSZ     R11+0, 1
	GOTO       L_main24
	NOP
;l2.c,59 :: 		PORTA=1;
	MOVLW      1
	MOVWF      PORTA+0
;l2.c,60 :: 		PORTB=2;   };
	MOVLW      2
	MOVWF      PORTB+0
	GOTO       L_main0
;l2.c,61 :: 		}
	GOTO       $+0
; end of _main
