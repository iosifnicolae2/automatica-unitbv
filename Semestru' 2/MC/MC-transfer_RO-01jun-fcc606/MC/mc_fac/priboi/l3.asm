
_main:
;l3.c,1 :: 		void main() {
;l3.c,8 :: 		TRISD.RD0=1;
	BSF        TRISD+0, 0
;l3.c,9 :: 		TRISD.RD1=1;
	BSF        TRISD+0, 1
;l3.c,10 :: 		TRISD.RD2=1;
	BSF        TRISD+0, 2
;l3.c,11 :: 		TRISD.RD3=1;
	BSF        TRISD+0, 3
;l3.c,12 :: 		TRISD.RD4=1;
	BSF        TRISD+0, 4
;l3.c,13 :: 		TRISD.RC1=0;
	BCF        TRISD+0, 1
;l3.c,14 :: 		TRISD.RC2=0;
	BCF        TRISD+0, 2
;l3.c,15 :: 		TRISD.RC3=0;
	BCF        TRISD+0, 3
;l3.c,16 :: 		TRISD.RC4=0;
	BCF        TRISD+0, 4
;l3.c,18 :: 		A=1;
	MOVLW      1
	MOVWF      R1+0
;l3.c,19 :: 		B=0;
	CLRF       R2+0
;l3.c,20 :: 		C=0;
	CLRF       R3+0
;l3.c,21 :: 		D=0;
	CLRF       R4+0
;l3.c,22 :: 		E=0;
	CLRF       R5+0
;l3.c,23 :: 		while(1)
L_main0:
;l3.c,25 :: 		if((A==1)&&(PORTD.RD0==1)){A=0;B=1;}
	MOVF       R1+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main4
	BTFSS      PORTD+0, 0
	GOTO       L_main4
L__main26:
	CLRF       R1+0
	MOVLW      1
	MOVWF      R2+0
L_main4:
;l3.c,26 :: 		if((B==1)&&(PORTD.RD2==1)){B=0;C=1;}
	MOVF       R2+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main7
	BTFSS      PORTD+0, 2
	GOTO       L_main7
L__main25:
	CLRF       R2+0
	MOVLW      1
	MOVWF      R3+0
L_main7:
;l3.c,27 :: 		if ((C==1)&&(PORTD.RD1=1)){C=0;A=1;}
	MOVF       R3+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main10
	BSF        PORTD+0, 1
	BTFSS      PORTD+0, 1
	GOTO       L_main10
L__main24:
	CLRF       R3+0
	MOVLW      1
	MOVWF      R1+0
L_main10:
;l3.c,29 :: 		if((D==1)&&(PORTD.RC0==1)){D=0;E=1;}
	MOVF       R4+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main13
	BTFSS      PORTD+0, 0
	GOTO       L_main13
L__main23:
	CLRF       R4+0
	MOVLW      1
	MOVWF      R5+0
L_main13:
;l3.c,30 :: 		if((E==1)&&(PORTD.RC2==1)){E=0;D=1;}
	MOVF       R5+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main16
	BTFSS      PORTD+0, 2
	GOTO       L_main16
L__main22:
	CLRF       R5+0
	MOVLW      1
	MOVWF      R4+0
L_main16:
;l3.c,32 :: 		if (A==1) {PORTD.RD3=0; PORTD.RD4=0;}
	MOVF       R1+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main17
	BCF        PORTD+0, 3
	BCF        PORTD+0, 4
L_main17:
;l3.c,33 :: 		if (B==1){ PORTD.RD3=1;PORTD.RD4=0;}
	MOVF       R2+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main18
	BSF        PORTD+0, 3
	BCF        PORTD+0, 4
L_main18:
;l3.c,34 :: 		if(C==1){PORTD.RD3=0;PORTD.RD4=1;}
	MOVF       R3+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main19
	BCF        PORTD+0, 3
	BSF        PORTD+0, 4
L_main19:
;l3.c,36 :: 		if (D==1) {PORTD.RC3=1; PORTD.RC4=0;}
	MOVF       R4+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main20
	BSF        PORTD+0, 3
	BCF        PORTD+0, 4
L_main20:
;l3.c,37 :: 		if (E==1){ PORTD.RC3=1;PORTD.RC4=0;}
	MOVF       R5+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main21
	BSF        PORTD+0, 3
	BCF        PORTD+0, 4
L_main21:
;l3.c,38 :: 		}
	GOTO       L_main0
;l3.c,39 :: 		}
	GOTO       $+0
; end of _main
