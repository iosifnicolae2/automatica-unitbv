
_main:
;l1.c,1 :: 		void main() {
;l1.c,2 :: 		TRISB=0;
	CLRF       TRISB+0
;l1.c,3 :: 		PORTB=1;
	MOVLW      1
	MOVWF      PORTB+0
;l1.c,4 :: 		DELAY_MS(500);
	MOVLW      6
	MOVWF      R11+0
	MOVLW      19
	MOVWF      R12+0
	MOVLW      173
	MOVWF      R13+0
L_main0:
	DECFSZ     R13+0, 1
	GOTO       L_main0
	DECFSZ     R12+0, 1
	GOTO       L_main0
	DECFSZ     R11+0, 1
	GOTO       L_main0
	NOP
	NOP
;l1.c,5 :: 		while(PORTB<128)
L_main1:
	MOVLW      128
	SUBWF      PORTB+0, 0
	BTFSC      STATUS+0, 0
	GOTO       L_main2
;l1.c,8 :: 		DELAY_MS(500);
	MOVLW      6
	MOVWF      R11+0
	MOVLW      19
	MOVWF      R12+0
	MOVLW      173
	MOVWF      R13+0
L_main3:
	DECFSZ     R13+0, 1
	GOTO       L_main3
	DECFSZ     R12+0, 1
	GOTO       L_main3
	DECFSZ     R11+0, 1
	GOTO       L_main3
	NOP
	NOP
;l1.c,9 :: 		PORTB=PORTB*2;
	MOVF       PORTB+0, 0
	MOVWF      R0+0
	RLF        R0+0, 1
	BCF        R0+0, 0
	MOVF       R0+0, 0
	MOVWF      PORTB+0
;l1.c,10 :: 		DELAY_MS(500);
	MOVLW      6
	MOVWF      R11+0
	MOVLW      19
	MOVWF      R12+0
	MOVLW      173
	MOVWF      R13+0
L_main4:
	DECFSZ     R13+0, 1
	GOTO       L_main4
	DECFSZ     R12+0, 1
	GOTO       L_main4
	DECFSZ     R11+0, 1
	GOTO       L_main4
	NOP
	NOP
;l1.c,11 :: 		if (PORTB==128)  PORTB=1;
	MOVF       PORTB+0, 0
	XORLW      128
	BTFSS      STATUS+0, 2
	GOTO       L_main5
	MOVLW      1
	MOVWF      PORTB+0
L_main5:
;l1.c,13 :: 		}
	GOTO       L_main1
L_main2:
;l1.c,14 :: 		}
	GOTO       $+0
; end of _main
